﻿namespace Member.Test.WinUI
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtTerm1 = new System.Windows.Forms.TextBox();
            this.txtTerm2 = new System.Windows.Forms.TextBox();
            this.cmbOperations = new System.Windows.Forms.ComboBox();
            this.txtResult = new System.Windows.Forms.TextBox();
            this.gbxOperations = new System.Windows.Forms.GroupBox();
            this.lblOp2 = new System.Windows.Forms.Label();
            this.lblResult = new System.Windows.Forms.Label();
            this.lblOp1 = new System.Windows.Forms.Label();
            this.btnRegenerateMembers = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pbMain = new System.Windows.Forms.ProgressBar();
            this.gbxOperations.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtTerm1
            // 
            this.txtTerm1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTerm1.ForeColor = System.Drawing.Color.Blue;
            this.txtTerm1.Location = new System.Drawing.Point(60, 19);
            this.txtTerm1.Multiline = true;
            this.txtTerm1.Name = "txtTerm1";
            this.txtTerm1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtTerm1.Size = new System.Drawing.Size(247, 107);
            this.txtTerm1.TabIndex = 0;
            this.txtTerm1.TextChanged += new System.EventHandler(this.txtTerm1_TextChanged);
            // 
            // txtTerm2
            // 
            this.txtTerm2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTerm2.ForeColor = System.Drawing.Color.Blue;
            this.txtTerm2.Location = new System.Drawing.Point(378, 19);
            this.txtTerm2.Multiline = true;
            this.txtTerm2.Name = "txtTerm2";
            this.txtTerm2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtTerm2.Size = new System.Drawing.Size(265, 107);
            this.txtTerm2.TabIndex = 1;
            this.txtTerm2.TextChanged += new System.EventHandler(this.txtTerm2_TextChanged);
            // 
            // cmbOperations
            // 
            this.cmbOperations.FormattingEnabled = true;
            this.cmbOperations.Items.AddRange(new object[] {
            "Please Select an Op.",
            "Addition",
            "Subtraction",
            "Multiplication",
            "Division"});
            this.cmbOperations.Location = new System.Drawing.Point(107, 132);
            this.cmbOperations.Name = "cmbOperations";
            this.cmbOperations.Size = new System.Drawing.Size(200, 21);
            this.cmbOperations.TabIndex = 2;
            this.cmbOperations.SelectedIndexChanged += new System.EventHandler(this.cmbOperations_SelectedIndexChanged);
            // 
            // txtResult
            // 
            this.txtResult.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtResult.ForeColor = System.Drawing.Color.Red;
            this.txtResult.Location = new System.Drawing.Point(107, 159);
            this.txtResult.Multiline = true;
            this.txtResult.Name = "txtResult";
            this.txtResult.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtResult.Size = new System.Drawing.Size(536, 100);
            this.txtResult.TabIndex = 3;
            this.txtResult.TextChanged += new System.EventHandler(this.txtResult_TextChanged);
            // 
            // gbxOperations
            // 
            this.gbxOperations.Controls.Add(this.lblOp2);
            this.gbxOperations.Controls.Add(this.lblResult);
            this.gbxOperations.Controls.Add(this.lblOp1);
            this.gbxOperations.Controls.Add(this.btnRegenerateMembers);
            this.gbxOperations.Controls.Add(this.label4);
            this.gbxOperations.Controls.Add(this.label3);
            this.gbxOperations.Controls.Add(this.label2);
            this.gbxOperations.Controls.Add(this.label1);
            this.gbxOperations.Controls.Add(this.cmbOperations);
            this.gbxOperations.Controls.Add(this.txtResult);
            this.gbxOperations.Controls.Add(this.txtTerm2);
            this.gbxOperations.Controls.Add(this.txtTerm1);
            this.gbxOperations.Location = new System.Drawing.Point(12, 12);
            this.gbxOperations.Name = "gbxOperations";
            this.gbxOperations.Size = new System.Drawing.Size(649, 265);
            this.gbxOperations.TabIndex = 3;
            this.gbxOperations.TabStop = false;
            this.gbxOperations.Text = "Operations";
            // 
            // lblOp2
            // 
            this.lblOp2.AutoSize = true;
            this.lblOp2.ForeColor = System.Drawing.Color.Blue;
            this.lblOp2.Location = new System.Drawing.Point(324, 38);
            this.lblOp2.Name = "lblOp2";
            this.lblOp2.Size = new System.Drawing.Size(43, 13);
            this.lblOp2.TabIndex = 5;
            this.lblOp2.Text = "[lblOp1]";
            // 
            // lblResult
            // 
            this.lblResult.AutoSize = true;
            this.lblResult.ForeColor = System.Drawing.Color.Red;
            this.lblResult.Location = new System.Drawing.Point(6, 183);
            this.lblResult.Name = "lblResult";
            this.lblResult.Size = new System.Drawing.Size(93, 13);
            this.lblResult.TabIndex = 5;
            this.lblResult.Text = "[Length Of Result]";
            // 
            // lblOp1
            // 
            this.lblOp1.AutoSize = true;
            this.lblOp1.ForeColor = System.Drawing.Color.Blue;
            this.lblOp1.Location = new System.Drawing.Point(6, 38);
            this.lblOp1.Name = "lblOp1";
            this.lblOp1.Size = new System.Drawing.Size(43, 13);
            this.lblOp1.TabIndex = 5;
            this.lblOp1.Text = "[lblOp1]";
            // 
            // btnRegenerateMembers
            // 
            this.btnRegenerateMembers.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRegenerateMembers.Location = new System.Drawing.Point(378, 130);
            this.btnRegenerateMembers.Name = "btnRegenerateMembers";
            this.btnRegenerateMembers.Size = new System.Drawing.Size(265, 23);
            this.btnRegenerateMembers.TabIndex = 4;
            this.btnRegenerateMembers.Text = "Regenerate Members";
            this.btnRegenerateMembers.UseVisualStyleBackColor = true;
            this.btnRegenerateMembers.Click += new System.EventHandler(this.btnRegenerateMembers_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 161);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Result";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 135);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Select Operation:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(324, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Member:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Member:";
            // 
            // pbMain
            // 
            this.pbMain.Location = new System.Drawing.Point(12, 283);
            this.pbMain.Name = "pbMain";
            this.pbMain.Size = new System.Drawing.Size(649, 22);
            this.pbMain.Step = 1;
            this.pbMain.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.pbMain.TabIndex = 4;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(673, 310);
            this.Controls.Add(this.pbMain);
            this.Controls.Add(this.gbxOperations);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "MainForm";
            this.Text = "Make Enourmous Operations with Member!";
            this.gbxOperations.ResumeLayout(false);
            this.gbxOperations.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtTerm1;
        private System.Windows.Forms.TextBox txtTerm2;
        private System.Windows.Forms.ComboBox cmbOperations;
        private System.Windows.Forms.TextBox txtResult;
        private System.Windows.Forms.GroupBox gbxOperations;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnRegenerateMembers;
        private System.Windows.Forms.Label lblOp2;
        private System.Windows.Forms.Label lblResult;
        private System.Windows.Forms.Label lblOp1;
        private System.Windows.Forms.ProgressBar pbMain;
    }
}

