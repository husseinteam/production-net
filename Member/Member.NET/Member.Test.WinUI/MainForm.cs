﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Member.Core.Types;
using System.Threading.Tasks;
using System.Threading;

namespace Member.Test.WinUI {
    public partial class MainForm : Form {

        private static Random r = new Random();

        public MainForm() {

            InitializeComponent();
            RegenerateMembers();
            Application.EnableVisualStyles();
            CheckForIllegalCrossThreadCalls = false;

        }

        private void cmbOperations_SelectedIndexChanged(object sender, EventArgs e) {
            if (cmbOperations.SelectedIndex == -1)
                return;

            var tokenSource = new CancellationTokenSource();
            var token = tokenSource.Token;

            var t1 = Task.Factory.StartNew(() => {
                while (!tokenSource.IsCancellationRequested) {
                    if (pbMain.Value < 100) {
                        Thread.SpinWait(1000000);
                        pbMain.PerformStep();
                    } else {
                        pbMain.Value = 0;
                    }
                }
                pbMain.Value = 0;
            }, token);

            var t2 = Task.Factory.StartNew(() => {
                Operate();
            }).ContinueWith((task) => tokenSource.Cancel());

            Task.WaitAny(t1, t2);
        }

        private void Operate() {
            switch (cmbOperations.SelectedIndex) {
                case 1:
                    Add();
                    break;
                case 2:
                    Subtract();
                    break;
                case 3:
                    Multiply();
                    break;
                case 4:
                    Divide();
                    break;
                default:
                    break;
            }
        }

        private void Divide() {
            TMember m1;
            TMember m2;
            ConstructTerms(out m1, out m2);
            txtResult.Text = (m1 / m2).ToString();
        }

        private void Multiply() {
            TMember m1;
            TMember m2;
            ConstructTerms(out m1, out m2);
            txtResult.Text = (m1 * m2).ToString();
        }

        private void Add() {
            TMember m1;
            TMember m2;
            ConstructTerms(out m1, out m2);
            txtResult.Text = (m1 + m2).ToString();
        }

        private void Subtract() {
            TMember m1;
            TMember m2;
            ConstructTerms(out m1, out m2);
            txtResult.Text = (m1 - m2).ToString();
        }

        private void ConstructTerms(out TMember m1, out TMember m2) {
            m1 = TMember.FromString(txtTerm1.Text, new TRadix(10));
            m2 = TMember.FromString(txtTerm2.Text, new TRadix(10));
        }

        private void btnRegenerateMembers_Click(object sender, EventArgs e) {

            RegenerateMembers();

        }

        private void RegenerateMembers() {
            txtTerm1.Text = RegenerateMemberString(1024);
            txtTerm2.Text = RegenerateMemberString(2048);
        }

        private static String RegenerateMemberString(Int32 loop) {
            var s = new StringBuilder();
            for (int i = 0; i < loop; i++) {
                s.Append(r.Next(1, 999).ToString());
            }
            return s.ToString();
        }

        private void txtTerm1_TextChanged(object sender, EventArgs e) {

            lblOp1.Text = (sender as TextBox).Text.Length.ToString();

        }

        private void txtTerm2_TextChanged(object sender, EventArgs e) {

            lblOp2.Text = (sender as TextBox).Text.Length.ToString();

        }

        private void txtResult_TextChanged(object sender, EventArgs e) {

            lblResult.Text = (sender as TextBox).Text.Length.ToString();

        }

        private void gbxOperations_Enter(object sender, EventArgs e) {

        }

    }
}
