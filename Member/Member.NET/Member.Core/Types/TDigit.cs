﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Member.Core.Constraints;

namespace Member.Core.Types
{
    public class TDigit : TSelfValidator<uint>
    {
        #region ctors
        public TDigit(uint value, TRadix r)
            : base(v =>v >= 0 && v < r.Value)
        {
            _Value = Validate(value);
            _Radix = r;
        }
        #endregion

        #region ToString()
        public override string ToString()
        {
            return "" + Value;
        }
        #endregion

        #region properties
        public uint Value
        {
            get { return _Value; }
            set { _Value = Validate(value); }
        }
        public TRadix Radix
        {
            get { return _Radix; }
        }
        #endregion

        #region fields
        private uint _Value;
        private TRadix _Radix;
        #endregion
    }
}
