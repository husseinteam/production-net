﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Member.Core.Arithmetics;
using Member.Core.Constraints;

namespace Member.Core.Types
{
    public class TMember
    {
        #region ctors
        public TMember(TRadix radix)
        {
            _Digits = new List<TDigit>();
            _Radix = radix;
            _IndexValidatorDelegate = o => o >= -this.Length && o < this.Length;
            _IndexObligatorDelegate = i_ => i_ >= 0 ? i_ : i_ + this.Length;
        }
        #endregion

        #region static generators
        public static TMember FromString(string repr, TRadix radix)
        {
            string str =
                SValidator<string>.Validate(s => s.All<char>(ch => Char.IsNumber(ch)), repr);
            char[] digits = str.Reverse<char>().ToArray<char>();
            TMember m = new TMember(radix);
            int limit = digits.Length;
            for (int i = 0; i < limit; i++)
            {
                m.AppendDigit(new TDigit(uint.Parse(""+digits[i]), radix));
            }
            return m;
        }
        public static TMember FromInt(uint n, TRadix rdx)
        {
            TMember m = new TMember(rdx);
            n = SValidator<uint>.Validate(num => num >= 0, n);
            uint carry = 0; uint temp = n;
            while (temp >= rdx.Value)
            {
                var q = SCalculator.SplitInteger(temp, rdx.Value);
                carry = q.carry;
                temp = q.truediv;
                m.AppendDigit(new TDigit(carry, rdx));
            }
            m.AppendDigit(new TDigit(temp, rdx));
            return m;
        }
        public static TMember FromMember(TMember m)
        {
            TMember mclone = new TMember(m.Radix);
            int limit = m.Length;
            for (int i = 0; i < limit; i++)
            {
                mclone.AppendDigit(m[i]);
            }
            return mclone;
        }
        public static TMember Zero(TRadix radix)
        {
            return TMember.FromInt(0, radix);
        }
        #endregion

        #region operator overloads
        public static TMember operator +(TMember term1, TDigit term2)
        {
            return SCalculator.Adder.Add(term1, term2);
        }
        public static TMember operator +(TMember term1, TMember term2)
        {
            return SCalculator.Adder.Add(term1, term2);
        }
        public static TMember operator -(TMember term1, TDigit term2)
        {
            return SCalculator.Subtractor.Abs(term1, term2);
        }
        public static TMember operator -(TMember term1, TMember term2)
        {
            return SCalculator.Subtractor.Abs(term1, term2);
        }
        public static TMember operator *(TMember term1, TMember term2)
        {
            return SCalculator.Multiplier.Multiply(term1, term2);
        }
        public static TMember operator *(TMember term1, TDigit term2)
        {
            return SCalculator.Multiplier.MultiplyDigit(term1, term2);
        }
        public static TMember operator /(TMember term1, TMember term2)
        {
            return SCalculator.Divisor.DivMod(term1, term2).div;
        }
        public static TMember operator /(TMember term1, TDigit term2)
        {
            return SCalculator.Divisor.DivMod(term1, term2).div;
        }
        public static TMember operator %(TMember term1, TMember term2)
        {
            return SCalculator.Divisor.DivMod(term1, term2).mod;
        }
        public static TMember operator %(TMember term1, TDigit term2)
        {
            return SCalculator.Divisor.DivMod(term1, term2).mod;
        }
        public static bool operator >(TMember term1, TMember term2)
        {
            return SCalculator.Comparer.Gt(term1, term2);
        }
        public static bool operator <(TMember term1, TMember term2)
        {
            return SCalculator.Comparer.Lt(term1, term2);
        }
        public static bool operator >(TMember term1, TDigit term2)
        {
            return SCalculator.Comparer.Gt(term1, term2);
        }
        public static bool operator <(TMember term1, TDigit term2)
        {
            return SCalculator.Comparer.Lt(term1, term2);
        }
        public static bool operator >=(TMember term1, TMember term2)
        {
            return SCalculator.Comparer.Ge(term1, term2);
        }
        public static bool operator >=(TMember term1, TDigit term2)
        {
            return SCalculator.Comparer.Ge(term1, term2);
        }
        public static bool operator <=(TMember term1, TDigit term2)
        {
            return SCalculator.Comparer.Le(term1, term2);
        }
        public static bool operator <=(TMember term1, TMember term2)
        {
            return SCalculator.Comparer.Le(term1, term2);
        }
        public static bool operator ==(TMember term1, TMember term2)
        {
            return SCalculator.Comparer.Eq(term1, term2);
        }
        public static bool operator ==(TMember term1, TDigit term2)
        {
            return SCalculator.Comparer.Eq(term1, term2);
        }
        public static bool operator !=(TMember term1, TMember term2)
        {
            return SCalculator.Comparer.Ne(term1, term2);
        }
        public static bool operator !=(TMember term1, TDigit term2)
        {
            return SCalculator.Comparer.Ne(term1, term2);
        }
       
        #endregion

        #region public methods
        internal void AppendDigit(TDigit d)
        {
            _Digits.Add(d);
        }
        internal TMember PadBegin(TDigit padDigit, uint n)
        {
            for (int i = 0; i < n; i++)
            {
                _Digits.Insert(0, padDigit);
            }
            return this;
        }
        internal TMember InsertBegin(TDigit digit)
        {
            _Digits.Insert(0, digit);
            return this;
        }
        internal TMember Slice(int begin, int end)
        {
            SValidator<int>.Validate(b => b < end, begin);
            TMember slice = new TMember(_Radix);

            for (int i = begin; i < end; i++)
            {
                slice.AppendDigit(this[i]);
            }

            return slice;
        }
        internal TMember TrimEnd()
        {
            for (int i = Length - 1; i >= 0; i--)
            {
                if (_Digits[i].Value != 0)
                    break;
                _Digits.RemoveAt(i);
            }
            return this;
        }
        #endregion

        #region overrides
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            for (int i = Length-1; i >= 0; i--)
            {
                sb.Append(_Digits[i].Value.ToString());
            }
            return sb.ToString();
        }
        public override bool Equals(object obj)
        {
            if (obj is uint)
                return SCalculator.Comparer.Eq(this, TMember.FromInt((uint)obj, _Radix));
            else if (obj is TMember)
                return SCalculator.Comparer.Eq(this, obj as TMember);
            else
                throw new NotSupportedException("not supported");
        }
        public override int GetHashCode()
        {
            return _Digits.GetHashCode();
        }
        #endregion

        #region indexer
        public TDigit this[int index]
        {
            get
            {
                int oblindex =
                    SObligator<int>.Obligate(_IndexValidatorDelegate, _IndexObligatorDelegate, index);
                return _Digits[oblindex];
            }
            set
            {
                _Digits[index] = value;
            }
        }
        #endregion

        #region properties
        public int Length
        {
            get { return _Digits.Count; }
        }
        public TRadix Radix
        {
            get { return _Radix; }
        }
        #endregion

        #region private fields
        private TRadix _Radix;
        List<TDigit> _Digits;
        DValidator<int> _IndexValidatorDelegate;
        DObligator<int> _IndexObligatorDelegate;
        #endregion

        
    }
}
