﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Member.Core.Constraints
{
    public static class SObligator<TType>
    {
        #region methods
        public static TType Obligate(DValidator<TType> validator, DObligator<TType> obligator, TType that)
        {
            return obligator(SValidator<TType>.Validate(validator, that));
        }
        #endregion
    }
}
