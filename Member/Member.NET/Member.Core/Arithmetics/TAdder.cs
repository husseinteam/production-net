﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Member.Core.Types;
using Member.Core.Constraints;

namespace Member.Core.Arithmetics
{
    internal class TAdder
    {
        internal TMember Add(TMember term1, TMember term2)
        {
            dynamic terms = SCalculator.ConstructTerms(term1, term2);
            TMember augend = terms.greater;
            TMember addend = terms.smaller;
            TMember summand = terms.opresult;

            uint c = 0;
            for (int i = 0; i < addend.Length; i++)
            {
                summand.AppendDigit(CarryDigit(augend[i], addend[i], ref c));
            }

            for (int i = addend.Length; i < augend.Length; i++)
            {
                summand.AppendDigit(CarryDigit(augend[i], new TDigit(0, augend.Radix), ref c));
            }
            if (c > 0)
                summand.AppendDigit(new TDigit(c, summand.Radix));
            return summand;
        }

        internal TMember Add(TMember term1, TDigit term2)
        {
            SValidator<TMember>.Validate(m => m.Radix == term2.Radix, term1);
            TMember summand = new TMember(term1.Radix);

            uint c = 0;
            for (int i = 0; i < term1.Length; i++)
            {
                summand.AppendDigit(CarryDigit(term1[i], term2, ref c));
            }
            if (c > 0)
                summand.AppendDigit(new TDigit(c, summand.Radix));

            return summand;
        }

        private TDigit CarryDigit(TDigit d1, TDigit d2, ref uint c)
        {
            SValidator<TDigit>.Validate(d => d.Radix == d2.Radix, d1);

            uint radix = d1.Radix.Value;
            uint digit = d1.Value + d2.Value + c;
            if (digit >= radix)
            {
                c = 1;
                digit %= radix;
            }
            else
            {
                //no overflow
                c = 0;
            }
            return new TDigit(digit, d1.Radix);
        }
    }
}
