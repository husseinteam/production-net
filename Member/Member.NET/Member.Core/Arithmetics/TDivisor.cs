﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Member.Core.Types;
using Member.Core.Constraints;

namespace Member.Core.Arithmetics
{
    internal class TDivisor
    {
        #region handlers
        internal dynamic DivMod(TMember term1, TMember term2)
        {
            dynamic terms = SCalculator.ConstructTerms(term1, term2);
            TMember dividend = terms.greater;
            TMember divisor = terms.smaller;
            TMember quotient = terms.opresult;

            //Evaluate First Slice
            TMember firstSlice =
                dividend.Slice(dividend.Length - divisor.Length, dividend.Length);
            if (firstSlice < divisor)
                firstSlice.InsertBegin(dividend[-1-divisor.Length]);
            
            //Divison starts
            for (int i = firstSlice.Length; i < dividend.Length; i++)
            {
                dynamic rq = DivideSimple(firstSlice, divisor);
                quotient.InsertBegin(rq.q);
                //pop next from dividend
                firstSlice = rq.r.InsertBegin(dividend[-i - 1]);
            }
            dynamic last = DivideSimple(firstSlice, divisor);
            quotient.InsertBegin(last.q);

            return new { div = quotient.TrimEnd(), mod = last.r.TrimEnd() };
        }
        private dynamic DivideSimple(TMember dividend, TMember divisor)
        {
            SValidator<TMember>.Validate(m => m.Radix == divisor.Radix, dividend);
            TMember remainder = TMember.FromMember(dividend);
            uint quotient = 0;

            while (remainder >= divisor)
            {
                remainder -= divisor;
                quotient++;
                SValidator<uint>.Validate(q => q != 0, quotient);
            }
            if (quotient >= dividend.Radix.Value)
                throw new InvalidOperationException(
                    String.Format("qoutient<{0}> overflows radix<{1}>"
                    , quotient, dividend.Radix.Value));
            return new { r = remainder, q = new TDigit(quotient, dividend.Radix) };
        }
        #endregion

        internal dynamic DivMod(TMember term1, TDigit term2)
        {
            return DivMod(term1, TMember.FromInt(term2.Value, term2.Radix));
        }
    }
}
