﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Member.Core.Types;
using Member.Core.Constraints;

namespace Member.Core.Arithmetics
{
    internal class TConverter
    {
        #region handlers
        internal TMember DecimalToRadix(TMember decimalTerm, TRadix rdx)
        {
            if (decimalTerm.Radix.Equals(rdx))
                return decimalTerm;
            SValidator<TMember>.Validate(m => m.Radix.Value == 10, decimalTerm);
            TMember radixTerm = new TMember(rdx);
            TMember divisionTerm = TMember.FromMember(decimalTerm);
            TDigit modulus = null;
            TMember toRadixMember = TMember.FromInt(rdx.Value, new TRadix(10));
            while (divisionTerm >= toRadixMember)
            {
                uint d = SValidator<TMember>.Validate(m=>m.Length == 1, divisionTerm % toRadixMember)[0].Value;
                modulus = new TDigit(d, rdx);
                divisionTerm /= toRadixMember;
                radixTerm.AppendDigit(modulus);
            }
            uint lastDigit = SValidator<TMember>.Validate(m => m.Length == 1, divisionTerm)[0].Value;
            radixTerm.AppendDigit(new TDigit(lastDigit, rdx));
            return radixTerm;
        }
        internal TMember ToDecimal(TMember term)
        {
            if (term.Radix.Equals(10))
                return term;
            TMember decimalTerm = TMember.FromInt(0, term.Radix);
            int limit = term.Length;
            for (uint i = 0; i < limit; i++)
            {
                decimalTerm += TMember.FromInt(1, term.Radix).PadBegin(new TDigit(0, term.Radix), i) * term[(int)i];
            }
            return decimalTerm;
        }
        #endregion
    }
}
