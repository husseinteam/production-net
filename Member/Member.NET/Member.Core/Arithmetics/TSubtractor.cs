﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Member.Core.Types;
using Member.Core.Constraints;

namespace Member.Core.Arithmetics
{
    public class TSubtractor
    {
        internal TMember Abs(TMember term1, TMember term2)
        {
            dynamic terms = SCalculator.ConstructTerms(term1, term2);
            TMember minuend = terms.greater;
            TMember subtrahend = terms.smaller;
            TMember difference = terms.opresult;

            uint c = 0;
            for (int i = 0; i < subtrahend.Length; i++)
            {
                difference.AppendDigit(CarryDigit(minuend[i], subtrahend[i], ref c));
            }
            for (int j = subtrahend.Length; j < minuend.Length; j++)
            {
                difference.AppendDigit(CarryDigit(minuend[j], new TDigit(0, subtrahend.Radix), ref c));
            }
            return difference.TrimEnd();
        }

        internal TMember Abs(TMember term1, TDigit term2)
        {
            SValidator<TMember>.Validate(m => m.Radix == term2.Radix && m >= term1, term1);
            TMember difference = new TMember(term1.Radix);

            uint c = 0;
            for (int i = 0; i < term1.Length; i++)
            {
                difference.AppendDigit(CarryDigit(term1[i], term2, ref c));
            }
            if (c > 0)
                difference.AppendDigit(CarryDigit(term1[-1], new TDigit(0, term1.Radix), ref c));

            return difference;
        }

        private TDigit CarryDigit(TDigit d1, TDigit d2, ref uint carry)
        {
            uint radix = d1.Radix.Value;
            uint digit = 0;

            //d1 - d2 - carry = d1 - (d2+carry)
            digit = d1.Value >= d2.Value + carry ? d1.Value - (d2.Value + carry) 
                : radix + d1.Value - (d2.Value + carry);
            carry = d1.Value >= d2.Value + carry ? (uint)0 : (uint)1;
            return new TDigit(digit, d1.Radix);
        }

    }
}
