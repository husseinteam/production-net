﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Member.Core.Types;

namespace Member.Core.Arithmetics
{
    internal class TModulator
    {
        #region handler
        internal TMember Modulate(TMember member, uint mod)
        {
            if (member.Length == 1 && member[0].Value < mod)
                return TMember.FromInt(0, member.Radix);
            TMember mnext = null;
            TMember modulus = TMember.FromInt(0, member.Radix);
            TDigit modulo = new TDigit(mod, member.Radix);

            for (int i = 0; i < member.Length; i++)
            {
                mnext = TMember.FromInt(
                    member[i].Value % modulo.Value
                    , member.Radix);
                for (int j = 0; j < i; j++)
                {
                    mnext *= new TDigit(member.Radix.Value % modulo.Value, member.Radix);
                    if (mnext == TMember.Zero(member.Radix))
                        break;
                }
                if (mnext > modulo)
                    mnext = mnext % modulo;
                modulus += mnext;
            }
            if (modulus > modulo)
                modulus %= modulo;
            
            return modulus;
        }
        #endregion
    }
}
