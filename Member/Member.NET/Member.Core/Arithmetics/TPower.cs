﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Member.Core.Types;

namespace Member.Core.Arithmetics
{
    internal class TPower
    {
        #region handlers
        internal TMember Pow(TMember m, uint n)
        {
            for (int i = 0; i < n; i++)
            {
                m *= new TDigit(n, m.Radix);
            }
            return m;
        }
        #endregion
    }
}
