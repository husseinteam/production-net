﻿namespace hangman_csharp {
    partial class AddQuestion {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnSoruEkle = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.dgvSorular = new System.Windows.Forms.DataGridView();
            this.txtSoru = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sorucumlesiDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.soruEntityBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSorular)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.soruEntityBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnSoruEkle);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.dgvSorular);
            this.groupBox1.Controls.Add(this.txtSoru);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(516, 253);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Soru Ekle";
            // 
            // btnSoruEkle
            // 
            this.btnSoruEkle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSoruEkle.Location = new System.Drawing.Point(81, 54);
            this.btnSoruEkle.Name = "btnSoruEkle";
            this.btnSoruEkle.Size = new System.Drawing.Size(429, 23);
            this.btnSoruEkle.TabIndex = 4;
            this.btnSoruEkle.Text = "Soru Ekle";
            this.btnSoruEkle.UseVisualStyleBackColor = true;
            this.btnSoruEkle.Click += new System.EventHandler(this.btnSoruEkle_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Soru Tablosu";
            // 
            // dgvSorular
            // 
            this.dgvSorular.AutoGenerateColumns = false;
            this.dgvSorular.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSorular.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.sorucumlesiDataGridViewTextBoxColumn});
            this.dgvSorular.DataSource = this.soruEntityBindingSource;
            this.dgvSorular.Location = new System.Drawing.Point(10, 95);
            this.dgvSorular.Name = "dgvSorular";
            this.dgvSorular.Size = new System.Drawing.Size(500, 152);
            this.dgvSorular.TabIndex = 2;
            // 
            // txtSoru
            // 
            this.txtSoru.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSoru.Location = new System.Drawing.Point(81, 27);
            this.txtSoru.Name = "txtSoru";
            this.txtSoru.Size = new System.Drawing.Size(429, 20);
            this.txtSoru.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Soru Cümlesi";
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "id";
            this.idDataGridViewTextBoxColumn.HeaderText = "ID";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // sorucumlesiDataGridViewTextBoxColumn
            // 
            this.sorucumlesiDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.sorucumlesiDataGridViewTextBoxColumn.DataPropertyName = "sorucumlesi";
            this.sorucumlesiDataGridViewTextBoxColumn.HeaderText = "Soru";
            this.sorucumlesiDataGridViewTextBoxColumn.Name = "sorucumlesiDataGridViewTextBoxColumn";
            this.sorucumlesiDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // soruEntityBindingSource
            // 
            this.soruEntityBindingSource.DataSource = typeof(hangman_csharp.EntityModel.Entities.SoruEntity);
            // 
            // AddQuestion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(541, 278);
            this.Controls.Add(this.groupBox1);
            this.Name = "AddQuestion";
            this.Text = "AddQuestion";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSorular)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.soruEntityBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtSoru;
        private System.Windows.Forms.DataGridView dgvSorular;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn sorucumlesiDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource soruEntityBindingSource;
        private System.Windows.Forms.Button btnSoruEkle;
    }
}