﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace hangman_csharp.Extensions {
    public static class UIExtensions {

        public static DataGridView GenerateCommand(this DataGridView dgv, String text,
            Action<Object, DataGridViewCellEventArgs> contentClicked) {

            var link = new DataGridViewLinkColumn();
            link.UseColumnTextForLinkValue = true;
            link.HeaderText = text;
            link.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            link.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            link.LinkBehavior = LinkBehavior.SystemDefault;
            link.Text = text;
            var colIndex = dgv.Columns.Count;
            dgv.CellContentClick += (s, e) => {
                if (e.ColumnIndex == colIndex) {
                    contentClicked(s, e);
                }
            };
            dgv.Columns.Add(link);
            return dgv;

        }

    }
}
