﻿namespace hangman_csharp {
    partial class AddWords {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.txtKelime = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnSoruEkle = new System.Windows.Forms.Button();
            this.cbxSorular = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dgvKelimeler = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kelimeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kelimeEntityBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnEkle = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvKelimeler)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kelimeEntityBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Kelime";
            // 
            // txtKelime
            // 
            this.txtKelime.Location = new System.Drawing.Point(58, 45);
            this.txtKelime.Name = "txtKelime";
            this.txtKelime.Size = new System.Drawing.Size(166, 20);
            this.txtKelime.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnSoruEkle);
            this.groupBox1.Controls.Add(this.cbxSorular);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.dgvKelimeler);
            this.groupBox1.Controls.Add(this.btnEkle);
            this.groupBox1.Controls.Add(this.txtKelime);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(486, 285);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Kelime Ekleme";
            // 
            // btnSoruEkle
            // 
            this.btnSoruEkle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSoruEkle.Location = new System.Drawing.Point(403, 89);
            this.btnSoruEkle.Name = "btnSoruEkle";
            this.btnSoruEkle.Size = new System.Drawing.Size(75, 190);
            this.btnSoruEkle.TabIndex = 6;
            this.btnSoruEkle.Text = "SoruEkle";
            this.btnSoruEkle.UseVisualStyleBackColor = true;
            this.btnSoruEkle.Click += new System.EventHandler(this.btnSoruEkle_Click);
            // 
            // cbxSorular
            // 
            this.cbxSorular.DisplayMember = "sorucumlesi";
            this.cbxSorular.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxSorular.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbxSorular.FormattingEnabled = true;
            this.cbxSorular.Location = new System.Drawing.Point(58, 19);
            this.cbxSorular.Name = "cbxSorular";
            this.cbxSorular.Size = new System.Drawing.Size(422, 21);
            this.cbxSorular.TabIndex = 5;
            this.cbxSorular.ValueMember = "id";
            this.cbxSorular.SelectedIndexChanged += new System.EventHandler(this.cbxSorular_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 89);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Kelime Tablosu";
            // 
            // dgvKelimeler
            // 
            this.dgvKelimeler.AutoGenerateColumns = false;
            this.dgvKelimeler.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvKelimeler.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.kelimeDataGridViewTextBoxColumn});
            this.dgvKelimeler.DataSource = this.kelimeEntityBindingSource;
            this.dgvKelimeler.Location = new System.Drawing.Point(20, 106);
            this.dgvKelimeler.Name = "dgvKelimeler";
            this.dgvKelimeler.Size = new System.Drawing.Size(379, 170);
            this.dgvKelimeler.TabIndex = 3;
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "id";
            this.idDataGridViewTextBoxColumn.HeaderText = "id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            this.idDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // kelimeDataGridViewTextBoxColumn
            // 
            this.kelimeDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.kelimeDataGridViewTextBoxColumn.DataPropertyName = "kelime";
            this.kelimeDataGridViewTextBoxColumn.HeaderText = "kelime";
            this.kelimeDataGridViewTextBoxColumn.Name = "kelimeDataGridViewTextBoxColumn";
            this.kelimeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // kelimeEntityBindingSource
            // 
            this.kelimeEntityBindingSource.DataSource = typeof(hangman_csharp.EntityModel.Entities.KelimeEntity);
            // 
            // btnEkle
            // 
            this.btnEkle.Location = new System.Drawing.Point(230, 43);
            this.btnEkle.Name = "btnEkle";
            this.btnEkle.Size = new System.Drawing.Size(250, 23);
            this.btnEkle.TabIndex = 2;
            this.btnEkle.Text = "Soruya Kelime Ekle";
            this.btnEkle.UseVisualStyleBackColor = true;
            this.btnEkle.Click += new System.EventHandler(this.btnEkle_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Soru";
            // 
            // AddWords
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(511, 310);
            this.Controls.Add(this.groupBox1);
            this.Name = "AddWords";
            this.Text = "AddWords";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvKelimeler)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kelimeEntityBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtKelime;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dgvKelimeler;
        private System.Windows.Forms.Button btnEkle;
        private System.Windows.Forms.ComboBox cbxSorular;
        private System.Windows.Forms.Button btnSoruEkle;
        private System.Windows.Forms.BindingSource kelimeEntityBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn kelimeDataGridViewTextBoxColumn;
    }
}