﻿using hangman_csharp.EntityModel;
using hangman_csharp.EntityModel.Entities;
using hangman_csharp.EntityModel.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using hangman_csharp.Extensions;

namespace hangman_csharp {
    public partial class AddQuestion : Form {
        public AddQuestion() {
            InitializeComponent();
            ModifyGrid();
            FillSorular();
        }

        private void btnSoruEkle_Click(object sender, EventArgs e) {
            using (var context = new HangmanContext()) {
                ISoruRepository repoSoru = new SoruRepository(context);
                repoSoru.AddSoru(new SoruEntity() { sorucumlesi = txtSoru.Text });
            }
            FillSorular();
        }

        private void FillSorular() {
            using (var context = new HangmanContext()) {
                ISoruRepository repoSoru = new SoruRepository(context);
                dgvSorular.DataSource = repoSoru.GetSorular();
            }

        }

        private void ModifyGrid() {
            dgvSorular.GenerateCommand("Sil", (s, e) => {
                var soru = dgvSorular.Rows[e.RowIndex].DataBoundItem as SoruEntity;
                if (soru != null) {
                    using (var context = new HangmanContext()) {
                        ISoruRepository repoSoru = new SoruRepository(context);
                        repoSoru.SilSoru(soru);
                        FillSorular();
                    }
                }
            });
        }

    }
}
