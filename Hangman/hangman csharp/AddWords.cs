﻿using hangman_csharp.EntityModel;
using hangman_csharp.EntityModel.Entities;
using hangman_csharp.EntityModel.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using hangman_csharp.Extensions;

namespace hangman_csharp {
    public partial class AddWords : Form {
        public AddWords() {
            InitializeComponent();
            FillCombo();
            ModifyGrid();
        }

        private void FillCombo() {
            using (var context = new HangmanContext()) {
                ISoruRepository repoSoru = new SoruRepository(context);
                cbxSorular.DataSource = repoSoru.GetSorular();
            }
        }

        private void ModifyGrid() {
            dgvKelimeler.GenerateCommand("Sil", (s, e) => {
                var kelime = dgvKelimeler.Rows[e.RowIndex].DataBoundItem as KelimeEntity;
                if (kelime != null) {
                    using (var context = new HangmanContext()) {
                        IKelimeRepository repoKelime = new KelimeRepository(context);
                        repoKelime.SilKelime(kelime);
                        FillDgv();
                    }
                }
            });
        }

        private void FillDgv() {

            var soruid = Convert.ToInt32(cbxSorular.SelectedValue);
            using (var context = new HangmanContext()) {
                IKelimeRepository repoKelime = new KelimeRepository(context);
                if (soruid != 0) {
                    dgvKelimeler.DataSource = repoKelime.GetKelimeler(soruid);
                } else {
                    dgvKelimeler.DataSource = null;
                }
            }

        }

        private void btnEkle_Click(object sender, EventArgs e) {
            var soruid = Convert.ToInt32(cbxSorular.SelectedValue);
            if (soruid != 0) {
                var kelime = txtKelime.Text;
                using (var context = new HangmanContext()) {
                    ISoruRepository repoSoru = new SoruRepository(context);
                    IKelimeRepository repoKelime = new KelimeRepository(context);
                    repoKelime.KelimeEkle(new KelimeEntity() { kelime = kelime, soru = repoSoru.GetSoru(soruid) });
                    dgvKelimeler.DataSource = repoKelime.GetKelimeler(soruid);
                }
            } else {
                MessageBox.Show("Lütfen Soru Seçiniz");
            }
        }

        private void cbxSorular_SelectedIndexChanged(object sender, EventArgs e) {
            var cbx = sender as ComboBox;
            var soruid = Convert.ToInt32(cbx.SelectedValue);
            if (soruid != 0) {
                using (var context = new HangmanContext()) {
                    ISoruRepository repoSoru = new SoruRepository(context);
                    IKelimeRepository repoKelime = new KelimeRepository(context);
                    dgvKelimeler.DataSource = repoKelime.GetKelimeler(soruid);
                }
            }
        }

        private void btnSoruEkle_Click(object sender, EventArgs e) {

            new AddQuestion().ShowDialog();
            FillCombo();
            FillDgv();
        }
    }
}
