﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using hangman_csharp.EntityModel.Entities;

namespace hangman_csharp.EntityModel.Repositories {
    public class SoruRepository : ISoruRepository {

        private readonly HangmanContext _context;

        public SoruRepository(HangmanContext context)
        {
            _context = context;
        }
 

        #region ISoruRepository Members

        public IEnumerable<Entities.KelimeEntity> GetKelimeler(int soruId) {
            return _context.Kelimeler.Where(k => k.soru.id == soruId).ToArray();
        }

        public string GetSoruCumlesi(int soruId) {
            return _context.Sorular.Find(soruId).sorucumlesi;
        }

        public Entities.SoruEntity GetSoru(int soruid) {
            return _context.Sorular.Find(soruid);
        }

        public IEnumerable<Entities.SoruEntity> GetSorular() {
            return _context.Sorular.ToArray();
        }

        public void AddSoru(Entities.SoruEntity soruEntity) {
            _context.Sorular.Add(soruEntity);
            SaveChanges();
        }

        public void SilSoru(SoruEntity soruEntity) {
            _context.Entry(soruEntity).State = EntityState.Deleted;
            var kelimeler = _context.Kelimeler.Include("soru").Where(k => k.soru.id == soruEntity.id);
            foreach (var kelime in kelimeler) {
                _context.Entry(kelime).State = EntityState.Deleted;
            }
            SaveChanges();
        }

        #endregion

        #region Context-Wide
        public void SaveChanges() {
            _context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing) {
            if (!this.disposed) {
                if (disposing) {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose() {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

    }
}
