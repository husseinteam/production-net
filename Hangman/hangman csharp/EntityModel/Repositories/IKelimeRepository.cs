﻿using hangman_csharp.EntityModel.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace hangman_csharp.EntityModel.Repositories {

    public interface IKelimeRepository {

        String GetSoruCumlesi(int kelimeId);

        String GetKelime(int kelimeId);

        void KelimeEkle(KelimeEntity kelimeEntity);

        void SilKelime(KelimeEntity kelimeEntity);

        IEnumerable<KelimeEntity> GetKelimeler(int soruid);
    }

}
