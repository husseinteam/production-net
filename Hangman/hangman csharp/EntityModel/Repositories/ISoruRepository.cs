﻿using hangman_csharp.EntityModel.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace hangman_csharp.EntityModel.Repositories {

    public interface  ISoruRepository {

        IEnumerable<KelimeEntity> GetKelimeler(int soruId);

        String GetSoruCumlesi(int soruId);

        SoruEntity GetSoru(int soruid);

        IEnumerable<SoruEntity> GetSorular();

        void AddSoru(SoruEntity soruEntity);

        void SilSoru(SoruEntity soruEntity);
    }

}
