﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using hangman_csharp.EntityModel.Entities;

namespace hangman_csharp.EntityModel.Repositories {
    public class KelimeRepository : IKelimeRepository {

        private readonly HangmanContext _context;

        public KelimeRepository(HangmanContext context) {
            _context = context;
        }

        #region IKelimeRepository Members

        public string GetSoruCumlesi(int kelimeId) {

            return _context.Kelimeler.Find(kelimeId).soru.sorucumlesi;

        }

        public string GetKelime(int kelimeId) {

            return _context.Kelimeler.Find(kelimeId).kelime;

        }

        public void KelimeEkle(Entities.KelimeEntity kelimeEntity) {
            _context.Kelimeler.Add(kelimeEntity);
            SaveChanges();
        }

        public void SilKelime(KelimeEntity kelimeEntity) {
            _context.Entry(kelimeEntity).State = System.Data.Entity.EntityState.Deleted;
            SaveChanges();
        }
        public IEnumerable<Entities.KelimeEntity> GetKelimeler(int soruid) {
            return _context.Kelimeler.Include("soru").Where(k => k.soru.id == soruid).ToArray();
        }


        #endregion

        #region Context-Wide
        public void SaveChanges() {
            _context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing) {
            if (!this.disposed) {
                if (disposing) {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose() {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

    }
}
