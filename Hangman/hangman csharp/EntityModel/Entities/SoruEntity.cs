﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace hangman_csharp.EntityModel.Entities {
    [Table("sorular")]
    public class SoruEntity {

        [Key]
        public int id { get; set; }
        public String sorucumlesi { get; set; }

        public virtual ICollection<KelimeEntity> Kelimeler { get; set; }
    }
}
