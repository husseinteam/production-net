﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace hangman_csharp.EntityModel.Entities {
    [Table("kelimeler")]
    public class KelimeEntity {

        [Key]
        public int id { get; set; }
        public String kelime{ get; set; }

        public virtual SoruEntity soru { get; set; }
    }
}
