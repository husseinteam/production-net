﻿using hangman_csharp.EntityModel.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;

namespace hangman_csharp.EntityModel {
    public class HangmanContext : DbContext {
        public HangmanContext() {

            Database.SetInitializer<HangmanContext>(null);

        }

        public DbSet<SoruEntity> Sorular { get; set; }
        public DbSet<KelimeEntity> Kelimeler { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder) {

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

        }

    } 
}
