﻿namespace hangman_csharp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Button15 = new System.Windows.Forms.Button();
            this.Button16 = new System.Windows.Forms.Button();
            this.Button19 = new System.Windows.Forms.Button();
            this.Button20 = new System.Windows.Forms.Button();
            this.Button21 = new System.Windows.Forms.Button();
            this.Button22 = new System.Windows.Forms.Button();
            this.Button23 = new System.Windows.Forms.Button();
            this.Button25 = new System.Windows.Forms.Button();
            this.Button26 = new System.Windows.Forms.Button();
            this.Button27 = new System.Windows.Forms.Button();
            this.Button9 = new System.Windows.Forms.Button();
            this.Button10 = new System.Windows.Forms.Button();
            this.Button11 = new System.Windows.Forms.Button();
            this.Button12 = new System.Windows.Forms.Button();
            this.Button13 = new System.Windows.Forms.Button();
            this.Button14 = new System.Windows.Forms.Button();
            this.Button8 = new System.Windows.Forms.Button();
            this.Button7 = new System.Windows.Forms.Button();
            this.Button6 = new System.Windows.Forms.Button();
            this.Button5 = new System.Windows.Forms.Button();
            this.Button4 = new System.Windows.Forms.Button();
            this.Button3 = new System.Windows.Forms.Button();
            this.Button2 = new System.Windows.Forms.Button();
            this.Button1 = new System.Windows.Forms.Button();
            this.btnYonetici = new System.Windows.Forms.Button();
            this.cbxSorular = new System.Windows.Forms.ComboBox();
            this.button28 = new System.Windows.Forms.Button();
            this.button29 = new System.Windows.Forms.Button();
            this.button30 = new System.Windows.Forms.Button();
            this.button31 = new System.Windows.Forms.Button();
            this.button24 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Button15
            // 
            this.Button15.Location = new System.Drawing.Point(445, 342);
            this.Button15.Name = "Button15";
            this.Button15.Size = new System.Drawing.Size(27, 23);
            this.Button15.TabIndex = 80;
            this.Button15.Text = "Z";
            this.Button15.UseVisualStyleBackColor = true;
            // 
            // Button16
            // 
            this.Button16.Location = new System.Drawing.Point(412, 342);
            this.Button16.Name = "Button16";
            this.Button16.Size = new System.Drawing.Size(27, 23);
            this.Button16.TabIndex = 79;
            this.Button16.Text = "Y";
            this.Button16.UseVisualStyleBackColor = true;
            // 
            // Button19
            // 
            this.Button19.Location = new System.Drawing.Point(379, 342);
            this.Button19.Name = "Button19";
            this.Button19.Size = new System.Drawing.Size(27, 23);
            this.Button19.TabIndex = 76;
            this.Button19.Text = "V";
            this.Button19.UseVisualStyleBackColor = true;
            // 
            // Button20
            // 
            this.Button20.Location = new System.Drawing.Point(313, 342);
            this.Button20.Name = "Button20";
            this.Button20.Size = new System.Drawing.Size(27, 23);
            this.Button20.TabIndex = 75;
            this.Button20.Text = "U";
            this.Button20.UseVisualStyleBackColor = true;
            // 
            // Button21
            // 
            this.Button21.Location = new System.Drawing.Point(278, 342);
            this.Button21.Name = "Button21";
            this.Button21.Size = new System.Drawing.Size(27, 23);
            this.Button21.TabIndex = 74;
            this.Button21.Text = "T";
            this.Button21.UseVisualStyleBackColor = true;
            // 
            // Button22
            // 
            this.Button22.Location = new System.Drawing.Point(245, 342);
            this.Button22.Name = "Button22";
            this.Button22.Size = new System.Drawing.Size(27, 23);
            this.Button22.TabIndex = 73;
            this.Button22.Text = "Ş";
            this.Button22.UseVisualStyleBackColor = true;
            // 
            // Button23
            // 
            this.Button23.Location = new System.Drawing.Point(179, 342);
            this.Button23.Name = "Button23";
            this.Button23.Size = new System.Drawing.Size(27, 23);
            this.Button23.TabIndex = 72;
            this.Button23.Text = "R";
            this.Button23.UseVisualStyleBackColor = true;
            // 
            // Button25
            // 
            this.Button25.Location = new System.Drawing.Point(146, 342);
            this.Button25.Name = "Button25";
            this.Button25.Size = new System.Drawing.Size(27, 23);
            this.Button25.TabIndex = 70;
            this.Button25.Text = "P";
            this.Button25.UseVisualStyleBackColor = true;
            // 
            // Button26
            // 
            this.Button26.Location = new System.Drawing.Point(80, 342);
            this.Button26.Name = "Button26";
            this.Button26.Size = new System.Drawing.Size(27, 23);
            this.Button26.TabIndex = 69;
            this.Button26.Text = "O";
            this.Button26.UseVisualStyleBackColor = true;
            // 
            // Button27
            // 
            this.Button27.Location = new System.Drawing.Point(47, 342);
            this.Button27.Name = "Button27";
            this.Button27.Size = new System.Drawing.Size(27, 23);
            this.Button27.TabIndex = 68;
            this.Button27.Text = "N";
            this.Button27.UseVisualStyleBackColor = true;
            // 
            // Button9
            // 
            this.Button9.Location = new System.Drawing.Point(14, 342);
            this.Button9.Name = "Button9";
            this.Button9.Size = new System.Drawing.Size(27, 23);
            this.Button9.TabIndex = 67;
            this.Button9.Text = "M";
            this.Button9.UseVisualStyleBackColor = true;
            // 
            // Button10
            // 
            this.Button10.Location = new System.Drawing.Point(478, 313);
            this.Button10.Name = "Button10";
            this.Button10.Size = new System.Drawing.Size(27, 23);
            this.Button10.TabIndex = 66;
            this.Button10.Text = "L";
            this.Button10.UseVisualStyleBackColor = true;
            // 
            // Button11
            // 
            this.Button11.Location = new System.Drawing.Point(445, 313);
            this.Button11.Name = "Button11";
            this.Button11.Size = new System.Drawing.Size(27, 23);
            this.Button11.TabIndex = 65;
            this.Button11.Text = "K";
            this.Button11.UseVisualStyleBackColor = true;
            // 
            // Button12
            // 
            this.Button12.Location = new System.Drawing.Point(412, 313);
            this.Button12.Name = "Button12";
            this.Button12.Size = new System.Drawing.Size(27, 23);
            this.Button12.TabIndex = 64;
            this.Button12.Text = "J";
            this.Button12.UseVisualStyleBackColor = true;
            // 
            // Button13
            // 
            this.Button13.Location = new System.Drawing.Point(346, 313);
            this.Button13.Name = "Button13";
            this.Button13.Size = new System.Drawing.Size(27, 23);
            this.Button13.TabIndex = 63;
            this.Button13.Text = "I";
            this.Button13.UseVisualStyleBackColor = true;
            // 
            // Button14
            // 
            this.Button14.Location = new System.Drawing.Point(313, 313);
            this.Button14.Name = "Button14";
            this.Button14.Size = new System.Drawing.Size(27, 23);
            this.Button14.TabIndex = 62;
            this.Button14.Text = "H";
            this.Button14.UseVisualStyleBackColor = true;
            // 
            // Button8
            // 
            this.Button8.Location = new System.Drawing.Point(245, 313);
            this.Button8.Name = "Button8";
            this.Button8.Size = new System.Drawing.Size(27, 23);
            this.Button8.TabIndex = 61;
            this.Button8.Text = "G";
            this.Button8.UseVisualStyleBackColor = true;
            // 
            // Button7
            // 
            this.Button7.Location = new System.Drawing.Point(212, 313);
            this.Button7.Name = "Button7";
            this.Button7.Size = new System.Drawing.Size(27, 23);
            this.Button7.TabIndex = 60;
            this.Button7.Text = "F";
            this.Button7.UseVisualStyleBackColor = true;
            // 
            // Button6
            // 
            this.Button6.Location = new System.Drawing.Point(179, 313);
            this.Button6.Name = "Button6";
            this.Button6.Size = new System.Drawing.Size(27, 23);
            this.Button6.TabIndex = 59;
            this.Button6.Text = "E";
            this.Button6.UseVisualStyleBackColor = true;
            // 
            // Button5
            // 
            this.Button5.Location = new System.Drawing.Point(146, 313);
            this.Button5.Name = "Button5";
            this.Button5.Size = new System.Drawing.Size(27, 23);
            this.Button5.TabIndex = 58;
            this.Button5.Text = "D";
            this.Button5.UseVisualStyleBackColor = true;
            // 
            // Button4
            // 
            this.Button4.Location = new System.Drawing.Point(113, 313);
            this.Button4.Name = "Button4";
            this.Button4.Size = new System.Drawing.Size(27, 23);
            this.Button4.TabIndex = 57;
            this.Button4.Text = "Ç";
            this.Button4.UseVisualStyleBackColor = true;
            // 
            // Button3
            // 
            this.Button3.Location = new System.Drawing.Point(47, 313);
            this.Button3.Name = "Button3";
            this.Button3.Size = new System.Drawing.Size(27, 23);
            this.Button3.TabIndex = 56;
            this.Button3.Text = "B";
            this.Button3.UseVisualStyleBackColor = true;
            // 
            // Button2
            // 
            this.Button2.Location = new System.Drawing.Point(14, 313);
            this.Button2.Name = "Button2";
            this.Button2.Size = new System.Drawing.Size(27, 23);
            this.Button2.TabIndex = 55;
            this.Button2.Text = "A";
            this.Button2.UseVisualStyleBackColor = true;
            // 
            // Button1
            // 
            this.Button1.Location = new System.Drawing.Point(410, 228);
            this.Button1.Name = "Button1";
            this.Button1.Size = new System.Drawing.Size(124, 23);
            this.Button1.TabIndex = 54;
            this.Button1.Text = "Yeniden Başla";
            this.Button1.UseVisualStyleBackColor = true;
            this.Button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // btnYonetici
            // 
            this.btnYonetici.Location = new System.Drawing.Point(454, 13);
            this.btnYonetici.Name = "btnYonetici";
            this.btnYonetici.Size = new System.Drawing.Size(75, 23);
            this.btnYonetici.TabIndex = 81;
            this.btnYonetici.Text = "Yönetici";
            this.btnYonetici.UseVisualStyleBackColor = true;
            this.btnYonetici.Click += new System.EventHandler(this.btnYonetici_Click);
            // 
            // cbxSorular
            // 
            this.cbxSorular.DisplayMember = "sorucumlesi";
            this.cbxSorular.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxSorular.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbxSorular.FormattingEnabled = true;
            this.cbxSorular.Location = new System.Drawing.Point(14, 13);
            this.cbxSorular.Name = "cbxSorular";
            this.cbxSorular.Size = new System.Drawing.Size(425, 21);
            this.cbxSorular.TabIndex = 82;
            this.cbxSorular.ValueMember = "id";
            this.cbxSorular.SelectedIndexChanged += new System.EventHandler(this.cbxSorular_SelectedIndexChanged);
            // 
            // button28
            // 
            this.button28.Location = new System.Drawing.Point(80, 313);
            this.button28.Name = "button28";
            this.button28.Size = new System.Drawing.Size(27, 23);
            this.button28.TabIndex = 57;
            this.button28.Text = "C";
            this.button28.UseVisualStyleBackColor = true;
            // 
            // button29
            // 
            this.button29.Location = new System.Drawing.Point(280, 313);
            this.button29.Name = "button29";
            this.button29.Size = new System.Drawing.Size(27, 23);
            this.button29.TabIndex = 61;
            this.button29.Text = "Ğ";
            this.button29.UseVisualStyleBackColor = true;
            // 
            // button30
            // 
            this.button30.Location = new System.Drawing.Point(379, 313);
            this.button30.Name = "button30";
            this.button30.Size = new System.Drawing.Size(27, 23);
            this.button30.TabIndex = 63;
            this.button30.Text = "İ";
            this.button30.UseVisualStyleBackColor = true;
            // 
            // button31
            // 
            this.button31.Location = new System.Drawing.Point(113, 342);
            this.button31.Name = "button31";
            this.button31.Size = new System.Drawing.Size(27, 23);
            this.button31.TabIndex = 69;
            this.button31.Text = "Ö";
            this.button31.UseVisualStyleBackColor = true;
            // 
            // button24
            // 
            this.button24.Location = new System.Drawing.Point(212, 342);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(27, 23);
            this.button24.TabIndex = 73;
            this.button24.Text = "S";
            this.button24.UseVisualStyleBackColor = true;
            // 
            // button18
            // 
            this.button18.Location = new System.Drawing.Point(346, 342);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(27, 23);
            this.button18.TabIndex = 76;
            this.button18.Text = "Ü";
            this.button18.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(546, 381);
            this.Controls.Add(this.cbxSorular);
            this.Controls.Add(this.btnYonetici);
            this.Controls.Add(this.Button15);
            this.Controls.Add(this.Button16);
            this.Controls.Add(this.button18);
            this.Controls.Add(this.Button19);
            this.Controls.Add(this.Button20);
            this.Controls.Add(this.Button21);
            this.Controls.Add(this.button24);
            this.Controls.Add(this.Button22);
            this.Controls.Add(this.Button23);
            this.Controls.Add(this.Button25);
            this.Controls.Add(this.button31);
            this.Controls.Add(this.Button26);
            this.Controls.Add(this.Button27);
            this.Controls.Add(this.Button9);
            this.Controls.Add(this.Button10);
            this.Controls.Add(this.Button11);
            this.Controls.Add(this.Button12);
            this.Controls.Add(this.button30);
            this.Controls.Add(this.Button13);
            this.Controls.Add(this.Button14);
            this.Controls.Add(this.button29);
            this.Controls.Add(this.Button8);
            this.Controls.Add(this.Button7);
            this.Controls.Add(this.Button6);
            this.Controls.Add(this.Button5);
            this.Controls.Add(this.button28);
            this.Controls.Add(this.Button4);
            this.Controls.Add(this.Button3);
            this.Controls.Add(this.Button2);
            this.Controls.Add(this.Button1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "HangMan";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Button Button15;
        internal System.Windows.Forms.Button Button16;
        internal System.Windows.Forms.Button Button19;
        internal System.Windows.Forms.Button Button20;
        internal System.Windows.Forms.Button Button21;
        internal System.Windows.Forms.Button Button22;
        internal System.Windows.Forms.Button Button23;
        internal System.Windows.Forms.Button Button25;
        internal System.Windows.Forms.Button Button26;
        internal System.Windows.Forms.Button Button27;
        internal System.Windows.Forms.Button Button9;
        internal System.Windows.Forms.Button Button10;
        internal System.Windows.Forms.Button Button11;
        internal System.Windows.Forms.Button Button12;
        internal System.Windows.Forms.Button Button13;
        internal System.Windows.Forms.Button Button14;
        internal System.Windows.Forms.Button Button8;
        internal System.Windows.Forms.Button Button7;
        internal System.Windows.Forms.Button Button6;
        internal System.Windows.Forms.Button Button5;
        internal System.Windows.Forms.Button Button4;
        internal System.Windows.Forms.Button Button3;
        internal System.Windows.Forms.Button Button2;
        internal System.Windows.Forms.Button Button1;
        private System.Windows.Forms.Button btnYonetici;
        private System.Windows.Forms.ComboBox cbxSorular;
        internal System.Windows.Forms.Button button28;
        internal System.Windows.Forms.Button button29;
        internal System.Windows.Forms.Button button30;
        internal System.Windows.Forms.Button button31;
        internal System.Windows.Forms.Button button24;
        internal System.Windows.Forms.Button button18;
    }
}

