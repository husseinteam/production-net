﻿using AV.Core.Converters;
using AV.Core.Crawler;
using AV.Core.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Extensions;

namespace AC.Console {

    class Program {

        static void Main(string[] args) {

            var li = new List<long>();
            var outputDir = new DirectoryInfo(@"..\..\..\Assets\Media-Items\Converted").DeleteAllChildren();

            var mp3Converter = new FFMpegConverter(outputDir, MediaTypes.MP3);
            mp3Converter.ConversionCompleted += (pr) => System.Console.WriteLine(pr.Success ? "Success" : pr.ErrorOutput);
            mp3Converter.ReportsConversion += (output, seconds) => li.Add(seconds);

            var crawler = new DirectoryCrawler(new DirectoryInfo(@"..\..\..\Assets\Media-Items"), MediaTypes.WAV);
            crawler.FileSet += (fi, depth) => mp3Converter.Convert(fi);

            Parallel.Invoke(crawler.EachFile().FileSetStack);
            System.Console.WriteLine("Done");
            System.Console.ReadKey();

        }

    }

}
