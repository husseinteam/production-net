﻿using AV.Core.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AV.Core.Converters {

    public class FFMpegConverter : AudioConverterBase {

        public FFMpegConverter(DirectoryInfo outputDirectory, MediaTypes conversionTarget)
            : base(outputDirectory, conversionTarget) {

        }

        protected override String StandartArguments {
            get { return @"-i ""{0}"" ""{1}"""; }
        }

        protected override FileInfo EnginePath {
            get { return new FileInfo(Path.Combine(Environment.CurrentDirectory, "ffmpeg.exe")); }
        }


        protected override Dictionary<KeyValuePair<MediaTypes, MediaTypes>, String> MediaArguments {
            get {
                return new Dictionary<KeyValuePair<MediaTypes, MediaTypes>, String>() {
                    { 
                        new KeyValuePair<MediaTypes, MediaTypes>(MediaTypes.FLV, MediaTypes.MPG)
                        , @" -i ""{0}"" -ab 56 -ar 22050 -b 500 -s 320×240 ""{1}""" 
                    },
                    { 
                        new KeyValuePair<MediaTypes, MediaTypes>(MediaTypes.GP3, MediaTypes.AVI)
                        , @"-i ""{0}"" -f avi -acodec mp3 ""{1}""" 
                    },
                    { 
                        new KeyValuePair<MediaTypes, MediaTypes>(MediaTypes.GP3, MediaTypes.MP3)
                        , @"-y -i ""{0}"" -ac 1 -acodec mp3 -ar 22050 -f wav ""{1}"""
                    },
                    { 
                        new KeyValuePair<MediaTypes, MediaTypes>(MediaTypes.WMA, MediaTypes.OGG)
                        , @"-i ""{0}"" -acodec vorbis -aq 100 ""{1}""" 
                    },
                    { 
                        new KeyValuePair<MediaTypes, MediaTypes>(MediaTypes.WMA, MediaTypes.MP3)
                        , @"-i ""{0}"" -acodec mp3 -aq 100 ""{1}""" 
                    },

                };
            }
        }
    }
    /*
     * convert .flv to .mpg
ffmpeg -i yourfile.flv -ab 56 -ar 22050 -b 500 -s 320×240 yourfile.mpg

convert .3gp to .avi
ffmpeg -i file.3gp -f avi -acodec mp3 *.avi

extract audio .3gp to .mp3
ffmpeg -y -i *.3gp -ac 1 -acodec mp3 -ar 22050 -f wav *.mp3

Convert wma to ogg
ffmpeg -i *.wma -acodec vorbis -aq 100 *.ogg

Convert wma to mp3
ffmpeg -i *wma -acodec mp3 -aq 100 *.mp3

If you have a lot of files to convert run this command in the directory where your wma files reside
convert wma to ogg
for i in *.wma; do ffmpeg -i $i -acodec vorbis -aq 100 ${i%.wma}.ogg; done

convert wma to mp3
for i in *.wma; do ffmpeg -i $i -acodec mp3 -aq 100 ${i%.wma}.mp3; done
     * */
}
