﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Extensions;
using System.IO;
using System.Diagnostics;
using AV.Core.Enums;

namespace AV.Core.Converters {

    public class LAMEConverter : AudioConverterBase {

        public LAMEConverter(DirectoryInfo outputDirectory, MediaTypes conversionTarget)
            : base(outputDirectory, conversionTarget) {

        }

        protected override String StandartArguments {
            get { return @"-b 32 --resample 22.05 -m m ""{0}"" ""{1}"""; }
        }

        protected override FileInfo EnginePath {
            get { return new FileInfo(Path.Combine(Environment.CurrentDirectory, "lame.exe")); }
        }

        protected override Dictionary<KeyValuePair<MediaTypes, MediaTypes>, string> MediaArguments {
            get {
                return new Dictionary<KeyValuePair<MediaTypes, MediaTypes>, String>();
            }
        }
    }

}
