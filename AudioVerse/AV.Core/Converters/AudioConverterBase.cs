﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Extensions;
using System.Diagnostics;
using AV.Core.Enums;
using System.Threading;

namespace AV.Core.Converters {

    public abstract class AudioConverterBase {

        #region Fields

        private DirectoryInfo _OutputDirectory;
        private MediaTypes _ConversionTarget;
        #endregion

        #region Ctors

        public AudioConverterBase(DirectoryInfo outputDirectory, MediaTypes conversionTarget) {

            this._ConversionTarget = conversionTarget;
            this._OutputDirectory = outputDirectory;

        }

        #endregion

        #region Event Handling

        public delegate void ConversionCompletedDelegate(ProcessResult pr);
        public event ConversionCompletedDelegate ConversionCompleted;
        private void OnConversionCompleted(ProcessResult pr) {
            if (ConversionCompleted != null) {
                ConversionCompleted(pr);
            }
        }

        public delegate void ReportsConversionDelegate(FileInfo output, Int64 seconds);
        public event ReportsConversionDelegate ReportsConversion;
        private void OnReportsConversion(FileInfo output, Int64 seconds) {
            if (ReportsConversion != null) {
                ReportsConversion(output, seconds);
            }
        }
        #endregion

        #region Obliged

        protected abstract String StandartArguments { get; }

        protected abstract FileInfo EnginePath { get; }

        protected abstract Dictionary<KeyValuePair<MediaTypes, MediaTypes>, String> MediaArguments { get; }

        #endregion

        #region Engine

        public void Convert(FileInfo infile) {

            //maxLen is in ms (1000 = 1 second) 
            var outfile = new FileInfo(
                Path.Combine(this._OutputDirectory.FullName,
                    infile.Name.Replace(infile.Extension, this._ConversionTarget.GetDescription())));
            if (outfile.Exists) {
                outfile.Delete();
            }
            var fromType = infile.Extension.ToLower().ToEnum<MediaTypes>();
            var arguments = "";
            var key = new KeyValuePair<MediaTypes, MediaTypes>(fromType, this._ConversionTarget);
            if (MediaArguments.ContainsKey(key)) {
                arguments = MediaArguments[key].PostFormat(infile.FullName, outfile.FullName);
            } else {
                arguments = StandartArguments.PostFormat(infile.FullName, outfile.FullName);
            }

            var psi = new ProcessStartInfo();
            psi.FileName = EnginePath.FullName;
            psi.Arguments = arguments;
            psi.WorkingDirectory = EnginePath.Directory.FullName;
            psi.RedirectStandardError = true;
            psi.RedirectStandardOutput = true;
            psi.UseShellExecute = false;
            psi.WindowStyle = ProcessWindowStyle.Hidden;

            DateTime start = DateTime.MinValue;
            var source = new CancellationTokenSource();
            Task.Run(() => {
                while (!source.IsCancellationRequested) {
                    OnReportsConversion(outfile, DateTime.Now.Subtract(start).Seconds);
                    Task.Delay(100);
                }
            }, source.Token);

            var p = Process.Start(psi);
            p.WaitForExit();
            source.Cancel();
            var pr = new ProcessResult(p, outfile);
            OnConversionCompleted(pr);
        }

        #endregion

    }

}
