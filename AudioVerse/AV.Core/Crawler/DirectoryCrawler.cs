﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Extensions;
using AV.Core.Enums;

namespace AV.Core.Crawler {

    public class DirectoryCrawler {

        #region Fields

        private DirectoryInfo _RootDirectory;
        private MediaTypes[] _MediaTypes;

        #endregion

        #region Ctors

        public DirectoryCrawler(DirectoryInfo rootDirectory, params MediaTypes[] mediaTypes) {

            this._MediaTypes = mediaTypes;
            this._RootDirectory = rootDirectory;
            if (!this._RootDirectory.Exists) {
                throw new ArgumentException("{0} Doesn't Exist".PostFormat(rootDirectory));
            }

        }

        #endregion

        #region Public

        public Action[] DirectorySetStack {
            get {
                return _DirectorySetStack.ToArray();
            }
        }
        public Action[] FileSetStack {
            get {
                return _FileSetStack.ToArray();
            }
        }

        public DirectoryCrawler EachFile() {

            this.Recurse(this._RootDirectory, 0);
            return this;

        }

        #endregion

        #region Event Handling

        public delegate void DirectorySetDelegate(DirectoryInfo di, Int32 depth);
        public event DirectorySetDelegate DirectorySet;
        private List<Action> _DirectorySetStack = new List<Action>();
        private void OnDirectorySet(DirectoryInfo di, Int32 depth) {
            if (DirectorySet != null) {
                _DirectorySetStack.Add(new Action(() => DirectorySet(di, depth)));
            }
        }

        public delegate void FileSetDelegate(FileInfo fi, Int32 depth);
        public event FileSetDelegate FileSet;
        private List<Action> _FileSetStack = new List<Action>();
        private void OnFileSet(FileInfo fi, Int32 depth) {
            if (FileSet != null) {
                _FileSetStack.Add(new Action(() => FileSet(fi, depth)));
            }
        }

        #endregion

        #region Private

        private void Recurse(DirectoryInfo parent, Int32 depth) {

            foreach (var pattern in this._MediaTypes) {
                foreach (var fi in parent.EnumerateFiles("*" + pattern.GetDescription())) {
                    OnFileSet(fi, depth);
                }
            }
            foreach (var di in parent.EnumerateDirectories()) {
                OnDirectorySet(di, depth);
                Recurse(di, depth + 1);
            }

        }

        #endregion

    }

}
