﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.IO;
using AV.Core.Enums;
using AV.Core.Converters;
using AV.Core.Crawler;
using System.Threading.Tasks;
using LLF.Extensions;

namespace AV.UnitTests.TestClasses {

    [TestClass]
    public class CrawlerTests {

        [TestMethod]
        public void CrawlRecursiveRunsOK() {

            var li = new List<long>();
            var outputDir = new DirectoryInfo(@"F:\__\Media-Items\Converted").DeleteAllChildren();

            var mp3Converter = new FFMpegConverter(outputDir, MediaTypes.MP3);
            mp3Converter.ConversionCompleted += (pr) => Assert.IsTrue(pr.Success, pr.ErrorOutput);
            mp3Converter.ReportsConversion += (output, seconds) => li.Add(seconds);

            var crawler = new DirectoryCrawler(new DirectoryInfo(@"H:\Production\AudioVerse\Assets"), MediaTypes.WAV);
            crawler.FileSet += (fi, depth) => mp3Converter.Convert(fi);

            Parallel.Invoke(crawler.EachFile().FileSetStack);
            Assert.AreNotEqual(0, li.Count);

        }

    }

}
