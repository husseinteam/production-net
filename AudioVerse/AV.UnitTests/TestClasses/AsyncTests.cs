﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AV.Core.Converters;
using System.IO;
using AV.Core.Enums;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Collections.Generic;
using AV.Core.Crawler;

namespace AV.UnitTests.TestClasses {

    [TestClass]
    public class AsyncTests {

        private FileInfo testCase = new FileInfo(Path.Combine(Environment.CurrentDirectory, "testcase.wav"));

        [TestMethod]
        public void ConvertWithReportOK() {

            var li = new List<long>();
            var mp3Converter = new FFMpegConverter(new DirectoryInfo(@"H:\Production\AudioVerse\Assets"), MediaTypes.MP3);
            mp3Converter.ConversionCompleted += (pr) => Assert.IsTrue(pr.Success, pr.ErrorOutput);
            mp3Converter.ReportsConversion += (output, seconds) => li.Add(seconds);
            mp3Converter.Convert(testCase);
            Assert.AreNotEqual(0, li.Count);

        }

    }

}
