﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AV.Core.Converters;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using AV.Core.Crawler;
using AV.Core.Enums;
using LLF.Extensions;

namespace AV.UnitTests.TestClasses {

    [TestClass]
    public class CoreTests {

        private FileInfo testCase = new FileInfo(Path.Combine(Environment.CurrentDirectory, "testcase.wav"));

        [TestMethod]
        public void LAMEConversionRunsOK() {

            var mp3Converter = new LAMEConverter(new DirectoryInfo(@"H:\Production\AudioVerse\Assets"), MediaTypes.MP3);
            mp3Converter.ConversionCompleted += (pr) => Assert.IsTrue(pr.Success);
            mp3Converter.Convert(testCase);
        }

        [TestMethod]
        public void FFMpegConversionRunsOK() {

            var mp3Converter = new FFMpegConverter(new DirectoryInfo(@"H:\Production\AudioVerse\Assets")
                , MediaTypes.MP3);
            mp3Converter.ConversionCompleted += (pr) => Assert.IsTrue(pr.Success);
            mp3Converter.Convert(testCase);
        }


    }

}
