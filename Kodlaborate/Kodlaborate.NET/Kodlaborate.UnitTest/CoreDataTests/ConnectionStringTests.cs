﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Kodlaborate.CoreData.StaticObjects;
using FluentAssertions;
using LLF.Extensions;
using LLF.Data.Tools;
using LLF.Data.Containers;
using LLF.Abstract.Data.Engine;
using Kodlaborate.Resources.Locals;
using MySql.Data.MySqlClient;

namespace Kodlaborate.UnitTest.CoreDataTests {

    [TestClass]
    public class ConnectionStringTests {

        [ClassInitialize]
        public static void ClassInit(TestContext ctx) {

            ResourceContainer.Register<IConnectionCredential>(Xmls.ConnectionCredentials);

        }

        [TestMethod]
        public void TestSetCurrent() {

            var cred = SCredentialProvider.Current();
            cred.Should().NotBeNull("because cred is intended");
            var c = cred.ProvideSuperAdminConnection();
            c.Should().NotBeNull("because mysql connection is intended");
            Action connectifier = () => c.Assign(conn => conn.Open()).Assign(conn => conn.Close());
            connectifier.ShouldNotThrow<Exception>("Because Connection To Local MySql should succeed");

        }

    }

}
