﻿using System;
using System.Collections.Generic;
using System.Linq;
using Kodlaborate.CoreData.EntityObjects;
using LLF.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ninject;
using Kodlaborate.CoreData.StaticObjects;
using LLF.Data.Containers;
using LLF.Abstract.Data.Engine;
using Kodlaborate.Resources.Locals;
using LLF.Data.Tools;
using Kodlaborate.Web.Models;
using System.Diagnostics;

namespace Kodlaborate.UnitTest.CoreDataTests {
    [TestClass]
    public class SeedTests {


        [ClassInitialize]
        public static void ClassInit(TestContext ctx) {

            Bootstrapper.RegisterDependencies();

        }

        [TestMethod]
        public void SeedCompleteRunsOK() {

            var expLi = new List<Exception>();
            expLi.Add(SEnterprise.ReinitializeModels());
            expLi.AddRange(SEnterprise.SeedDatabase());
            expLi.Where(e => e != null).ToList().ForEach(e => Debug.Write(e.Message + "\r\n"));
            Assert.AreEqual(0, expLi.Where(e => e != null).Count());

        }

    }
}
