﻿using System;
using System.Dynamic;
using System.Collections;

using LLF.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using Kodlaborate.CoreData.StaticObjects;
using Kodlaborate.ViewModels.Engine;
using Kodlaborate.CoreData.EntityObjects;

namespace Kodlaborate.UnitTest.TranslatorTests {
    [TestClass]
    public class TokenTransformTests {

        [ClassInitialize]
        public static void ClassInit(TestContext ctx)
        {

            Bootstrapper.RegisterDependencies();

        }

        [TestMethod]
        public void GetAllTokensRunsOK() {

            var list = SCodeEngine.GetAllTokens();
            Assert.AreNotEqual(0, list.Count());
            list.Enumerate(li => Assert.IsNotNull(li));

        }

        [TestMethod]
        public void GetAuthorRunsOK() {

            var author = SAuthEngine.GetAuthorByUserName("lampiclobe@outlook.com");
            Assert.IsNotNull(author);
            Assert.AreNotEqual(0, author.CodeLinks.Count);

            Assert.IsTrue(author.CodeLinks.ToList().Any(cl =>
                cl.Include<CodeBlock>().CodeBlocks.Count > 0));

            author.Include<ToDoTask>();
            Assert.AreNotEqual(0, author.ToDoTasks.Count);

        }

    }
}
