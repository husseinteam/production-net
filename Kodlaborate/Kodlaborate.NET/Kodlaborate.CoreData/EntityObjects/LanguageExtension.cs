﻿using LLF.Annotations.Data;
using LLF.Data.Engine;
using LLF.Extensions.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kodlaborate.CoreData.EntityObjects {

    [EntityTitle("LanguageExtension", Schema = "KL")]
    public class LanguageExtension : BaseEntity<LanguageExtension> {

        #region Properties

        [DataColumn(EDbType.NVarChar, MaxLength = 32)]
        public String Extension { get; set; }

        #endregion

        #region Navigation Properties

        [ReferenceKey("CodeLanguageID")]
        public CodeLanguage CodeLanguage { get; set; }

        #endregion

    }
}
