﻿using Kodlaborate.CoreData.Enums;
using LLF.Annotations.Data;
using LLF.Data.Engine;
using LLF.Extensions.Data.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kodlaborate.CoreData.EntityObjects {

    [EntityTitle("CodeLink", Schema = "KL")]
    public class CodeLink : BaseEntity<CodeLink> {

        public CodeLink() {
            this.CodeBlocks = new List<CodeBlock>();
        }

        #region Properties

        [DataColumn(EDbType.NVarChar, MaxLength = 128)]
        public String Token { get; set; }

        [DataColumn(EDbType.Int32)]
        public Int32 ClickCount { get; set; }

        [Enumeration]
        public ELinkStatus LinkStatus { get; set; }

        #endregion

        #region Navigation Properties

        [ReferenceCollection]
        public IList<CodeBlock> CodeBlocks { get; internal set; }

        [ReferenceKey("OwnerID"), Nullable]
        public Author Owner { get; set; }

        #endregion


    }
}
