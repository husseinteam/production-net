﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kodlaborate.CoreData.Enums;
using LLF.Data.Engine;
using LLF.Annotations.Data;
using LLF.Extensions.Data.Enums;

namespace Kodlaborate.CoreData.EntityObjects {

    [EntityTitle("Author", Schema = "KL")]
    public class Author : BaseEntity<Author> {

        #region Ctor
        public Author() {

            this.CodeLinks = new List<CodeLink>();
            this.ToDoTasks = new List<ToDoTask>();

        }
        #endregion

        #region Properties

        [DataColumn(EDbType.NVarChar, MaxLength = 128)]
        public String Identity { get; set; }

        [DataColumn(EDbType.NVarChar, MaxLength = 128), Unique]
        public String Email { get; set; }

        [DataColumn(EDbType.NVarChar, MaxLength = 512)]
        public String PasswordHash { get; set; }

        [DataColumn(EDbType.Boolean)]
        public Boolean IsAdministrator { get; set; }

        [DataColumn(EDbType.NVarChar, MaxLength = 128), Nullable]
        public String PasswordRecoveryToken { get; set; }

        [Enumeration]
        public EMembershipStatus MembershipStatus { get; set; }

        #endregion

        #region Navigation Properties

        [ReferenceCollection]
        public IList<CodeLink> CodeLinks { get; set; }

        [ReferenceCollection]
        public IList<ToDoTask> ToDoTasks { get; set; }

        #endregion

    }
}
