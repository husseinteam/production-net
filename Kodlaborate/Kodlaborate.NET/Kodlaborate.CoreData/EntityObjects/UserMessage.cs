﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kodlaborate.CoreData.Enums;
using LLF.Data.Engine;
using LLF.Annotations.Data;
using LLF.Extensions.Data.Enums;

namespace Kodlaborate.CoreData.EntityObjects {

    [EntityTitle("UserMessage", Schema = "KL")]
    public class UserMessage : BaseEntity<UserMessage> {

        #region Ctor
        public UserMessage() {

        }
        #endregion

        #region Properties

        [DataColumn(EDbType.NVarChar, MaxLength = 128)]
        public String Subject { get; set; }

        [DataColumn(EDbType.Text)]
        public String Content { get; set; }

        [DataColumn(EDbType.DateTime)]
        public DateTime SentOn { get; set; }

        [DataColumn(EDbType.NVarChar, MaxLength = 128)]
        public String ContactEmail { get; set; }

        [Enumeration]
        public EReplyStatus ReplyStatus { get; set; }

        #endregion

        #region Computed Properties

        public EColorClass ColorClass {
            get {
                switch (ReplyStatus) {
                    case EReplyStatus.Pending:
                        return EColorClass.Warning;
                    case EReplyStatus.Issued:
                        return EColorClass.Information;
                    case EReplyStatus.Rejected:
                        return EColorClass.Danger;
                    case EReplyStatus.Replied:
                        return EColorClass.Success;
                    default:
                        return EColorClass.Information;
                }
            }
        }

        #endregion

    }
}
