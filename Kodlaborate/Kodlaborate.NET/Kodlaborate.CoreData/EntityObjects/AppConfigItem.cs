﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kodlaborate.CoreData.Enums;
using LLF.Data.Engine;
using LLF.Annotations.Data;
using LLF.Extensions.Data.Enums;

namespace Kodlaborate.CoreData.EntityObjects {

    [EntityTitle("AppConfigItem", Schema = "KL")]
    public class AppConfigItem : BaseEntity<AppConfigItem> {

        #region Properties

        [Enumeration, Unique]
        public EAppConfig AppConfigType { get; set; }

        [DataColumn(EDbType.NVarChar, MaxLength = 512), Nullable]
        public String Value { get; set; }

        #endregion

    }

}
