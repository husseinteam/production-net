﻿using LLF.Annotations.Data;
using LLF.Data.Engine;
using LLF.Extensions.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kodlaborate.CoreData.EntityObjects {

    [EntityTitle("CodeLanguage", Schema = "KL")]
    public class CodeLanguage : BaseEntity<CodeLanguage> {

        public CodeLanguage() {

            this.LanguageExtensions = new List<LanguageExtension>();

        }

        #region Properties

        [DataColumn(EDbType.NVarChar, MaxLength = 128)]
        public String Name { get; set; }

        [DataColumn(EDbType.NVarChar, MaxLength = 128)]
        public String MimeType { get; set; }

        [DataColumn(EDbType.NVarChar, MaxLength = 128)]
        public String Mode { get; set; }

        #endregion

        #region Navigation Properties

        [ReferenceCollection]
        public IList<LanguageExtension> LanguageExtensions { get; set; }

        #endregion

    }
}
