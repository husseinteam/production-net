﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kodlaborate.CoreData.Enums;
using LLF.Data.Engine;
using LLF.Annotations.Data;
using LLF.Extensions.Data.Enums;

namespace Kodlaborate.CoreData.EntityObjects {

    [EntityTitle("ToDoTask", Schema = "KL")]
    public class ToDoTask : BaseEntity<ToDoTask> {

        #region Ctor
        public ToDoTask() {

        }
        #endregion

        #region Properties

        [DataColumn(EDbType.NVarChar, MaxLength = 128)]
        public String Title { get; set; }

        [DataColumn(EDbType.NVarChar, MaxLength = 128)]
        public Double CompletionRatio { get; set; }
        
        [Enumeration]
        public EColorClass TaskClass { get; set; }

        #endregion

        #region Navigation Properties

        [ReferenceKey("OwnerID")]
        public Author Owner { get; set; }

        #endregion

    }

}
