﻿using LLF.Annotations.Data;
using LLF.Data.Engine;
using LLF.Extensions.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kodlaborate.CoreData.EntityObjects {

    [EntityTitle("CodeBlock", Schema = "KL")]
    public class CodeBlock : BaseEntity<CodeBlock> {

        #region Properties

        [DataColumn(EDbType.NVarChar, MaxLength = 128)]
        public String Title { get; set; }

        [DataColumn(EDbType.Text)]
        public String Text { get; set; }

        #endregion

        #region Navigation Properties

        [ReferenceKey("CodeLanguageID")]
        public CodeLanguage CodeLanguage { get; set; }

        [ReferenceKey("CodeLinkID")]
        public CodeLink CodeLink { get; set; }

        #endregion

    }
}
