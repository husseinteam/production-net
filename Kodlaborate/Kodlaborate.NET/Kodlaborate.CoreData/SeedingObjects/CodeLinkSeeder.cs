﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kodlaborate.CoreData.EntityObjects;
using Kodlaborate.CoreData.StaticObjects;
using LLF.Extensions;
using Kodlaborate.CoreData.Enums;
using LLF.Abstract.Data.Engine;
using LLF.Data.Tools;

namespace Kodlaborate.CoreData.SeedingObjects {

    public class CodeLinkSeeder {

        public static ISeedResponse<CodeLink> GetResponse(Author author) {

            var response = SeedProvider.SeedInstanceOf<CodeLink>(18,
                        (i) => new CodeLink() {
                            Token = "{0}".GenerateToken(),
                            ClickCount = 0,
                            Owner = author,
                            LinkStatus = ELinkStatus.Online
                        });
            return response;

        }

        public static ISeedResponse<CodeLink> GetResponse() {

            var response = SeedProvider.SeedInstanceOf<CodeLink>(18,
                        (i) => new CodeLink() {
                            Token = "{0}".GenerateToken(),
                            ClickCount = 0,
                            Owner = null,
                            LinkStatus = ELinkStatus.Online
                        });
            return response;

        }

    }

}
