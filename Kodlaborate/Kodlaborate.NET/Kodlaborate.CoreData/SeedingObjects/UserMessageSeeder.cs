﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kodlaborate.CoreData.EntityObjects;
using Kodlaborate.CoreData.StaticObjects;
using Kodlaborate.CoreData.Enums;
using LLF.Extensions;
using LLF.Data.Tools;
using LLF.Abstract.Data.Engine;

namespace Kodlaborate.CoreData.SeedingObjects {
    public class UserMessageSeeder {

        #region ISeedResponse<UserMessage> Members

        public static ISeedResponse<UserMessage> GetResponse() {

			var emails = new String[] { 
				"andrew@outlook.com", 
				"jonney@gmail.com", 
				"osman@outlook.com",
				"hasan@gmail.com",
				"hakki.sagdic@outlook.com" 
			};

            var subjects = new String[] {
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend",
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                "Consectetur adipiscing elit. Pellentesque eleifend",
                "Lorem ipsum dolor sit amet, pellentesque eleifend",
				"Lorem ipsum dolor sit amet."
			};

            var contents = Enumerable.Repeat("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse eu purus posuere, venenatis sem sit amet, fringilla quam. Nullam quis rutrum quam. Duis quis porta velit. Aenean tempus tellus vitae eros blandit egestas ac ut massa. Aliquam finibus varius diam eget sagittis. Proin et commodo arcu, ac lacinia felis. Nunc ex nisi, imperdiet a pellentesque at, tempor vel massa. Cras ornare molestie diam. In lacinia, mauris sed ultricies ornare, lorem neque volutpat ligula, vel malesuada risus nibh a enim. Proin vulputate sit amet est vitae commodo. In interdum gravida pharetra. Vivamus congue risus nec fermentum rutrum.", 5).ToArray();

            var dates = new DateTime[] {
                2.MonthsAgo(),
                1.MinutesAgo(),
                3.DaysAgo(),
                30.MinutesAgo(),
                1.HoursAgo()
            };

            var statuses = new EReplyStatus[] {
                EReplyStatus.Pending,
                EReplyStatus.Issued,
                EReplyStatus.Rejected,
                EReplyStatus.Pending,
                EReplyStatus.Replied
            };

            var response = SeedProvider.SeedInstanceOf<UserMessage>(emails.Count(),
                      (i) => new UserMessage() {
                          ContactEmail = emails.ElementAt(i),
                          Content = contents.ElementAt(i),
                          SentOn = dates.ElementAt(i),
                          Subject = subjects.ElementAt(i),
                          ReplyStatus = statuses.ElementAt(i)
                      });
            return response;

		}

		#endregion

	}
}
