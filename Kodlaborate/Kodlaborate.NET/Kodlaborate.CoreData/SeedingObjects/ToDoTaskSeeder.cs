﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kodlaborate.CoreData.EntityObjects;
using Kodlaborate.CoreData.StaticObjects;
using Kodlaborate.CoreData.Enums;
using LLF.Extensions;
using LLF.Data.Tools;
using LLF.Abstract.Data.Engine;

namespace Kodlaborate.CoreData.SeedingObjects {

    public class ToDoTaskSeeder {

        #region IEntitySeeder<UserMessage> Members

        public static ISeedResponse<ToDoTask> GetResponse(Author author) {

            var titles = Enumerable.Repeat("Task ", 5).EnumerateFrom((i, t) => t + i.ToString()).ToArray();

            var completionRatios = new Double[] {
                0.4, 0.2, 0.6, 0.8, 0.7
			};

            var types = new EColorClass[] {
                EColorClass.Success,
                EColorClass.Information,
                EColorClass.Danger,
                EColorClass.Success,
                EColorClass.Information
            };

            var response = SeedProvider.SeedInstanceOf<ToDoTask>(titles.Count(),
                       (i) => new ToDoTask() {
                           Title = titles.ElementAt(i),
                           TaskClass = types.ElementAt(i),
                           CompletionRatio = completionRatios.ElementAt(i),
                           Owner = author
                       });
            return response;

        }

		#endregion

	}
}
