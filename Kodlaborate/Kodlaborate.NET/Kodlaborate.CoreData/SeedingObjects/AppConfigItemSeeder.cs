﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kodlaborate.CoreData.EntityObjects;
using Kodlaborate.CoreData.StaticObjects;
using Kodlaborate.CoreData.Enums;
using LLF.Extensions;
using LLF.Data.Tools;
using LLF.Abstract.Data.Engine;

namespace Kodlaborate.CoreData.SeedingObjects {

    public class AppConfigItemSeeder {

        #region IEntitySeeder<AppConfigItem> Members

        public static ISeedResponse<AppConfigItem> GetResponse() {

            var types = new EAppConfig[] {
                EAppConfig.DailyUserVisitCount,
                EAppConfig.MonthlyUserRegistrationCount,
                EAppConfig.DailyLinkAdded
            };

            var valTypes = new EAppConfig[] { 
                EAppConfig.DailyUserVisitLastValue,
                EAppConfig.MonthlyUserRegistrationLastValue,
                EAppConfig.DailyLinkAddedLastValue
            };
            var response = SeedProvider.SeedInstanceOf<AppConfigItem>(types.Count() + valTypes.Count(),
                        (i) => new AppConfigItem() {
                            AppConfigType = i < 3 ? types.ElementAt(i) : valTypes.ElementAt(i - 3),
                            Value = i < 3 ? "0" : i.DaysAgo().ToString()
                        });
            return response;

        }

        #endregion

    }
}
