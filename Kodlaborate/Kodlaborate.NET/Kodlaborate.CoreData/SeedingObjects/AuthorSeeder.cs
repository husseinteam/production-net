﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kodlaborate.CoreData.EntityObjects;
using Kodlaborate.CoreData.StaticObjects;
using Kodlaborate.CoreData.Enums;
using LLF.Extensions;
using LLF.Data.Tools;
using LLF.Abstract.Data.Engine;

namespace Kodlaborate.CoreData.SeedingObjects {
    public class AuthorSeeder {

		#region IEntitySeeder<Author> Members

        public static ISeedResponse<Author> GetResponse() {

            var names = new String[] { 
				"lampiclobe", 
				"ondermetu", 
				"dilek1982",
				"section",
				"hithere",
			};
            var identities = new String[] {
                "Özgür Eser Sönmez",
                "Önder Heper",
                "Dilek Neidek",
                "Naber Babalık",
				"Önder Değer",
			};

            var response = SeedProvider.SeedInstanceOf<Author>(names.Count(),
                      (i) => new Author() {
                          Email = i == 0 ? "lampiclobe@outlook.com" :
                            "{0}-{1}@lampiclobe.com".PostFormat(names.Cycle(i), "{0}".GenerateNumber()),
                          Identity = identities.Cycle(i),
                          PasswordHash = "a1q2w3e".ComputeHash(),
                          IsAdministrator = i == 0,
                          PasswordRecoveryToken = Guid.Empty.ToString(),
                          MembershipStatus = EMembershipStatus.Active
                      });
            return response;

        }

		#endregion

	}
}
