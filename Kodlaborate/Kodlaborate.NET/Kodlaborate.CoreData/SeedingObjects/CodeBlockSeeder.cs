﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kodlaborate.CoreData.EntityObjects;
using Kodlaborate.CoreData.StaticObjects;
using LLF.Extensions;
using Kodlaborate.CoreData.Enums;
using LLF.Abstract.Data.Engine;
using LLF.Data.Tools;
using Kodlaborate.CoreData.Properties;

namespace Kodlaborate.CoreData.SeedingObjects {

    public class CodeBlockSeeder {

        public static ISeedResponse<CodeBlock> GetResponse(CodeLanguage lang, IList<CodeLink> cbli) {

            var response = SeedProvider.SeedInstanceOf<CodeBlock>(9,
                        (i) => new CodeBlock() {
                            CodeLanguage = lang,
                            CodeLink = cbli.Cycle(i),
                            Text = i % 2 == 0 ? Properties.Resources.DirectoryCrawler : Properties.Resources.SRarReader,
                            Title = "title-{0}".GenerateNumber()    
                        });
            return response;

        }

    }

}
