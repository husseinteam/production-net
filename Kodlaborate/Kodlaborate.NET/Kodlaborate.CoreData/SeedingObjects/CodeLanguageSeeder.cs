﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kodlaborate.CoreData.EntityObjects;
using Kodlaborate.CoreData.StaticObjects;
using LLF.Abstract.Data.Engine;
using LLF.Data.Tools;

namespace Kodlaborate.CoreData.SeedingObjects {

    public class CodeLanguageSeeder {

        public static ISeedResponse<CodeLanguage> GetResponse() {

            var response = SeedProvider.SeedInstanceOf<CodeLanguage>(SCodeLanguages.MetaDict.Count(),
                      (i) => new CodeLanguage() {
                          Name = SCodeLanguages.MetaDict.ElementAt(i).name.ToString(),
                          Mode = SCodeLanguages.MetaDict.ElementAt(i).mode.ToString(),
                          MimeType = SCodeLanguages.MetaDict.ElementAt(i).mime.ToString(),
                          LanguageExtensions = (SCodeLanguages.MetaDict.ElementAt(i) as IDictionary<String, Object>).ContainsKey("ext") ?
                              ((String[])SCodeLanguages.MetaDict.ElementAt(i).ext).Select<String, LanguageExtension>(e => new LanguageExtension() { Extension = e }).ToArray() : new LanguageExtension[] { 
                            new LanguageExtension() { Extension = (String)SCodeLanguages.MetaDict.ElementAt(i).mime }
                        }
                      });
            return response;

        }
    }
}
