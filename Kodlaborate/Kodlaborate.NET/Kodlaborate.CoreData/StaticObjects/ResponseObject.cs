﻿using Kodlaborate.Resources.Locals;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Extensions;
using Newtonsoft.Json;

namespace Kodlaborate.CoreData.StaticObjects {

    public class ResponseObject {

        #region Fields

        private Object _Data;

        #endregion

        #region Methods

        public TData GetData<TData>() {

            return _Data != null ? _Data.ToType<TData>() :
                (typeof(TData).IsCollection() ? (TData)Activator.CreateInstance(typeof(List<>).MakeGenericType(typeof(TData).GetGenericArguments().First())) : default(TData));

        }

        #endregion

        #region Properties

        public Boolean ShowModal { get; private set; }
        public Boolean Success { get; set; }
        private String _Message;

        public String Message {
            get { return _Message; }
            set {
                _Message = value;
                ShowModal = !_Message.IsUnassigned();
            }
        }
        public String ModalTitle {
            get {
                return Success ? PageContent.ModalSuccess : PageContent.ModalError;
            }
        }
        public String ModalDismiss {
            get {
                return PageContent.ModalDismiss;
            }
        }
        public String Redirect { get; set; }
        public Object Data {
            get {
                return _Data;
            }
            set {
                _Data = value;
            }
        }
        public String AjaxTarget { get; set; }
        public String AjaxSource { get; set; }

        #endregion

        #region Serialization

        public String toJSON() {

            return JsonConvert.SerializeObject(this, Formatting.None);

        }

        #endregion

    }
}
