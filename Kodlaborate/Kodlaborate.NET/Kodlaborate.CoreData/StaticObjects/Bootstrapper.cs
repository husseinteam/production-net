﻿
using Kodlaborate.CoreData.EntityObjects;
using Kodlaborate.CoreData.SeedingObjects;
using Kodlaborate.CoreData.StaticObjects;
using Kodlaborate.Resources.Locals;
using LLF.Abstract.Data.Engine;
using LLF.Data.Containers;
using LLF.Data.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kodlaborate.CoreData.StaticObjects {

    public static class Bootstrapper {

        public static void RegisterDependencies() {

            if (!_Booted) {

                SCredentialProvider.Current();
                _Booted = true;
            }
        }

        private static Boolean _Booted = false;

    }
}