﻿using Kodlaborate.Resources.Globals;
using Kodlaborate.Resources.Locals;
using LLF.Abstract.Data.Engine;
using LLF.Data.Containers;
using LLF.Data.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kodlaborate.CoreData.StaticObjects {

    public static class SCredentialProvider {

        public static IConnectionCredential Current() {

            ResourceContainer.Register<IConnectionCredential>(Xmls.ConnectionCredentials);
            return CredentialProvider.CurrentCredential = ResourceContainer.ReadXmlFor<IConnectionCredential>(SDefinitions.CurrentCredentialKey);

        }

    }

}
