﻿using Kodlaborate.CoreData.EntityObjects;
using Kodlaborate.CoreData.SeedingObjects;
using LLF.Abstract.Data.Engine;
using LLF.Data.Containers;
using LLF.Data.Tools;
using LLF.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kodlaborate.CoreData.StaticObjects {
    public static class SEnterprise {

        public static Exception ReinitializeModels() {

            DbProvider.WipeDatabase();

            var expt = DependencyContainer.Take<IDatabaseEngine>()
                   .EngineFor<AppConfigItem>()
                   .EngineFor<Author>()
                   .EngineFor<ToDoTask>()
                   .EngineFor<UserMessage>()
                   .EngineFor<LanguageExtension>()
                   .EngineFor<CodeLanguage>()
                   .EngineFor<CodeBlock>()
                   .EngineFor<CodeLink>()
                   .GenerateModels();
            return expt;

        }

        public static IEnumerable<Exception> SeedDatabase() {
            IList<Exception> exptList = new List<Exception>();

            UserMessageSeeder.GetResponse().DestroySeed().PersistSeed(ref exptList);
            AppConfigItemSeeder.GetResponse().DestroySeed().PersistSeed(ref exptList);
            var langs = CodeLanguageSeeder.GetResponse().DestroySeed().PersistSeed(ref exptList).SeedList;
            var authors = AuthorSeeder.GetResponse().DestroySeed().PersistSeed(ref exptList).SeedList;

            var author = authors.Single(auth => auth.Email == "lampiclobe@outlook.com");
            var lang = langs.Single(lng => lng.Name == "C#");

            var todoTasks = ToDoTaskSeeder.GetResponse(author).DestroySeed().PersistSeed(ref exptList).SeedList;

            var links = CodeLinkSeeder.GetResponse(author).DestroySeed().PersistSeed(ref exptList).PersistCollectionSeed<CodeBlock>(ref exptList).SeedList;
            CodeLinkSeeder.GetResponse().PersistSeed(ref exptList).PersistCollectionSeed<CodeBlock>(ref exptList);

            var blocks = CodeBlockSeeder.GetResponse(lang, links).DestroySeed().PersistSeed(ref exptList).SeedList;

            return exptList.ToArray();

        }

    }
}
