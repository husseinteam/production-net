﻿using Kodlaborate.Resources.Locals;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kodlaborate.CoreData.Enums {

    public enum EColorClass {

        [Description("danger")]
        Danger = 1,

        [Description("info")]
        Information,

        [Description("warning")]
        Warning,

        [Description("success")]
        Success

    }

}
