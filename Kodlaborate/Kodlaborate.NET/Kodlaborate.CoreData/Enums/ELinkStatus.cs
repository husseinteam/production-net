﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kodlaborate.CoreData.Enums {

    public enum ELinkStatus {

        [Description("success")]
        Online = 1,
        [Description("warning")]
        Offline,
        [Description("danger")]
        Deleted

    }

}
