﻿using Kodlaborate.CoreData.EntityObjects;
using Kodlaborate.Resources.Locals;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kodlaborate.CoreData.Enums {
    public static class EnumExtensions {

        public static String GetStatusText(this EMembershipStatus status) {

            switch (status) {
                case EMembershipStatus.Active:
                    return AdminContent.MembershipStatusActive;
                case EMembershipStatus.Frozen:
                    return AdminContent.MembershipStatusFrozen;
                case EMembershipStatus.Deleted:
                    return AdminContent.MembershipStatusDeleted;
                case EMembershipStatus.PendingActivation:
                    return AdminContent.MembershipStatusPendingActivation;
                case EMembershipStatus.Any:
                default:
                    return "";
            }

        }
        public static String GetStatusText(this ELinkStatus status) {

            switch (status) {
                case ELinkStatus.Online:
                    return AdminContent.LinkStatusOnline;
                case ELinkStatus.Offline:
                    return AdminContent.LinkStatusOffline;
                case ELinkStatus.Deleted:
                    return AdminContent.LinkStatusDeleted;
                default:
                    return "";
            }

        }

        public static EAppConfig GetValueMatch(this EAppConfig self) {

            switch (self) {
                case EAppConfig.DailyUserVisitCount:
                    return EAppConfig.DailyUserVisitLastValue;
                case EAppConfig.MonthlyUserRegistrationCount:
                    return EAppConfig.MonthlyUserRegistrationLastValue;
                case EAppConfig.DailyLinkAdded:
                    return EAppConfig.DailyLinkAddedLastValue;
                default:
                    throw new ArgumentException("Is A Value");
            }

        }

        public static Boolean IsToBeIncrementedOnSession(this EAppConfig self) {

            switch (self) {
                case EAppConfig.DailyUserVisitCount:
                    return true;
                case EAppConfig.MonthlyUserRegistrationCount:
                    return false;
                case EAppConfig.DailyLinkAdded:
                    return false;
                default:
                    throw new ArgumentException("Is A Value");
            }

        }

        public static Func<DateTime, Boolean> GetValidatorOfValue(this EAppConfig self, DateTime now) {

            switch (self) {
                case EAppConfig.DailyUserVisitLastValue:
                    return new Func<DateTime, Boolean>((dt) => (now - dt).Hours > 24);
                case EAppConfig.MonthlyUserRegistrationLastValue:
                    return new Func<DateTime, Boolean>((dt) => (now - dt).Days > 30);
                case EAppConfig.DailyLinkAddedLastValue:
                    return new Func<DateTime, Boolean>((dt) => (now - dt).Hours > 24);
                default:
                    throw new ArgumentException("Is A Value");
            }

        }

    }
}
