﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kodlaborate.CoreData.Enums {

    public enum EReplyStatus {

        Pending = 1,
        Issued,
        Rejected,
        Replied
    
    }

}

