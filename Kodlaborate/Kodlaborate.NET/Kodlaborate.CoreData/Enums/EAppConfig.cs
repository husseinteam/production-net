﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kodlaborate.CoreData.Enums {

    public enum EAppConfig {

        DailyUserVisitCount = 1,
        DailyUserVisitLastValue,
        MonthlyUserRegistrationCount,
        MonthlyUserRegistrationLastValue,
        DailyLinkAdded,
        DailyLinkAddedLastValue

    }

}
