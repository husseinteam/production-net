﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kodlaborate.Resources.Globals {

    public static class SSegmentConfig {

        public const String Relay = "relay";

        public const String Dashboard = "dashboard";

        public const String UserGrid = "usergrid";

        public const String LinkGrid = "linkgrid";

        public const String ErrorIndex = "genericerror";

        public const String ErrorInternal = "internal";
        
        public const String ErrorNotFound = "notfound";

        public const String Login = "login";

        public const String Register = "register";

        public const String ModifyProfile = "modifyprofile";

        public const String NewLink = "newlink";
        
        public const String CodeGrid = "codegrid";
        
        public const String ModifyLink = "modifylink";
        
        public const String RemoveLink = "removelink";
        
        public const String DeleteBlock = "deleteblock";

        public const String ExtendLink = "extendlink";

        public const String Tokenize = "tokenize";

        public const String DisplayLink = "displaylink";

        public const String CodeBlock = "codeblock";

        public const String Logout = "logout";

		public const String Recover = "recover";

		public const String Repass = "repass";

		public const String Lang = "lang";

		public const String Terms = "terms";

		public const String Privacy = "privacy";

		public const String Unsubscribe = "unsubscribe";

        public const String GenerateLink = "generatelink";

    }

}
