﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Kodlaborate.Web.Models {

    public class AjaxCommand : ContextRenderer, IAjaxCommand {

        #region IAjaxCommand Members Properties

        private String _Action;

        public String Action {
            get { return _Action; }
        }

        private String _Controller;

        public String Controller {
            get { return _Controller; }
        }

        private Object _RouteData;

        public Object RouteData {
            get { return _RouteData; }
        }

        private String _OnSuccess;

        public String OnSuccess {
            get { return _OnSuccess; }
        }

        private String _CommandTitle;

        public String CommandTitle {
            get { return _CommandTitle; }
        }

        private Boolean _AllowCache;

        public Boolean AllowCache {
            get { return _AllowCache; }
        }

        #endregion

        #region IAjaxCommand Members Setters


        public IAjaxCommand SetAction(string action) {

            this._Action = action;
            return this;

        }

        public IAjaxCommand SetController(string controller) {

            this._Controller = controller;
            return this;

        }

        public IAjaxCommand SetRouteData(object routeData) {

            this._RouteData = routeData;
            return this;

        }

        public IAjaxCommand SetOnSuccess(string onSuccess) {

            this._OnSuccess = onSuccess;
            return this;

        }

        public IAjaxCommand SetCommandTitle(string commandTitle) {

            this._CommandTitle = commandTitle;
            return this;

        }

        public IAjaxCommand SetAllowCache(bool allowCache) {

            this._AllowCache = allowCache;
            return this;

        }

        #endregion


        protected override string ViewName {
            get { return "_AjaxCommand"; }
        }
    }

}