﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LLF.Extensions;

namespace Kodlaborate.Web.Models {

    public abstract class ContextRenderer : IContextRenderer {

        protected abstract String ViewName { get; }

        public IHtmlString Render(ViewContext context, String format = "{0}") {

            var ViewData = new ViewDataDictionary();
            ViewData.Model = this;
            var TempData = new TempDataDictionary();
            using (var sw = new StringWriter()) {
                var viewResult = ViewEngines.Engines.FindPartialView(context.Controller.ControllerContext, ViewName);
                var viewContext = new ViewContext(context.Controller.ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                return new MvcHtmlString(format.PostFormat(sw.GetStringBuilder()));
            }

        }

    }

}