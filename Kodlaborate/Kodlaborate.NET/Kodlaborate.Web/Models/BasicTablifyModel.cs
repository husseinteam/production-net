﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LLF.Extensions;
using System.Web.Mvc;
using System.Text;
using System.IO;
using System.Web.UI;

namespace Kodlaborate.Web.Models {

    public class BasicTablifyModel : ContextRenderer, IBasicTablifyModel {

        #region Fields

        private IEnumerable enumerable;
        private IBasicPaginationModel paginator;
        private IList<IBasicColumnModel> columns;
        private IList<IAjaxCommand> commands;
        private Int32 _ColumnIndexCounter = 0;
        private static Int32 _InstanceIndex = 0;

        #endregion

        #region Ctor

        public BasicTablifyModel(IEnumerable enumerable, String action, String controller, String ajaxTarget) {

            this._Action = action;
            this._Controller = controller;
            this._AjaxTarget = ajaxTarget;
            this.enumerable = enumerable;
            this.columns = new List<IBasicColumnModel>();
            this.commands = new List<IAjaxCommand>();
            BasicTablifyModel._InstanceIndex++;
        }

        #endregion

        #region IBasicTablifyModel Members

        public IBasicTablifyModel SetPaginated(Int32 pageSize) {

            var page = HttpContext.Current.Request.RequestContext.RouteData.Values["argument"].ToType<Int32>();
            this.paginator = new BasicPaginationModel(enumerable, page, pageSize, this._Action);
            return this;

        }

        public IBasicTablifyModel AppendColumn(Action<IBasicColumnModel> generator) {

            IBasicColumnModel column = new BasicColumnModel(++this._ColumnIndexCounter, this);
            generator(column);
            this.columns.Add(column);
            return this;

        }

        public IBasicTablifyModel AppendCommand(Action<IAjaxCommand> generator) {

            IAjaxCommand command = new AjaxCommand();
            generator(command);
            this.commands.Add(command);
            return this;

        }

        public IBasicPaginationModel Paginator {
            get { return this.paginator; }
        }

        public IEnumerable<IBasicColumnModel> Columns {

            get {
                return this.columns.ToArray();
            }

        }

        public IEnumerable<IBasicRowModel> Rows {

            get {
                return this.paginator.PaginatedModel(this._ModelType).OfType<Object>().EnumerateFrom((i, m) => new BasicRowModel(i, m, this.columns.ToArray(), this));
            }

        }

        public IEnumerable<IAjaxCommand> Commands {
            get {
                return this.commands;
            }
        }

        private String _AjaxTarget;
        public String AjaxTarget {
            get { return _AjaxTarget; }
        }

        private String _Controller;
        public String Controller {
            get { return _Controller; }
        }

        private String _Action;
        public String Action {
            get { return _Action; }
        }

        private Type _ModelType;

        public IBasicTablifyModel SetEmptyString(String emptyString) {

            this._EmptyString = emptyString;
            return this;

        }

        public IBasicTablifyModel SetTableTitle(String tableString) {

            this._TableTitle = tableString;
            return this;

        }

        public IBasicTablifyModel SetActionsTitle(String actionsTitle) {

            this._ActionsTitle = actionsTitle;
            return this;

        }

        public IBasicTablifyModel SetRowModelType(Type modelType) {

            this._ModelType = modelType;
            return this;

        }

        public Int32 InstanceIndex {
            get {
                return BasicTablifyModel._InstanceIndex;
            }
        }

        public Int32 Page {
            get { return HttpContext.Current.Request.RequestContext.RouteData.Values["argument"].ToType<Int32>(); }
        }

        private String _EmptyString;

        public String EmptyString {
            get { return _EmptyString; }
        }

        private String _TableTitle;

        public String TableTitle {
            get { return _TableTitle; }
            set { _TableTitle = value; }
        }

        private String _ActionsTitle;

        public String ActionsTitle {
            get { return _ActionsTitle; }
        }

        public Boolean DisplaysCommandList {
            get {
                return this.commands.Count > 0;
            }
        }

        public Boolean DisplaysPagination {
            get {
                return this.paginator != null && 
                    this.paginator.PaginatedModel(this._ModelType).OfType<Object>().Count() > 0;
            }
        }

        #endregion


        protected override string ViewName {
            get { return "_BasicTablify"; }
        }
    }

}