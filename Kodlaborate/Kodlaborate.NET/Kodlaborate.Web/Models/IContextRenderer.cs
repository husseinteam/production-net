﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Kodlaborate.Web.Models {

    public interface IContextRenderer {

        IHtmlString Render(ViewContext context, String format = "{0}");

    }

}
