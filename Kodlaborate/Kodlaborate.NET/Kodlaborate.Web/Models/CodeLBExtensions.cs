﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Kodlaborate.Resources.Globals;
using Kodlaborate.Web.Controllers;
using LLF.Extensions;
using Kodlaborate.ViewModels.Engine;

namespace Kodlaborate.Web.Models {
    public static class CodeLBExtensions {

        public static Boolean UserIsLoggedin(this ViewContext self) {

            return (self.Controller as BaseController).SessionFor<Int32>("auth") != default(Int32);

        }

        public static Boolean UserIsAdministrator(this ViewContext self) {

            var authorID = (self.Controller as BaseController).SessionFor<Int32>("auth");
            if (authorID == default(Int32)) {
                return false;
            }
            return SAuthEngine.IsAuthorAdministrator(authorID);

        }

        public static Boolean CurrentActionIsBy(this ControllerContext self, params String[] keys) {

            return self.RouteData.Values["action"].ToString().ToLowerInvariant().In(keys);

        }


		public static String RouteValueForKey(this UrlHelper self, String key) {

			return (self.RequestContext.RouteData.Values[key] ?? "").ToString();

		}

        public static String GetPublicUrl(this ViewContext self, String token) {


            return (self.Controller as BaseController).GeneratePublicUrlFrom(token);
        }

    }
}