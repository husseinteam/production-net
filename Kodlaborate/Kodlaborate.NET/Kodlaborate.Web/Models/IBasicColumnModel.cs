﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Kodlaborate.Web.Models {

    public interface IBasicColumnModel<TModel> : IBasicColumnModel {

    }

    public interface IBasicColumnModel : IContextRenderer {

        String Title { get; }

        dynamic ColumnGenerator{ get; }
        dynamic HtmlGenerator { get; }
        dynamic RowIndexGenerator { get; }

        IBasicColumnModel SetSortable();
        IBasicColumnModel SetContent<TModel>(Func<Int32, TModel, Object> generator);
        IBasicColumnModel SetHtmlContent<TModel>(Func<Int32, TModel, String[]> generator);
        IBasicColumnModel SetRowIndexContent<TModel>(Func<Int32, TModel, String> generator);
        IBasicColumnModel SetTitle(String title);

    }

}
