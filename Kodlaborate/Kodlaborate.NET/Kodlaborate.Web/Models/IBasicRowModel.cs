﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Kodlaborate.Web.Models {

    public interface IBasicRowModel {
        
        System.Web.IHtmlString Render();

    }

}
