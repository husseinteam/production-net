﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;
using LLF.Extensions;
using Kodlaborate.Web.Controllers;

namespace Kodlaborate.Web.Models {
    public static class Extensions {

        public static Boolean ActorLoggedIn(this ViewContext self) {

            return (self.Controller as BaseController).SessionFor<Int64>("primarypk") != default(Int64);

        }

        public static Boolean ContextActionIs(this ViewContext self, String action) {

            return self.RouteData.Values["action"].ToString().ToLower() == action.ToLower();

        }

        public static MvcHtmlString Script(this HtmlHelper htmlHelper, Int32 order, params Func<object, HelperResult>[] templates) {
            templates.Enumerate((i, template) =>
                htmlHelper.ViewContext.HttpContext.Items["_script_{0}_{1}".PostFormat(order, i)] = template);
            return MvcHtmlString.Empty;
        }

        public static MvcHtmlString ScriptSingleton(this HtmlHelper htmlHelper, Func<object, HelperResult> template) {

            htmlHelper.ViewContext.HttpContext.Items["_script_singleton_".PostFormat(htmlHelper.ViewContext.RouteData.Values["action"].ToString().ToLower())] = template;
            return MvcHtmlString.Empty;

        }

        public static MvcHtmlString Style(this HtmlHelper htmlHelper, params Func<object, HelperResult>[] templates) {
            templates.Enumerate((i, template) =>
                htmlHelper.ViewContext.HttpContext.Items["_style_" + i] = template);
            return MvcHtmlString.Empty;
        }

		public static IHtmlString Virtualize(this HtmlHelper htmlHelper, String url) {

			return new MvcHtmlString(url);

		}

		public static IHtmlString RenderScripts(this HtmlHelper htmlHelper) {
			var sorted = htmlHelper.ViewContext.HttpContext.Items.Keys.OfType<String>()
                .Where(k => k.StartsWith("_script_") && !k.StartsWith("_script_singleton_")).ToList();
			sorted.Sort((prev, next) =>
				Convert.ToInt32(prev.Replace("_script_", "").Split('_')[0]).CompareTo(
					Convert.ToInt32(next.Replace("_script_", "").Split('_')[0])));
			foreach (var key in sorted) {
				var template = htmlHelper.ViewContext.HttpContext.Items[key] as Func<object, HelperResult>;
				if (template != null) {
					htmlHelper.ViewContext.Writer.Write(template(null));
				}
			}
            foreach (var key in htmlHelper.ViewContext.HttpContext.Items.Keys.OfType<String>().Where(k => k.StartsWith("_script_singleton_"))) {
                var template = htmlHelper.ViewContext.HttpContext.Items[key] as Func<object, HelperResult>;
                if (template != null) {
                    htmlHelper.ViewContext.Writer.Write(template(null));
                }
            }
			return MvcHtmlString.Empty;
		}

        public static IHtmlString RenderStyles(this HtmlHelper htmlHelper) {
            var sorted = htmlHelper.ViewContext.HttpContext.Items.Keys.OfType<String>()
                .Where(k => k.StartsWith("_style_")).ToList();
            sorted.Sort((prev, next) =>
                prev.Replace("_style_", "").ToType<Int32>().CompareTo(
                    next.Replace("_style_", "").ToType<Int32>()));
            foreach (var key in sorted) {
                var template = htmlHelper.ViewContext.HttpContext.Items[key] as Func<object, HelperResult>;
                if (template != null) {
                    htmlHelper.ViewContext.Writer.Write(template(null));
                }
            }
            return MvcHtmlString.Empty;
        }

    }

}


