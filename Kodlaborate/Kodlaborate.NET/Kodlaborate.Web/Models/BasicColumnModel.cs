﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using LLF.Extensions;
using System.Web;

namespace Kodlaborate.Web.Models {

    public class BasicColumnModel : IBasicColumnModel {

        #region Fields

        private Int32 index;
        private String title;
        private Boolean sortable;
        private dynamic generator;
        private dynamic rowIndexGenerator;
        private dynamic htmlGenerator;
        private BasicTablifyModel container;

        #endregion

        #region Ctorss

        public BasicColumnModel(Int32 index, BasicTablifyModel container) {

            this.container = container;
            this.index = index;

        }

        #endregion

        #region IBasicTablifyColumn Members

        public IBasicColumnModel SetTitle(string title) {

            this.title = title;
            return this;

        }

        public string Title {
            get { return title; }
        }

        public dynamic ColumnGenerator {
            get {
                return generator;
            }
        }

        public dynamic HtmlGenerator {
            get {
                return htmlGenerator;
            }
        }

        public dynamic RowIndexGenerator {
            get {
                return rowIndexGenerator;
            }
        }

        public IBasicColumnModel SetContent<TModel>(Func<Int32, TModel, Object> generator) {

            this.generator = generator;
            return this;

        }

        public IBasicColumnModel SetRowIndexContent<TModel>(Func<Int32, TModel, String> generator) {

            this.rowIndexGenerator = generator;
            return this;

        }

        public IBasicColumnModel SetHtmlContent<TModel>(Func<Int32, TModel, String[]> generator) {

            this.htmlGenerator = generator;
            return this;

        }

        public IBasicColumnModel SetSortable() {

            this.sortable = true;
            return this;

        }

        #endregion


        #region IContextRenderer Members

        public System.Web.IHtmlString Render(ViewContext context, String format = "{0}") {

            IHtmlString resp = null;
            var postFormat = format.PostFormat("<th>{0}</th>");
            if (this.sortable) {
                var sortCommand = new AjaxCommand()
                        .SetAction(this.container.Action)
                        .SetController(this.container.Controller)
                        .SetCommandTitle(this.title)
                        .SetRouteData(new { command = "sort", argument = this.index > 0 ? (this.index = -this.index).ToString() : (this.index = -this.index).ToString(), target = this.container.AjaxTarget })
                        .SetOnSuccess("succeeded");
                resp = sortCommand.Render(context, postFormat);
            } else {
                resp = new MvcHtmlString(postFormat.PostFormat(this.title));
            }
            return resp;

        }

        #endregion
    }

}
