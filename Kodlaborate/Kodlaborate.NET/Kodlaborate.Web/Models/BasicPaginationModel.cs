﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LLF.Extensions;

namespace Kodlaborate.Web.Models {
 
    public class BasicPaginationModel : IBasicPaginationModel {

        private IEnumerable enumerable;
        private Int32 page;
        private Int32 pageSize;
        private Int32 paginationWidth;

        public BasicPaginationModel(IEnumerable enumerable, Int32 page, Int32 pageSize, String action, Int32 paginationWidth = 11) {

            this.enumerable = enumerable;
            this.page = page == 0 ? 1 : page;
            this.pageSize = pageSize;
            this.paginationWidth = paginationWidth;
            this.Action = action;

        }

        public Int32 RowStartIndex {
            get {
                return ((this.ActivePage - 1) * this.pageSize) + 1;
            }
        }

        public Int32 ActivePage {
            get {
                return this.page > 0 ? this.page : this.Beginning + (this.OnEllipsis ? 1 : 0);
            }
        }

        public Boolean OnEllipsis {

            get {
                var end = (this.enumerable.OfType<Object>().Count() / pageSize) + 1;
                return this.page == -1 || (this.page >= paginationWidth / 2 && this.page <= end - paginationWidth / 2 );
            }

        }

        public IEnumerable<KeyValuePair<Int32, Boolean>> EnumeratePages() {
            var ellipsisProcessed = false;
            for (int k = this.Beginning; k <= this.End; k++) {
                if (!this.PageIsEllipsis(k)) {
                    yield return new KeyValuePair<Int32, Boolean>(k, true);
                } else if (!ellipsisProcessed) {
                    yield return new KeyValuePair<Int32, Boolean>(k, false);
                    ellipsisProcessed = true;
                }
            }
        }

        public IEnumerable PaginatedModel(Type modelType) {

            return this.enumerable.ThatType(modelType).WithPaging(this.page, this.pageSize);

        }

        public IEnumerable<TModel> PaginatedModel<TModel>() {

            return this.enumerable.OfType<TModel>().WithPaging(this.page, this.pageSize);

        }

        public String Action { get; private set; }

        private Boolean PageIsEllipsis(Int32 k) {

            var length = (this.End - this.Beginning + 1);
            if (this.OnEllipsis) {
                return k == this.Beginning;
            } else {
                return !(k < paginationWidth / 2 || k > length - paginationWidth / 2);
            }

        }

        private Int32 Beginning {
            get {
                return this.OnEllipsis ? paginationWidth / 2 - 1 : 1;
            }
        }

        private Int32 End {
            get {
                var end = this.enumerable.OfType<Object>().Count();
                end = end % pageSize == 0 ? end / pageSize : end / pageSize + 1;
                return this.OnEllipsis ? end - this.Beginning + -1 : end;
            }
        }

    }

}