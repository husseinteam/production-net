﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Kodlaborate.Web.Models {

    public interface IAjaxCommand : IContextRenderer {

        String Action { get; }
        String Controller { get; }
        Object RouteData { get; }

        String OnSuccess { get; }
        String CommandTitle { get; }
        Boolean AllowCache { get; }


        IAjaxCommand  SetAction(String action);
        IAjaxCommand  SetController(String controller);
        IAjaxCommand  SetRouteData(Object routeData);

        IAjaxCommand  SetOnSuccess(String onSuccess);
        IAjaxCommand  SetCommandTitle(String commandTitle);
        IAjaxCommand SetAllowCache(Boolean allowCache);



    }

}
