﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kodlaborate.Web.Models {

    public interface IBasicPaginationModel {

        Int32 RowStartIndex { get; }
        Int32 ActivePage { get; }
        Boolean OnEllipsis { get; }
        String Action { get; }

        IEnumerable<KeyValuePair<Int32, Boolean>> EnumeratePages();
        IEnumerable PaginatedModel(Type modelType);

    }

}
