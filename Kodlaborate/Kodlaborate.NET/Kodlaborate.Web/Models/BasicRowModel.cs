﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using LLF.Extensions;

namespace Kodlaborate.Web.Models {

    public class BasicRowModel : IBasicRowModel {

        #region Fields

        private dynamic content;
        private IEnumerable<IBasicColumnModel> columns;
        private Int32 index;
        private BasicTablifyModel container;

        #endregion

        #region Ctors

        public BasicRowModel(Int32 index, object content, IEnumerable<IBasicColumnModel> columns, BasicTablifyModel container) {

            this.container = container;
            this.columns = columns;
            this.content = content;
            this.index = index;
        }

        #endregion

        #region IBasicTablifyColumn Members

        public System.Web.IHtmlString Render() {

            var sb = new StringBuilder("<tr>");

            foreach (var col in this.columns) {
                Object generated = null;
                if (col.ColumnGenerator != null) {
                    generated = col.ColumnGenerator(this.index, this.content);
                } else if (col.HtmlGenerator != null) {
                    IEnumerable<String> array = col.HtmlGenerator(this.index, this.content);
                    var format = array.First();
                    generated = format.PostFormat(array.Skip(1).ToArray());
                } else if (col.RowIndexGenerator != null) {
                    generated = col.RowIndexGenerator(
                        this.container.Paginator.RowStartIndex + this.index, this.content);
                } else {
                    generated = "";
                }
                sb.AppendFormat(@"<td>{0}</td>".PostFormat(generated));
            }
            sb.AppendFormat("</tr>");
            return new MvcHtmlString(sb.ToString());

        }

        #endregion

    }

}
