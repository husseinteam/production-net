﻿function ShowModal(parsed, callback) {

    if (parsed.IsHtml) {
        $('#div-modal-body').html($(parsed.Html).html());
    } else {
        $('#div-modal-body').html(parsed.Message);
    }
    $('#div-modal-body')
		.removeClass('text-danger').removeClass('text-success')
		.addClass(parsed.Success ? 'text-success' : 'text-danger');
    $('#h-modal-title').html(parsed.ModalTitle);
    $('#h-modal-title')
		.removeClass('text-danger').removeClass('text-success')
		.addClass(parsed.Success ? 'text-success' : 'text-danger');
    $('#modal-container').on('hide.bs.modal', function (e) {
        callback && callback(parsed);
    });
    $('#modal-container').modal({
        show: true
    });
    if ($('#modal-container').length == 0) {
        callback(parsed);
    }
    return false;

}
function bootstrapper() {
    $('a.ajax-submitter').click(function (e) {
        onbtnclick($(this), $(this).data('confirm'));
    });
    $('*[data-toggle=tooltip]').tooltip();

};
function paginator(ajaxTarget, ajaxSource, action) {

    $('ul.pagination[data-lmpc-action="' + action + '"] li:not(.active) > a').each(function (i, a) {
        $(a).click(function (e) {
            e.preventDefault();
            var pg = $(a).data('page');
            var newSource = ajaxSource.replace(/\/([\w-]+)\/([\w-]+)\/([\w-]+)\/(\d+)\/([\w-]+)/, '/$1' + '/' + action + '/' + '$3' + '/' + pg + '/' + '$5')
            if (newSource.indexOf('page') == -1) {
                newSource += '/page/' + pg + '/' + ajaxTarget;
            }
            $('#' + ajaxTarget).load(newSource, function () {
                bootstrapper();
                paginator(ajaxTarget, ajaxSource, action);
            });
        });
    });
};
function succeeded(data) {
    var parsed = {};
    try {
        parsed = JSON.parse(data)
    } catch (e) {
        parsed = data;
    }
    parsed = parsed.Response;
    function callback(p) {
        if (p.Redirect) {
            if (p.Redirect != "postback") {
                window.location.pathname = p.Redirect;
            } else {
                document.location.href = document.location.href;
            }
        } else if (p.AjaxTarget) {
            var action = p.AjaxSource.match(/\/([\w-]+)\/([\w-]+)/).last();
            var pg = $('ul.pagination[data-lmpc-action="' + action + '"] li.active > a:first').data('page');
            var newSource = p.AjaxSource.replace(/(page\/)(\d+)(\/)/, "$1" + pg + "$3");
            if (newSource.indexOf('page') == -1) {
                newSource += '/page/' + pg + '/' + p.AjaxTarget;
            }
            $('#' + p.AjaxTarget).load(newSource, function () {
                bootstrapper();
                paginator(p.AjaxTarget, newSource, action)
            });
        } 

    };
    ShowModal(parsed, function (parsed) {
        callback(parsed);
    });
}
function succeeds(Message, ModalTitle, Success, Redirect) {
    var parsed = {
        Message: Message,
        ModalTitle: ModalTitle,
        Success: Success,
        Redirect: Redirect
    }
    ShowModal(parsed, function (parsed) {
        if (parsed.Redirect) {
            if (parsed.Redirect != "postback") {
                window.location.pathname = parsed.Redirect;
            } else {
                document.location.href = document.location.href;
            }
        }
    });
};

$(function () {
    //var copyTextareaBtn = document.querySelector('.clipboard-trigger');

    //copyTextareaBtn.addEventListener('click', function (event) {
    //    var copyTextarea = document.querySelector('.clipboard-trigger');
    //    copyTextarea.select();
    //    try {
    //        document.execCommand('copy');
    //    } catch (err) {
    //        console.log('Oops, unable to copy');
    //        console.log(err);
    //    }
    //});
});

