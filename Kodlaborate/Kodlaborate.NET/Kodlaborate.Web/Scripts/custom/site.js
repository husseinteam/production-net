﻿
$(document).ajaxStart(ajaxStart);
$(document).ajaxStop(ajaxComplete);

function ajaxStart() {

    if ($('#__screenlobe').length > 0) {
        $('#__screenlobe').remove();
    }
	var screen = $('<div id="__screenlobe"></div>').css({
		'opacity': '0.6', 'background-color': 'rgb(242,242,242)', 'z-index': '99999',
		'background-image': 'url("/Images/loading.gif")',
		'background-position': 'center', 'background-repeat': 'no-repeat',
		'margin-top': '0px',
		'position': 'fixed',
		'top': '0px',
		'right': '0px'
	}).width($(document).width()).height($(window).height());
	
	screen.appendTo('body');
	console.log('ajaxStarted');
}
function ajaxComplete() {

	if ($('#__screenlobe').length > 0) {
	    $('#__screenlobe').remove();
	}
	console.log('ajaxEnded');

}

function onbtnclick(element, confirmText) {
    if (confirmText) {
        if (confirm(confirmText)) {
            $(element).closest('form').submit();
        };
    } else {
        $(element).closest('form').submit();
    }
}
$(function () {
    bootstrapper();
    $.ajaxSetup({
        global: true
    });
});


String.prototype.replaceAll = function (find, replace) {

    return this.replace(new RegExp(find, 'g'), replace);

}
Array.prototype.last = function () {

    return this[this.length - 1];

}