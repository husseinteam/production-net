﻿using Kodlaborate.CoreData.StaticObjects;
using System;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using LLF.Extensions;
using Kodlaborate.Resources.Globals;
using Kodlaborate.Web.Controllers;
using LLF.Annotations.CustomMVCFilters;
using Kodlaborate.ViewModels.Engine;
using Kodlaborate.CoreData.EntityObjects;
using Kodlaborate.CoreData.Enums;

namespace Kodlaborate.Web {

    public class MvcApplication : System.Web.HttpApplication {

        protected void Application_Start() {

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            Bootstrapper.RegisterDependencies();

        }

        protected void Application_AcquireRequestState(object sender, EventArgs e) {

            String cultureName = null;
            // Attempt to read the culture cookie from Request
            var cultureCookie = Request.Cookies["culture"];
            if (cultureCookie != null)
                cultureName = cultureCookie.Value;
            else
                cultureName = Request.UserLanguages != null ? Request.UserLanguages[0] : "en-US"; // obtain it from HTTP header AcceptLanguages

            // Modify current thread's cultures            
            Thread.CurrentThread.CurrentCulture = cultureName.Globalize();
            Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;

        }

        protected void Session_Start(object sender, EventArgs e) {

#if DEBUG
            Session["auth"] = SAuthEngine.GetAuthorByUserName("lampiclobe@outlook.com").ID;
#endif

            SAppConfig.ResetCrement(DateTime.Now, EAppConfig.DailyUserVisitCount);
            SAppConfig.ResetCrement(DateTime.Now, EAppConfig.MonthlyUserRegistrationCount);
            SAppConfig.ResetCrement(DateTime.Now, EAppConfig.DailyLinkAdded);

        }

        protected void Application_Error(object sender, EventArgs e) {
            //CustomErrorProviderInjector.Inject(this, Server.GetLastError(), new BaseController(),
            //    (statusCode) => {
            //        switch (statusCode) {
            //            case 404:
            //                return "NotFound";
            //            case 500:
            //                return "Internal";
            //            default:
            //                return "GenericError";
            //        }
            //    });
        }

    }
}
