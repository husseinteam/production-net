﻿using System;
using System.Linq;
using System.Web.Mvc;

using Kodlaborate.Resources.Globals;
using Kodlaborate.ViewModels.PageModels;
using Kodlaborate.CoreData.Enums;

using LLF.Extensions;
using Kodlaborate.ViewModels.Engine;
using Newtonsoft.Json;

namespace Kodlaborate.Web.Controllers {

    [RoutePrefix("a")]
    public class AdminPanelController : BaseController {

        // GET: AdminPanel
        [Route("", Name = SSegmentConfig.Dashboard)]
        public ActionResult Dashboard(DashboardModel vm) {

            return SuperUserAccess<DashboardModel>(vm, (id, model) => View(model));

        }

        [Route(SSegmentConfig.UserGrid + "/{command:values(delete|freeze|activate|page|deleteall|freezeall|activateall)?}/{argument?}/{target?}", Name = SSegmentConfig.UserGrid)]
        public ActionResult UserGrid(UserGridModel vm) {

            return SuperUserAccess<UserGridModel>(vm, (id, model) => {
                var command = RouteData.Values["command"].ToType<String>();
                var argument = RouteData.Values["argument"].ToType<Int32>();
                if (Request.IsAjaxRequest()) {
                    if (Request.HttpMethod == "POST") {
                        model.Response.AjaxTarget = RouteData.Values["target"].ToType<String>();
                        model.Response.AjaxSource = Url.Action(SSegmentConfig.UserGrid, "adminpanel", new { command = String.Empty, argument = String.Empty, target = String.Empty });
                        switch (command) {
                            case "delete":
                                SAuthEngine.ChangeMembershipStatus(model, argument, EMembershipStatus.Deleted);
                                break;
                            case "freeze":
                                SAuthEngine.ChangeMembershipStatus(model, argument, EMembershipStatus.Frozen);
                                break;
                            case "activate":
                                SAuthEngine.ChangeMembershipStatus(model, argument, EMembershipStatus.Active);
                                break;
                            case "deleteall":
                                SAuthEngine.ChangeMembershipStatusForAll(model, EMembershipStatus.Deleted);
                                break;
                            case "freezeall":
                                SAuthEngine.ChangeMembershipStatusForAll(model, EMembershipStatus.Frozen);
                                break;
                            case "activateall":
                                SAuthEngine.ChangeMembershipStatusForAll(model, EMembershipStatus.Active);
                                break;
                            case "page":
                                break;
                            default:
                                break;
                        }
                        return Json(JsonConvert.SerializeObject(model), JsonRequestBehavior.AllowGet);
                    }
                } 
                return PartialView("_UserGrid", model);
            });

        }

        [Route(SSegmentConfig.LinkGrid + "/{command:values(sort|delete|offline|online|page|deleteall|offlineall|onlineall)?}/{argument?}/{target?}", Name = SSegmentConfig.LinkGrid)]
        public ActionResult LinkGrid(PublicLinkModel vm) {

            return SuperUserAccess<PublicLinkModel>(vm, (id, model) => {
                var command = RouteData.Values["command"].ToType<String>();
                var argument = RouteData.Values["argument"].ToType<Int32>();
                if (Request.IsAjaxRequest()) {
                    if (Request.HttpMethod == "POST") {
                        model.Response.AjaxTarget = RouteData.Values["target"].ToType<String>();
                        model.Response.AjaxSource = Url.Action(SSegmentConfig.LinkGrid, "adminpanel", new { command = String.Empty, argument = String.Empty, target = String.Empty });
                        switch (command) {
                            case "delete":
                                SAdminEngine.ChangePublicLinkStatus(model, argument, ELinkStatus.Deleted);
                                break;
                            case "offline":
                                SAdminEngine.ChangePublicLinkStatus(model, argument, ELinkStatus.Offline);
                                break;
                            case "online":
                                SAdminEngine.ChangePublicLinkStatus(model, argument, ELinkStatus.Online);
                                break;
                            case "deleteall":
                                SAdminEngine.ChangePublicLinkStatusForAll(model, ELinkStatus.Deleted);
                                break;
                            case "offlineall":
                                SAdminEngine.ChangePublicLinkStatusForAll(model, ELinkStatus.Offline);
                                break;
                            case "onlineall":
                                SAdminEngine.ChangePublicLinkStatusForAll(model, ELinkStatus.Online);
                                break;
                            case "sort":
                                switch (argument) {
                                    case 1:
                                    case -1:
                                        model.PublicLinks = argument > 0 ? model.PublicLinks.OrderByDescending(l => l.CodeLinkID).ToArray() : model.PublicLinks.OrderBy(l => l.CodeLinkID).ToArray();
                                        break;
                                    case 2:
                                    case -2:
                                        model.PublicLinks = argument > 0 ? model.PublicLinks.OrderByDescending(l => l.Token).ToArray() : model.PublicLinks.OrderBy(l => l.ClickCount).ToArray();
                                        break;
                                    case 3:
                                    case -3:
                                        model.PublicLinks = argument > 0 ? model.PublicLinks.OrderByDescending(l => l.ClickCount).ToArray() : model.PublicLinks.OrderBy(l => l.ClickCount).ToArray();
                                        break;
                                    case 4:
                                    case -4:
                                        model.PublicLinks = argument > 0 ? model.PublicLinks.OrderByDescending(l => l.CodeBlockCount).ToArray() : model.PublicLinks.OrderBy(l => l.CodeBlockCount).ToArray();
                                        break;
                                    case 5:
                                    case -5:
                                        model.PublicLinks = argument > 0 ? model.PublicLinks.OrderByDescending(l => l.GeneratedOn).ToArray() : model.PublicLinks.OrderBy(l => l.GeneratedOn).ToArray();
                                        break;
                                    case 6:
                                    case -6:
                                        model.PublicLinks = argument > 0 ? model.PublicLinks.OrderByDescending(l => l.StatusText).ToArray() : model.PublicLinks.OrderBy(l => l.StatusText).ToArray();
                                        break;
                                    default:
                                        break;
                                }
                                break;
                            case "page":
                                break;
                            default:
                                break;
                        }
                        return Json(JsonConvert.SerializeObject(model), JsonRequestBehavior.AllowGet);
                    }
                }
                return PartialView("_LinkGrid", model);
            });

        }

    }
}