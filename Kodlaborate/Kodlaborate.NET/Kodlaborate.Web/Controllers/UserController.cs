﻿using System;
using System.Configuration;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web.Mvc;
using Kodlaborate.Resources.Locals;
using Kodlaborate.CoreData.Enums;
using Kodlaborate.Resources.Globals;
using LLF.Extensions;
using Newtonsoft.Json;
using LLF.Annotations.CustomMVCFilters;
using Kodlaborate.ViewModels.PageModels;
using Kodlaborate.ViewModels.Engine;
using Kodlaborate.CoreData.EntityObjects;

namespace Kodlaborate.Web.Controllers {

    [RoutePrefix("u")]
    public class UserController : BaseController {

        [Route(SSegmentConfig.Login, Name = SSegmentConfig.Login)]
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        [AjaxErrorHandler]
        public ActionResult Login(LoginModel vm) {

            if (Request.IsAjaxRequest()) {
                SAuthEngine.TryLogin(vm);
                if (vm.Response.Success == true) {
                    var authID = vm.Response.GetData<Int32>();
                    SessionFor<Int32>("auth", authID);

                    vm.Response.Redirect = SAuthEngine.IsAuthorAdministrator(authID) ?
                        Url.Action(SSegmentConfig.Dashboard, "adminpanel") :
                        Url.Action(SSegmentConfig.NewLink, "user");
                }
                return PublicAccess<LoginModel>(vm, model => Json(JsonConvert.SerializeObject(model), JsonRequestBehavior.AllowGet));
            } else {
                return PublicAccess<LoginModel>(vm, model => View(model));
            }
        }

        [Route(SSegmentConfig.Recover, Name = SSegmentConfig.Recover)]
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        [AjaxErrorHandler]
        public async Task<ActionResult> Recover(RecoverModel vm) {

            Func<String, String> siteLinker = (url) => "{0}://{1}{2}".PostFormat(Request.Url.Scheme, Request.Url.Authority, url);
            if (Request.IsAjaxRequest()) {

                vm.MailingUserName = ConfigurationManager.AppSettings["MailingUserName"];
                vm.MailingPassword = ConfigurationManager.AppSettings["MailingPassword"];
                vm.FromAddress = new MailAddress(ConfigurationManager.AppSettings["MailingFromEMail"], PageContent.SiteName);
                vm.ResetLink = siteLinker(Url.Action(SSegmentConfig.Repass, "user", new { token = String.Empty }));
                vm.RecoveryLink = siteLinker(Url.Action(SSegmentConfig.Recover, "user"));
                vm.TermsLink = siteLinker(Url.Action(SSegmentConfig.Terms, "user"));
                vm.PrivacyLink = siteLinker(Url.Action(SSegmentConfig.Privacy, "user"));
                vm.UnsubscribeLink = siteLinker(Url.Action(SSegmentConfig.Unsubscribe, "user"));
                vm.CompanyContactPhone = ConfigurationManager.AppSettings["CompanyContactPhone"];
                vm.CompanyContactEmail = ConfigurationManager.AppSettings["CompanyContactEmail"];
                vm.AddressLine1 = ConfigurationManager.AppSettings["AddressLine1"];
                vm.AddressLine2 = ConfigurationManager.AppSettings["AddressLine2"];
                vm.CopyrightTerm = "{0} {1} Inc.".PostFormat(DateTime.Now.Year, ConfigurationManager.AppSettings["CoorporationName"]);
                vm.SiteUrl = "{0}://{1}".PostFormat(Request.Url.Scheme, Request.Url.Authority);
                vm.SmtpServer = ConfigurationManager.AppSettings["SmtpServer"];

                vm.SiteName = PageContent.SiteName;
                vm.Sitelogo = siteLinker("/Images/MailPlaceHolder200x50.jpg");


                await SAuthEngine.TryRecover(vm);
                if (vm.Response.Success) {
                    vm.Response.Redirect = Url.Action(SSegmentConfig.Login, "user");
                }
                return PublicAccess<RecoverModel>(vm, model =>
                    Json(JsonConvert.SerializeObject(model), JsonRequestBehavior.AllowGet));
            } else {
                return PublicAccess<RecoverModel>(vm, model => View(model));
            }
        }

        [Route(SSegmentConfig.Repass + "/{token?}", Name = SSegmentConfig.Repass)]
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        [AjaxErrorHandler]
        public ActionResult Repass(RepassModel vm) {

            if (Request.IsAjaxRequest()) {

                SAuthEngine.TryRepassPassword(vm);
                if (vm.Response.Success) {
                    vm.Response.Redirect = Url.Action(SSegmentConfig.Login, "user");
                }
                return PublicAccess<RepassModel>(vm, model =>
                    Json(JsonConvert.SerializeObject(model), JsonRequestBehavior.AllowGet));
            } else {
                SAuthEngine.ValidateRepass(vm);
                if (!vm.Response.Success) {
                    vm.Response.Redirect = Url.Action(SSegmentConfig.Login, "user");
                }
                return PublicAccess<RepassModel>(vm, model => View(model));
            }

        }

        [Route(SSegmentConfig.Register, Name = SSegmentConfig.Register)]
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        [AjaxErrorHandler]
        public ActionResult Register(IdentityModel vm) {

            if (Request.IsAjaxRequest()) {
                SAuthEngine.TryRegister(vm);
                if (vm.Response.Success) {
                    vm.Response.Redirect = Url.Action(SSegmentConfig.CodeGrid, "user", new { command = String.Empty, argument = String.Empty, target = String.Empty });
                    SessionFor<Int32>("auth", vm.Response.GetData<Int32>());
                    SAppConfig.ResetCrement(DateTime.Now, EAppConfig.MonthlyUserRegistrationCount);
                }
                return PublicAccess<IdentityModel>(vm, model =>
                    Json(JsonConvert.SerializeObject(model), JsonRequestBehavior.AllowGet));
            } else {
                return PublicAccess<IdentityModel>(vm, model => View(model));
            }

        }

        [Route(SSegmentConfig.ModifyProfile, Name = SSegmentConfig.ModifyProfile)]
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        [AjaxErrorHandler]
        public ActionResult ModifyProfile(IdentityModel vm) {

            if (Request.IsAjaxRequest()) {

                return ProtectedAccess<IdentityModel>(vm, (id, model) => {
                    SAuthEngine.ModifyProfile(id, model);
                    if (model.Response.Success) {
                        model.Response.Redirect = Url.Action(SSegmentConfig.ModifyProfile, "user");
                    }
                    return Json(JsonConvert.SerializeObject(model), JsonRequestBehavior.AllowGet);
                });
            } else {
                return ProtectedAccess<IdentityModel>(vm, (id, model) =>
                    View(SAuthEngine.CurrentIdentity(id)));
            }

        }

        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        [ValidateInput(false)]
        [AjaxErrorHandler]
        [Route(SSegmentConfig.NewLink, Name = SSegmentConfig.NewLink)]
        public ActionResult NewLink(CodeBlockModel vm) {

            return ProtectedAccess<CodeBlockModel>(vm, (id, model) => {
                if (Request.IsAjaxRequest()) {
                    SCodeEngine.SetNewCodeBlock(id, model, row =>
                        row.PublicUrl = GeneratePublicUrlFrom(row.Token));
                    if (model.Response.Success) {
                        model.Response.Redirect = Url.Action(SSegmentConfig.CodeGrid, "user", new { command = String.Empty, argument = String.Empty, target = String.Empty });
                    }
                    return Json(JsonConvert.SerializeObject(model), JsonRequestBehavior.AllowGet);
                } else {
                    return View(model);
                }
            });

        }

        [Route(SSegmentConfig.CodeGrid + "/{command:values(page|deleteall)?}/{argument?}/{target?}", Name = SSegmentConfig.CodeGrid)]
        public ActionResult CodeGrid(CodeGridModel vm) {

            return ProtectedAccess<CodeGridModel>(vm, (id, model) => {
                var command = RouteData.Values["command"].ToType<String>();
                var argument = RouteData.Values["argument"].ToType<Int32>();
                model.SetLinkRows(id, row => row.PublicUrl = GeneratePublicUrlFrom(row.Token));
                if (Request.IsAjaxRequest()) {
                    if (Request.HttpMethod == "POST") {
                        model.Response.AjaxTarget = RouteData.Values["target"].ToType<String>();
                        model.Response.AjaxSource = Url.Action(SSegmentConfig.CodeGrid, "user", new { command = String.Empty, argument = String.Empty, target = String.Empty });
                        switch (command) {
                            case "deleteall":
                                SCodeEngine.DeleteAllCodesOf(id, model);
                                break;
                            case "page":
                                break;
                            default:
                                break;
                        }
                        return Json(JsonConvert.SerializeObject(model), JsonRequestBehavior.AllowGet);
                    }
                }
                return View(model);
            });

        }

        [Route(SSegmentConfig.ExtendLink + "/{token}", Name = SSegmentConfig.ExtendLink)]
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        [ValidateInput(false)]
        [AjaxErrorHandler]
        public ActionResult ExtendLink(CodeBlockModel vm) {

            var token = RouteData.Values["token"].ToType<String>();
            return ProtectedAccess<CodeBlockModel>(vm, (id, model) => {
                if (Request.IsAjaxRequest()) {
                    SCodeEngine.ExtendLinkForAuthor(id, model,
                        row => row.PublicUrl = GeneratePublicUrlFrom(token));
                    if (model.Response.Success) {
                        model.Response.Redirect = Url.Action(SSegmentConfig.CodeGrid, "user", new { command = String.Empty, argument = String.Empty, target = String.Empty });
                    }
                    return Json(JsonConvert.SerializeObject(model), JsonRequestBehavior.AllowGet);
                } else {
                    return View(SCodeEngine.GetLinkOf(id, token,
                        row => row.PublicUrl = GeneratePublicUrlFrom(token)));
                }
            });

        }

        [Route(SSegmentConfig.RemoveLink + "/{argument}", Name = SSegmentConfig.RemoveLink)]
        [HttpPost]
        [AjaxErrorHandler]
        public ActionResult RemoveLink(DisplayLinkModel vm) {

            if (Request.IsAjaxRequest()) {

                var clinkID = RouteData.Values["argument"].ToType<Int32>();
                return ProtectedAccess<DisplayLinkModel>(vm, (id, model) => {
                    SCodeEngine.RemoveLink(clinkID, model);
                    if (model.Response.Success) {
                        model.Response.Redirect = Url.Action(SSegmentConfig.CodeGrid, "user", new { command = String.Empty, argument = String.Empty, target = String.Empty });
                    }
                    return Json(JsonConvert.SerializeObject(model), JsonRequestBehavior.AllowGet);
                });
            } else {
                return ProtectedAccess<DisplayLinkModel>(vm, (id, model) => View(model));
            }

        }


        [Route(SSegmentConfig.DeleteBlock + "/{argument}", Name = SSegmentConfig.DeleteBlock)]
        [HttpPost]
        [AjaxErrorHandler]
        public ActionResult DeleteBlock(CodeBlockModel vm) {

            if (Request.IsAjaxRequest()) {

                var bid = RouteData.Values["argument"].ToType<Int32>();
                return ProtectedAccess<CodeBlockModel>(vm, (id, model) => {
                    SCodeEngine.DeleteCodeBlock(bid, model);
                    return Json(JsonConvert.SerializeObject(model), JsonRequestBehavior.AllowGet);
                });
            } else {
                return ProtectedAccess<CodeBlockModel>(vm, (id, model) => View(model));
            }

        }

        [Route(SSegmentConfig.ModifyLink + "/{command:values(view|edit)}/{argument}", Name = SSegmentConfig.ModifyLink)]
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        [ValidateInput(false)]
        [AjaxErrorHandler]
        public ActionResult ModifyLink(DisplayLinkModel vm) {

            var command = RouteData.Values["command"].ToType<String>();
            return ProtectedAccess<DisplayLinkModel>(vm, (id, model) => {
                if (Request.IsAjaxRequest()) {
                    var bid = RouteData.Values["argument"].ToType<Int32>();
                    if (command == "edit" && !bid.IsUnassigned()) {
                        SCodeEngine.ModifyCodeBlock(bid, model, row => row.PublicUrl = GeneratePublicUrlFrom(row.Token));
                    } else if (command == "view") {
                        var token = RouteData.Values["argument"].ToType<String>();
                        model = SCodeEngine.GetLinkOf(id, token,
                            row => row.PublicUrl = GeneratePublicUrlFrom(token));
                    }
                    if (model.Response.Success) {
                        model.Response.Redirect = Url.Action(SSegmentConfig.CodeGrid, "user", new { command = String.Empty, argument = String.Empty, target = String.Empty });
                    }
                    return Json(JsonConvert.SerializeObject(model), JsonRequestBehavior.AllowGet);
                } else {
                    var token = RouteData.Values["argument"].ToType<String>();
                    var mLink = SCodeEngine.GetLinkOf(id, token,
                            row => row.PublicUrl = GeneratePublicUrlFrom(token));
                    if (!mLink.Response.Success) {
                        mLink.Response.Redirect = Url.Action(SSegmentConfig.CodeGrid, "user", new { command = String.Empty, argument = String.Empty, target = String.Empty });
                    }
                    return View(mLink);
                }
            });

        }

        [Route(SSegmentConfig.Logout, Name = SSegmentConfig.Logout)]
        [HttpGet]
        public ActionResult Logout() {

            RemoveSessionFor<Int32>("auth");
            return Redirect(SSegmentConfig.Login);

        }

    }
}