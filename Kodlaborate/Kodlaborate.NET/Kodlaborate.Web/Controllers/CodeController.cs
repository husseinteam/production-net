﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Kodlaborate.Resources.Globals;
using Kodlaborate.Web.Models;
using LLF.Extensions;
using Kodlaborate.ViewModels.PageModels;
using Newtonsoft.Json;
using Kodlaborate.CoreData.StaticObjects;
using Kodlaborate.ViewModels.Engine;
using LLF.Annotations.CustomMVCFilters;

namespace Kodlaborate.Web.Controllers {

    [RoutePrefix("c")]
    public class CodeController : BaseController {

        [Route("{token}", Name = SSegmentConfig.DisplayLink)]
        [HttpGet]
        public ActionResult DisplayLink(DisplayLinkModel vm) {

            var token = RouteData.Values["token"].ToType<String>();
            vm.Token = token;
            SCodeEngine.SetCodeBlocksFor(vm);
            SCodeEngine.IncrementClickCount(vm, SessionFor<Int32>("auth"));

            if (!vm.Response.Success) {
                vm.Response.Redirect = Url.Action(SSegmentConfig.Login, "user");
            }
            return PublicAccess<DisplayLinkModel>(vm, (model) => View(model));

        }

        [Route(SSegmentConfig.GenerateLink, Name = SSegmentConfig.GenerateLink)]
        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Get)]
        [ValidateInput(false)]
        [AjaxErrorHandler]
        public ActionResult GenerateLink(CodeBlockModel vm) {

            if (Request.IsAjaxRequest()) {
                SCodeEngine.AddPublicLink(vm);
                return PublicAccess<CodeBlockModel>(vm, (model) =>
                    Json(JsonConvert.SerializeObject(model)));
            } else {
                return PublicAccess<CodeBlockModel>(vm, (model) => View(model));
            }

        }

        [Route(SSegmentConfig.Tokenize, Name = SSegmentConfig.Tokenize)]
        [HttpGet]
        [AjaxErrorHandler]
        public ActionResult Tokenize(CodeBlockModel vm) {

            if (Request.IsAjaxRequest()) {
                SCodeEngine.Tokenize(vm, vmodel => vmodel.PublicUrl = GeneratePublicUrlFrom(vmodel.Token));
                return PublicAccess<CodeBlockModel>(vm, (model) =>
                    Json(JsonConvert.SerializeObject(model), JsonRequestBehavior.AllowGet));
            } else {
                return PublicAccess<CodeBlockModel>(vm, (vmodel) => View(vmodel));
            }

        }

    }
}