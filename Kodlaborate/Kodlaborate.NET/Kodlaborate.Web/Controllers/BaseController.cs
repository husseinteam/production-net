﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Kodlaborate.Resources.Globals;
using LLF.Extensions;
using Kodlaborate.Web.Models;

using System.Threading;
using Newtonsoft.Json;
using LLF.Annotations.CustomMVCFilters;
using Kodlaborate.CoreData.StaticObjects;
using Kodlaborate.ViewModels.PageModels;
using Kodlaborate.ViewModels.Engine;
using Kodlaborate.ViewModels.Protocols;

namespace Kodlaborate.Web.Controllers {
    [LLFErrorHandler(typeof(Exception))]
    public class BaseController : Controller {

        #region Internal

        internal TItem SessionFor<TItem>(String key, TItem item = default(TItem)) {

            if (!item.Equals(default(TItem))) {
                Session[key] = item;
            }
            return Session[key].ToType<TItem>();
        }

        internal String GeneratePublicUrlFrom(String token) {

            return "{0}://{1}{2}".PostFormat(Request.Url.Scheme, Request.Url.Authority, Url.Action(SSegmentConfig.DisplayLink, "code", new { token = token }));

        }

        #endregion

        #region Protected

        protected ActionResult PublicAccess<TModel>(TModel model, Func<TModel, ActionResult> callback) {

            return callback(model);

        }

        protected ActionResult ProtectedAccess<TModel>(TModel model, Func<Int32, TModel, ActionResult> doneback) {
            var id = SessionFor<Int32>("auth");
            if (id != default(Int32)) {
                return doneback(id, model);
            } else {
                return RedirectToAction(SSegmentConfig.Relay, "default");
            }
        }

        protected ActionResult SuperUserAccess<TModel>(TModel model, Func<Int32, TModel, ActionResult> doneback) 
            where TModel : IGenericViewModel {
            var id = SessionFor<Int32>("auth");
            if (id != default(Int32)) {
                if (SAuthEngine.IsAuthorAdministrator(id)) {
                    model.SetContent(id);
                    return doneback(id, model);
                } else {
                    return RedirectToAction(SSegmentConfig.CodeGrid, "user");
                }
            } else {
                return RedirectToAction(SSegmentConfig.Relay, "default");
            }
        }

        protected void RemoveSessionFor<TItem>(String key) {

            Session[key] = default(TItem);

        }

        protected RedirectResult ToHome() {

            return Redirect("~/");

        }

        protected RedirectResult ToReferrer() {

            return Redirect(Request.UrlReferrer.ToString());

        }

        #endregion

    }
}