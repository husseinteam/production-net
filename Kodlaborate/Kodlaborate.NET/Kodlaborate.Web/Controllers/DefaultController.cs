﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

using Kodlaborate.Resources.Globals;
using LLF.Extensions;
using Kodlaborate.ViewModels.Engine;

namespace Kodlaborate.Web.Controllers {

    [RoutePrefix("d")]
    public class DefaultController : BaseController {

        [Route("~/", Name = SSegmentConfig.Relay)]
        public ActionResult Relay() {

            var authID = SessionFor<Int32>("auth");
            if (authID != 0) {
                return SAuthEngine.IsAuthorAdministrator(authID) ?
                        RedirectToAction(SSegmentConfig.Dashboard, "adminpanel") :
                        RedirectToAction(SSegmentConfig.NewLink, "user");
            } else {
                return RedirectToAction(SSegmentConfig.GenerateLink, "code");
            }
        }


        [Route(SSegmentConfig.Lang + "/{language}", Name = SSegmentConfig.Lang)]
        [HttpGet]
        public ActionResult Lang(String language) {

            var cookie = Response.Cookies["culture"];
            if (cookie == null) {
                Response.Cookies.Add(new HttpCookie("culture", language));
            } else {
                cookie.Value = language;
            }
            return ToReferrer();

        }

    }
}