﻿using System.Web;
using System.Web.Optimization;

namespace Kodlaborate.Web {
    public class BundleConfig {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles) {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/custom").Include(
                        "~/Scripts/custom/site.js",
                        "~/Scripts/custom/modal.js"
						));

            bundles.Add(new ScriptBundle("~/bundles/msajax")
                .Include("~/Scripts/jquery.validate*")
                .Include("~/Scripts/jquery.unobtrusive-ajax.min.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
			//bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
			//			"~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.min.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/codeparser")
                .Include("~/Scripts/code/codemirror.js")
                );

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/codemirror.css",
                      "~/Content/bootstrap.yeti.min.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/admincss").Include(
                      "~/Content/codemirror.css",
                      "~/DashboardContent/plugins/bootstrap/bootstrap.css",
                      "~/DashboardContent/plugins/pace/pace-theme-big-counter.css",
                      "~/DashboardContent/css/style.css",
                      "~/DashboardContent/css/main-style.css",
                      "~/DashboardContent/plugins/social-buttons/social-buttons.css",
                      "~/DashboardContent/plugins/timeline/timeline.css",
                      "~/Content/site.css"));

        }
    }
}
