﻿using LLF.Annotations.CustomMVCFilters;
using System.Web;
using System.Web.Mvc;

namespace Kodlaborate.Web {
    public class FilterConfig {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters) {
            filters.Add(new HandleErrorAttribute());
            //filters.Add(new AjaxErrorHandlerAttribute());
        }
    }
}
