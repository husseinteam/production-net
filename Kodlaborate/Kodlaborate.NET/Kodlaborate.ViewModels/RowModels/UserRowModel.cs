﻿using Kodlaborate.CoreData.EntityObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Extensions;
using Kodlaborate.ViewModels.PageModels;
using Kodlaborate.ViewModels.Protocols;
using Kodlaborate.CoreData.Enums;

namespace Kodlaborate.ViewModels.RowModels {

    public class UserRowModel : IdentityModel, IGenericViewModel {

        public UserRowModel(Author auth) 
            : base(auth) {

            this.LinkCount = auth.CodeLinks.Count;
            this.StatusLabelClass = auth.MembershipStatus.GetDescription();
            this.StatusText = auth.MembershipStatus.GetStatusText();
            this.GeneratedOn = auth.GeneratedOn;

        }

        public UserRowModel() {

        }

        public Int32 LinkCount { get; internal set; }

        public String StatusText { get; set; }

        public String StatusLabelClass { get; set; }

        public DateTime GeneratedOn { get; set; }

        #region IGenericViewModel Members

        public virtual IGenericViewModel SetContent(int id) {

            return this;

        }

        #endregion
    }

}

