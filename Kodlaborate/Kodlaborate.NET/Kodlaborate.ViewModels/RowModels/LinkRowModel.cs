﻿using Kodlaborate.CoreData.EntityObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Extensions;
using Kodlaborate.CoreData.Enums;

namespace Kodlaborate.ViewModels.RowModels {

    public class LinkRowModel {

        public LinkRowModel(CodeLink li) {

            li.Include<CodeBlock>();
            this.Token = li.Token;
            this.CodeLinkID = li.ID;
            this.ClickCount = li.ClickCount;
            this.CodeBlockCount = li.CodeBlocks.Count;
            this.TitlesSummary = String.Join(", ", li.CodeBlocks.Unique(cb => cb.Title)
                .Select(cb => cb.Title)).Summary(25);
            this.PublicUrl = "";
            this.LinkStatus = li.LinkStatus;
            this.StatusLabelClass = this.LinkStatus.GetDescription();
            this.StatusText = this.LinkStatus.GetStatusText();
            this.GeneratedOn = li.GeneratedOn;

        }

        public DateTime GeneratedOn { get; set; }
        public ELinkStatus LinkStatus { get; set; }
        public String Token { get; private set; }
        public Int64 CodeLinkID { get; set; }
        public int ClickCount { get; set; }
        public String TitlesSummary { get; set; }
        public String PublicUrl { get; set; }
        public int CodeBlockCount { get; set; }
        public String StatusText { get; set; }
        public String StatusLabelClass { get; set; }

    }

}

