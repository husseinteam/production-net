﻿using Kodlaborate.CoreData.EntityObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Extensions;

namespace Kodlaborate.ViewModels.RowModels {

    public class UserMessageRowModel {

        public UserMessageRowModel(UserMessage msg) {

            this.SentOn = msg.SentOn.ToString("dd MMMM yyyy");
            this.Sender = msg.ContactEmail;
            this.SubjectSummary = msg.Subject.Summary(66);
            this.SenderLabelClass = msg.ColorClass.GetDescription();
        }

        public String SentOn { get; private set; }
        public String Sender { get; private set; }
        public String SubjectSummary { get; private set; }
        public string SenderLabelClass { get; private set; }

    }
}
