﻿using Kodlaborate.CoreData.EntityObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Extensions;
using Kodlaborate.Resources.Locals;

namespace Kodlaborate.ViewModels.RowModels {

    public class ToDoTaskRowModel {

        public ToDoTaskRowModel(ToDoTask task) {

            this.Title = task.Title;
            this.Percent = (task.CompletionRatio * 100).ToType<Int32>();
            this.CompletionSentence = "%{0} {1}".PostFormat(this.Percent, AdminContent.CompletionTerm);
            this.LabelClass = task.TaskClass.GetDescription();

        }

        public string Title { get; internal set; }
        public string CompletionSentence { get; internal set; }
        public string LabelClass { get; internal set; }
        public int Percent { get; set; }

    }

}
