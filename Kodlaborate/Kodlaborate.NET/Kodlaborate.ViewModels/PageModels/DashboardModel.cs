﻿using Kodlaborate.CoreData.EntityObjects;
using Kodlaborate.CoreData.StaticObjects;
using Kodlaborate.Resources.Locals;
using Kodlaborate.ViewModels.Engine;
using Kodlaborate.ViewModels.PageModels.Base;
using Kodlaborate.ViewModels.RowModels;
using Kodlaborate.CoreData.Enums;
using LLF.Annotations.ViewModel;
using LLF.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Kodlaborate.ViewModels.Protocols;

namespace Kodlaborate.ViewModels.PageModels {

    [LocalResource(typeof(PageViewModels))]
    public class DashboardModel : AdminPanelModel, IGenericViewModel {

        public override IGenericViewModel SetContent(int id) {

            base.SetContent(id);
            this.PendingUserMessageRows = SAdminEngine.GetUserMessagesBy(EReplyStatus.Pending);

            this.DailyLinkAdded = SAppConfig.GetConfigValue<Int32>(EAppConfig.DailyLinkAdded);
            this.DailyUserVisitCount = SAppConfig.GetConfigValue<Int32>(EAppConfig.DailyUserVisitCount);
            this.MonthlyUserRegistrationCount = SAppConfig.GetConfigValue<Int32>(EAppConfig.MonthlyUserRegistrationCount);
            return this;

        }


        public int DailyLinkAdded { get; internal set; }
        public int DailyUserVisitCount { get; internal set; }
        public int MonthlyUserRegistrationCount { get; internal set; }
        public IEnumerable<UserMessageRowModel> PendingUserMessageRows { get; internal set; }

        public override string PageTitle {
            get { return PageTitles.Dashboard; }
        }

    }

}
