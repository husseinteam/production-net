﻿using Kodlaborate.CoreData.StaticObjects;
using Kodlaborate.Resources.Locals;
using Kodlaborate.ViewModels.PageModels.Base;
using LLF.Annotations.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kodlaborate.ViewModels.PageModels {

    [LocalResource(typeof(PageViewModels))]
    public class LoginModel : RecoverModel {

        #region ToUI

        [ToUI(ValidationErrorKey = "PasswordNullError")]
        [ApplyRegex("Password", "PasswordValidationError")]
        public String Password { get; set; }

        #endregion

        public override string PageTitle {
            get { return PageTitles.Login; }
        }
    }

}
