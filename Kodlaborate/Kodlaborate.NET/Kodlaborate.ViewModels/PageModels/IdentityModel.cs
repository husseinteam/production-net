﻿using Kodlaborate.CoreData.EntityObjects;
using Kodlaborate.CoreData.StaticObjects;
using Kodlaborate.Resources.Locals;
using Kodlaborate.ViewModels.PageModels.Base;
using LLF.Annotations.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kodlaborate.ViewModels.PageModels {

    [LocalResource(typeof(PageViewModels))]
    public class IdentityModel : LoginModel {

        #region Ctor

        public IdentityModel(Author author) {

            this.Identity = author.Identity;
            this.LastName = author.Identity.Split(' ').Last();
            this.FirstName = author.Identity.Replace(this.LastName, "").Trim();
            this.Email = author.Email;
            this.AuthorID = author.ID;

        }

        public IdentityModel() {
                
        }

        #endregion

        #region ToUI

        [ToUI(ValidationErrorKey = "IdentityNullError")]
        [ApplyRegex("Identity", "IdentityValidationError")]
        public String Identity { get; set; }

        [ToUI(ValidationErrorKey = "PasswordRepeatNullError")]
        [ApplyRegex("Password", "PasswordValidationError")]
        public String PasswordRepeat { get; set; }

        #endregion

        public String FirstName { get; set; }
        public String LastName { get; set; }
        public Int64 AuthorID { get; set; }

        public override string PageTitle {
            get { return PageTitles.Register; }
        }
    }

}
