﻿using Kodlaborate.CoreData.EntityObjects;
using Kodlaborate.CoreData.StaticObjects;
using Kodlaborate.Resources.Locals;
using Kodlaborate.ViewModels.Engine;
using Kodlaborate.ViewModels.PageModels.Base;
using Kodlaborate.ViewModels.Protocols;
using Kodlaborate.ViewModels.RowModels;
using LLF.Annotations.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Kodlaborate.ViewModels.PageModels {

    [LocalResource(typeof(PageViewModels))]
    public abstract class AdminPanelModel : UserModelBase, IGenericViewModel {

        public virtual IGenericViewModel SetContent(Int32 id) {
            
            this.UserMessageRows = SAdminEngine.GetAllUserMessages();
            this.ToDoTaskRows = SAdminEngine.GetToDoTasksOf(id);

            var identity = SAuthEngine.CurrentIdentity(id);
            this.FirstName = identity.FirstName;
            this.LastName = identity.LastName;

            this.UserGrid = new UserGridModel().SetContent(id) as UserGridModel;
            this.PublicLink = new PublicLinkModel().SetContent(id) as PublicLinkModel;

            return this;

        }

        public String FirstName { get; set; }
        public String LastName { get; set; }

        public UserGridModel UserGrid { get; set; }
        public PublicLinkModel PublicLink { get; set; }
        public IEnumerable<UserMessageRowModel> UserMessageRows { get; set; }
        public IEnumerable<ToDoTaskRowModel> ToDoTaskRows { get; set; }

        public abstract override string PageTitle { get; }
    }

}
