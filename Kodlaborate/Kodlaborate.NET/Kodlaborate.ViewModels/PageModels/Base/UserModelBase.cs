﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kodlaborate.ViewModels.PageModels.Base {

    public abstract class UserModelBase : PageViewModelBase {

        #region Forced

        public override abstract String PageTitle { get; }

        #endregion

    }

}
