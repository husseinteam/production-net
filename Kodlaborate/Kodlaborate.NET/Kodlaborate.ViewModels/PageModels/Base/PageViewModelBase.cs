﻿using Kodlaborate.CoreData.StaticObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kodlaborate.ViewModels.PageModels.Base {

    public abstract class PageViewModelBase {

        #region Ctor
        public PageViewModelBase() {
            Response = new ResponseObject();
        }
        #endregion

        #region Inherited

        public ResponseObject Response { get; set; }

        #endregion

        #region Forced

        public abstract String PageTitle { get; }

        #endregion

    }

}
