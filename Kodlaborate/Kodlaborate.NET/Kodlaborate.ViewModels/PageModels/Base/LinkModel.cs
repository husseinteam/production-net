﻿using Kodlaborate.CoreData.EntityObjects;
using Kodlaborate.Resources.Locals;

using Kodlaborate.CoreData.StaticObjects;
using LLF.Annotations.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kodlaborate.ViewModels.PageModels;
using Newtonsoft.Json;
using Kodlaborate.ViewModels.RowModels;
using Kodlaborate.ViewModels.PageModels.Base;

namespace Kodlaborate.ViewModels.PageModels {

    [LocalResource(typeof(PageViewModels))]
    public abstract class LinkModel : UserModelBase {

        #region ToUI

        [ToUI(ValidationErrorKey = "TokenNullError")]
        public String Token { get; set; }

        public String PublicUrl { get; set; }

        public Int32 ClickCount { get; set; }

        #endregion

        public abstract override String PageTitle { get; }

    }
}
