﻿using Kodlaborate.CoreData.EntityObjects;
using Kodlaborate.Resources.Locals;

using Kodlaborate.CoreData.StaticObjects;
using LLF.Annotations.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kodlaborate.ViewModels.PageModels;
using Newtonsoft.Json;
using Kodlaborate.ViewModels.RowModels;

namespace Kodlaborate.ViewModels.PageModels {

    [LocalResource(typeof(PageViewModels))]
    public class DisplayLinkModel : CodeBlockModel {

        public new String Token { get; set; }

        public override String PageTitle {
            get { return PageTitles.DisplayLink; }
        }

    }
}
