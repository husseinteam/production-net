﻿using Kodlaborate.CoreData.StaticObjects;
using Kodlaborate.Resources.Locals;
using Kodlaborate.ViewModels.PageModels.Base;
using LLF.Annotations.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Kodlaborate.ViewModels.PageModels {

    [LocalResource(typeof(PageViewModels))]
    public class RecoverModel : UserModelBase {

        #region ToUI

        [ToUI(ValidationErrorKey = "EmailNullError")]
        [ApplyRegex("Email", "EmailValidationError")]
        public String Email { get; set; }

        #endregion

        #region NonUI

        public String MailingUserName { get; set; }
        public String MailingPassword { get; set; }
        public MailAddress FromAddress { get; set; }
        public String ResetLink { get; set; }
        public String RecoveryLink { get; set; }
        public String TermsLink { get; set; }
        public String PrivacyLink { get; set; }
        public String UnsubscribeLink { get; set; }
        public String CompanyContactPhone { get; set; }
        public String CompanyContactEmail { get; set; }
        public String SiteName { get; set; }
        public String Sitelogo { get; set; }

        public string AddressLine2 { get; set; }
        public string AddressLine1 { get; set; }
        public string CopyrightTerm { get; set; }
        public string SiteUrl { get; set; }
        public string SmtpServer { get; set; }

        #endregion

        public override string PageTitle {
            get { return PageTitles.Recover; }
        }

    }

}
