﻿using Kodlaborate.CoreData.StaticObjects;
using Kodlaborate.Resources.Locals;
using Kodlaborate.ViewModels.PageModels.Base;
using LLF.Annotations.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Kodlaborate.ViewModels.PageModels {

    [LocalResource(typeof(PageViewModels))]
    public class RepassModel : LinkModel {

        #region ToUI

        [ToUI(ValidationErrorKey = "PasswordNullError")]
        [ApplyRegex("Password", "PasswordValidationError")]
        public String Password { get; set; }

        [ToUI(ValidationErrorKey = "PasswordRepeatNullError")]
        [ApplyRegex("Password", "PasswordValidationError")]
        public String PasswordRepeat { get; set; }

        #endregion

        public override string PageTitle {
            get { return PageTitles.Reset; }
        }
    }

}
