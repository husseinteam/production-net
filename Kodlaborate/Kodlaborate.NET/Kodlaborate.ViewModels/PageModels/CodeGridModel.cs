﻿using Kodlaborate.CoreData.StaticObjects;
using Kodlaborate.Resources.Locals;
using Kodlaborate.ViewModels.Engine;
using Kodlaborate.ViewModels.PageModels.Base;
using Kodlaborate.ViewModels.RowModels;
using LLF.Annotations.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Kodlaborate.ViewModels.PageModels {

    [LocalResource(typeof(PageViewModels))]
    public class CodeGridModel : UserModelBase {

        public CodeGridModel SetLinkRows(int authorID, Action<LinkRowModel> rowModifier) {

            this.CodeLinkRows = SCodeEngine.GetCodeLinkRows(authorID, rowModifier);
            return this;

        }

        public CodeGridModel() {
            this.CodeLinkRows = new List<LinkRowModel>().ToArray();
        }

        public override string PageTitle {
            get { return PageTitles.CodeGrid; }
        }

        public IEnumerable<LinkRowModel> CodeLinkRows { get; set; }
    }

}
