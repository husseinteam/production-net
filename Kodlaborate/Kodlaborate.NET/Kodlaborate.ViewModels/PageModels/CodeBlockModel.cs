﻿using Kodlaborate.CoreData.EntityObjects;
using Kodlaborate.Resources.Locals;

using Kodlaborate.CoreData.StaticObjects;
using LLF.Annotations.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kodlaborate.ViewModels.PageModels;
using Newtonsoft.Json;
using Kodlaborate.ViewModels.RowModels;
using Kodlaborate.ViewModels.PageModels.Base;
using Kodlaborate.ViewModels.Engine;

namespace Kodlaborate.ViewModels.PageModels {

    [LocalResource(typeof(PageViewModels))]
    public class CodeBlockModel : LinkModel {

        #region Ctor

        public CodeBlockModel() {

            Languages = SCodeEngine.AllLanguages();

        }

        public CodeBlockModel(CodeBlock cb)
            : this() {

            this.Title = cb.Title;
            this.BlockText = cb.Text;
            this.SelectedLanguageID = cb.CodeLanguage.ID;
            this.SelectedLanguage = cb.CodeLanguage;
            this.ID = cb.ID;

        }

        #endregion

        #region ToUI

        [ToUI(ValidationErrorKey = "TitleNullError")]
        public String Title { get; set; }

        [ToUI(ValidationErrorKey = "CodeBlockNullError")]
        public String BlockText { get; set; }

        [ToUI(ValidationErrorKey = "LanguageNotSelectedError")]
        public Int64 SelectedLanguageID { get; set; }

        public CodeLanguage SelectedLanguage { get; set; }

        #endregion

        public override String PageTitle {
            get { return PageTitles.GenerateLink; }
        }

        public Int64 ID { get; private set; }

        [JsonIgnore]
        public IEnumerable<CodeLanguage> Languages { get; set; }

    }
}
