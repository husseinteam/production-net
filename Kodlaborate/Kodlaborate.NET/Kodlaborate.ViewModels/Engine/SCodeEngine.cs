﻿using Kodlaborate.CoreData.StaticObjects;
using Kodlaborate.ViewModels.PageModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kodlaborate.ViewModels.Extensions;
using LLF.Data.Containers;
using LLF.Extensions;
using Kodlaborate.CoreData.EntityObjects;
using Kodlaborate.Resources.Locals;
using Kodlaborate.ViewModels.RowModels;
using Kodlaborate.CoreData.Enums;
using LLF.Data.Tools;

namespace Kodlaborate.ViewModels.Engine {

    public static class SCodeEngine {

        public static void AddPublicLink(CodeBlockModel vm) {

            var validated = vm.Validate();
            if (validated.IsValid) {

                var clang = DbProvider.ProvideEntity<CodeLanguage>().SelectThe(vm.SelectedLanguageID);
                var clnk = DbProvider.ProvideEntity<CodeLink>().SelectOne(new { Token = vm.Token });
                if (clnk == null) {
                    clnk = new CodeLink() {
                        ClickCount = 0,
                        Token = vm.Token,
                        LinkStatus = ELinkStatus.Online
                    };
                    var exc = clnk.InsertSelf().Fault;
                    if (exc != null) {
                        vm.Response.Success = false;
                        vm.Response.Message = exc.Message;
                        return;
                    } else {
                        SAppConfig.ResetCrement(DateTime.Now, EAppConfig.DailyLinkAdded);
                    }
                }
                clnk.Include<CodeBlock>().CodeBlocks.Extend(new CodeBlock() {
                    Text = vm.BlockText,
                    CodeLanguage = clang,
                    Title = vm.Title
                });
                var exc2 = clnk.UpsertCollections<CodeBlock>().Fault;
                if (exc2 == null) {
                    vm.Response.Success = true;
                    vm.Response.Message = Messages.SuccessFullAddCodeBlockToLinkFormat.PostFormat(clnk.Token);
                } else {
                    vm.Response.Success = false;
                    vm.Response.Message = exc2.Message;
                }
            } else {
                vm.Response.Success = false;
                vm.Response.Message = validated.FinalMessage;
            }

        }

        public static void SetCodeBlocksFor(DisplayLinkModel vm) {

            var link = DbProvider.ProvideEntity<CodeLink>().SelectOne(new { Token = vm.Token });
            if (link != null) {
                if (link.LinkStatus == ELinkStatus.Online) {
                    link.Include<CodeBlock>();
                    if (link.CodeBlocks.Count > 0) {
                        vm.Response.Data = link.CodeBlocks.Select(cb => new CodeBlockModel(cb)).ToArray();
                        vm.Response.Success = true;
                    } else {
                        vm.Response.Success = false;
                        vm.Response.Message = Messages.LinkWithTokenDoesNotHaveAnyCode.PostFormat(link.Token);
                    }
                } else {
                    vm.Response.Success = false;
                    vm.Response.Message = Messages.LinkStatusIs.PostFormat(link.LinkStatus.GetStatusText());
                }
            } else {
                vm.Response.Success = false;
                vm.Response.Message = Messages.TokenIsNonregisteredFormat.PostFormat(vm.Token);
            }

        }

        public static void IncrementClickCount(DisplayLinkModel vm, Int32 authorID) {

            var clink = DbProvider.ProvideEntity<CodeLink>().SelectOne(new { Token = vm.Token });

            if (clink != null) {
                var existingAuthor = DbProvider.ProvideEntity<Author>().SelectThe(authorID);
                if (existingAuthor != null) {
                    if (clink.Owner != null && clink.Owner.ID == existingAuthor.ID) {
                        return;
                    }
                } else {
                    clink.ClickCount++;
                    clink.UpdateSelf();
                    vm.Response.Success = clink.Fault == null;
                }
            } else {
                vm.Response.Success = false;
            }
        }

        public static void SetNewCodeBlock(Int32 authorID, CodeBlockModel vm, Action<CodeBlockModel> rowModifier) {

            var validated = vm.Validate();
            if (validated.IsValid) {

                var author = DbProvider.ProvideEntity<Author>().SelectThe(authorID);
                var clnk = DbProvider.ProvideEntity<CodeLink>().SelectOne(new { Token = vm.Token });
                if (clnk == null) {
                    clnk = new CodeLink()
                        .Assign(cl => cl.Token = vm.Token)
                        .Assign(cl => cl.ClickCount = 0)
                        .Assign(cl => cl.Owner = author)
                        .Assign(cl => cl.LinkStatus = ELinkStatus.Online);
                    clnk.InsertSelf();
                    SAppConfig.ResetCrement(DateTime.Now, EAppConfig.DailyLinkAdded);
                }
                var clang = DbProvider.ProvideEntity<CodeLanguage>().SelectThe(vm.SelectedLanguageID);
                clnk.CodeBlocks.Extend(new CodeBlock() {
                    Text = vm.BlockText,
                    CodeLanguage = clang,
                    Title = vm.Title
                });
                clnk.Include<CodeBlock>();

                if (author != null) {
                    author.CodeLinks.Add(clnk);
                    author.Include<CodeLink>();
                }

                if (clnk.UpdateSelf().UpsertCollections<CodeBlock>().Fault == null) {
                    if (author.UpdateSelf().UpsertCollections<CodeLink>().Fault == null) {
                        vm.Response.Success = true;
                        vm.Response.Message = Messages.AddLinkSuccessfulFormat.PostFormat(author.Identity);
                        vm.Response.Data = clnk.CodeBlocks.Select(cb => {
                            var row = new CodeBlockModel(cb);
                            rowModifier(row);
                            return row;
                        }).ToArray();
                    } else {
                        vm.Response.Success = false;
                        vm.Response.Message = author.Fault.Message;
                    }
                } else {
                    vm.Response.Success = false;
                    vm.Response.Message = clnk.Fault.Message;
                }
            } else {
                vm.Response.Success = false;
                vm.Response.Message = validated.FinalMessage;
            }

        }

        public static void ExtendLinkForAuthor(int authorID, CodeBlockModel vm, Action<CodeBlockModel> rowModifier) {

            var validated = vm.Validate();
            if (validated.IsValid) {
                var author = DbProvider.ProvideEntity<Author>().SelectThe(authorID).Include<CodeLink>();
                var clang = DbProvider.ProvideEntity<CodeLanguage>().SelectThe(vm.SelectedLanguageID);
                var clnk = author.CodeLinks.SingleOrDefault(cl => cl.Token == vm.Token);
                if (clnk != null) {
                    clnk.CodeBlocks.Extend(new CodeBlock() {
                        Text = vm.BlockText,
                        CodeLanguage = clang,
                        Title = vm.Title
                    });
                    clnk.Include<CodeBlock>();
                    clnk.UpdateSelf().UpsertCollections<CodeBlock>();
                    if (clnk.Fault == null) {
                        vm.Response.Success = true;
                        vm.Response.Message = Messages.SuccessFullAddCodeBlockToLinkFormat.PostFormat(clnk.Token);
                        rowModifier(vm);
                        vm.Response.Data = clnk.CodeBlocks.Select(cb => {
                            var row = new CodeBlockModel(cb);
                            rowModifier(row);
                            return row;
                        });
                    } else {
                        vm.Response.Success = false;
                        vm.Response.Message = clnk.Fault.Message;
                    }
                } else {
                    vm.Response.Success = false;
                    vm.Response.Message = Messages.TokenDoesNotExist;
                }
            } else {
                vm.Response.Success = false;
                vm.Response.Message = validated.FinalMessage;
            }

        }

        public static CodeGridModel GenerateCodeGrid(int authorID, Action<LinkRowModel> rowModifier) {

            var vm = new CodeGridModel();
            vm.Response.Success = true;
            vm.Response.Data = GetCodeLinkRows(authorID, rowModifier);
            return vm;

        }

        internal static IEnumerable<LinkRowModel> GetCodeLinkRows(int authorID, Action<LinkRowModel> rowModifier) {

            var auth = DbProvider.ProvideEntity<Author>().SelectThe(authorID);
            if (auth != null) {
                auth.Include<CodeLink>();
            }
            return auth.CodeLinks.Select(cl => {
                var row = new LinkRowModel(cl);
                rowModifier(row);
                return row;
            });

        }


        public static void ModifyCodeBlock(Int64 blockID, DisplayLinkModel vm, Action<CodeBlockModel> rowModifier) {

            var validated = vm.Validate();
            if (validated.IsValid) {
                var cblck = DbProvider.ProvideEntity<CodeBlock>().SelectThe(blockID);
                var clang = DbProvider.ProvideEntity<CodeLanguage>().SelectThe(vm.SelectedLanguageID);

                cblck
                    .Assign(cb => cb.CodeLanguage = clang)
                    .Assign(cb => cb.Text = vm.BlockText)
                    .Assign(cb => cb.Title = vm.Title);

                if (cblck.UpdateSelf().Fault == null) {
                    vm.Response.Success = true;
                    vm.Response.Message = Messages.SuccessFullModifyCodeBlockToLinkFormat.PostFormat(vm.Token);
                    rowModifier(vm);
                } else {
                    vm.Response.Success = false;
                    vm.Response.Message = cblck.Fault.Message;
                }
            } else {
                vm.Response.Success = false;
                vm.Response.Message = validated.FinalMessage;
            }

        }

        public static void DeleteCodeBlock(Int64 blockID, CodeBlockModel vm) {

            var cblck = DbProvider.ProvideEntity<CodeBlock>().SelectThe(blockID);
            cblck.DeleteSelf();
            if (cblck.Fault == null) {
                vm.Response.Success = true;
                vm.Response.Data = blockID;
                vm.Response.Message = Messages.DeleteCodeBlockSuccess.PostFormat(vm.Token);
            } else {
                vm.Response.Success = false;
                vm.Response.Message = cblck.Fault.Message;
            }

        }

        public static void Tokenize(LinkModel vm, Action<LinkModel> modifier) {

            vm.Token = "{0}".GenerateToken();
            modifier(vm);
            vm.Response.Success = true;

        }

        public static DisplayLinkModel GetLinkOf(int authorID, String token, Action<CodeBlockModel> rowModifier) {

            var auth = DbProvider.ProvideEntity<Author>().SelectThe(authorID)
                .Include<CodeLink>();
            var clink = auth.CodeLinks.SingleOrDefault(c => c.Token == token);
            var vm = new DisplayLinkModel();
            vm.Token = token;
            rowModifier(vm);
            if (clink != null) {
                clink.Include<CodeBlock>();
                vm.Response.Success = true;
                vm.Response.Data = clink.CodeBlocks.Select(cb => {
                    var row = new CodeBlockModel(cb);
                    rowModifier(row);
                    return row;
                }).ToArray();
            } else {
                vm.Response.Success = false;
                vm.Response.Message = Messages.LinkWithTokenDoesNotHaveAnyCode.PostFormat(token);
            }
            return vm;

        }

        public static void RemoveLink(Int32 clinkID, DisplayLinkModel vm) {

            var clink = DbProvider.ProvideEntity<CodeLink>().SelectThe(clinkID);
            clink.DeleteSelf();

            if (clink.Fault == null) {
                vm.Response.Success = true;
                vm.Response.Message = Messages.DeleteCodeLinkSuccess.PostFormat(vm.Token);
            } else {
                vm.Response.Success = false;
                vm.Response.Message = clink.Fault.Message;
            }

        }

        internal static IEnumerable<CodeLanguage> AllLanguages() {

            return DbProvider.ProvideEntity<CodeLanguage>().SelectMany().ToList();

        }

        public static IEnumerable<String> GetAllTokens() {

            return DbProvider.ProvideEntity<CodeLink>().SelectMany().Select(cl => cl.Token).ToArray();

        }


        public static void DeleteAllCodesOf(int authorID, CodeGridModel vm) {

            var auth = DbProvider.ProvideEntity<Author>().SelectThe(authorID);
            if (auth != null) {
                auth.Include<CodeLink>();
            }
            var success = true;
            auth.CodeLinks.ToList().ForEach(cl =>
                success = cl.DeleteSelf().Fault != null ? false : success);
            vm.Response.Success = success;
            vm.Response.Message = success ? Messages.DeleteCodeLinkSuccess : Messages.DeleteCodeLinkFailed;

        }
    }

}
