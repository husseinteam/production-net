﻿using Kodlaborate.CoreData.StaticObjects;
using Kodlaborate.ViewModels.PageModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kodlaborate.ViewModels.Extensions;
using Kodlaborate.CoreData.Enums;
using LLF.Data.Containers;
using LLF.Extensions;
using Kodlaborate.CoreData.EntityObjects;
using Kodlaborate.Resources.Locals;
using System.Net.Mail;

using System.Net;
using Kodlaborate.ViewModels.RowModels;
using LLF.Data.Tools;

namespace Kodlaborate.ViewModels.Engine {
    public static class SAuthEngine {

        public static void TryLogin(LoginModel vm) {

            var validated = vm.Validate();
            if (validated.IsValid) {
                var hash = vm.Password.ComputeHash();
                Func<Author, Boolean> validator = (author) =>
                    author.Email == vm.Email && author.PasswordHash == hash;
                var existing = DbProvider.ProvideEntity<Author>().SelectOne(new { Email = vm.Email, PasswordHash = hash });
                if (existing != null) {
                    vm.Response.Success = true;
                    vm.Response.Message = Messages.WelcomeAmongUs.PostFormat(existing.Identity);
                    vm.Response.Data = existing.ID;
                } else {
                    vm.Response.Success = false;
                    vm.Response.Message = Messages.LoginFailed;
                }
            } else {
                vm.Response.Success = false;
                vm.Response.Message = validated.FinalMessage;
            }

        }

        public static void TryRegister(IdentityModel vm) {

            var validated = vm.Validate();
            if (validated.IsValid) {
                var author = new Author() {
                    Email = vm.Email,
                    Identity = vm.Identity,
                    PasswordHash = vm.Password.ComputeHash(),
                    IsAdministrator = false,
                    PasswordRecoveryToken = Guid.Empty.ToString()
                };
                if (DbProvider.ProvideEntity<Author>().SelectOne(new { Email = author.Email }) != null) {
                    vm.Response.Success = false;
                    vm.Response.Message = Messages.EmailTaken.PostFormat(author.Email);
                } else {
                    author.InsertSelf();
                    if (author.Fault == null) {
                        vm.Response.Success = true;
                        vm.Response.Message = Messages.WelcomeAmongUs.PostFormat(author.Identity);
                    } else {
                        vm.Response.Success = true;
                        vm.Response.Message = author.Fault.Message;
                    }
                }
            } else {
                vm.Response.Success = false;
                vm.Response.Message = validated.FinalMessage;
            }

        }

        public static async Task TryRecover(RecoverModel vm) {

            var validated = vm.Validate();
            if (validated.IsValid) {

                try {
                    var existing = DbProvider.ProvideEntity<Author>().SelectOne(new { Email = vm.Email });
                    if (existing != null) {
                        var to = new MailAddressCollection().Assign(coll => coll.Add(existing.Email));
                        var subject = SMailEngine.ComposeMailSubjectForRecovery(existing, vm.SiteName);
                        var body = SMailEngine.ComposeMailBodyForRecovery(existing, vm);
                        await SMailEngine.SendAsync(vm, new NetworkCredential(vm.MailingUserName, vm.MailingPassword), vm.FromAddress, to, subject, body);
                    } else {
                        vm.Response.Success = false;
                        vm.Response.Message = Messages.RecoverFailed.PostFormat(vm.Email);
                    }
                } catch (Exception exc) {
                    vm.Response.Success = false;
                    vm.Response.Message = exc.Message;
                }
            } else {
                vm.Response.Success = false;
                vm.Response.Message = validated.FinalMessage;
            }

        }

        public static String GenerateResetQueryString(Author author, String resetLink) {

            if (author != null) {
                author.PasswordRecoveryToken = Guid.NewGuid().ToString();
                author.UpdateSelf();
                if (author.Fault == null) {
                    return "{0}/{1}".PostFormat(resetLink, author.PasswordRecoveryToken);
                } else {
                    return null;
                }
            } else {
                return null;
            }

        }


        public static void TryRepassPassword(RepassModel vm) {
            var validated = vm.Validate();
            if (validated.IsValid) {
                Guid guid = Guid.Empty;
                if (Guid.TryParse(vm.Token, out guid)) {
                    try {
                        var existing = DbProvider.ProvideEntity<Author>().SelectOne(new { PasswordRecoveryToken = guid.ToString() });
                        if (existing != null && existing.PasswordRecoveryToken != Guid.Empty.ToString()) {
                            existing.PasswordHash = vm.Password.ComputeHash();
                            existing.PasswordRecoveryToken = Guid.Empty.ToString();
                            existing.UpdateSelf();
                            vm.Response.Success = existing.Fault == null;
                            vm.Response.Message = vm.Response.Success ? Messages.RepassDone.PostFormat(existing.Email) : Messages.RepassFailed.PostFormat(existing.Email);
                        } else {
                            vm.Response.Success = false;
                            vm.Response.Message = Messages.RepassExpired;
                        }

                    } catch (Exception exc) {
                        vm.Response.Success = false;
                        vm.Response.Message = exc.Message;
                    }
                }
            } else {
                vm.Response.Success = false;
                vm.Response.Message = validated.FinalMessage;
            }
        }

        public static void ValidateRepass(RepassModel vm) {

            Guid guid = Guid.Empty;

            if (Guid.TryParse(vm.Token, out guid)) {
                try {
                    var existing = DbProvider.ProvideEntity<Author>().SelectOne(new { PasswordRecoveryToken = guid.ToString() });
                    if (existing != null) {
                        vm.Response.Success = true;
                    } else {
                        vm.Response.Success = false;
                        vm.Response.Message = Messages.PasswordResetTokenIsNotLegal;
                    }
                } catch (Exception exc) {
                    vm.Response.Success = false;
                    vm.Response.Message = exc.Message;
                } 
            }

        }

        public static void ModifyProfile(Int64 authorID, IdentityModel vm) {

            var validated = vm.Validate();
            if (validated.IsValid) {

                var auth = DbProvider.ProvideEntity<Author>().SelectThe(authorID);

                auth.Identity = vm.Identity;
                auth.PasswordHash = vm.Password.ComputeHash();

                auth.UpdateSelf();
                if (auth.Fault == null) {
                    vm.Response.Success = true;
                    vm.Response.Message = Messages.ProfileChangeSavedFormat.PostFormat(auth.Identity);
                } else {
                    vm.Response.Success = false;
                    vm.Response.Message = auth.Fault.Message;
                }
            } else {
                vm.Response.Success = false;
                vm.Response.Message = validated.FinalMessage;
            }

        }


        public static IdentityModel CurrentIdentity(int authorID) {

            return new IdentityModel(DbProvider.ProvideEntity<Author>().SelectThe(authorID));

        }

        public static Boolean IsAuthorAdministrator(int authorID) {

            return DbProvider.ProvideEntity<Author>().SelectThe(authorID).IsAdministrator;

        }

        public static Author GetAuthorByUserName(string userName) {

            return DbProvider.ProvideEntity<Author>().SelectOne(new { Email = userName }).Include<ToDoTask>().Include<CodeLink>();

        }

        public static IEnumerable<UserRowModel> GetUserRowsBy(EMembershipStatus status) {

            Func<Author, Boolean> selector = status == EMembershipStatus.Any ? new Func<Author, Boolean>((auth) => true) :
                new Func<Author, Boolean>((auth) => auth.MembershipStatus == status);
            return DbProvider.ProvideEntity<Author>().SelectMany()
                .Where(selector).ToList().Select(cl => new UserRowModel(cl)).ToArray();

        }

        public static void ChangeMembershipStatus(UserGridModel vm, Int32 authorID, EMembershipStatus status) {

            var existing = DbProvider.ProvideEntity<Author>().SelectThe(authorID);
            if (existing != null) {
                existing.MembershipStatus = status;
                existing.UpdateSelf();
                if (existing.Fault == null) {
                    vm.Response.Success = true;
                    vm.Response.Message = Messages.MembershipStatusSuccessfullyChanged;
                } else {
                    vm.Response.Success = true;
                    vm.Response.Message = Messages.MembershipStatusChangeFailed;
                }
            } else {
                vm.Response.Success = true;
                vm.Response.Message = Messages.MembershipStatusNoSuchUser;
            }

        }

        public static void ChangeMembershipStatusForAll(UserGridModel vm, EMembershipStatus changeWithStatus) {

            var found = DbProvider.ProvideEntity<Author>().SelectMany().ToList();
            Exception exc = null;
            foreach (var auth in found) {
                auth.MembershipStatus = changeWithStatus;
                exc = exc == null ? auth.UpdateSelf().Fault : exc;
            }

            if (exc == null) {
                vm.Response.Success = true;
                vm.Response.Message = Messages.MembershipStatusSuccessfullyChanged;
            } else {
                vm.Response.Success = true;
                vm.Response.Message = Messages.MembershipStatusChangeFailed;
            }

        }
    }
}
