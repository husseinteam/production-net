﻿using Kodlaborate.CoreData.EntityObjects;
using Kodlaborate.ViewModels.RowModels;
using Kodlaborate.CoreData.Enums;
using LLF.Data.Containers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Extensions;
using LLF.Data.Tools;

namespace Kodlaborate.ViewModels.Engine {

    public static class SAppConfig {



        internal static Int32 DailyUserVisitCount() {

            return DbProvider.ProvideEntity<AppConfigItem>()
                .SelectOne(new { AppConfigType = EAppConfig.DailyUserVisitCount }).Value.ToType<Int32>();

        }

        internal static Int32 MonthlyUserRegistrationCount() {

            return DbProvider.ProvideEntity<AppConfigItem>()
                .SelectOne(new {
                    AppConfigType =
                        EAppConfig.MonthlyUserRegistrationCount
                }).Value.ToType<Int32>();
        }

        public static void ResetCrement(DateTime now, EAppConfig config) {

            var cfgValue = config.GetValueMatch();
            var existingValue = DbProvider.ProvideEntity<AppConfigItem>()
                .SelectOne(new { AppConfigType = cfgValue });
            if (existingValue != null) {
                var lastDate = existingValue.Value.ToType<DateTime>();
                var existing = DbProvider.ProvideEntity<AppConfigItem>()
                    .SelectOne(new { AppConfigType = config });
                if (cfgValue.GetValidatorOfValue(DateTime.Now)(lastDate)) {
                    existing.Value = "0";
                    existingValue.Value = now.ToString();
                    var exc = existingValue.UpdateSelf().Fault;
                } else {
                    if (config.IsToBeIncrementedOnSession()) {
                        existing.Value = (existing.Value.ToType<Int32>() + 1).ToString();
                    }
                }
            }
        }

        public static TItem GetConfigValue<TItem>(EAppConfig config) {

            var existingValue = DbProvider.ProvideEntity<AppConfigItem>()
                    .SelectOne(new { AppConfigType = config });
            if (existingValue != null) {
                return existingValue.Value.ToType<TItem>();
            } else {
                return default(TItem);
            }

        }
    }

}
