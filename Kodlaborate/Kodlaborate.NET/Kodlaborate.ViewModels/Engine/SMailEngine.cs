﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using SendGrid;
using LLF.Extensions;
using System.Threading.Tasks;
using System.Text;
using Kodlaborate.CoreData.EntityObjects;
using Kodlaborate.ViewModels.PageModels;
using Kodlaborate.Resources.Locals;

namespace Kodlaborate.ViewModels.Engine {
    public static class SMailEngine {

		public static async Task SendAsync(RecoverModel vm, NetworkCredential credentials, MailAddress from, MailAddressCollection to, String subject, String bodyHtml) {

			ISendGrid myMessage = new SendGridMessage();
			myMessage.From = from;

			// Add multiple addresses to the To field.
			var recipients = (from t in to.GetEnumerator().MakeQuery() select t.Address).ToList();
			myMessage.AddTo(recipients);

			myMessage.Subject = subject;

			//Add the HTML and Text bodies
			myMessage.Html = bodyHtml;
			//myMessage.Text = "Hello World plain text!";

			var transportWeb = new Web(credentials);

			try {
				// Send the email.
				// You can also use the **DeliverAsync** method, which returns an awaitable task.
				await transportWeb.DeliverAsync(myMessage);
                vm.Response.Success = true;
                vm.Response.Message = MailTemplates.MailSendSuccess;
			} catch {
                vm.Response.Success = false;
                vm.Response.Message = MailTemplates.MailSendFail;
			}

		}

		public static String ComposeMailBodyForRecovery(Author author, RecoverModel vm) {

			var body = MailTemplates.TextTemplate
				.Replace("##identity##", author.Identity)
				.Replace("##site_name##", vm.SiteName)
                //.Replace("##sitelogo##", vm.Sitelogo)
				.Replace("##recovery_link##", vm.RecoveryLink)
				.Replace("##reset_link##", SAuthEngine.GenerateResetQueryString(author, vm.ResetLink))
                //.Replace("##terms_link##", vm.TermsLink)
                //.Replace("##privacy_link##", vm.PrivacyLink)
                //.Replace("##unsubscribe_link##", vm.UnsubscribeLink)
                //.Replace("##company_phone##", vm.CompanyContactPhone)
                .Replace("##contact_email##", vm.CompanyContactEmail)
                .Replace("##address_line1##", vm.AddressLine1)
                .Replace("##address_line2##", vm.AddressLine2)
                .Replace("##copyright_term##", vm.CopyrightTerm)
                .Replace("##site_url##", vm.SiteUrl)
                ;
			return body;

		}
		public static String ComposeMailSubjectForRecovery(Author author, String sitename) {

			return MailTemplates.MailSubjectForRecovery.Replace("##identity##", author.Identity).Replace("##sitename##", sitename);

		}

	}
}
