﻿using Kodlaborate.CoreData.EntityObjects;
using Kodlaborate.ViewModels.RowModels;
using Kodlaborate.CoreData.Enums;
using LLF.Data.Containers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kodlaborate.ViewModels.PageModels;
using Kodlaborate.Resources.Locals;
using LLF.Data.Tools;

namespace Kodlaborate.ViewModels.Engine {

    public static class SAdminEngine {

        public static IEnumerable<UserMessageRowModel> GetAllUserMessages() {

            var many = DbProvider.ProvideEntity<UserMessage>().SelectMany();
            return many.ToList().Select(um => new UserMessageRowModel(um)).ToArray();

        }


        public static IEnumerable<ToDoTaskRowModel> GetToDoTasksOf(int authorID) {

            var many = DbProvider.ProvideEntity<ToDoTask>().SelectMany(new { OwnerID = authorID });
            return many.Select(task => new ToDoTaskRowModel(task)).ToArray();

        }

        public static IEnumerable<UserMessageRowModel> GetUserMessagesBy(EReplyStatus status) {

            var many = DbProvider.ProvideEntity<UserMessage>().SelectMany(new { ReplyStatus = status });
            return many.Select(um => new UserMessageRowModel(um)).ToArray();

        }

        public static IEnumerable<LinkRowModel> GetPublicLinks() {

            var many = DbProvider.ProvideEntity<CodeLink>().SelectMany();
            return many.Where(cl => cl.Owner == null).ToList().Select(cl => new LinkRowModel(cl));

        }


        public static void ChangePublicLinkStatus(PublicLinkModel model, Int64 linkID, ELinkStatus status) {

            var existingLink = DbProvider.ProvideEntity<CodeLink>().SelectThe(linkID);
            if (existingLink != null) {
                existingLink.LinkStatus = status;
                var exc = existingLink.UpdateSelf().Fault;
                if (exc == null) {
                    model.Response.Success = true;
                    model.Response.Message = Messages.ChangePublicLinkStatusDone;
                } else {
                    model.Response.Success = false;
                    model.Response.Message = Messages.ChangePublicLinkStatusFailed;
                }
            }


        }

        public static void ChangePublicLinkStatusForAll(PublicLinkModel model, ELinkStatus status) {

            var li = DbProvider.ProvideEntity<CodeLink>().SelectMany().Where(cl => cl.Owner == null);
            Exception exc = null;
            foreach (var cl in li) {
                cl.LinkStatus = status;
                exc = exc == null ? cl.UpdateSelf().Fault : exc;
            }

            if (exc == null) {
                model.Response.Success = true;
                model.Response.Message = Messages.ChangePublicLinkStatusDone;
            } else {
                model.Response.Success = false;
                model.Response.Message = Messages.ChangePublicLinkStatusFailed;
            }
        }

    }

}
