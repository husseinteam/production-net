﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kodlaborate.ViewModels.Protocols {
    public interface IGenericViewModel {

        IGenericViewModel SetContent(Int32 id);

    }
}
