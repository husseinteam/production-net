﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LLF.Extensions;
using System.IO;
using LLF.Data.Containers;
using LLF.Abstract.Xml;
using LLF.Data.Tools;
using LLF.Abstract.Data.Engine;
using System.Net.Http.Headers;
using LLF.Static.Globals;

namespace LLF.TestBase {

	public abstract class ApiTestBase : DataTestBase {

        #region Local-Remote

#if DEBUG
        private static String authority = "http://localhost:{0}";
#else
        private static String authority = "http://api.lampiclobe.com";
#endif
        #endregion

        #region Inherited

        protected async Task<TResponse> ClientCallGetAsync<TResponse>(String path)
			where TResponse : class {

            var uri = new Uri(authority + "/{1}/{2}/{3}".PostFormat(Port, ApiRoute, ControllerRoutePrefix, path));
            var response = await uri.RequestGetJson<TResponse>();
            return response;

		}

		protected async Task<TResponse> ClientCallPostAsync<TEntity, TResponse>(String path
			, TEntity entity)
			where TEntity : class {

            var route = "/{0}/{1}/{2}".PostFormat(ApiRoute, ControllerRoutePrefix, path);
            var response = await entity.RequestPostJson<TEntity, TResponse>(new Uri(authority.PostFormat(Port)), route);
            return response;

		}

		#endregion

		#region Abstract

		protected abstract String ControllerRoutePrefix { get; }
		protected abstract String ApiRoute { get; }
		protected abstract Int32 Port { get; }

		#endregion

        protected override string CredentialKey {
            get { return SGlobalDefaults.CredentialKey; }
        }
    }

}
