﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LLF.Extensions;
using System.IO;
using LLF.Data.Containers;
using LLF.Abstract.Xml;
using LLF.Data.Tools;
using LLF.Abstract.Data.Engine;
using LLF.Static;

namespace LLF.TestBase {

    public abstract class DataTestBase {

        public DataTestBase() {

            DatabaseInit();

        }

        protected void DatabaseInit() {

			CredentialProvider.CurrentCredential = ResourceContainer
                .ReadXmlFor<IConnectionCredential>(CredentialKey);

		}

        protected abstract String CredentialKey { get; }

	}

}
