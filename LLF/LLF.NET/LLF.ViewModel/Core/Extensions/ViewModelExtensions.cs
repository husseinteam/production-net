﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Extensions;
using System.Text.RegularExpressions;
using System.Resources;
using System.Reflection;
using LLF.Data.Xml;
using LLF.Annotations.ViewModel;
using LLF.Data.Containers;
using LLF.Abstract.Xml;
using LLF.Abstract.Data.Engine;
using LLF.ViewModel.Response;

namespace LLF.ViewModel.Core.Extensions {

	public static class ViewModelExtensions {

		public static IValidationResponse ApplyRegexConstraints<TViewModel>(this TViewModel vm,
			ResourceManager resource, IValidationResponse response) {

			var sep = ResourceContainer.ReadXmlFor<IAttributeItem>("MessageSeperator").Value;
			foreach (var pi in typeof(TViewModel).GetPublicProperties()) {
				var rgxList = pi.GetCustomAttributes(typeof(ApplyRegexAttribute), true);
				foreach (ApplyRegexAttribute rgx in rgxList) {
					var value = pi.GetValue(vm, null);
					if (!Object.ReferenceEquals(value, null)) {
						var patternKey = ResourceContainer.ReadXmlFor<IDefinitionItem>(
							rgx.PatternKey).Value;
						if (!Regex.IsMatch(value.ToString(), patternKey,
								rgx.Options | RegexOptions.IgnoreCase)) {
							response.AppendMessage("{0} {1} ", resource.GetString(rgx.ValidationErrorKey), sep);
                            (response as ValidationResponse).IsValid = false;
						}
					}
				}
			}
			return response;
		}

		public static IValidationResponse CheckUnassigned<TViewModel>(this TViewModel vm, ResourceManager resource) {

			var response = new ValidationResponse();
			var sep = ResourceContainer.ReadXmlFor<IAttributeItem>("MessageSeperator").Value;
			foreach (var pi in typeof(TViewModel).GetPublicProperties()) {
				var attr = pi.GetCustomAttribute<ToUIAttribute>();
				if (attr != null) {
					var value = pi.GetValue(vm, null);
					if (value.IsUnassigned() &&
						!Object.ReferenceEquals(attr.ValidationErrorKey, null)) {
						response.AppendMessage("{0} {1} ",
							resource.GetString(attr.ValidationErrorKey), sep);
						response.IsValid = false;
					}
				}
			}
			return response;
		}

		public static IValidationResponse CommonValidator<TViewModel>(this TViewModel vmodel, Action<ValidationResponse, TViewModel> commit = null, Boolean validateRegex = true) {

			ResourceManager resource = null;
			var resourcer = vmodel.GetType().GetCustomAttribute<LocalResourceAttribute>();
			if (resourcer != null) {
				resource = resourcer.ResourceType
					.GetRuntimeProperty<ResourceManager>("ResourceManager");
			} else {
				throw new ArgumentException("No Resource Manager Specified Use [LocalResource]");
			}

			var sep = ResourceContainer.ReadXmlFor<IAttributeItem>("MessageSeperator").Value;
            var result = vmodel.CheckUnassigned(resource);

			if (commit != null) {
                commit(result as ValidationResponse, vmodel);
			}

            if (validateRegex) {
                vmodel.ApplyRegexConstraints(resource, result); 
            }

			result.ModifyMessage(msb => msb.ToString().TrimLast(" {0} ".PostFormat(sep)));
            return result;

		}

	}

}
