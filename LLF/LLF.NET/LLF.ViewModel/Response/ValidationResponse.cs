﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Abstract.Data.Engine;
using LLF.Data.Containers;
using LLF.Abstract.Xml;

namespace LLF.ViewModel.Response {
	public class ValidationResponse : IValidationResponse {

		public ValidationResponse() {

			IsValid = true;
			_MessageBuilder = new StringBuilder();

		}

		private StringBuilder _MessageBuilder;
		public String FinalMessage {
			get {
				return _MessageBuilder.ToString();
			}
		}
		public Boolean IsValid { get; set; }

		public IValidationResponse AppendMessage(String appended, params String[] arguments) {

            _MessageBuilder.AppendFormat("{0} ",
                ResourceContainer.ReadXmlFor<IAttributeItem>("MessageSeperator").Value);
			_MessageBuilder.AppendFormat(appended, arguments);
			return this;

		}

		public IValidationResponse ModifyMessage(Func<StringBuilder, String> commit) {

			_MessageBuilder = new StringBuilder(commit(_MessageBuilder));
			return this;

		}

	}
}
