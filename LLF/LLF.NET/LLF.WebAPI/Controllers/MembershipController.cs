﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LLF.ApiBase;
using LLF.ApiBase.Filters;
using LLF.Claim.Actors.Core;
using LLF.Claim.Model.ActionResponses;
using LLF.Claim.Model.ViewModels.Identity;
using LLF.Claim.Model.ViewModels.Membership;
using LLF.Static.Globals;

namespace LLF.WebAPI.Controllers {

    [RoutePrefix(SGlobalDefaults.ApiPrefix + "/membership")]
	public class MembershipController : BaseApiController {

		[Route("edit")]
		[HttpPost]
		[TokenAuthorization]
		public ApiPostResponse Edit([FromBody]EditMembershipViewModel vmodel) {

			var vresp = MembershipManager.EditMembership(vmodel);
			return vresp.Response;

		}

	}

}
