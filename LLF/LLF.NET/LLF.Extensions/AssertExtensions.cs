﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LLF.Extensions {
    public static class AssertExtensions {

        public static void Commit(this Boolean trueOrFalse, Action<Boolean> commit) {

            commit(trueOrFalse);

        }

        public static void Assert<TException>(this Boolean isTrue, String message)
            where TException : Exception, new() {

            if (!isTrue) {
                var ex = Activator.CreateInstance(typeof(TException), message)
                    as TException;
                throw ex;
            }

        }

        public static TItem AssertNotNull<TItem>(this TItem instance) {

            var assertion = false;
            if (typeof(TItem).IsValueType) {
                assertion = !instance.Equals(0);
            } else {
                assertion = !instance.Equals(default(TItem));
            }
            assertion.Assert<NullReferenceException>("{0} type is null or default".PostFormat(typeof(TItem).Name));
            return instance;
        }

    }
}
