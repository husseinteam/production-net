﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace LLF.Extensions {
	public static class DictionaryExtensions {
		public static IDictionary<string, object> Apply(this IDictionary<string, object> self, String key, Object value) {

			if (self.ContainsKey(key)) {
				self[key] = value;
			} else {
				self.Add(key, value);
			}
			return self;

		}

		public static IDictionary<string, TItem> Apply<TItem>(this IDictionary<string, TItem> self, String key, TItem value) {

			if (self.ContainsKey(key)) {
				self[key] = value;
			} else {
				self.Add(key, value);
			}
			return self;

		}

		public static Dictionary<String, TItem> Gather<TItem>(this IEnumerable<KeyValuePair<String, TItem>> list) {

			var dict = new Dictionary<String, TItem>();
			foreach (var li in list) {
				dict.Apply(li.Key, li.Value);
			}
			return dict;

		}

	}

}
