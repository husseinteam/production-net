﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LLF.Extensions {
    public static class AttributeExtensions {

        public static IEnumerable<TAttribute> GetAllPropertyDecorations<TAttribute>(this Type item)
        where TAttribute : System.Attribute {

            return item.GetProperties()
                .SelectMany<PropertyInfo, TAttribute>(pi 
                => pi.GetCustomAttributes<TAttribute>());

        }
        public static TAttribute GetClassDecoration<TAttribute>(this Type item)
            where TAttribute : Attribute {
            return item.GetCustomAttribute<TAttribute>();
        }

        public static IEnumerable<PropertyInfo> GetPropertiesDecoratedWith<TAttribute>(this Type item)
            where TAttribute : Attribute {
            return item.GetProperties()
                .Where(pi => pi.GetCustomAttribute<TAttribute>() != null);
        }

    }
}
