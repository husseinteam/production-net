﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using LLF.Extensions.Data.Enums;


namespace LLF.Extensions.Data.Enums {

    public static class DbTypeExtensions {

        #region Static Methods

        public static Boolean IsText(this EDbType columnType) {
            return columnType == EDbType.Char
                || columnType == EDbType.NChar
                || columnType == EDbType.NText
                || columnType == EDbType.NVarChar
                || columnType == EDbType.Text
                || columnType == EDbType.VarChar;
        }

        public static Boolean IsNumeric(this EDbType columnType) {
            return columnType == EDbType.Byte
                    || columnType == EDbType.Long
                    || columnType == EDbType.Decimal
                    || columnType == EDbType.Int32
                    || columnType == EDbType.Int16;
        }

        public static Boolean IsDecimal(this EDbType columnType) {
            return 
                columnType == EDbType.Decimal
                || columnType == EDbType.Money;
        }

        public static Boolean IsBoolean(this EDbType columnType) {
            return columnType == EDbType.Boolean;
        }

        public static EDbType GetEDbType(this string columnType) {
            switch (columnType) {
                case "Long":
                    return EDbType.Long;
                case "Boolean":
                    return EDbType.Boolean;
                case "binary":
                    return EDbType.Binary;
                case "char":
                    return EDbType.Char;
                case "datetime":
                    return EDbType.DateTime;
                case "decimal":
                    return EDbType.Decimal;
                case "float":
                    return EDbType.Float;
                case "image":
                    return EDbType.Image;
                case "int":
                    return EDbType.Int32;
                case "money":
                    return EDbType.Money;
                case "nchar":
                    return EDbType.NChar;
                case "numeric":
                    return EDbType.Decimal;
                case "nvarchar":
                    return EDbType.NVarChar;
                case "Double":
                    return EDbType.Double;
                case "smalldatetime":
                    return EDbType.SmallDateTime;
                case "smallint":
                    return EDbType.Int16;
                case "smallmoney":
                    return EDbType.SmallMoney;
                case "sql_Object":
                    return EDbType.Object;
                case "text":
                    return EDbType.Text;
                case "timestamp":
                    return EDbType.Timestamp;
                case "Byte":
                    return EDbType.Byte;
                case "uniqueidentifier":
                    return EDbType.UniqueIdentifier;
                case "varbinary":
                    return EDbType.VarBinary;
                case "varchar":
                    return EDbType.VarChar;
                case "xml":
                    return EDbType.Xml;
                default:
                    return EDbType.Int32;
            }
        }

        public static EDbType GetEDbTypeFrom(this Type propertyType) {
            switch (propertyType.Name.ToLowerInvariant()) {
                case "int64":
                    return EDbType.Long;
                case "boolean":
                    return EDbType.Boolean;
                case "datetime":
                    return EDbType.DateTime;
                case "single":
                    return EDbType.Float;
                case "image":
                    return EDbType.Image;
                case "int32":
                    return EDbType.Int32;
                case "decimal":
                    return EDbType.Decimal;
                case "String":
                    return EDbType.NVarChar;
                case "Double":
                    return EDbType.Double;
                case "int16":
                    return EDbType.Int16;
                case "Object":
                    return EDbType.Object;
                case "timestamp":
                    return EDbType.Timestamp;
                case "byte":
                    return EDbType.Byte;
                case "guid":
                    return EDbType.UniqueIdentifier;
                case "byte[]":
                    return EDbType.VarBinary;
                case "xml":
                    return EDbType.Xml;
                default:
                    return EDbType.Int32;
            }
        }

        #endregion

    }

}
