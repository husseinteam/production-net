﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LLF.Extensions {
   public static class RegexExtensions {

        public static Boolean ValidateEmail(this String input, RegexOptions options = RegexOptions.IgnoreCase) {

            return Regex.IsMatch(input, @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", options); 

        }

        public static Boolean TestPattern(this String self, String pattern, RegexOptions options = RegexOptions.IgnoreCase) {

            return Regex.IsMatch(self, pattern, options);

        }

        public static String StripHtml(this String self) {

            var noHTML = Regex.Replace(self, @"<[^>]+>|&nbsp;", "").Trim();
            return Regex.Replace(noHTML, @"\s{2,}", " ");

        }

    }
}
