﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace LLF.Extensions {
    public static class RandomExtensions {

        private static Random r = new Random();


        public static TItem SelectRandom<TItem>(this IEnumerable<TItem> list) {

            return list.ElementAt(r.Next(list.Count()));

        }

        public static TProperty SelectRandom<TItem, TProperty>(this IEnumerable<TItem> list, Func<TItem, TProperty> selector) {

            return list.Select(selector).ElementAt(r.Next(list.Count()));

        }

        public static Int32 Randomize(this Int32 number, Int32 minimum = 1) {

            return r.Next(minimum, number);

        }

        public static IEnumerable<TItem> Shuffle<TItem>(this IEnumerable<TItem> array) {

            return array.Count().Randomize().Times(k => array.ElementAt(k.Randomize()));

        }

        public static IEnumerable<TItem> Stir<TItem>(this IEnumerable<TItem> array) {

            return array.OrderBy(x => r.Next()).ToArray();

        }

        public static Int32 DigitsRandom(this Int32 digits) {

            if (digits == 0) {
                throw new Exception("digit cannot be 0");
            }
            var begin = 1;
            var end = 1;
            (digits -1).Times(() => begin *= 10);
            (digits).Times(() => end *= 10);
            return r.Next(begin, end + 1);

        }

        public static TItem Random<TItem>(this IEnumerable<TItem> self) {

            return self.ElementAt(r.Next(self.Count()));

        }

    }

}
