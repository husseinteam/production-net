using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using Microsoft.Win32.SafeHandles;

namespace LLF.Extensions {
    /// <summary>
    /// Provides access to NTFS junction points in .Net.
    /// </summary>
    public static class JunctionPointExtensions {
        public static Boolean CheckNotJunctionPoint(this String junctionPoint) {

            try {
                return !JunctionPoint.Exists(junctionPoint);
            } catch {
                return true;
            }

        }
    }
}
