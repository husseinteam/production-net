﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LLF.Extensions {
	public static class GraphicsExtensions {

		public static Image ResizeImage(this Image self, Size size) {

			return (Image)(new Bitmap(self, size));

		}

		public static Image CropImage(this Image image, Int32 x, Int32 y, Int32 width, Int32 height) {

			var cropRect = new Rectangle(x, y, width, height);
			var target = new Bitmap(cropRect.Width, cropRect.Height, PixelFormat.Format32bppArgb);

			using (Graphics g = Graphics.FromImage(target)) {
				g.DrawImage(image, new Rectangle(0, 0, target.Width, target.Height),
					cropRect, GraphicsUnit.Pixel);
			}
			return target;

		}

		public static Image RotateImage(this Image image, Single angle) {
			if (image == null)
				throw new ArgumentNullException("image");

			const double pi2 = Math.PI / 2.0;


			double oldWidth = (double)image.Width;
			double oldHeight = (double)image.Height;

			// Convert degrees to radians
			double theta = ((double)angle) * Math.PI / 180.0;
			double locked_theta = theta;

			// Ensure theta is now [0, 2pi)
			while (locked_theta < 0.0)
				locked_theta += 2 * Math.PI;

			double newWidth, newHeight;
			int nWidth, nHeight; // The newWidth/newHeight expressed as ints



			double adjacentTop, oppositeTop;
			double adjacentBottom, oppositeBottom;


			if ((locked_theta >= 0.0 && locked_theta < pi2) ||
				(locked_theta >= Math.PI && locked_theta < (Math.PI + pi2))) {
				adjacentTop = Math.Abs(Math.Cos(locked_theta)) * oldWidth;
				oppositeTop = Math.Abs(Math.Sin(locked_theta)) * oldWidth;

				adjacentBottom = Math.Abs(Math.Cos(locked_theta)) * oldHeight;
				oppositeBottom = Math.Abs(Math.Sin(locked_theta)) * oldHeight;
			} else {
				adjacentTop = Math.Abs(Math.Sin(locked_theta)) * oldHeight;
				oppositeTop = Math.Abs(Math.Cos(locked_theta)) * oldHeight;

				adjacentBottom = Math.Abs(Math.Sin(locked_theta)) * oldWidth;
				oppositeBottom = Math.Abs(Math.Cos(locked_theta)) * oldWidth;
			}

			newWidth = adjacentTop + oppositeBottom;
			newHeight = adjacentBottom + oppositeTop;

			nWidth = (int)Math.Ceiling(newWidth);
			nHeight = (int)Math.Ceiling(newHeight);

			Bitmap rotatedBmp = new Bitmap(nWidth, nHeight);

			using (Graphics g = Graphics.FromImage(rotatedBmp)) {

				Point[] points;

				if (locked_theta >= 0.0 && locked_theta < pi2) {
					points = new Point[] { 
                                             new Point( (int) oppositeBottom, 0 ), 
                                             new Point( nWidth, (int) oppositeTop ),
                                             new Point( 0, (int) adjacentBottom )
                                         };

				} else if (locked_theta >= pi2 && locked_theta < Math.PI) {
					points = new Point[] { 
                                             new Point( nWidth, (int) oppositeTop ),
                                             new Point( (int) adjacentTop, nHeight ),
                                             new Point( (int) oppositeBottom, 0 )                        
                                         };
				} else if (locked_theta >= Math.PI && locked_theta < (Math.PI + pi2)) {
					points = new Point[] { 
                                             new Point( (int) adjacentTop, nHeight ), 
                                             new Point( 0, (int) adjacentBottom ),
                                             new Point( nWidth, (int) oppositeTop )
                                         };
				} else {
					points = new Point[] { 
                                             new Point( 0, (int) adjacentBottom ), 
                                             new Point( (int) oppositeBottom, 0 ),
                                             new Point( (int) adjacentTop, nHeight )        
                                         };
				}

				g.DrawImage(image, points);
			}

			return rotatedBmp;
		}

		//public static Image RotateImage(this Image self, Single angle) {

		//	var newSize = self.Size.ApplyRotation(angle);
		//	var target = new Bitmap(newSize.Width, newSize.Height);
		//	using (Graphics g = Graphics.FromImage(target)) {
		//		g.TranslateTransform((float)target.Width / 2, (float)target.Height / 2);
		//		g.RotateTransform(angle);
		//		g.TranslateTransform(-(float)target.Width / 2, -(float)target.Height / 2);
		//		g.DrawImage(self, new Point(0, 0));
		//	}
		//	return target;

		//}

		public static Image ToImage(this Stream stream) {

			return Image.FromStream(stream);

		}

		public static Size ApplyRotation(this Size sz, float angle) {
			var m = new Matrix();
			m.Rotate(angle);
			var pts = new[]
                  {
                      new Point(sz.Width, sz.Height),
                      new Point(sz.Width, 0)
                  };
			m.TransformPoints(pts);
			return new Size(
				Math.Max(Math.Abs(pts[0].X), Math.Abs(pts[1].X)),
				Math.Max(Math.Abs(pts[0].Y), Math.Abs(pts[1].Y)));
		}

	}
}
