﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LLF.Extensions {
    public static class DynamicExtensions {

        public static void TransferTo(this Object source, Object dest) {

            var props = source.GetType().GetProperties(BindingFlags.Instance
                | BindingFlags.Public).ToList();

            props.ForEach(pi => {
                try {
                    pi.SetValue(dest, pi.GetValue(source, null));
                } catch {

                }
            });
            dynamic destExp = dest;
            props.ForEach(pi => {
                var dict = (destExp as IDictionary<String, Object>);
                if (dict != null) {
                    if (dict.ContainsKey(pi.Name))
                        dict[pi.Name] = pi.GetValue(source, null);
                    else
                        dict.Add(pi.Name, pi.GetValue(source, null));
                }
            });

        }

        public static void TransferTo<TItem>(this TItem source, TItem dest, Func<PropertyInfo, Boolean> checker, Func<PropertyInfo, IEnumerable<Object>> itemActor = null) {

            var props = typeof(TItem).GetProperties(BindingFlags.Instance
                | BindingFlags.Public).ToList();

            props.ForEach(pi => {
                if (pi.PropertyType.IsCollection()) {
                    var enumerable = pi.GetValue(source, null) as IEnumerable<Object>;
                    if (itemActor != null) {
                        if (checker(pi)) {
                            try {
                                pi.SetValue(dest, itemActor(pi));
                            } catch {
                            }
                        }
                    } else {
                        try {
                            pi.SetValue(dest, pi.GetValue(source, null));
                        } catch {
                        }
                    }
                } else {
                    if (checker(pi)) {
                        if (itemActor != null) {
                            try {
                                pi.SetValue(dest, itemActor(pi));
                            } catch {
                            }
                        }else {
                            try {
                                pi.SetValue(dest, pi.GetValue(source, null));
                            } catch {
                            }
                        }
                    }
                }
            });

        }

        public static TResponse Import<TResponse>(this Object dest, Object importedExpandoObject)
                where TResponse : class {

            var props = dest.GetType()
                .GetProperties(BindingFlags.Instance
                | BindingFlags.Public)
                .Where(pi => pi.CanRead && pi.CanWrite).ToList();

            props.ForEach(pi => {
                var dict = (importedExpandoObject as IDictionary<String, Object>);
                if (dict.ContainsKey(pi.Name))
                    dict[pi.Name] = pi.GetValue(dest, null);
                else
                    dict.Add(pi.Name, pi.GetValue(dest, null));
            });
            return dest as TResponse;
        }
        public static dynamic ToExpando(this Object self, params Action<dynamic>[] modifiers) {

            var to = new ExpandoObject();
            var props = self.GetType().GetPublicProperties().ToList();

            props.ForEach(pi => {
                var dict = (to as IDictionary<String, Object>);
                if (dict.ContainsKey(pi.Name))
                    dict[pi.Name] = pi.GetValue(self, null);
                else
                    dict.Add(pi.Name, pi.GetValue(self, null));
            });
            foreach (var mod in modifiers) {
                mod(to);
            }
            return to;

        }

        public static dynamic ToExpando(this Object self, Action<dynamic> modifier, Action<dynamic>[] modifiers) {

            var to = new ExpandoObject();
            var props = self.GetType().GetPublicProperties().ToList();

            props.ForEach(pi => {
                var dict = (to as IDictionary<String, Object>);
                if (dict.ContainsKey(pi.Name))
                    dict[pi.Name] = pi.GetValue(self, null);
                else
                    dict.Add(pi.Name, pi.GetValue(self, null));
            });
            foreach (var mod in modifiers) {
                mod(to);
            }
            modifier(to);
            return to;

        }

    }
}
