﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LLF.Extensions {
    public static class DataExtensions {
        public static DbType GetDbType(this String columnType) {

            switch (columnType) {
            case "bigint":
                return DbType.Int64;
            case "bit":
                return DbType.Boolean;
            case "binary":
                return DbType.Binary;
            case "char":
                return DbType.SByte;
            case "datetime":
                return DbType.DateTime;
            case "decimal":
                return DbType.Decimal;
            case "float":
                return DbType.Single;
            case "image":
                return DbType.Binary;
            case "int":
                return DbType.Int32;
            case "money":
                return DbType.Decimal;
            case "nchar":
                return DbType.StringFixedLength;
            case "numeric":
                return DbType.Decimal;
            case "nvarchar":
                return DbType.String;
            case "real":
                return DbType.Double;
            case "smalldatetime":
                return DbType.DateTime;
            case "smallint":
                return DbType.Int16;
            case "smallmoney":
                return DbType.Binary;
            case "text":
                return DbType.String;
            case "timestamp":
                return DbType.Time;
            case "tinyint":
                return DbType.Int16;
            case "uniqueidentifier":
                return DbType.Guid;
            case "varbinary":
                return DbType.Binary;
            case "varchar":
                return DbType.String;
            case "xml":
                return DbType.Xml;
            default:
                return DbType.Int32;
            }

        }

    }
}
