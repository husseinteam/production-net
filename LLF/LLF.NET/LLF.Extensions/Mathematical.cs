﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LLF.Extensions {
    public static class Mathematical {

        public static Int64 Modulus(this Int64 number, Int64 mod) {

            return number % mod;

        }

        public static Int32 Modulus(this Int32 number, Int32 mod) {

            return number % mod;

        }


        public static Int32 Percentise(this Single number) {

            return Convert.ToInt32(number * 100f);

        }

    }
}
