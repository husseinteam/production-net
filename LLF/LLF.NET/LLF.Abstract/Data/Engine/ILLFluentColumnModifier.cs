﻿using LLF.Extensions.Data.Enums;
using LLF.Annotations.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LLF.Abstract.Data.Engine {
    public interface ILLFluentColumnModifier<TEntity>
        where TEntity : IBaseEntity<TEntity> {

        #region Methods

        ILLFluentColumnModifier<TEntity> WithName(String name);
        ILLFluentColumnModifier<TEntity> WithType(EDbType dbType);
        ILLFluentColumnModifier<TEntity> WithMaximum(Int32 maximum);
        ILLFluentColumnModifier<TEntity> IsNotNull();
        ILLFluentColumnModifier<TEntity> IsNull();
        ILLFluentColumnModifier<TEntity> SetIdentity(Int32 seed = 1, Int32 increment = 1);
        ILLFluentColumnModifier<TEntity> SetDefault(Object value);
        ILLFluentColumnModifier<TEntity> SetUnique(String order = "ASC");
        ILLFluentColumnModifier<TEntity> IsEnumeration();
        ILLFluentColumnModifier<TEntity> References<TReferencedEntity>(
            String fkKey, Func<TReferencedEntity, PropertyInfo> keySelector)
            where TReferencedEntity : IBaseEntity<TReferencedEntity>; 

        #endregion

        #region Properties

        Type PropertyType { get; }
        String Name { get; }
        EDbType Type { get; }
        IdentityAttribute Identity { get; }
        DefaultValueAttribute DefaultValue { get; }
        Boolean NotNull { get; }
        Int32 Maximum { get; }
        UniqueAttribute Unique { get; }
        Boolean Enumeration { get; }
        Object Column { get; }
        String ReferencedKey { get; }
        String FKKey { get; }
        #endregion

    }
}
