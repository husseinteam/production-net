﻿using LLF.Annotations.Data;
using LLF.Extensions.Data.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LLF.Abstract.Data.Engine {

    public interface IEntityModifier<TEntity> 
        where TEntity : IBaseEntity<TEntity> {

        #region CRUD

        TEntity InsertSelf();

        TEntity DeleteSelf(Boolean cascade = false);

        TEntity UpdateSelf();

        TEntity UpsertSelf();

        TEntity UpsertCollections<TElement>()
            where TElement : class, IBaseEntity<TElement>;

        #endregion

    }

}
