﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Extensions.Data.Enums;
using LLF.Abstract.Xml;
using System.Data;

namespace LLF.Abstract.Data.Engine {
	public interface IConnectionCredential : IXmlResource {

		#region Members

		IConnectionCredential SetServiceType(EServiceType serviceType, ECredentialType credentialType);

        IDbCommand ProvideDbCommand();
        String Server { get; set; }
		String Database { get; set; }
		String UserID { get; set; }
		String Password { get; set; }
		String SuperAdminUserID { get; set; }
		String SuperAdminPassword { get; set; }
		Int32 Port { get; set; }
		String AttachDbFilename { get; set; }
		String ServerCompatibilityLevel { get; set; }
		EServiceType ServiceType { get; set; }
		ECredentialType CredentialType { get; set; }
        DbConnection ProvideSuperAdminConnection();
        DbConnection ProvideMasterConnection();
        DbConnection ProvideConnection();
        IDbCommand SetDbParameter(IDbCommand cmd, IDbDataParameter prm);
        IDbDataParameter ProvideDbParameter(String key, Object value);
		#endregion


    }
}
