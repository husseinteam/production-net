﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LLF.Abstract.Data.Engine {

	public interface ILLFRepository<TEntity>
		where TEntity : class {

		#region FromDb Queries
		
		IEnumerable<TEntity> GetAll();
		TEntity GetThe<TKey>(TKey pk);
		TEntity GetSingle(Object selector);
		IEnumerable<TEntity> GetMany(object selector, object selectornot = null);
		Int32 GetCountOf(object selector = null, object selectornot = null);

		#endregion

		#region ToDb Queries

		IDMLResponse<TEntity> ToDb(TEntity entity);
        IDMLResponse<TEntity> OutOfDb<TKey>(TKey key);
		#endregion

	}

}
