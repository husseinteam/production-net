﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using LLF.Annotations.Data;
using LLF.Extensions.Data.Enums;

namespace LLF.Abstract.Data.Engine {

    public interface IBaseEntity<TEntity> : ILLFluent<TEntity>, IEntityModifier<TEntity>, IEntitySelector<TEntity>, IEntityReader<TEntity>, IEntityDefiner<TEntity>
		where TEntity : IBaseEntity<TEntity> {

		#region Properties

		Exception Fault { get; set; }

        #endregion

    }
}
