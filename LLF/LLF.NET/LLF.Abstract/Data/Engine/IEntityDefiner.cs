﻿using LLF.Annotations.Data;
using LLF.Extensions.Data.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LLF.Abstract.Data.Engine {

    public interface IEntityDefiner<TEntity> 
        where TEntity : IBaseEntity<TEntity> {

        #region DDL

        IEntityDefiner<TEntity> Truncate();

        #endregion

    }

}
