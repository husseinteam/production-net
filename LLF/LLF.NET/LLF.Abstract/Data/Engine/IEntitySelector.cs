﻿using LLF.Annotations.Data;
using LLF.Extensions.Data.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LLF.Abstract.Data.Engine {

    public interface IEntitySelector<TEntity> 
        where TEntity : IBaseEntity<TEntity> {

        #region Select

        TEntity SelectThe(Int64 id);

        TScalar SelectCount<TScalar>(object selector = null, object selectornot = null);

        TEntity SelectOne(object selector = null);

        ICollection<TEntity> SelectMany(object selector = null, object selectornot = null);

        #endregion


    }

}
