﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LLF.Abstract.Data.Engine {

	public interface ISeedResponse<TEntity>
        where TEntity : IBaseEntity<TEntity>, new() {

		Int32 Magnitude { get; }
		List<TEntity> SeedList { get; }

		Func<Int32, TEntity> SingleSeed { get; }
		ISeedResponse<TEntity> CollectSeed();

        ISeedResponse<TEntity> PersistSeed(ref IList<Exception> exceptions);
        ISeedResponse<TEntity> PersistCollectionSeed<TElement>(ref IList<Exception> exceptions)
            where TElement : class, IBaseEntity<TElement>;
        ISeedResponse<TEntity> DestroySeed();
        ISeedResponse<TEntity> TruncateSeed();

		ISeedResponse<TEntity> Extend(ISeedResponse<TEntity> other);

		Boolean HasFault { get; }
		Exception Fault { get; }

	}

}
