﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LLF.Abstract.Data.Engine {

	public interface IScriptEngine {

		String Sprocs { get; }

        String Finalize();

		IScriptEngine GenerateModelFor<TEntity>()
            where TEntity : IBaseEntity<TEntity>;

		IScriptEngine GenerateSproc(String sprocBody, params Object[] args);

    }
    
}
