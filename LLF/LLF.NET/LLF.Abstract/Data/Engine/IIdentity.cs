﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LLF.Abstract.Data.Engine {

    public interface IIdentity {

        Int64 ID { get; set; }
        DateTime GeneratedOn { get; set; }

    }

}
