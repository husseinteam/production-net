﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LLF.Abstract.Data.Engine {

    public interface IDatabaseEngine {

		IDatabaseEngine EngineFor<TEntity>()
            where TEntity : IBaseEntity<TEntity>, new();

        Exception GenerateModels();

    }

}
