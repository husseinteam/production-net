﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Abstract.Xml;

namespace LLF.Abstract.Xml {

	public interface IAttributeItem : IXmlResource {

		String Value { get; set; }

	}
}
