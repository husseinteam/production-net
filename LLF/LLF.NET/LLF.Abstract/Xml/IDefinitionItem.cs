﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LLF.Abstract.Xml {

	public interface IDefinitionItem : IXmlResource {

		String Value { get; set; }

	}
}
