﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Abstract.Base;
using Ninject;

namespace LLF.Abstract.Xml {

	public interface IXmlDigester : IDependencyInterface {

		List<TModelInterface> ToModels<TModelInterface>();
		IXmlDigester TakeXmlContent(String xml);
	}

}
