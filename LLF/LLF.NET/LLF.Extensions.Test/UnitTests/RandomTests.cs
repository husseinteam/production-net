﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LLF.Extensions;
using System.Collections.Generic;
using System.Threading;
using System.Diagnostics;

namespace LLF.Extensions.Test.UnitTests {
    [TestClass]
    public class RandomTests {

        [TestMethod]
        public void SelectRandomReturnsroperly() {

            var list = new List<Int32>() { 1, 2, 4, 123, 45, 78, 98, 65 };
            var r = list.SelectRandom();
            Assert.IsTrue(list.Contains(r));

        }

        [TestMethod]
        public void ShuffleRunsOK() {

            var list = new List<Int32>() { 1, 2, 4 };
            var r = list.Shuffle();
            Assert.AreNotEqual(0, r.Count());

        }

        [TestMethod]
        public void DigitRandomizeRunsOK() {

            var r = 3.DigitsRandom();
            Debug.Write(r);
            Assert.AreEqual(r, r % 1000);
        }

    }

}
