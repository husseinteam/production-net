﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LLF.Extensions;
using System.Globalization;

namespace LLF.Extensions.Test.UnitTests {

    [TestClass]
	public class CultureTests {

		[TestMethod]
		public void GlobalizeRunsOK() {

			var cultureName = "tr-TR";
			var culture = new CultureInfo("en-US");
			culture = cultureName.Globalize();
			Assert.AreEqual(cultureName, culture.Name);

		}

    }
}
