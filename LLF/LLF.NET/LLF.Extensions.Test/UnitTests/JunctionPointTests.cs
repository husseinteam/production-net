﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;
using LLF.Extensions;

namespace LLF.Extensions.Test.UnitTests {
    [TestClass]
    public class JunctionPointTests {

        [TestMethod]
        public void JunctionPointCorrect() {

            DriveInfo.GetDrives().Enumerate(di => di.RootDirectory.Name
            .CheckNotJunctionPoint().Assert<IOException>(
                "Drive<{0}> is not a junction point".PostFormat(di.RootDirectory.Name)));
            var drive = @"D:\".CheckNotJunctionPoint();
            Assert.IsTrue(drive);
        }
    }
}
