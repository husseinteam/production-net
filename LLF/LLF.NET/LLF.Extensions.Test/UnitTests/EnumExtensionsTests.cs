﻿using System;

using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LLF.Extensions;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LLF.Extensions.Test.UnitTests {
    [TestClass]
    public class EnumExtensionsTests {

        [TestMethod]
        public void GetDescriptionRunsOK() {

            var na = EProperty.NotApplicable;
            Assert.AreEqual("NotApplicable", na.GetDescription());

        }

        [TestMethod]
        public void ToEnumRunsOK() {

            var no = "Nope";
            var en = no.ToEnum<EProperty>(e => e.GetDescription());
            Assert.AreEqual(EProperty.No, en);

        }

        [TestMethod]
        public void EnumDescriptionInRunsOK() {

            Assert.IsTrue("Nope".EnumDescriptionIn<EProperty>());
            Assert.IsFalse("Noo".EnumDescriptionIn<EProperty>());
            Assert.IsTrue("Yes".EnumDescriptionIn<EProperty>());
            Assert.IsTrue("NotApplicable".EnumDescriptionIn<EProperty>());

        }


        [TestMethod]
        public void EnumValueInRunsOK() {

            Assert.IsTrue("Nope".EnumValueIn<EProperty>(en => en.GetDescription()));
            Assert.IsFalse("Noo".EnumValueIn<EProperty>(en => en.GetDescription()));

        }
        [TestMethod]
        public void EnlistEnumDescriptionsRunsOK() {

            var list = new List<String>().EnlistEnumDescriptions<EProperty>();
            Assert.AreEqual(3, list.Count);

        }

        [TestMethod]
        public void PassOverEnumRunsOK() {

            var list = new List<String>().PassOverEnum<EProperty>(en => en.GetDescription());
            Assert.AreEqual(3, list.Count);

        }

        public enum EProperty {

            [Display(Name = "Yes")]
            Yes,
			[Display(Name = "Nope")]
            No,
			[Display(Name = "NotApplicable")]
            NotApplicable

        }

        public enum EState {
            [Display(Name = "Published")]
            Published = 1,

			[Display(Name = "Accepted")]
            Accepted

        }


    }
}
