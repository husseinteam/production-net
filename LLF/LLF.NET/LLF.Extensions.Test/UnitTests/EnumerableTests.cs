﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LLF.Extensions;
using System.Collections.Generic;
using System.Threading;
using System.Collections;

namespace LLF.Extensions.Test.UnitTests {
    [TestClass]
    public class EnumerableTests {
        [TestMethod]
        public void SelectByWorksProperly() {

            var array = new Int32[] { 1, 2, 4 };
            var filtered = array.SelectBy(n => n % 2 == 1);
            Assert.AreEqual(1, filtered.Count());
            Assert.AreEqual(filtered.ElementAt(0), 1);

        }

        [TestMethod]
        public void ExtendRunsProperly() {

            var list = new List<Int32>() { 1, 2, 4 };
            list.Extend(5, 6, 7);
            Assert.AreEqual(6, list.Count);

            list.Extend(12).Extend(144);
            Assert.AreEqual(8, list.Count);

        }


        [TestMethod]
        public void InRunsOK() {

            var list = new List<Int32>() { 1, 2, 4 };
            var r = list.Shuffle();
            Assert.IsTrue(1.In(list.ToArray()));
            Assert.IsFalse(5.In(list.ToArray()));

        }
        [TestMethod]
        public void UniqueRunsOK() {

            var list = new List<Int32>() { 1, 2, 4, 4, 4, 6, 6, 7, 7, 9, 9, 9, 9 };
            var r = list.Unique();
            Assert.AreEqual(6, r.Count());

            var tagList = new List<TagItem>();
            10.Times(i => tagList.Add(new TagItem() { Display = i.ToString() }));
            var filtered = tagList.Unique(t => t.Display);
            Assert.AreEqual(10, filtered.Count());

            10.Times(idx => tagList.AddRange(10.Times(i => new TagItem() { Display = i.ToString() })));
            filtered = tagList.Unique(t => t.Display);
            Assert.AreEqual(11, filtered.Count());

        }

        [TestMethod]
        public void ReverseRunsOK() {

            var array = new Int32[] { 1, 2, 3, 4, 5, 6, 7 };
            var reversed = array.Invert();
            Assert.AreEqual(7, reversed.ElementAt(0));
        }

        [TestMethod]
        public void EnumerateRunsOK() {

            var arr = new Int32[] { 0, 1, 2, 3, 4, 5, 6 };
            arr.Enumerate((i, item) => Assert.AreEqual(i, item));

        }

        [TestMethod]
        public void TimesRunsOK() {

            var hundreds = 100.Times(k => new { Value = k, Text = k.ToString() });

            Assert.AreEqual(hundreds.ElementAt(2).Value, 3);


        }

        [TestMethod]
        public void PopulateRunsOK() {

            var hundreds = 100.Populate();

            Assert.AreEqual(hundreds.Count(), 100);


        }

        [TestMethod]
        public void GroupIntoByRunsOK() {

            var list = new Int32[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2 };
            var gr = list.GroupIntoBy(4);
            foreach (var fourthlet in gr) {
                Assert.AreEqual(4, fourthlet.Count());
                Assert.AreEqual(1, fourthlet.First() % 4);
            }

        }

        [TestMethod]
        public void WithPagingRunsOK() {

            var li = Enumerable.Repeat(0, 18).EnumerateFrom((i, e) => e + i);
            var paged = li.WithPaging(4, 5);
            Assert.AreEqual(3, paged.Count());
            Assert.IsTrue(paged.SequenceEqual(new Int32[] { 15, 16, 17 }));

            var paged2 = li.WithPaging(5, 5);
            Assert.AreEqual(0, paged2.Count());

            var paged3 = li.WithPaging(2, 5);
            Assert.AreEqual(5, paged3.Count());
            Assert.IsTrue(paged3.SequenceEqual(new Int32[] { 5, 6, 7, 8, 9 }));

        }

        [TestMethod]
        public void ThatTypeRunsOK() {

            IEnumerable arr = new Object[] { new TagItem() { Display = "disp" }, "hey", "there", new TagItem() { Display = "lay" } };
            Assert.AreEqual(2, arr.ThatType(typeof(TagItem)).OfType<TagItem>().Count());
            Assert.AreEqual(2, arr.ThatType(typeof(String)).OfType<String>().Count());

        }

    }

    public class TagItem {

        public String Display { get; set; }
    }

}
