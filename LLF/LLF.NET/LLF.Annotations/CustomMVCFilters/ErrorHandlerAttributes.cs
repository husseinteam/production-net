﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using LLF.Extensions;
using System.Web;

namespace LLF.Annotations.CustomMVCFilters {

    [AttributeUsage(AttributeTargets.Method, Inherited = false, AllowMultiple = false)]

    public sealed class AjaxErrorHandlerAttribute : FilterAttribute, IExceptionFilter {

        public void OnException(ExceptionContext filterContext) {

            filterContext.ExceptionHandled = true;
            filterContext.Result = new JsonResult {
                Data = new { Success = false, Message = filterContext.Exception.ToString() },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

        }

    }
    public class LLFErrorHandlerAttribute : HandleErrorAttribute {

        public LLFErrorHandlerAttribute(Type ExceptionType) {
            this.ExceptionType = ExceptionType;
        }

        public override void OnException(ExceptionContext filterContext) {
            if (filterContext.ExceptionHandled || !filterContext.HttpContext.IsCustomErrorEnabled) {
                return;
            }

            if (new HttpException(null, filterContext.Exception).GetHttpCode() != 500) {
                return;
            }

            if (!ExceptionType.IsInstanceOfType(filterContext.Exception)) {
                return;
            }

            // if the request is AJAX return JSON else view.
            if (filterContext.HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest") {
                filterContext.Result = new JsonResult {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = new {
                        Error = true,
                        Message = filterContext.Exception.Message
                    }
                };
            } else {
                var controllerName = (string)filterContext.RouteData.Values["controller"];
                var actionName = (string)filterContext.RouteData.Values["action"];
                var model = new HandleErrorInfo(filterContext.Exception, controllerName, actionName);

                filterContext.Result = new ViewResult {
                    ViewName = View,
                    ViewData = new ViewDataDictionary<HandleErrorInfo>(model),
                    TempData = filterContext.Controller.TempData
                };
            }

            // log the error using log4net.
            //_logger.Error(filterContext.Exception.Message, filterContext.Exception);

            filterContext.ExceptionHandled = true;
            filterContext.HttpContext.Response.TrySkipIisCustomErrors = true;
        }

    }

}
