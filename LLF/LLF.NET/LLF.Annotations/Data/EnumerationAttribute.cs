﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LLF.Annotations.Data {
    [System.AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public sealed class EnumerationAttribute : GenericColumnAttribute{

        public EnumerationAttribute() {

        }

    }
}
