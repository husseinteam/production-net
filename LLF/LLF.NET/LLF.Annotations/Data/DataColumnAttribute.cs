﻿using System;
using LLF.Extensions;
using LLF.Extensions.Data.Enums;

namespace LLF.Annotations.Data {
    [AttributeUsage(AttributeTargets.Property, Inherited = true, AllowMultiple = false)]
    public sealed class DataColumnAttribute : GenericColumnAttribute{

        readonly EDbType dataType;

        public DataColumnAttribute(EDbType dataType) {

            this.dataType = dataType;

        }

        public EDbType DataType {
            get { return dataType; }
        }

        public Int32 MaxLength { get; set; }
        public String ComputedMaxLength {
            get {
                if (this.dataType.IsDecimal()) {
                    return "";
                } else {
                    return this.MaxLength > 0 ? "({0})".PostFormat(this.MaxLength) : 
                        (this.MaxLength == -1 ? "(MAX)" : "");
                }
            }
        }
    }
}
