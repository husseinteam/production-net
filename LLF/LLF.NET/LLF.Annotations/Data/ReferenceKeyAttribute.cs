﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LLF.Annotations.Data {
    [System.AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = true)]
    public sealed class ReferenceKeyAttribute : GenericColumnAttribute{

        readonly string referenceKey;

        public ReferenceKeyAttribute(string referenceKey) {

            this.referenceKey = referenceKey;
        }

        public string ReferenceKey {

            get { return referenceKey; }

        }

        public Boolean IsUniqueIndex { get; set; }

    }
}
