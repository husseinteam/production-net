﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LLF.Annotations.Data {

    [System.AttributeUsage(AttributeTargets.Property, Inherited = true, AllowMultiple = false)]
    public sealed class UniqueAttribute : GenericColumnAttribute{

        public UniqueAttribute() {
        }

        // This is a named argument
        public String Order { get; set; }
    }
}
