﻿using LLF.Abstract.Data.Engine;
using LLF.Abstract.Xml;
using LLF.Data.Containers;
using LLF.Extensions;
using LLF.Data.Engine;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LLF.Data.Tools {

    public static class DbProvider {

        public static TEntity ProvideEntity<TEntity>()
            where TEntity : IBaseEntity<TEntity> {

            return Activator.CreateInstance<TEntity>();

        }

        public static Exception WipeDatabase() {


            var spClearAllTables =
                ResourceContainer.ReadXmlFor<IDatabaseTemplate>("CallSprocsTemplate")
                .Script.PostFormat(CredentialProvider.CurrentCredential.Database, "spClearAllTables");
            using (var conn = CredentialProvider.CurrentCredential.ProvideSuperAdminConnection()) {
                conn.Open();
                try {
                    conn.RunSql(spClearAllTables);
                    return null;
                } catch (Exception ex) {
                    return ex;
                } finally {
                    conn.Close();
                }
            }

        }

    }

}
