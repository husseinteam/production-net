﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using LLF.Extensions;
using LLF.Annotations.Data;
using LLF.Data.Tools;
using LLF.Extensions.Data.Enums;
using LLF.Data.Containers;
using LLF.Abstract.Xml;

namespace LLF.Data.EntityEngine.Tools {
    internal static class AliasFormatter {

        internal static Dictionary<Type, String> _AliasDict = new Dictionary<Type, string>();
        internal static String MakeTableAlias(Type t) {

            if (!_AliasDict.ContainsKey(t)) {
                var val = Guid.NewGuid().ToString("N");
                _AliasDict[t] = val;
            }
            return "{0}_{1}".PostFormat(t.Name.ToLowerInvariant(), _AliasDict[t]);

        }

        internal static String MakeColumnAlias(String tableAlias, String columnName) {

            return "{0}_{1}".PostFormat(tableAlias, columnName);

        }

        internal static String MakeTableName(EntityTitleAttribute eattr) {

            if (CredentialProvider.CurrentCredential.ServiceType == EServiceType.MySQL ||
                CredentialProvider.CurrentCredential.ServiceType == EServiceType.AzureMySql) {
                return eattr.Title;
            } else {
                return eattr.ToString();
            }



        }
        internal static String Shematize(this EntityTitleAttribute eattr, String suffix) {

            var template = ResourceContainer.ReadXmlFor<IDatabaseTemplate>("ShematizedTemplate").Script;
            return template.PostFormat(eattr.Schema.DefaultIfNullOrEmpty("dbo"), suffix);

        }

    }
}
