﻿using LLF.Abstract.Data.Engine;
using LLF.Extensions.Data.Enums;
using LLF.Abstract.Xml;
using LLF.Annotations.Data;
using LLF.Data.Containers;
using LLF.Data.EntityEngine.Tools;
using LLF.Data.Tools;
using LLF.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Collections;

namespace LLF.Data.Engine {

    public class BaseEntity<TEntity> : LLFluent<TEntity>, IBaseEntity<TEntity>
        where TEntity : class, IBaseEntity<TEntity> {

        #region Ctor

        public BaseEntity() {

            Alias = AliasFormatter.MakeTableAlias(typeof(TEntity));
            GeneratedOn = DateTime.Now;

        }

        #endregion

        #region Generators

        internal IDbCommand GenerateSelectCommand(IDbConnection conn, object selector = null, object selectornot = null, ESelectClauseType genType = ESelectClauseType.AllColumns) {

            var et = GetEntityTitle();
            var sqlb = new StringBuilder(
                "SELECT #select# FROM {0} {1} #join# ".PostFormat(
                AliasFormatter.MakeTableName(et), Alias));
            GenerateCommandBody(sqlb, genType);
            var qtext = sqlb.Replace("#join#", "").Replace(", #select#", "").ToString();

            var where = new StringBuilder();
            IDbCommand command = CredentialProvider.CurrentCredential.ProvideDbCommand();
            var whereTemplate = ResourceContainer
                .ReadXmlFor<IDatabaseTemplate>("WhereTemplate").Script;
            if (selector != null) {
                var dict = selector as IDictionary<String, Object>;
                if (dict != null) {
                    foreach (var kv in dict) {
                        command.Parameters.Add(CredentialProvider.CurrentCredential
                            .ProvideDbParameter(kv.Key, kv.Value));
                        where.AppendFormat(whereTemplate.PostFormat(Alias, kv.Key, kv.Key));
                    }
                } else {
                    foreach (var spi in selector.GetAnonymousProperties()) {
                        command.Parameters.Add(CredentialProvider.CurrentCredential
                            .ProvideDbParameter(spi.Name, spi.GetValue(selector, null)));
                        where.AppendFormat(whereTemplate, Alias, spi.Name, spi.Name);
                    }
                }
            }
            if (selectornot != null) {
                var dict = selectornot as IDictionary<String, Object>;
                if (dict != null) {
                    foreach (var kv in dict) {
                        if (!command.Parameters.Contains(kv.Key)) {
                            command.Parameters.Add(CredentialProvider.CurrentCredential
                                .ProvideDbParameter(kv.Key, kv.Value));
                        }
                        where.AppendFormat(whereTemplate, Alias, kv.Key, kv.Key);
                    }
                } else {
                    foreach (var spi in selectornot.GetAnonymousProperties()) {
                        if (!command.Parameters.Contains(spi.Name)) {
                            command.Parameters.Add(CredentialProvider.CurrentCredential
                                .ProvideDbParameter(spi.Name, spi.GetValue(selectornot, null)));
                        }
                        where.AppendFormat(whereTemplate, Alias, spi.Name, spi.Name);
                    }
                }
            }
            var wh = where.ToString().TrimLast(" AND ");
            command.CommandText = "{0} {1}".PostFormat(qtext,
                        wh.IsUnassigned() ? "" : "WHERE {0}".PostFormat(wh));
            command.Connection = conn;
            return command;

        }

        internal void GenerateCommandBody(StringBuilder sqlb, ESelectClauseType genType = ESelectClauseType.AllColumns) {

            var columnTemplate = ResourceContainer
                .ReadXmlFor<IDatabaseTemplate>("SelectColumnTemplate").Script;
            if (genType == ESelectClauseType.AllColumns) {
                var pkcols = GetPrimaryKeys();
                foreach (var pkpi in pkcols) {
                    sqlb.Replace("#select#", columnTemplate.PostFormat(
                        Alias, pkpi.Name, AliasFormatter.MakeColumnAlias(Alias, pkpi.Name)));
                }
                if (base.LLFluentActive) {
                    foreach (var col in LLFluentColumnModifiers) {
                        sqlb.Replace("#select#", columnTemplate.PostFormat(
                            Alias, col.Name, AliasFormatter.MakeColumnAlias(Alias, col.Name)));
                    }
                    foreach (var fkcol in LLFluentFKModifiers) {
                        var refColInstance = Activator.CreateInstance(fkcol.PropertyType, true);
                        var refTableAlias = refColInstance.PropertyFor<String>("Alias");
                        var selfET = GetEntityTitle();
                        var referenceET = refColInstance.PropertyFor<EntityTitleAttribute>("EntityTitle");
                        //var pkattr = this.LLFluentPKModifiers.First();
                        sqlb.Replace("#join#", @"
                        left join {0} {1} on {1}.{2} = {3}.{4}
                        #join#".PostFormat(AliasFormatter.MakeTableName(referenceET), refTableAlias,
                                fkcol.ReferencedKey, Alias, fkcol.FKKey));
                        this.GetType().GetProperties().Single(pi => pi.Name == fkcol.Name).SetValue(this, refColInstance);
                        if (!fkcol.PropertyType.Equals(typeof(TEntity))) {
                            refColInstance.RunMethod("GenerateCommandBody", new Object[] { sqlb, genType });
                        }
                    }
                } else {
                    var cols = GetColumns();

                    foreach (var colpi in cols) {
                        var dt = colpi.GetCustomAttributes(typeof(DataColumnAttribute), true)
                           .FirstOrDefault() as DataColumnAttribute;
                        var en = colpi.GetCustomAttributes(typeof(EnumerationAttribute), true)
                           .FirstOrDefault() as EnumerationAttribute;
                        var rk = colpi.GetCustomAttribute<ReferenceKeyAttribute>();
                        if (dt != null || en != null) {
                            sqlb.Replace("#select#", columnTemplate.PostFormat(
                                Alias, colpi.Name, AliasFormatter.MakeColumnAlias(Alias, colpi.Name)));
                        } else if (rk != null) {
                            sqlb.Replace("#select#", columnTemplate.PostFormat(
                                Alias, rk.ReferenceKey, AliasFormatter.MakeColumnAlias(Alias,
                                    rk.ReferenceKey)));
                        }
                    }

                    foreach (var colpi in cols) {
                        var refkey = colpi.GetCustomAttribute<ReferenceKeyAttribute>();
                        if (refkey != null) {
                            var refColInstance = Activator.CreateInstance(colpi.PropertyType, true);
                            var refTableAlias = refColInstance.PropertyFor<String>("Alias");
                            var selfET = GetEntityTitle();
                            var referenceET = colpi.PropertyType.GetCustomAttribute<EntityTitleAttribute>();
                            var pkattr = GetPrimaryKeys().First();
                            var referencedPKName = colpi.PropertyType.GetProperties().Single(pp => pp.GetCustomAttribute<PrimaryKeyAttribute>() != null).Name;
                            sqlb.Replace("#join#", @"
                        left join {0} {1} on {1}.{2} = {3}.{4}
                        #join#".PostFormat(AliasFormatter.MakeTableName(referenceET), refTableAlias,
                                    pkattr.Name, Alias, refkey.ReferenceKey));
                            colpi.SetValue(this.As<TEntity>(), refColInstance);
                            if (!colpi.PropertyType.Equals(typeof(TEntity))) {
                                refColInstance.RunMethod("GenerateCommandBody", new Object[] { sqlb, genType });
                            }
                        }
                    }
                }

            } else if (genType == ESelectClauseType.Count) {
                sqlb.Replace("#select#", "COUNT(*)");
            }

        }

        private IDbDataParameter[] GenerateParameters(Func<PropertyInfo, DataColumnAttribute, IDbDataParameter> rowActor,
            Func<PropertyInfo, ReferenceKeyAttribute, IDbDataParameter> rkActor,
            Action<PropertyInfo, IList> rcActor,
            Func<PropertyInfo, EnumerationAttribute, IDbDataParameter> enumActor) {

            var plist = new List<IDbDataParameter>();
            var cols = GetColumns();

            foreach (var colpi in cols) {
                var colvalue = colpi.GetValue(this, null);
                if (!colvalue.IsUnassigned()) {
                    if (colpi.GetCustomAttribute<EnumerationAttribute>() != null) {
                        plist.Add(enumActor(colpi, colpi.GetCustomAttribute<EnumerationAttribute>()));
                    }
                }
                if (colpi.GetCustomAttribute<DataColumnAttribute>() != null) {
                    plist.Add(rowActor(colpi, colpi.GetCustomAttribute<DataColumnAttribute>()));
                }
            }

            foreach (var colpi in cols) {
                var colvalue = colpi.GetValue(this, null);
                if (!colvalue.IsUnassigned()) {
                    if (colpi.GetCustomAttribute<ReferenceKeyAttribute>() != null) {
                        plist.Add(rkActor(colpi, colpi.GetCustomAttribute<ReferenceKeyAttribute>()));
                    }
                }
            }

            foreach (var colpi in cols) {
                var colvalue = colpi.GetValue(this, null);
                if (!colvalue.IsUnassigned()) {
                    if (colpi.GetCustomAttribute<ReferenceCollectionAttribute>() != null) {
                        var li = colpi.GetValue(this) as IList;
                        rcActor(colpi, li);
                    }
                }
            }

            return plist.ToArray();

        }

        internal EntityTitleAttribute GetEntityTitle() {

            EntityTitleAttribute entityTitle = null;
            if (this.LLFluentActive) {
                entityTitle = this.EntityTitle;
            } else {
                entityTitle = typeof(TEntity).GetClassDecoration<EntityTitleAttribute>();
                if (entityTitle == null) {
                    throw new InvalidOperationException("Should have EntityTitleAttribute");
                }
            }
            return entityTitle;

        }

        #endregion

        #region Select

        public TEntity SelectThe(Int64 id) {

            using (IDbConnection conn = CredentialProvider.CurrentCredential.ProvideSuperAdminConnection()) {
                var pk = GetPrimaryKeys().First();
                var cmd = GenerateSelectCommand(conn, pk.Name.GenerateAnonymousObject(id));
                cmd.Connection.Open();
                try {
                    return cmd.Locate<TEntity>(this.Like<TEntity>());
                } catch {
                    return default(TEntity);
                } finally {
                    cmd.Connection.Close();
                }
            }

        }

        public TScalar SelectCount<TScalar>(object selector = null, object selectornot = null) {

            using (IDbConnection conn = CredentialProvider.CurrentCredential.ProvideSuperAdminConnection()) {
                var cmd = GenerateSelectCommand(conn, selector, selectornot, ESelectClauseType.Count);
                cmd.Connection.Open();
                try {
                    return cmd.Scalar<TScalar>();
                } catch {
                    return default(TScalar);
                } finally {
                    cmd.Connection.Close();
                }
            }

        }

        public TEntity SelectOne(object selector = null) {

            using (IDbConnection conn = CredentialProvider.CurrentCredential.ProvideSuperAdminConnection()) {
                var cmd = GenerateSelectCommand(conn, selector);
                cmd.Connection.Open();
                try {
                    return cmd.Locate<TEntity>(this.Like<TEntity>());
                } catch {
                    return default(TEntity);
                } finally {
                    cmd.Connection.Close();
                }
            }

        }

        public ICollection<TEntity> SelectMany(object selector = null, object selectornot = null) {

            using (IDbConnection conn = CredentialProvider.CurrentCredential.ProvideSuperAdminConnection()) {
                var cmd = GenerateSelectCommand(conn, selector, selectornot);
                cmd.Connection.Open();
                try {
                    return cmd.Populate<TEntity>(this.Like<TEntity>());
                } catch {
                    return new List<TEntity>().ToArray();
                } finally {
                    cmd.Connection.Close();
                }
            }

        }

        #endregion

        #region CRUD

        public TEntity InsertSelf() {

            StringBuilder paramList = new StringBuilder(), columnList = new StringBuilder();
            var insertColumnTemplate = ResourceContainer
                .ReadXmlFor<IDatabaseTemplate>("InsertColumnTemplate").Script;
            var parameterTemplate = ResourceContainer
                .ReadXmlFor<IDatabaseTemplate>("ParameterTemplate").Script;
            var p = GenerateParameters((colpi, rowattr) => {
                var colvalue = colpi.GetValue(this, null);
                colvalue = Object.ReferenceEquals(colvalue, null) ? (Object)DBNull.Value : colvalue;
                var dt = colpi.GetCustomAttribute<DefaultValueAttribute>();
                paramList.AppendFormat(parameterTemplate, colpi.Name);
                columnList.AppendFormat(insertColumnTemplate, colpi.Name);
                if (dt != null) {
                    return CredentialProvider.CurrentCredential.ProvideDbParameter(colpi.Name, colvalue ??
                        Convert.ChangeType(dt.Value, colpi.PropertyType));
                } else {
                    return CredentialProvider.CurrentCredential.ProvideDbParameter(colpi.Name, colvalue);
                }
            }, (colpi, rkattr) => {
                var colvalue = colpi.GetValue(this, null);
                colvalue = colvalue.IsUnassigned() ? (Object)DBNull.Value : colvalue;
                var rkeyAttribute = colpi.GetCustomAttribute<ReferenceKeyAttribute>();
                paramList.AppendFormat(parameterTemplate, rkeyAttribute.ReferenceKey);
                columnList.AppendFormat(insertColumnTemplate, rkeyAttribute.ReferenceKey);
                var referencedPKName = colpi.PropertyType.GetProperties().Single(pp => pp.GetCustomAttribute<PrimaryKeyAttribute>() != null).Name;
                var referencedPKValue = colpi.PropertyType.PropertyFor(referencedPKName)
                    .GetValue(colvalue, null);
                if (referencedPKValue.IsUnassigned()) {
                    colvalue.RunMethod("InsertSelf", null);
                    referencedPKValue = colpi.PropertyType.PropertyFor(referencedPKName)
                        .GetValue(colvalue, null);
                }
                return CredentialProvider.CurrentCredential.ProvideDbParameter(
                    rkeyAttribute.ReferenceKey, referencedPKValue);
            }, (colpi, refList) => {

            }, (colpi, enumattr) => {
                paramList.AppendFormat(parameterTemplate, colpi.Name);
                columnList.AppendFormat(insertColumnTemplate, colpi.Name);
                return CredentialProvider.CurrentCredential.ProvideDbParameter(
                    colpi.Name, colpi.GetValue(this, null).ToCode());
            });

            var values = "VALUES({0})".PostFormat(paramList.ToString().Trim().TrimEnd(','));
            var inserts = "({0})".PostFormat(columnList.ToString().Trim().TrimEnd(','));

            var sql = "INSERT INTO {0} {1} {2}".PostFormat(AliasFormatter.MakeTableName(GetEntityTitle()),
                inserts != "()" ? inserts : "", values != "VALUES()" ? values : "DEFAULT VALUES");

            IdentityInsert(sql, p);
            return this.Like<TEntity>();
        }

        public TEntity UpdateSelf() {

            var where = new StringBuilder();
            var whereValueTemplate = ResourceContainer
                .ReadXmlFor<IDatabaseTemplate>("WhereValueTemplate").Script;
            var setTemplate = ResourceContainer
                .ReadXmlFor<IDatabaseTemplate>("SetTemplate").Script;
            GetPrimaryKeys().Enumerate(pkpi => where.AppendFormat(whereValueTemplate,
                pkpi.Name, pkpi.GetValue(this, null)));

            var setList = new StringBuilder();
            var p = GenerateParameters((colpi, rowattr) => {
                var colvalue = colpi.GetValue(this, null);
                colvalue = Object.ReferenceEquals(colvalue, null) ? (Object)DBNull.Value : colvalue;
                setList.AppendFormat(setTemplate, colpi.Name);
                return CredentialProvider.CurrentCredential.ProvideDbParameter(colpi.Name, colvalue);
            }, (colpi, rkattr) => {
                var colvalue = colpi.GetValue(this, null);
                colvalue = colvalue.IsUnassigned() ? (Object)DBNull.Value : colvalue;
                var rkeyAttribute = colpi.GetCustomAttribute<ReferenceKeyAttribute>();
                var referencedPKName = colpi.PropertyType.GetProperties().Single(pp => pp.GetCustomAttribute<PrimaryKeyAttribute>() != null).Name;
                var referencedPKValue = colpi.PropertyType.PropertyFor(referencedPKName)
                    .GetValue(colvalue, null);
                if (!referencedPKValue.IsUnassigned()) {
                    colvalue.RunMethod("UpdateSelf", null);
                    referencedPKValue = colpi.PropertyType.PropertyFor(referencedPKName)
                        .GetValue(colvalue, null);
                }
                setList.AppendFormat(setTemplate, rkeyAttribute.ReferenceKey);
                return CredentialProvider.CurrentCredential.ProvideDbParameter(
                    rkeyAttribute.ReferenceKey, referencedPKValue);
            }, (colpi, refList) => {

            }, (colpi, enumattr) => {
                setList.AppendFormat(setTemplate, colpi.Name);
                return CredentialProvider.CurrentCredential.ProvideDbParameter(
                    colpi.Name, colpi.GetValue(this, null).ToCode());
            });

            var sql = "UPDATE {0} SET {1} WHERE {2}".PostFormat(
                AliasFormatter.MakeTableName(GetEntityTitle())
                , setList.ToString().Trim().TrimEnd(','), where.ToString().TrimLast(" AND "));
            ExecuteSql(sql, p);
            return this.Like<TEntity>();

        }

        public TEntity UpsertSelf() {

            var pkcols = GetPrimaryKeys();
            var pk = pkcols.First();
            if (pk.GetValue(this).IsUnassigned()) {
                return this.InsertSelf();
            } else {
                return this.UpdateSelf();
            }

        }

        public TEntity DeleteSelf(Boolean cascade = false) {

            var where = new StringBuilder();
            var whereValueTemplate = ResourceContainer
                .ReadXmlFor<IDatabaseTemplate>("WhereValueTemplate").Script;

            GetPrimaryKeys().Enumerate(pkpi => where.AppendFormat(whereValueTemplate,
               pkpi.Name, pkpi.GetValue(this, null)));
            var sql = "DELETE FROM {0} WHERE {1}".PostFormat(AliasFormatter.MakeTableName(GetEntityTitle())
                , where.ToString().TrimLast(" AND "));

            if (cascade) {
                GenerateParameters((colpi, rowattr) => {
                    var colvalue = colpi.GetValue(this, null);
                    colvalue = colvalue.IsUnassigned() ? (Object)DBNull.Value : colvalue;
                    return CredentialProvider.CurrentCredential.ProvideDbParameter(colpi.Name, colvalue);
                }, (colpi, rkattr) => {
                    var colvalue = colpi.GetValue(this, null);
                    colvalue = colvalue.IsUnassigned() ? (Object)DBNull.Value : colvalue;
                    var rkeyAttribute = colpi.GetCustomAttribute<ReferenceKeyAttribute>();
                    var referencedPKName = colpi.PropertyType.GetProperties().Single(pp => pp.GetCustomAttribute<PrimaryKeyAttribute>() != null).Name;
                    var referencedPKValue = colpi.PropertyType.PropertyFor(referencedPKName)
                        .GetValue(colvalue, null);
                    if (!referencedPKValue.IsUnassigned()) {
                        if (cascade) {
                            colvalue.RunMethod("SetReloopType", new object[] { typeof(TEntity) });
                        }
                        if (this.reloopType != null) {
                            if (!this.reloopType.TypesEqual(colpi.PropertyType)) {
                                colvalue.RunMethod("DeleteSelf", new object[] { cascade });
                                referencedPKValue = colpi.PropertyType.PropertyFor(referencedPKName)
                                    .GetValue(colvalue, null);
                            }
                        } else {
                            colvalue.RunMethod("DeleteSelf", new object[] { cascade });
                            referencedPKValue = colpi.PropertyType.PropertyFor(referencedPKName)
                                .GetValue(colvalue, null);
                        }
                    }
                    return CredentialProvider.CurrentCredential.ProvideDbParameter(
                        rkeyAttribute.ReferenceKey, referencedPKValue);
                }, (colpi, refList) => {
                    foreach (var item in refList) {
                        if (cascade) {
                            item.RunMethod("SetReloopType", new object[] { typeof(TEntity) });
                        }
                        if (this.reloopType != null) {
                            if (!this.reloopType.TypesEqual(colpi.PropertyType)) {
                                item.RunMethod("DeleteSelf", new object[] { cascade });
                            }
                        } else {
                            item.RunMethod("DeleteSelf", new object[] { cascade });
                        }
                    }
                }, (colpi, enumattr) => {
                    return CredentialProvider.CurrentCredential.ProvideDbParameter(
                        colpi.Name, colpi.GetValue(this, null).ToCode());
                });
            }

            ExecuteSql(sql);
            return this.Like<TEntity>();

        }

        public TEntity UpsertCollections<TElement>()
            where TElement : class, IBaseEntity<TElement> {

            var cols = GetColumns();

            foreach (var colpi in cols) {
                var colvalue = colpi.GetValue(this, null);
                if (!colvalue.IsUnassigned()) {
                    if (colpi.GetCustomAttribute<ReferenceCollectionAttribute>() != null) {
                        var li = colpi.GetValue(this, new Object[] { }) as IList;
                        var elementType = colpi.PropertyType.GetGenericArguments().First();
                        var refPk = elementType.GetPublicProperties().Single(
                            pi => pi.GetCustomAttribute<ReferenceKeyAttribute>() != null &&
                            pi.PropertyType.Equals(typeof(TEntity)));
                        var pkid = GetPrimaryKeys().First().GetValue(this, null);
                        if (!pkid.IsUnassigned()) {
                            var refkey = refPk.GetCustomAttribute<ReferenceKeyAttribute>().ReferenceKey;
                            li.OfType<TElement>().Enumerate(ent => ent.Assign(e =>
                                elementType.PropertyFor(refPk.Name).SetValue(e, this)).UpsertSelf());
                        }
                    }
                }
            }
            return this.Like<TEntity>();

        }

        public IEntityDefiner<TEntity> Truncate() {

            var truncateTableTemplate = ResourceContainer
                .ReadXmlFor<IDatabaseTemplate>("TruncateTableTemplate").Script;
            var eTitle = GetEntityTitle();
            var statement = truncateTableTemplate.PostFormat(eTitle.Title, eTitle.Schema);
            ExecuteSql(statement);
            return this;

        }

        public void SetReloopType(Type reloopType) {

            this.reloopType = reloopType;

        }
        private Type reloopType = null;

        #endregion

        #region Sql Execution

        private void IdentityInsert(String sql, params IDbDataParameter[] parameters) {

            var pkpi = GetPrimaryKeys().First();
            using (IDbConnection conn = CredentialProvider.CurrentCredential.ProvideSuperAdminConnection()) {
                conn.Open();
                try {
                    conn.RunSql(sql, parameters);
                    pkpi.SetValue(this, conn.Identity());
                } catch (Exception ex) {
                    Fault = ex;
                } finally {
                    conn.Close();
                }
            }

        }

        private void ExecuteSql(String sql, params IDbDataParameter[] parameters) {

            using (IDbConnection conn = CredentialProvider.CurrentCredential.ProvideSuperAdminConnection()) {
                conn.Open();
                try {
                    conn.RunSql(sql, parameters);
                    Fault = Fault ?? null;
                } catch (Exception ex) {
                    Fault = ex;
                } finally {
                    conn.Close();
                }
            }

        }

        #endregion

        #region Properties

        internal String Alias { get; set; }
        public Exception Fault { get; set; }

        #endregion

        #region Implemented

        public TEntity ReadSelf(IDataReader reader) {

            var pkcols = GetPrimaryKeys();
            foreach (var pkpi in pkcols) {
                var key = AliasFormatter.MakeColumnAlias(Alias, pkpi.Name);
                var value = reader[key];
                pkpi.SetValue(this, value);
            }

            var cols = GetColumns();
            foreach (var colpi in cols) {
                if (colpi.GetCustomAttribute<DataColumnAttribute>() != null
                    || colpi.GetCustomAttribute<EnumerationAttribute>() != null) {
                    var key = AliasFormatter.MakeColumnAlias(Alias, colpi.Name);
                    if (reader[key] != DBNull.Value) {
                        Object value;
                        if (!colpi.PropertyType.IsEnum &&
                            typeof(IConvertible).IsAssignableFrom(colpi.PropertyType)) {
                            value = Convert.ChangeType(reader[key], colpi.PropertyType);
                        } else {
                            value = reader[key];
                        }
                        colpi.SetValue(this, value);
                    }
                }
            }
            foreach (var colpi in cols) {
                var rk = colpi.GetCustomAttributes(typeof(ReferenceKeyAttribute), true)
                    .FirstOrDefault() as ReferenceKeyAttribute;
                if (rk != null) {
                    if (!colpi.PropertyType.Equals(typeof(TEntity))) {
                        var reference = colpi.GetValue(this, null);
                        var key = AliasFormatter.MakeColumnAlias(Alias, rk.ReferenceKey);
                        if (reader[key] != DBNull.Value && !reader[key].IsUnassigned()) {
                            Object value;
                            if (!colpi.PropertyType.IsEnum &&
                                typeof(IConvertible).IsAssignableFrom(colpi.PropertyType)) {
                                value = Convert.ChangeType(reader[key], colpi.PropertyType);
                            } else {
                                value = reader[key];
                            }
                            if (!value.IsUnassigned()) {
                                reference = reference ?? Activator.CreateInstance(colpi.PropertyType);
                                (reference as IIdentity).ID = Convert.ToInt64(value);
                                reference.RunMethod("ReadSelf", new object[] { reader });
                            }
                            colpi.SetValue(this, reference);
                        } else {
                            colpi.SetValue(this, null);
                        }
                    }
                }
            }
            return this.Like<TEntity>();

        }

        public TEntity Include<TElement>() {

            var cols = GetColumns();
            foreach (var colpi in cols) {
                var rk = colpi.GetCustomAttributes(typeof(ReferenceCollectionAttribute), true)
                    .FirstOrDefault() as ReferenceCollectionAttribute;
                if (rk != null) {
                    var pk = GetPrimaryKeys().First();
                    var elementType = colpi.PropertyType.GetGenericArguments().First();
                    if (elementType == typeof(TElement)) {
                        var element = Activator.CreateInstance(elementType);
                        var fkname = element.GetAnonymousProperties().SingleOrDefault(
                                fkpi => fkpi.PropertyType.TypesEqual(typeof(TEntity)))
                                .GetCustomAttribute<ReferenceKeyAttribute>().ReferenceKey;
                        var fkValue = pk.GetValue(this);
                        var referenceCount = element.RunMethod("SelectCount",
                            new Object[] { fkname.GenerateAnonymousObject(fkValue), null }, new Type[] { typeof(Int32) }).ToType<Int32>();
                        if (referenceCount > 0) {
                            var referenceList = element.RunMethod("SelectMany", new Object[] { fkname.GenerateAnonymousObject(fkValue), null }, Type.EmptyTypes);
                            var li = Convert.ChangeType(colpi.GetValue(this, new Object[] { }), typeof(List<>).MakeGenericType(elementType)) as IList;
                            li.Clear();
                            foreach (var re in (referenceList as ICollection)) {
                                var fkprop = re.GetAnonymousProperties().SingleOrDefault(
                                    fkpi => fkpi.PropertyType.TypesEqual(typeof(TEntity)));
                                fkprop.SetValue(re, this);
                                li.Add(re);
                            }
                            colpi.SetValue(this, li);
                        }
                        break;
                    }
                }
            }
            return this.Like<TEntity>();

        }

        internal IEnumerable<PropertyInfo> GetColumns() {

            return typeof(TEntity).GetPublicProperties()
                .Where(pi => pi.GetCustomAttributes<GenericColumnAttribute>(true).Count() > 0)
                .Where(pi => pi.GetCustomAttribute<PrimaryKeyAttribute>() == null);

        }

        internal IEnumerable<PropertyInfo> GetPrimaryKeys() {

            var cols = typeof(TEntity).GetPublicProperties();
            foreach (var colpi in cols) {
                if (colpi.GetCustomAttribute<PrimaryKeyAttribute>() != null) {
                    yield return colpi;
                }
            }
        }

        #endregion

        #region Entity Inherited Properties

        [PrimaryKey, Identity, DataColumn(EDbType.Long)]
        public Int64 ID { get; set; }

        [DataColumn(EDbType.DateTime)]
        public DateTime GeneratedOn { get; set; }

        #endregion

    }

}
