﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Extensions;
using LLF.Abstract.Data.Engine;
using LLF.Data.Engine;
using LLF.Abstract.Xml;
using Ninject;
using LLF.Data.Xml;
using LLF.Static;
using LLF.Data.Tools;
using LLF.Data.Containers;

namespace LLF.Data.Engine {

	internal class DatabaseEngine : IDatabaseEngine {

		#region Ctor

		public DatabaseEngine(IKernel container) {

			this._Container = container;
			this._ScriptEngine = container.Get<IScriptEngine>();

		}

		#endregion

		#region Fields

		private readonly IScriptEngine _ScriptEngine;

		private readonly IKernel _Container;

		#endregion

		#region IDatabaseEngine Implementation

		public IDatabaseEngine EngineFor<TEntity>()
			where TEntity : IBaseEntity<TEntity>, new() {

			this._ScriptEngine.GenerateModelFor<TEntity>();
			return this;

		}

		public Exception GenerateModels() {

			Exception expt = null;
			using (IDbConnection conn = CredentialProvider.CurrentCredential.ProvideSuperAdminConnection()) {
				try {
					var script = ResourceContainer
                        .ReadXmlFor<IDatabaseTemplate>("spClearAllTables").Script;
					var aggr = this._ScriptEngine
                        .GenerateSproc(script, "spClearAllTables", CredentialProvider.CurrentCredential.Database);
					conn.Open();
					conn.RunSqlScript(aggr.Sprocs);
					conn.RunSqlScript(aggr.Finalize());
					return null;
				} catch (Exception ex) {
					expt = ex;
				} finally {
					conn.Close();
				}
			}
			return expt;

		}

		#endregion

		private static Exception ExecuteByGo(IDbConnection conn, IEnumerable<String> scripts) {
			Exception expt = null;
			foreach (var sql in scripts) {
				try {
					conn.Open();
					conn.RunSqlScript(sql);
				} catch (Exception ex) {
					expt = ex;
					break;
				} finally {
					conn.Close();
				}
			}
			return expt;
		}

	}

}
