﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using LLF.Annotations.Data;
using LLF.Extensions.Data.Enums;
using LLF.Abstract.Data.Engine;
using LLF.Extensions;
using MySql.Data.MySqlClient;
using LLF.Data.Tools;
using LLF.Data.EntityEngine.Tools;

namespace LLF.Data.Engine {

    public static class EngineExtensions {

        public static TEntity Locate<TEntity>(this IDbCommand command, TEntity entity)
            where TEntity : IBaseEntity<TEntity> {

            var reader = command.ExecuteReader(CommandBehavior.CloseConnection);
            if (reader.Read()) {
                var read = entity.ReadSelf(reader);
                reader.Close();
                return read;
            } else {
                return default(TEntity);
            }

        }

        public static ICollection<TEntity> Populate<TEntity>(this IDbCommand command, TEntity entity)
            where TEntity : IBaseEntity<TEntity> {

            var reader = command.ExecuteReader(CommandBehavior.CloseConnection);
            var entArr = new List<TEntity>();
            while (reader.Read()) {
                var e = entity.ReadSelf(reader)
                    .ShallowCopy();
                entArr.Add(e);
            }
            reader.Close();
            return entArr.ToArray();

        }

        public static TScalar Scalar<TScalar>(this IDbCommand command) {

            return command.ExecuteScalar().ToType<TScalar>();

        }

        public static Int64 Identity(this IDbConnection conn) {

            var command = PrepareCommand(conn, "SELECT @@IDENTITY ID");
            return command.ExecuteScalar().ToType<Int64>();

        }

        public static IEnumerable<TEnumerable> Collect<TEntity, TEnumerable>(this TEntity self)
            where TEntity : BaseEntity<TEntity>
            where TEnumerable : BaseEntity<TEnumerable>, new() {

            var cols = self.GetColumns();
            var pkcols = self.GetPrimaryKeys();
            var colpi = cols.Where(pi => pi.PropertyType
                        .GetGenericArguments().FirstOrDefault() != null)
                        .Single(pi =>
                            pi.PropertyType.GetGenericArguments().First().Equals(typeof(TEnumerable)));
            var collkey = colpi.GetCustomAttribute<ReferenceCollectionAttribute>();

            if (collkey != null) {
                var elementType = typeof(TEnumerable);
                var selfTargetingPi = elementType.GetProperties()
                    .Where(pi => pi.GetCustomAttribute<ReferenceKeyAttribute>() != null
                        && pi.PropertyType.Equals(typeof(TEntity))).First();
                var selfTargetKey = selfTargetingPi
                    .GetCustomAttribute<ReferenceKeyAttribute>().ReferenceKey;
                //selfTargetKey = SFormatter.MakeColumnAlias(
                //    SFormatter.MakeTableAlias(elementType), selfTargetKey);

                IDictionary<String, object> selector = new { }.ToExpando();
                selector[selfTargetKey] = pkcols.First().GetValue(self, null);

                var many = new TEnumerable().SelectMany(selector);
                colpi.SetValue(self, many);
                return many;
            }
            return new List<TEnumerable>().ToArray();

        }

        public static Int64 RunSqlScript(this IDbConnection conn, String sqlScript,
            params IDbDataParameter[] parameters) {

            DbCommand command = null;
            if (conn is SqlConnection) {
                command = new SqlCommand(sqlScript, conn as SqlConnection);
            } else if (conn is MySqlConnection) {
                command = new MySqlCommand(sqlScript, conn as MySqlConnection);
                command.Prepare();
            }
            if (parameters != null) {
                foreach (var prm in parameters) {
                    CredentialProvider.CurrentCredential.SetDbParameter(command, prm);
                }
            }
            if (conn is MySqlConnection) {
                var script = new MySqlScript(conn as MySqlConnection, sqlScript);
                script.Delimiter = "$$";
                return script.Execute();
            } else {
                return command.ExecuteNonQuery();
            }

        }

        public static Int64 RunSql(this IDbConnection conn, String sqlScript,
            params IDbDataParameter[] parameters) {

            var command = PrepareCommand(conn, sqlScript);
            if (parameters != null) {
                foreach (var prm in parameters) {
                    CredentialProvider.CurrentCredential.SetDbParameter(command, prm);
                }
            }
            return command.ExecuteNonQuery();

        }

        private static DbCommand PrepareCommand(IDbConnection conn, String sqlScript) {

            DbCommand command = null;
            if (conn is SqlConnection) {
                command = new SqlCommand(sqlScript, conn as SqlConnection);
            } else if (conn is MySqlConnection) {
                command = new MySqlCommand(sqlScript, conn as MySqlConnection);
                command.Prepare();
            }
            return command;

        }

        public static DbDataReader RunStoredProcedure(this IDbConnection conn, String name,
            params IDbDataParameter[] parameters) {

            var command = PrepareCommand(conn, name);
            command.CommandType = CommandType.StoredProcedure;
            if (parameters.Count() > 0) {
                command.Parameters.AddRange(parameters);
            }

            return command.ExecuteReader();

        }

    }
}
