﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using LLF.Annotations.Data;
using LLF.Abstract.Data.Engine;
using LLF.Extensions;
using LLF.Data.Containers;
using LLF.Abstract.Xml;
using LLF.Data.Tools;
using LLF.Extensions.Data.Enums;
using LLF.Data.EntityEngine.Tools;

namespace LLF.Data.Engine {

    internal class ScriptEngine : IScriptEngine {

        #region Ctor

        public ScriptEngine() {

            _SprocTemplate = ResourceContainer.ReadXmlFor<IDatabaseTemplate>("SprocTemplate").Script;

            _TableTemplate = ResourceContainer.ReadXmlFor<IDatabaseTemplate>("TableTemplate").Script;

            _Script = new StringBuilder();

            _Sprocs = new StringBuilder();

            _AlterConstraints = new StringBuilder(@"
                ##alter##
                ");
        }

        #endregion

        #region Implementation

        public IScriptEngine GenerateModelFor<TEntity>()
            where TEntity : IBaseEntity<TEntity> {

            var entityInstance = Activator.CreateInstance<TEntity>();
            EntityTitleAttribute entityTitle = null;
            if (entityInstance.LLFluentActive) {
                entityTitle = entityInstance.EntityTitle;
            } else {
                entityTitle = typeof(TEntity).GetClassDecoration<EntityTitleAttribute>();
                if (entityTitle == null) {
                    throw new InvalidOperationException("Should have EntityTitleAttribute");
                }
            }
            _Script.Append(_TableTemplate.PostFormat(AliasFormatter.MakeTableName(entityTitle)));

            _Script.Replace("##schema##", entityTitle.Schema.DefaultIfNullOrEmpty("dbo"));
            _Script.Replace("##user##", CredentialProvider.CurrentCredential.UserID);
            if (entityInstance.LLFluentActive) {
                foreach (var pkmod in entityInstance.LLFluentPKModifiers) {
                    var pkConstraintName = "PK_{0}{1}".PostFormat(typeof(TEntity).Name, pkmod.Name);
                    _Script.Replace("##row##", ResourceContainer.
                        ReadXmlFor<IDatabaseTemplate>("PKTemplate").Script
                        .PostFormat(pkConstraintName, pkmod.Name));
                }
            } else {
                ParsePKs<TEntity>();
            }

            if (entityInstance.LLFluentActive) {
                ParseRowsFluent<TEntity>(entityInstance, entityTitle);
            } else {
                ParseRows<TEntity>(entityTitle);
            }
            return this;

        }

        public IScriptEngine GenerateSproc(String sprocBody, params Object[] args) {

            _Sprocs.AppendFormat(_SprocTemplate.Replace("##sprocbody##", sprocBody), args);
            return this;

        }

        public String Finalize() {

            var resp = @"
						{0} 
                        {1}
						{2} {3}".PostFormat(ResourceContainer
                                .ReadXmlFor<IDatabaseTemplate>("CallSprocsTemplate")
                                .Script.PostFormat(CredentialProvider.CurrentCredential.Database,
                                    "spClearAllTables"),
                "",
                _Script.Replace(",\r\n##row##", "").Replace("##constraint##", ""),
                _AlterConstraints.Replace("\r\n##alter##", ""),
                _Sprocs
                );

            return resp.Replace("\r\n\r\n", "\r\n");

        }

        public String Sprocs {
            get {
                return _Sprocs.ToString();
            }
        }

        #endregion

        #region Parsers

        private void ParseRows<TEntity>(EntityTitleAttribute entityTitle) where TEntity : IBaseEntity<TEntity> {
            var piList = typeof(TEntity).GetProperties();

            foreach (var pi in piList) {
                var colName = pi.Name;

                var dt = pi.GetCustomAttributes(typeof(DataColumnAttribute), false).FirstOrDefault()
                    as DataColumnAttribute;
                var nl = pi.GetCustomAttributes(typeof(NullableAttribute), false).FirstOrDefault()
                    as NullableAttribute;
                var idt = pi.GetCustomAttributes(typeof(IdentityAttribute), false)
                    .FirstOrDefault() as IdentityAttribute;
                if (dt != null) {
                    var nullable = nl != null ? ResourceContainer
                        .ReadXmlFor<IDatabaseTemplate>("NullTemplate").Script :
                        ResourceContainer.ReadXmlFor<IDatabaseTemplate>("NotNullTemplate").Script;
                    var identity = "";
                    if (idt != null) {
                        identity = ResourceContainer.ReadXmlFor<IDatabaseTemplate>("AutoIncrementTemplate").Script.PostFormat(idt.Seed, idt.Increment);
                    }
                    _Script.Replace("##row##", ResourceContainer
                        .ReadXmlFor<IDatabaseTemplate>("DataColumnTemplate").Script
                        .PostFormat(colName, dt.DataType.GetColumnType(), dt.ComputedMaxLength, nullable, identity));

                    var dv = pi.GetCustomAttributes(typeof(DefaultValueAttribute), false)
                            .FirstOrDefault() as DefaultValueAttribute;
                    if (dv != null) {
                        _AlterConstraints.Replace("##alter##", ResourceContainer
                            .ReadXmlFor<IDatabaseTemplate>("DFTemplate").Script
                            .PostFormat(dv.Value, pi.Name, AliasFormatter.MakeTableName(entityTitle)));
                    }
                } else {
                    var refKey = pi.GetCustomAttributes(typeof(ReferenceKeyAttribute), false)
                        .FirstOrDefault() as ReferenceKeyAttribute;
                    if (refKey != null) {
                        var referencedTitle =
                            pi.PropertyType.GetCustomAttributes(typeof(EntityTitleAttribute), false)
                                .FirstOrDefault() as EntityTitleAttribute;
                        var rkName = "FK_{0}_{1}_{2}".PostFormat(
                            entityTitle.Schema, typeof(TEntity).Name, refKey.ReferenceKey);

                        var referencedID = pi.PropertyType.GetPropertiesDecoratedWith<PrimaryKeyAttribute>().First().Name;

                        _AlterConstraints.Replace("##alter##", ResourceContainer
                            .ReadXmlFor<IDatabaseTemplate>("FKTemplate")
                            .Script.PostFormat(entityTitle.Shematize(rkName), AliasFormatter.MakeTableName(entityTitle),
                                rkName, refKey.ReferenceKey, AliasFormatter.MakeTableName(referencedTitle), referencedID));

                        if (refKey.IsUniqueIndex) {
                            var uqName = "UQ_{0}_{1}_{2}".PostFormat(
                            entityTitle.Schema, typeof(TEntity).Name, pi.Name);
                            _AlterConstraints.Replace("##alter##", ResourceContainer
                                .ReadXmlFor<IDatabaseTemplate>("UQTemplate")
                                .Script.PostFormat(AliasFormatter.MakeTableName(entityTitle),
                                uqName, refKey.ReferenceKey, entityTitle.Schema));
                        }

                        if (!refKey.ReferenceKey.In(piList.Select(pin => pin.Name).ToArray())) {
                            var referencedPK = pi.PropertyType.PropertyFor(referencedID);
                            var dtRef = referencedPK.GetCustomAttribute<DataColumnAttribute>();
                            var nullable = nl != null ? "NULL" : "NOT NULL";
                            _Script.Replace("##row##", ResourceContainer
                                .ReadXmlFor<IDatabaseTemplate>("ReferencedRowTemplate")
                                .Script.PostFormat(refKey.ReferenceKey, dtRef.DataType.GetColumnType(), nullable));
                        }
                    }
                    var en = pi.GetCustomAttributes(typeof(EnumerationAttribute), false)
                            .FirstOrDefault() as EnumerationAttribute;
                    if (en != null) {
                        var nullable = nl != null ? "NULL" : "NOT NULL";
                        _Script.Replace("##row##", ResourceContainer
                            .ReadXmlFor<IDatabaseTemplate>("EnumTemplate")
                            .Script.PostFormat(colName, nullable));
                    }
                }
            }
            var unique = new StringBuilder("##unique##");
            foreach (var uq in piList.Where(p => p.GetCustomAttribute<UniqueAttribute>() != null)) {
                var uqRef = uq.GetCustomAttribute<ReferenceKeyAttribute>();
                var uqcol = uqRef != null ? uqRef.ReferenceKey : uq.Name;
                var order = uq.GetCustomAttribute<UniqueAttribute>().Order.DefaultIfNullOrEmpty("ASC");
                unique.Replace("##unique##", ResourceContainer
                    .ReadXmlFor<IDatabaseTemplate>("UniqueIndexTemplate")
                    .Script.PostFormat(uqcol, order));
            }
            if (!unique.ToString().Equals("##unique##")) {
                var uqIndexName = "UQ_{0}_{1}".PostFormat(
                           entityTitle.Schema, typeof(TEntity).Name);
                _AlterConstraints.Replace("##alter##", ResourceContainer
                    .ReadXmlFor<IDatabaseTemplate>("UQTemplate")
                    .Script.PostFormat(AliasFormatter.MakeTableName(entityTitle), uqIndexName,
                    unique.Replace("##unique##", "").ToString().Trim().TrimEnd(','), entityTitle.Schema));
            }
            _Script.Replace(",\r\n##row##", "");

        }

        private void ParseRowsFluent<TEntity>(TEntity entityInstance, EntityTitleAttribute entityTitle)
            where TEntity : IBaseEntity<TEntity> {

            foreach (var colmod in entityInstance.LLFluentColumnModifiers) {
                var colName = colmod.Name;
                var nullable = colmod.NotNull ? ResourceContainer
                    .ReadXmlFor<IDatabaseTemplate>("NotNullTemplate").Script :
                    ResourceContainer.ReadXmlFor<IDatabaseTemplate>("NullTemplate").Script;
                var maxLengthForm = colmod.Maximum > 0 ? "({0})".PostFormat(colmod.Maximum) :
                    (colmod.Maximum == -1 ? ResourceContainer
                    .ReadXmlFor<IDatabaseTemplate>("MaximumTemplate").Script : "");
                var identity = "";
                if (colmod.Identity != null) {
                    identity = ResourceContainer.ReadXmlFor<IDatabaseTemplate>("AutoIncrementTemplate").Script.PostFormat(
                        colmod.Identity.Seed, colmod.Identity.Increment);
                }
                if (!colmod.Enumeration) {
                    _Script.Replace("##row##", ResourceContainer
                        .ReadXmlFor<IDatabaseTemplate>("DataColumnTemplate").Script
                        .PostFormat(colName, colmod.Type.GetColumnType(), maxLengthForm, nullable, identity));
                } else {
                    _Script.Replace("##row##", ResourceContainer
                        .ReadXmlFor<IDatabaseTemplate>("EnumTemplate")
                        .Script.PostFormat(colName, nullable));
                }
                if (colmod.DefaultValue != null) {
                    _AlterConstraints.Replace("##alter##", ResourceContainer
                        .ReadXmlFor<IDatabaseTemplate>("DFTemplate").Script
                        .PostFormat(colmod.DefaultValue.Value, colmod.Name, AliasFormatter.MakeTableName(entityTitle)));
                }
                foreach (var fkcol in entityInstance.LLFluentFKModifiers) {
                    var refColInstance = Activator.CreateInstance(fkcol.PropertyType, true);
                    var refTableAlias = refColInstance.PropertyFor<String>("Alias");
                    var referencedTitle = refColInstance.PropertyFor<EntityTitleAttribute>("EntityTitle");
                    var fkName = "FK_{0}_{1}_{2}_{3}".PostFormat(
                        entityTitle.Schema, typeof(TEntity).Name, fkcol.FKKey, fkcol.ReferencedKey);
                    _AlterConstraints.Replace("##alter##", ResourceContainer
                       .ReadXmlFor<IDatabaseTemplate>("FKTemplate")
                       .Script.PostFormat(entityTitle.Shematize(fkName), AliasFormatter.MakeTableName(entityTitle), fkName, fkcol.FKKey,
                       AliasFormatter.MakeTableName(referencedTitle), fkcol.ReferencedKey));

                    if (fkcol.Unique != null) {
                        var uqName = "UQ_{0}_{1}_{2}".PostFormat(
                        entityTitle.Schema, typeof(TEntity).Name, fkcol.Name);
                        _AlterConstraints.Replace("##alter##", ResourceContainer
                            .ReadXmlFor<IDatabaseTemplate>("UQTemplate")
                            .Script.PostFormat(AliasFormatter.MakeTableName(entityTitle),
                            uqName, fkcol.FKKey, entityTitle.Schema));
                    }

                    if (!fkcol.FKKey.In(entityInstance.LLFluentColumnModifiers.Select(pin => pin.Name).ToArray())) {
                        _Script.Replace("##row##", ResourceContainer
                            .ReadXmlFor<IDatabaseTemplate>("ReferencedRowTemplate")
                            .Script.PostFormat(fkcol.FKKey, fkcol.Type.GetColumnType(),
                            nullable));
                    }

                }
            }

            var unique = new StringBuilder("##unique##");
            foreach (var uqcol in entityInstance.LLFluentUQModifiers) {
                var uqRef = uqcol.FKKey;
                var order = uqcol.Unique.Order.DefaultIfNullOrEmpty("ASC");
                unique.Replace("##unique##", ResourceContainer
                    .ReadXmlFor<IDatabaseTemplate>("UniqueIndexTemplate")
                    .Script.PostFormat(uqcol.FKKey, order));
            }
            if (!unique.ToString().Equals("##unique##")) {
                var uqIndexName = "UQ_{0}_{1}".PostFormat(
                           entityTitle.Schema, typeof(TEntity).Name);
                _AlterConstraints.Replace("##alter##", ResourceContainer
                    .ReadXmlFor<IDatabaseTemplate>("UQTemplate")
                    .Script.PostFormat(AliasFormatter.MakeTableName(entityTitle), uqIndexName,
                    unique.Replace("##unique##", "").ToString().Trim().TrimEnd(','), entityTitle.Schema));
            }
            _Script.Replace(",\r\n##row##", "");
        }

        private void ParsePKs<TEntity>() where TEntity : IBaseEntity<TEntity> {
            var pkPropertyList = typeof(TEntity).GetPropertiesDecoratedWith<PrimaryKeyAttribute>();
            var pkName = "";
            if (pkPropertyList.Count() == 0) {
                throw new InvalidOperationException("no pk defined!");
            } else if (pkPropertyList.Count() == 1) {
                pkName = pkPropertyList.First().Name;
            } else {
                throw new InvalidOperationException("more than one primary keys are not supported!: " + typeof(TEntity).Name);
            }

            foreach (var pkpi in pkPropertyList) {
                var pkConstraintName = "PK_{0}{1}".PostFormat(typeof(TEntity).Name, pkpi.Name);
                _Script.Replace("##row##", ResourceContainer.ReadXmlFor<IDatabaseTemplate>("PKTemplate").Script.PostFormat(pkConstraintName, pkpi.Name));
            }

        }

        #endregion

        #region Fields

        private readonly String _TableTemplate;
        private readonly String _SprocTemplate;
        private StringBuilder _Sprocs;
        private StringBuilder _Script;
        private StringBuilder _AlterConstraints;

        #endregion

    }
}
