﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Extensions;
using LLF.Abstract.Data.Engine;

namespace LLF.Data.Engine {

	internal class PartialModel : IPartialModel {

		#region Fields

		private Dictionary<String, ICollection<Object>> _Attributes;
		private Int32 _Count;
		private String _CurrentKey;
		private Int32 _CurrentIndex;

		#endregion

		#region Ctor

        public PartialModel() {

			this._Attributes = new Dictionary<string, ICollection<Object>>();
			this._CurrentIndex = 0;

		}

		#endregion

		#region IPartialModel Implemetation

		public IPartialModel Attribute(String key) {

			this._CurrentKey = key;
			this._Count = 0;
			return this;

		}

		public IPartialModel Has(Object value) {

			if (this._Attributes.ContainsKey(this._CurrentKey)) {
				this._Attributes[this._CurrentKey].Add(value);
			} else {
				this._Attributes.Add(this._CurrentKey, new List<Object>().Extend(value));
			}
			this._Count++;
			return this;
		}

		public Object this[String key, Int32 index] {

			get {
				return this._Attributes[key].ElementAt(index);
			}

		}

		public ICollection<Dictionary<String, Object>> Enumerate() {

			var resp = new List<Dictionary<String, Object>>();
			do {
				var li = new List<KeyValuePair<String, Object>>();
				foreach (var key in this._Attributes.Keys) {
					li.Add(new KeyValuePair<String, Object>(key,
						this._Attributes[key].ElementAt(this._CurrentIndex)));
				}
				this._CurrentIndex++;
				resp.Add(li.Gather());
			} while (this._CurrentIndex < this._Count);

			this._CurrentIndex = 0;
			return resp;

		}

		#endregion

		#region IDisposable Members

		public void Dispose() {

			this._CurrentIndex = 0;
			this._Attributes = null;

		}

		#endregion

	}

}
