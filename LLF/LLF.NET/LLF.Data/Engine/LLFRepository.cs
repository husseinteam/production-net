﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Abstract.Data.Engine;
using LLF.Data.Containers;
using LLF.Data.Engine;
using LLF.Extensions;
using LLF.Static.Locals;

namespace LLF.Data.Engine {

    internal class LLFRepository<TEntity> : ILLFRepository<TEntity>
        where TEntity : class, IBaseEntity<TEntity>, new() {

        #region From Db

        public IEnumerable<TEntity> GetAll() {

            return new TEntity().SelectMany();

        }

        public Int32 GetCountOf(object selector = null, object selectornot = null) {

            return new TEntity().SelectCount<Int32>(selector, selectornot);

        }

        public TEntity GetThe<TKey>(TKey pk) {

            return new TEntity().SelectThe(pk.ToType<Int64>());

        }
        public TEntity GetSingle(object selector) {

            return new TEntity().SelectOne(selector);

        }
        public IEnumerable<TEntity> GetMany(object selector, object selectornot = null) {

            return new TEntity().SelectMany(selector, selectornot);

        }

        #endregion

        #region To Db


        public IDMLResponse<TEntity> ToDb(TEntity entity) {

            var resp = new DMLResponse<TEntity>();
            if (entity.ID.IsUnassigned()) {
                resp.Success = entity.InsertSelf().Fault == null;
                resp.Message = ApiMessages.DatabaseInsertSuccessful;
            } else {
                resp.Success = entity.UpdateSelf().Fault == null;
                resp.Message = ApiMessages.DatabaseUpdateSuccessful;
            }
            resp.SingleEntity = entity;
            return resp;

        }


        public IDMLResponse<TEntity> OutOfDb<TKey>(TKey key) {

            var resp = new DMLResponse<TEntity>();
            var entity = GetThe<TKey>(key);
            resp.Success = entity.DeleteSelf().Fault == null;
            resp.Message = ApiMessages.DatabaseDeleteSuccessful;
            resp.SingleEntity = entity;
            return resp;

        }
        #endregion

    }

}
