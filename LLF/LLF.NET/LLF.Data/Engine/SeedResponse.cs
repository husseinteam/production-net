﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Extensions;
using LLF.Abstract.Data.Engine;

namespace LLF.Data.Engine {

    internal class SeedResponse<TEntity> : ISeedResponse<TEntity>
        where TEntity : IBaseEntity<TEntity>, new() {

        public SeedResponse(Int32 magnitude, Action<TEntity> extraops = null) {
            _Magnitude = magnitude;
            _SeedList = new List<TEntity>();
            _Extended = false;
            _ExtraOps = extraops ?? new Action<TEntity>(e => { });
        }

        public SeedResponse(List<TEntity> seedList) {
            _Magnitude = seedList.Count;
            _SeedList = seedList;
            _Extended = true;
            _ExtraOps = new Action<TEntity>(e => { });
        }

        private Boolean _Extended;
        private Int32 _Magnitude;
        private List<TEntity> _SeedList;
        private Action<TEntity> _ExtraOps;

        public Func<Int32, TEntity> SingleSeed { get; internal set; }
        public Int32 Magnitude { get { return _Magnitude; } }
        public List<TEntity> SeedList { get { return _SeedList; } }

        #region ISeedResponse<TEntity> Members

        public ISeedResponse<TEntity> CollectSeed() {

            if (!_Extended) {
                _SeedList.Clear();
                for (int i = 0; i < _Magnitude; i++) {
                    var seed = SingleSeed(i);
                    _SeedList.Add(seed);
                }
            }
            return this;

        }

        public ISeedResponse<TEntity> Extend(ISeedResponse<TEntity> other) {

            var extendedList = _SeedList.Extend(other.SeedList.ToArray()) as List<TEntity>;
            return new SeedResponse<TEntity>(extendedList);

        }

        public ISeedResponse<TEntity> PersistSeed(ref IList<Exception> exceptions) {

            CollectSeed();
            _HasFault = false;
            foreach (var sd in _SeedList) {
                var hf = sd.InsertSelf().Fault != null;
                if (hf) {
                    _Fault = sd.Fault;
                    _HasFault = true;
                    exceptions.Add(_Fault);
                }
                _ExtraOps(sd);
            }
            return this;

        }

        public ISeedResponse<TEntity> PersistCollectionSeed<TElement>(ref IList<Exception> exceptions)
            where TElement : class, IBaseEntity<TElement> {

            CollectSeed();
            _HasFault = false;
            foreach (var sd in _SeedList) {
                var hf = sd.UpsertCollections<TElement>().Fault != null;
                if (hf) {
                    _Fault = sd.Fault;
                    _HasFault = true;
                    exceptions.Add(_Fault);
                }
                _ExtraOps(sd);
            }
            return this;

        }

        public ISeedResponse<TEntity> DestroySeed() {

            var fromDB = new TEntity().SelectMany();
            try {
                foreach (var e in fromDB) {
                    e.DeleteSelf(true);
                }
                _HasFault = false;
                _Fault = null;
            } catch (Exception ex) {
                _Fault = ex;
                _HasFault = true;
            }
            return this;

        }

        public ISeedResponse<TEntity> TruncateSeed() {

            try {
                new TEntity().Truncate();
                _HasFault = false;
                _Fault = null;
            } catch (Exception ex) {
                _Fault = ex;
                _HasFault = true;
            }
            return this;

        }

        private Boolean _HasFault;
        public Boolean HasFault {
            get { return _HasFault; }
        }

        private Exception _Fault;
        public Exception Fault {
            get { return _Fault; }
        }

        #endregion

    }

}
