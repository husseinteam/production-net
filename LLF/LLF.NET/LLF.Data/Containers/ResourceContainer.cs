﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Abstract.Data.Engine;
using LLF.Abstract.Xml;
using LLF.Data.Xml;
using LLF.Extensions;
using LLF.Data.Containers;
using LLF.Static.Globals;
using LLF.Data.Tools;
using LLF.Extensions.Data.Enums;

namespace LLF.Data.Containers {

    public class ResourceContainer {

        static ResourceContainer() {

            Register<IAttributeItem>(XmlResources.Attributes);
            Register<IDefinitionItem>(XmlResources.Definitions);
            Register<IMessageItem>(XmlResources.Messages);
            Register<IConnectionCredential>(XmlResources.ConnectionCredentials);

        }

        public static void Register<IResource>(String xml) 
            where IResource : IXmlResource {

            var key = typeof(IResource).Name;
            _Registry[key] = xml;

        }

        public static IResource ReadXmlFor<IResource>(String key)
            where IResource : IXmlResource {

            var xml = _Registry[typeof(IResource).Name];

            var resource = DependencyContainer.Take<IXmlDigester>().TakeXmlContent(xml).ToModels<IResource>()
                .SingleOrDefault(r => r.Key == key);
            if (typeof(IResource).TypesEqual(typeof(IConnectionCredential))) {
                SetDatabaseTemplate(resource as IConnectionCredential);
            }
            return resource;

        }

        private static void SetDatabaseTemplate(IConnectionCredential cred) {
            switch (cred.ServiceType) {
                case EServiceType.MSSQL:
                case EServiceType.MSSQLNamedPipe:
                case EServiceType.LocalDB:
                case EServiceType.AzureSql:
                    Register<IDatabaseTemplate>(XmlResources.MSSQLTemplates);
                    Register<IDbTypeTemplate>(XmlResources.MSSqlDbTypes);
                    break;
                case EServiceType.MySQL:
                case EServiceType.AzureMySql:
                    Register<IDatabaseTemplate>(XmlResources.MySqlTemplates);
                    Register<IDbTypeTemplate>(XmlResources.MySqlDbTypes);
                    break;
                case EServiceType.PostgreSql:
                    break;
                default:
                    break;
            }
        }

        private static Dictionary<String, String> _Registry = new Dictionary<String, String>();

    }
}
