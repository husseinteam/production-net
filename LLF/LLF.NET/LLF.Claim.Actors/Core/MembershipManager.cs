﻿using LLF.Claim.Model.Entities.POCO;
using LLF.Claim.Model.ViewModels.Identity;
using LLF.Claim.Model.ViewModels.Extensions;
using LLF.Extensions;
using LLF.Static.Locals;
using LLF.Claim.Model.ActionResponses;
using LLF.Claim.Model.ViewModels.Membership;

namespace LLF.Claim.Actors.Core {

	public static class MembershipManager {

		public static RegisterViewModel Register(RegisterViewModel vmodel) {

			var resp = new ApiPostResponse();
			var validationResponse = vmodel.Validate();
			if (validationResponse.IsValid) {
				var existing = new Author().SelectOne(new { Email = vmodel.Email });
				if (existing == null) {
					var newactor = new Actor() {
						Author = new Author() {
							Email = vmodel.Email,
							PasswordHash = vmodel.Password.ComputeHash(),
							Profile = new Profile() {
								Identity = vmodel.Identity,
								SecretQuestion = "",
								AnswerOfSecretQuestion = ""
							}
						}
					};
					resp.HasFault = !newactor.InsertSelf().Fault.IsUnassigned();
					if (!resp.HasFault) {
						resp.Message = ApiMessages.WelcomeAmongUs.PostFormat(vmodel.Identity);
					} else {
						newactor.DeleteSelf();
						resp.Message = ApiMessages.RegisterFailed;
					}
				} else {
					resp.HasFault = true;
					resp.Message = ApiMessages.UserExists.PostFormat(existing.Email);
				}
			} else {
				resp.HasFault = true;
				resp.Message = validationResponse.FinalMessage;
			}
			return vmodel.Assign(vm => vm.Response = resp);

		}

		public static LoginViewModel Login(LoginViewModel vmodel) {

			var resp = new ApiPostResponse();
			var validationResponse = vmodel.Validate();
			if (validationResponse.IsValid) {
				var existing = new Author().SelectOne(new { Email = vmodel.Email });
				if (existing == null) {
					resp.HasFault = true;
					resp.Message = ApiMessages.UserCouldNotBeIdentified.PostFormat(vmodel.Email);
				} else if(existing.PasswordHash != vmodel.Password.ComputeHash()){
					resp.HasFault = true;
					resp.Message = ApiMessages.WrongPassword;
				} else {
					resp.HasFault = false;
					resp.Message = ApiMessages.WelcomeHomeUser.PostFormat(existing.Email);
					var tokenticationResponse = Authenticator.Tokenticate(new TokenticationViewModel(vmodel));
					resp.TokenVector = tokenticationResponse.Response.TokenVector;
				}
			} else {
				resp.HasFault = true;
				resp.Message = validationResponse.FinalMessage;
			}
			return vmodel.Assign(vm => vm.Response = resp);

		}


		public static EditMembershipViewModel EditMembership(EditMembershipViewModel vmodel) {

			var resp = new ApiPostResponse();
			var validationResponse = vmodel.Validate();
			if (validationResponse.IsValid) {
				var existing = new Author().SelectOne(new { Email = vmodel.Email });
				if (existing == null) {
					resp.HasFault = true;
					resp.Message = ApiMessages.UserCouldNotBeIdentified.PostFormat(vmodel.Email);
				} else if (existing.PasswordHash != vmodel.OldPassword.ComputeHash()) {
					resp.HasFault = true;
					resp.Message = ApiMessages.IncorrectOldPassword;
				} else {
					existing.Profile.Identity = vmodel.Identity;
					existing.PasswordHash = vmodel.NewPassword.ComputeHash();
					resp.HasFault = !existing.UpdateSelf().Fault.IsUnassigned();
					resp.Message = resp.HasFault ? ApiMessages.EditMembershipFailed : ApiMessages.EditMembershipDone;
				}
			} else {
				resp.HasFault = true;
				resp.Message = validationResponse.FinalMessage;
			}
			return vmodel.Assign(vm => vm.Response = resp);


		}
	}

}
