﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LLF.Data.Containers;
using LLF.Abstract.Xml;
using System.IO;
using LLF.Data.Xml;
using LLF.Static;
using LLF.Abstract.Data.Engine;
using FluentAssertions;

namespace LLF.Data.Test.UnitTests {

	[TestClass]
	public class DependencyContainerTests {

		[TestMethod]
		public void DependencyContainerTakes() {

			var xmlDigester = DependencyContainer.Take<IXmlDigester>();
            xmlDigester.Should().NotBeNull("Because XmlDigester Should be registered");

			var messageItem = DependencyContainer.Take<IMessageItem>();
			messageItem.Should().NotBeNull("Because MessageSeperator Should be registered");

		}

		[TestMethod]
		public void DependencyContainerTakesWithParameter() {

			var dmlResponseFalse = DependencyContainer.Take<IDMLResponse>(new { Success = false });
			dmlResponseFalse.Success.Should().BeFalse("Because Object was Initiaized With Success = false");

			var dmlResponseTrue = DependencyContainer.Take<IDMLResponse>(new { Success = true });
            dmlResponseTrue.Success.Should().BeTrue("Because Object was Initiaized With Success = true");

		}

		[TestMethod]
		public void DigesterExtractsXml() {

            ResourceContainer.ReadXmlFor<IAttributeItem>("MessageSeperator").Should().NotBeNull("Because MessageSeperator Should be registered");
            ResourceContainer.ReadXmlFor<IConnectionCredential>("local-mssql").Should().NotBeNull("Because local Should be registered");
            ResourceContainer.ReadXmlFor<IDefinitionItem>("Email").Should().NotBeNull("Because Email Should be registered");
            ResourceContainer.ReadXmlFor<IMessageItem>("InvalidServiceType").Should().NotBeNull("Because InvalidServiceType Should be registered");
            ResourceContainer.ReadXmlFor<IDatabaseTemplate>("IdentityTemplate").Should().NotBeNull("Because IdentityTemplate Should be registered");

		}

        [TestMethod]
        public void IsTakenRunsOK() {

            DependencyContainer.IsTaken<IXmlDigester>().Should().BeTrue("Because XmlDigester Is Registered");
            DependencyContainer.IsTaken<IDemoInterface>().Should().BeFalse("Because IDemoInterface Is Not Registered");
            DependencyContainer.Put<IDemoInterface, Demo>();
            DependencyContainer.IsTaken<IDemoInterface>().Should().BeTrue("Because IDemoInterface Is Registered Now");
            DependencyContainer.Take<IDemoInterface>(new { name = "hi there" })
                .Name.Should().Be("hi there", "Because new object is taken with arguments");
        }

	}

    interface IDemoInterface {
        String Name { get; set; }
    }
    class Demo : IDemoInterface {
        private string _Name;

        public string Name {
            get { return _Name; }
            set { _Name = value; }
        }

        public Demo(String name) {
            this._Name = name;
        }


    }

}
