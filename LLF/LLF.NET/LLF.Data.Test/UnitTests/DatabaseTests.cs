﻿using LLF.Abstract.Data.Engine;
using LLF.Claim.Model.Entities.POCO;
using LLF.Data.Containers;
using LLF.Extensions;
using LLF.TestBase;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using LLF.Static.Globals;
using LLF.Data.Tools;
using LLF.Claim.Model.Entities;
using LLF.Claim.Model.Entities.SeedCatalog;

namespace LLF.Data.Test.UnitTests {

    [TestClass]
    public class DatabaseTests : DataTestBase {

        [ClassCleanup]
        public static void Cleanup() {

            var exList = SClaimEngine.RegenerateDatabase();

        }

        [ClassInitialize]
        public static void TestEach(TestContext ctx) {

            var exList = SClaimEngine.RegenerateDatabase(false);
            
        }

        #region Tests

        [TestMethod]
        public void CurrentCredentialConnectsOK() {

            var cred = CredentialProvider.CurrentCredential;
            cred.Should().NotBeNull("because cred is intended");
            var c = cred.ProvideSuperAdminConnection();
            c.Should().NotBeNull("because sql connection is intended");
            Action connectifier = () => c.Assign(conn => conn.Open()).Assign(conn => conn.Close());
            connectifier.ShouldNotThrow<Exception>("Because Connection To Local Sql should succeed");

        }
        [TestMethod]
        public void RegenerateDatabaseRunsOK() {

            var exList = SClaimEngine.RegenerateDatabase();
            Assert.AreNotEqual(0, exList.Count());

        }

        [TestMethod]
        public void NullOwnerWorksOk() {

            var fi = new FileItem() { FileName = "file-{0}".GenerateNumber(), ServerDirectory = "ServerDirectory", Owner = null };
            fi.InsertSelf();
            var fili = DbProvider.ProvideEntity<FileItem>().SelectMany();
            fi.DeleteSelf();
            Assert.IsNull(fi.Fault, (fi.Fault ?? new Exception("Success")).Message);
            Assert.AreNotEqual(0, fili.Where(f => f.Owner == null).Count());

        }

        [TestMethod]
        public void CrudOK() {

            var actorSeed = ActorSeed.GetResponse().CollectSeed();
            var randomActor = actorSeed.SeedList.Random();
            randomActor.Author.Email = "arch-{0}itect@gmail.com".GenerateNumber();
            randomActor.InsertSelf().Fault.Should().BeNull("Because insertion should succeed");
            randomActor.ID.Should().NotBe(0, "because it is inserted");

            randomActor.Author.PasswordResetRequested = true;
            randomActor.UpdateSelf().Fault.Should().BeNull("Because update should succeed");

            var takenActor = new Actor().SelectThe(randomActor.ID);
            takenActor.Author.PasswordResetRequested.Should().Be(
                randomActor.Author.PasswordResetRequested, "because we've updated it recently");

            takenActor.DeleteSelf().Fault.Should().BeNull("Because delete should succeed");
            new Actor().SelectThe(randomActor.ID).Should().BeNull("because we've deleted it");

        }

        [TestMethod]
        public void ReadSelfRunsOKForCollections() {

            var randomActor = GetRandomActor();
            Assert.AreNotEqual(0, randomActor.FileItems.Count);

            var actorFromDb = DbProvider.ProvideEntity<Actor>().SelectThe(randomActor.ID).Include<FileItem>();
            randomActor.DeleteSelf();
            Assert.AreNotEqual(0, actorFromDb.FileItems.Count);

        }

        [TestMethod]
        public void SelectCountOK() {

            new Actor().SelectCount<Int32>().Should().BeGreaterThan(0);

        }

        [TestMethod]
        public void DeleteCascadeRunsOk() {

            var randomActor = GetRandomActor();
            var f = randomActor.DeleteSelf(true).Fault;
            Assert.IsNull(f);

        }

        [TestMethod]
        public void SelectManyRunsOK() {

            var act = GetRandomActor();
            var fili = DbProvider.ProvideEntity<FileItem>().SelectMany(new { OwnerID = act.ID });
            var f = act.DeleteSelf(true).Fault;
            Assert.IsNull(f);
            Assert.AreNotEqual(0, fili.Count);

        }

        #endregion

        private static Actor GetRandomActor() {
            return new Actor() {
                Author = new Author() {
                    Email = "name-{0}@domain.com".GenerateNumber(),
                    PasswordHash = "a1q2w3e".ComputeHash(),
                    Profile = new Profile() {
                        SecretQuestion = "How much?",
                        AnswerOfSecretQuestion = "nope",
                        Identity = "Name Surname"
                    },
                },
                FileItems = new FileItem[] {
                        new FileItem() { FileName = "file-{0}".GenerateNumber(), ServerDirectory = "ServerDirectory" },
                        new FileItem() { FileName = "file-{0}".GenerateNumber(), ServerDirectory = "ServerDirectory" },
                        new FileItem() { FileName = "file-{0}".GenerateNumber(), ServerDirectory = "ServerDirectory" },
                        new FileItem() { FileName = "file-{0}".GenerateNumber(), ServerDirectory = "ServerDirectory" },
                        new FileItem() { FileName = "file-{0}".GenerateNumber(), ServerDirectory = "ServerDirectory" },
                    }
            }.InsertSelf().UpsertCollections<FileItem>();
        }

        protected override string CredentialKey {
            get { return SGlobalDefaults.CredentialKey; }
        }


    }
}
