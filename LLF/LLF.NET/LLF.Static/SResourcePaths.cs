﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LLF.Static {
	public static class SResourcePaths {

		private static readonly String BasePath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase), "Resources");

		public static String AttributesPath {
			get {
				return Path.Combine(BasePath, "Attributes.xml");
			}
		}

		public static String DatabaseScriptsPath {
			get {
				return Path.Combine(BasePath, "DatabaseScripts.xml");
			}
		}

		public static String DefinitionsPath {
			get {
				return Path.Combine(BasePath, "Definitions.xml");
			}
		}

		public static String MessagesPath {
			get {
				return Path.Combine(BasePath, "Messages.xml");
			}
		}

		public static String CredentialsPath {
			get {
				return Path.Combine(BasePath, "ConnectionCredentials.xml");
			}
		}

	}
}
