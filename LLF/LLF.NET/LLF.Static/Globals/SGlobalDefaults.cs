﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LLF.Static.Globals {
    public static class SGlobalDefaults {

        public const String ApiPrefix = "llf";

#if DEBUG
        public const String CredentialKey = "local-mssql";
#else
        public const String CredentialKey = "turhost-mssql";
#endif

    }
}
