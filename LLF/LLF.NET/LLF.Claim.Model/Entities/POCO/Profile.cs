﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Annotations.Data;
using LLF.Data.Engine;
using LLF.Extensions.Data.Enums;

namespace LLF.Claim.Model.Entities.POCO {

    [EntityTitle("Profiles", Schema = "LLF")]
    public class Profile : BaseEntity<Profile>{

        public Profile() {

            //this.WithColumn(md => md.Identity)
            //    .WithType(EDbType.NVarChar)
            //    .WithMaximum(255)
            //    .IsNotNull();
            //this.WithColumn(md => md.AvatarPath)
            //    .WithType(EDbType.NVarChar)
            //    .WithMaximum(512)
            //    .IsNull();
            //this.WithColumn(md => md.AnswerOfSecretQuestion)
            //    .WithType(EDbType.NVarChar)
            //    .WithMaximum(255)
            //    .IsNull();
            //this.WithColumn(md => md.SecretQuestion)
            //    .WithType(EDbType.NVarChar)
            //    .WithMaximum(512)
            //    .IsNull();
            //this.WithKey(prf => prf.ID);
            //this.SetEntityTable("Profiles", "Identity");
        }

        [DataColumn(EDbType.NVarChar, MaxLength = 255)]
        public String Identity { get; set; }


        [DataColumn(EDbType.NVarChar, MaxLength = 512), Nullable]
        public String AvatarPath { get; set; }


        [DataColumn(EDbType.NVarChar, MaxLength = 512), Nullable]
        public String SecretQuestion { get; set; }


        [DataColumn(EDbType.NVarChar, MaxLength = 255), Nullable]
        public String AnswerOfSecretQuestion { get; set; }



    }
}
