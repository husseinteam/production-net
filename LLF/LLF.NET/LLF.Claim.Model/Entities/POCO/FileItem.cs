﻿using LLF.Annotations.Data;
using LLF.Data.Engine;
using LLF.Extensions.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LLF.Claim.Model.Entities.POCO {

    [EntityTitle("FileItems", Schema = "LLF")]
    public class FileItem : BaseEntity<FileItem> {

        [DataColumn(EDbType.NVarChar, MaxLength = 255)]
        public String ServerDirectory { get; set; }

        [DataColumn(EDbType.NVarChar, MaxLength = 255), Unique]
        public String FileName { get; set; }

        [ReferenceKey("OwnerID"), Nullable]
        public Actor Owner { get; set; }


    }

}
