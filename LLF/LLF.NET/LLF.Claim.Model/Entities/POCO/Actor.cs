﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Annotations.Data;
using LLF.Data.Engine;

namespace LLF.Claim.Model.Entities.POCO {

    [EntityTitle("Actors", Schema = "LLF")]
    public class Actor : BaseEntity<Actor> {

        public Actor() {
            this.FileItems = new List<FileItem>();
        }

        [ReferenceKey("AuthorID", IsUniqueIndex = true)]
		public Author Author { get; set; }

        [ReferenceCollection]
        public ICollection<FileItem> FileItems { get; set; }
    }

}
