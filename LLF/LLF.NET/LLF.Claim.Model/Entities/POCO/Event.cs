﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Annotations.Data;
using LLF.Data.Engine;
using LLF.Extensions.Data.Enums;

namespace LLF.Claim.Model.Entities.POCO {

    [EntityTitle("Events", Schema = "LLF")]
    public class Event : BaseEntity<Event> {

        public Event() {

        }

        #region Properties

        [DataColumn(EDbType.DateTime)]
        public DateTime HappenedOn { get; set; }

        #endregion

    }

}
