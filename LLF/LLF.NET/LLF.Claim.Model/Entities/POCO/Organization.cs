﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Annotations.Data;
using LLF.Data.Engine;
using LLF.Extensions.Data.Enums;

namespace LLF.Claim.Model.Entities.POCO {

    [EntityTitle("Organizations", Schema = "LLF")]
    public class Organization : BaseEntity<Organization> {

        public Organization() {

        }

        #region Properties

        #endregion

        #region Navigation Properties

        [ReferenceKey("ParticipantID"), Unique]
        public Actor Participant { get; set; }

        [ReferenceKey("EventID"), Unique]
        public Event Event { get; set; }

        #endregion

    }

}
