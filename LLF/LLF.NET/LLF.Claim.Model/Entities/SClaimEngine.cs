﻿using LLF.Abstract.Data.Engine;
using LLF.Claim.Model.Entities.POCO;
using LLF.Claim.Model.Entities.SeedCatalog;
using LLF.Data.Containers;
using LLF.Data.Tools;
using LLF.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LLF.Claim.Model.Entities {

    public static class SClaimEngine {

        public static IEnumerable<Exception> RegenerateDatabase(Boolean seed = true) {

            DbProvider.WipeDatabase();
            var expt = DependencyContainer.Take<IDatabaseEngine>()
               .EngineFor<Profile>()
               .EngineFor<Author>()
               .EngineFor<Actor>()
               .EngineFor<FileItem>()
               .EngineFor<Event>()
               .EngineFor<Organization>()
               .GenerateModels();


            IList<Exception> exptList = new List<Exception>().Extend(expt);

            if (seed) {
                var actors = ActorSeed.GetResponse().DestroySeed().PersistSeed(ref exptList).SeedList;
            }

            return exptList.ToArray();

        }

    }

}
