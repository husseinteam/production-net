﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Abstract.Data.Engine;
using LLF.Claim.Model.Entities.POCO;
using LLF.Extensions;
using LLF.Data.Tools;

namespace LLF.Claim.Model.Entities.SeedCatalog {
	public static class ActorSeed {

		public static ISeedResponse<Actor> GetResponse() {

			var emails = new String[] { 
				"lampiclobe@outlook.com", 
				"ondermetu@gmail.com", 
				"dilek1982@outlook.com",
				"section@gmail.com",
				"hithere@outlook.com" 
			};
			var identities = new String[] {
                "Özgür Eser Sönmez",
                "Önder Heper",
                "Dilek Neidek",
                "Naber Babalık",
				"Özgür Dönmez Sönmez"
			};

			ISeedResponse<Actor> response = SeedProvider.SeedInstanceOf<Actor>(emails.Count(),
				(i) => new Actor() {
					Author = new Author() {
						Email = emails.ElementAt(i),
						PasswordHash = "a1q2w3e".ComputeHash(),
						Profile = new Profile() {
							SecretQuestion = "How much?",
							AnswerOfSecretQuestion = "nope",
							Identity = identities.ElementAt(i)
						}
					},
                    FileItems = new FileItem[] { 
                        new FileItem() { FileName = "file1", ServerDirectory = "ServerDirectory" },
                        new FileItem() { FileName = "file2", ServerDirectory = "ServerDirectory" },
                        new FileItem() { FileName = "file3", ServerDirectory = "ServerDirectory" },
                        new FileItem() { FileName = "file4", ServerDirectory = "ServerDirectory" },
                        new FileItem() { FileName = "file5", ServerDirectory = "ServerDirectory" },
                    }
				});
			return response;

		}

	}
}
