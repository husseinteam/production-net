﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Claim.Model.ViewModels.Base;
using LLF.Annotations.ViewModel;
using LLF.Static.Locals;

namespace LLF.Claim.Model.ViewModels.Membership {

	[LocalResource(typeof(ValidationMessages))]
	public class EditMembershipViewModel : BaseAPIViewModel {

		#region ToUI

		[ToUI(ValidationErrorKey = "EmailNullError")]
		[ApplyRegex("Email", "EmailValidationError")]
		public String Email { get; set; }

		[ToUI(ValidationErrorKey = "PasswordNullError")]
		[ApplyRegex("Password", "PasswordValidationError")]
		public String OldPassword { get; set; }

		[ToUI(ValidationErrorKey = "PasswordRepeatNullError")]
		[ApplyRegex("Password", "PasswordValidationError")]
		public String NewPassword { get; set; }

		[ToUI(ValidationErrorKey = "PasswordRepeatNullError")]
		[ApplyRegex("Password", "PasswordValidationError")]
		public String NewPasswordRepeat { get; set; }

		[ToUI(ValidationErrorKey = "IdentityNullError")]
		[ApplyRegex("Identity", "IdentityValidationError")]
		public String Identity { get; set; }

		#endregion

	}

}
