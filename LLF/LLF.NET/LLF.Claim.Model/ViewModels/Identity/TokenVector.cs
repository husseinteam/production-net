﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Extensions;

namespace LLF.Claim.Model.ViewModels.Identity {

	public class TokenVector {

		public String Token { get; set; }
		public Boolean Loaded {
			get {
				return !Token.IsUnassigned();
			}
		}

	}

}
