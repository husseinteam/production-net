﻿using LLF.Claim.Model.ViewModels.Identity;
using LLF.Static.Locals;
using LLF.ViewModel.Core.Extensions;
using LLF.Claim.Model.ViewModels.Membership;
using LLF.Data.Containers;
using LLF.Abstract.Xml;
using LLF.Abstract.Data.Engine;

namespace LLF.Claim.Model.ViewModels.Extensions {

    public static class ValidationExtensions {

		public static IValidationResponse Validate(this TokenticationViewModel vmodel) {

			return vmodel.CommonValidator<TokenticationViewModel>();

		}

		public static IValidationResponse Validate(this RegisterViewModel vmodel) {

			return vmodel.CommonValidator<RegisterViewModel>((response, vm) => {
				var sep = ResourceContainer.ReadXmlFor<IAttributeItem>("MessageSeperator").Value;
				if (vm.Password != vm.PasswordRepeat) {
					response.AppendMessage("{0} {1}", ValidationMessages.PasswordsDoesNotMatch, sep);
					response.IsValid = false;
				} else {
                    response.IsValid = true;
                }
			});

		}

		public static IValidationResponse Validate(this LoginViewModel vmodel) {

			return vmodel.CommonValidator<LoginViewModel>();

		}

		public static IValidationResponse Validate(this EditMembershipViewModel vmodel) {

			return vmodel.CommonValidator<EditMembershipViewModel>((response, vm) => {
				var sep = ResourceContainer.ReadXmlFor<IAttributeItem>("MessageSeperator").Value;
				if (vm.NewPassword != vm.NewPasswordRepeat) {
					response.AppendMessage("{0} {1}", ValidationMessages.PasswordsDoesNotMatch, sep);
                    response.IsValid = false;
                } else {
                    response.IsValid = true;
                }
			});

		}
	}

}
