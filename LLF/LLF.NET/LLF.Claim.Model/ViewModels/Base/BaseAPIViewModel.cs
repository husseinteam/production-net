﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Abstract.Data.Engine;
using LLF.Claim.Model.ActionResponses;
using LLF.Claim.Model.ViewModels.Identity;
using LLF.Data.Engine;
using Newtonsoft.Json;

namespace LLF.Claim.Model.ViewModels.Base {
	public class BaseAPIViewModel {

		public ApiPostResponse Response { get; set; }

	}
}
