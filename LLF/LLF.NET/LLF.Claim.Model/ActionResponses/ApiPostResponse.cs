﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LLF.Claim.Model.ViewModels.Identity;

namespace LLF.Claim.Model.ActionResponses {

	public class ApiPostResponse {

		public ApiPostResponse() {
			TokenVector = new TokenVector();
		}

		public Boolean HasFault { get; set; }
		public String Message { get; set; }
		public TokenVector TokenVector { get; set; }

	}

}