﻿using System.Diagnostics;
using System.Linq;
using System.Reflection;
using FluentAssertions;
using LLF.Abstract.Data.Engine;
using LLF.Data.Containers;
using LLF.Data.Tools;
using LLF.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TRSM.BusinessLogic.CommonLogic;
using TRSM.BusinessLogic.Mocks;
using TRSM.Model.Core;
using TRSM.Resources.Locals;
using TRSM.Web;

namespace TRSM.BusinessLogic.Tests.UnitTests {

	[TestClass]
	public class RepositoryTests {

		[ClassInitialize]
		public static void ClassInit(TestContext ctx) {

			ResourceContainer.Register<IConnectionCredential>(Xmls.ConnectionCredentials);
			CredentialProvider.CurrentCredential = ResourceContainer
				.ReadXmlFor<IConnectionCredential>("local");

		}

		[TestMethod]
		public void RegenerateDatabase() {

			var expt = DependencyContainer.Take<IDatabaseEngine>()
               .EngineFor<Slide>()
               .EngineFor<Message>()
			   .GenerateModels();

			expt.Should().BeNull("Because There should be no error");

            new SlideRepositoryMock().GetAll().Enumerate(sld => {
                DependencyContainer.RepositoryOf<Slide>().ToDb(sld).Success.Should().BeTrue("because SlideRepositoryMock operation should have been committed successfully");
            });

            new MessageRepositoryMock().GetAll().Enumerate(sld => {
                DependencyContainer.RepositoryOf<Message>().ToDb(sld).Success.Should().BeTrue("because MessageRepositoryMock operation should have been committed successfully");
            });
		}

		[TestMethod]
		public void GetAllSlidesWithMockTest() {

            var repo = new SlideRepository(new SlideRepositoryMock());
			repo.GetAllSlidesOrdered().Should().BeInAscendingOrder(x => x.SlideIndex);
			repo.GetAllSlidesOrdered().Count().Should().BeGreaterThan(0, "because there should be slides in the db");
			repo.GetAllSlidesOrdered().First().SlidePath.Should().NotBeNullOrWhiteSpace("because server path should exists");

		}

		[TestMethod]
		public void GetAllSlidesFromDbTest() {

			var repo = new SlideRepository();
			repo.GetAllSlidesOrdered().Should().BeInAscendingOrder(x => x.SlideIndex);
			repo.GetAllSlidesOrdered().Count().Should().BeGreaterThan(0, "because there should be slides in the db");
			repo.GetAllSlidesOrdered().First().SlidePath.Should().NotBeNullOrWhiteSpace("because server path should exists");

		}

	}

}
