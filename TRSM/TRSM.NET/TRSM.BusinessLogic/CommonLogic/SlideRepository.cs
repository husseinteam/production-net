﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using LLF.Abstract.Data.Engine;
using LLF.Data.Containers;
using LLF.Extensions;
using Newtonsoft.Json;
using TRSM.Model.Core;
using TRSM.ViewModel;
using TRSM.ViewModel.Static;

namespace TRSM.BusinessLogic.CommonLogic {

	public class SlideRepository {

		private ILLFRepository<Slide> _Repository;

		#region Ctor

		public SlideRepository() {

			_Repository = DependencyContainer.RepositoryOf<Slide>();

		}

		public SlideRepository(ILLFRepository<Slide> repository) {

			_Repository = repository;

		}

		#endregion

		public IDMLResponse AddSlide(CroppedSlideViewModel vmodel, String basePath) {

			var validationResponse = vmodel.Validate();
			if (validationResponse.IsValid) {
				Func<String, String> stripFileName = (p) => p.Substring(0, p.LastIndexOf('.'));
				Func<String, String> stripExtension = (p) => p.Substring(p.LastIndexOf('.') + 1);

				var fileName = "{0}-{1}.{2}".PostFormat(stripFileName(Path.GetFileName(vmodel.ImageFile.FileName)),
					Guid.NewGuid().ToString("N"), stripExtension(Path.GetFileName(vmodel.ImageFile.FileName)));
				var destPath = Path.Combine(basePath, "Images", "ClientImages", "ForGallery", fileName);
				var thumbFileName = "thumb-{0}-{1}.{2}".PostFormat(stripFileName(Path.GetFileName(vmodel.ImageFile.FileName)),
					Guid.NewGuid().ToString("N"), stripExtension(Path.GetFileName(vmodel.ImageFile.FileName)));
				var thumbPath = Path.Combine(basePath, "Images", "ClientImages", "ForGallery", "thumbs", thumbFileName);

				Int32 x, y, twidth, theight;
				Single angle;
				ExtractSize(vmodel.ImageData, out twidth, out theight, out x, out y, out angle);

				var clipped = vmodel.ImageFile.InputStream.ToImage()
					.RotateImage(angle)
					.CropImage(x, y, twidth, theight).As<Bitmap>();
				clipped.Save(destPath);
				var thumb = clipped.GetThumbnailImage(300, 200, null, IntPtr.Zero);
				thumb.Save(thumbPath);

				var slide = new Slide() {
					SlideIndex = vmodel.SlideIndex,
					SlideText = vmodel.SlideText,
                    SlideTitle = vmodel.SlideTitle
				};
				slide.FileName = fileName;
				slide.SlidePath = destPath.Replace(basePath, "/").Replace('\\', '/');
				slide.ThumbnailPath = thumbPath.Replace(basePath, "/").Replace('\\', '/');

				return _Repository.ToDb(slide);
			} else {
				var response = DependencyContainer.Take<IDMLResponse>(new { Success = false });
				response.Message = validationResponse.FinalMessage;
				return response;
			}

		}

		private void ExtractSize(String data, out Int32 twidth, out Int32 theight, out Int32 x, out Int32 y, out Single angle) {

			dynamic ddata = JsonConvert.DeserializeObject(data);
			twidth = (Int32)ddata.width;
			theight = (Int32)ddata.height;
			x = (Int32)ddata.x;
			y = (Int32)ddata.y;
			angle = (Single)ddata.rotate;

		}

		public IEnumerable<SlideViewModel> GetAllSlidesOrdered() {

            var resp = _Repository.GetAll().OrderBy(sld => sld.SlideIndex).Select(sl => new SlideViewModel(sl)).ToArray();
            return resp;

		}

	}

}
