﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using LLF.Abstract.Data.Engine;
using LLF.Data.Containers;
using LLF.Extensions;
using Newtonsoft.Json;
using TRSM.Model.Core;
using TRSM.ViewModel;
using TRSM.ViewModel.Static;

namespace TRSM.BusinessLogic.CommonLogic {

	public class MessageRepository {

		private ILLFRepository<Message> _Repository;

		#region Ctor

		public MessageRepository() {

            _Repository = DependencyContainer.RepositoryOf<Message>();

		}

        public MessageRepository(ILLFRepository<Message> repository) {

			_Repository = repository;

		}

		#endregion

        public IDMLResponse AddMessage(MessageViewModel vmodel) {

			var validationResponse = vmodel.Validate();
			if (validationResponse.IsValid) {
                var message = new Message() {
                    Name = vmodel.Name,
                    Email = vmodel.Email,
                    Comments = vmodel.Comments,
                    PostedOn = DateTime.Now
                };
                return _Repository.ToDb(message);
			} else {
				var response = DependencyContainer.Take<IDMLResponse>(new { Success = false });
				response.Message = validationResponse.FinalMessage;
				return response;
			}

		}

        public Message GetMessageByID(Int64 messageID) {

            return _Repository.GetThe(messageID);

        }


        public IDMLResponse ModifyMessage(MessageViewModel vmodel) {

            var validationResponse = vmodel.Validate();
            if (validationResponse.IsValid) {
                var message = new Message() {
                    ID = vmodel.MessageID,
                    Name = vmodel.Name,
                    Email = vmodel.Email,
                    Comments = vmodel.Comments,
                    PostedOn = DateTime.Now,
                };
                return _Repository.ToDb(message);
            } else {
                var response = DependencyContainer.Take<IDMLResponse>(new { Success = false });
                response.Message = validationResponse.FinalMessage;
                return response;
            }

        }

        public IDMLResponse<Message> DeleteMessage(MessageViewModel vmodel) {

            return _Repository.OutOfDb(vmodel.MessageID);

        }

        public IEnumerable<MessageViewModel> GetAllMessages() {

            return _Repository.GetAll().Select(msg => new MessageViewModel(msg)).ToArray();

        }
    }

}
