﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using LLF.Abstract.Data.Engine;
using LLF.Data.Containers;
using LLF.Data.Tools;
using LLF.Extensions;
using TRSM.Resources.Locals;

namespace TRSM.Web {

	public class MvcApplication : System.Web.HttpApplication {

		protected void Application_Start() {

			AreaRegistration.RegisterAllAreas();
			FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
			RouteConfig.RegisterRoutes(RouteTable.Routes);

            BundleConfig.RegisterBundles(BundleTable.Bundles);
            BundleConfig.RegisterBBizBundles(BundleTable.Bundles);
			BundleConfig.RegisterDashboardBundles(BundleTable.Bundles);

		}

		private void RegisterConnectionCredentials() {

			ResourceContainer.Register<IConnectionCredential>(Xmls.ConnectionCredentials);
			CredentialProvider.CurrentCredential = ResourceContainer
				.ReadXmlFor<IConnectionCredential>("local");

		}

		protected void Application_AcquireRequestState(object sender, EventArgs e) {

			String cultureName = null;
			// Attempt to read the culture cookie from Request
			var cultureCookie = Request.Cookies["culture"];
			if (cultureCookie != null)
				cultureName = cultureCookie.Value;
			else
				cultureName = Request.UserLanguages != null ?
					Request.UserLanguages[0] : "tr-TR"; // obtain it from HTTP header AcceptLanguages

			// Modify current thread's cultures            
			Thread.CurrentThread.CurrentCulture = cultureName.Globalize();
			Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;

		}

		protected void Session_Start(object sender, EventArgs e) {

#if DEBUG
			Session["userid"] = 1;
#endif
			RegisterConnectionCredentials();

		}

	}
}
