﻿using System.Web;
using System.Web.Optimization;

namespace TRSM.Web {

    public class BundleConfig {

        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles) {

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));
            bundles.Add(new ScriptBundle("~/bundles/msajax")
                .Include("~/Scripts/jquery.validate*")
                .Include("~/Scripts/jquery.unobtrusive-ajax.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/gridmvc")
                .Include("~/Scripts/gridmvc.min.js"));


            bundles.Add(new StyleBundle("~/Content/gridmvc").Include(
                      "~/Content/Gridmvc.css"));


            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/font-awesome.min.css",
                      "~/Content/bootstrap.min.css",
                      "~/Content/reset.css",
                      "~/Content/custom.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/common/css").Include(
                      "~/Content/site.css"));

        }

        public static void RegisterBBizBundles(BundleCollection bundles) {

            var basePath = "~/Themes/bbiz/";

            bundles.Add(new ScriptBundle("~/bundles/bbiz/js").Include(
                       basePath + "scripts/jquery.min.js",
                       basePath + "scripts/bootstrap/js/bootstrap.min.js",
                       basePath + "scripts/default.js",
                       basePath + "scripts/carousel/jquery.carouFredSel-6.2.0-packed.js",
                       basePath + "scripts/camera/scripts/camera.min.js",
                       basePath + "scripts/easing/jquery.easing.1.3.js"
                       ));

            bundles.Add(new ScriptBundle("~/bundles/bbiz/portfolio").Include(
                       basePath + "scripts/wookmark/js/jquery.wookmark.js",
                       basePath + "scripts/yoxview/yox.js",
                       basePath + "scripts/yoxview/jquery.yoxview-2.21.js"
                       ));

            bundles.Add(new StyleBundle("~/Content/bbiz/yoxcss").Include(
                      basePath + "scripts/wookmark/css/style.css",
                      basePath + "scripts/yoxview/yoxview.css"
                      ));

            bundles.Add(new StyleBundle("~/Content/bbiz/css").Include(
                      "~/Content/font-awesome.min.css",
                      basePath + "scripts/bootstrap/css/bootstrap.min.css",
                      basePath + "scripts/bootstrap/css/bootstrap-responsive.min.css",
                      basePath + "scripts/icons/general/stylesheets/general_foundicons.css",
                      basePath + "scripts/icons/social/stylesheets/social_foundicons.css",
                      basePath + "scripts/fontawesome/css/font-awesome.min.css",
                      basePath + "scripts/carousel/style.css",
                      basePath + "scripts/camera/css/camera.css",
                      basePath + "styles/custom.css"
                      ));
        }

        public static void RegisterDashboardBundles(BundleCollection bundles) {

            bundles.Add(new ScriptBundle("~/bundles/dashboard").Include(
                      "~/Scripts/dashboard/jquery.min.js",
                      "~/Scripts/dashboard/bootstrap.min.js",
                      "~/Scripts/common/lmpc-js.js",
                      "~/Scripts/dashboard/metisMenu.min.js",
                      "~/Scripts/dashboard/sb-admin-2.js"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/dashboard/index").Include(
                      "~/Scripts/dashboard/raphael-min.js",
                      "~/Scripts/dashboard/morris.min.js",
                      "~/Scripts/dashboard/morris-data.js"
                      ));

            bundles.Add(new StyleBundle("~/Content/dashboard").Include(
                      "~/Content/dashboard/bootstrap.min.css",
                      "~/Content/dashboard/metisMenu.min.css",
                      "~/Content/dashboard/timeline.css",
                      "~/Content/dashboard/morris.css",
                      "~/Content/dashboard/font-awesome.min.css",
                      "~/Content/dashboard/sb-admin-2.css",
                      "~/Content/cropper.min.css",
                      "~/Content/cropper-main.css"));

        }

    }

}
