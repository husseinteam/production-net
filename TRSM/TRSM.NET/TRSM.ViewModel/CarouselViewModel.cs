﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Annotations.ViewModel;
using TRSM.Model.Core;
using TRSM.Resources.Locals;

namespace TRSM.ViewModel {

	[LocalResource(typeof(ValidationErrors))]
	public class CarouselViewModel {

		#region Ctors

		public CarouselViewModel(String galleryTitle, String gallerySubTitle) {

			this.GalleryTitle = galleryTitle;
			this.GallerySubTitle = gallerySubTitle;

		}

		public CarouselViewModel() {

		}

		#endregion

		#region UI

		public String GalleryTitle { get; set; }
		public String GallerySubTitle { get; set; }

		#endregion

		#region Properties

		public IEnumerable<SlideViewModel> Slides { get; set; }

		#endregion

	}

}
