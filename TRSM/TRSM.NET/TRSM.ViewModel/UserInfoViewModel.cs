﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TRSM.ViewModel.Enums;
using LLF.Extensions;
using LLF.Abstract.Data.Engine;

namespace TRSM.ViewModel {

    public class UserInfoViewModel {

        public UserInfoViewModel(IDMLResponse resp) {

            ContextualClass = resp.Success ? EContextualClass.Success : EContextualClass.Danger;
            Message = resp.Message;
            Success = resp.Success;

        }

        public UserInfoViewModel() {
        }

        public EContextualClass ContextualClass { get; private set; }

        public Boolean Success { get; private set; }
        public String Message { get; private set; }

        public String ContextualClassText {
            get {
                return ContextualClass.GetDescription();
            }
        }

    }

}
