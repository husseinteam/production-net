﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Annotations.ViewModel;
using TRSM.Model.Core;
using TRSM.Resources.Locals;
using LLF.Extensions;
using System.Web;
using LLF.Data.Containers;

namespace TRSM.ViewModel {

	[LocalResource(typeof(ValidationErrors))]
	public class CroppedSlideViewModel : SlideViewModel {

		#region Ctors

		public CroppedSlideViewModel() {

			var repo = DependencyContainer.RepositoryOf<Slide>();
			this.AvaliableSlideIndices = (repo.GetCountOf() + 66).Populate().Except(repo.GetAll().Select(sld => sld.SlideIndex));
			this.SlideIndex = this.AvaliableSlideIndices.FirstOrDefault();

		}

		public CroppedSlideViewModel(NameValueCollection requestForm, HttpFileCollectionBase requestFiles) {

			this.ImageSource = requestForm["ImageSource"].ToType<String>();
			this.ImageData = requestForm["ImageData"].ToType<String>();
			this.ImageFile = requestFiles["ImageFile"];
			this.SlideIndex = requestForm["SlideIndex"].ToType<Int32>();
            this.SlideText = requestForm["SlideText"].ToType<String>();
            this.SlideTitle = requestForm["SlideTitle"].ToType<String>();

		}

		#endregion

		#region UI

		public String ImageSource { get; set; }
		public String ImageData { get; set; }
		[ToUI(ValidationErrorKey = "NullErrorImageFile")]
		public HttpPostedFileBase ImageFile { get; set; }

		#endregion

		#region Properties

		public IEnumerable<Int32> AvaliableSlideIndices { get; set; }
		public Int32 FirstAvaliableSlideIndex { get; set; }

		#endregion

	}

}
