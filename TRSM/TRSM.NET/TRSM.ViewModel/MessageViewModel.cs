﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Annotations.ViewModel;
using TRSM.Model.Core;
using TRSM.Resources.Locals;
using LLF.ViewModel.Core;

namespace TRSM.ViewModel {

	[LocalResource(typeof(ValidationErrors))]
	public class MessageViewModel {

		#region Ctors
		
		public MessageViewModel() {

            this.ViewModelMode = EViewModelMode.ViewMode;

		}

        public MessageViewModel(Message message) 
            : this() {

            this.Name = message.Name;
            this.Email = message.Email;
            this.Comments = message.Comments;
            this.MessageID = message.ID;
            this.PostedOn = message.PostedOn;

        }

		#endregion

        #region Properties

        public Int64 MessageID { get; set; }

        public Boolean ModalActivated { get; set; }

        public String Redirect { get; set; }


        public EViewModelMode ViewModelMode { get; set; }

        #endregion

        #region ToUI

        [ToUI(ValidationErrorKey = "NullErrorContactName")]
        public String Name { get; set; }
        [ToUI(ValidationErrorKey = "NullErrorContactEmail")]
        [ApplyRegex("Email", "EmailValidationError")]
        public String Email { get; set; }
        [ToUI(ValidationErrorKey = "NullErrorContactComments")]
        public String Comments { get; set; }
        [ToUI(ValidationErrorKey = "NullErrorContactPostedOn")]
        public DateTime PostedOn { get; set; }

		#endregion

	}

}
