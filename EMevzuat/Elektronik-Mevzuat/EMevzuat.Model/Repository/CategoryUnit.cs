﻿using EMevzuat.Model.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace EMevzuat.Model.Repository {
    internal class CategoryUnit : ScienceLab<MTSAVCategory>, IScienceLab<MTSAVCategory> {

        #region ctor
        internal CategoryUnit(Entities context)
            : base(context) {
            LegislationUnitOfWork = new LegislationUnit(context);
            RegisterRepository<MTSAVCategoriesForPackage>();
        }

        #endregion

        #region IRepository Members

        public override IScienceLab<MTSAVCategory> Disjoint(Object entryID) {
            if (null != entryID) {
                IEnumerable<MTSAVCategory> children;
                var id = Int32.Parse(entryID.ToString());
                var entry = Locate(entryID);
                children = Locate((ctg) => ctg.ParentID == id);
                foreach (MTSAVCategory ent in children) {
                    ent.MTSAVLegislations.ToList().ForEach((l) => LegislationUnitOfWork.Disjoint(l.ID));
                    ent.MTSAVCategoriesForPackages.ToList().ForEach((cfp) => Repository<MTSAVCategoriesForPackage>().Disjoint(cfp.ID));
                    Disjoint(ent.ID);
                }
                entry.MTSAVLegislations.ToList().ForEach((l) => LegislationUnitOfWork.Disjoint(l.ID));
                entry.MTSAVCategoriesForPackages.ToList().ForEach((cfp) => Repository<MTSAVCategoriesForPackage>().Disjoint(cfp.ID));
                return base.Disjoint(entryID);
            } else {
                return this;
            }
        }

        #endregion

        #region Properties
        private LegislationUnit LegislationUnitOfWork;
        #endregion

    }
}
