﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EMevzuat.Model.Repository {
    internal interface IScienceLab<TEntry> : IDisposable {
        ICollection<TEntry> Locate();
        ICollection<TEntry> Locate(
            Func<TEntry, bool> filter,
            Func<IQueryable<TEntry>, IOrderedQueryable<TEntry>> orderBy = null,
            string includeProperties = "");
        TEntry Locate(Object entryID);
        void RunStoredProcedure(String sprocIdentifier, params Object[] parameters);
        IScienceLab<TEntry> Articulate(TEntry entry);
        IScienceLab<TEntry> Modify(TEntry entry, Object entryID);
        IScienceLab<TEntry> Disjoint(Object entryID);
        IScienceLab<TOther> RepositoryGenerator<TOther>() where TOther : class;
        Boolean Commit();
        Boolean Commit(out Exception exception);
    }
}
