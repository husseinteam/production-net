﻿using EMevzuat.Logic.Parse;
using EMevzuat.Model.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace EMevzuat.Model.Repository {
    internal class LegislationUnit : ScienceLab<MTSAVLegislation>, IScienceLab<MTSAVLegislation> {

        #region ctor
        internal LegislationUnit(Entities context)
            : base(context) {
            RegisterRepository<MTSAVCategoriesForPackage>();
            RegisterRepository<MTSAVLegislationsForUser>();
            RegisterRepository<MTSAVFile>();
        }
        #endregion

        #region Overrides
        public override IScienceLab<MTSAVLegislation> Disjoint(Object entryID) {
            var entry = Locate(entryID);
            entry.MTSAVCategory.MTSAVCategoriesForPackages.ToList().ForEach((cp) => Repository<MTSAVCategoriesForPackage>().Disjoint(cp.ID));
            entry.MTSAVLegislationsForUsers.ToList().ForEach((lu) => Repository<MTSAVLegislationsForUser>().Disjoint(lu.ID));
            var files = Repository<MTSAVFile>().Locate(f => f.ReferencedPK == entryID.ToString() || f.ReferencedPK == Guid.Empty.ToString());
            foreach (var file in files) {
                Repository<MTSAVFile>().Disjoint(file.ID);
            }
            return base.Disjoint(entryID);
        }

        #endregion

    }
}
