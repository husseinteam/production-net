﻿using EMevzuat.Model.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace EMevzuat.Model.Repository {
    internal class ExceptionUnit : ScienceLab<MTSAVException>, IScienceLab<MTSAVException> {

        #region ctor

        internal ExceptionUnit(Entities context)
            : base(context) {
            RegisterRepository<MTSAVSolution>();
        }

        #endregion

        #region Overrides

        public override IScienceLab<MTSAVException> Disjoint(Object entryID) {
            var entry = Locate(entryID);
            entry.MTSAVSolutions.ToList().ForEach((sln) => Repository<MTSAVSolution>().Disjoint(sln.ID));
            return base.Disjoint(entryID);
        }

        #endregion

    }
}
