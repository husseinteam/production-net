﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

using EMevzuat.Model.Repository;
using EMevzuat.Model.Context;

namespace EMevzuat.Model.Repository {
    internal class PackageUnit : ScienceLab<MTSAVPackage>, IScienceLab<MTSAVPackage> {

        #region ctor
        internal PackageUnit(Entities context)
            : base(context) {
            RegisterRepository<MTSAVBulletin>();
            RegisterRepository<MTSAVCategoriesForPackage>();
        }
        #endregion

        #region Overrides
        public override IScienceLab<MTSAVPackage> Disjoint(Object entryID) {
            var entry = Locate(entryID);
            entry.MTSAVBulletins.ToList().ForEach((blt) => Repository<MTSAVBulletin>().Disjoint(blt.ID));
            entry.MTSAVCategoriesForPackages.ToList().ForEach((cfp) => Repository<MTSAVCategoriesForPackage>().Disjoint(cfp.ID));
            return base.Disjoint(entryID);
        }
        #endregion

    }
}
