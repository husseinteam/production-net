//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EMevzuat.Model.Context
{
    using System;
    using System.Collections.Generic;
    
    public partial class MTSAVLegislationsForUser
    {
        public System.Guid ID { get; set; }
        public System.Guid UserID { get; set; }
        public System.DateTime Timestamp { get; set; }
        public System.Guid LegislationID { get; set; }
    
        public virtual MTSAVLegislation MTSAVLegislation { get; set; }
        public virtual MTSAVUser MTSAVUser { get; set; }
    }
}
