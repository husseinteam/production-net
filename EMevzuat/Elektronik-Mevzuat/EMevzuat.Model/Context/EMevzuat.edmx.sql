
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 09/19/2014 20:25:35
-- Generated from EDMX file: C:\Users\Özgür\Documents\Visual Studio 2013\Projects\Lampiclobe\Elektronik-Mevzuat\EMevzuat\EMevzuat.Model\Context\EMevzuat.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [emevzuatdb];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_MTSAVAdmin_MTSAVUsers]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MTSAVAdministrators] DROP CONSTRAINT [FK_MTSAVAdmin_MTSAVUsers];
GO
IF OBJECT_ID(N'[dbo].[FK_MTSAVAnnouncements_MTSAVMembershipPackages]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MTSAVBulletins] DROP CONSTRAINT [FK_MTSAVAnnouncements_MTSAVMembershipPackages];
GO
IF OBJECT_ID(N'[dbo].[FK_MTSAVDocumentsForUsers_MTSAVUsers]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MTSAVLegislationsForUsers] DROP CONSTRAINT [FK_MTSAVDocumentsForUsers_MTSAVUsers];
GO
IF OBJECT_ID(N'[dbo].[FK_MTSAVExceptions_MTSAVUsers]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MTSAVExceptions] DROP CONSTRAINT [FK_MTSAVExceptions_MTSAVUsers];
GO
IF OBJECT_ID(N'[dbo].[FK_MTSAVHtmls_ToMTSAVHtmlPages]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MTSAVHtmls] DROP CONSTRAINT [FK_MTSAVHtmls_ToMTSAVHtmlPages];
GO
IF OBJECT_ID(N'[dbo].[FK_MTSAVLegislationCategories_MTSAVLegislationCategories]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MTSAVCategories] DROP CONSTRAINT [FK_MTSAVLegislationCategories_MTSAVLegislationCategories];
GO
IF OBJECT_ID(N'[dbo].[FK_MTSAVLegislationCategoriesForMembershipPackages_MTSAVLegislationCategories]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MTSAVCategoriesForPackages] DROP CONSTRAINT [FK_MTSAVLegislationCategoriesForMembershipPackages_MTSAVLegislationCategories];
GO
IF OBJECT_ID(N'[dbo].[FK_MTSAVLegislationCategoriesForMembershipPackages_MTSAVMembershipPackages]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MTSAVCategoriesForPackages] DROP CONSTRAINT [FK_MTSAVLegislationCategoriesForMembershipPackages_MTSAVMembershipPackages];
GO
IF OBJECT_ID(N'[dbo].[FK_MTSAVLegislations_MTSAVLegislationCategories]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MTSAVLegislations] DROP CONSTRAINT [FK_MTSAVLegislations_MTSAVLegislationCategories];
GO
IF OBJECT_ID(N'[dbo].[FK_MTSAVLegislationsForUsers_MTSAVLegislations]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MTSAVLegislationsForUsers] DROP CONSTRAINT [FK_MTSAVLegislationsForUsers_MTSAVLegislations];
GO
IF OBJECT_ID(N'[dbo].[FK_MTSAVMails_MTSAVUsers]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MTSAVMails] DROP CONSTRAINT [FK_MTSAVMails_MTSAVUsers];
GO
IF OBJECT_ID(N'[dbo].[FK_MTSAVPayments_MTSAVUsers]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MTSAVPayments] DROP CONSTRAINT [FK_MTSAVPayments_MTSAVUsers];
GO
IF OBJECT_ID(N'[dbo].[FK_MTSAVSolutions_MTSAVExceptions]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MTSAVSolutions] DROP CONSTRAINT [FK_MTSAVSolutions_MTSAVExceptions];
GO
IF OBJECT_ID(N'[dbo].[FK_MTSAVUserActions_MTSAVUsers]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MTSAVUserActions] DROP CONSTRAINT [FK_MTSAVUserActions_MTSAVUsers];
GO
IF OBJECT_ID(N'[dbo].[FK_MTSAVUsers_MTSAVPackages]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MTSAVUsers] DROP CONSTRAINT [FK_MTSAVUsers_MTSAVPackages];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[MTSAVAdministrators]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MTSAVAdministrators];
GO
IF OBJECT_ID(N'[dbo].[MTSAVBulletins]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MTSAVBulletins];
GO
IF OBJECT_ID(N'[dbo].[MTSAVCategories]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MTSAVCategories];
GO
IF OBJECT_ID(N'[dbo].[MTSAVCategoriesForPackages]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MTSAVCategoriesForPackages];
GO
IF OBJECT_ID(N'[dbo].[MTSAVExceptions]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MTSAVExceptions];
GO
IF OBJECT_ID(N'[dbo].[MTSAVFiles]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MTSAVFiles];
GO
IF OBJECT_ID(N'[dbo].[MTSAVHtmlPages]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MTSAVHtmlPages];
GO
IF OBJECT_ID(N'[dbo].[MTSAVHtmls]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MTSAVHtmls];
GO
IF OBJECT_ID(N'[dbo].[MTSAVLegislations]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MTSAVLegislations];
GO
IF OBJECT_ID(N'[dbo].[MTSAVLegislationsForUsers]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MTSAVLegislationsForUsers];
GO
IF OBJECT_ID(N'[dbo].[MTSAVMails]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MTSAVMails];
GO
IF OBJECT_ID(N'[dbo].[MTSAVPackages]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MTSAVPackages];
GO
IF OBJECT_ID(N'[dbo].[MTSAVPayments]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MTSAVPayments];
GO
IF OBJECT_ID(N'[dbo].[MTSAVSolutions]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MTSAVSolutions];
GO
IF OBJECT_ID(N'[dbo].[MTSAVUserActions]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MTSAVUserActions];
GO
IF OBJECT_ID(N'[dbo].[MTSAVUsers]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MTSAVUsers];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'MTSAVAdministrators'
CREATE TABLE [dbo].[MTSAVAdministrators] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [UserID] uniqueidentifier  NOT NULL,
    [IsDeveloper] bit  NOT NULL
);
GO

-- Creating table 'MTSAVBulletins'
CREATE TABLE [dbo].[MTSAVBulletins] (
    [ID] uniqueidentifier  NOT NULL,
    [Title] nvarchar(150)  NOT NULL,
    [Body] nvarchar(max)  NOT NULL,
    [Timestamp] datetime  NOT NULL,
    [PublishDate] datetime  NULL,
    [Announced] bit  NOT NULL,
    [PackageID] int  NOT NULL
);
GO

-- Creating table 'MTSAVCategories'
CREATE TABLE [dbo].[MTSAVCategories] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(50)  NOT NULL,
    [Description] nvarchar(100)  NOT NULL,
    [ParentID] int  NULL
);
GO

-- Creating table 'MTSAVCategoriesForPackages'
CREATE TABLE [dbo].[MTSAVCategoriesForPackages] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [PackageID] int  NOT NULL,
    [CategoryID] int  NOT NULL,
    [Allowed] bit  NOT NULL
);
GO

-- Creating table 'MTSAVExceptions'
CREATE TABLE [dbo].[MTSAVExceptions] (
    [ID] uniqueidentifier  NOT NULL,
    [Message] nvarchar(max)  NOT NULL,
    [Detail] nvarchar(max)  NOT NULL,
    [IsCrucial] bit  NOT NULL,
    [UserID] uniqueidentifier  NOT NULL
);
GO

-- Creating table 'MTSAVFiles'
CREATE TABLE [dbo].[MTSAVFiles] (
    [ID] uniqueidentifier  NOT NULL,
    [Name] nvarchar(255)  NOT NULL,
    [Data] varbinary(max)  NOT NULL,
    [Extension] nvarchar(8)  NOT NULL,
    [ReferencedPK] nvarchar(36)  NOT NULL,
    [TableID] int  NOT NULL,
    [Index] int  NULL
);
GO

-- Creating table 'MTSAVHtmlPages'
CREATE TABLE [dbo].[MTSAVHtmlPages] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [RelativeUrl] nvarchar(512)  NOT NULL,
    [Description] nvarchar(50)  NOT NULL,
    [Route] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'MTSAVHtmls'
CREATE TABLE [dbo].[MTSAVHtmls] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [Html] nvarchar(max)  NOT NULL,
    [PageID] int  NOT NULL
);
GO

-- Creating table 'MTSAVLegislations'
CREATE TABLE [dbo].[MTSAVLegislations] (
    [ID] uniqueidentifier  NOT NULL,
    [Title] nvarchar(500)  NOT NULL,
    [Text] nvarchar(max)  NOT NULL,
    [Tags] nvarchar(max)  NOT NULL,
    [Timestamp] datetime  NOT NULL,
    [CategoryID] int  NOT NULL,
    [PdfData] varbinary(max)  NOT NULL
);
GO

-- Creating table 'MTSAVLegislationsForUsers'
CREATE TABLE [dbo].[MTSAVLegislationsForUsers] (
    [ID] uniqueidentifier  NOT NULL,
    [UserID] uniqueidentifier  NOT NULL,
    [Timestamp] datetime  NOT NULL,
    [LegislationID] uniqueidentifier  NOT NULL
);
GO

-- Creating table 'MTSAVMails'
CREATE TABLE [dbo].[MTSAVMails] (
    [ID] uniqueidentifier  NOT NULL,
    [Subject] nvarchar(256)  NOT NULL,
    [Body] nvarchar(max)  NOT NULL,
    [Html] bit  NOT NULL,
    [Timestamp] datetime  NOT NULL,
    [Success] bit  NOT NULL,
    [SenderID] uniqueidentifier  NOT NULL,
    [Recipient] nvarchar(255)  NOT NULL
);
GO

-- Creating table 'MTSAVPayments'
CREATE TABLE [dbo].[MTSAVPayments] (
    [ID] uniqueidentifier  NOT NULL,
    [Amount] decimal(19,4)  NOT NULL,
    [Timestamp] datetime  NOT NULL,
    [UserID] uniqueidentifier  NOT NULL,
    [Status] int  NOT NULL
);
GO

-- Creating table 'MTSAVSolutions'
CREATE TABLE [dbo].[MTSAVSolutions] (
    [ID] uniqueidentifier  NOT NULL,
    [Reply] nvarchar(max)  NOT NULL,
    [Solved] bit  NOT NULL,
    [ExceptionID] uniqueidentifier  NOT NULL
);
GO

-- Creating table 'MTSAVUserActions'
CREATE TABLE [dbo].[MTSAVUserActions] (
    [ID] uniqueidentifier  NOT NULL,
    [Timestamp] datetime  NOT NULL,
    [Action] int  NOT NULL,
    [UserID] uniqueidentifier  NOT NULL
);
GO

-- Creating table 'MTSAVPackages'
CREATE TABLE [dbo].[MTSAVPackages] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(50)  NOT NULL,
    [MembershipFee] decimal(19,4)  NOT NULL,
    [ExpireDuration] int  NOT NULL,
    [MaxDocCount] int  NOT NULL
);
GO

-- Creating table 'MTSAVUsers'
CREATE TABLE [dbo].[MTSAVUsers] (
    [ID] uniqueidentifier  NOT NULL,
    [IPv4] nvarchar(50)  NOT NULL,
    [IPv6] nvarchar(50)  NULL,
    [MailAddress] nvarchar(255)  NOT NULL,
    [FullName] nvarchar(255)  NOT NULL,
    [Cipher] varbinary(255)  NOT NULL,
    [Key] varbinary(max)  NOT NULL,
    [IV] varbinary(max)  NOT NULL,
    [ActivationCode] uniqueidentifier  NULL,
    [MembershipDate] datetime  NOT NULL,
    [Institution] nvarchar(60)  NOT NULL,
    [Position] nvarchar(60)  NOT NULL,
    [MembershipPackageID] int  NOT NULL,
    [ViewToken] uniqueidentifier  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [ID] in table 'MTSAVAdministrators'
ALTER TABLE [dbo].[MTSAVAdministrators]
ADD CONSTRAINT [PK_MTSAVAdministrators]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'MTSAVBulletins'
ALTER TABLE [dbo].[MTSAVBulletins]
ADD CONSTRAINT [PK_MTSAVBulletins]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'MTSAVCategories'
ALTER TABLE [dbo].[MTSAVCategories]
ADD CONSTRAINT [PK_MTSAVCategories]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'MTSAVCategoriesForPackages'
ALTER TABLE [dbo].[MTSAVCategoriesForPackages]
ADD CONSTRAINT [PK_MTSAVCategoriesForPackages]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'MTSAVExceptions'
ALTER TABLE [dbo].[MTSAVExceptions]
ADD CONSTRAINT [PK_MTSAVExceptions]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'MTSAVFiles'
ALTER TABLE [dbo].[MTSAVFiles]
ADD CONSTRAINT [PK_MTSAVFiles]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'MTSAVHtmlPages'
ALTER TABLE [dbo].[MTSAVHtmlPages]
ADD CONSTRAINT [PK_MTSAVHtmlPages]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'MTSAVHtmls'
ALTER TABLE [dbo].[MTSAVHtmls]
ADD CONSTRAINT [PK_MTSAVHtmls]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'MTSAVLegislations'
ALTER TABLE [dbo].[MTSAVLegislations]
ADD CONSTRAINT [PK_MTSAVLegislations]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'MTSAVLegislationsForUsers'
ALTER TABLE [dbo].[MTSAVLegislationsForUsers]
ADD CONSTRAINT [PK_MTSAVLegislationsForUsers]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'MTSAVMails'
ALTER TABLE [dbo].[MTSAVMails]
ADD CONSTRAINT [PK_MTSAVMails]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'MTSAVPayments'
ALTER TABLE [dbo].[MTSAVPayments]
ADD CONSTRAINT [PK_MTSAVPayments]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'MTSAVSolutions'
ALTER TABLE [dbo].[MTSAVSolutions]
ADD CONSTRAINT [PK_MTSAVSolutions]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'MTSAVUserActions'
ALTER TABLE [dbo].[MTSAVUserActions]
ADD CONSTRAINT [PK_MTSAVUserActions]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'MTSAVPackages'
ALTER TABLE [dbo].[MTSAVPackages]
ADD CONSTRAINT [PK_MTSAVPackages]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'MTSAVUsers'
ALTER TABLE [dbo].[MTSAVUsers]
ADD CONSTRAINT [PK_MTSAVUsers]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [ParentID] in table 'MTSAVCategories'
ALTER TABLE [dbo].[MTSAVCategories]
ADD CONSTRAINT [FK_MTSAVLegislationCategories_MTSAVLegislationCategories]
    FOREIGN KEY ([ParentID])
    REFERENCES [dbo].[MTSAVCategories]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MTSAVLegislationCategories_MTSAVLegislationCategories'
CREATE INDEX [IX_FK_MTSAVLegislationCategories_MTSAVLegislationCategories]
ON [dbo].[MTSAVCategories]
    ([ParentID]);
GO

-- Creating foreign key on [CategoryID] in table 'MTSAVCategoriesForPackages'
ALTER TABLE [dbo].[MTSAVCategoriesForPackages]
ADD CONSTRAINT [FK_MTSAVLegislationCategoriesForMembershipPackages_MTSAVLegislationCategories]
    FOREIGN KEY ([CategoryID])
    REFERENCES [dbo].[MTSAVCategories]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MTSAVLegislationCategoriesForMembershipPackages_MTSAVLegislationCategories'
CREATE INDEX [IX_FK_MTSAVLegislationCategoriesForMembershipPackages_MTSAVLegislationCategories]
ON [dbo].[MTSAVCategoriesForPackages]
    ([CategoryID]);
GO

-- Creating foreign key on [CategoryID] in table 'MTSAVLegislations'
ALTER TABLE [dbo].[MTSAVLegislations]
ADD CONSTRAINT [FK_MTSAVLegislations_MTSAVLegislationCategories]
    FOREIGN KEY ([CategoryID])
    REFERENCES [dbo].[MTSAVCategories]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MTSAVLegislations_MTSAVLegislationCategories'
CREATE INDEX [IX_FK_MTSAVLegislations_MTSAVLegislationCategories]
ON [dbo].[MTSAVLegislations]
    ([CategoryID]);
GO

-- Creating foreign key on [ExceptionID] in table 'MTSAVSolutions'
ALTER TABLE [dbo].[MTSAVSolutions]
ADD CONSTRAINT [FK_MTSAVSolutions_MTSAVExceptions]
    FOREIGN KEY ([ExceptionID])
    REFERENCES [dbo].[MTSAVExceptions]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MTSAVSolutions_MTSAVExceptions'
CREATE INDEX [IX_FK_MTSAVSolutions_MTSAVExceptions]
ON [dbo].[MTSAVSolutions]
    ([ExceptionID]);
GO

-- Creating foreign key on [PageID] in table 'MTSAVHtmls'
ALTER TABLE [dbo].[MTSAVHtmls]
ADD CONSTRAINT [FK_MTSAVHtmls_ToMTSAVHtmlPages]
    FOREIGN KEY ([PageID])
    REFERENCES [dbo].[MTSAVHtmlPages]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MTSAVHtmls_ToMTSAVHtmlPages'
CREATE INDEX [IX_FK_MTSAVHtmls_ToMTSAVHtmlPages]
ON [dbo].[MTSAVHtmls]
    ([PageID]);
GO

-- Creating foreign key on [LegislationID] in table 'MTSAVLegislationsForUsers'
ALTER TABLE [dbo].[MTSAVLegislationsForUsers]
ADD CONSTRAINT [FK_MTSAVLegislationsForUsers_MTSAVLegislations]
    FOREIGN KEY ([LegislationID])
    REFERENCES [dbo].[MTSAVLegislations]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MTSAVLegislationsForUsers_MTSAVLegislations'
CREATE INDEX [IX_FK_MTSAVLegislationsForUsers_MTSAVLegislations]
ON [dbo].[MTSAVLegislationsForUsers]
    ([LegislationID]);
GO

-- Creating foreign key on [PackageID] in table 'MTSAVBulletins'
ALTER TABLE [dbo].[MTSAVBulletins]
ADD CONSTRAINT [FK_MTSAVAnnouncements_MTSAVMembershipPackages]
    FOREIGN KEY ([PackageID])
    REFERENCES [dbo].[MTSAVPackages]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MTSAVAnnouncements_MTSAVMembershipPackages'
CREATE INDEX [IX_FK_MTSAVAnnouncements_MTSAVMembershipPackages]
ON [dbo].[MTSAVBulletins]
    ([PackageID]);
GO

-- Creating foreign key on [PackageID] in table 'MTSAVCategoriesForPackages'
ALTER TABLE [dbo].[MTSAVCategoriesForPackages]
ADD CONSTRAINT [FK_MTSAVLegislationCategoriesForMembershipPackages_MTSAVMembershipPackages]
    FOREIGN KEY ([PackageID])
    REFERENCES [dbo].[MTSAVPackages]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MTSAVLegislationCategoriesForMembershipPackages_MTSAVMembershipPackages'
CREATE INDEX [IX_FK_MTSAVLegislationCategoriesForMembershipPackages_MTSAVMembershipPackages]
ON [dbo].[MTSAVCategoriesForPackages]
    ([PackageID]);
GO

-- Creating foreign key on [UserID] in table 'MTSAVAdministrators'
ALTER TABLE [dbo].[MTSAVAdministrators]
ADD CONSTRAINT [FK_MTSAVAdmin_MTSAVUsers]
    FOREIGN KEY ([UserID])
    REFERENCES [dbo].[MTSAVUsers]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MTSAVAdmin_MTSAVUsers'
CREATE INDEX [IX_FK_MTSAVAdmin_MTSAVUsers]
ON [dbo].[MTSAVAdministrators]
    ([UserID]);
GO

-- Creating foreign key on [UserID] in table 'MTSAVExceptions'
ALTER TABLE [dbo].[MTSAVExceptions]
ADD CONSTRAINT [FK_MTSAVExceptions_MTSAVUsers]
    FOREIGN KEY ([UserID])
    REFERENCES [dbo].[MTSAVUsers]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MTSAVExceptions_MTSAVUsers'
CREATE INDEX [IX_FK_MTSAVExceptions_MTSAVUsers]
ON [dbo].[MTSAVExceptions]
    ([UserID]);
GO

-- Creating foreign key on [UserID] in table 'MTSAVLegislationsForUsers'
ALTER TABLE [dbo].[MTSAVLegislationsForUsers]
ADD CONSTRAINT [FK_MTSAVDocumentsForUsers_MTSAVUsers]
    FOREIGN KEY ([UserID])
    REFERENCES [dbo].[MTSAVUsers]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MTSAVDocumentsForUsers_MTSAVUsers'
CREATE INDEX [IX_FK_MTSAVDocumentsForUsers_MTSAVUsers]
ON [dbo].[MTSAVLegislationsForUsers]
    ([UserID]);
GO

-- Creating foreign key on [SenderID] in table 'MTSAVMails'
ALTER TABLE [dbo].[MTSAVMails]
ADD CONSTRAINT [FK_MTSAVMails_MTSAVUsers]
    FOREIGN KEY ([SenderID])
    REFERENCES [dbo].[MTSAVUsers]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MTSAVMails_MTSAVUsers'
CREATE INDEX [IX_FK_MTSAVMails_MTSAVUsers]
ON [dbo].[MTSAVMails]
    ([SenderID]);
GO

-- Creating foreign key on [MembershipPackageID] in table 'MTSAVUsers'
ALTER TABLE [dbo].[MTSAVUsers]
ADD CONSTRAINT [FK_MTSAVUsers_MTSAVPackages]
    FOREIGN KEY ([MembershipPackageID])
    REFERENCES [dbo].[MTSAVPackages]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MTSAVUsers_MTSAVPackages'
CREATE INDEX [IX_FK_MTSAVUsers_MTSAVPackages]
ON [dbo].[MTSAVUsers]
    ([MembershipPackageID]);
GO

-- Creating foreign key on [UserID] in table 'MTSAVPayments'
ALTER TABLE [dbo].[MTSAVPayments]
ADD CONSTRAINT [FK_MTSAVPayments_MTSAVUsers]
    FOREIGN KEY ([UserID])
    REFERENCES [dbo].[MTSAVUsers]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MTSAVPayments_MTSAVUsers'
CREATE INDEX [IX_FK_MTSAVPayments_MTSAVUsers]
ON [dbo].[MTSAVPayments]
    ([UserID]);
GO

-- Creating foreign key on [UserID] in table 'MTSAVUserActions'
ALTER TABLE [dbo].[MTSAVUserActions]
ADD CONSTRAINT [FK_MTSAVUserActions_MTSAVUsers]
    FOREIGN KEY ([UserID])
    REFERENCES [dbo].[MTSAVUsers]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MTSAVUserActions_MTSAVUsers'
CREATE INDEX [IX_FK_MTSAVUserActions_MTSAVUsers]
ON [dbo].[MTSAVUserActions]
    ([UserID]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------