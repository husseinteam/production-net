//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EMevzuat.Model.Context
{
    using System;
    using System.Collections.Generic;
    
    public partial class MTSAVHtmlPage
    {
        public MTSAVHtmlPage()
        {
            this.MTSAVHtmls = new HashSet<MTSAVHtml>();
        }
    
        public int ID { get; set; }
        public string RelativeUrl { get; set; }
        public string Description { get; set; }
        public string Route { get; set; }
    
        public virtual ICollection<MTSAVHtml> MTSAVHtmls { get; set; }
    }
}
