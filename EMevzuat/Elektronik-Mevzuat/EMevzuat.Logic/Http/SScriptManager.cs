﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace EMevzuat.Logic.Http {
    public static class SScriptManager {

        #region Public Methods

        public static void RegisterStartupBrifing(String brifing) {
            var script = ProvideStartUpScript(String.Format(@"information(""{0}"");", brifing));
            RegisterStartupScript(script);
        }

        public static String ProvideStartUpScript(String scripture) {
            String script;
            script = String.Format(@"
$(document).ready(function(){{
    {0}
}});
", scripture);
            return script;
        }
        public static void RegisterStartupScript(string scripture) {
            var script = new HtmlGenericControl("script");
            script.ID = Guid.NewGuid().ToString("N");
            script.Attributes.Add("type", "text/javascript");
            script.InnerHtml = scripture;
            ScriptPlaceHolder.Controls.Add(script);
        }
        public static void LoadAssets(String fromRelativePath) {
            LoadScriptsFrom(fromRelativePath);
            LoadStylesFrom(fromRelativePath);
        }
        public static void LoadAssets(String[] sourceOrRelativeURLs) {
            foreach (var src in sourceOrRelativeURLs) {
                var source = Regex.IsMatch(src, @"^http[s]?:.*$") ? src : PathFromSrc(src);
                if (Regex.IsMatch(source, @"^.*\.css.*$")) {
                    LoadStyle(source);
                } else {
                    LoadScript(source);
                }
            }

        }
        #endregion

        #region Private Methods
        private static void LoadScriptsFrom(String relativePath, HttpContext httpContext = null) {
            String fullPath;
            fullPath = (httpContext ?? HttpContext.Current).Server.MapPath(relativePath);

            foreach (String file in Directory.EnumerateFiles(fullPath, "*.js")) {
                LoadScript(file, httpContext);
            }
        }
        private static void LoadStylesFrom(String relativePath, HttpContext httpContext = null) {
            String fullPath;
            fullPath = (httpContext ?? HttpContext.Current).Server.MapPath(relativePath);

            foreach (String file in Directory.EnumerateFiles(fullPath, "*.css")) {
                LoadStyle(file, httpContext);
            }
        }
        private static void LoadScript(String path, HttpContext httpContext = null) {
            var script = new HtmlGenericControl("script");
            script.Attributes.Add("type", "text/javascript");
            script.Attributes.Add("src", SrcFromPath(path));

            ScriptPlaceHolder.Controls.Add(script);
        }
        private static void LoadStyle(String path, HttpContext httpContext = null) {
            var style = new HtmlLink();
            style.Href = SrcFromPath(path);
            style.Attributes["rel"] = "stylesheet";
            style.Attributes["type"] = "text/css";
            style.ID = Guid.NewGuid().ToString("N");
            ScriptPlaceHolder.Controls.Add(style);
        }
        private static String SrcFromPath(String fullPath, HttpContext httpContext = null) {
            return fullPath.Replace((httpContext ?? HttpContext.Current).Server.MapPath("~"), @"\" + SHttpContext.VirtualDirectory)
                .Replace(@"\", "/");
        }
        private static String PathFromSrc(String src, HttpContext httpContext = null) {
            return @"\" + SHttpContext.VirtualDirectory + src.Replace("~", "");
        }

        #endregion

        #region private fields
        private static Control ScriptPlaceHolder {
            get {
                var plh = SHttpContext.CurrentHandler.Form.FindControl(SHttpContext.Identifiers["ScriptsPlaceHolder"]);
                if (null == plh) {
                    plh = new PlaceHolder() {
                        ID = SHttpContext.Identifiers["ScriptsPlaceHolder"]
                    };
                    SHttpContext.CurrentHandler.Form.Controls.Add(plh);
                }
                return plh;
            }
        }
        #endregion

    }
}
