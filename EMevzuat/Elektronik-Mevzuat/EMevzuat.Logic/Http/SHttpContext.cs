﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Routing;
using System.Web.UI;

namespace EMevzuat.Logic.Http {
    public static class SHttpContext {

        #region Ctor
        static SHttpContext() {
            var assets = "~/Assets";
            Paths = new Dictionary<string, string>() {
                { "Assets", assets},
                { "Local", assets +"/Local"},
                { "Templates", assets + "/Templates"},
                { "Contract", assets + "/Templates/Contract.html"},
                { "Loading", assets + "/Sections/Common/Layouts/images/loading.gif"},
                { "LoadingBase64", assets + "/Templates/ExportLoadingBase64.txt"},
                { "GhostScript64", assets + "/Libraries/ghostscript64/gswin64c.exe"},
                { "GhostScript", assets + "/Libraries/ghostscript/gswin32c.exe"},
                { "Fonts", assets + "/Fonts"},
            };
            Identifiers = new Dictionary<string, string>() {
                { "MailTemplate", "MailTemplate.format"},
                { "ScriptsPlaceHolder", "PreScriptsPlaceHolder"},
                { "RouteURLToken", "relative-url"},
                { "RouteNameToken", "route-name"},
                { "EntityModelAssembly", "EMevzuat.Model"},
                { "EntityModelNamespace", "EMevzuat.Model.Context"},
                { "EntityConnection", "ec"},
                { "ScienceLab", "sl"},
            };
            var sqlBuilder = new SqlConnectionStringBuilder();

            //sqlBuilder.DataSource = @"213.142.130.11";
            //sqlBuilder.UserID = "emevzuat-admin";
            //sqlBuilder.Password = "elegislation";
            //sqlBuilder.InitialCatalog = "emevzuatdb";
            //sqlBuilder.PersistSecurityInfo = false;
            //sqlBuilder.MultipleActiveResultSets = true;

            sqlBuilder.DataSource = @".";
            sqlBuilder.UserID = "emadmin";
            sqlBuilder.Password = "1q2w3e4r5t";
            sqlBuilder.InitialCatalog = "emevzuatdb";
            sqlBuilder.PersistSecurityInfo = false;
            sqlBuilder.MultipleActiveResultSets = true;

            var entityBuilder = new EntityConnectionStringBuilder();
            entityBuilder.Provider = "System.Data.SqlClient";
            entityBuilder.ProviderConnectionString = sqlBuilder.ToString();
            entityBuilder.Metadata = @"res://*/";
            ConnectionString = entityBuilder.ToString();
        }
        #endregion

        #region Properties
        public static Dictionary<String, String> Paths { get; private set; }
        public static Dictionary<String, String> Identifiers { get; private set; }
        public static string ConnectionString { get; private set; }
        public static DbConnection CurrentConnection {
            get {
                return new EntityConnection(ConnectionString);
            }
        }
        public static Object AuthUserID {
            get {
                return HttpContext.Current.Session[AuthUserIDQueryParameter];
            }
            set {
                HttpContext.Current.Session[AuthUserIDQueryParameter] = value;
            }
        }
        public static String AuthUserIDQueryParameter {
            get {
                return "uid";
            }
        }
        public static String DocumentIDQueryParameter {
            get {
                return "did";
            }
        }
        public static String IDQueryParameter { get { return "id"; } }
        public static string ImplodeQueryParameter {
            get {
                return "quit";
            }
        }
        public static string ActivationQueryParameter {
            get {
                return "activate";
            }
        }
        public static Page CurrentHandler {
            get {
                return null != HttpContext.Current ? (HttpContext.Current.CurrentHandler as Page) : null;
            }
        }
        public static HttpRequest CurrentRequest {
            get {
                return HttpContext.Current.Request;
            }
        }
        public static String VirtualDirectory {
            get {
                return "emevzuat";
            }
        }
        public static string CurrentRelativeURL {
            get {
                return SHttpContext.CurrentRequest.Url.LocalPath.Replace("/" + SHttpContext.VirtualDirectory, "~");
            }
        }

        public static Route CurrentRoute {
            get {
                return RouteTable.Routes.Select<Object, Route>(r => r as Route)
                    .SingleOrDefault((r) => r.DataTokens != null && r.DataTokens["relative-url"].ToString().Substring(1) == CurrentRelativeURL)
                    ?? HttpContext.Current.Request.RequestContext.RouteData.Route as Route;
            }
        }
        public static Guid CurrentViewToken {
            get {
                return Guid.Parse(CurrentHandler.Session["CurrentViewToken"].ToString());
            }
            set {
                CurrentHandler.Session["CurrentViewToken"] = value;
            }
        }
        #endregion

    }
}
