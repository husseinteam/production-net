﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;

namespace EMevzuat.Logic.Http {
    public class AsyncOperation : IAsyncResult {
        private bool _completed;
        private Object _state;
        private AsyncCallback _callback;
        private HttpContext _context;

        bool IAsyncResult.IsCompleted { get { return _completed; } }
        WaitHandle IAsyncResult.AsyncWaitHandle { get { return null; } }
        Object IAsyncResult.AsyncState { get { return _AsyncResult; } }
        bool IAsyncResult.CompletedSynchronously { get { return false; } }

        public AsyncOperation(AsyncCallback callback, HttpContext context, Object state) {
            _callback = callback;
            _context = context;
            _state = state;
            _completed = false;
        }

        public void StartAsyncWork(Func<HttpContext, Dictionary<String, Object>> contexter) {
            Contexter = contexter;
            ThreadPool.QueueUserWorkItem(new WaitCallback(StartAsyncTask), null);
        }

        private void StartAsyncTask(Object workItemState) {
            _AsyncResult = Contexter(_context);
            _completed = true;
            _callback(this);
        }

        private Func<HttpContext, Dictionary<String, Object>> Contexter { get; set; }
        public Dictionary<String, Object> _AsyncResult;
    }
}