﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace EMevzuat.Logic.Crypto {
    public static class SCryptoManager {

        #region Public Methods
        public static byte[] Encrypt(string plainText, byte[] key, byte[] iv) {
            ICryptoTransform encryptor = crypto.CreateEncryptor(key.ToArray(), iv.ToArray());
            using (MemoryStream msEncrypt = new MemoryStream()) {
                CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write);
                StreamWriter swEncrypt = new StreamWriter(csEncrypt);
                swEncrypt.Write(plainText);
                swEncrypt.Dispose();
                csEncrypt.Dispose();
                return msEncrypt.ToArray();
            }
        }

        public static string Decrypt(byte[] cipher, byte[] key, byte[] iv) {
            ICryptoTransform decryptor = crypto.CreateDecryptor(key.ToArray(), iv.ToArray());
            using (MemoryStream msDecrypt = new MemoryStream(cipher.ToArray())) {
                CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read);
                StreamReader srDecrypt = new StreamReader(csDecrypt);
                return srDecrypt.ReadToEnd();
            }
        }

        public static void GenerateSymmetricAlgorithms(out byte[] key, out byte[] iv) {
            using (RijndaelManaged crpt = new RijndaelManaged()) {
                crpt.GenerateKey();
                crpt.GenerateIV();
                key = crpt.Key;
                iv = crpt.IV;
            }
        }
        #endregion
        #region Fields
        private static RijndaelManaged crypto = new RijndaelManaged();
        #endregion
    }
}
