﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EMevzuat.Logic.Extensions {
    public static class SString {
        public static String Ellipse(this String text, Int32 limit) {
            var temp = text.Trim();
            return temp.Substring(0, Math.Min(temp.Length, limit) - 3) + "...";
        }
    }
}
