﻿using System;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;

namespace EMevzuat.Logic.Parse {
    public static class SPdfParser {

        #region Methods
        public static byte[] ContentToPdf(string content, String password, string ownerPassword) {
            MemoryStream outputStream;
            using (outputStream = new MemoryStream()) {
                using (iTextSharp.text.Document document = new iTextSharp.text.Document(PageSize.A4, 20, 20, 30, 60)) {

                    PdfWriter writer;
                    writer = PdfWriter.GetInstance(document, outputStream);
                    writer.SetEncryption(PdfWriter.STRENGTH128BITS, password, ownerPassword
                        , PdfWriter.AllowCopy | PdfWriter.AllowPrinting);

                    document.Open();
                    document.SetPageSize(new Rectangle(document.PageSize.Width, document.PageSize.Height - document.Bottom));

                    XMLWorkerHelper.GetInstance().ParseXHtml(writer, document, new StringReader(content));
                    writer.CloseStream = false;
                    document.Close();
                    return outputStream.ToArray();
                }
            }
        }
        #endregion

    }
}
