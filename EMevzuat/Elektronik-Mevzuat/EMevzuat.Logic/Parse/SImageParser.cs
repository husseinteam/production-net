﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Windows.Interop;
using System.Windows.Media.Imaging;
using EMevzuat.Logic.Http;

namespace EMevzuat.Logic.Parse {
    public static class SImageParser {

        #region Public
        public static IEnumerable<byte[]> GetGhostData(dynamic entity, Int32 width, String ownerPassword, HttpContext context = null) {
            FileInfo ghostPath, pdfPath;
            var gswinc = GetGhostProcess(entity.PdfData, entity.Timestamp.ToFileTime().ToString(), ownerPassword, context, out ghostPath, out pdfPath);

            gswinc.Start();
            gswinc.WaitForExit();
            byte[][] imageDatas = null;
            if (gswinc.ExitCode == 0) {
                using (var ghost = new FileStream(ghostPath.ToString(), FileMode.Open, FileAccess.Read)) {
                    using (var tiff = Image.FromStream(ghost)) {
                        var frameCount = tiff.GetFrameCount(FrameDimension.Page);
                        imageDatas = new byte[frameCount + 1][];
                        var sizePage = new Size(Math.Min(width, tiff.Width) - 10, Math.Min((Int32)Math.Floor(Math.Sqrt(2) * width), tiff.Height) - 10);
                        var sizeThumbnail = new Size(150, (Int32)Math.Floor(Math.Sqrt(2) * 150));
                        Action<Int32, Int32, Size> loop = (i, m, size) => {
                            tiff.SelectActiveFrame(FrameDimension.Page, i);
                            var img = new Bitmap(size.Width, size.Height);
                            using (Graphics gr = Graphics.FromImage(img)) {
                                gr.SmoothingMode = SmoothingMode.HighQuality;
                                gr.InterpolationMode = InterpolationMode.HighQualityBicubic;
                                gr.PixelOffsetMode = PixelOffsetMode.HighQuality;
                                gr.DrawImage(tiff, new Rectangle(0, 0, img.Size.Width, img.Size.Height));
                                gr.Save();
                                var ms = new MemoryStream();
                                img.Save(ms, ImageFormat.Png);
                                imageDatas[m] = ms.ToArray();
                                ms.Dispose();
                                ms = null;
                            }
                        };
                        for (int i = 0; i < frameCount; i++) {
                            loop(i, i + 1, sizePage);
                        }
                        loop(0, 0, sizeThumbnail);
                    }
                }
                pdfPath.Refresh();
                if (true == pdfPath.Exists) {
                    pdfPath.Delete();
                }
                ghostPath.Refresh();
                if (true == ghostPath.Exists) {
                    ghostPath.Delete();
                }
            }
            return imageDatas;
        }

        #endregion

        #region Private
        private static Process GetGhostProcess(byte[] pdfData, String token, String ownerPassword, HttpContext context, out FileInfo ghostPath, out FileInfo pdfPath) {
            var serverUtility = (context ?? HttpContext.Current).Server;
            var pdfName = token;
            var ghostName = token;
            var silo = serverUtility.MapPath(SHttpContext.Paths["Local"]);

            pdfPath = new FileInfo(Path.Combine(silo, pdfName + ".pdf"));
            ghostPath = new FileInfo(Path.Combine(silo, ghostName + ".tiff"));
            var fontsPath = serverUtility.MapPath(SHttpContext.Paths["Fonts"]);

            pdfPath.Refresh();
            if (false == pdfPath.Exists) {
                using (var pdf = new FileStream(pdfPath.ToString(), FileMode.Create, FileAccess.Write, FileShare.ReadWrite)) {
                    pdf.Write(pdfData, 0, pdfData.Length);
                    pdf.Flush();
                }
            }

            String[] info = {
                                serverUtility.MapPath(SHttpContext.Paths["GhostScript"]),
                                String.Format("-sDEVICE=tiffscaled -dBATCH -dNOPAUSE -sFONTPATH={3} -sOutputFile={0} -sPDFPassword={2} {1}"
                                    , ghostPath, pdfPath, ownerPassword, fontsPath) };
            var gswinc = new Process();
            gswinc.StartInfo.FileName = info[0];
            gswinc.StartInfo.Arguments = info[1];
            gswinc.StartInfo.CreateNoWindow = true;
            gswinc.StartInfo.ErrorDialog = false; ;
            gswinc.StartInfo.WorkingDirectory = info[0];
            return gswinc;
        }
        #endregion

    }
}

