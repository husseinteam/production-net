﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;

namespace EMevzuat.Logic.Parse {
    public static class SParser {

        #region Methods
        public static string TakeDescription<TType>(TType instance, Object value) {
            FieldInfo fields = typeof(TType).GetFields().Where<FieldInfo>(
                (fi) => fi.GetCustomAttributes(true).Where(
                    (cattr) => cattr is DescriptionAttribute && fi.GetValue(instance) == value)
                    .FirstOrDefault() != null).FirstOrDefault();
            if (fields == null)
                return String.Empty;
            return (fields.GetCustomAttributes(true)[0] as DescriptionAttribute).Description;
        }
        public static string TakeDescription<TEnum>(Object value) {
            var q = from TEnum en in Enum.GetValues(typeof(TEnum)).AsQueryable()
                    where ParseType<Int32>(en) == ParseType<Int32>(value)
                    select typeof(TEnum).GetFields().Where<FieldInfo>((fi) =>
                        fi.GetCustomAttributes(true).Where(
                    (ca) => ca is DescriptionAttribute && ParseType<Int32>(fi.GetValue(en)) == ParseType<Int32>(value))
                    .FirstOrDefault() != null)
                        .Select<FieldInfo, String>((fi) =>
                            fi.GetCustomAttributes(true).OfType<DescriptionAttribute>().First().Description).FirstOrDefault();

            return q.FirstOrDefault();
        }
        public static Int32 TakeCode<TEnum>(TEnum value) {
            return ParseType<Int32>(value);
        }
        public static TType ParseType<TType>(Object instance) {
            if (instance.GetType().GetInterface("IConvertible") == null || typeof(TType).Equals(typeof(ValueType))) {
                return instance != null ? (TType)instance : default(TType);
            } else {
                TypeConverter conv;
                conv = TypeDescriptor.GetConverter(typeof(TType));
                return (TType)(conv.CanConvertFrom(instance.GetType()) == true ? conv.ConvertFrom(instance) : instance);
            }
        }
        public static string EncodeIP(string ipAddress) {
            string quadrified = "";
            IEnumerable<byte> quadrants;
            quadrants = ipAddress.Split('.').Select<string, byte>((q) => Convert.ToByte(q));
            foreach (byte q in quadrants) {
                quadrified += (char)q;
            }
            return quadrified;
        }
        public static IPAddress DecodeIP(string quadrifiedIp) {
            byte[] quadrants;

            quadrants = new byte[4];
            for (int i = 0; i < quadrants.Length; i++) {
                quadrants[i] = (byte)quadrifiedIp[i];
            }
            return new IPAddress(quadrants);
        }
        public static void Copy<T>(T source, T destination)
            where T : class {
            foreach (PropertyInfo pi in typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance)) {
                Object set = null;
                try {
                    set = pi.GetValue(source, null);
                    if ((set is ValueType && set.Equals(Activator.CreateInstance(pi.PropertyType)))
                        || (true == String.IsNullOrEmpty(set.ToString())) 
                        || (set.ToString() == Guid.Empty.ToString())) {
                        continue;
                    } else {
                        pi.SetValue(destination, set, null);
                    }
                } catch (Exception) {
                    continue;
                }
            }
        }
        public static dynamic DynamicCopy(dynamic source, String entityModelAssembly, String entityModelNamespace, string typeIdentifier) {
            var destinationType = AssemblyBuilder.Load(entityModelAssembly)
                .GetType(entityModelNamespace + "." + typeIdentifier.Split(',')[0]);
            dynamic copy = Activator.CreateInstance(destinationType);
            Type sourceType = source.GetType();
            foreach (var sp in (source as IDictionary<String, Object>)) {
                var target = destinationType.GetProperties().SingleOrDefault(pi => pi.Name == sp.Key);
                if (null != target) {
                    if (false == sp.Value is ExpandoObject) {
                        target.SetValue(copy, sp.Value, null);
                    } else {
                        var derived = DynamicCopy(sp.Value, entityModelAssembly, entityModelNamespace, typeIdentifier.Split(',')[1]);
                        target.SetValue(copy, derived, null);

                    }
                }
            }
            return copy;
        }

        public static String EncodeTurkish(String turkish) {
            if (turkish.Contains('Ġ') || turkish.Contains('Ģ')) {
                return turkish.Replace("Ġ", "İ").Replace("Ģ", "Ş");
            }
            else {
                var t = new Char[] { 'Ç', 'ç', 'Ğ', 'ğ', 'ı', 'İ', 'Ö', 'ö', 'Ş', 'ş', 'Ü', 'ü' };
                var e = new Char[] { 'C', 'c', 'G', 'g', 'i', 'I', 'O', 'o', 'S', 's', 'U', 'u' };
                for (int i = 0; i < t.Length; i++) {
                    turkish = turkish.Replace(t[i], e[i]);
                }
                return turkish;
            }
        }
        public static String Pluralise(String name) {
            return name.EndsWith("y") ? name.Substring(0, name.Length - 1) + "ies" : name.EndsWith("s") || name.EndsWith("z") ? name + "es" : name + "s";
        }
        #endregion

    }
}
