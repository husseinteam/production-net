﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace EMevzuat.Logic.Parse {
    public static class SValidator {

        #region Public Methods

        public static Boolean ValidateMailAddress(String address) {
            return Regex.IsMatch(address, @"^[a-zA-Z][a-zA-Z0-9_\.\-]*@[a-zA-Z0-9_\-]+\.[\w+.]+$");
        }

        #endregion

    }
}
