﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Routing;

namespace EMevzuat.Controller.Routes {
    public class ContentRouteHandler : IRouteHandler {

        #region ctor
        public ContentRouteHandler(String contentVirtualPath, String contentType) {
            ContentVirtualPath = contentVirtualPath;
            ContentType = contentType;
        }
        #endregion

        public class HttpHandler : IHttpHandler {

            public bool IsReusable {
                get {
                    return false;
                }
            }

            public void ProcessRequest(HttpContext context) {
                context.Response.ContentType = ContentType;
                context.Response.WriteFile(context.Server.MapPath(VirtualPath));
            }

            public String VirtualPath { get; internal set; }
            public String ContentType { get; internal set; }
        }

        public IHttpHandler GetHttpHandler(System.Web.Routing.RequestContext requestContext) {
            return new HttpHandler() {
                VirtualPath = ContentVirtualPath,
                ContentType = ContentType
            };
        }

        public string ContentVirtualPath { get; private set; }
        public string ContentType { get; private set; }
    }
}
