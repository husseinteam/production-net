﻿
using EMevzuat.Controller.Extension.Documents;
using EMevzuat.Controller.Extension.Files;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Routing;

namespace EMevzuat.Controller.Routes {
    public class ImageFrameRouteHandler : IRouteHandler {

        #region ctor
        public ImageFrameRouteHandler() {
        }
        #endregion

        public class HttpHandler : IHttpHandler {

            public bool IsReusable {
                get {
                    return false;
                }
            }

            public void ProcessRequest(HttpContext context) {
                var fileID = Guid.Parse(context.Request.QueryString["m"]);
                var file = SFile.ThatFileFor(fileID);
                context.Response.ContentType = "image/" + file.Extension == "jpg" ? "jpeg" : file.Extension;
                context.Response.BinaryWrite(file.Data);

                context.Response.Flush();
            }

            public String VirtualPath { get; internal set; }
            public String ContentType { get; internal set; }
        }

        public IHttpHandler GetHttpHandler(System.Web.Routing.RequestContext requestContext) {
            return new HttpHandler();
        }
    }
}
