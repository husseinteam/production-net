﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EMevzuat.Controller.ModifyView {
    public interface IPagedList<T> {
        Int32 TotalCount { get; }
        Int32 PageCount { get; }
        Int32 Page { get; }
        Int32 PageSize { get; }
    }

    public class PagedList<T> : List<T>, IPagedList<T> {
        public Int32 TotalCount { get; private set; }
        public Int32 PageCount { get; private set; }
        public Int32 Page { get; private set; }
        public Int32 PageSize { get; private set; }

        public PagedList(IQueryable<T> source, Int32 page, Int32 pageSize) {

            TotalCount = source.Count();
            PageCount = GetPageCount(pageSize, TotalCount);
            Page = page < 1 ? 0 : page - 1;
            PageSize = pageSize;

            AddRange(source.Skip(Page * PageSize).Take(PageSize).ToList());

        }

        private Int32 GetPageCount(Int32 pageSize, Int32 totalCount) {
            if (pageSize == 0)
                return 0;

            var remainder = totalCount % pageSize;
            return (totalCount / pageSize) + (remainder == 0 ? 0 : 1);
        }

    }
    public static class PagedListExtensions {

        public static PagedList<T> ToPagedList<T>(this IQueryable<T> source, Int32 page, Int32 pageSize) {

            return new PagedList<T>(source, page, pageSize);

        }

    }
}