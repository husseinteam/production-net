﻿using EMevzuat.Logic.Http;
using EMevzuat.Logic.Parse;
using EMevzuat.Model.Repository;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using WebFormsMvp.Web;
using System.Linq.Expressions;
using EMevzuat.Controller.Extension.Static;

namespace EMevzuat.Controller.ModifyView {
    public abstract class MvpModifyControl : MvpUserControl<ModifyModel>, IModifyView {

        #region Overrides
        protected override void OnInit(EventArgs e) {
            this.Controls.Add(FormDataSource);
            this.Controls.Add(BoardDataSource);
            ModifyControls();
            base.OnInit(e);
        }

        private void ModifyControls() {
            DisplayBoard.DataSourceID = "pdsBoard";
            EntityForm.DataSourceID = "pdsForm";
            DisplayBoard.AllowPaging = DisplayBoard.AllowSorting = true;
            //DisplayBoard.PageIndexChanging += (s, args) => {
            //    (s as GridView).PageIndex = args.NewPageIndex;
            //    (s as GridView).DataBind();
            //};
        }
        protected override void OnPreRender(EventArgs e) {
            LoadBase();
            LoadControl();
            base.OnPreRender(e);
        }
        #endregion

        #region Private Methods
        private void LoadBase() {
            Object primaryKey;
            if (true == LocateKeyGenerator(out primaryKey)) {
                Model.Entity = UnitOfWork.Locate(primaryKey);
                EntityForm.ChangeMode(FormViewMode.Edit);
                EntityForm.DataBind();
            } else {
                EntityForm.ChangeMode(FormViewMode.Insert);
            }
        }

        #endregion

        #region Inherited Members
        private PageDataSource _FormDataSource;
        private PageDataSource _BoardDataSource;
        protected PageDataSource BoardDataSource {
            get {
                if (_BoardDataSource == null) {
                    _BoardDataSource = new PageDataSource() {
                        ID = "pdsBoard",
                        SelectMethod="GetDisplay",
                        DeleteMethod = "DisjointEntity",
                        EnablePaging = true,
                        SortParameterName = "sortExpression",
                        StartRowIndexParameterName = "startRowIndex",
                        MaximumRowsParameterName = "maximumRows"
                    };
                }
                return _BoardDataSource;
            }
        }
        protected PageDataSource FormDataSource {
            get {
                if (_FormDataSource == null) {
                    _FormDataSource = new PageDataSource() {
                        ID = "pdsForm",
                        SelectMethod = "GetEntity",
                        UpdateMethod = "ModifyEntity",
                        InsertMethod = "ArticulateNew",
                        EnablePaging = false
                    };
                }
                return _FormDataSource;
            }
        }
        protected virtual Boolean WillDisjoint(Object ID, out String message) {
            message = "";
            return true;
        }
        #endregion

        #region Abstract Members
        public abstract void SetMessage();
        protected abstract void LoadControl();
        protected abstract dynamic TakeEntity(Object ID = default(Object));
        protected abstract FormView EntityForm { get; }
        protected abstract GridView DisplayBoard { get; }
        public abstract bool LocateKeyGenerator(out object locateKey);
        public abstract String GenerateEntityIdentifier();
        #endregion

        #region IModifyView Members

        public event ArticulateItemEventHandler Articulate;

        public event ArticulateItemEventHandler Modify;

        public event SelectItemEventHandler Disjoint;

        public event EventHandler Display;

        public void ArticulateNew() {
            if (Articulate != null) {
                Articulate(this, new ArticulateItemEventArgs() {
                    Entity = SParser.DynamicCopy(TakeEntity(), SHttpContext.Identifiers["EntityModelAssembly"], SHttpContext.Identifiers["EntityModelNamespace"], GenerateEntityIdentifier())
                });
            }
        }

        public void DisjointEntity(Object ID) {
            String message;
            if (Disjoint != null) {
                if (WillDisjoint(ID, out message) == true) {
                    Disjoint(this, new SelectItemEventArgs() { EntityID = ID});
                } else {
                    Model.Message = message;
                    Model.Done = false;
                    SetMessage();
                }
            }
        }

        public void ModifyEntity(Object ID) {
            if (Modify != null) {
                Modify(this, new ArticulateItemEventArgs() {
                    Entity = SParser.DynamicCopy(TakeEntity(ID), SHttpContext.Identifiers["EntityModelAssembly"], SHttpContext.Identifiers["EntityModelNamespace"], GenerateEntityIdentifier()),
                    EntityID = ID
                });
            }
        }

        public dynamic GetEntity() {
            return Model.Entity;
        }

        public dynamic GetDisplay(String sortExpression, int startRowIndex, int maximumRows) {

            if (Display != null) {
                Display(this, EventArgs.Empty);
            }

            sortExpression = sortExpression.Trim();
            var list = new List<dynamic>(Model.Display);
            if (String.IsNullOrEmpty(sortExpression) == false) {
                list = list.Skip(startRowIndex).ToList();
                if (sortExpression.ToLowerInvariant().EndsWith("desc")) {
                    sortExpression = sortExpression.Replace("DESC", "").Replace("desc", "").Trim();
                    foreach (String sort in sortExpression.Split(',')) {
                        list = list
                            .OrderByDescending(sort)
                            .ToList();
                    }
                } else {
                    sortExpression = sortExpression.Replace("ASC", "").Replace("asc", "").Trim();
                    foreach (String sort in sortExpression.Split(',')) {
                        list = list
                            .OrderBy(sort)
                            .ToList();
                    }
                }
            }
            return Model.Display = list;
            //.AsQueryable().ToPagedList(startRowIndex / 10, 10); 
        }

        public dynamic UnitOfWork {
            get {
                var identifier = GenerateEntityIdentifier().Split(',')[0];
                return SRepository.Generate(identifier);
            }
        }

        #endregion

    }
}
