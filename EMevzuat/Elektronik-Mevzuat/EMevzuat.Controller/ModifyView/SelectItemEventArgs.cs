﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EMevzuat.Controller.ModifyView {
    public class SelectItemEventArgs : EventArgs{
        public Object EntityID { get; set; }
    }
    public delegate void SelectItemEventHandler(Object sender, SelectItemEventArgs e);
}
