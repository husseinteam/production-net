﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EMevzuat.Controller.Extension.Enums;
using EMevzuat.Model.Context;
using EMevzuat.Logic.Parse;
using EMevzuat.Model.Repository;
using EMevzuat.Controller.Extension.Documents;
using EMevzuat.Logic.Http;
using EMevzuat.Controller.Extension.User;
using EMevzuat.Controller.Extension.Files;

namespace EMevzuat.Controller.Extension.DocumentUser {
    public static class SDocumentCounter {

        #region Public Methods

        public static Boolean PerformDocumentView(Guid userID, Guid documentID, out String message) {
            if (ValidateUserID(userID) == true && ValidateDocumentID(documentID) == true) {
                var unit = SRepository.Generic<MTSAVUser>();

                MTSAVLegislationsForUser entry;
                entry = new MTSAVLegislationsForUser() {
                    ID = Guid.NewGuid(),
                    Timestamp = DateTime.Now,
                    UserID = userID,
                    LegislationID = documentID
                };
                SRepository.Generic<MTSAVLegislationsForUser>().Articulate(entry).Commit();

                MTSAVUser auth;
                auth = unit.Locate(userID);

                if (null != auth.MTSAVAdministrators.FirstOrDefault()) {
                    message = "Dokümanı Tam Yetkili Kullanıcı Olarak Görüntülüyorsunuz";
                    return true;
                } else {
                    if (auth.MTSAVPayments.Count() == 0) {
                        message = "Ödemeniz Gerçekleşmeden Belge Görüntüleyemezsiniz";
                    } else {
                        var viewCount = CountViewedDocumentsOf(userID);
                        if (viewCount > auth.MTSAVPackage.MaxDocCount == true) {
                            message = String.Format("Belge Görüntüleme Limitinizi #{0} Aştınız. Lütfen Ödemenizi Yenileyiniz", auth.MTSAVPackage.MaxDocCount);
                        } else {
                            if (auth.MTSAVPayments.All((p) => p.Status == SParser.TakeCode<EStatus>(EStatus.Approved)) == false) {
                                message = String.Format("Sn. {0}, Onaylı Ödemeniz Bulunmamaktadır.", auth.FullName);
                                return false;
                            } else {
                                var amount = auth.MTSAVPayments.OrderByDescending(p => p.Timestamp)
                                    .First(p => p.Status == SParser.TakeCode<EStatus>(EStatus.Approved)).Amount;
                                if (amount < auth.MTSAVPackage.MembershipFee) {
                                    message = String.Format("Son Ödemenizin Bakiyesi<{0:C}> Yetersizdir. Geçerli Ödeme, Üyelik Paketiniz Gereği {1:C} olmalıdır."
                                        , amount, auth.MTSAVPackage.MembershipFee);
                                    return false;
                                } else {
                                    message = String.Format("Son Ödemenizden Şu Ana Dek {0} Adet Belge Görüntülediniz. {1} Adet Daha Görüntüleyebilirsiniz."
                                   , viewCount, auth.MTSAVPackage.MaxDocCount - viewCount);
                                    return true;
                                }
                            }
                        }
                    }
                }
            } else {
                message = "Yanlış Parametre";
            }
            return false;
        }
        public static Int32 CountViewedDocumentsOf(Object userID) {
            Guid uid;
            if (Guid.TryParse(userID.ToString(), out uid) == true) {
                var repo = SRepository.Generic<MTSAVLegislationsForUser>();
                return repo.Locate(lu => lu.UserID == uid).Count();
            } else {
                return -1;
            }
        }

        public static DateTime LastViewDate(Object userID) {
            Guid uid;
            if (Guid.TryParse(userID.ToString(), out uid) == true) {
                var repo = SRepository.Generic<MTSAVLegislationsForUser>();
                var registry = repo.Locate(lu => lu.UserID == uid).FirstOrDefault();
                return registry != null ? registry.Timestamp : default(DateTime);
            } else {
                return default(DateTime);
            }
        }

        public static String CountExtraDocumentsOf(Object userID) {
            var repo = SRepository.Generic<MTSAVUser>();
            var user = repo.Locate(userID);
            var count = -1;
            if (user != null) {
                count = user.MTSAVPackage.MaxDocCount - CountViewedDocumentsOf(userID);
            }
            if (count < 0) {
                return "Sınırsız Adet";
            } else {
                return count.ToString() + " Adet";
            }
        }

        #endregion

        #region Private Methods

        private static bool ValidateDocumentID(Guid documentID) {
            return SRepository.Generic<MTSAVLegislation>().Locate(documentID) != null;
        }

        private static bool ValidateUserID(Guid userID) {
            return SRepository.Generic<MTSAVUser>().Locate(userID) != null;
        }
        #endregion

    }
}
