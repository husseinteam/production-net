﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EMevzuat.Model.Context;
using EMevzuat.Model.Repository;
using EMevzuat.Logic.Parse;
using System.Web;
using System.IO;
using EMevzuat.Logic.Extensions;
using System.Data.Entity.Core.Objects;
namespace EMevzuat.Controller.Extension.Files {
    public static class SFile {

        #region ctor
        static SFile() {
            TableIDParameter = new ObjectParameter("TableID", typeof(Int32));
            SRepository.Generic<MTSAVFile>().RunStoredProcedure("spThatTableID", TableIdentifier, TableIDParameter);
        }
        #endregion

        #region Public Static
        public static Guid UpsertFile(String name, byte[] data, String extension, Guid documentID, Guid fileID) {
            var pk = Guid.NewGuid();
            if (documentID == Guid.Empty) {
                SRepository.Generic<MTSAVFile>().Articulate(new MTSAVFile() {
                    ID = pk,
                    Name = name,
                    Data = data,
                    Extension = extension,
                    Index = -2,
                    ReferencedPK = Guid.Empty.ToString(),
                    TableID = Int32.Parse(TableIDParameter.Value.ToString())
                }).Commit();
            } else {
                pk = fileID;
                SRepository.Generic<MTSAVFile>().Modify(new MTSAVFile() {
                    ID = pk,
                    Name = name,
                    Data = data,
                    Extension = extension,
                    Index = -2,
                    ReferencedPK = documentID.ToString(),
                    TableID = Int32.Parse(TableIDParameter.Value.ToString())
                }, pk).Commit();
            }
            return pk;
        }
        public static dynamic CommitDocumentArticulation(Guid documentID, Guid fileID) {
            var entry = new MTSAVFile() {
                ID = fileID,
                ReferencedPK = documentID.ToString(),
            };
            var entity = SRepository.Generic<MTSAVFile>().Locate(fileID);
            SParser.Copy<MTSAVFile>(entity, entry);
            if (true == SRepository.Generic<MTSAVFile>().Modify(entry, fileID).Commit()) {
                return entry;
            } else {
                return null;
            }
        }
        public static Boolean ArticulateDocumentPages(IEnumerable<byte[]> pages, dynamic file, Guid referencedPK) {
            var limit = pages.Count();
            var committed = true;
            for (int i = 0; i < limit; i++) {
                var page = pages.ElementAt(i);
                committed = SRepository.Generic<MTSAVFile>().Articulate(new MTSAVFile() {
                    ID = Guid.NewGuid(),
                    Name = file.Name + (i - 1).ToString(),
                    Data = page,
                    Extension = file.Extension,
                    Index = i - 1,
                    ReferencedPK = referencedPK.ToString(),
                    TableID = Int32.Parse(TableIDParameter.Value.ToString())
                }).Commit() == false ? false : committed;
            }
            return committed;
        }
        public static Boolean PrepareForModificationOFDocument(Guid documentID) {
            var deed = SRepository.Generic<MTSAVFile>();
            (from img in ThoseFilesFor(documentID) where img.Index >= -1 select img).ToList().ForEach(f => deed.Disjoint(f.ID));
            return deed.Commit();
        }
        public static IEnumerable<MTSAVFile> ThoseFilesFor(Guid documentID) {
            return SRepository.Generic<MTSAVFile>().Locate(f => f.ReferencedPK == documentID.ToString() && f.TableID == Int32.Parse(TableIDParameter.Value.ToString())
                , q => q.OrderBy(f => f.Index));
        }

        public static IEnumerable<MTSAVFile> ThosePageFilesFor(Guid documentID) {
            return (from img in ThoseFilesFor(documentID) where img.Index > -1 select img).ToList();
        }
        public static MTSAVFile ThatFileFor(Guid fileID) {
            return SRepository.Generic<MTSAVFile>().Locate(fileID);
        }
        public static MTSAVFile MiniPageFileFor(Guid documentID) {
            return (from img in ThoseFilesFor(documentID) where img.Index == -1 select img).FirstOrDefault();
        }
        public static dynamic ThatThumbnailFileFor(Guid documentID) {
            return (from img in ThoseFilesFor(documentID) where img.Index == -2 select img).FirstOrDefault();
        }
        public static void ThumbnailStreamer(Guid thumbnailID, HttpContext context = null) {
            var outputStream = (context ?? HttpContext.Current).Response.OutputStream;
            using (var ms = new MemoryStream(ThatFileFor(thumbnailID).Data)) {
                ms.CopyTo(outputStream);
            }
        }
        public static void GhostStreamer(Guid documentID, HttpContext context = null) {
            var outputStream = (context ?? HttpContext.Current).Response.OutputStream;
            using (var ms = new MemoryStream(ThatFileFor(documentID).Data)) {
                ms.CopyTo(outputStream);
            }
        }
        #endregion

        #region Properties
        private static String TableIdentifier = SParser.Pluralise(typeof(MTSAVLegislation).Name);
        public static ObjectParameter TableIDParameter { get; private set; }
        #endregion


    }
}
