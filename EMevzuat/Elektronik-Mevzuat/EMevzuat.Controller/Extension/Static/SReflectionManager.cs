﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace EMevzuat.Controller.Extension.Static {
	public static class SReflectionManager {

		public static Object ExecuteMethod(dynamic target, string methodName, Type[] genericTypes, params object[] parameters) {

			MethodInfo minfo = target.GetType().GetMethod(methodName);
			if (genericTypes != Type.EmptyTypes) {
				minfo = minfo.MakeGenericMethod(genericTypes);
			}
			var response = minfo.Invoke(target, parameters);
			return response;

		}

		public static TResponse ExecuteMethod<TResponse>(dynamic target, String methodName, Type[] genericTypes, params Object[] parameters)
			where TResponse : class {

			return ExecuteMethod(target, methodName, genericTypes, parameters) as TResponse;

		}

	}
}
