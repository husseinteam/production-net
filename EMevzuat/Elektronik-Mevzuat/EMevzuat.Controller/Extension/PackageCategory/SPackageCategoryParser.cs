﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EMevzuat.Model.Context;
using EMevzuat.Model.Repository;

namespace EMevzuat.Controller.Extension.PackageCategory {
    public static class SPackageCategoryParser {

        #region Public Methods

        public static ICollection<dynamic> GetPackages(Object categoryForPackageID) {
            var q = from cp in SRepository.Generic<MTSAVCategoriesForPackage>().Locate()
                    where cp.ID == Int32.Parse(categoryForPackageID.ToString())
                    select new {
                        Name = cp.MTSAVPackage.Name,
                        ID = cp.MTSAVPackage.ID
                    };
            return q.ToList<dynamic>();
        }

        public static ICollection<dynamic> GetCategories(Object categoryForPackageID) {
            var q = from cp in SRepository.Generic<MTSAVCategoriesForPackage>().Locate()
                    where cp.ID == Int32.Parse(categoryForPackageID.ToString())
                    select new {
                        Name = cp.MTSAVCategory.Name,
                        ID = cp.MTSAVCategory.ID
                    };
            return q.ToList<dynamic>();
        }

        public static String GetPackageName(Object packageID, Object categoryID) {
            if (ValidateParameters(packageID, categoryID) == false) {
                return String.Empty;
            } else {
                return GetCategoriesForPackage((Int32)packageID, (Int32)categoryID).MTSAVPackage.Name;
            }
        }

        public static String GetPackageMaxDocCount(Object packageID, Object categoryID) {
            if (ValidateParameters(packageID, categoryID) == false) {
                return String.Empty;
            } else {
                return GetCategoriesForPackage((Int32)packageID, (Int32)categoryID).MTSAVPackage.MaxDocCount.ToString();
            }
        }

        public static String GetCategoryName(Object packageID, Object categoryID) {
            if (ValidateParameters(packageID, categoryID) == false) {
                return String.Empty;
            } else {
                return GetCategoriesForPackage((Int32)packageID, (Int32)categoryID).MTSAVCategory.Name;
            }
        }

        public static String GetCategoryDescription(Object packageID, Object categoryID) {
            if (ValidateParameters(packageID, categoryID) == false) {
                return String.Empty;
            } else {
                return GetCategoriesForPackage((Int32)packageID, (Int32)categoryID).MTSAVCategory.Description;
            }
        }

        #endregion

        #region Private Methods
        private static MTSAVCategoriesForPackage GetCategoriesForPackage(Int32 packageID, Int32 categoryID) {
            return SRepository.Generic<MTSAVCategoriesForPackage>().Locate((um) => um.MTSAVPackage.ID == packageID && um.MTSAVCategory.ID == categoryID).FirstOrDefault();
        }
        private static Boolean ValidateParameters(Object packageID, Object categoryID) {
            return packageID != null && categoryID != null;
        }
        #endregion

    }
}
