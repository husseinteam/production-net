﻿using EMevzuat.Model.Context;
using EMevzuat.Logic.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Routing;
using EMevzuat.Model.Repository;
using EMevzuat.Logic.Parse;
using EMevzuat.Logic.Extensions;

namespace EMevzuat.Controller.Extension.HtmlPages {
    public static class SHttpTransmit {

        #region Static Ctor
        static SHttpTransmit() {
            Pager = SRepository.Generic<MTSAVHtmlPage>();
        }
        #endregion

        #region Methods
        public static void TransmitFile(byte[] data, string fileName, HttpContext httpContext = null) {

            var lck = new Object();
            lock (lck) {
                var response = (HttpContext.Current ?? httpContext).Response;
                var path = new FileInfo(String.Format("{0}/{1}", SHttpContext.CurrentHandler.Server.MapPath(SHttpContext.Paths["Local"])
                    , fileName).Ellipse(233) + ".pdf");
                if (true == path.Exists) {
                    path.Delete();
                }
                using (var file = new FileStream(path.ToString(), FileMode.Create, FileAccess.ReadWrite)) {
                    file.Write(data, 0, data.Length);
                }
                response.AddHeader("content-type", GetMimeType(path.ToString()));
                response.TransmitFile(path.ToString());
                response.Flush();
                path.Delete();
            }
        }
        public static void DisplayLoading(HttpContext context) {
            context.Response.ContentType = "text/html";
            context.Response.Write(String.Format(
                "<style type='text/css'>body {{ background: white url('data:image/gif;base64,{0}') norepeat center;  }}</style>",
            System.IO.File.ReadAllText(context.Server.MapPath(SHttpContext.Paths["LoadingBase64"]))));
            context.Response.Flush();
        }
        public static void Transfer(String relativeUrl, Boolean endResponse = false) {
            var pathAndQuery = relativeUrl.Split('?');
            relativeUrl = pathAndQuery[0];
            var redirect = pathAndQuery.Length > 1 ? String.Format(@"{0}?{1}", GetRoute(relativeUrl), pathAndQuery[1]) : GetRoute(relativeUrl);
            if (endResponse == true) {
                SHttpContext.CurrentHandler.Response.Redirect(redirect, true);
            } else {
                SHttpContext.CurrentHandler.Response.Redirect(redirect, false);
                SHttpContext.CurrentRequest.RequestContext.HttpContext.ApplicationInstance.CompleteRequest();
            }
        }
        public static void Pass(string relativeUrl, int seconds) {
            if (true) {
                SScriptManager.RegisterStartupScript(
                    String.Format(@"setTimeout('window.open(""{0}"");', {1});"
                    , GetRoute(relativeUrl), seconds * 1000));
            } 
        }
        public static String GetRoute(String relativeUrl) {
            var page = Pager.Locate((p) => p.RelativeUrl == relativeUrl).FirstOrDefault();
            if (null == page) {
                return relativeUrl;
            } else {
                return page.Route.Contains('{') ? page.Route.Substring(0, page.Route.IndexOf('{') - 1)
                    : page.Route;
            }
        }
        public static String RoutePathFromRoute(String url) {
            return String.Format("http://{2}/{0}/{1}", SHttpContext.VirtualDirectory, url
                , SHttpContext.CurrentRequest.Url.Host);
        }
        public static String RoutePathFromRelativeURL(String relativeURL) {
            var route = GetRoute(relativeURL);
            return RoutePathFromRoute(route);
        }
        #endregion

        #region Private Methods
        private static String GetMimeType(String fileName) {
            String ext;
            ext = fileName.Contains('.') ? fileName.Substring(fileName.LastIndexOf('.') + 1) : "";
            switch (ext) {
                case "pdf":
                    return "application/pdf";
                case "bmp":
                    return "application/bmp";
                default:
                    return "application/octet-stream";
            }
        }
        #endregion

        #region Private Static
        private static IScienceLab<MTSAVHtmlPage> Pager { get; set; }
        #endregion
    }
}
