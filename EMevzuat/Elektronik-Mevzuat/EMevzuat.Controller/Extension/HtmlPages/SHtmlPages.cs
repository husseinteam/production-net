﻿using EMevzuat.Model.Context;
using EMevzuat.Logic.Http;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Runtime.Caching;
using EMevzuat.Model.Repository;

namespace EMevzuat.Controller.Extension.HtmlPages {
    public static class SHtmlPages {

        #region Public Static Methods
        public static IEnumerable<dynamic> AsData() {
            var response = SRepository.Generic<MTSAVHtmlPage>().Locate();
            foreach (var r in response) {
                yield return new {
                    FullDescription = r.Description + " | " + r.Route,
                    RelativeUrl = r.RelativeUrl,
                    Description = r.Description,
                    Route = r.Route,
                    ID = r.ID,
                    MTSAVHtmls = r.MTSAVHtmls
                };
            }
        }
        public static IEnumerable<string> HtmlByCurrentHandler(String htmlSubstitute = "#HtmlSubstitute#") {
            var currentRoute = SHttpContext.CurrentRequest["URL"].Replace("/", "").Replace(SHttpContext.VirtualDirectory, "");
            MTSAVHtmlPage page;
            page = SRepository.Generic<MTSAVHtmlPage>()
                .Locate(((p) => p.Route == currentRoute)).FirstOrDefault();
            if (null != page && 0 != page.MTSAVHtmls.Count()) {
                return page.MTSAVHtmls.Select<MTSAVHtml, String>((h)
                    => htmlSubstitute.Replace("#HtmlSubstitute#", h.Html)).ToList();
            } else {
                return new String[] { "" };
            }
        }

        public static dynamic DataBy(Int32 ID) {
            var eo = new ExpandoObject();
            dynamic resp = eo;
            resp = SRepository.Generic<MTSAVHtmlPage>().Locate(ID);
            return resp;
        }
        public static String GenerateLinkOf(String relativeURL, String cssClass = null, String content = null, Boolean checkIfCurrentRoute = false) {
            MTSAVHtmlPage page = GetPageFromCacheBy(relativeURL);

            if (null != page) {
                cssClass = false == checkIfCurrentRoute ? cssClass : (page.Route == SHttpContext.CurrentRoute.Url ? cssClass : null);
                cssClass = null != cssClass ? String.Format(@" class=""{0}""", cssClass) : "";
                if (content != null && content.Contains("@@")) {
                    return String.Format(@"<a href=""{0}""{1}>{2}</a>", SHttpTransmit.GetRoute(page.RelativeUrl), cssClass
                        , content.Replace("@@", DataBy(page.ID).Description));
                } else {
                    content = null == content ? DataBy(page.ID).Description : content;
                    return String.Format(@"<a href=""{0}""{1}>{2}</a>", SHttpTransmit.GetRoute(page.RelativeUrl), cssClass, content);
                }
            } else {
                return String.Format(@"<a href=""#"">Hatalı Link!</a>");
            }
        }
        public static dynamic GetPageFromCacheBy(String relativeOrRouteURL) {
            var cache = MemoryCache.Default;
            var repr = relativeOrRouteURL.Replace("~", "").Replace("/", "");
            MTSAVHtmlPage page;
            if (null == cache["#" + repr]) {
                var policy = new CacheItemPolicy() { AbsoluteExpiration = DateTime.Now.AddMinutes(10) };
                var route = relativeOrRouteURL.Replace("~/", "");
                page = SRepository.Generic<MTSAVHtmlPage>().Locate((p)
                    => p.RelativeUrl == relativeOrRouteURL || route == p.Route).SingleOrDefault();
                if (null != page) {
                    cache.AddOrGetExisting("@" + repr, relativeOrRouteURL, policy);
                    cache.AddOrGetExisting("#" + repr, page, policy);
                }
            } else {
                page = cache["#" + repr] as MTSAVHtmlPage;
            }
            return page;
        }
        public static dynamic GetCurrentPage() {
            var relativeURL = SHttpContext.CurrentRelativeURL;
            return GetPageFromCacheBy(relativeURL);
        }
        public static IEnumerable<String> GetRouteIdentifiers(String route) {
            var identifiers = route.Split('/').Select<String, String>((r) => r.Replace("{", "").Replace("}", "").Replace("?", "")).ToArray();
            return identifiers;
        }
        #endregion

    }
}
