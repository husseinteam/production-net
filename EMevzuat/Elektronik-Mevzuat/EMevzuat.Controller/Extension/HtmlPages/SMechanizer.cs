﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EMevzuat.Controller.Extension.DocumentUser;
using EMevzuat.Logic.Crypto;
using EMevzuat.Logic.Http;
using EMevzuat.Logic.Parse;
using EMevzuat.Model.Repository;
using EMevzuat.Model.Context;

namespace EMevzuat.Controller.Extension.HtmlPages {
    public static class SMechanizer {

        #region Public Static Methods

        public static Dictionary<String, Object> ExportDocument(String documentID, HttpContext httpContext = null) {
            var data = new Dictionary<String, Object>();
            String message;
            var auth = SRepository.Generic<MTSAVUser>().Locate(SHttpContext.AuthUserID);
            var admin = SRepository.Generic<MTSAVAdministrator>().Locate().First().MTSAVUser;

            if (SDocumentCounter.PerformDocumentView(auth.ID
                , Guid.Parse(documentID), out message) == true || auth.MTSAVAdministrators.Count() > 0) {
                var worker = SRepository.Generic<MTSAVLegislation>();
                var legislation = worker.Locate(Guid.Parse(documentID));
                var password = SCryptoManager.Decrypt(auth.Cipher, auth.Key, auth.IV);
                var ownerPassword = SCryptoManager.Decrypt(admin.Cipher, admin.Key, admin.IV);
                data["pdfData"] = SPdfParser.ContentToPdf(legislation.Text, password, ownerPassword);
                data["pdfName"] = legislation.Title;
                data["done"] = true;
            } else {
                data["done"] = false;
            }
            data["message"] = message;
            SHttpTransmit.TransmitFile((byte[])data["pdfData"], data["pdfName"].ToString());
            SScriptManager.RegisterStartupBrifing(data["message"].ToString()); 
            return data;
        }
        public static void TransferCarousel(Guid documentID) {
            SHttpContext.CurrentHandler.Session[SHttpContext.DocumentIDQueryParameter] = documentID;
            SHttpTransmit.Transfer("~/Common/DocumentCarousel.aspx");
        }

        #endregion

    }
}