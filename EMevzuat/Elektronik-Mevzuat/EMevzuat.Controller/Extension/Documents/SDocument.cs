﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;
using EMevzuat.Model.Context;
using EMevzuat.Logic.Extensions;
using EMevzuat.Logic.Parse;
using EMevzuat.Model.Repository;
using EMevzuat.Controller.Extension.User;
using EMevzuat.Controller.Extension.Files;
using EMevzuat.Logic.Http;

namespace EMevzuat.Controller.Extension.Documents {
    public static class SDocument {

        #region Methods
        public static IEnumerable<dynamic> AsData() {
            return (from d in SRepository.Generic<MTSAVLegislation>().Locate(null, (q) => q.OrderByDescending((d) => d.Timestamp))
                    select new {
                        Title = d.Title,
                        CategoryID = d.CategoryID,
                        ID = d.ID,
                        Tags = d.Tags,
                        ThumbnailID = SFile.ThatThumbnailFileFor(d.ID).ID,
                        PageID = SFile.MiniPageFileFor(d.ID).ID,
                        Timestamp = d.Timestamp,
                        Ellipsis = d.Text.Ellipse(1024)
                    }).ToList();
        }
        public static IEnumerable<dynamic> AsData(Int32 categoryID) {
            return (from d in SRepository.Generic<MTSAVLegislation>().Locate((doc) =>
                doc.MTSAVCategory.ID == categoryID && SSessionManager.AuthorisedUser.MTSAVPackage.MTSAVCategoriesForPackages.Any(c
                    => c.CategoryID == categoryID && c.Allowed)
                , (q) => q.OrderByDescending((d) => d.Timestamp))
                    select new {
                        Title = d.Title,
                        CategoryID = d.CategoryID,
                        ID = d.ID,
                        ThumbnailID = SFile.ThatThumbnailFileFor(d.ID).ID,
                        PageID = SFile.MiniPageFileFor(d.ID).ID,
                        Timestamp = d.Timestamp,
                        Ellipsis = d.Text.Ellipse(1024)
                    }).ToList();
        }
        public static IEnumerable<dynamic> AsData(string[] words) {
            return (from d in SRepository.Generic<MTSAVLegislation>().Locate((doc) =>
                SSessionManager.AuthorisedUser.MTSAVLegislationsForUsers.Any(lu => lu.LegislationID == doc.ID)
                    && words.Any((w) => doc.Tags.Contains(w) || doc.Text.Contains(w) || doc.Title.Contains(w))
                , (q) => q.OrderByDescending((d) => d.Timestamp))
                    select new {
                        Title = d.Title,
                        CategoryID = d.CategoryID,
                        ID = d.ID,
                        ThumbnailID = SFile.ThatThumbnailFileFor(d.ID).ID,
                        PageID = SFile.MiniPageFileFor(d.ID).ID,
                        Timestamp = d.Timestamp,
                        Ellipsis = d.Text.Ellipse(1024)
                    }).ToList();
        }
        public static StringBuilder GetTextOfDocument(Guid documentID) {
            return new StringBuilder(SRepository.Generic<MTSAVLegislation>().Locate(documentID).Text);
        }
        public static byte[] GetDataOfDocument(object documentID) {
            return SRepository.Generic<MTSAVLegislation>().Locate(documentID).PdfData;
        }
        public static Boolean ValidateToken(Guid viewToken, Guid userID) {
            var userToken = SRepository.Generic<MTSAVUser>().Locate(userID).ViewToken;
            return Guid.Empty != viewToken && userToken.HasValue && viewToken == userToken.Value;
        }
        public static IEnumerable<dynamic> CarouselDataFor(Guid documentID) {
            var token = InValidateToken();
            var list = new List<dynamic>();
            var files = SFile.ThosePageFilesFor(documentID);
            var limit = files.Count();
            for (int i = 0; i < limit; i++) {
                var file = files.ElementAt(i);
                list.Add(new {
                    ViewToken = token,
                    ImageID = file.ID
                });
            }
            return list.ToArray();
        }
        public static IEnumerable<dynamic> GetAllDocumentsOf(Object userID) {
            var user = SRepository.Generic<MTSAVUser>().Locate(userID);
            var q = from cp in user.MTSAVPackage.MTSAVCategoriesForPackages
                    join l in AsData() on cp.CategoryID equals l.CategoryID
                    where cp.Allowed == true
                    select l;
            return q.ToList();
        }
        public static Guid? InValidateToken() {
            if (SHttpContext.AuthUserID != null) {
                var entry = new MTSAVUser() {
                    ViewToken = Guid.NewGuid(),
                };
                SRepository.Generic<MTSAVUser>().Modify(entry, SHttpContext.AuthUserID).Commit();
                return entry.ViewToken;
            } else {
                return null;
            }
        }
        public static Guid GetDemoDocumentID() {
            var doc = SRepository.Generic<MTSAVLegislation>().Locate(d => d.Tags == "demo").FirstOrDefault();
            return doc != null ? doc.ID : Guid.Empty;
        }
        #endregion

    }
}
