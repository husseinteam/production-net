﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EMevzuat.Model.Context;
using EMevzuat.Model.Repository;

namespace EMevzuat.Controller.Extension.Package {
    public static class SPackageParser {

        public static String GetPackageName(Int32 packageID) {
            return SRepository.Generic<MTSAVPackage>().Locate(packageID).Name;
        }

        public static IEnumerable<dynamic> AsData() {
            return SRepository.Generic<MTSAVPackage>().Locate();
        }

        public static Int32 CreateDemoPackage() {
            var pn = "Demo Paket";
            SRepository.Generic<MTSAVPackage>().Articulate(new MTSAVPackage() {
                ExpireDuration = 0,
                MaxDocCount = 0,
                MembershipFee = 0,
                Name = pn
            }).Commit();
            return SRepository.Generic<MTSAVPackage>().Locate(p => p.Name == pn).First().ID;
        }

        public static DateTime MembershipExpiresOn(Object userID, Object packageID) {
            if (packageID == null || userID == null) {
                return DateTime.MinValue;
            } else {
                Int32 pid; Guid uid;
                if (Int32.TryParse(packageID.ToString(), out pid)
                    && Guid.TryParse(userID.ToString(), out uid)) {
                    var user = SRepository.Generic<MTSAVUser>().Locate(uid);
                    var package = SRepository.Generic<MTSAVPackage>().Locate(pid);
                    return user.MembershipDate.AddDays(package.ExpireDuration);
                } else {
                    return DateTime.MinValue;
                }
            }
        }

    }
}
