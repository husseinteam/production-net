﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace EMevzuat.Controller.Extension.Enums {
    public enum EStatus : int {
        [Description("Askıda")]
        Pending = 0,
        [Description("Onaylandı")]
        Approved = 1,
        [Description("İptal Edildi")]
        Cancelled = 2,
    }
}
