﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace EMevzuat.Controller.Extension.Enums {
    public enum EUserAction : int {
        [Description("Oturum Açtı")]
        LoggedIn = 1,
        [Description("Üyelik Kaydı Yaptı")]
        Registered = 2,
        [Description("Üyeliğini Aktive Etti")]
        Activated = 3,
        [Description("Oturum Kapattı")]
        LoggedOut = 4,
    }
}
