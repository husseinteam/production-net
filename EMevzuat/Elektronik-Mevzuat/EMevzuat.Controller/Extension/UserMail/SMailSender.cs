﻿using System;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using EMevzuat.Controller.Extension.HtmlPages;
using EMevzuat.Model.Context;
using EMevzuat.Logic.Http;

namespace EMevzuat.Controller.Extension.UserMail {
    public static class SMailSender {

        #region Public Methods
        public static void RegisterMailTask(MailMessage message, MailAddressCollection to, dynamic userMail = null, Action<dynamic> CompletedCallback = null) {
            if (to == null) {
                to = message.To;
            }
            AsyncTaskDelegate async;
            async = null;
            SHttpContext.CurrentHandler.RegisterAsyncTask(new PageAsyncTask(new BeginEventHandler((s, e, cb, extra) => {
                async = new AsyncTaskDelegate(() => {
                    using (var smtp = new SmtpClient()) {
                        smtp.Port = 25;
                        smtp.Host = "213.142.130.53";
                        smtp.UseDefaultCredentials = false;
                        smtp.Credentials = new NetworkCredential("emevzuat@mtsav.org.tr", "elegislation");
                        //message.Headers.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", "1");	//basic authentication
                        //message.Headers.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", "emevzuat@mtsav.org.tr"); //set your username here
                        //message.Headers.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", "034dw4f2z4");	//set your password here
                        try {
                            smtp.Send(message);
                        } catch (Exception) {
                            AsyncTaskFailed = true;
                        }
                    }
                });
                return async.BeginInvoke(cb, extra);
            }), new EndEventHandler((ar) => {
                if (CompletedCallback != null) {
                    CompletedCallback((dynamic)ar.AsyncState);
                }
                async.EndInvoke(ar);
            }), null, userMail));
        }
        public static void ExecutePageTasks() {
            AsyncTaskFailed = false;
            try {
                SHttpContext.CurrentHandler.ExecuteRegisteredAsyncTasks();
            } catch (Exception) {
                AsyncTaskFailed = true;
            }
        }
        #endregion

        #region Internal
        internal static string GenerateActivationMailBody(MTSAVUser auth) {
            var url = SHttpTransmit.RoutePathFromRelativeURL("~/FrontGarden/Activation.aspx");
            return new StreamReader(Path.Combine(
                    SHttpContext.CurrentHandler.Server.MapPath(SHttpContext.Paths["Templates"])
                    , SHttpContext.Identifiers["MailTemplate"]))
                .ReadToEnd()
                .Replace("#ExplodeLink#", String.Format("{2}?{0}={1}", SHttpContext.ImplodeQueryParameter, auth.ID
                    , url))
                .Replace("#FullName#", auth.FullName)
                .Replace("#ActivationLink#", String.Format("{2}?{0}={1}", SHttpContext.ActivationQueryParameter, auth.ActivationCode
                    , url));
        }

        #endregion

        #region Properties

        public static Boolean AsyncTaskFailed = false;

        #endregion

        #region Private
        private delegate void AsyncTaskDelegate();
        #endregion

    }
}
