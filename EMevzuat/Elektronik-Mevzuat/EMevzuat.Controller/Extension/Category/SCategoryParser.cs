﻿using System;
using System.Web.UI;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EMevzuat.Model.Context;
using EMevzuat.Model.Repository;

namespace EMevzuat.Controller.Extension.Category {
    public static class SCategoryParser {

        #region Methods
        public static String GetPathRecursive(MTSAVCategory category) {
            if (category.MTSAVCategory1 == null)
                return category.Name.ToString();

            StringBuilder accumulator = new StringBuilder();
            accumulator.AppendFormat("{0}/{1}", GetPathRecursive(category.MTSAVCategory1), category.Name);
            return accumulator.ToString();
        }

        public static String GetPathRecursive(Int32 categoryID) {
            MTSAVCategory c;
            c = SRepository.Generic<MTSAVCategory>().Locate(categoryID);
            return GetPathRecursive(c);
        }

        public static IEnumerable<dynamic> GetCategoryLines() {
            var lines = (from c in SRepository.Generic<MTSAVCategory>().Locate().AsQueryable()
                         select new {
                             Line = GetPathRecursive(c),
                             LineID = c.ID
                         }).ToList();
            if (lines.Count == 0) {
                lines.Add(
                     new {
                         Line = "Üst Kategori Yok",
                         LineID = 0
                     });
            }
            return lines;
        }

        public static IEnumerable<dynamic> GetCategoryLinesAvailable(Guid userID) {
            return (from c in SRepository.Generic<MTSAVCategory>().Locate().AsQueryable()
                    join cp in SRepository.Generic<MTSAVCategoriesForPackage>().Locate() on c.ID equals cp.CategoryID
                    join p in SRepository.Generic<MTSAVPackage>().Locate() on cp.PackageID equals p.ID
                    join u in SRepository.Generic<MTSAVUser>().Locate() on p.ID equals u.MembershipPackageID
                    where u.ID == userID && cp.Allowed == true
                    select new {
                        Line = GetPathRecursive(c),
                        LineID = c.ID
                    }).ToList();
        }

        public static IEnumerable<dynamic> GetCategoryLinesAbove(Int32 categoryID) {
            var unit = SRepository.Generic<MTSAVCategory>();
            var lines = (from c in unit.Locate().AsQueryable()
                         where IsCategoryisUnderTheID(c, categoryID) == false && categoryID != c.ID
                         select new {
                             Line = GetPathRecursive(c),
                             LineID = c.ID
                         }).ToList();
            if (lines.Count == 0) {
                lines.Add(
                     new {
                         Line = "Üst Kategori Yok",
                         LineID = 0
                     });
            }
            return lines;
        }
        public static String GetParentName(Int32 categoryID) {
            var parent = SRepository.Generic<MTSAVCategory>().Locate(categoryID).MTSAVCategory1;
            if (parent != null) {
                return parent.Name;
            } else {
                return "Kök Kategori";
            }
        }
        public static bool CategoryHasParent(int categoryID) {
            return SRepository.Generic<MTSAVCategory>().Locate(categoryID).ParentID.HasValue;
        }
        #endregion

        #region private
        private static bool IsCategoryisUnderTheID(MTSAVCategory category, Int32 categoryID) {
            var up = category.MTSAVCategory1;
            while (null != up) {
                if (up.ID == categoryID) {
                    return true;
                }
                up = up.MTSAVCategory1;
            }
            return false;
        }
        #endregion

    }
}
