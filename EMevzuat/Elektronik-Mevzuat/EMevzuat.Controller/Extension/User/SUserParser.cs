﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EMevzuat.Model.Context;
using EMevzuat.Logic.Crypto;
using EMevzuat.Model.Repository;
using EMevzuat.Logic.Http;

namespace EMevzuat.Controller.Extension.User {
    public static class SUserParser {

        #region Public Methods
        public static IEnumerable<dynamic> AsData() {
            return SRepository.Generic<MTSAVUser>().Locate();
        }
        public static string GetUserMailAddress(object userID) {
            return SRepository.Generic<MTSAVUser>().Locate(userID).MailAddress;
        }
        public static String GetPdfOwnerPassword() {
            var admin = SRepository.Generic<MTSAVAdministrator>().Locate(ad => ad.IsDeveloper == true).FirstOrDefault();
            return null != admin ? SCryptoManager.Decrypt(admin.MTSAVUser.Cipher, admin.MTSAVUser.Key, admin.MTSAVUser.IV) : String.Empty;
        }

        public static dynamic GetAdministrator() {
            var id = Guid.Parse(SHttpContext.AuthUserID.ToString());
            return SRepository.Generic<MTSAVAdministrator>().Locate(ad => ad.IsDeveloper == true && ad.UserID == id).FirstOrDefault();
        }

        public static String GetUserFullName(Object userID) {
            if (userID == null) {
                return String.Empty;
            } else {
                return SRepository.Generic<MTSAVUser>().Locate(userID).FullName;
            }
        }
        public static String GetUserCurrentUserFullName() {
            return GetUserFullName(SHttpContext.AuthUserID);
        }
        public static Boolean Implode(Guid userID, out string message) {
            IScienceLab<MTSAVUser> unit;
            MTSAVUser auth;
            unit = SRepository.Generic<MTSAVUser>();
            auth = unit.Locate(userID);

            if (auth == null) {
                message = "Geçersiz Parametre";
            } else {
                if (unit.Disjoint(auth).Commit() == false) {
                    message = "Kullanıcı Kaldırılırken Hata Oluştu";
                } else {
                    message = "Bütün Kayıtlarınız Temizlendi. Sisteme Oturum Açabilmeniz İçin Yeniden Kayıt Olmanız Gerekmektedir";
                    return true;
                }
            }
            return false;
        }

        public static String GetUserPassword(Object userID) {
            if (userID == null) {
                return String.Empty;
            } else {
                var auth = SRepository.Generic<MTSAVUser>().Locate(userID);
                if (auth == null) {
                    return String.Empty;
                } else {
                    return SCryptoManager.Decrypt(auth.Cipher, auth.Key, auth.IV);
                }

            }
        }

        public static bool IsAdministrator(object userID) {
            if (null == userID) {
                return false;
            } else {
                var id = Guid.Parse(userID.ToString());
                return null != SRepository.Generic<MTSAVAdministrator>().Locate(ad
                    => ad.IsDeveloper == true && ad.UserID == id).FirstOrDefault();
            }
        }

        #endregion

    }
}
