﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EMevzuat.Controller.Extension.HtmlPages;
using EMevzuat.Controller.Extension.User;
using EMevzuat.Logic.Http;

namespace EMevzuat.View {
    public partial class Site : System.Web.UI.MasterPage {

        #region Overrides
        protected override void OnInit(EventArgs e) {
            LoadResources();
            InitTitle();
            base.OnInit(e);
        }
        #endregion

        #region Private
        protected void InitTitle() {
            var page = SHtmlPages.GetCurrentPage();
            if (Page.IsPostBack == false && null != page) {
                Page.Title = "E-Mevzuat | " + page.Description;
            }   
        }

        private static void LoadResources() {
            SScriptManager.LoadAssets("~/Assets/Sections/Common/Layouts/Framework");
            SScriptManager.LoadAssets("~/Assets/Sections/Common/Layouts");
        }
        #endregion

    }
}