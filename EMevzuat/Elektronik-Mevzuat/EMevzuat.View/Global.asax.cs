﻿using System;
using EMevzuat.Logic.Http;
using System.Web.Routing;
using EMevzuat.Controller.Routes;


namespace EMevzuat.View {
    public class Global : System.Web.HttpApplication {

        protected void Application_Start(object sender, EventArgs e) {
            SRouter.RegisterRoutes(RouteTable.Routes);
        }

        protected void Session_Start(object sender, EventArgs e) {
            //#warning auth user!
            //SHttpContext.AuthUserID = Guid.Parse("99979319-2227-462a-9496-4027cb8f0df6"); //admin
        }

        protected void Application_BeginRequest(object sender, EventArgs e) {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e) {

        }

        protected void Application_Error(object sender, EventArgs e) {

        }

        protected void Session_End(object sender, EventArgs e) {

        }

        protected void Application_End(object sender, EventArgs e) {

        }
    }
}