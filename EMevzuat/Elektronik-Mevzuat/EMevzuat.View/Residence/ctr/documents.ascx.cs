﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EMevzuat.BLL.Extension.DocumentUser;
using EMevzuat.DAL.EntityModel;
using EMevzuat.DAL.UnitOfWork;
using EMevzuat.LL.Http;

namespace EMevzuat.POC.Board.ctr {
    public partial class documents : System.Web.UI.UserControl {
  
        #region Overrides
        protected override void OnLoad(EventArgs e) {
            LoadControl();
            base.OnLoad(e);
        }

        #endregion

        #region Private Methods
        private void LoadControl() {
            DataBindDocuments();
        }

        private void DataBindDocuments() {
            Int32 categoryID;;
            if (Page.Request.QueryString["cid"] != null && Int32.TryParse(Page.Request.QueryString["cid"], out categoryID) == true) {
                rptDocuments.DataSource = SUnitOfWorkFactory<MTSAVLegislation>.Generate()
                    .Select((doc) => doc.MTSAVCategory.ID == categoryID, (q) => q.OrderByDescending((d) => d.Timestamp));
                rptDocuments.DataBind();
            } else if (Page.Request.QueryString["q"] != null) {
                String[] words;
                words = Page.Request.QueryString["q"].Split(new String[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                rptDocuments.DataSource = SUnitOfWorkFactory<MTSAVLegislation>.Generate()
                    .Select((l) => words.Any((w) => l.Tags.Contains(w)
                    || l.Text.Contains(w) || l.Title.Contains(w) ));
                rptDocuments.DataBind();
            } else {
                rptDocuments.DataSource = SDocumentCounter.GetAllDocumentsOf((Page.Master as Board).AuthorizedUser);
                rptDocuments.DataBind();
            }
        }
        #endregion
  
    }
}