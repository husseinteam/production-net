﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="categories.ascx.cs" Inherits="EMevzuat.POC.Board.ctr.categories" %>

<asp:Repeater runat="server" ID="rptCategories" DataSourceID="odsCategories">
    <HeaderTemplate>
        <h3>Kategorileriniz</h3>
        <ul>
    </HeaderTemplate>
    <ItemTemplate>
        <li>
            <asp:LinkButton Text='<%# Eval("Line") %>' PostBackUrl='<%#DataBinder.Eval(Container.DataItem, "LineID", "~/Board/Default.aspx?cid={0}") %>' runat="server"/>
        </li>
    </ItemTemplate>
    <FooterTemplate>
        </ul>
    </FooterTemplate>
</asp:Repeater>

<asp:ObjectDataSource runat="server" ID="odsCategories" TypeName="EMevzuat.BLL.Extension.Category.SCategoryParser" SelectMethod="GetCategoryLinesAvailable">
    <SelectParameters>
        <asp:SessionParameter Name="userID" SessionField="uid" DbType="Guid" />
    </SelectParameters>
</asp:ObjectDataSource>
