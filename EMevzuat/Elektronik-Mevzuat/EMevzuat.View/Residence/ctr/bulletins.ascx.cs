﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EMevzuat.DAL.EntityModel;
using EMevzuat.DAL.UnitOfWork;

namespace EMevzuat.POC.Board.ctr {
    public partial class bulletins : System.Web.UI.UserControl {

        #region Overrides
        protected override void OnPreRender(EventArgs e) {
            if (Page.IsPostBack == false) {
                LoadControl();
            }
            base.OnPreRender(e);
        }

        #endregion

        #region Private Methods
        private void LoadControl() {
            DataBindBulletins();
        }

        private void DataBindBulletins() {
            Int32 packageID;
            packageID = (Page.Master as Board).AuthorizedUser.MembershipPackageID;
            rptBulletins.DataSource = SUnitOfWorkFactory<MTSAVBulletin>.Generate()
                .Select((bl) => bl.PackageID == packageID && bl.Announced == true, (q) => q.OrderByDescending((blt) => blt.PublishDate));
            rptBulletins.DataBind();
        }
        #endregion

    }
}