﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="menu.ascx.cs" Inherits="EMevzuat.POC.Board.ctr.menu" %>

<%@ Import Namespace="EMevzuat.BLL.Extension.HtmlPages" %>

<script type="text/javascript">
    $(document).ready(function () {
        $('#menu li').each(function () {
            $(this).prop('id', '');
        });
        $($('a[href*="<%= Request.Url.ToString().Split(new String[] {"/"}, StringSplitOptions.RemoveEmptyEntries).Last() %>"]')
            .closest('li')[0]).prop('id', 'current');
    });
</script>
<ul id="menu">
    <li id="current">
        <asp:LinkButton runat="server" PostBackUrl="<%# SHttpTransmit.GetRouteUrl(13) %>" OnClientClick="$(this).parent().addClass('current')">
            Anasayfa</asp:LinkButton></li>
    <li>
        <asp:LinkButton runat="server" PostBackUrl="<%# SHttpTransmit.GetRouteUrl(12) %>" OnClientClick="$(this).parent().addClass('current')">
            İletişim</asp:LinkButton></li>
    <li>
        <asp:LinkButton runat="server" PostBackUrl="<%# SHttpTransmit.GetRouteUrl(11) %>" OnClientClick="$(this).parent().addClass('current')">
            Hakkımızda</asp:LinkButton></li>
</ul>
<ul>
    <li>
        <asp:LinkButton Text="Şifre Sıfırla" runat="server" ID="lnbReactivate" OnClick="lnbReactivate_Click" /></li>
    <li>
        <asp:LinkButton Text="Çıkış" runat="server" ID="lnbLogout" OnClick="lnbLogout_Click" /></li>
</ul>

