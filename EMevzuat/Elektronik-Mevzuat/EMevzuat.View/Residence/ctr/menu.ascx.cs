﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EMevzuat.BLL.Extension.User;
using EMevzuat.DAL.EntityModel;
using EMevzuat.LL.Http;
using EMevzuat.BLL.Extension.HtmlPages;

namespace EMevzuat.POC.Board.ctr {
    public partial class menu : System.Web.UI.UserControl {

        #region Event Handlers

        protected void lnbLogout_Click(object sender, EventArgs e) {
            Logout();
            SHttpTransmit.Transfer("~/Lobby/Login.aspx");
        }

        protected void lnbReactivate_Click(object sender, EventArgs e) {
            Logout();
            SHttpTransmit.Transfer("~/Lobby/Activation.aspx");
        }

        #endregion

        #region Private Methods

        private void Logout() {
            MTSAVUser auth;
            auth = (Page.Master as Board).AuthorizedUser;

            if (SSessionManager.Logout(auth.ID) == false) {
                SScriptManager.RegisterStartupMessage("Çıkış Kaydınız Alınamadı. Ancak Oturumunuz Yine de Sonlanacak");
                SHttpContext.CurrentHandler.Session.Abandon();
            }
        }

        #endregion

    }
}