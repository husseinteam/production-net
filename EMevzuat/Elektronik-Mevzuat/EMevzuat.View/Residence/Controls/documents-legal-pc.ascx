﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="documents.ascx.cs" Inherits="EMevzuat.View.Residence.Controls.documents"%>
<%@ Import Namespace="EMevzuat.Controller.Extension.Category" %>
<%@ Import Namespace="EMevzuat.Logic.Extensions " %>

<asp:Repeater runat="server" ID="rptDocuments">
    <ItemTemplate>
        <div class="post">

            <div class="right">

                <h5>
                    <asp:LinkButton Text='<%# Eval("Title") %>' runat="server" /></h5>

                <p class="post-info">
                    Belge Kategorisi:
                    <asp:HyperLink Text='<%# SCategoryParser.GetPathRecursive(Int32.Parse(Eval("CategoryID").ToString())) %>' runat="server" NavigateUrl='<%#DataBinder.Eval(Container.DataItem, "CategoryID", Request["URL"]  + "?cid={0}") %>' />
                </p>

                <p class="image-section">
                    <img style="width:100px; height: 100px;" src='http://<%=Request.Url.Host %>/<%=EMevzuat.Logic.Http.SHttpContext.VirtualDirectory %>/Modules/ImageFrame.ashx?fileid=<%# Eval("ImageFileID") %>' />
                </p>
                <blockquote class="ellipsis">
                    <img style="width:150px; height: 200px;"  src='http://<%=Request.Url.Host %>/<%=EMevzuat.Logic.Http.SHttpContext.VirtualDirectory %>/Modules/DocumentFrame.ashx?d=<%# Eval("ID") %>' />
                    <section style="display: inline; float: right;"><asp:ImageButton runat="server" ID="btnContinued" CommandName="Continued" CommandArgument='<%# Eval("ID") %>' ImageUrl="~/Assets/Sections/Residence/Layouts/images/continued.png" AlternateText="Devamını Okuyun.." OnClick="btnContinued_Click" /></section>
                </blockquote>

            </div>
            <div class="left">

                <p class="dateinfo">
                    <%# DataBinder.Eval(Container.DataItem, "Timestamp", "{0:MMM}") %>
                    <span><%# DataBinder.Eval(Container.DataItem, "Timestamp", "{0:dd}") %></span>
                </p>
                <div class="post-meta">
                    <h4>Detay</h4>
                    <ul>
                        <li class="user"><a href="#">Yönetici</a></li>
                        <li class="time"><a href="#"><%# DataBinder.Eval(Container.DataItem, "Timestamp", "{0:HH:mm}") %></a></li>
                        <li class="comment"><a href="#">0 Yorum</a></li>
                        <li class="permalink"><a href="#">Kalıcı Link</a></li>
                    </ul>
                </div>

            </div>

        </div>

    </ItemTemplate>
</asp:Repeater>
