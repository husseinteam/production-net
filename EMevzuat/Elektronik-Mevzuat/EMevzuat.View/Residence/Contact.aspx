﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Residence/Residence.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="EMevzuat.View.Residence.Contact" %>


<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">
    <!-- content -->
    <div id="content">
        <div id="main">

            <div id="MainForm" class="post">

                <h3>İletişim</h3>
                <p class="no-border"><strong>Bize Mesaj Yazın</strong></p>

                <p>
                    <label for="subject">Konu</label><br />
                    <input type="text" name="name" placeholder="Konu Giriniz.." tabindex="1" runat="server" id="txtSubject" class="text-field" />
                </p>

                <p>
                    <label for="message">Mesajınız</label><br />
                    <textarea name="message" placeholder="Kısaca Mesajınızı Giriniz.." rows="10" cols="20" tabindex="4" runat="server" id="txtMessage" class="message text-field"></textarea>
                </p>

                <p class="no-border">
                    <asp:Button Text="Gönderin" runat="server" TabIndex="5" ID="btnSendMail" OnClick="btnSendMail_Click" CssClass="button" />
                    <input class="button reset" type="reset" value="Reset" tabindex="6" onclick="$('div.post').find('p').not('.no-border').find(':input').each(function (i, el) { $(this).val('') });" />
                </p>
            </div>
        </div>
    </div>
</asp:Content>
