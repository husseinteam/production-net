﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EMevzuat.Controller.Extension.UserMail;


using EMevzuat.Logic.Http;
using EMevzuat.Controller.Extension.User;

namespace EMevzuat.View.Residence {
    public partial class Contact : System.Web.UI.Page {

        #region Event Handlers
        protected void btnSendMail_Click(object sender, EventArgs e) {
            MailAddress from;
            from = new MailAddress("emevzuat@mtsav.org.tr");
            MailMessage mailMessage;
            mailMessage = new MailMessage(from, new MailAddress("emevzuat@mtsav.org.tr")) {
                IsBodyHtml = true,
                Subject = String.Format("{0} E-Posta Adresi'nden <{1}> Konulu Mesaj Var"
                        , SUserParser.GetUserMailAddress(SHttpContext.AuthUserID), txtSubject.Value),
                Body = txtMessage.Value
            };
            SMailSender.RegisterMailTask(mailMessage, null);
            SMailSender.ExecutePageTasks();
            if (SMailSender.AsyncTaskFailed) {
                SScriptManager.RegisterStartupBrifing("Sunucu Hatası. İletiniz Gönderilemedi");
            } else {
                SScriptManager.RegisterStartupBrifing("İletiniz Alınmıştır. Teşekkür Ederiz");
            }
        }
        #endregion

    }
}