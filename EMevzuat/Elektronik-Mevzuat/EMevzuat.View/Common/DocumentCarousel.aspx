﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Residence/Residence.Master" AutoEventWireup="true" CodeBehind="DocumentCarousel.aspx.cs" Inherits="EMevzuat.View.Common.DocumentCarousel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" runat="server">
    <div id="main">
        <div id="carousel">
            <asp:Repeater runat="server" ID="rptCarousel">
                <ItemTemplate>
                    <img src='/<%=EMevzuat.Logic.Http.SHttpContext.VirtualDirectory %>/belge?m=<%# Eval("ImageID") %>&t=<%# Eval("ViewToken") %>&u=<%= EMevzuat.Logic.Http.SHttpContext.AuthUserID.ToString() %>' />
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>
</asp:Content>
