﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EMevzuat.Controller.Extension.HtmlPages;

namespace EMevzuat.View.Common {
    public partial class GenericError : System.Web.UI.Page {
        #region Overrides
        protected override void OnInit(EventArgs e) {
            base.OnInit(e);
            SHttpTransmit.Pass("~/FrontGarden/Home.aspx", 4);
        }
        #endregion
    }
}