﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EMevzuat.Controller.Extension.Documents;
using EMevzuat.Controller.Extension.DocumentUser;
using EMevzuat.Controller.Extension.HtmlPages;
using EMevzuat.Logic.Http;

namespace EMevzuat.View.Common {
    public partial class DocumentCarousel : System.Web.UI.Page {

        #region Page Methods
        protected override void OnLoad(EventArgs e) {
            Guid documentID;
            if (null != SHttpContext.CurrentHandler.Session[SHttpContext.DocumentIDQueryParameter]) {
                if (true == Guid.TryParse(SHttpContext.CurrentHandler.Session[SHttpContext.DocumentIDQueryParameter].ToString(), out documentID)) {
                    var message = String.Empty;
                    if (true == SDocumentCounter.PerformDocumentView(Guid.Parse(SHttpContext.AuthUserID.ToString()), documentID, out message)) {
                        rptCarousel.DataSource = SDocument.CarouselDataFor(documentID);
                        rptCarousel.DataBind();
                    }
                    SScriptManager.RegisterStartupBrifing(message);
                }
            } else {
                SScriptManager.RegisterStartupBrifing("Lütfen İzleme Linkini Kullanınız!");
                SHttpTransmit.Pass(Request.UrlReferrer.LocalPath, 5);
            }
            
            base.OnLoad(e);
        }
        #endregion

    }
}