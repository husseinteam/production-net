﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Windows.Media.Imaging;
using EMevzuat.Controller.Extension.Documents;
using EMevzuat.Controller.Extension.DocumentUser;
using EMevzuat.Controller.Extension.HtmlPages;
using EMevzuat.Controller.Extension.User;
using EMevzuat.Logic.Http;
using EMevzuat.Logic.Parse;

namespace EMevzuat.View.Modules {
    /// <summary>
    /// Summary description for ImageHandler
    /// </summary>
    public class DocumentFrame : IHttpHandler {

        public void ProcessRequest(HttpContext context) {
            var documentID = Guid.Parse(context.Request.QueryString["d"]);
            var data = SDocument.DataForDocument(documentID);

            Guid token;
            Int32 index;
            if (true == Guid.TryParse(context.Request.QueryString["t"], out token)) {
                Int32.TryParse(context.Request.QueryString["i"], out index);
                if (true == SDocumentCounter.ValidateToken(documentID, token)) {
                    context.Response.ContentType = "image/png";
                    SImageParser.GhostBuster(data, index, token, SUserParser.GetPdfOwnerPassword(), context.Response.OutputStream, context);
                } else {
                    context.Response.Write("<span style='color:Red; font-size: xx-large;'>Yetkisiz Erişim!</span>");
                }
            } else {
                context.Response.ContentType = "image/png";
                SImageParser.GhostBuster(data, 0, Guid.NewGuid(), SUserParser.GetPdfOwnerPassword(), context.Response.OutputStream, context);
            }
            context.Response.Flush();
        }


        #region IHttpHandler Members

        public bool IsReusable {
            get { return true; }
        }

        #endregion
    }
}