﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GenericError.aspx.cs" Inherits="EMevzuat.View.Common.GenericError" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        @import url(http://fonts.googleapis.com/css?family=Cuprum);

        body {
            background: url(../Assets/Sections/Common/Layouts/images/404.jpg) black no-repeat;
            text-align: center;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <span style="position: relative; top: 200px; font-family: 'Cuprum'; color: yellowgreen; font-size: large; font-weight: bold;">İstek Yapılan Sayfa Yönlendirici Tabloda Bulunamadı. Anasayfaya Yönlendiriliyorsunuz..
        </span>
    </form>
</body>
</html>
