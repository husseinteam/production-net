﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Residence/Residence.Master" AutoEventWireup="true" CodeBehind="DocumentCarousel.aspx.cs" Inherits="EMevzuat.View.Modules.DocumentCarousel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" runat="server">
    <div id="carousel">
        <asp:Repeater runat="server" ID="rptCarousel">
            <ItemTemplate>
                 <img src='http://<%=Request.Url.Host %>/<%=EMevzuat.Logic.Http.SHttpContext.VirtualDirectory %>/Modules/DocumentFrame.ashx?d=<%# Eval("ID") %>&t=<%# Eval("ViewToken") %>&i=<%# Eval("Index") %>' />
            </ItemTemplate>
        </asp:Repeater>
    </div>
</asp:Content>
