﻿<%@ Page Title="" Language="C#" MasterPageFile="~/FrontGarden/FrontGarden.Master" AutoEventWireup="true" CodeBehind="Activation.aspx.cs" Inherits="EMevzuat.View.FrontGarden.Activation" %>


<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">
    <div id="MainForm">

        <h2 runat="server" id="h2Title">Şifrenizi Aktive Edin</h2>

        <input runat="server" type="text" class="text-field" placeholder="E-Posta Adresi" id="txtMailAddress" visible="false" />
        <input runat="server" type="password" class="text-field" placeholder="Şifre" id="txtPassword" visible="false" />

        <asp:Button Text="Aktive Edin" runat="server" CssClass="button" ID="btnActivate" OnClick="btnActivate_Click" />
    </div>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="ScriptsPlaceHolderChild" ID="contenS">
</asp:Content>