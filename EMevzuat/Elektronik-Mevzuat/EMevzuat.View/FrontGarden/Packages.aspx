﻿<%@ Page Title="" Language="C#" MasterPageFile="~/FrontGarden/FrontGarden.Master" AutoEventWireup="true" CodeBehind="Packages.aspx.cs" Inherits="EMevzuat.View.FrontGarden.Packages" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">
    <div id="MainForm">
        <h2>Abonelik Tipleri</h2>
        <asp:Repeater runat="server" ID="rptPackages" DataSourceID="dsPackages">
            <ItemTemplate>
                <h3 class="packagetitle">
                    <%#Eval("Name") %>
                </h3>
                <blockquote class="site">
                    <div class="content">
                        Bu abonelik tipinde Yıllık Ücret <span class="money"><%#DataBinder.Eval(Container.DataItem, "MembershipFee", "{0:C}") %></span> kadardır.
                <br />
                        En çok <span class="times"><%#DataBinder.Eval(Container.DataItem, "MaxDocCount", "{0} Adet") %></span> Mevzuat Görüntüleyebilirsiniz.
                <br />
                        Abonelik Tipi Başlangıcından İtibaren <span class="date"><%#DataBinder.Eval(Container.DataItem, "ExpireDuration", "{0} Gün") %></span> Sürecektir.
                    </div>
                </blockquote>
            </ItemTemplate>
        </asp:Repeater>
    </div>

    <asp:ObjectDataSource runat="server" ID="dsPackages" SelectMethod="AsData" TypeName="EMevzuat.Controller.Extension.Package.SPackageParser" />
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="ScriptsPlaceHolderChild" ID="contenS">
</asp:Content>