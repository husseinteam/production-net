﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EMevzuat.Controller.Extension.User;
using EMevzuat.Logic.Http;
using EMevzuat.Controller.Extension.HtmlPages;

namespace EMevzuat.View.FrontGarden {
    public partial class Activation : System.Web.UI.Page {

        #region Overrides
        protected override void OnLoad(EventArgs e) {
            if (SHttpContext.CurrentRequest.QueryString[SHttpContext.ImplodeQueryParameter] == null) {
                if (SHttpContext.CurrentRequest.QueryString[SHttpContext.ActivationQueryParameter] == null) {
                    txtMailAddress.Visible = true;
                } else {
                    txtPassword.Visible = true;
                }
            } else if (SHttpContext.AuthUserID == null) {
                SScriptManager.RegisterStartupBrifing("Kullanıcı Girişi Yapıp Yeniden Deneyin");
                SHttpTransmit.Pass("~/FrontGarden/Login.aspx", 4);
            } else {
                btnActivate.OnClientClick =
                    "return confirm('Üyeliğiniz Geri Dönüşsüz Biçimde Tamamen Kaldırılacaktır!');";
                btnActivate.Style.Add(HtmlTextWriterStyle.MarginTop, "100px");
                btnActivate.Text = "Üyeliğimi Kaldır!";
                h2Title.InnerText = "Üyelik Silme";
            }
            base.OnLoad(e);
        }
        #endregion

        #region Event Handlers

        protected void btnActivate_Click(object sender, EventArgs e) {
            String message;
            if (SHttpContext.CurrentRequest.QueryString[SHttpContext.ImplodeQueryParameter] == null) {
                Guid code;
                if (SHttpContext.CurrentRequest.QueryString[SHttpContext.ActivationQueryParameter] == null) {
                    SSessionManager.RegisterActivation(txtMailAddress.Value, out message);
                    SScriptManager.RegisterStartupBrifing(message);
                } else {
                    if (Guid.TryParse(SHttpContext.CurrentRequest.QueryString[SHttpContext.ActivationQueryParameter], out code) == false) {
                        SScriptManager.RegisterStartupBrifing("Geçersiz Dize");
                    } else {
                        SSessionManager.Activate(txtPassword.Value, code, out message);
                        SScriptManager.RegisterStartupBrifing(message);
                    }
                }
            } else if (SHttpContext.AuthUserID != null) {
                Guid userID;
                if (Guid.TryParse(SHttpContext.CurrentRequest.QueryString[SHttpContext.ImplodeQueryParameter], out userID) == false) {
                    SScriptManager.RegisterStartupBrifing("Geçersiz Dize");
                } else {
                    SUserParser.Implode(userID, out message);
                    SScriptManager.RegisterStartupBrifing(message);
                    SHttpTransmit.Pass("~/FrontGarden/Register.aspx", 4);
                }
            } else {
                SScriptManager.RegisterStartupBrifing("Kullanıcı girişi Yapıp Yeniden deneyin.");
            }
        }

        #endregion

    }
}