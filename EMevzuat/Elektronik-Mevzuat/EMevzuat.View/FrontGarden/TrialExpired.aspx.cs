﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using EMevzuat.Controller.Extension.HtmlPages;

namespace EMevzuat.View.FrontGarden {
    public partial class TrialExpired : System.Web.UI.Page {
       
        #region override
        protected override void OnLoad(EventArgs e) {
            Thread.Sleep(1000);
            var meta = new HtmlMeta() {
                HttpEquiv = "refresh",
                Content = "2;url=" + SHttpTransmit.RoutePathFromRelativeURL("~/Default.aspx")
            };
            Header.Controls.Add(meta);
            base.OnLoad(e);
        }
        #endregion

    }
}