﻿$(document).ready(function () {
    LoadValidate();
    SwitchRadio();
});

function registerDropzoneReset(url) {
    if ($('div#zone').length === 1) {
        var dz = new Dropzone('div#zone', {
            url: url,
            success: function (file, resp) {
                var result = resp.match(/>([a-f0-9\-]+)</i)[1];
                result && $('#hdnOther').val(result);
                $('input.alt_btn').fadeIn('slow').css({ 'border-color': 'green' });
                alert((result ? 'Yüklemesi Başarılan: ' : 'Başarısız Yükleme: ') + file.name);
            },
            accept: function (file, done) {
                if (/^image\/.*/i.test(file.type)) {
                    $('input.alt_btn').hide();
                    done();
                } else {
                    done('Yalnızca Resim Yükleyebilirsiniz');
                }
            },
            error: function (file, errorMessage, xhr) {
                alert('Yükleme Hatası: ' + errorMessage);
                dz.removeFile(file);
            },
            cancelled: function (file) {
                alert('Yükleme Yarıda Kaldı: ' + file.name);
            }
        });
        var reset = $('input.reset').each(function (i, el) {
            $(this).click(function (e) {
                e.preventDefault();
                dz.removeAllFiles(true);
            });
        });
    }
};
function LoadValidate() {
    var submit = $('input.alt_btn').each(function (i, el) {
        $(this).click(function (e) {
            var validated = true;
            var checked = false;
            var article = $(this).closest($('article.module'));
            article.find($('input[type="text"]').not($('input.aspNetDisabled'))).add(article.find('select')).add(article.find('textarea').not(':hidden')).each(function (i, el) {
                if (!$(this).val()) {
                    $(this).css({ 'border-color': 'Red' });
                    e.preventDefault();
                    validated = false;
                } else {
                    $(this).css({ 'border-color': 'inherit' });
                }
            })
            article.find($('input[type="radio"]').not($('input.aspNetDisabled'))).each(function (i, el) {
                checked = this.checked ? true : checked;
            });
            article.find($('input[type="radio"]').not($('input.aspNetDisabled'))).each(function (i, el) {
                if (!checked) {
                    $(this).before($('<table class="__temp"><tr><td>*</td></tr></table>').css({ 'color': 'Red', 'position': 'absolute' }));
                    $('.__temp').animate({
                        'font-size': '2.5em',
                    }, 1000, function () {
                        $(this).remove();
                    });
                    $(this).css({ 'border-color': 'Red' });
                    e.preventDefault();
                    validated = false;
                }
            });

            if ($('div#zone').length === 1) {
                if ($(submit).hasClass('alt_btn update')) {
                    $('#zone').css({ 'border-color': 'green' });
                } else {
                    if ('' === $('#zone').html().trim()) {
                        $('#zone').css({ 'border-color': 'Red' });
                        e.preventDefault();
                        validated = false;
                    }
                }
            }
            if (true === validated) {
                DisplayLoading();
            }
        });
    });
}
function SwitchRadio() {
    var hdn = $('#hdnRadio');
    var radio1 = $('#rblAllowed_0');
    var radio2 = $('#rblAllowed_1');
    radio1.attr('checked', hdn.val() == '1')
    radio2.attr('checked', hdn.val() == '0')
    radio1.on('change', function (e) {
        hdn.val(this.checked === true ? '1' : '0');
    });
    radio2.on('change', function (e) {
        hdn.val(this.checked === false ? '1' : '0');
    });
}
