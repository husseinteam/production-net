﻿$(document).ready(function () {
    $("#carousel").waterwheelCarousel({
        horizon: 900,
        horizonOffset: -20,
        horizonOffsetMultiplier: .7,
        separation: 300,
        edgeFadeEnabled: true
    });
});