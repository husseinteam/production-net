﻿$(document).ready(function () {
    $('ul.vertmenu li a.item').each(function () {
        $(this).click(function (e) {
            $(this).closest($('ul')).find($('.selected')).removeClass('selected');
            $(this).parent().addClass('selected');
            e.preventDefault();
        });
    });
    $('ul.vertmenu li').first().addClass('selected');
    $("#carousel").waterwheelCarousel({
        horizon: 110,
        horizonOffset: -40,
        horizonOffsetMultiplier: .4,
        separation: 144,
        edgeFadeEnabled: true
    });
});