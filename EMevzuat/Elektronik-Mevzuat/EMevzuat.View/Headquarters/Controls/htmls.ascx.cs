﻿using EMevzuat.Controller.ModifyView;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebFormsMvp;
using System.Web.UI.HtmlControls;
using CKEditor.NET;
using System.Dynamic;

namespace EMevzuat.View.Headquarters.Controls {
    [PresenterBinding(typeof(ModifyPresenter))]
    public partial class htmls : _Root {

        protected override void LoadControl() {
            if (true == Model.Visible) {
                hdnSelect.Value = Model.Entity.PageID.ToString();
            }
        }

        protected override dynamic TakeEntity(Object ID = default(Object)) {
            dynamic entity;
            String text;
            Int32 pageID;
            text = (EntityForm.FindControl("EditorArea") as CKEditorControl).Text;
            pageID = Int32.Parse(hdnSelect.Value);
            var id = ID == default(Object) ? 0 : Int32.Parse(ID.ToString());
            entity = new ExpandoObject();
            if (0 == id) {
                entity. ID = ID;
                entity.Html = text;
                entity.PageID = pageID;
            } else {
                entity.PageID = pageID;
                entity.Html = text;
            }
            return entity;
        }

        protected override FormView EntityForm {
            get { return fvwHtml; }
        }

        protected override GridView DisplayBoard {
            get { return grdHtmls; }
        }

        public override string GenerateEntityIdentifier() {
            return "MTSAVHtml";
        }
    }
}