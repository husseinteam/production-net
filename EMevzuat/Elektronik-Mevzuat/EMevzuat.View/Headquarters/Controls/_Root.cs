﻿using EMevzuat.Controller.ModifyView;
using EMevzuat.Logic.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace EMevzuat.View.Headquarters.Controls {
    public abstract class _Root : MvpModifyControl {

        protected abstract override void LoadControl();

        protected abstract override dynamic TakeEntity(object ID = default(Object));

        protected abstract override FormView EntityForm { get; }

        protected abstract override GridView DisplayBoard { get; }

        public abstract override string GenerateEntityIdentifier();

        public override bool LocateKeyGenerator(out object locateKey) {
            var key = SHttpContext.CurrentRequest.QueryString[SHttpContext.IDQueryParameter];
            if (false == String.IsNullOrEmpty(key)) {
                Guid guid = default(Guid);
                Int32 int32 = 0;
                if (true == Guid.TryParse(key, out guid)) {
                    locateKey = guid;
                } else if (true == Int32.TryParse(key, out int32)) {
                    locateKey = int32;
                } else{
                    locateKey = 0;
                    return false;
                }
                return true;
            } else {
                locateKey = null;
                return false;
            }
        }

        public override void SetMessage() {
            (Page.Master as Headquarters).SetWarning(Model.Message, Model.Done);
        }

        protected HiddenField hdnSelect { get { return (Page.Master as Headquarters).HdnSelect; } }
        protected HiddenField hdnOther { get { return (Page.Master as Headquarters).HdnOther; } }
    }
}