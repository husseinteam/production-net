﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EMevzuat.Controller.ModifyView;
using EMevzuat.Controller.Extension.Category;

using EMevzuat.View.Headquarters;
using WebFormsMvp;
using WebFormsMvp.Web;
using System.Dynamic;

namespace EMevzuat.View.Headquarters.Controls {
    [PresenterBinding(typeof(ModifyPresenter))]
    public partial class categories : _Root {

        #region Overrides
        protected override void LoadControl() {
            if (true == Model.Visible && null != Model.Entity.ParentID) {
                hdnSelect.Value = Model.Entity.ParentID.ToString();
            }
        }

        protected override bool WillDisjoint(object ID, out string message) {
            if (false == SCategoryParser.CategoryHasParent(Int32.Parse(ID.ToString()))) {
                message = "Kök Kategori Silinemez; Yalnızca Üzerinde Değişiklik Yapılabilir.";
                return false;
            } else {
                message = "Siliniyor..";
                return true;
            }
        }

        protected override dynamic TakeEntity(Object ID = default(Object)) {
            dynamic entity;
            entity = new ExpandoObject();
            entity.Name = (EntityForm.FindControl("txtName") as TextBox).Text;
            entity.Description = (EntityForm.FindControl("txtDescription") as TextBox).Text;
            Int32 id = ID == default(Object) ? 0 : Int32.Parse(ID.ToString());
            if (id > 0) {
                entity.ID = ID;
            }
            if (String.IsNullOrEmpty(hdnSelect.Value) == false && hdnSelect.Value != "0") {
                entity.ParentID = Int32.Parse(hdnSelect.Value);                
            } else {
                entity.ParentID = null;
            }

            return entity;
        }
        #endregion

        protected override FormView EntityForm {
            get { return fvwCategory; }
        }

        protected override GridView DisplayBoard {
            get { return grdCategories; }
        }

        public override string GenerateEntityIdentifier() {
            return "MTSAVCategory";
        }
    }
}