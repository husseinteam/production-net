﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="packagecategories.ascx.cs" Inherits="EMevzuat.View.Headquarters.Controls.packagecategories" %>

<%@ Import Namespace="EMevzuat.Controller.Extension.PackageCategory" %>

<asp:HiddenField runat="server" ID="hdnRadio" ClientIDMode="Static" />

<article class="module width_full">
    <header>
        <h3 class="tabs_involved">Üyelik Paketi'ne Göre Kategoriler</h3>
        <ul class="tabs">
            <li><a href="#tab1">Tüm Liste</a></li>
            <li><a href="#tab2">Ekle/Düzenle</a></li>
        </ul>
    </header>
    <div class="tab_container">
        <div id="tab1" class="tab_content">
            <asp:GridView runat="server" ID="grdPackageCategories" AllowPaging="true" AllowSorting="true" CssClass="tablesorter" AutoGenerateColumns="false" DataKeyNames="ID" BorderWidth="0px">
                <Columns>
                    <asp:TemplateField HeaderText="Paket İsmi" SortExpression="PackageID">
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server"
                                Text='<%# SPackageCategoryParser.GetPackageName(Eval("PackageID"), Eval("CategoryID")) %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Paket Limiti">
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server"
                                Text='<%# SPackageCategoryParser.GetPackageMaxDocCount(Eval("PackageID"), Eval("CategoryID")) %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Kategori Adı" SortExpression="CategoryID">
                        <ItemTemplate>
                            <asp:Label ID="Label3" runat="server"
                                Text='<%# SPackageCategoryParser.GetCategoryName(Eval("PackageID"), Eval("CategoryID")) %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Kategori Açıklaması">
                        <ItemTemplate>
                            <asp:Label ID="Label4" runat="server"
                                Text='<%# SPackageCategoryParser.GetCategoryDescription(Eval("PackageID"), Eval("CategoryID")) %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Erişim" SortExpression="Allowed">
                        <ItemTemplate>
                            <asp:Label ID="Label5" runat="server"
                                Text='<%# (Boolean)Eval("Allowed") == true ? "Yetkilendirilmiş" : "Erişimi Yok" %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Komutlar">
                        <ItemTemplate>
                            <asp:HyperLink runat="server" NavigateUrl='<%# DataBinder.Eval(Container.DataItem, "ID", Request.ServerVariables["URL"] + "?id={0}") %>' ToolTip="Paket Yetkilendrimesini Düzenleyin"><img runat="server" src="~/Assets/Sections/Headquarters/Layouts/images/icn_eye.png" /></asp:HyperLink>
                            <asp:ImageButton runat="server" CommandName="Delete" ImageUrl="~/Assets/Sections/Headquarters/Layouts/images/icn_cancel.png" CausesValidation="False"
                                OnClientClick="return confirm('Paket Yetkilendirmesi Kaldırılacaktır');" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
        <div id="tab2" class="tab_content">
            <asp:FormView runat="server" ID="fvwPackageCategory" DataKeyNames="ID">
                <InsertItemTemplate>
                    <article class="module width_inner">
                        <header>
                            <h3>Belge Kategorisine Erişim Yetkisi Verin</h3>
                        </header>
                        <fieldset>
                            <label>Belge Kategorisi Seçin</label>
                            <asp:DropDownList runat="server" ID="ddlCategories" DataSourceID="dsCategories" DataTextField="Line" DataValueField="LineID" CssClass="select"></asp:DropDownList>
                        </fieldset>
                        <fieldset>
                            <label>Yetki Verilecek Paketi Seçin</label>
                            <asp:DropDownList runat="server" ID="ddlPackages" DataSourceID="dsPackages" DataTextField="Name" DataValueField="ID" CssClass="other"></asp:DropDownList>
                        </fieldset>
                        <fieldset>
                            <label>Erişim</label>
                            <asp:RadioButtonList ID="rblAllowed" runat="server" ClientIDMode="Static">
                                <asp:ListItem Text="Yetkilendirilmiş" />
                                <asp:ListItem Text="Erişimi Yok" />
                            </asp:RadioButtonList>
                        </fieldset>
                        <div class="clear"></div>
                        <footer>
                            <div class="submit_link">
                                <asp:Button Text="Atamayı Yapın" runat="server" CssClass="alt_btn" ID="btnInsertDocument" CommandName="Insert" />
                                <input type="submit" value="Sıfırlayın" class="reset">
                            </div>
                        </footer>
                    </article>
                </InsertItemTemplate>
                <EditItemTemplate>
                    <article class="module width_inner">
                        <header>
                            <h3>Erişim Düzenleyin</h3>
                        </header>
                        <fieldset>
                            <label>Belge Kategorisi Seçin</label>
                            <asp:DropDownList runat="server" ID="ddlCategories" DataSourceID="odsCategories" DataTextField="Name" DataValueField="ID" CssClass="select" Enabled="false"></asp:DropDownList>
                        </fieldset>
                        <fieldset>
                            <label>Yetki Verilecek Paketi Seçin</label>
                            <asp:DropDownList runat="server" ID="ddlPackages" DataSourceID="odsPackages" DataTextField="Name" DataValueField="ID" CssClass="other" Enabled="false"></asp:DropDownList>
                        </fieldset>
                        <fieldset>
                            <label>Erişim</label>
                            <asp:RadioButtonList ID="rblAllowed" runat="server" ClientIDMode="Static">
                                <asp:ListItem Text="Yetkilendirilmiş" />
                                <asp:ListItem Text="Erişimi Yok" />
                            </asp:RadioButtonList>
                        </fieldset>
                        <div class="clear"></div>
                        <footer>
                            <div class="submit_link">
                                <asp:Button Text="Güncelleyin" runat="server" CssClass="alt_btn" ID="btnUpdateDocument" CommandName="Update" />
                                <input type="submit" value="Sıfırlayın" class="reset">
                            </div>
                        </footer>
                    </article>
                </EditItemTemplate>
            </asp:FormView>
        </div>
    </div>
    <footer>
    </footer>
</article>
<!-- end of content manager article -->
<div class="clear"></div>
<asp:ObjectDataSource runat="server" ID="odsCategories" TypeName="EMevzuat.Controller.Extension.PackageCategory.SPackageCategoryParser" SelectMethod="GetCategories">
    <SelectParameters>
        <asp:QueryStringParameter QueryStringField="id" Name="categoryForPackageID" Type="Int32" />
    </SelectParameters>
</asp:ObjectDataSource>

<asp:ObjectDataSource runat="server" ID="odsPackages" TypeName="EMevzuat.Controller.Extension.PackageCategory.SPackageCategoryParser" SelectMethod="GetPackages">
    <SelectParameters>
        <asp:QueryStringParameter QueryStringField="id" Name="categoryForPackageID" Type="Int32" />
    </SelectParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource runat="server" ID="dsCategories" SelectMethod="GetCategoryLines" TypeName="EMevzuat.Controller.Extension.Category.SCategoryParser"></asp:ObjectDataSource>
<asp:ObjectDataSource runat="server" ID="dsPackages" SelectMethod="AsData" TypeName="EMevzuat.Controller.Extension.Package.SPackageParser"></asp:ObjectDataSource>

