﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EMevzuat.Controller.ModifyView;
using WebFormsMvp;

namespace EMevzuat.View.Headquarters.Controls {

    [PresenterBinding(typeof(ModifyPresenter))]
    public partial class legislationsforusers : _Root {

        protected override void LoadControl() {
            
        }

        protected override dynamic TakeEntity(object ID = default(Object)) {
            throw new NotImplementedException();
        }

        protected override FormView EntityForm {
            get { return fvwlegislationForUser; }
        }

        protected override GridView DisplayBoard {
            get { return grdLegislationsForUsers; }
        }

        public override string GenerateEntityIdentifier() {
            return "MTSAVUser";
        }
    }
}