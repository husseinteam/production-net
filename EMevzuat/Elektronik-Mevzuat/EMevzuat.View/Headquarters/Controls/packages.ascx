﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="packages.ascx.cs" Inherits="EMevzuat.View.Headquarters.Controls.packages" %>

<article class="module width_full">
    <header>
        <h3 class="tabs_involved">Paketler</h3>
        <ul class="tabs">
            <li><a href="#tab1">Paket Listesi</a></li>
            <li><a href="#tab2">Paket Ekle/Düzenle</a></li>
        </ul>
    </header>
    <div class="tab_container">
        <div id="tab1" class="tab_content">
            <asp:GridView runat="server" ID="grdPackages" AllowPaging="true" AllowSorting="true" CssClass="tablesorter" AutoGenerateColumns="false" DataKeyNames="ID" BorderWidth="0px">
                <Columns>
                    <asp:BoundField DataField="Name" HeaderText="Paket Adı" />
                    <asp:BoundField DataField="MembershipFee" DataFormatString="{0:C}" HeaderText="Paket Ücreti" />
                    <asp:BoundField DataField="ExpireDuration" DataFormatString="{0} Gün" HeaderText="Üyelik Süresi" />
                    <asp:BoundField DataField="MaxDocCount" DataFormatString="{0} Adet" HeaderText="Belge Limiti" />
                    <asp:TemplateField HeaderText="Komutlar" ItemStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:ImageButton runat="server" CommandName="Delete" ImageUrl="~/Assets/Sections/Headquarters/Layouts/images/icn_cancel.png" OnClientClick="return confirm('Paket Tamamen Silinecektir');" ToolTip="Paketi Silin"/>
                            <asp:HyperLink runat="server" NavigateUrl='<%# DataBinder.Eval(Container.DataItem, "ID", Request.ServerVariables["URL"] + "?id={0}") %>' ToolTip="Paketi Düzenleyin"><img runat="server" src="~/Assets/Sections/Headquarters/Layouts/images/icn_eye.png" /></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
        <div id="tab2" class="tab_content">
            <asp:FormView runat="server" ID="fvwPackage" DataKeyNames="ID">
                <EditItemTemplate>
                    <article class="module width_inner">
                        <header>
                            <h3>Paket Düzenleyin</h3>
                        </header>
                        <fieldset>
                            <label>Paket Adı</label>
                            <asp:TextBox runat="server" ID="txtName" Text='<%# Eval("Name") %>' />
                        </fieldset>
                        <fieldset>
                            <label>Paket Ücreti </label>
                            <asp:TextBox runat="server" ID="txtMembershipFee" Text='<%# Eval("MembershipFee") %>' />
                        </fieldset>
                        <fieldset>
                            <label>Üyelik Süresi (Gün) </label>
                            <asp:TextBox runat="server" ID="txtExpireDuration" Text='<%# Eval("ExpireDuration") %>' />
                        </fieldset>
                        <fieldset>
                            <label>Belge Limiti (Adet) </label>
                            <asp:TextBox runat="server" ID="txtMaxDocCount" Text='<%# Eval("MaxDocCount") %>' />
                        </fieldset>
                        <div class="clear"></div>
                        <footer>
                            <div class="submit_link">
                                <asp:Button Text="Güncelleyin" runat="server" CssClass="alt_btn" ID="btnUpdate" CommandName="Update" />
                                <input type="submit" value="Sıfırlayın" class="reset">
                            </div>
                        </footer>
                    </article>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <article class="module width_inner">
                        <header>
                            <h3>Yeni Paket Ekleyin</h3>
                        </header>
                        <fieldset>
                            <label>Paket Adı</label>
                            <asp:TextBox runat="server" ID="txtName" Text='<%# Eval("Name") %>' />
                        </fieldset>
                        <fieldset>
                            <label>Paket Ücreti </label>
                            <asp:TextBox runat="server" ID="txtMembershipFee" Text='<%# Eval("MembershipFee") %>' />
                        </fieldset>
                        <fieldset>
                            <label>Üyelik Süresi (Gün) </label>
                            <asp:TextBox runat="server" ID="txtExpireDuration" Text='<%# Eval("ExpireDuration") %>' />
                        </fieldset>
                        <fieldset>
                            <label>Belge Limiti (Adet) </label>
                            <asp:TextBox runat="server" ID="txtMaxDocCount" Text='<%# Eval("MaxDocCount") %>' />
                        </fieldset>
                        <div class="clear"></div>
                        <footer>
                            <div class="submit_link">
                                <asp:Button Text="Ekleyin" runat="server" CssClass="alt_btn" ID="btnInsert" CommandName="Insert" />
                                <input type="submit" value="Sıfırlayın" class="reset">
                            </div>
                        </footer>
                    </article>
                </InsertItemTemplate>
            </asp:FormView>
        </div>
    </div>
    <footer>
    </footer>
</article>
<div class="clear"></div>