﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CKEditor.NET;
using EMevzuat.Controller.Extension.UserMail;
using EMevzuat.Controller.ModifyView;
using EMevzuat.Logic.Http;
using WebFormsMvp;
using System.Dynamic;
using System.IO;

namespace EMevzuat.View.Headquarters.Controls {

    [PresenterBinding(typeof(ModifyPresenter))]
    public partial class exceptions : _Root {

        protected override void LoadControl() {

        }

        protected override dynamic TakeEntity(Object ID = default(Object)) {
            dynamic entity;
            entity = new ExpandoObject();
            entity.ID = ID != default(Object) ? Guid.Parse(ID.ToString()) : Guid.NewGuid();
            entity.Message = (EntityForm.FindControl("txtMessage") as TextBox).Text;
            entity.Detail = (EntityForm.FindControl("EditorArea") as CKEditorControl).Text;
            entity.IsCrucial = (EntityForm.FindControl("rblCrucial") as RadioButtonList).SelectedValue == "1";
            entity.UserID = Guid.Parse(SHttpContext.AuthUserID.ToString());

            MailAddress from;
            from = new MailAddress("emevzuat@mtsav.org.tr");
            MailMessage mailMessage;
            mailMessage = new MailMessage(from, new MailAddress("lampiclobe@outlook.com")) {
                IsBodyHtml = true,
                Subject = "lmpcaspnet.emevzuat.exception",
                Body = GetMailBody(entity)
            };

            SMailSender.RegisterMailTask(mailMessage, null);
            SMailSender.ExecutePageTasks();

            return entity;
        }

        private string GetMailBody(dynamic entity) {
            return String.Format("Hata Mesajı: <br/>{0}<br/>Açıklama:<br/>{1}",
                entity.Message, entity.Detail);
        }

        protected override FormView EntityForm {
            get { return fvwException; }
        }

        protected override GridView DisplayBoard {
            get { return grdExceptions; }
        }


        public override string GenerateEntityIdentifier() {
            return "MTSAVException";
        }
    }
}