﻿//------------------------------------------------------------------------------
// <otomatik üretildi>
//     Bu kod bir araç tarafından oluşturuldu.
//
//     Bu dosyada yapılacak değişiklikler yanlış davranışa neden olabilir ve
//     kod tekrar üretildi. 
// </otomatik üretildi>
//------------------------------------------------------------------------------

namespace EMevzuat.View.Headquarters.Controls {
    
    
    public partial class usermails {
        
        /// <summary>
        /// grdUsers denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.GridView grdUsers;
        
        /// <summary>
        /// fvwUserMail denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.FormView fvwUserMail;
        
        /// <summary>
        /// dsUsers denetimi.
        /// </summary>
        /// <remarks>
        /// Otomatik üretilmiş alan.
        /// Değiştirmek için, alan bildirimini tasarımcı dosyasından arka plan kod dosyasına taşıyın.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ObjectDataSource dsUsers;
    }
}
