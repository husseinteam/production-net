﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CKEditor.NET;
using EMevzuat.Controller.ModifyView;
using EMevzuat.View.Headquarters;
using WebFormsMvp;
using EMevzuat.Logic.Http;
using System.Dynamic;

namespace EMevzuat.View.Headquarters.Controls {
    [PresenterBinding(typeof(ModifyPresenter))]
    public partial class bulletins : _Root {

        protected override void LoadControl() {
            if (true == Model.Visible) {
                hdnSelect.Value = Model.Entity.PackageID.ToString();
            }
        }

        protected override dynamic TakeEntity(object ID = default(Object)) {
            dynamic entity;
            entity = new ExpandoObject();
            entity.Title = (fvwBulletin.FindControl("txtTitle") as TextBox).Text;
            entity.Timestamp = DateTime.Now;
            entity.Body = (fvwBulletin.FindControl("EditorArea") as CKEditorControl).Text;
            entity.Announced = (fvwBulletin.FindControl("rblAnnounced") as RadioButtonList).SelectedItem.Value == "1";
            entity.PackageID = Int32.Parse(hdnSelect.Value);

            entity.ID = ID != default(Object) ? Guid.Parse(ID.ToString()) : Guid.NewGuid();
            entity.PublishDate = entity.Announced == true ? new Nullable<DateTime>(DateTime.Now) : null;

            return entity;
        }

        protected override FormView EntityForm {
            get { return fvwBulletin; }
        }

        protected override GridView DisplayBoard {
            get { return grdBulletins; }
        }


        public override string GenerateEntityIdentifier() {
            return "MTSAVBulletin";
        }
    }
}