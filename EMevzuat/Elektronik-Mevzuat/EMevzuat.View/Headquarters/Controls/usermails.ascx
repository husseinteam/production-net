﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="usermails.ascx.cs" Inherits="EMevzuat.View.Headquarters.Controls.usermails" %>
<%@ Import Namespace="EMevzuat.Controller.Extension.UserMail" %>
<%@ Import Namespace="EMevzuat.Controller.Extension.User" %>

<article class="module width_full">
    <header>
        <h3 class="tabs_involved">E-Postalar</h3>
        <ul class="tabs">
            <li><a href="#tab1">E-Posta Listesi</a></li>
            <li><a href="#tab2">E-Posta Gönder</a></li>
        </ul>
    </header>
    <div class="tab_container">
        <div id="tab1" class="tab_content">
            <asp:GridView runat="server" ID="grdUsers" AllowPaging="true" AllowSorting="true" CssClass="tablesorter" AutoGenerateColumns="false" DataKeyNames="ID" BorderWidth="0px">
                <Columns>
                    <asp:TemplateField HeaderText="Ad Soyad" SortExpression="UserID">
                        <ItemTemplate>
                            <asp:Label runat="server"
                                Text='<%# SUserParser.GetUserFullName(Eval("SenderID")) %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="E-Posta" SortExpression="MailID">
                        <ItemTemplate>
                            <asp:Label runat="server"
                                Text='<%# SUserParser.GetUserMailAddress(Eval("SenderID")) %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Konu" ItemStyle-Width="300px">
                        <ItemTemplate>
                            <asp:Label runat="server"
                                Text='<%# Eval("Subject") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Gönderim Tarihi">
                        <ItemTemplate>
                            <asp:Label runat="server"
                                Text='<%# Eval("Timestamp") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Başarı Durumu">
                        <ItemTemplate>
                            <asp:Label runat="server"
                                Text='<%# (Boolean)Eval("Success") == true ? "Başarılı" : "Başarısız" %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Komutlar">
                        <ItemTemplate>
                            <asp:HyperLink runat="server" NavigateUrl='<%# DataBinder.Eval(Container.DataItem, "ID", Request.ServerVariables["URL"] + "?id={0}") %>' ToolTip="E-Posta'yı Yeniden Gönderin"><img runat="server" src="~/Assets/Sections/Headquarters/Layouts/images/icn_eye.png" />
                            </asp:HyperLink>
                            <asp:ImageButton runat="server" CommandName="Delete" ImageUrl="~/Assets/Sections/Headquarters/Layouts/images/icn_cancel.png" ToolTip="E-Posta'yı Silin"
                                OnClientClick="return confirm('E-Posta Kaydı Tamamen Silinecektir');" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
        <div id="tab2" class="tab_content">
            <asp:FormView runat="server" ID="fvwUserMail" DataKeyNames="ID">
                <InsertItemTemplate>
                    <article class="module width_inner">
                        <header>
                            <h3>E-Posta Gönderin</h3>
                        </header>
                        <fieldset>
                            <label>E-Posta Seçin</label>
                            <div style="margin-right: 3%; float: right;">
                                <span>
                                    <input runat="server" type="checkbox" id="cbSendToAll"
                                        name="cbSendToAll" class="omit" onchange="$($('.select').closest(':input')[0]).toggle('fade')" />
                                    Tüm Kullanıcılara Gönder</span>
                            </div>
                            <asp:DropDownList runat="server" ID="ddlSelect" DataSourceID="dsUsers" DataTextField="MailAddress" DataValueField="ID" CssClass="select"></asp:DropDownList>
                        </fieldset>
                        <fieldset>
                            <label>Konu</label>
                            <asp:TextBox runat="server" ID="txtMailSubject"
                                Text='<%# Eval("Subject") %>' />
                        </fieldset>
                        <fieldset>
                            <label>İçerik</label>
                            <editor:CKEditorControl BasePath="~/Assets/Libraries/ckeditor" runat="server" ID="EditorArea"
                                Text='<%# Eval("Body") %>' />
                        </fieldset>
                        <div class="clear"></div>
                        <footer>
                            <div class="submit_link">
                                <asp:Button Text="Gönderin" runat="server" CssClass="alt_btn" ID="btnInsertDocument" CommandName="Insert" />
                                <input type="submit" value="Sıfırlayın" class="reset">
                            </div>
                        </footer>
                    </article>
                </InsertItemTemplate>
                <EditItemTemplate>
                    <article class="module width_inner">
                        <header>
                            <h3>Yeniden E-Posta Gönderin</h3>
                        </header>
                        <fieldset>
                            <label>E-Posta</label>
                            <asp:DropDownList runat="server" ID="ddlSelect" DataSourceID="dsUsers" DataTextField="MailAddress" DataValueField="ID" CssClass="select" Enabled="false"></asp:DropDownList>
                        </fieldset>
                        <fieldset>
                            <label>Konu</label>
                            <asp:TextBox runat="server" ID="txtMailSubject"
                                Text='<%# Eval("Subject") %>' />
                        </fieldset>
                        <fieldset>
                            <label>Gönderim Tarihi</label>
                            <asp:TextBox runat="server" ID="txtMailDate"
                                Text='<%# DataBinder.Eval(Container.DataItem, "Timestamp", "{0:dd MMM yyyy-H:m}") %>' Enabled="false" />
                        </fieldset>
                        <fieldset>
                            <label>İçerik</label>
                            <editor:CKEditorControl BasePath="~/Assets/Libraries/ckeditor" runat="server" ID="EditorArea"
                                Text='<%# Eval("Body") %>' />
                        </fieldset>
                        <div class="clear"></div>
                        <footer>
                            <div class="submit_link">
                                <asp:Button Text="Yeniden Gönderin" runat="server" CssClass="alt_btn" ID="btnUpdateDocument" CommandName="Update" />
                                <input type="submit" value="Sıfırlayın" class="reset">
                            </div>
                        </footer>
                    </article>
                </EditItemTemplate>
            </asp:FormView>
        </div>
    </div>
    <footer>
    </footer>
</article>
<!-- end of content manager article -->
<div class="clear"></div>
<asp:ObjectDataSource runat="server" ID="dsUsers" SelectMethod="AsData" TypeName="EMevzuat.Controller.Extension.User.SUserParser" />

