﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="exceptions.ascx.cs" Inherits="EMevzuat.View.Headquarters.Controls.exceptions" %>
<%@ Import Namespace="EMevzuat.Controller.Extension.Exceptions" %>

<article class="module width_full">
    <header>
        <h3 class="tabs_involved">Hata Kayıtları</h3>
        <ul class="tabs">
            <li><a href="#tab1">Hata Listesi</a></li>
            <li><a href="#tab2">Hata Ekle/Düzenle</a></li>
        </ul>
    </header>
    <div class="tab_container">
        <div id="tab1" class="tab_content">
            <asp:GridView runat="server" ID="grdExceptions" AllowPaging="true" AllowSorting="true" CssClass="tablesorter" AutoGenerateColumns="false" DataKeyNames="ID" BorderWidth="0px">
                <Columns>
                    <asp:BoundField DataField="Message" HeaderText="Hata Mesajı"></asp:BoundField>
                    <asp:TemplateField HeaderText="Hata Tipi" SortExpression="IsCrucial">
                        <ItemTemplate>
                            <asp:Label Text='<%# (Boolean)Eval("IsCrucial") == true ? "Kritik Hata" : "Kritik Değil" %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Bildirimi Yapan Kullanıcı" SortExpression="UserID">
                        <ItemTemplate>
                            <asp:Label Text='<%# SExceptionParser.GetUserFullName(Eval("UserID")) %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Komutlar" ItemStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:ImageButton runat="server" CommandName="Delete" ImageUrl="~/Assets/Sections/Headquarters/Layouts/images/icn_cancel.png" OnClientClick="return confirm('Hata Kaydı Tamamen Silinecektir');" ToolTip="Hata Kaydını Silin"/>
                            <asp:HyperLink runat="server" NavigateUrl='<%# DataBinder.Eval(Container.DataItem, "ID", Request.ServerVariables["URL"] + "?id={0}") %>' ToolTip="Hata Kaydını Düzenleyin"><img runat="server" src="~/Assets/Sections/Headquarters/Layouts/images/icn_eye.png" /></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
        <div id="tab2" class="tab_content">
            <asp:FormView runat="server" ID="fvwException" DataKeyNames="ID">
                <EditItemTemplate>
                    <article class="module width_inner">
                        <header>
                            <h3>Hata Kaydı Düzenleyin</h3>
                        </header>
                        <fieldset>
                            <label>Hata Mesajı</label>
                            <asp:TextBox runat="server" ID="txtMessage" Text='<%# Eval("Message") %>' />
                        </fieldset>
                        <fieldset>
                            <label>Çözüm Talebiniz</label>
                            <br />
                            <editor:CKEditorControl BasePath="~/Assets/Libraries/ckeditor" ID="EditorArea" runat="server" Rows="12" Text='<%# Eval("Detail") %>'/>
                        </fieldset>
                        <fieldset style="width: 48%; float: left; margin-right: 3%;">
                            <label>Hata Tipi</label>
                            <asp:RadioButtonList runat="server" ID="rblCrucial" RepeatDirection="Vertical" TextAlign="Right">
                                <asp:ListItem Text="Kritik Hata" Value="1" />
                                <asp:ListItem Text="Kritik Değil" Value="0" Selected="True" />
                            </asp:RadioButtonList>
                        </fieldset>
                        <div class="clear"></div>
                        <footer>
                            <div class="submit_link">
                                <asp:Button Text="Hatayı Yeniden Bildirin" runat="server" CssClass="alt_btn" ID="btnUpdateDocument" CommandName="Update" />
                                <input type="submit" value="Sıfırlayın" class="reset">
                            </div>
                        </footer>
                    </article>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <article class="module width_inner">
                        <header>
                            <h3>Yeni Hata Kaydı Ekleyin</h3>
                        </header>
                        <fieldset>
                            <label>Hata Mesajı</label>
                            <asp:TextBox runat="server" ID="txtMessage" TextMode="MultiLine" Columns="60" Rows="12" Text='<%# Eval("Message") %>' />
                        </fieldset>
                        <fieldset>
                            <label>Çözüm Talebiniz</label>
                            <br />
                                <editor:CKEditorControl BasePath="~/Assets/Libraries/ckeditor" ID="EditorArea" runat="server" Rows="12" Text='<%# Eval("Detail") %>'/>
                        </fieldset>
                        <fieldset style="width: 48%; float: left; margin-right: 3%;">
                            <label>Hata Tipi</label>
                            <asp:RadioButtonList runat="server" ID="rblCrucial" RepeatDirection="Vertical" TextAlign="Right">
                                <asp:ListItem Text="Kritik Hata" Value="1" />
                                <asp:ListItem Text="Kritik Değil" Value="0" Selected="True" />
                            </asp:RadioButtonList>
                        </fieldset>
                        <div class="clear"></div>
                        <footer>
                            <div class="submit_link">
                                <asp:Button Text="Hata Bildirin" runat="server" CssClass="alt_btn" ID="btnInsertPublish" CommandName="Insert" />
                                <input type="submit" value="Sıfırlayın" class="reset">
                            </div>
                        </footer>
                    </article>
                </InsertItemTemplate>
            </asp:FormView>
        </div>
    </div>
    <footer>
    </footer>
</article>
<div class="clear"></div>
