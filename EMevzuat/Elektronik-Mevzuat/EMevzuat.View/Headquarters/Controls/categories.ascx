﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="categories.ascx.cs" Inherits="EMevzuat.View.Headquarters.Controls.categories" %>
<%@ Import Namespace="EMevzuat.Controller.Extension.Category" %>

<article class="module width_full">
    <header>
        <h3 class="tabs_involved">Kategoriler</h3>
        <ul class="tabs">
            <li><a href="#tab1">Kategori Listesi</a></li>
            <li><a href="#tab2">Kategori Ekle/Düzenle</a></li>
        </ul>
    </header>
    <div class="tab_container">
        <div id="tab1" class="tab_content">
            <asp:GridView runat="server" ID="grdCategories" AllowPaging="true" AllowSorting="true" CssClass="tablesorter" AutoGenerateColumns="false" DataKeyNames="ID" BorderWidth="0px">
                <Columns>
                    <asp:BoundField DataField="Name" DataFormatString="" HeaderText="Tanım" NullDisplayText="Veri Yok"></asp:BoundField>
                    <asp:BoundField DataField="Description" DataFormatString="" HeaderText="Açıklama" NullDisplayText="Veri Yok"></asp:BoundField>
                    <asp:TemplateField HeaderText="Kategori Yolu">
                        <ItemTemplate>
                            <asp:Label runat="server" Text='<%# SCategoryParser.GetPathRecursive(Int32.Parse(Eval("ID").ToString())) %>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Üst Kategori">
                        <ItemTemplate>
                            <asp:Label runat="server" Text='<%# SCategoryParser.GetParentName(Int32.Parse(Eval("ID").ToString())) %>'>

                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Komutlar">
                        <ItemTemplate>
                            <asp:HyperLink runat="server" NavigateUrl='<%# DataBinder.Eval(Container.DataItem, "ID", Request.ServerVariables["URL"] + "?id={0}") %>' ToolTip="Belge Kategorisini Düzenleyin"><img runat="server" src="~/Assets/Sections/Headquarters/Layouts/images/icn_eye.png" /></asp:HyperLink>
                            <asp:ImageButton runat="server" CommandName="Delete" ImageUrl="~/Assets/Sections/Headquarters/Layouts/images/icn_cancel.png" ToolTip="Belge Kategorisini Silin"
                                OnClientClick="return confirm('Kategori ve Bağlı Tüm Belgeler Tamamen Silinecektir');"/>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
        <div id="tab2" class="tab_content">
            <asp:FormView runat="server" ID="fvwCategory" DataKeyNames="ID">
                <EditItemTemplate>
                    <article class="module width_inner">
                        <header>
                            <h3>Kategoriyi Düzenleyin</h3>
                        </header>
                        <fieldset>
                            <label>Tanım</label>
                            <asp:TextBox runat="server" ID="txtName" Text='<%# Eval("Name") %>' />
                        </fieldset>
                        <fieldset>
                            <label>Açıklama</label>
                            <asp:TextBox runat="server" ID="txtDescription" Text='<%# Eval("Description") %>' />
                        </fieldset>
                        <fieldset>
                            <label>Üst Kategori Seçin</label>
                            <asp:DropDownList runat="server" ID="ddlSelect" DataSourceID="odsCategories" DataTextField="Line" DataValueField="LineID" CssClass="select"></asp:DropDownList>
                            <asp:ObjectDataSource runat="server" SelectMethod="GetCategoryLinesAbove" ID="odsCategories" TypeName="EMevzuat.Controller.Extension.Category.SCategoryParser">
                                <SelectParameters>
                                    <asp:QueryStringParameter Name="categoryID" QueryStringField="id" Type="Int32" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </fieldset>
                        <div class="clear"></div>
                        <footer>
                            <div class="submit_link">
                                <asp:Button Text="Güncelleyin" runat="server" CssClass="alt_btn" ID="btnUpdate" CommandName="Update" />
                                <input type="submit" value="Sıfırlayın" class="reset">
                            </div>
                        </footer>
                    </article>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <article class="module width_inner">
                        <header>
                            <h3>Yeni Kategori Ekleyin</h3>
                        </header>
                        <fieldset>
                            <label>Tanım</label>
                            <asp:TextBox runat="server" ID="txtName" Text='<%# Eval("Name") %>' />
                        </fieldset>
                        <fieldset>
                            <label>Açıklama</label>
                            <asp:TextBox runat="server" ID="txtDescription" Text='<%# Eval("Description") %>' />
                        </fieldset>
                        <fieldset>
                            <label>Üst Kategori Seçin</label>
                            <asp:DropDownList runat="server" ID="ddlSelect" DataSourceID="odsCategories" DataTextField="Line" DataValueField="LineID" CssClass="select">
                            </asp:DropDownList>
                            <asp:ObjectDataSource runat="server" SelectMethod="GetCategoryLines" ID="odsCategories" TypeName="EMevzuat.Controller.Extension.Category.SCategoryParser"></asp:ObjectDataSource>
                        </fieldset>
                        <div class="clear"></div>
                        <footer>
                            <div class="submit_link">
                                <asp:Button Text="Ekleyin" runat="server" CssClass="alt_btn" ID="btnInsert" CommandName="Insert" />
                                <input type="submit" value="Sıfırlayın" class="reset">
                            </div>
                        </footer>
                    </article>
                </InsertItemTemplate>
            </asp:FormView>
        </div>
    </div>
</article>
