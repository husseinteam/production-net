﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EMevzuat.Controller.ModifyView;

using EMevzuat.Logic.Http;
using WebFormsMvp;
using EMevzuat.Controller.Extension.HtmlPages;
using System.Dynamic;
using EMevzuat.Controller.Extension.User;

namespace EMevzuat.View.Headquarters.Controls {
    [PresenterBinding(typeof(ModifyPresenter))]
    public partial class solutions : _Root {

        protected override void LoadControl() {
            if (true == Model.Visible) {
                if (null == SUserParser.GetAdministrator()) {
                    SHttpTransmit.Transfer("~/Headquarters/Solutions.aspx", true);
                } else {
                    hdnSelect.Value = Model.Entity.ExceptionID.ToString();
                }
            }
        }

        protected override dynamic TakeEntity(Object ID = default(Object)) {
            dynamic entity;
            entity = new ExpandoObject();
            entity.ID = ID != default(Object) ? Guid.Parse(ID.ToString()) : Guid.NewGuid();
            entity.Reply = (EntityForm.FindControl("txtReply") as TextBox).Text;
            entity.Solved = (EntityForm.FindControl("rblSolved") as RadioButtonList).SelectedValue == "1";
            entity.ExceptionID = Guid.Parse(hdnSelect.Value);
            return entity;
        }

        protected override FormView EntityForm {
            get { return fvwSolution; }
        }

        protected override GridView DisplayBoard {
            get { return grdSolutions; }
        }

        public override string GenerateEntityIdentifier() {
            return "MTSAVSolution";
        }
    }
}