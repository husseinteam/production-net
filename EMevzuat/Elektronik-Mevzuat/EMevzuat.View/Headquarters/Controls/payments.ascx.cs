﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EMevzuat.Controller.Extension.Enums;
using EMevzuat.Controller.ModifyView;

using EMevzuat.Logic.Parse;
using EMevzuat.View.Headquarters;
using WebFormsMvp;
using System.Dynamic;

namespace EMevzuat.View.Headquarters.Controls {
    [PresenterBinding(typeof(ModifyPresenter))]
    public partial class payments : _Root {

        protected override void LoadControl() {
            if (true == Model.Visible) {
                hdnSelect.Value = Model.Entity.UserID.ToString();
            }
        }

        protected override dynamic TakeEntity(Object ID = default(Object)) {
            dynamic entity;
            entity = new ExpandoObject();
            entity.ID = ID != default(Object) ? Guid.Parse(ID.ToString()) : Guid.NewGuid();
            entity.UserID = Guid.Parse(hdnSelect.Value);
            entity.Timestamp = DateTime.Now;
            entity.Amount = Decimal.Parse((EntityForm.FindControl("txtAmount") as TextBox).Text);
            entity.Status = SParser.ParseType<Int32>((EntityForm.FindControl("rblPaymentStatus") as RadioButtonList).SelectedItem.Value);

            return entity;
        }

        protected override FormView EntityForm {
            get { return fvwPayment; }
        }

        protected override GridView DisplayBoard {
            get { return grdPayments; }
        }

        public override string GenerateEntityIdentifier() {
            return "MTSAVPayment";
        }

    }
}