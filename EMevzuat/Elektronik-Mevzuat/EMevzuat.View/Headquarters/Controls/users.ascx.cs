﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EMevzuat.Controller.Extension.Enums;
using EMevzuat.Controller.ModifyView;

using EMevzuat.Logic.Crypto;
using EMevzuat.Logic.Parse;
using EMevzuat.View.Headquarters;
using WebFormsMvp;
using WebFormsMvp.Web;
using EMevzuat.Logic.Http;
using System.Dynamic;
using EMevzuat.Controller.Extension.User;

namespace EMevzuat.View.Headquarters.Controls {

    [PresenterBinding(typeof(ModifyPresenter))]
    public partial class users : _Root {

        #region Private Methods

        protected override void LoadControl() {
            if (true == Model.Visible) {
                hdnSelect.Value = Model.Entity.MembershipPackageID.ToString();
            }
        }

        #endregion

        #region Overrides
        protected override bool WillDisjoint(Object ID, out String message) {
            if (SUserParser.IsAdministrator(ID)) {
                message = "Developer Hesap Silinemez";
                return false;
            } else if (ID == SHttpContext.AuthUserID) {
                message = "Oturum Açmış Kullanıcı Silinemez";
                return false;
            } else {
                message = "Silme İşlemi Yetkilendirildi";
                return true;
            }
        }

        protected override dynamic TakeEntity(Object ID = default(Object)) {
            dynamic user;
            user = new ExpandoObject();
            Byte[] key, iv;
            SCryptoManager.GenerateSymmetricAlgorithms(out key, out iv);
            user.IV = iv;
            user.Key = key;
            user.Cipher = SCryptoManager.Encrypt(SUserParser.GetUserPassword(ID), key, iv);
            user.FullName = (EntityForm.FindControl("txtFullName") as TextBox).Text;
            user.MailAddress = (EntityForm.FindControl("txtMail") as TextBox).Text;
            user.IPv4 = (EntityForm.FindControl("txtIPv4") as TextBox).Text;

            user.Institution = (EntityForm.FindControl("txtInstitution") as TextBox).Text;
            user.Position = (EntityForm.FindControl("txtPosition") as TextBox).Text;
            user.MembershipPackageID = Int32.Parse(hdnSelect.Value);
            if (ID == default(Object)) {
                user.ID = Guid.NewGuid();
                user.MembershipDate = DateTime.Now;
            } else {
                user.ID = Guid.Parse(ID.ToString());
            }

            return user;
        }

        protected override FormView EntityForm {
            get { return fvwUser; }
        }

        protected override GridView DisplayBoard {
            get { return grdUsers; }
        }

        public override string GenerateEntityIdentifier() {
            return "MTSAVUser";
        }

        #endregion
    }
}