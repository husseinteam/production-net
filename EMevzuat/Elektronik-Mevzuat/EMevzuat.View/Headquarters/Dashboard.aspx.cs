﻿using EMevzuat.Logic.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EMevzuat.View.Headquarters {
    public partial class Dashboard : System.Web.UI.Page {

        #region Page Methods
        protected override void OnLoad(EventArgs e) {
            if (Page.IsPostBack == false) {
                LoadPage();
            }
            base.OnLoad(e);
        }

        #endregion

        #region Loader
        private void LoadPage() {
            (Master as Headquarters).SetSectionMessage("Yönetim Modülüne Hoş Geldiniz");
        }
        #endregion
    }
}