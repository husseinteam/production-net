﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EMevzuat.View.Headquarters {
    public partial class Users : System.Web.UI.Page {

        #region Overrides
        protected override void OnLoad(EventArgs e) {
            if (Page.IsPostBack == false) {
                LoadPage();
            }
            base.OnLoad(e);
        }

        #endregion

        #region
        private void LoadPage() {
            (Master as Headquarters).SetSectionMessage("Bu Modülden Kullanıcı Görüntüleme, Silme, Değiştirme ve Ekleme İşlevlerini Kullanabilirsiniz.");
        }
        #endregion
    }
}