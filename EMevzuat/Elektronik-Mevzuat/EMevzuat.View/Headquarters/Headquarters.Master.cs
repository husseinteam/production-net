﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using EMevzuat.Controller.Extension.User;
using EMevzuat.Logic.Http;
using EMevzuat.Controller.Extension.HtmlPages;

namespace EMevzuat.View.Headquarters {
    public partial class Headquarters : System.Web.UI.MasterPage {

        #region Overrides
        protected override void OnInit(EventArgs e) {
            LoadResources();
            base.OnInit(e);
        }
        protected override void OnLoad(EventArgs e) {
            if (null != SHttpContext.AuthUserID) {
                LoadPage();
                if (null == SUserParser.GetAdministrator()) {
                    SHttpTransmit.Transfer("~/Residence/Default.aspx", true);
                }
            } else {
                SHttpTransmit.Transfer("~/FrontGarden/Home.aspx", true);
            }
            base.OnLoad(e);
        }

        #endregion

        #region Private Methods
        private void LoadPage() {
            if (Page.IsPostBack == false) {
                lblAdminTitle.Text = String.Format("Sn. {0}, Hoşgeldiniz", SUserParser.GetUserFullName(SHttpContext.AuthUserID));
            }
        }

        private void LoadResources() {
            if (SHttpContext.CurrentRequest.Browser.Browser == "IE" && SHttpContext.CurrentRequest.Browser.MajorVersion <= 9) {
                SScriptManager.LoadAssets("~/Assets/Sections/Headquarters/Optional");
            }
            SScriptManager.LoadAssets("~/Assets/Sections/Headquarters/Layouts");
        }
        private static void Logout() {
            if (SHttpContext.AuthUserID != null && SSessionManager.Logout(Guid.Parse(SHttpContext.AuthUserID.ToString())) == true) {
                SScriptManager.RegisterStartupBrifing("Çıkış İşleminiz Tamamlandı");
            }
            SHttpTransmit.Transfer("~/FrontGarden/Login.aspx");
        }
        #endregion

        #region Event Handlers
        protected void btnLogout_Click(object sender, EventArgs e) {
            Logout();
        }
        #endregion

        #region Buses
        public void SetSectionMessage(String message) {
            lblSectionMessage.Text = message;
        }

        public void SetWarning(String content, Boolean? done = null) {
            if (done.HasValue) {
                if (done.Value == true) {
                    msgDone.Visible = true;
                    msgDone.InnerText = content;
                } else {
                    msgError.Visible = true;
                    msgError.InnerText = content;
                }
            } else {
                msgWarning.Visible = true;
                msgWarning.InnerText = content;
            }
        }
        public HiddenField HdnSelect { get { return hdnSelect; } }
        public HiddenField HdnOther { get { return hdnOther; } }
        #endregion

    }
}