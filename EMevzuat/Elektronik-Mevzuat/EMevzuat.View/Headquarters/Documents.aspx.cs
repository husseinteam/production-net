﻿using EMevzuat.Controller.Extension.Files;
using EMevzuat.Logic.Parse;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EMevzuat.View.Headquarters {
    public partial class Documents : System.Web.UI.Page {

        #region Overrides
        protected override void OnLoad(EventArgs e) {
            if (Page.IsPostBack == false) {
                LoadPage();
            }
            base.OnLoad(e);
        }

        private void LoadPage() {
            (Master as Headquarters).SetSectionMessage("Bu Modülden Belge Ekleme, Düzenleme, Listeleme ve Görüntüleme İşlemlerini Yapabilirsiniz.");
        }
        #endregion

    }
}