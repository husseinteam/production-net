﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EMevzuat.View.Headquarters {
    public partial class Htmls : System.Web.UI.Page {

        #region Overrides
        protected override void OnLoad(EventArgs e) {
            if (Page.IsPostBack == false) {
                LoadPage();
            }
            base.OnLoad(e);
        }

        private void LoadPage() {
            (Master as Headquarters).SetSectionMessage("Bu Modülden Sayfaların İçeriklerini Değiştirebilir, Ekleme ve Düzenleme İşlemlerinizi Gerçekleştirebilirsiniz.");
        }
        #endregion


    }
}