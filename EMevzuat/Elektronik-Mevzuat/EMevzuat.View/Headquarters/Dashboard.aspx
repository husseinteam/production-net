﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Headquarters/Headquarters.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="EMevzuat.View.Headquarters.Dashboard" %>

<%@ Import Namespace="EMevzuat.Controller.Extension.HtmlPages" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="server">
    <article class="module width_full">
        <header>
            <h3 class="tabs_involved"><%= SHtmlPages.GenerateLinkOf("~/Headquarters/Dashboard.aspx", null, "Genel Görünüm") %></h3>
            <ul class="tabs">
                <li><a href="#tab1">İçerik Yönetimi</a></li>
                <li><a href="#tab2">Üyelik Yönetimi</a></li>
                <li><a href="#tab3">Ayarlar</a></li>
                <li><a href="#tab4">Yardım</a></li>
            </ul>
        </header>
        <div id="dashboard" class="tab_container">
            <div id="tab1" class="tab_content">
                <table class="CommandRow">
                    <tr>
                        <td>
                            <%= SHtmlPages.GenerateLinkOf("~/Headquarters/Documents.aspx", "", "<div class='CommandBox d'><span>@@</span></div>")%></td>
                        <td><%= SHtmlPages.GenerateLinkOf("~/Headquarters/Categories.aspx", "", "<div class='CommandBox c'><span>@@</span></div>")%></td>
                        <td><%= SHtmlPages.GenerateLinkOf("~/Headquarters/UserMails.aspx", "", "<div class='CommandBox m'><span>@@</span></div>")%></td>
                        <td><%= SHtmlPages.GenerateLinkOf("~/Headquarters/Bulletins.aspx", "", "<div class='CommandBox b'><span>@@</span></div>")%></td>
                        <td><%= SHtmlPages.GenerateLinkOf("~/Headquarters/Htmls.aspx", "", "<div class='CommandBox h'><span>@@</span></div>")%></td>
                    </tr>
                </table>
            </div>
            <div id="tab2" class="tab_content">
                <table class="CommandRow">
                    <tr>

                        <td><%= SHtmlPages.GenerateLinkOf("~/Headquarters/Users.aspx", "", "<div class='CommandBox u'><span>@@</span></div>")%></td>

                        <td><%= SHtmlPages.GenerateLinkOf("~/Headquarters/Packages.aspx", "", "<div class='CommandBox p'><span>@@</span></div>")%></td>

                        <td><%= SHtmlPages.GenerateLinkOf("~/Headquarters/PackageCategories.aspx", "", "<div class='CommandBox pc'><span>@@</span></div>")%></td>

                        <td><%= SHtmlPages.GenerateLinkOf("~/Headquarters/Payments.aspx", "", "<div class='CommandBox py'><span>@@</span></div>")%></td>
                    </tr>
                </table>
            </div>
            <div id="tab3" class="tab_content">
                <table class="CommandRow">
                    <tr>
                        <td><%= SHtmlPages.GenerateLinkOf("~/Headquarters/Exceptions.aspx", "", "<div class='CommandBox e'><span>@@</span></div>")%></td>
                        <td><%= SHtmlPages.GenerateLinkOf("~/Headquarters/Solutions.aspx", "", "<div class='CommandBox s'><span>@@</span></div>")%></td>
                    </tr>
                </table>
            </div>
            <div id="tab4" class="tab_content">
                <table class="CommandRow">
                    <tr>
                        <td>

                            <a href="#" onclick="boxify();">
                                <div class="CommandBox va">
                                    <span>Yardım</span>
                                </div>
                            </a>
                            <div style="display: none;">
                                <video id="VideoHelp" class="video-js vjs-default-skin vjs-big-play-centered"
                                    controls preload="auto" width="800" height="600"
                                    poster="http://<%=Request.Url.Host %>/<%= EMevzuat.Logic.Http.SHttpContext.VirtualDirectory %>/Assets/Local/Anime/clip.png"
                                    data-setup='{ "controls": true, "autoplay": false, "preload": "auto" }'>
                                    <source src="http://<%=Request.Url.Host %>/<%= EMevzuat.Logic.Http.SHttpContext.VirtualDirectory %>/Assets/Local/Anime/2014-02-07_15-40-33.mp4" type='video/mp4' />
                                    <span>Tarayıcınız Html5 Video Oynatımını Desteklemiyor.</span>
                                    <span>İsterseniz video dosyasını <a href="http://<%=Request.Url.Host %>/<%= EMevzuat.Logic.Http.SHttpContext.VirtualDirectory %>/Assets/Local/Anime/2014-02-07_15-40-33.mp4">indirebilirsiniz</a>.</span>
                                </video>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </article>
    <!-- end of stats article -->
    <div class="clear"></div>
    <script>
        videojs.options.flash.swf = "http://<%=Request.Url.Host %>/<%= EMevzuat.Logic.Http.SHttpContext.VirtualDirectory %>/Assets/Local/Anime/video-js.swf";
        function boxify() {
            $('#VideoHelp').parents().first().show();
            $('#VideoHelp').css({
                'z-index': 100, position: 'absolute', boxShadow: '0x000 7px 7px 9px',
                left: '50%',
                right: '50%',
                top: '50%',
                bottom: '50%',
                width: '0px',
                height: '0px'
            }).animate({
                width: '94%',
                height: '90%',
                left: '6%',
                right: '6%',
                top: '6%',
                bottom: '10%'
            }, 1500, 'linear', function () {

            });
            var screen = $('<div class="__screenlobe"/>').css({
                'opacity': '0.6', 'background-color': 'rgb(242,242,242)', 'z-index': '99',
                'background-position': 'center', 'background-repeat': 'no-repeat',
                'margin-top': '0px',
                'position': 'fixed',
                'top': '0px',
                'right': '0px'
            }).width($(document).width()).height($(window).height()).click(function (e) {
                $('.__screenlobe').remove();
                $('#VideoHelp').parents().first().hide();
            });

            if ($('.__screenlobe').length === 0) {
                screen.appendTo('body');
            }
        }
    </script>

</asp:Content>
