﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace DICTROM.Transcription.Attributes
{
    [AttributeUsage(AttributeTargets.Assembly, AllowMultiple = false)]
    public class DNADescriptorAttribute : Attribute
    {
        #region ctor
        public DNADescriptorAttribute()
        {
        }
        #endregion

        #region properties
        #endregion
    }
}
