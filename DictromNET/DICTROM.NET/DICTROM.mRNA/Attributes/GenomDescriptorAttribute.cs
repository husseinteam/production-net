﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DICTROM.Transcription.Attributes
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class GenomDescriptorAttribute : Attribute
    {
        #region ctor
        public GenomDescriptorAttribute(Type objectType)
        {
            this.ObjectType = objectType;
        }
        #endregion

        #region properties
        public Type ObjectType { get; set; }
        #endregion
    }
}
