﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DICTROM.Transcription.Constraints
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true, Inherited = false)]
    public class EnumerationConstraint : Attribute
    {
        public EnumerationConstraint(params object[] enumerationValues)
        {
            this.EnumerationValues = enumerationValues;
        }

        public object[] EnumerationValues { get; set; }
    }
}
