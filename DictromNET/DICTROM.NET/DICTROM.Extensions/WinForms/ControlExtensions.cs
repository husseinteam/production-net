﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace DICTROM.Extensions.WinForms {
    
    public static class ControlExtensions {

        public static void CycleSteps(this ToolStripProgressBar self, Single delaySeconds) {

            if (self.Value <= self.Maximum - self.Step) {
                self.PerformStep();
                Thread.Sleep(Convert.ToInt32(delaySeconds * 1000));
            } else {
                self.Value = self.Minimum;
            }

        }

    }

}
