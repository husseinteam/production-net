﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace DICTROM.Extensions.GenericExtensions {

    public static class ReflectionExtensions {

        public static TAttribute GetCustomAttribute<TAttribute>(this PropertyInfo prop)
            where TAttribute : Attribute {

            return prop.GetCustomAttributes(typeof(TAttribute), false).FirstOrDefault() as TAttribute;

        }

    }

}
