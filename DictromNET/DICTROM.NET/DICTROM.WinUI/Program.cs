﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DICTROM.Core.WinForms.EntryUI;
using System.IO;
using DICTROM.Core.WinForms.ViewerUI;
using DICTROM.Core.Model.DicomEntity;
using DICTROM.Transcription.Transcriptor;
using System.Reflection;
using DICTROM.Core.ProxyService.Managers;
using System.Text;

namespace DICTROM.WinUI {
    static class Program {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args) {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            STranscriptor.MakeProgress(AppDomain.CurrentDomain);

            string entityPath;
            if (args.Length == 0) {
                var ef = new TEntryForm();
                ef.InitWorkList();
                ef.ReloadNetworkList();
                Application.Run(ef);
            } else {
                MessageBox.Show("Test");
                entityPath = args[0].Replace("-", "");

                var vw = new TViewerForm();
                if (Directory.Exists(entityPath)) {
                    var entity = new TDicomEntity(entityPath);
                    entity.Import(entityPath);
                    vw.InitForm(entity);
                }

                Application.Run(vw);
            }
        }
    }
}
