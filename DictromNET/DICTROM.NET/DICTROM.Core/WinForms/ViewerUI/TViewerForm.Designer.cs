﻿namespace DICTROM.Core.WinForms.ViewerUI
{
    partial class TViewerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ucDicomViewer = new DICTROM.Core.WinUC.ViewerUI.DicomViewerUC();
            this.SuspendLayout();
            // 
            // ucDicomViewer
            // 
            this.ucDicomViewer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ucDicomViewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucDicomViewer.Location = new System.Drawing.Point(0, 0);
            this.ucDicomViewer.Name = "ucDicomViewer";
            this.ucDicomViewer.Size = new System.Drawing.Size(748, 484);
            this.ucDicomViewer.TabIndex = 0;
            // 
            // ViewerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(748, 484);
            this.Controls.Add(this.ucDicomViewer);
            this.Name = "ViewerForm";
            this.Text = "Görüntüleme Ekranı";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);

        }

        #endregion

        private WinUC.ViewerUI.DicomViewerUC ucDicomViewer;
    }
}