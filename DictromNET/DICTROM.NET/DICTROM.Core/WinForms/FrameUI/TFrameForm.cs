﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DICTROM.Core.Model.DicomEntity;
using DICTROM.Core.WinUC.ViewerUI;
using DICTROM.Core.WinUC.FrameUI;
using System.Threading.Tasks;

namespace DICTROM.Core.WinForms.FrameUI
{
    public partial class TFrameForm : Form
    {
        #region ctor
        public TFrameForm()
        {
            InitializeComponent();
        }
        #endregion

        #region public methods
        #endregion

        public void DisplayForm(TDicomSeries dicomSeries)
        {
            var uc = new DicomFrameUC();
            uc.SetSeries(dicomSeries);

            this.Controls.Add(uc);
            this.ShowDialog();
        }
    }
}
