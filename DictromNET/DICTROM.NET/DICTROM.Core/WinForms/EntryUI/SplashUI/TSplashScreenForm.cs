﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace DICTROM.Core.WinForms.EntryUI.SplashUI
{
    public partial class TSplashScreenForm : Form
    {
        public TSplashScreenForm()
        {
            InitializeComponent();
            closer = new dCloser(() => this.Close());
        }

        private Thread tDisplayer;
        private delegate void dCloser();
        private dCloser closer;

        public void ShowAsync()
        {
            tDisplayer = new Thread(new ParameterizedThreadStart((t) => (t as TSplashScreenForm).Show()));
            tDisplayer.Start(this);
        }

        public void CloseAsync()
        {
            tDisplayer.Abort();
            if (InvokeRequired)
                Invoke(closer);
            else
                this.Close();
        }
    }
}
