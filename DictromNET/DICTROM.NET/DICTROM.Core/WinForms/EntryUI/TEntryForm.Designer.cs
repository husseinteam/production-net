﻿namespace DICTROM.Core.WinForms.EntryUI
{
    partial class TEntryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TEntryForm));
            this.ucEntry = new DICTROM.Core.WinUC.EntryUI.EntryUC();
            this.SuspendLayout();
            // 
            // ucEntry
            // 
            this.ucEntry.BackColor = System.Drawing.Color.White;
            this.ucEntry.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucEntry.Location = new System.Drawing.Point(0, 0);
            this.ucEntry.Name = "ucEntry";
            this.ucEntry.Size = new System.Drawing.Size(906, 529);
            this.ucEntry.TabIndex = 0;
            // 
            // EntryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(906, 529);
            this.Controls.Add(this.ucEntry);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "EntryForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DICTROM.NET PACS Görüntüleme";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);

        }

        #endregion

        private WinUC.EntryUI.EntryUC ucEntry;
    }
}