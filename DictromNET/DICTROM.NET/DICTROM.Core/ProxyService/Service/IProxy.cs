﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.Net.Security;
using DICTROM.Core.ProxyService.ProxyData;

namespace DICTROM.Core.ProxyService.Service
{
    [ServiceContract(ProtectionLevel = ProtectionLevel.None)]
    public interface IProxy
    {
        [OperationContract]
        string Connect(string hostName);

        [OperationContract]
        void Disconnect();

        [OperationContract]
        void TransferEntityTree(TRemoteMessage entityToTransfer);

        [OperationContract]
        bool Alive();

    }
}
