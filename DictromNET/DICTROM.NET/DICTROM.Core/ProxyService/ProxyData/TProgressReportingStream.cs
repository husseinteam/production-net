﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using DICTROM.Core.Resource;

namespace DICTROM.Core.ProxyService.ProxyData
{
    public class ProgressChangedEventArgs : EventArgs
    {
        public long BytesRead;
        public long Length;

        public ProgressChangedEventArgs(long BytesRead, long Length)
        {
            this.BytesRead = BytesRead;
            this.Length = Length;
        }

        #region methods
        public double GetCompletionRatio()
        {
            return (double)BytesRead / Length;
        }
        #endregion
    }

    public class TProgressReportingStream : Stream
    {
        #region ctors
        public TProgressReportingStream(FileStream file)
        {
            this._File = file;
            _Length = file.Length;
            _BytesRead = 0;
        }
        #endregion

        #region events
        public event EventHandler<ProgressChangedEventArgs> ProgressChanged;
        private void OnProgressChanged(object sender, ProgressChangedEventArgs args)
        {
            if (ProgressChanged != null)
                ProgressChanged(sender, args);
        }
        #endregion

        #region Stream Abstract Members
        public override int Read(byte[] buffer, int offset, int count)
        {
            ProgressChangedEventArgs args;
            long result = 0;

            args = new ProgressChangedEventArgs(0, _Length);
            result = _File.Read(buffer, 0, buffer.Length);
            _BytesRead += result;

            args.BytesRead = _BytesRead;
            OnProgressChanged(this, args);

            return (int)result;
        }
        public override long Length
        {
            get
            {
                return _File.Length;
            }
        }
        public override bool CanRead
        {
            get { return _File.CanRead; }
        }

        public override bool CanSeek
        {
            get { return _File.CanSeek; }
        }

        public override bool CanWrite
        {
            get { return _File.CanWrite; }
        }

        public override void Flush()
        {
            _File.Flush();
        }

        public override long Position
        {
            get
            {
                return _File.Position;
            }
            set
            {
                _File.Position = value;
            }
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            return _File.Seek(offset, origin);
        }

        public override void SetLength(long value)
        {
            _File.SetLength(value);
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            _File.Write(buffer, offset, count);
        }
        #endregion

        #region fields
        private readonly FileStream _File;
        private readonly long _Length;
        private long _BytesRead;
        #endregion
    }
}
