﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace DICTROM.Core.ProxyService.ProxyData
{
    public struct TProxyData
    {
        #region methods
        public TProxyData(string alias, IPAddress ip, string aet, Int32 port)
        {
            ServerAlias = alias;
            IP = ip;
            AET = aet;
            Port = port;

            string appName  = String.Format("{0}/{1}",
                ServerAlias, aet);
            ServiceUrl = new Uri(String.Format("net.tcp://{0}:{1}/{2}"
                , ip.ToString(), port, appName));
        }
        #endregion

        #region properties
        public string ServerAlias;
        public IPAddress IP;
        public string AET;
        public int Port;

        public Uri ServiceUrl;
        #endregion

    }
}
