﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.IO;

namespace DICTROM.Core.ProxyService.ProxyData
{
    [MessageContract]
    public class TRemoteMessage : IDisposable
    {
        #region messages
        [MessageHeader(MustUnderstand = true)]
        public string FileName;

        [MessageHeader(MustUnderstand = true)]
        public long FileLength;

        [MessageBodyMember(Order = 1)]
        public Stream FileByteStream;

        #endregion

        public TProgressReportingStream ProgressReportingStream
        {
            get { return FileByteStream as TProgressReportingStream; }
        }

        #region IDisposable Members
        public void Dispose()
        {
            if (FileByteStream != null)
            {
                FileByteStream.Close();
                FileByteStream = null;
            }
        }
        #endregion

    }
}
