﻿using System;
using System.Net;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using DICTROM.Core.Data.AccessLayer;
using DICTROM.Core.ProxyService.ProxyData;
using DICTROM.Core.Resource;
using DICTROM.Core.ProxyService.Service;
using DICTROM.Core.ProxyService.Managers;
using System.Threading;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Text;

namespace DICTROM.Core.ProxyService.Managers {
    public static class SProxyManager {
        #region service methods
        public static Boolean StopService() {

            var resp = true;
            if (_Host == null)
                return false;

            Parallel.Invoke(() => {
                if (_Host.State == CommunicationState.Opened) {
                    _Host.Close();
                    _Host = null;
                    GC.Collect();
                    resp = true;
                } else {
                    resp = false;
                }
            }, () => {
                var start = DateTime.Now;
                while (DateTime.Now.Subtract(start).Seconds <= 5 && resp) { }
                resp = resp || false;
            });
            return resp;

        }

        public static bool StartService(String serverAlias, String ip, String aet, Int32 port, StringBuilder error) {

            try {
                var pdata = new TProxyData(serverAlias, IPAddress.Parse(ip), aet, port);
                SetServiceParameters(pdata.ServiceUrl);
                SQRDAL.Remote.Upsert(serverAlias, IPAddress.Parse(ip), aet, port);
                _ServerAlias = serverAlias;
                return true;
            } catch (Exception ex) {
                error.AppendFormat("Servis Başlatılma Hatası: {0}\n", ex.Message);
                return false;
            }
        }
        public static bool StartService() {

            var error = new StringBuilder();
            var settings = SQRDAL.Remote.SelectAll().FirstOrDefault(stt => stt.ServerAlias == Environment.MachineName);
            if (settings != null) {
                return StartService(settings.ServerAlias, settings.ClientIP, settings.AET, settings.Port, error);
            } else {
                return false;
            }
            
        }
        public static IProxy GetProxy(TProxyData pdata) {

            if (Bind != null) {
                var generatorChannel = new ChannelFactory<IProxy>(Bind);

                IProxy prx = generatorChannel.CreateChannel(new EndpointAddress(pdata.ServiceUrl));
                //IClientChannel contextChannel = prx as IClientChannel;
                //contextChannel.OperationTimeout = TimeSpan.FromMinutes(10);
                return prx;
            } else {
                return null;
            }
        }
        public static bool IsServerStarted() {

            return _Host != null ? _Host.State == CommunicationState.Opened : false;

        }
        #endregion

        #region private methods
        private static void SetServiceParameters(Uri objURI) {
            if (Bind == null) {
                _Bind = new NetTcpBinding(SecurityMode.None) {
                    SendTimeout = TimeSpan.FromMinutes(100),

                    ReceiveTimeout = TimeSpan.FromMinutes(100),

                    OpenTimeout = TimeSpan.FromMinutes(10),

                    CloseTimeout = TimeSpan.FromMinutes(10)
                };
                Bind.ReaderQuotas.MaxArrayLength = 2147483647;
                Bind.MaxReceivedMessageSize = 2147483647;
            }

            _Host = new ServiceHost(typeof(TProxy), objURI);
            _Host.AddServiceEndpoint(
                typeof(IProxy),
                Bind,
                objURI);

            _Host.OpenTimeout = TimeSpan.FromMinutes(10);

            _Host.CloseTimeout = TimeSpan.FromMinutes(10);

            // Find the ContractDescription of the operation to find.
            ContractDescription cd = _Host.Description.Endpoints[0].Contract;
            OperationDescription myOperationDescription = cd.Operations.Find("TransferEntityTree");

            // Find the serializer behavior.
            DataContractSerializerOperationBehavior serializerBehavior =
                myOperationDescription.Behaviors.
                   Find<DataContractSerializerOperationBehavior>();

            // If the serializer is not found, create one and add it.
            if (serializerBehavior == null) {
                serializerBehavior = new DataContractSerializerOperationBehavior(myOperationDescription);
                myOperationDescription.Behaviors.Add(serializerBehavior);
            }

            // Change the settings of the behavior.
            serializerBehavior.MaxItemsInObjectGraph = 2147483647;
            //serializerBehavior.IgnoreExtensionDataObject = true;

            _Host.Open();
        }
        public static void PingProxy(TProxyData pdata,  Action<IProxy, StringBuilder> callback) {

            var error = new StringBuilder();
            Thread t = new Thread(new ThreadStart(() => {
                IProxy proxy = null;
                try {
                    proxy = SProxyManager.GetProxy(pdata);
                    if (proxy != null) {
                        if (!proxy.Alive()) {
                            proxy = null;
                        }
                    } else {
                        error.Append("Sunucu başlatılmamış");
                        proxy = null;
                    }
                } catch (EndpointNotFoundException ex) {
                    error.Append(ex.Message);
                    proxy = null;
                }
                if (proxy == null) {
                    error.AppendFormat("Sunucu Yoklama Hatası: {0}\n", error);
                } else {
                    error.AppendFormat("Proxy: {0}", proxy.Connect(Environment.MachineName));
                }
                callback(proxy, error);
            }));

            t.Start();

        }
        private static string LocalIPAddress() {
            IPHostEntry host;
            string localIP = "";
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList) {
                if (ip.AddressFamily == AddressFamily.InterNetwork) {
                    localIP = ip.ToString();
                    break;
                }
            }
            return localIP;
        }
        #endregion

        #region add handler methods
        public static void AddImageTransmittedEventHandler(SEventManager.ImageTransmittedDelegate eh) {
            SEventManager.ImageTransmitted += new SEventManager.ImageTransmittedDelegate(eh);
        }
        public static void AddServerConnectedEventHandler(SEventManager.ServerConnectedEventHandler eh) {
            SEventManager.ServerConnected += new SEventManager.ServerConnectedEventHandler(eh);
        }
        public static void AddClientDisconnectedEventHandler(SEventManager.ClientDisconnectedEventHandler eh) {
            SEventManager.ClientDisconnected += new SEventManager.ClientDisconnectedEventHandler(eh);
        }
        #endregion

        #region fields
        private static String _ServerAlias;

        public static String ServerAlias {
            get { return SProxyManager._ServerAlias; }
        }

        private static ServiceHost _Host;

        private static NetTcpBinding _Bind;

        public static NetTcpBinding Bind {
            get { return SProxyManager._Bind; }
        }

        #endregion


    }
}
