﻿using System;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DICTROM.Core.Model.DicomEntity;
using DICTROM.Core.WinUC.FrameUI;

namespace DICTROM.Core.WinUC.ViewerUI {
    public partial class DicomViewerUC : UserControl {
        #region ctor
        public DicomViewerUC() {
            InitializeComponent();
            InitializeCustomComponents();
        }
        private void InitializeCustomComponents() {
            this.Dock = DockStyle.Fill;
            Frames2D = new DicomFrameUC[0, 0];

            AssignLeftPanelButtonEvents();
        }
        #endregion

        #region private methods
        private void SetThumbnails() {
            for (int i = 0; i < DicomSeriesSet.Length; i++) {
                Thumbnails[i] = new SeriesThumbnailUC(ref DicomSeriesSet[i], tsThumbnails);
                tsThumbnails.Items.Add(Thumbnails[i]);
            }
        }

        private void RepaintControl() {

            ucDicomFrame.Invalidate();
            ucDicomFrame.Focus();

        }
        private void MultiplyScreen(int colcount = 1, int rowCount = 1) {

            var tlpMultiplicatedScreens = new TableLayoutPanel();
            Frames2D = new DicomFrameUC[colcount, rowCount];

            Size windowSize = new Size(ucDicomFrame.Width / colcount, ucDicomFrame.Height / rowCount);
            float colPercent = 100.0f / (float)colcount;
            float rowPercent = 100.0f / (float)rowCount;

            tlpMultiplicatedScreens.Dock = DockStyle.Fill;
            tlpMultiplicatedScreens.ColumnCount = colcount;
            tlpMultiplicatedScreens.RowCount = rowCount;

            for (int i = 0; i < colcount; i++) {
                tlpMultiplicatedScreens.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, windowSize.Width));
            }
            for (int i = 0; i < rowCount; i++) {
                tlpMultiplicatedScreens.RowStyles.Add(new RowStyle(SizeType.Absolute, windowSize.Height));
            }

            int counter = 0;
            int counterlimit = DicomSeriesSet.Length;

            for (int i = 0; i < Frames2D.GetLength(0); i++) {
                for (int j = 0; j < Frames2D.GetLength(1) && counter < counterlimit; j++) {
                    Frames2D[i, j] = new DicomFrameUC();
                    Frames2D[i, j].SetSeries(DicomSeriesSet[counter++]);
                    Frames2D[i, j].Size = windowSize;
                    tlpMultiplicatedScreens.Controls.Add(Frames2D[i, j], i, j);
                }
            }

            ucDicomFrame.Controls.Clear();
            ucDicomFrame.Controls.Add(tlpMultiplicatedScreens);

            RepaintControl();
        }
        #endregion

        #region public methods
        public void InitOnce(Form parentForm, params TDicomStudy[] dicomStudyList) {
            //parentForm.FormClosed += new FormClosedEventHandler(parentForm_FormClosed);
            var seriesLength = (int)dicomStudyList.Sum<TDicomStudy>(
                new Func<TDicomStudy, decimal?>((sdyt) => sdyt.DicomSeriesList.Length));

            DicomSeriesSet = new TDicomSeries[seriesLength];

            int seriesIndex = 0;
            for (int i = 0; i < dicomStudyList.Length; i++) {
                TDicomStudy stdy = dicomStudyList[i];
                for (int j = 0; j < stdy.DicomSeriesList.Length; j++) {
                    DicomSeriesSet[seriesIndex++] = stdy.DicomSeriesList[j];
                }
            }

            ucDicomFrame.SetSeries(DicomSeriesSet[0]);
            Frames2D = new DicomFrameUC[1, 1] { { ucDicomFrame } };

            Thumbnails = new SeriesThumbnailUC[DicomSeriesSet.Length];
            SetThumbnails();

            pnlPatientInformation.Visible = true;

            if (dicomStudyList.Length == 1) {
                lblPatientName.Text = dicomStudyList[0].PatientInfoLocator.AnalyzeHeader.PatientsName;
                lblPatientSex.Text = dicomStudyList[0].PatientInfoLocator.AnalyzeHeader.PatientSex;
                lblPatientAge.Text = dicomStudyList[0].PatientInfoLocator.AnalyzeHeader.PatientsAge;
                pnlPatientInformation.Visible = true;
            } else {
                pnlPatientInformation.Visible = false;
            }

            Invalidate(true);
        }
        #endregion

        #region properties
        public DicomFrameUC[,] Frames2D { get; private set; }
        public TDicomSeries[] DicomSeriesSet { get; set; }
        private SeriesThumbnailUC[] Thumbnails { get; set; }
        #endregion
    }
}
