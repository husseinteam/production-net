﻿using System;
using System.Windows.Forms;
using DICTROM.Core.Resource;
using DICTROM.Core.Assistant;
using System.Drawing;
using DICTROM.Core.Model.Geometry;
using System.Threading.Tasks;

namespace DICTROM.Core.WinUC.ViewerUI {
    partial class DicomViewerUC {
        #region delegate
        delegate void navigate2D(int i, int j);
        private void NavigateFrames(navigate2D navigator) {
            for (int i = 0; i < Frames2D.GetLength(0); i++) {
                for (int j = 0; j < Frames2D.GetLength(1); j++) {
                    if (Frames2D[i, j] == null)
                        continue;
                    if (Frames2D[i, j].cbxSelect.Checked)
                        navigator(i, j);
                }
            }
        }
        #endregion

        #region left panel handlers
        private void AssignLeftPanelButtonEvents() {
            //overall handlers
            tsbAutoPlay.Click += new EventHandler(btnAutoPlay_Click) + new EventHandler(RepaintHandler);
            tsbSwitchInfo.Click += new EventHandler(btnInfo_Click) + new EventHandler(RepaintHandler);
            tsbLens.Click += new EventHandler(btnLens_Click) + new EventHandler(RepaintHandler);
            tsbReport.Click += new EventHandler(btnReport_Click) + new EventHandler(RepaintHandler);
            tsbResetImage.Click += new EventHandler(btnReset_Click) + new EventHandler(RepaintHandler);

            //cmsNegate
            tsmiBWNegative.Click += new EventHandler(tsmiBWNegative_Click) + new EventHandler(RepaintHandler);
            tsmiColored.Click += new EventHandler(tsmiColored_Click) + new EventHandler(RepaintHandler);

            //cmsChangeDose
            tsmiAutoDose.Click += new EventHandler(tsmiAutoDose_Click) + new EventHandler(RepaintHandler);
            tsmiBonesDose.Click += new EventHandler(tsmiBonesDose_Click) + new EventHandler(RepaintHandler);
            tsmiAbdomenDose.Click += new EventHandler(tsmiAbdomenDose_Click) + new EventHandler(RepaintHandler);
            tsmiMediastinumDose.Click += new EventHandler(tsmiMediastinumDose_Click) + new EventHandler(RepaintHandler);
            tsmiLungDose.Click += new EventHandler(tsmiLungDose_Click) + new EventHandler(RepaintHandler);
            tsmiSkullDose.Click += new EventHandler(tsmiSkullDose_Click) + new EventHandler(RepaintHandler);
            tsmiBrainDose.Click += new EventHandler(tsmiBrainDose_Click) + new EventHandler(RepaintHandler);
            tsmiBrainBaseDose.Click += new EventHandler(tsmiBrainBaseDose_Click) + new EventHandler(RepaintHandler);
            tsmiBrightnessContrast.Click += new EventHandler(tsmiBrightnessContrast_Click);

            //cmsMeasure
            tsmiMeasureDistance.Click += new EventHandler(tsmiMeasureDistance_Click) + new EventHandler(RepaintHandler);
            tsmiMeasurePolygon.Click += new EventHandler(tsmiMeasurePolygon_Click) + new EventHandler(RepaintHandler);
            tsmiAngle.Click += new EventHandler(tsmiAngle_Click) + new EventHandler(RepaintHandler);
            //cmsMagnifyImage
            tsmiMagnification1x.Click += new EventHandler(tsmiMagnification1x_Click) + new EventHandler(RepaintHandler);
            tsmiMagnification15x.Click += new EventHandler(tsmiMagnification15x_Click) + new EventHandler(RepaintHandler);
            tsmiMagnification2x.Click += new EventHandler(tsmiMagnification2x_Click) + new EventHandler(RepaintHandler);
            tsmiMagnification3x.Click += new EventHandler(tsmiMagnification3x_Click) + new EventHandler(RepaintHandler);

            //cmsRotate
            tsmiRotateDefault.Click += new EventHandler(tsmiRotateDefault_Click) + new EventHandler(RepaintHandler);
            tsmiRotate90Degrees.Click += new EventHandler(tsmiRotate90Degrees_Click) + new EventHandler(RepaintHandler);
            tsmiRotate180Degrees.Click += new EventHandler(tsmiRotate180Degrees_Click) + new EventHandler(RepaintHandler);

            //cmsMultiplyScreen
            tsmiMultiplyScreenSqr1.Click += new EventHandler(tsmiMultiplyScreenSqr1_Click) + new EventHandler(RepaintHandler);
            tsmiMultiplyScreenSqr21.Click += new EventHandler(tsmiMultiplyScreenSqr21_Click) + new EventHandler(RepaintHandler);
            tsmiMultiplyScreenSqr31.Click += new EventHandler(tsmiMultiplyScreenSqr31_Click) + new EventHandler(RepaintHandler);

            //cmsLines
            tsmiLinesSync.Click += new EventHandler(tsmiLinesSync_Click) + new EventHandler(RepaintHandler);
            tsmiLinesSyncLines.Click += new EventHandler(tsmiLinesSyncLines_Click) + new EventHandler(RepaintHandler);
        }

        void tsmiBrightnessContrast_Click(object sender, EventArgs e) {
            NavigateFrames((i, j) => {
                Frames2D[i, j].DicomFlags.BrigthnessContrastOn = !Frames2D[i, j].DicomFlags.BrigthnessContrastOn;
            });
        }

        void tsmiMultiplyScreenSqr31_Click(object sender, EventArgs e) {
            MultiplyScreen(3, 1);
        }

        void tsmiMultiplyScreenSqr21_Click(object sender, EventArgs e) {
            MultiplyScreen(2, 1);
        }

        void tsmiMultiplyScreenSqr1_Click(object sender, EventArgs e) {
            MultiplyScreen();
        }

        void RepaintHandler(object sender, EventArgs e) {
            NavigateFrames((i, j) => {
                Frames2D[i, j].RepaintControl();
            });
        }

        void tsmiLinesSyncLines_Click(object sender, EventArgs e) {
            NavigateFrames((i, j) => {
                Frames2D[i, j].DicomFlags.LinesSyncMode = !Frames2D[i, j].DicomFlags.LinesSyncMode;
            });
        }

        void tsmiLinesSync_Click(object sender, EventArgs e) {
            ToolStripMenuItem tsmi = sender as ToolStripMenuItem;

            NavigateFrames((i, j) => {
                Frames2D[i, j].DicomFlags.Synchronization = !Frames2D[i, j].DicomFlags.Synchronization;
            });
        }

        void tsmiRotate180Degrees_Click(object sender, EventArgs e) {
            NavigateFrames((i, j) => {
                Frames2D[i, j].RotationScale += 180.0f;
                Frames2D[i, j].DicomFlags.RotationTransform = true;
            });
        }

        void tsmiRotate90Degrees_Click(object sender, EventArgs e) {
            NavigateFrames((i, j) => {
                Frames2D[i, j].RotationScale += 90.0f;
                Frames2D[i, j].DicomFlags.RotationTransform = true;
            });
        }

        void tsmiRotateDefault_Click(object sender, EventArgs e) {
            NavigateFrames((i, j) => {
                Frames2D[i, j].RotationScale = 0.0f;
                Frames2D[i, j].RepaintControl();
                Frames2D[i, j].DicomFlags.RotationTransform = true;
            });
        }

        void tsmiMagnification3x_Click(object sender, EventArgs e) {
            NavigateFrames((i, j) => {
                Frames2D[i, j].MagnificationScale[0] = 3.0f;
                Frames2D[i, j].MagnificationScale[1] = 3.0f;
                Frames2D[i, j].DicomFlags.ScalingTransform = true;
            });
        }

        void tsmiMagnification2x_Click(object sender, EventArgs e) {
            NavigateFrames((i, j) => {
                Frames2D[i, j].MagnificationScale[0] = 2.0f;
                Frames2D[i, j].MagnificationScale[1] = 2.0f;
                Frames2D[i, j].DicomFlags.ScalingTransform = true;
            });

        }

        void tsmiMagnification15x_Click(object sender, EventArgs e) {
            NavigateFrames((i, j) => {
                Frames2D[i, j].MagnificationScale[0] = 1.5f;
                Frames2D[i, j].MagnificationScale[1] = 1.5f;
                Frames2D[i, j].DicomFlags.ScalingTransform = true;
            });
        }

        void tsmiMagnification1x_Click(object sender, EventArgs e) {
            NavigateFrames((i, j) => {
                Frames2D[i, j].MagnificationScale[0] = 1.0f;
                Frames2D[i, j].MagnificationScale[1] = 1.0f;
                Frames2D[i, j].RepaintControl();
                Frames2D[i, j].DicomFlags.ScalingTransform = false;
            });
        }

        void tsmiAngle_Click(object sender, EventArgs e) {
            NavigateFrames((i, j) => {
                Frames2D[i, j].DicomFlags.AngleDrawing = true;
            });
        }

        void tsmiAutoDose_Click(object sender, EventArgs e) {
            NavigateFrames((i, j) => {
                Frames2D[i, j].DicomSeries.ChangeDose(SImageDoses.Dict["AutoContrast"][0], SImageDoses.Dict["AutoContrast"][1]);
            });
        }

        void tsmiBrainBaseDose_Click(object sender, EventArgs e) {
            NavigateFrames((i, j) => {
                Frames2D[i, j].DicomSeries.ChangeDose(SImageDoses.Dict["BrainBase"][0], SImageDoses.Dict["BrainBase"][1]);
            });
        }

        void tsmiBrainDose_Click(object sender, EventArgs e) {
            NavigateFrames((i, j) => {
                Frames2D[i, j].DicomSeries.ChangeDose(SImageDoses.Dict["Brain"][0], SImageDoses.Dict["Brain"][1]);
            });
        }

        void tsmiSkullDose_Click(object sender, EventArgs e) {
            NavigateFrames((i, j) => {
                Frames2D[i, j].DicomSeries.ChangeDose(SImageDoses.Dict["Skull"][0], SImageDoses.Dict["Skull"][1]);
            });
        }

        void tsmiLungDose_Click(object sender, EventArgs e) {
            NavigateFrames((i, j) => {
                Frames2D[i, j].DicomSeries.ChangeDose(SImageDoses.Dict["Lung"][0], SImageDoses.Dict["Lung"][1]);
            });
        }

        void tsmiMediastinumDose_Click(object sender, EventArgs e) {
            NavigateFrames((i, j) => {
                Frames2D[i, j].DicomSeries.ChangeDose(SImageDoses.Dict["Mediastinum"][0], SImageDoses.Dict["Mediastinum"][1]);
            });
        }

        void tsmiAbdomenDose_Click(object sender, EventArgs e) {
            NavigateFrames((i, j) => {
                Frames2D[i, j].DicomSeries.ChangeDose(SImageDoses.Dict["Abdomen"][0], SImageDoses.Dict["Abdomen"][1]);
            });
        }

        void tsmiBonesDose_Click(object sender, EventArgs e) {
            NavigateFrames((i, j) => {
                Frames2D[i, j].DicomSeries.ChangeDose(SImageDoses.Dict["Bones"][0], SImageDoses.Dict["Bones"][1]);
            });
        }

        void tsmiColored_Click(object sender, EventArgs e) {
            NavigateFrames((i, j) => {
                Frames2D[i, j].DicomSeries.Colorize();
            });
        }

        void tsmiBWNegative_Click(object sender, EventArgs e) {
            NavigateFrames((i, j) => {
                Frames2D[i, j].DicomSeries.Invert();
            });
        }

        void btnReset_Click(object sender, EventArgs e) {
            MultiplyScreen();

            NavigateFrames((i, j) => {
                Frames2D[i, j].Reset();
            });
        }

        void btnReport_Click(object sender, EventArgs e) {
            return;
        }

        void tsmiMeasurePolygon_Click(object sender, EventArgs e) {
            NavigateFrames((i, j) => {
                Frames2D[i, j].DicomFlags.PolygonDrawing = true;
            });
        }

        void tsmiMeasureDistance_Click(object sender, EventArgs e) {
            NavigateFrames((i, j) => {
                Frames2D[i, j].DicomFlags.MeasurementLinesDrawing = true;
            });
        }

        void btnLens_Click(object sender, EventArgs e) {
            NavigateFrames((i, j) => {
                Frames2D[i, j].DicomFlags.LensOn = !Frames2D[i, j].DicomFlags.LensOn;
            });
        }

        void btnInfo_Click(object sender, EventArgs e) {
            NavigateFrames((i, j) => {
                Frames2D[i, j].DicomFlags.DrawPatientInfo = !Frames2D[i, j].DicomFlags.DrawPatientInfo;
            });
        }

        void btnAutoPlay_Click(object sender, EventArgs e) {
            Task.Factory
                 .StartNew(() => NavigateFrames((i, j) => Frames2D[i, j].AutoPlaySeries()));
        }

        #endregion

    }
}
