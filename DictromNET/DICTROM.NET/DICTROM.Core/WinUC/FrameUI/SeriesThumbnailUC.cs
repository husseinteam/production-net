﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DICTROM.Core.Model.DicomFrame;
using DICTROM.Core.Model.DicomEntity;
using DICTROM.Core.Model.Event;
using System.Drawing;
using System.Threading.Tasks;

namespace DICTROM.Core.WinUC.FrameUI {
    public partial class SeriesThumbnailUC : ToolStripSplitButton {
        public SeriesThumbnailUC() {
            InitializeComponent();
            InitializeCustomComponent();
        }

        public SeriesThumbnailUC(IContainer container)
            : this() {
            container.Add(this);

            InitializeComponent();
            InitializeCustomComponent();
        }
        #region ctor
        public SeriesThumbnailUC(ref TDicomSeries series, ToolStrip pToolStrip)
            : this() {
            ThumbnailSeries = series;

            Size = new Size(pToolStrip.Size.Height + DropDownButtonWidth, pToolStrip.Size.Height);
            ImageCount = series.DicomPlanes.Length;

            this.Parent = pToolStrip;
            Thumb = ThumbnailSeries.DicomPlanes[0];
        }
        private void InitializeCustomComponent() {
            Event = new EventThumbnail();
        }
        #endregion

        #region override
        protected override void OnPaint(PaintEventArgs e) {

            base.OnPaint(e);
            Graphics dc = e.Graphics;

            //Thumb.DrawThumbnail(dc, 0, 0, Size.Width, Size.Height);
            if (this.Image == null)
                this.Image = Thumb.GetSnapshot(dc, Size);

            if (ThumbnailFont == null)
                ThumbnailFont = new Font(this.Font.FontFamily, 15f, FontStyle.Bold, GraphicsUnit.Pixel);
            dc.DrawString(
                  ImageCount.ToString(),
                  ThumbnailFont,
                  Brushes.White,
                  Point.Empty);

        }
        protected override void OnMouseDown(MouseEventArgs e) {
            base.OnMouseDown(e);
            DoDragDrop(ThumbnailSeries, DragDropEffects.All);
        }
        #endregion

        #region property
        public TDicomFrame Thumb { get; set; }
        public int ImageCount { get; private set; }
        public TDicomSeries ThumbnailSeries { get; private set; }
        public EventThumbnail Event { get; set; }
        #endregion

        #region field
        private Font ThumbnailFont;
        #endregion
    }
}
