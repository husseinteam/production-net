﻿using System;
using System.Drawing;
using System.Windows.Forms;
using DICTROM.Core.Model;
using DICTROM.Core.Model.DicomEntity;
using DICTROM.Core.Assistant;
using System.Threading;
using System.Collections.Generic;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using DICTROM.Core.Model.Geometry;
using DICTROM.Core.Model.Flags;
using DICTROM.Core.MPH;
using System.Threading.Tasks;

namespace DICTROM.Core.WinUC.FrameUI {
    public partial class DicomFrameUC : UserControl {

        #region ctor
        public DicomFrameUC() {
            InitializeComponent();
            InitializeCustomComponents();
        }
        private void InitializeCustomComponents() {
            this.Dock = DockStyle.Fill;

            Reset();

            SynchronizationRequested +=
                new SynchronizationRequestedDelegate(DicomFrameUC_SynchronizationRequested);
            LineSynchronization += new LineSynchronizationDelegate(DicomFrameUC_LineSynchronization);

            AssignImagePanelEvents();
        }
        private void SupplyFrameBuffer() {

            for (int index = 0; index < DicomSeries.DicomPlanes.Length; index++) {
                DicomSeries.DicomPlanes[index].PreloadPixelBuffer();
            }
        }
        #endregion

        #region public methods
        public void SetSeries(TDicomSeries series) {

            DicomSeries = series;
            pnlMainImage.SetParent(this);

            Position = 0;
            SupplyFrameBuffer();

        }
        public void AutoPlaySeries() {

            PlaySeries();

        }
        public void RepaintControl() {

            pnlMainImage.Invalidate();

        }
        public void Reset() {
            _Position = 0;

            HorizontalMargin = 10;
            VerticalMargin = 10;

            AnglePolygon = new TPolygon();
            PolygonVertices = new List<Point>();
            MeasurementLines = new List<TLine>();

            MagnificationScale = new float[] { 1.0f, 1.0f };
            RotationScale = 0.0f;
            LastScaleTransformation = Point.Empty;

            DicomFlags = new TDicomFlags();
            DicomFlags.DrawPatientInfo = true;
        }
        #endregion

        #region event handlers
        private void DicomFrameUC_SynchronizationRequested(float syncFactor) {
            Position = (int)((float)DicomSeries.DicomPlanes.Length * syncFactor);
            RepaintControl();
        }
        private void DicomFrameUC_LineSynchronization(TDicomPlane proxyLane) {

            Projection = CurrentDicomPlane.TransformProjection(proxyLane);
            RepaintControl();

        }
        #endregion

        #region private methods
        private void DisplayMainImage(PaintEventArgs e) {
            if (DicomSeries == null)
                return;
            Graphics dc = e.Graphics;

            //Apply Transformations
            if (DicomFlags.ScalingTransform) {
                dc.ScaleTransform(MagnificationScale[0], MagnificationScale[1]);

                dc.TranslateTransform(LastScaleTransformation.X, LastScaleTransformation.Y);
                if (MouseButtons == MouseButtons.Left)
                    dc.TranslateTransform(CursorLocator.X - _CursorOffset.X, CursorLocator.Y - _CursorOffset.Y);
            }
            if (DicomFlags.RotationTransform) {
                dc.TranslateTransform(pnlMainImage.Width / 2, pnlMainImage.Height / 2);
                dc.RotateTransform(RotationScale);
                dc.TranslateTransform(-pnlMainImage.Width / 2, -pnlMainImage.Height / 2);
            }

            //Display base
            //dc.TranslateTransform(this.AutoScrollPosition.X, this.AutoScrollPosition.Y);
            CurrentDicomPlane.ScaleImage(this.Size);
            var shift = new Point((this.Width - CurrentDicomPlane.ScaledSize.Width) / 2, 0);
            CurrentDicomPlane.Display(dc, ref shift);

            //Draw patient info
            if (DicomFlags.DrawPatientInfo)
                DrawPatientInfo(dc, (float)pnlMainImage.Height / (float)Screen.PrimaryScreen.WorkingArea.Height);

            //Draw Line if available
            if (DicomFlags.PolygonDrawing)
                DrawMeasurementPolygon(dc);
            if (DicomFlags.MeasurementLinesDrawing)
                DrawMeasurementLines(dc);
            if (DicomFlags.AngleDrawing)
                DrawAngles(dc);

            //magnification
            if (DicomFlags.LensOn && MouseButtons == System.Windows.Forms.MouseButtons.Left)
                DrawLens(dc);

            //sync
            if (DicomFlags.LinesSyncMode)
                DrawSynchronizationLines(dc);
        }
        private void DrawSynchronizationLines(Graphics dc) {
            if (Projection.Empty)
                return;

            Projection.SetScale(dc.ClipBounds.Size);
            Projection.Draw(dc, new Pen(Brushes.Yellow, 2.0f));
            Projection.Clear();
        }
        private void AdjustDose(MouseEventArgs e) {
            float deltaX = e.X - _CursorOffset.X;
            float deltaY = e.Y - _CursorOffset.Y;

            float contrast = deltaY / Height;
            CurrentDicomPlane.AdjustImageAttributes(deltaX / Width, 1.0f + contrast);
        }
        private delegate void repaintDelegate();
        private void DrawPatientInfo(Graphics dc, float scaleFactor) {

            //check lblPatientInfo
            lblPatientName.Text = String.Format("{0} Modalite: {1} Tarih: {2}",
                     DicomSeries.LocatorFrame.AnalyzeHeader.PatientsName,
                     CurrentDicomPlane.AnalyzeHeader.Modality,
                     CurrentDicomPlane.AnalyzeHeader.StudyDate.ToShortDateString());
            lblPatientName.Visible = true;


            //draw on patient info graphics object
            Font f = new Font(new FontFamily(GenericFontFamilies.SansSerif), 11.0f * scaleFactor, FontStyle.Regular);
            Brush b = Brushes.White;
            Point p = new Point(HorizontalMargin, VerticalMargin);
            int verticalSpacing = (int)dc.MeasureString("d", f).Height;
            int horizontalSpacing = (int)dc.MeasureString("_", f).Width;

            //Draw left pane
            dc.DrawString(CurrentDicomPlane.PatientsNameFormat, f, b, p);

            p.Offset(0, verticalSpacing);
            dc.DrawString(CurrentDicomPlane.PatientSexFormat, f, b, p);

            p.Offset(0, verticalSpacing);
            dc.DrawString(CurrentDicomPlane.PatientIDFormat, f, b, p);

            p.Offset(0, verticalSpacing);
            dc.DrawString(CurrentDicomPlane.AccessionNumberFormat, f, b, p);

            //bottom left
            p = new Point(HorizontalMargin, pnlMainImage.Height - VerticalMargin - verticalSpacing);
            dc.DrawString(CurrentDicomPlane.SliceInformationFormat, f, b, p);

            p.Offset(0, -verticalSpacing);
            dc.DrawString(CurrentDicomPlane.DoseInfoFormat, f, b, p);

            p.Offset(0, -verticalSpacing);
            dc.DrawString(String.Format("X/Y : {0}/{1}", CursorLocator.X, CursorLocator.Y), f, b, p);

            //InstutionName, StudyDate, StudyTime
            //Calculate new p:
            SizeF[] sizes = new SizeF[]
            {
                dc.MeasureString(CurrentDicomPlane.InstitutionNameFormat, f),
                dc.MeasureString(CurrentDicomPlane.StudyDateFormat, f),
                dc.MeasureString(CurrentDicomPlane.StudyTimeFormat, f),
                dc.MeasureString(CurrentDicomPlane.PatientPositionFormat, f),
            };


            //Draw right panel:
            p = new Point(pnlMainImage.Width - (Int32)sizes[0].Width, VerticalMargin);
            dc.DrawString(CurrentDicomPlane.InstitutionNameFormat, f, b, p);

            p = new Point(pnlMainImage.Width - (Int32)sizes[1].Width, VerticalMargin + verticalSpacing);
            dc.DrawString(CurrentDicomPlane.StudyDateFormat, f, b, p);

            p = new Point(pnlMainImage.Width - (Int32)sizes[2].Width, VerticalMargin + verticalSpacing * 2);
            dc.DrawString(CurrentDicomPlane.StudyTimeFormat, f, b, p);

            p = new Point(pnlMainImage.Width - (Int32)sizes[3].Width, pnlMainImage.Height - VerticalMargin - verticalSpacing);
            dc.DrawString(CurrentDicomPlane.PatientPositionFormat, f, b, p);
        }
        private void DrawMeasurementPolygon(Graphics dc) {

            float distance = 0.0f;
            for (int i = 1; i < PolygonVertices.Count; i++) {
                distance += DrawDistance(dc, PolygonVertices[i - 1], PolygonVertices[i]);

                //draw measurement string
                string ms = distance + " mm.";
                dc.DrawString(ms, this.Font, Brushes.Yellow, PolygonVertices[i]);
            }

        }
        private void DrawMeasurementLines(Graphics dc) {

            for (int i = 0; i < MeasurementLines.Count; i++) {
                float distance = DrawDistance(dc, MeasurementLines[i].Start.ToPoint(), MeasurementLines[i].End.ToPoint());

                //draw measurement string
                string ms = distance + " mm.";
                dc.DrawString(ms, this.Font, Brushes.Yellow, MeasurementLines[i].End.ToPoint());
            }

        }
        private float DrawDistance(Graphics dc, Point offset, Point next) {

            PointF p1 = CurrentDicomPlane.TranslatePoint(ref offset);
            PointF p2 = CurrentDicomPlane.TranslatePoint(ref next);
            float distance =
                SGraphicsAssistant.ComputeDistance(p1, p2,
                    (float)CurrentDicomPlane.AnalyzeHeader.PixelSpacing[0],
                    (float)CurrentDicomPlane.AnalyzeHeader.PixelSpacing[1]);
            //check if measurement exists
            if (distance == 0.0f)
                return distance;

            //draw pen
            Pen pen = new Pen(Brushes.Yellow);
            dc.DrawLine(pen, offset, next);

            //return result
            return distance;

        }
        private void DrawAngles(Graphics dc) {

            AnglePolygon.Draw(dc);

        }
        private void DrawLens(Graphics dc) {

            int min = Math.Min(pnlMainImage.Width, pnlMainImage.Height) / 2;
            Size lensSize = new Size(min, min);
            Point lensLocation = new Point(CursorLocator.X - lensSize.Width / 2, CursorLocator.Y - lensSize.Height / 2);
            Rectangle drawRect = new Rectangle(lensLocation, lensSize);
            Point lensCenter = new Point(CursorLocator.X, CursorLocator.Y);
            //GraphicsState state = dc.Save();

            dc.SetClip(drawRect, CombineMode.Intersect);
            dc.TranslateTransform(-lensCenter.X, -lensCenter.Y);
            dc.ScaleTransform(2.0f, 2.0f);

            Point shift = new Point((this.Width - CurrentDicomPlane.ScaledSize.Width) / 2, 0);
            CurrentDicomPlane.Display(dc, ref shift);
            //dc.Restore(state);

        }
        private void PlaySeries() {

            for (Position = 0; Position < ImageCount; ++Position) {
                Thread.Sleep(50);
                RepaintControl();
            }
            Position = 0;

        }
        #endregion

        #region events
        public delegate void SynchronizationRequestedDelegate(float syncFactor);
        public delegate void LineSynchronizationDelegate(TDicomPlane syncRect);

        private static SynchronizationRequestedDelegate SynchronizationRequestedHandle;
        private static LineSynchronizationDelegate LineSynchronizationHandle;

        public static event SynchronizationRequestedDelegate SynchronizationRequested {
            add { SynchronizationRequestedHandle += value; }
            remove { SynchronizationRequestedHandle -= value; }
        }
        public static event LineSynchronizationDelegate LineSynchronization {
            add { LineSynchronizationHandle += value; }
            remove { LineSynchronizationHandle -= value; }
        }

        public static void OnSynchronizationRequested(float syncFactor) {
            if (SynchronizationRequestedHandle != null)
                SynchronizationRequestedHandle(syncFactor);
        }
        public static void OnLineSynchronization(TDicomPlane syncRect) {
            if (LineSynchronizationHandle != null)
                LineSynchronizationHandle(syncRect);
        }
        #endregion

        #region property
        public Int32 ImageCount { get { return DicomSeries.DicomPlanes.Length; } }
        public TDicomPlane CurrentDicomPlane;
        public TDicomSeries DicomSeries { get; private set; }

        //scalars
        public float[] MagnificationScale { get; set; }
        public float RotationScale { get; set; }
        private TDicomRectangle Projection;
        private int VerticalMargin { get; set; }
        private int HorizontalMargin { get; set; }
        private Point CursorLocator {
            get { return _CursorLocator; }
            set { _CursorLocator = value; }
        }
        private List<Point> PolygonVertices { get; set; }
        private TPolygon AnglePolygon { get; set; }
        private List<TLine> MeasurementLines { get; set; }
        private Point LastScaleTransformation { get; set; }
        private Int32 Position {
            get { return _Position; }
            set {
                lock (this) {
                    if (value < 0)
                        _Position = 0;
                    else if (value >= ImageCount)
                        _Position = ImageCount - 1;
                    else
                        _Position = value;
                    CurrentDicomPlane = DicomSeries.DicomPlanes[_Position];
                }
            }
        }

        //flags
        public TDicomFlags DicomFlags { get; set; }
        #endregion

        #region field
        private Int32 _Position;
        private Point _CursorOffset;
        private Point _CursorLocator;
        #endregion
    }
}
