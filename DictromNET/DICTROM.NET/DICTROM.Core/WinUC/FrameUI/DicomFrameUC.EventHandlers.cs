﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using DICTROM.Core.Resource;
using DICTROM.Core.Model.Geometry;
using DICTROM.Core.WinForms.ViewerUI;
using DICTROM.Core.WinForms.FrameUI;

namespace DICTROM.Core.WinUC.FrameUI {
    partial class DicomFrameUC {
        #region Image Panel Handlers

        private void AssignImagePanelEvents() {
            pnlMainImage.Paint += new PaintEventHandler(pnlMainImage_Paint);
            pnlMainImage.Resize += new EventHandler(pnlMainImage_Resize) + new EventHandler((s, e) => RepaintControl());
            pnlMainImage.MouseClick += new MouseEventHandler(pnlMainImage_MouseClick) + new MouseEventHandler((s, e) => RepaintControl());
            pnlMainImage.MouseMove += new MouseEventHandler(pnlMainImage_MouseMove) + new MouseEventHandler((s, e) => RepaintControl());
            pnlMainImage.MouseDoubleClick += new MouseEventHandler(pnlMainImage_MouseDoubleClick) + new MouseEventHandler((s, e) => RepaintControl());
            pnlMainImage.MouseWheel += new MouseEventHandler(pnlMainImage_MouseWheel) + new MouseEventHandler((s, e) => RepaintControl());
            pnlMainImage.MouseDown += new MouseEventHandler(pnlMainImage_MouseDown) + new MouseEventHandler((s, e) => RepaintControl());
            pnlMainImage.MouseUp += new MouseEventHandler(pnlMainImage_MouseUp) + new MouseEventHandler((s, e) => RepaintControl());
        }

        void pnlMainImage_MouseUp(object sender, MouseEventArgs e) {
            if (e.Button == MouseButtons.Left) {
                if (DicomFlags.ScalingTransform) {
                    Point curTransformation = new Point(_CursorLocator.X - _CursorOffset.X, _CursorLocator.Y - _CursorOffset.Y);
                    LastScaleTransformation = new Point(LastScaleTransformation.X + curTransformation.X,
                        LastScaleTransformation.Y + curTransformation.Y);

                    Cursor.Current = Cursors.Arrow;
                }
            }
        }

        void pnlMainImage_MouseDown(object sender, MouseEventArgs e) {
            _CursorOffset = e.Location;
            if (e.Button == MouseButtons.Left) {
                if (DicomFlags.MeasurementLinesDrawing) {
                    MeasurementLines.Add(new TLine(new TPoint(e.Location), new TPoint(e.Location)));
                }
                if (DicomFlags.AngleDrawing)
                    AnglePolygon.HandleVertex(new TPoint(e.Location));
            }
        }

        void pnlMainImage_MouseClick(object sender, MouseEventArgs e) {
            if (e.Button == MouseButtons.Left) {
                if (DicomFlags.PolygonDrawing)
                    PolygonVertices.Add(e.Location);
            } else if (e.Button == MouseButtons.Right) {
                //reset lines if drawing
                if (DicomFlags.PolygonDrawing)
                    PolygonVertices.Clear();

                //reset multilines on right click
                if (DicomFlags.MeasurementLinesDrawing)
                    MeasurementLines.Clear();

                //reset angles on right click
                if (DicomFlags.AngleDrawing)
                    AnglePolygon.Clear();
            }
        }

        public void pnlMainImage_MouseWheel(object sender, MouseEventArgs e) {
            if (e.Delta > 0)
                Position = _Position - 1;
            else
                Position = _Position + 1;

            if (DicomFlags.Synchronization) {
                float syncFactor = (float)Position / (float)DicomSeries.DicomPlanes.Length;
                OnSynchronizationRequested(syncFactor);
            }

            //sync
            if (DicomFlags.LinesSyncMode) {
                OnLineSynchronization(CurrentDicomPlane);
            }
        }

        void pnlMainImage_MouseDoubleClick(object sender, MouseEventArgs e) {
            bool isInnerControl = false;
            if (e.Button == System.Windows.Forms.MouseButtons.Right && e.Clicks == 2)
                DicomSeries.Invert();
            else if (Boolean.TryParse((FindForm().Tag ?? new object()).ToString(), out isInnerControl) == true) {
                if (isInnerControl == true)
                    return;
            } else if (e.Button == System.Windows.Forms.MouseButtons.Left && e.Clicks == 2) {
                using (TFrameForm frameForm = new TFrameForm()) {
                    if (KeyPressHandler == null)
                        KeyPressHandler = (s, args) => {
                            if (args.KeyChar == (char)27) {
                                Form f = (s as Form);
                                f.Close();
                            }
                        };

                    frameForm.KeyPress += new KeyPressEventHandler(KeyPressHandler);
                    frameForm.Tag = true;
                    frameForm.DisplayForm(this.DicomSeries);
                }
            }
        }

        void pnlMainImage_MouseMove(object sender, MouseEventArgs e) {
            CursorLocator = e.Location;
            if (e.Button == MouseButtons.Right) {
                //int vector = ((e.X - _CursorOffset.X) + (e.Y - _CursorOffset.Y)) / 2;
                //DicomSeries.AdjustBrigthness(vector);
            } else if (e.Button == MouseButtons.None) {
                if (DicomFlags.LensOn)
                    Cursor.Current = SCustomCursors.Zoom;
            } else if (e.Button == MouseButtons.Left) {
                if (DicomFlags.ScalingTransform) {
                    Cursor.Current = Cursors.Hand;
                }
                if (DicomFlags.MeasurementLinesDrawing)
                    MeasurementLines[MeasurementLines.Count - 1].End = new TPoint(e.Location);
                if (DicomFlags.AngleDrawing)
                    AnglePolygon.HandleVertex(new TPoint(e.Location), true);
                if (DicomFlags.BrigthnessContrastOn) {
                    Cursor.Current = SCustomCursors.BrigthnessContrast;
                    AdjustDose(e);
                } else
                    Cursor.Current = Cursors.Default;
            }
        }

        void pnlMainImage_Resize(object sender, EventArgs e) {
            //Scale
            if (DicomSeries == null)
                return;

            DicomSeries.ScaleImage(this.Size);
        }

        void pnlMainImage_Paint(object sender, PaintEventArgs e) {
            DisplayMainImage(e);
        }
        #endregion

        #region fields
        KeyPressEventHandler KeyPressHandler = null;
        #endregion
    }
}
