﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DICTROM.Core.Data.Model;
using DICTROM.Core.Model.Event;
using System.Windows.Forms;
using DICTROM.Core.Resource;
using DICTROM.Core.ProxyService.Managers;
using DICTROM.Core.Data.ConnectionParams;
using System.Data.SqlClient;
using DICTROM.Core.Model.DicomEntity;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using DICTROM.Core.WinUC.UCBase;
using DICTROM.Core.Data.Model.POCO;
using DICTROM.Core.Data.AccessLayer;
using System.Net;

namespace DICTROM.Core.WinUC.SettingsUI {
    public class TUserSettingsEH : AEventHolder<UCSettings> {
        #region ctor
        public TUserSettingsEH(UCSettings control)
            : base(control) {
        }
        #endregion

        #region private methods
        private void TriggerWCFServiceReStart() {
            //Trigger User Service
            try {
                ParentControl.btnStopWCFServer.PerformClick();
                ParentControl.btnStartWCFServer.PerformClick();
            } catch (Exception ex) {
                MessageBox.Show("Servis Başlatılamadı.\nHata Mesajı: " + ex.Message);
                SwitchUserAuthenticated(false);
                return;
            }

        }
        #endregion

        #region Handlers

        private void btnStartWCFServer_Click(object sender, EventArgs e) {

            if (ParentControl.SelfValidated()) {
                if (!SProxyManager.IsServerStarted()) {
                    StringBuilder error = new StringBuilder();
                    if (SProxyManager.StartService(
                        Environment.MachineName,
                        ParentControl.cmbIPList.SelectedItem.ToString(),
                        ParentControl.txtAET.Text, Convert.ToInt32(ParentControl.nudPort.Value), error)) {
                        ParentControl.lblWCFServerStatusInfo.Text = "Sunucu Aktif.";
                        ParentControl.OnServerActive(true);
                    } else {
                        MessageBox.Show(String.Format(
                            "Sunucu Başlatılamadı\n{0}", error));
                    }
                } else {
                    ParentControl.lblWCFServerStatusInfo.Text = "Sunucu Daha Önce Başlatılmış.";
                }
            } else {
                ParentControl.lblWCFServerStatusInfo.Text = "Form Eksik.";
            }

        }
        private void btnStopWCFServer_Click(object sender, EventArgs e) {

            if (SProxyManager.IsServerStarted()) {
                if (SProxyManager.StopService()) {
                    ParentControl.lblWCFServerStatusInfo.Text = "Sunucu Durduruldu.";
                } else {
                    ParentControl.lblWCFServerStatusInfo.Text = "İşlem Başarısız.";
                }
                ParentControl.OnServerActive(false);
            } else {
                ParentControl.lblWCFServerStatusInfo.Text = "Sunucu Daha Önce Durdurulmuş.";
            }

        }

        private void cmbIPList_SelectedIndexChanged(object sender, EventArgs e) {
            //if (ParentControl.cmbIPList.SelectedIndex == -1)
            //    return;
            //txtServerAlias_KeyDown(txtServerAlias, new KeyEventArgs(Keys.Enter));
        }

        #endregion

        #region switch methods
        private void SwitchUserAuthenticated(bool authenticated) {
            ParentControl.btnStartWCFServer.Enabled = authenticated;
            ParentControl.btnStopWCFServer.Enabled = !ParentControl.btnStartWCFServer.Enabled;
        }
        #endregion

        #region overrides
        protected override void HandleEvents() {

            ParentControl.btnStartWCFServer.Click += new EventHandler(btnStartWCFServer_Click);
            ParentControl.btnStopWCFServer.Click += new EventHandler(btnStopWCFServer_Click);

            ParentControl.cmbIPList.SelectedIndexChanged += new EventHandler(cmbIPList_SelectedIndexChanged);
        }

        protected override void InitControl() {
            if (SProxyManager.IsServerStarted()) {
                ParentControl.lblWCFServerStatusInfo.Text = "Sunucu Aktif.";
                ParentControl.OnServerActive(true);
            } else {
                ParentControl.lblWCFServerStatusInfo.Text = "Sunucu Çalışmıyor.";
                ParentControl.OnServerActive(false);
            }
        }
        #endregion

    }
}