using DICTROM.Core.Data.AccessLayer;
using DICTROM.Core.ProxyService.Managers;
using DICTROM.Core.WinUC.UCBase;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DICTROM.Core.WinUC.SettingsUI {
    public partial class UCSettings : UserControl, ISelfValidatingControl {

        #region ctors

        public UCSettings() {
            InitializeComponent();
        }

        #endregion

        #region public methods

        public void InitInstance() {
            this.Dock = DockStyle.Fill;
            InitEventHolder();
            InitLocalServerControls();
        }

        public int GetIPIndexFor(string ip) {
            for (int i = 0; i < cmbIPList.Items.Count; i++) {
                if (cmbIPList.Items[i].ToString() == ip)
                    return i;
            }
            return -1;
        }

        #endregion

        #region private methods
        private void InitEventHolder() {
            var holder = new TUserSettingsEH(this);
            holder.AssignEvents();
        }
        private void InitLocalServerControls() {

            var setting = SQRDAL.Remote.SelectAll().SingleOrDefault(st =>
                   st.ServerAlias == Environment.MachineName);
            if (setting != null) {
                cmbIPList.Items.Clear();
                cmbIPList.Items.Add(setting.ClientIP);
                txtServerAlias.Text = setting.ServerAlias;
                txtAET.Text = setting.AET;
                nudPort.Value = setting.Port;
                cmbIPList.SelectedIndex = 0;
            } else {
                cmbIPList.Items.Clear();
                IPAddress[] ipList = Dns.GetHostAddresses(Dns.GetHostName());
                cmbIPList.Items.AddRange(
                    (from ip in ipList
                     where ip.AddressFamily != System.Net.Sockets.AddressFamily.InterNetworkV6
                     select ip).ToArray<IPAddress>());
                txtServerAlias.Text = Environment.MachineName;
            }

        }
        #endregion

        #region ISelfValidatingControl Members

        public bool SelfValidated() {
            return !(
                cmbIPList.SelectedIndex == -1 ||
                String.IsNullOrEmpty(txtServerAlias.Text) ||
                String.IsNullOrEmpty(txtAET.Text) ||
                nudPort.Value < nudPort.Minimum || nudPort.Value > nudPort.Maximum
                );
        }
        #endregion


        public delegate void ServerActivatedDelegate(Boolean active);
        public event ServerActivatedDelegate ServerActivatedEvent;
        public void OnServerActive(Boolean active) {
            if (ServerActivatedEvent != null) {
                ServerActivatedEvent(active);
            }
        }
    }
}
