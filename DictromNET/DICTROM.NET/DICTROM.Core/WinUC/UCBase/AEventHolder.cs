﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DICTROM.Core.WinUC.UCBase {

    public abstract class AEventHolder<TParentControl>
        where TParentControl : ISelfValidatingControl {

        #region ctor
        public AEventHolder(TParentControl pControl) {
            ParentControl = pControl;
        }
        #endregion

        #region event management
        public void AssignEvents() {
            HandleEvents();
            InitControl();
        }
        #endregion

        #region abstracts
        protected abstract void HandleEvents();
        protected abstract void InitControl();
        #endregion

        #region ParentControl
        protected TParentControl ParentControl { get; set; }
        protected ISelfValidatingControl SelfValidatingControl {
            get { return ParentControl as ISelfValidatingControl; }
        }
        #endregion

    }

}
