﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DICTROM.Core.WinUC.UCBase
{
    public interface ISelfValidatingControl
    {
        bool SelfValidated();
    }
}
