﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using DICTROM.Core.Assistant;
using DICTROM.Core.Data.AccessLayer;
using DICTROM.Core.Resource;
using DICTROM.Core.ProxyService.Managers;
using System.Threading;
using DICTROM.Core.Model.DicomEntity;
using DICTROM.Core.ProxyService.ProxyData;
using System.Threading.Tasks;
using System.Net.Sockets;

namespace DICTROM.Core.WinUC.NetworkingUI {
    public partial class DicomQRConfig : UserControl {
        #region ctors
        public DicomQRConfig() {
            InitializeComponent();
            this.Dock = DockStyle.Fill;

            dgvClientConfiguration.AutoGenerateColumns = false;
            RefillDGV();
        }
        #endregion

        #region private methods
        private Boolean InsertInput() {
            IPAddress clientIP = null;
            bool inputOK = !String.IsNullOrEmpty(txtServerAlias.Text) &&
                !String.IsNullOrEmpty(txtAETitle.Text) &&
                !String.IsNullOrEmpty(txtClientIPAddress.Text) &&
                IPAddress.TryParse(txtClientIPAddress.Text, out clientIP);
            if (!inputOK)
                return false;

            if (Environment.MachineName != txtServerAlias.Text && txtClientIPAddress.Text != Dns.GetHostAddresses(Dns.GetHostName()).First(ip => ip.AddressFamily == AddressFamily.InterNetwork).ToString()) {
                return SQRDAL.Remote.Upsert(txtServerAlias.Text, clientIP, txtAETitle.Text, (int)nudPort.Value);
            } else
                return false;
        }
        private void RefillDGV() {
            dgvClientConfiguration.DataSource = SQRDAL.Remote.SelectAll();
        }

        #endregion

        #region event handlers
        private void btnAddClient_Click(object sender, EventArgs e) {
            var resp = InsertInput();
            if (!resp)
                MessageBox.Show("Girdiğiniz Değerleri Kontrol Ediniz.\nLütfen Uzak Sunucuya Ait Değerleri Giriniz.");
            RefillDGV();
        }
        private void dgvClientConfiguration_CellContentClick(object sender, DataGridViewCellEventArgs e) {
            if (e.ColumnIndex == 0) {
                //echo
                DataGridViewCellCollection cells;
                cells = dgvClientConfiguration.Rows[e.RowIndex].Cells;

                try {
                    //Check if loopback
                    IPAddress clientIP;
                    clientIP = IPAddress.Parse(cells["colClientIP"].Value.ToString());
                    if (clientIP.ToString() == cells["colServerAlias"].Value.ToString()) {
                        MessageBox.
                            Show(String.Format(
                            "Uzak Sunucu Adresi<{0}> Yerel Sunucu Adresinden farklı olmalıdır",
                            clientIP));
                        return;
                    }

                    SProxyManager.PingProxy(new TProxyData(
                        cells["colServerAlias"].Value.ToString(),
                        clientIP,
                        cells["colAET"].Value.ToString(),
                        Convert.ToInt32(cells["colPort"].Value.ToString())), (proxy, error) => {
                            if (proxy != null) {
                                var message = proxy.Connect(Environment.MachineName);
                                MessageBox.Show(message);
                            } else
                                MessageBox.Show(String.Format("Yankı Başarısız: {0}", error));
                        });
                    
                } catch (Exception ex) {
                    MessageBox.Show("Yankı Başarısız.\n" + ex.Message);
                }
            } else if (e.ColumnIndex == 5) {
                //delete
                string serverAlias;
                serverAlias = dgvClientConfiguration.Rows[e.RowIndex].Cells["colServerAlias"].Value.ToString();

                var resp = SQRDAL.Remote.DeleteRow(serverAlias);
                if (!resp)
                    MessageBox.Show("Silme Başarısız.");
            }
            RefillDGV();
        }
        private void btnSlider_Click(object sender, EventArgs e) {
            string flag = (sender as Button).Tag.ToString();
            if (flag == "open") {
                //collapse panel1
                (sender as Button).Tag = "closed";
                SplitterDistance = spcSlider.Height - btnSlider.Height;
            } else if (flag == "closed") {
                //expand panel1
                (sender as Button).Tag = "open";
                SplitterDistance = spcSlider.Height -
                    (pnlServerSettings.Height + btnSlider.Height);
            }
        }
        #endregion

        #region overrides
        protected override void OnResize(EventArgs e) {
            base.OnResize(e);
            SplitterDistance = pnlServerSettings.Width + btnSlider.Width;
        }
        #endregion

        #region properties
        private int SplitterDistance { set { spcSlider.SplitterDistance = value; } }
        #endregion

    }
}
