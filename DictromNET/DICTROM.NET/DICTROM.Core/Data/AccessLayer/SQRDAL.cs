﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

using System.Data;
using DICTROM.Core.Data.Model.POCO;

using System.Threading.Tasks;
using System.Data.SQLite;
using System.IO;

namespace DICTROM.Core.Data.AccessLayer {

    public static class SQRDAL {

        #region Ctor

        static SQRDAL() {

            InitDatabase();

        }

        #endregion

        #region Fields
        private static SQLiteConnection _Connection;
        private static SQLiteConnection Connection {
            get {
                _Connection = _Connection ?? new SQLiteConnection("Data Source=user.sqlite;Version=3;");
                if (_Connection.State != ConnectionState.Open) {
                    _Connection.Open();
                }
                return _Connection;
            }
        }
        private static String _Database;
        private static String Database {
            get {
                _Database = String.IsNullOrEmpty(_Database) ?
                    Path.Combine(Environment.CurrentDirectory, "user.sqlite") : _Database;
                return _Database;
            }
        }
        #endregion

        #region Remote

        public static class Remote {

            public static ICollection<DicomQR> SelectAll() {

                var sql = "select * from QR";
                var reader = new SQLiteCommand(sql, Connection).ExecuteReader(CommandBehavior.CloseConnection);

                var li = new List<DicomQR>();
                while (reader.Read()) {
                    li.Add(new DicomQR() {
                        AET = reader["AET"].ToString(),
                        ClientIP = reader["ClientIP"].ToString(),
                        Port = Int32.Parse(reader["Port"].ToString()),
                        ServerAlias = reader["ServerAlias"].ToString()
                    });
                }

                Connection.Close();
                return li.ToArray();

            }

            public static Boolean Insert(string alias, IPAddress ip, string aet, int port) {

                try {
                    var sql = String.Format("insert into QR (ServerAlias, ClientIP, AET, Port) values ('{0}', '{1}', '{2}', {3})", alias, ip, aet, port);
                    new SQLiteCommand(sql, Connection).ExecuteNonQuery();
                    Connection.Close();
                    return true;
                } catch {
                    return false;
                }

            }

            public static Boolean Upsert(string serverAlias, IPAddress iPAddress, string aet, int port) {

                if (!Contains(serverAlias)) {
                    return Insert(serverAlias, iPAddress, aet, port);
                } else {
                    return Update(serverAlias, iPAddress, aet, port);
                }
            }

            private static bool Update(string serverAlias, IPAddress iPAddress, string aet, int port) {

                var sql = String.Format("update QR SET ClientIP='{1}', AET='{2}', Port='{3}' where ServerAlias='{0}'", serverAlias, iPAddress, aet, port);
                try {
                    new SQLiteCommand(sql, Connection).ExecuteScalar();
                    return true;
                } catch (Exception) {
                    return false;
                } finally {
                    Connection.Close();
                }

            }

            public static Boolean Contains(string alias) {

                var sql = String.Format("select COUNT(*) from QR where ServerAlias='{0}'", alias);
                var count = Int32.Parse(new SQLiteCommand(sql, Connection).ExecuteScalar().ToString());
                Connection.Close();
                return count > 0;

            }

            public static Boolean DeleteRow(string alias) {

                var sql = String.Format("delete from QR where ServerAlias='{0}'", alias);
                try {
                    new SQLiteCommand(sql, Connection).ExecuteScalar();
                    return true;
                } catch (Exception) {
                    return false;
                } finally {
                    Connection.Close();
                }

            }

            public static Boolean Reset() {

                var sql = String.Format("DELETE FROM QR ");
                try {
                    new SQLiteCommand(sql, Connection).ExecuteNonQuery();
                    return true;
                } catch (Exception) {
                    return false;
                } finally {
                    Connection.Close();
                }
            
            }

        }

        #endregion

        #region Settings

        public static class Settings {

            public static ICollection<DicomSettings> SelectAll() {

                var sql = "select * from Settings";
                var reader = new SQLiteCommand(sql, Connection).ExecuteReader(CommandBehavior.CloseConnection);

                var li = new List<DicomSettings>();
                while (reader.Read()) {
                    li.Add(new DicomSettings() {
                        EntityBasePath = reader["EntityBasePath"].ToString()
                    });
                }

                Connection.Close();
                return li.ToArray();

            }

            public static Boolean Insert(String entityBasePath) {

                try {
                    var sql = String.Format("insert into Settings (EntityBasePath) values ('{0}')",
                        entityBasePath);
                    new SQLiteCommand(sql, Connection).ExecuteNonQuery();
                    Connection.Close();
                    return true;
                } catch {
                    return false;
                }

            }

            public static Boolean Upsert(String entityBasePath) {

                if (!Contains(entityBasePath)) {
                    return Insert(entityBasePath);
                } else {
                    return Update(entityBasePath);
                }
            }

            private static bool Update(String entityBasePath) {

                var sql = String.Format("update Settings SET EntityBasePath='{0}'", entityBasePath);
                try {
                    new SQLiteCommand(sql, Connection).ExecuteScalar();
                    return true;
                } catch {
                    return false;
                } finally {
                    Connection.Close();
                }

            }

            public static Boolean Contains(string entityBasePath) {

                var sql = String.Format("select COUNT(*) from Settings where EntityBasePath='{0}'",
                    entityBasePath);
                var count = Int32.Parse(new SQLiteCommand(sql, Connection).ExecuteScalar().ToString());
                Connection.Close();
                return count > 0;

            }

            public static Boolean DeleteRow(string entityBasePath) {


                var sql = String.Format("delete from Settings where EntityBasePath='{0}'",
                    entityBasePath);
                try {
                    new SQLiteCommand(sql, Connection).ExecuteScalar();
                    return true;
                } catch (Exception) {
                    return false;
                } finally {
                    Connection.Close();
                }
            }

            public static Boolean Reset() {

                var sql = String.Format("DELETE FROM Settings");
                try {
                    new SQLiteCommand(sql, Connection).ExecuteNonQuery();
                    return true;
                } catch (Exception) {
                    return false;
                } finally {
                    Connection.Close();
                }

            }

        }

        #endregion

        #region Database Wide

        public static void ResetDatabase() {

            Remote.Reset();
            Settings.Reset();
            InitDatabase();
        }

        #endregion

        #region Private

        private static void InitDatabase() {
            if (!File.Exists(Database)) {
                SQLiteConnection.CreateFile(Database);
            }
            try {
                var qrTable = "create table QR (ServerAlias varchar(128), ClientIP varchar(128), AET varchar(128), Port int)";
                var settingsTable = "create table Settings (EntityBasePath varchar(255))";
                new SQLiteCommand(qrTable, Connection).ExecuteNonQuery();
                new SQLiteCommand(settingsTable, Connection).ExecuteNonQuery();
            } catch {
            } finally {
                Connection.Close();
            }
        }

        #endregion

    }

}
