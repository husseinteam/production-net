﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using DICTROM.Core.Model.DataElement;
using DICTROM.Core.Model.DataSet;

namespace DICTROM.Core.Assistant
{
    
    public static class SFileAssistant
    {
        #region method
        /* machine order
          big	most significant	...	least significant	Similar to a number written on paper (in English)
          little least significant	...	most significant	Arithmetic calculation order (see carry propagation)
        */
        public static int ReadAttribute(FileStream dcmFile, TDicomCursor cursor)
        {
            UInt16 w1 = SByteAssistant.SwapBytes(
                ReadByte(dcmFile, cursor),
                ReadByte(dcmFile, cursor));
            UInt16 w2 = SByteAssistant.SwapBytes(
                ReadByte(dcmFile, cursor),
                ReadByte(dcmFile, cursor));
            return (int)(w1 << 16 | w2);
        }
        public static int ReadDataLength32(FileStream dcmFile, TDicomCursor cursor)
        {
            Byte[] bytes = ReadBytes(dcmFile, 4, ref cursor);
            return (int)(bytes[3] << 24 | bytes[2] << 16 | bytes[1] << 8 | bytes[0]);
        }
        public static UInt16 ReadDataLength16(FileStream dcmFile, TDicomCursor cursor)
        {
            Byte[] bytes = ReadBytes(dcmFile, 2, ref cursor);
            return (UInt16)(bytes[1] << 8 | bytes[0]);
        }
        public static UInt16 ReadU16Swapped(FileStream dcmFile, TDicomCursor cursor)
        {
            byte b1, b2;
            b1 = ReadByte(dcmFile, cursor);
            b2 = ReadByte(dcmFile, cursor);
            return SByteAssistant.SwapBytes(b1, b2);
        }
        public static int ReadU16(FileStream dcmFile, TDicomCursor cursor)
        {
            byte b1, b2;
            b1 = ReadByte(dcmFile, cursor);
            b2 = ReadByte(dcmFile, cursor);
            return (UInt16)(b2 << 8 | b1);
        }
        public static byte ReadByte(FileStream dcmFile, TDicomCursor cursor)
        {
            byte b = (byte)dcmFile.ReadByte();
            cursor.AccumulateData(new byte[] { b });
            return b;
        }
        public static string ReadString(FileStream dcmFile, int length, TDicomCursor cursor)
        {
            return ASCIIEncoding.ASCII.GetString(ReadBytes(dcmFile, length, ref cursor));
        }
        public static byte[] ReadBytes(Stream dcmStream, long length, ref TDicomCursor cursor)
        {
            byte[] bytes = new byte[length];
            dcmStream.Read(bytes, 0, bytes.Length);

            cursor.AccumulateData(bytes);
            return bytes;
        }
        public static byte[] ReadBytes(Stream dcmStream, long length)
        {
            byte[] bytes = new byte[length];

            dcmStream.Read(bytes, 0, bytes.Length);
            return bytes;
        }
        #endregion

    }
}
