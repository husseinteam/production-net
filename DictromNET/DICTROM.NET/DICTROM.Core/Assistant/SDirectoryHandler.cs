﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;

namespace DICTROM.Core.Assistant
{
    public static class SDirectoryHandler
    {
        #region externs
        [DllImport("kernel32.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.StdCall,
            SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool CopyFile(
          [MarshalAs(UnmanagedType.LPWStr)]string lpExistingFileName,
          [MarshalAs(UnmanagedType.LPWStr)]string lpNewFileName,
          [MarshalAs(UnmanagedType.Bool)]bool bFailIfExists);
        
        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool DeleteFile(string lpFileName);
        
        [DllImport("kernel32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        private static extern bool RemoveDirectory(string lpPathName);

        #endregion

        #region methods
        public static string[] GetMultitypeFiles(string topDir, string pattern, SearchOption searchOption)
        {
            List<string> files;
            string[] patterns;

            files = new List<string>();
            patterns = pattern.Split('|');

            for (int i = 0; i < patterns.Length; i++)
            {
                files.AddRange(Directory.GetFiles(topDir, patterns[i], searchOption));
            }

            return files.ToArray<string>();
        }
        public static bool CopyOne(string filePath, string destination)
        {
            if (!File.Exists(filePath))
                throw new Exception(filePath);
            string fileName = filePath.Substring(filePath.LastIndexOf('\\') + 1);
            string fileDest = Path.Combine(destination, fileName);
            return CopyFile(String.Concat(@"\\?\", filePath), String.Concat(@"\\?\", fileDest), true);
        }
        public static void CopyAll(string source, string destination)
        {
            // Check if the target directory exists, if not, create it.
            if (Directory.Exists(source) == false)
                Directory.CreateDirectory(source);

            foreach (string filePath in Directory.GetFiles(source))
            {
                if (!filePath.EndsWith(".dcm"))
                    continue;
                string fileName = filePath.Substring(filePath.LastIndexOf('\\') + 1);
                string fileDest = Path.Combine(destination, fileName);
                CopyFile(String.Concat(@"\\?\", filePath), String.Concat(@"\\?\", fileDest), true);
            }

            // Copy each subdirectory using recursion.
            foreach (string dirPath in Directory.GetDirectories(source))
            {
                string subDir = Path.Combine(
                    destination,
                    dirPath.Replace(Path.GetDirectoryName(dirPath), "").Replace("\\", ""));
                Directory.CreateDirectory(subDir);
                CopyAll(dirPath, subDir);
            }
        }
        public static void DeletePath(string path)
        {
            foreach (string file in Directory.GetFiles(path))
            {
                DeleteFile(String.Concat(@"\\?\", file));
            }

            foreach (string dir in Directory.GetDirectories(path))
            {
                DeletePath(dir);
            }
            RemoveDirectory(String.Concat(@"\\?\",path));
        }
        #endregion
    }
}
