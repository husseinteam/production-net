﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Diagnostics;
using DICTROM.Core.Model.DicomCursor;
using DICTROM.Core.DebugManagement.Asserter;
using DICTROM.Core.Model.Protocols;

namespace DICTROM.Core.Assistant
{
    public enum EEndianness
    {
        Little,
        Big,
    }
    public static class SByteAssistant
    {
        public static EEndianness Endianness = EEndianness.Little;

        #region method
        public static byte[] transfer(byte[] data)
        {
            return data;
        }
        public static uint transform(uint value)
        {
            return deserialize<uint>(serialize(value));
        }
        public static ushort transform(ushort value)
        {
            return deserialize<ushort>(serialize(value));
        }
        public static TTResult deserialize<TTResult>(byte[] data)
        {
            object result = null;
            
            if (typeof(TTResult) == typeof(uint))
                result = Endianness == EEndianness.Big ?
                    (uint)(data[0] | data[1] << 8 | data[2] << 16 | data[3] << 24) :
                    (uint)(data[0] | data[1] >> 8 | data[2] >> 16 | data[3] >> 24);
            else if (typeof(TTResult) == typeof(ushort))
                result = Endianness == EEndianness.Big ?
                    (ushort)(data[0] | data[1] << 8) :
                    (ushort)(data[0] | data[1] >> 8);
            else
                SAssert.assert(false, typeof(SByteAssistant));
            return (TTResult)Convert.ChangeType(result, typeof(TTResult));
        }
        public static byte[] serialize(uint input)
        {
            byte[] fragmsb, fraglsb;
            fragmsb = serialize((ushort)((input & (uint)0xFFFF0000) >> 16));
            fraglsb = serialize((ushort)(input & (uint)0x0000FFFF));
            return Endianness == EEndianness.Big ?
                new byte[] { fragmsb[0], fragmsb[1], fraglsb[0], fraglsb[1] } :
                new byte[] { fraglsb[0], fraglsb[1], fragmsb[0], fragmsb[1] };
        }
        public static byte[] serialize(ushort input)
        {
            byte fragmsb, fraglsb;
            fragmsb = (byte)((input & (ushort)0xFF00) >> 8);
            fraglsb = (byte)(input & (ushort)0x00FF);
            return Endianness == EEndianness.Big ?
                new byte[] { fragmsb, fraglsb } :
                new byte[] { fraglsb, fragmsb };
        }
        public static byte[] serialize(string str)
        {
            byte[] res = ASCIIEncoding.Default.GetBytes(str);
            return res;
        }
        private static byte[] GetBytes(ushort data)
        {
            return TransformBytes(System.BitConverter.GetBytes(data));
        }
        private static byte[] GetBytes(uint data)
        {
            return TransformBytes(System.BitConverter.GetBytes(data));
        }
        private static byte[] TransformBytes(byte[] bytes)
        {
            if (bytes.Length == 1)
                return bytes;
            SAssert.assert(bytes.Length % 2 == 0 && bytes.Length <= sizeof(long),
                typeof(SByteAssistant));
            byte[] bytes1, bytes2;
            if (Endianness == EEndianness.Little)
            {
                bytes1 = ArraySlice(bytes, bytes.Length / 2, bytes.Length / 2);
                bytes2 = ArraySlice(bytes, 0, bytes.Length / 2);
            }
            else
            {
                bytes1 = ArraySlice(bytes, 0, bytes.Length / 2);
                bytes2 = ArraySlice(bytes, bytes.Length / 2, bytes.Length / 2);
            }

            return ConcatenateArrays(TransformBytes(bytes1), TransformBytes(bytes2));
        }
        private static TTNumeric GetNumeric<TTNumeric>(byte[] bytes)
        {
            if (bytes.Length == 1)
                return (TTNumeric)Convert.ChangeType(bytes[0], typeof(TTNumeric));
            SAssert.assert(bytes.Length % 2 == 0 && bytes.Length <= sizeof(long),
                typeof(SByteAssistant));

            byte[] bytes1;
            byte[] bytes2;
            SByteAssistant.split(bytes, out bytes1, out bytes2);
            ulong ret1, ret2;
            if (Endianness == EEndianness.Little)
            {
                ret1 = (ulong)Convert.ChangeType(GetNumeric<TTNumeric>(bytes1), typeof(ulong));
                ret2 = (ulong)Convert.ChangeType(GetNumeric<TTNumeric>(bytes2), typeof(ulong));
            }
            else
            {
                ret1 = (ulong)Convert.ChangeType(GetNumeric<TTNumeric>(bytes2), typeof(ulong));
                ret2 = (ulong)Convert.ChangeType(GetNumeric<TTNumeric>(bytes1), typeof(ulong));
            }

            return (TTNumeric)Convert.ChangeType(ret2 << bytes1.Length | ret1, typeof(TTNumeric));
        }
        private static void split(byte[] bytes, out byte[] bytes1, out byte[] bytes2)
        {
            if (Endianness == EEndianness.Little)
            {
                bytes1 = ArraySlice(bytes, bytes.Length / 2, bytes.Length / 2);
                bytes2 = ArraySlice(bytes, 0, bytes.Length / 2);
            }
            else
            {
                bytes1 = ArraySlice(bytes, 0, bytes.Length / 2);
                bytes2 = ArraySlice(bytes, bytes.Length / 2, bytes.Length / 2);
            }
        }
        public static byte[] ArraySlice(byte[] bytes, long start, long delta)
        {
            byte[] res = new byte[delta];
            for (Int32 i = 0; i < delta; i++)
            {
                res[i] = bytes[start + i];
            }
            return res;
        }
        public static void ArrayCopy(byte[] bytes, Int32 sourceIndex, byte[] destination, long destinationIndex, long distance, ref long cursor)
        {
            Array.Copy(bytes, sourceIndex, destination, destinationIndex, distance);
            cursor += distance;
        }
        public static byte[] ConcatenateArrays(params byte[][] arrays)
        {
            int length = 0;
            for (int i = 0; i < arrays.GetLength(0); i++)
            {
                length += arrays[i].Length;
            }

            byte[] combinationArray = new byte[length];
            long cursor = 0;

            for (int i = 0; i < arrays.GetLength(0); i++)
            {
                ArrayCopy(arrays[i], 0, combinationArray, cursor, arrays[i].Length, ref cursor);
            }

            Debug.Assert(cursor == length);
            return combinationArray;
        }
        public static UInt32 SwapWords(UInt16 w1, UInt16 w2)
        {
            //little endian
            UInt16 first = w1, second = w2;

            //big endian
            if (Endianness == EEndianness.Big)
            {
                first = w2;
                second = w1;
            }

            return (UInt32)(second << 16 | first);
        }
        public static UInt16 SwapBytes(byte b1, byte b2)
        {
            //little endian
            byte first = b1, second = b2;

            //big endian
            if (Endianness == EEndianness.Big)
            {
                first = b2;
                second = b1;
            }

            return (UInt16)(second << 8 | first);
        }
        public static byte ConstructByte(BitArray ba)
        {
            string byt = "";
            for (Int32 i = 0; i < ba.Length; i++)
            {
                byt = ba.Get(i) ? "1" : "0" + byt;
            }
            return Convert.ToByte(byt);
        }
        public static string ConstructString(byte[] bytes)
        {
            string s = "";
            for (int i = 0; i < bytes.Length; i++)
            {
                s += ((char)bytes[i]).ToString();
            }
            return s;
        }
        public static void TransformBitArray(List<byte> pixelBytes, BitArray baPixelData)
        {
            for (Int32 i = 0; i < baPixelData.Length; i += 8)
            {
                BitArray allocatedBits = new BitArray(8);
                for (Int32 j = 0; j < 8; j++)
                {
                    if (j < 7)
                        allocatedBits.Set(j, baPixelData.Get(i + j));
                    else if (j == 7)
                    {
                        pixelBytes.Add(SByteAssistant.ConstructByte(allocatedBits));
                        allocatedBits.SetAll(false);
                    }
                    else if (j < 15)
                        allocatedBits.Set(j - 8, baPixelData.Get(i + j));
                    else if (j == 15)
                    {
                        pixelBytes.Add(SByteAssistant.ConstructByte(allocatedBits));
                        allocatedBits.SetAll(false);
                    }
                    else if (j < 23)
                        allocatedBits.Set(j - 16, baPixelData.Get(i + j));
                    else if (j == 23)
                    {
                        pixelBytes.Add(SByteAssistant.ConstructByte(allocatedBits));
                        allocatedBits.SetAll(false);
                    }
                    else if (j < 31)
                        allocatedBits.Set(j - 24, baPixelData.Get(i + j));
                    else if (j == 31)
                    {
                        pixelBytes.Add(SByteAssistant.ConstructByte(allocatedBits));
                        allocatedBits.SetAll(false);
                    }
                }
            }
        }
        #endregion
    }
}
