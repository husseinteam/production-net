﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DICTROM.Core.Model.DicomEntity;
using DICTROM.Extensions.GenericExtensions;
using System.Reflection;
using System.Threading.Tasks;

namespace DICTROM.Core.Assistant {
    public static class SReflectionAssistant {
        #region methods
        public static TTValue GetDicomGridColumnByIndex<TTValue>(object obj, Int32 index) {
            PropertyInfo[] pi = obj.GetType().GetProperties();
            for (int i = 0; i < pi.Length; i++) {
                object[] attrs = pi[i].GetCustomAttributes(typeof(TDicomGridColumnAttribute), false);
                if (attrs.Length == 0)
                    continue;
                TDicomGridColumnAttribute attr = attrs[0] as TDicomGridColumnAttribute;
                if (attr.ColumnIndex == index)
                    return (TTValue)Convert.ChangeType(pi[i].GetValue(obj, null), typeof(TTValue));
            }
            //return default(TTValue);
            throw new ArgumentException();
        }
        public static string[] GetDicomGridColumnNamesSorted(Type dataType) {
            List<TDicomGridColumnAttribute> collected = new List<TDicomGridColumnAttribute>();

            PropertyInfo[] pi = dataType.GetProperties();
            for (int i = 0; i < pi.Length; i++) {
                object[] attrs = pi[i].GetCustomAttributes(typeof(TDicomGridColumnAttribute), false);
                if (attrs.Length == 0)
                    continue;
                TDicomGridColumnAttribute attr = attrs[0] as TDicomGridColumnAttribute;
                //attr.AssignedValue = pi[i].GetValue(obj, null);
                collected.Add(attr);
            }

            collected.Sort(new Comparison<TDicomGridColumnAttribute>((attr1, attr2) =>
                attr1.ColumnIndex.CompareTo(attr2.ColumnIndex)));

            string[] colNames = new string[collected.Count];
            int iAttr = 0;
            collected.ForEach(new Action<TDicomGridColumnAttribute>((attr) => {
                colNames[iAttr++] = attr.ColumnName;
            }));

            return colNames;
        }
        public static object[] GetDicomGridColumnValuesSorted(TDicomStudy study) {

            var collected = new List<TDicomGridColumnAttribute>();

            foreach (var prop in study.GetType().GetProperties()) {
                var attr = prop.GetCustomAttribute<TDicomGridColumnAttribute>();
                if (attr == null) {
                    continue;
                }
                attr.AssignedValue = prop.GetValue(study, null);
                collected.Add(attr);
            }

            collected.Sort(new Comparison<TDicomGridColumnAttribute>((attr1, attr2) =>
                attr1.ColumnIndex.CompareTo(attr2.ColumnIndex)));

            var colValues = new List<Object>();

            collected.ForEach((attr) => colValues.Add(attr.AssignedValue));

            return colValues.ToArray();

        }
        #endregion
    }
}
