﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using DICTROM.Core.Model;
using DICTROM.Core.Model.Geometry;
using DICTROM.Core.Model.AnalyzeModel;

namespace DICTROM.Core.Assistant
{
    public static class SGraphicsAssistant
    {
        #region public method
        public static Size TranslateDisplay(Size displaySize, Size screenSize)
        {
            float factor = (float)screenSize.Height / (float)displaySize.Height;
            if (displaySize.Width > displaySize.Height)
                factor = (float)screenSize.Width / (float)displaySize.Width;

            int w = (int)(displaySize.Width * factor);
            int h = (int)(displaySize.Height * factor);

            return new System.Drawing.Size(w, h);
        }
        public static double ComputeAngleToXAxis(TLine line)
        {
            return Math.Atan(line.Slope);
        }
        public static double ComputeSweepAngle(TLine line1, TLine line2)
        {
            return Math.Atan(line1.Slope - line2.Slope / (1D + line1.Slope * line2.Slope));
        }
        public static float ComputeDistance(PointF p1, PointF p2, float rowspac = 1.0f, float colspac = 1.0f)
        {
            return (float)Math.Sqrt((p1.X - p2.X) * (p1.X - p2.X) * rowspac +
                (p1.Y - p2.Y) * (p1.Y - p2.Y) * colspac);

        }
        public static double CubicInterpolate(double[] p, double x)
        {
            return p[1] +
                0.5 * x * (p[2] - p[0] +
                x * (2.0 * p[0] - 5.0 * p[1] + 4.0 * p[2] - p[3] +
                x * (3.0 * (p[1] - p[2]) + p[3] - p[0])));
        }

        #endregion
    }
}
