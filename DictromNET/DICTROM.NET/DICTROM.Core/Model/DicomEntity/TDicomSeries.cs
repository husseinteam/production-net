﻿using System;
using System.Linq;
using System.IO;
using System.Drawing;
using DICTROM.Core.Model.Geometry;
using System.Runtime.Serialization;
using DICTROM.Core.Data.Model;
using DICTROM.Core.Model.DicomFrame;
using DICTROM.Core.Resource;
using DICTROM.Core.Assistant;
using DICTROM.Core.Data.Model.POCO;
using System.Threading.Tasks;

namespace DICTROM.Core.Model.DicomEntity {
    [Serializable()]
    public class TDicomSeries : ISerializable {
        #region ctor
        public TDicomSeries(string seriesFolderPath) {
            //set dicom image path
            SeriesPath = seriesFolderPath;
            if (!Directory.Exists(SeriesPath))
                throw new Exception(SeriesPath + " not found.");
        }
        public void InitOneLevel() {
            string[] dicomFilePaths = SDirectoryHandler.GetMultitypeFiles(
                SeriesPath,
                SConstants.DicomFileSearchPattern,
                SearchOption.TopDirectoryOnly);

            if (dicomFilePaths.Length == 0)
                return;

            DicomPlanes = new TDicomPlane[dicomFilePaths.Length];

            //initialize dicom planes
            for (int index = 0; index < DicomPlanes.Length; index++) {
                DicomPlanes[index] = new TDicomPlane(dicomFilePaths[index]);
                DicomPlanes[index].InitDicomDataset();
            }

        }
        #endregion

        #region public methods
        public void Invert() {
            for (int i = 0; i < DicomPlanes.Length; i++) {
                DicomPlanes[i].Invert();
            }
        }
        public void Colorize() {
            for (int i = 0; i < DicomPlanes.Length; i++) {
                DicomPlanes[i].Colorize();
            }
        }

        public void MakeStrong(Byte gray, Int32 dose) {
            for (int i = 0; i < DicomPlanes.Length; i++) {
                DicomPlanes[i].MakeStrong(gray, dose);
            }
        }
        public void ScaleImage(Size sz) {

            for (int i = 0; i < DicomPlanes.Length; i++) {
                DicomPlanes[i].ScaleImage(sz);
            }

        }
        public void ChangeDose(decimal wCenter = 0, decimal wWidth = 0) {
            for (int i = 0; i < DicomPlanes.Length; i++) {
                DicomPlanes[i].ChangeDose(wCenter, wWidth);
            }
        }
        #endregion

        #region property
        public TDicomFrame LocatorFrame { get { return DicomPlanes[0]; } }
        public TDicomPlane[] DicomPlanes;
        public string SeriesPath;
        #endregion

        #region ISerializable Members

        public void GetObjectData(SerializationInfo info, StreamingContext context) {
            info.AddValue("DicomPlanes", DicomPlanes);
            info.AddValue("SeriesPath", SeriesPath);
        }
        private TDicomSeries(SerializationInfo info, StreamingContext ctx) {
            DicomPlanes = (TDicomPlane[])info.GetValue("DicomPlanes", typeof(TDicomPlane[]));
            SeriesPath = info.GetString("SeriesPath");
        }
        #endregion
    }
}
