﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Runtime.InteropServices;
using DICTROM.Core.Model.DicomFrame;

namespace DICTROM.Core.Model.Geometry
{
    [Serializable()]
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct TPoint
    {
        #region ctor
        public TPoint(params double[] bases)
        {
            Bases = new double[bases.Length];
            for (int i = 0; i < Bases.Length; i++)
            {
                Bases[i] = bases[i];
            }
            Gray = 0;
        }
        public TPoint(Point point)
            : this(point.X, point.Y)
        { }
        public TPoint(TVoxel vx)
            : this(vx.X, vx.Y, vx.Z) { }
        #endregion

        #region public methods
        public PointF ToPointF()
        {
            return new PointF((float)Bases[0], (float)Bases[1]);
        }
        public Point ToPoint()
        {
            return new Point((Int32)Bases[0], (Int32)Bases[1]);
        }
        public TPoint Transform(TVector tv)
        {
            if (Bases.Length != tv.EndPoint.Bases.Length)
                throw new Exception();

            TPoint p = TPoint.Empty;

            for (int i = 0; i < Bases.Length; i++)
            {
                p.Bases[i] = Bases[i] + tv.EndPoint.Bases[i];
            }

            return p;
        }
        public TPoint Clone()
        {
            TPoint p = TPoint.Empty;

            for (int i = 0; i < p.Bases.Length; i++)
            {
                p.Bases[i] = Bases[i];
            }

            return p;
        }
        #endregion

        #region operators
        public static bool operator ==(TPoint p1, TPoint p2)
        {
            return p1.Equals(p2);
        }
        public static bool operator !=(TPoint p1, TPoint p2)
        {
            return !p1.Equals(p2);
        }
        public static TVector operator -(TPoint p1, TPoint p2)
        {
            if (p1.Bases.Length != p2.Bases.Length)
                throw new Exception();

            TPoint r = TPoint.Empty;
            for (int i = 0; i < p1.Bases.Length; i++)
            {
                r.Bases[i] = p1.Bases[i] - p2.Bases[i];
            }

            return new TVector(r);
        }
        #endregion

        #region override
        public override bool Equals(object obj)
        {
            TPoint other = (TPoint)obj;
            for (int i = 0; i < Bases.Length; i++)
            {
                if (Bases[i] != other.Bases[i])
                    return false;
            }
            return true;
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        public override string ToString()
        {
            return String.Format("x: {0} y: {1} z: {2}", X, Y, Z);
        }
        #endregion

        #region properties
        public double[] Bases;
        public double X { get { return Bases[0]; } set { Bases[0] = value; } }
        public double Y { get { return Bases[1]; } set { Bases[1] = value; } }
        public double Z { get { return Bases[2]; } set { Bases[2] = value; } }
        public byte Gray;
        public static TPoint Empty { get { return new TPoint(0, 0, 0); } }
        #endregion

    }
}
