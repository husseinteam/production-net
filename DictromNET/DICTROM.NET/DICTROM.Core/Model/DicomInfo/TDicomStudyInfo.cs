﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DICTROM.Core.Model.DicomInfo {
    public class TDicomStudyInfo {

        #region ctor
        public TDicomStudyInfo() {

        }

        public DateTime StudyDate { get; set; }
        public DateTime StudyTime { get; set; }

        #endregion

    }
}
