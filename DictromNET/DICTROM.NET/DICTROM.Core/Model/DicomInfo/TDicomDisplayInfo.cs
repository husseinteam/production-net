﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DICTROM.Core.Model.DicomInfo {
    public class TDicomDisplayInfo {

        #region ctor
        public TDicomDisplayInfo() {

        }

        public UInt32 Columns { get; set; }
        public UInt32 Rows { get; set; }
        public UInt32 HighBit { get; set; }
        public Int32 ImagePosition { get; set; }
        public Int32 PixelDataSize { get; set; }
        public Int32 PixelRepresentation { get; set; }
        public Double SliceThickness { get; set; }
        public Int32 WindowCenter { get; set; }
        public Int32 WindowWidth { get; set; }
        public decimal[] ImageOrientationPatient { get; set; }
        public decimal[] ImagePositionPatient { get; set; }
        public decimal[] PixelSpacing { get; set; }

        public byte[] GetPixelDataAtOnce() {
            throw new NotImplementedException();
        }

        #endregion

    }
}
