﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DICTROM.Core.Model.Event
{
    #region delegates
    public delegate void ThumbnailClickDelegate(int index);
    public delegate void ThumbnailDragDelegate(int index);
    #endregion

    public class EventThumbnail
    {
        #region handler
        public ThumbnailClickDelegate ThumbnailClickHandler;
        public ThumbnailDragDelegate ThumbnailDragHandler;
        #endregion

        #region event
        public event ThumbnailClickDelegate ThumbnailClick
        {
            add { ThumbnailClickHandler += value; }
            remove { ThumbnailClickHandler -= value; }
        }
        public event ThumbnailDragDelegate ThumbnailDrag
        {
            add { ThumbnailDragHandler += value; }
            remove { ThumbnailDragHandler -= value; }
        }
        #endregion

        #region on
        public void OnThumbnailClick(int index)
        {
            if (ThumbnailClickHandler != null)
                ThumbnailClickHandler(index);
        }
        public void OnThumbnailDrag(int index)
        {
            if (ThumbnailDragHandler != null)
                ThumbnailDragHandler(index);
        }
        #endregion
    }
}
