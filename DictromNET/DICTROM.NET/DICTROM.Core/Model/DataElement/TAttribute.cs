﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using DICTROM.Core.Assistant;
using DICTROM.Core.Resource;
using DICTROM.Core.Model.DataSet;

namespace DICTROM.Core.Model.DataElement
{
    public struct TAttribute
    {
        #region ctors
        public TAttribute(FileStream dcmFile, ref TDicomCursor cursor)
        {
            Tag = SFileAssistant.ReadAttribute(dcmFile, cursor);
            GroupNumber = (UInt16)(Tag >> 16);
            ElementNumber = (UInt16)(Tag & 0x0000FFFF);

            string strVR = SFileAssistant.ReadString(dcmFile, 2, cursor);

            VR = SVRAssistant.ParseVRString(strVR);

            if (
                VR == EVR.OB ||
                VR == EVR.OW ||
                VR == EVR.OF ||
                VR == EVR.SQ ||
                VR == EVR.UT ||
                VR == EVR.UN
                )
            {
                //explicit vr
                if (SFileAssistant.ReadU16(dcmFile, cursor) != 0x0000)
                    throw new Exception();
                ValueLength =
                    SFileAssistant.ReadDataLength32(dcmFile, cursor);
            }
            else if (VR != EVR.Implicit)
            {
                //explicit vr
                ValueLength =
                    SFileAssistant.ReadDataLength16(dcmFile, cursor); ;
            }
            else  //implicit vr
            {
                //seek 2 back to reuse datalength space
                dcmFile.Seek(-2, SeekOrigin.Current);
                cursor.Advance(-2);
                ValueLength =
                    SFileAssistant.ReadDataLength32(dcmFile, cursor);
            }
        }
        #endregion

        #region property
        public int Tag;
        public UInt16 GroupNumber;
        public UInt16 ElementNumber;
        public int ValueLength;
        public EVR VR;
        #endregion

    }
}
