﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DICTROM.Core.Model.DataSet
{
    public enum ETransferSyntax
    {
        JpegLossless,
        Jpeg8bitLossy,
        Jpeg12bitLossy,
        Bitmap,
        NotSet,
    }
}
