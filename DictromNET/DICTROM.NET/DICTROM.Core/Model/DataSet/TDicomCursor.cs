﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DICTROM.Core.Model.DataSet
{
    public class TDicomCursor
    {
        #region ctor
        public TDicomCursor(long datasize)
        {
            Data = new byte[datasize];
            Position = 0;
        }
        #endregion

        #region public methods
        public void AccumulateData(byte[] data)
        {
            if (Position + data.LongLength > Data.LongLength)
                throw new Exception();
            Array.Copy(data, 0, Data, Position, data.LongLength);
            Position += data.LongLength;
        }
        public void Advance(long delta)
        {
            if (Position + delta > Data.LongLength)
                throw new Exception();
            Position += delta;
        }
        public void Reset()
        {
            Position = 0;
            Data = new byte[Data.LongLength];
        }
        #endregion

        #region properties
        public byte[] Data;
        public long Position { get; private set; }
        public bool EOF
        {
            get { return Position == Data.LongLength; }
        }
        #endregion

    }
}
