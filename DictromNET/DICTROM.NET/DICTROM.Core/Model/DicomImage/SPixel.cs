﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using DICTROM.Core.Assistant;
using System.Drawing;
using System.Windows.Forms;

namespace DICTROM.Core.Model
{
    public static class SPixel
    {
        #region public static get
        public static byte Get(byte[] pixel, int pixelSize, UInt32 highBit, bool twosComplement,
            double colMax, double colMin, bool colorsInverted)
        {
            double range = (colMax - colMin);
            if (range < 1) range = 1;
            double factor = 255.0D / range;

            if (pixelSize == 2)
            {
                //16 bit pixel format

                UInt16 pixelMask = (UInt16)(UInt16.MaxValue >> (Int32)(sizeof(UInt16) * 8 - highBit - 1));
                byte gray;

                UInt16 pixel16 = ParsePixel16(pixel, twosComplement);
                pixel16 &= pixelMask;

                if (pixel16 <= colMin)
                    gray = 0;
                else if (pixel16 >= colMax)
                    gray = 255;
                else
                    gray= (byte)((pixel16 - colMin) * factor);

                gray = (colorsInverted) ? (byte)(255 - gray) : gray;
                return gray;
            }
            else if (pixelSize == 1)
            {
                Byte pixelMask = (Byte)(Byte.MaxValue >> (Int32)(sizeof(Byte) * 8 - highBit - 1));
                
                byte gray;

                Byte pixel8 = pixel[0];
                pixel8 &= pixelMask;

                if (pixel8 <= colMin)
                    gray= 0;
                else if (pixel8 >= colMax)
                    gray= 255;
                else
                    gray= (byte)((pixel8 - colMin) * factor);
                
                gray = (colorsInverted) ? (byte)(255 - gray) : gray;
                return gray;
            }
            throw new Exception();
        }

        public static byte Luminosity(Int32 r, Int32 g, Int32 b)
        {
            //r,g,b .30, .59, .11
            //0.21 R + 0.71 G + 0.07 B
            return (byte)(r * 0.21f + g * 0.71f + b * 0.07);
        }
        public static void ToRGB(ref byte gray, out byte r, out byte g, out byte b)
        {
            //r,g,b .30, .59, .11
            //0.21 R + 0.71 G + 0.07 B
            r = (byte)(gray * 0.21);
            g = (byte)(gray * 0.71);
            b = (byte)(gray * 0.07);

            //gray = (byte)(r * .3 + g * .59 + b * .11);
        }
        #endregion

        #region private method
        private static UInt16 ParsePixel16(byte[] pixel, bool twosComplement)
        {
            Int16 pixel16 = System.BitConverter.ToInt16(pixel, 0);
            if (twosComplement)
                return (UInt16)(pixel16 - UInt16.MinValue);
            return (UInt16)pixel16;
        }
        //luminance index
        //private static void SetRGB565(UInt16 pixel16)
        //{
        //    //pixel is little endian

        //    //RGB565
        //    UInt16 red_mask = 0xF800;
        //    UInt16 green_mask = 0x7E0;
        //    UInt16 blue_mask = 0x1F;

        //    byte red = (byte)((pixel16 & red_mask) >> 11);
        //    byte green = (byte)((pixel16 & green_mask) >> 5);
        //    byte blue = (byte)((pixel16 & blue_mask));

        //    //To 256wide range
        //    red <<= 3;
        //    green <<= 2;
        //    blue <<= 3;

        //    R = red; G = green; B = blue;
        //}
        //private void SetRGB555(UInt16 pixel16)
        //{
        //    //pixel is little endian

        //    //RGB555
        //    UInt16 red_mask = 0x7C00;
        //    UInt16 green_mask = 0x3E0;
        //    UInt16 blue_mask = 0x1F;

        //    byte red = (byte)((pixel16 & red_mask) >> 10);
        //    byte green = (byte)((pixel16 & green_mask) >> 5);
        //    byte blue = (byte)((pixel16 & blue_mask));

        //    ////To 256wide range
        //    R = (byte)((red << 3));
        //    G = (byte)((green << 3));
        //    B = (byte)((blue << 3));
        //}
        //private void SetRGB332(Byte pixel8)
        //{
        //    //RGB:332
        //    byte red_mask = 0xe0;
        //    byte green_mask = 0x1c;
        //    byte blue_mask = 0x3;

        //    R &= red_mask;
        //    G &= green_mask;
        //    B &= blue_mask;

        //    R <<= 5;
        //    G <<= 5;
        //    B <<= 6;
        //}
        
        #endregion
    }
}
