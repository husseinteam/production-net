﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DICTROM.Core.DebugManagement.Asserter;
using DICTROM.Core.Assistant;
using DICTROM.Core.Model.DicomMessage.Types;

namespace DICTROM.Core.Model.DicomCursor
{
    public struct TCursor
    {
        #region ctor
        public TCursor(byte[] data)
        {
            this._position = 0;
            this._data = new TBinary(data); 
        }
        #endregion

        #region methods
        public long advance(int delta = 1)
        {
            this._position += delta;
            return this.position;
        }
        public TBinary slice(int delta, bool pop = true)
        {
            SAssert.assert(delta >= 0 && this._position + delta <= this.data.length, typeof(TCursor));
            TCursor c = this;
            byte[] res = new byte[delta];
            for (Int32 i = 0; i < delta; i++)
            {
                res[i] = this.data.sequence[this._position + i];
            }
            if (pop)
                advance(delta);
            return new TBinary(res);
        }
        #endregion

        #region operator overloads
        public static bool operator ==(TCursor d1, TCursor d2)
        {
            return d1.position == d2.position;
        }
        public static bool operator !=(TCursor d1, TCursor d2)
        {
            return d1.position != d2.position;
        }
        public static bool operator ==(TCursor d1, long n)
        {
            return d1.position == n;
        }
        public static bool operator !=(TCursor d1, long n)
        {
            return d1.position != n;
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            return obj is TCursor ? this.position == ((TCursor)(obj)).position : false;
        }
        #endregion

        #region props
        private TBinary _data;
        public TBinary data { get { return _data; } }
        public int position { get { return _position; } }
        private int _position;
        #endregion
    }
}
