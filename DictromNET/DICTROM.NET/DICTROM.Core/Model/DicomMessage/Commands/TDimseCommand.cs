﻿using System;
using System.Collections.Generic;
using System.Linq;
using DICTROM.Core.Model.DataSet;
using System.Net.Sockets;
using DICTROM.Core.Model.Protocols;
using DICTROM.Core.Assistant;
using DICTROM.Core.Model.DicomCursor;
using DICTROM.Core.Model.DicomMessage.Enumerations;
using DICTROM.Core.Model.DicomMessage.Types;
using DICTROM.Core.DebugManagement.Asserter;
using System.Net;
using System.IO;
using System.Text;

namespace DICTROM.Core.Model.DicomMessage.Commands
{
    [Serializable]
    public class TDimseCommand : List<TItem>, IAllocatable
    {
        #region ctor
        public TDimseCommand(params TItem[] items)
        {
            SAssert.assert(items.Any<TItem>((it) => it.tag.commandCode == ECommandCode.CommandGroupLength),
                typeof(TDimseCommand));
            this.AddRange(items);
            this.setGroupLength();
        }
        public TDimseCommand(params ECommandCode[] skeleton)
        {
            SAssert.assert(skeleton.Any<ECommandCode>((cc) => cc == ECommandCode.CommandGroupLength),
                typeof(TDimseCommand));
            for (int i = 0; i < skeleton.Length; i++)
            {
                this.Add(new TItem(new TTag(skeleton[i]), TBinary.none));
            }
        }
        #endregion

        #region methods
        public TDimseCommand queryServer(params ECommandCode[] skeleton)
        {
            //IPAddress host = IPAddress.Parse("87.106.65.167");
            IPAddress host = IPAddress.Parse("127.0.0.1");
            IPEndPoint hostep = new IPEndPoint(host, 114);
            Socket sock = new Socket(AddressFamily.InterNetwork,
                   SocketType.Stream, ProtocolType.Tcp);
            sock.Connect(hostep);
            sock.Send(binary.sequence);
            byte[] rcv = new byte[4096];
            sock.Receive(rcv);
            sock.Shutdown(SocketShutdown.Both);
            return null;
        }
        private void setGroupLength()
        {
            uint length = 0;
            for (int i = 0; i < Count; i++)
            {
                if (this[i].tag.commandCode == ECommandCode.CommandGroupLength)
                    continue;
                length += (uint)this[i].binary.sequence.Length;
            }
            this[ECommandCode.CommandGroupLength].setValue(new TBinary(TBinary.serialize<uint>(length)));
        }
        #endregion

        #region this overload
        public TItem this[ECommandCode cc]
        {
            get
            {
                return (from TItem co in this
                        where co.tag.commandCode == cc
                        select co).FirstOrDefault<TItem>();
            }
            set
            {
                for (int i = 0; i < this.Count; i++)
                {
                    if (this[i].tag.commandCode == cc)
                    {
                        this[i] = value;
                        return;
                    }
                }
            }
        }
        #endregion

        #region props
        public TDicomDataset dataset = null;
        public long length
        {
            get
            {
                long l = 0;
                for (int i = 0; i < this.Count; i++)
                {
                    if (this[i].tag.commandCode != ECommandCode.CommandGroupLength)
                        l += this[i].binary.sequence.Length;
                }
                return l;
            }
        }
        #endregion

        #region IAllocatable Members

        public IAllocatable allocate(ref TCursor cursor)
        {
            for (int i = 0; i < this.Count; i++)
            {
                this[i].allocate(ref cursor);
            }
            return this;
        }
        private TBinary _binary;
        public TBinary binary
        {
            get
            {
                if (_binary.Equals(TBinary.none))
                {
                    TBinary[] bArr = new TBinary[this.Count];
                    for (int i = 0; i < this.Count; i++)
                    {
                        bArr[i] = new TBinary(this[i].binary.sequence);
                    }
                    _binary = new TBinary(bArr);
                }
                return _binary;
            }
        }
        #endregion
    }
}
