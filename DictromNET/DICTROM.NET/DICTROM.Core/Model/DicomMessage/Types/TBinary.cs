﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DICTROM.Core.Model.DicomMessage.Enumerations;
using DICTROM.Core.Assistant;
using DICTROM.Core.DebugManagement.Asserter;

namespace DICTROM.Core.Model.DicomMessage.Types
{
    public struct TBinary
    {
        #region static generators
        public static TBinary generateFrom<TTValue>(TTValue value)
        {
            return new TBinary().internalize<TTValue>(value);
        }
        #endregion

        #region static members
        public static EEndianess Endianess = EEndianess.LittleEndian;
        public static byte[] serialize<TTValue>(TTValue value)
        {
            if (typeof(TTValue) == typeof(uint))
            {
                uint input = (uint)Convert.ChangeType(value, typeof(uint));
                byte[] fragmsb, fraglsb;
                fragmsb = serialize<ushort>((ushort)(input >> 16));
                fraglsb = serialize<ushort>((ushort)((input << 16) >> 16));
                return Endianess == EEndianess.BigEndian ?
                    new byte[] { fragmsb[0], fragmsb[1], fraglsb[0], fraglsb[1] } :
                    new byte[] { fraglsb[0], fraglsb[1], fragmsb[0], fragmsb[1] };
            }
            else if (typeof(TTValue) == typeof(ushort))
            {
                ushort input = (ushort)Convert.ChangeType(value, typeof(ushort));
                byte fragmsb, fraglsb;
                fragmsb = (byte)(input >> 8);
                fraglsb = (byte)((input << 8) >> 8);
                return Endianess == EEndianess.BigEndian ?
                    new byte[] { fragmsb, fraglsb } :
                    new byte[] { fraglsb, fragmsb };
            }
            else if (typeof(TTValue) == typeof(string))
            {
                string input = (string)Convert.ChangeType(value, typeof(string));
                return ASCIIEncoding.Default.GetBytes(input);
            }
            else if (typeof(TTValue) == typeof(byte[]))
            {
                byte[] input = (byte[])Convert.ChangeType(value, typeof(byte[]));
                if (input.Length == 2)
                    return new byte[] { input[1], input[0] };
                
                //split in parts
                byte[] frag1, frag2;
                frag1 = SByteAssistant.ArraySlice(input, 0, input.Length / 2);
                frag2 = SByteAssistant.ArraySlice(input, input.Length / 2, input.Length / 2);

                return combine(serialize<byte[]>(frag2), serialize<byte[]>(frag1));
            }
            else
                SAssert.assert(false, typeof(SByteAssistant));
            return null;
        }
        public static byte[] combine(params byte[][] arrRawData)
        {
            int length = 0;
            for (int i = 0; i < arrRawData.GetLength(0); i++)
            {
                length += arrRawData[i].Length;
            }

            byte[] combined = new byte[length];
            long cursor = 0;

            for (int i = 0; i < arrRawData.GetLength(0); i++)
            {
                Array.Copy(arrRawData[i], 0, combined, cursor, arrRawData[i].LongLength);
                cursor += arrRawData[i].LongLength;
            }

            SAssert.assert(cursor == length, typeof(TBinary));
            return combined;
        }
        public static void split<TTFragment>(byte[] sequence, out TTFragment frag1, out TTFragment frag2)
        {
            TBinary[] d2 = new TBinary[2];
            d2[0] = new TBinary(SByteAssistant.ArraySlice(sequence, 0, sequence.Length / 2));
            d2[1] = new TBinary(SByteAssistant.ArraySlice(sequence, sequence.Length / 2, sequence.Length / 2));

            frag1 = d2[0].deserialize<TTFragment>();
            frag2 = d2[1].deserialize<TTFragment>();
        }
        public static void split<TTComplement, TTFragment>(
            TTComplement value, out TTFragment frag1, out TTFragment frag2)
        {
            split<TTFragment>(serialize<TTComplement>(value), out frag1, out frag2);
        }
        #endregion

        #region ctors
        public TBinary(byte[] rawData)
        {
            this._sequence = rawData;
        }
        public TBinary(params TBinary[] binaries)
        {
            byte[][] arr2d = new byte[binaries.Length][];
            for (int i = 0; i < binaries.Length; i++)
            {
                arr2d[i] = binaries[i].sequence;
            }
            this._sequence = null;
            this._sequence = combine(arr2d); 
        }
        public TBinary(params byte[][] arrRawData)
        {
            this._sequence = null;
            this._sequence = combine(arrRawData);
        }
        #endregion

        #region methods
        public TTValue deserialize<TTValue>()
        {
            object result = null;

            if (typeof(TTValue) == typeof(uint))
            {
                SAssert.assert(this._sequence.Length == sizeof(uint), typeof(TBinary));
                result = Endianess == EEndianess.BigEndian ?
                    (uint)(sequence[0] | sequence[1] << 8 | sequence[2] << 16 | sequence[3] << 24) :
                    (uint)(sequence[0] | sequence[1] >> 8 | sequence[2] >> 16 | sequence[3] >> 24);
            }
            else if (typeof(TTValue) == typeof(ushort))
            {
                SAssert.assert(this._sequence.Length == sizeof(ushort), typeof(TBinary));
                result = Endianess == EEndianess.BigEndian ?
                    (ushort)(sequence[0] | sequence[1] << 8) :
                    (ushort)(sequence[0] | sequence[1] >> 8);
            }
            else
                SAssert.assert(false, typeof(SByteAssistant));
            return (TTValue)Convert.ChangeType(result, typeof(TTValue));
        }
        public void split<TTFragment>(out TTFragment frag1, out TTFragment frag2)
        {
            split<TTFragment>(this.sequence, out frag1, out frag2);
        }
        public TBinary extend(params TBinary[] binaries)
        {
            int size = 0;
            for (int i = 0; i < binaries.Length; i++)
            {
                size += binaries[i].length;
            }

            int cursor = 0;
            Array.Resize<byte>(ref this._sequence, size);
            for (int i = 0; i < binaries.Length; i++)
            {
                Array.Copy(binaries[i].sequence, 0, this._sequence, cursor, this.length);
                cursor += binaries[i].length;
            }

            return this;
        }
        private TBinary internalize<TTValue>(TTValue value)
        {
            this._sequence = serialize<TTValue>(value);
            return this;
        }
        #endregion

        #region override
        public override string ToString()
        {
            return Encoding.UTF8.GetString(this._sequence);
        }
        #endregion

        #region props
        public byte[] sequence { get { return _sequence; } }
        public int length { get { return _sequence.Length; } }
        private byte[] _sequence;
        public static TBinary none = new TBinary();
        #endregion

    }
}
