﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DICTROM.Core.Model.Protocols;
using DICTROM.Core.Assistant;
using System.Diagnostics;
using DICTROM.Core.Model.DicomCursor;
using DICTROM.Core.Model.DicomMessage.Enumerations;

namespace DICTROM.Core.Model.DicomMessage.Types
{
    public struct TTag : IAllocatable
    {
        #region ctor
        public TTag(ECommandCode cc)
        {
            _commandCode = cc;
            _binary = new TBinary();
            _gn = _en = 0;
            TBinary.split<uint, ushort>((uint)cc, out _gn, out _en);
        }
        #endregion

        #region methods
        public override string ToString()
        {
            ushort v = (ushort)commandCode;
            string frmt = String.Format("0x{{0:X{0}}}", sizeof(uint) * 2);
            return String.Format(frmt, v);
        }
        #endregion

        #region properties
        private ushort _gn;
        private ushort _en;
        public ushort groupNumber
        {
            get { return _gn; }
        }
        public ushort elementNumber
        {
            get { return _en; }
        }
        public uint code
        {
            get { return (uint)_commandCode; }
        }

        private ECommandCode _commandCode;
        public ECommandCode commandCode { get { return _commandCode; } }
        #endregion

        #region IAllocatable Members
        private TBinary _binary;
        public TBinary binary
        {
            get
            {
                if (_binary.Equals(TBinary.none))
                {
                    _binary = new TBinary(
                        TBinary.generateFrom<ushort>((ushort)this.groupNumber),
                        TBinary.generateFrom<ushort>((ushort)this.elementNumber)
                        );
                }
                return _binary;
            }
        }
        public IAllocatable allocate(ref TCursor cursor)
        {
            uint tag = cursor.slice(4).deserialize<uint>();
            return this;
        }
        #endregion
    }
}
