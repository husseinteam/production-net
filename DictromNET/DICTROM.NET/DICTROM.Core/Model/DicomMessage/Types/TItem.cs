﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DICTROM.Core.Model.Protocols;
using System.Diagnostics;
using DICTROM.Core.Model.DicomCursor;
using DICTROM.Core.Assistant;
using DICTROM.Core.Model.DicomMessage.Enumerations;

namespace DICTROM.Core.Model.DicomMessage.Types
{
    public class TItem : IAllocatable
    {
        #region ctor
        public TItem(TTag tag, TBinary value)
        {
            this.tag = tag;
            if (value.Equals(TBinary.none))
                return;
            this.value = value;
        }
        #endregion

        #region methods
        public override string ToString()
        {
            return this.tag.commandCode.ToString();
        }
        public void setValue(TBinary value)
        {
            this.value = value;
        }
        #endregion

        #region props
        public TTag tag { get; private set; }
        public int length { get { return this.value.length; } }
        public TBinary value { get; private set; }
        #endregion

        #region IAllocatable Members

        private TBinary _binary;
        public TBinary binary
        {
            get
            {
                if (_binary.Equals(TBinary.none))
                    _binary = new TBinary( 
                        this.tag.binary.sequence, 
                        TBinary.serialize<uint>((uint)this.length),
                        this.value.sequence
                    );
                return _binary;
            }
        }
        public IAllocatable allocate(ref TCursor cursor)
        {
            ECommandCode cc = ECommandCode.NaN;
            Enum.TryParse<ECommandCode>(cursor.slice(sizeof(uint)).ToString(), out cc); ;
            Debug.Assert(cc != ECommandCode.NaN);
            this.tag.allocate(ref cursor);
            int len = (int)cursor.slice(sizeof(uint)).deserialize<uint>();
            this.value = cursor.slice(len);
            return this;
        }
        #endregion
    }
}
