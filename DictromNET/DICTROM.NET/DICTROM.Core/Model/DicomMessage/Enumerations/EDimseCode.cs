﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DICTROM.Core.Model.DicomMessage.Enumerations
{
    public enum EDimseCode : ushort
    {
        CSTORERSP = 0x8001,
        CGETRQ = 0x0010,
        CGETRSP = 0x8010,
        CFINDRQ = 0x0020,
        CFINDRSP = 0x8020,
        CMOVERQ = 0x0021,
        CMOVERSP = 0x8021,
        CECHORQ = 0x0030,
        CECHORSP = 0x8030,
        NEVENTREPORTRQ = 0x0100,
        NEVENTREPORTRSP = 0x8100,
        NGETRQ = 0x0110,
        NGETRSP = 0x8110,
        NSETRQ = 0x0120,
        NSETRSP = 0x8120,
        NACTIONRQ = 0x0130,
        NACTIONRSP = 0x8130,
        NCREATERQ = 0x0140,
        NCREATERSP = 0x8140,
        NDELETERQ = 0x0150,
        NDELETERSP = 0x8150,
    }
}
