﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DICTROM.Core.Model.DicomMessage.Enumerations
{
    public enum ECommandCode : uint
    {
        CommandGroupLength = 0x00000000,
        AffectedSOPClassUID = 0x00000002,
        RequestedSOPClassUID = 0x00000003,
        CommandField = 0x00000100,
        MessageID = 0x00000110,
        MessageIDBeingRespondedTo = 0x00000120,
        MoveDestination = 0x00000600,
        Priority = 0x00000700,
        CommandDataSetType = 0x00000800,
        Status = 0x00000900,
        OffendingElement = 0x00000901,
        ErrorComment = 0x00000902,
        ErrorID = 0x00000903,
        AffectedSOPInstanceUID = 0x00001000,
        RequestedSOPInstanceUID = 0x00001001,
        EventTypeID = 0x00001002,
        AttributeIdentifierList = 0x00001005,
        ActionTypeID = 0x00001008,
        NumberOfRemainingSuboperations = 0x00001020,
        NumberOfCompletedSuboperations = 0x00001021,
        NumberOfFailedSuboperations = 0x00001022,
        NumberOfWarningSuboperations = 0x00001023,
        MoveOriginatorApplicationEntityTitle = 0x00001030,
        MoveOriginatorMessageID = 0x00000110,
        NaN = 0x9999FFFF,
    }
}
