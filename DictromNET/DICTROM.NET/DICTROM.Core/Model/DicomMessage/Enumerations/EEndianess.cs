﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DICTROM.Core.Model.DicomMessage.Enumerations
{
    public enum EEndianess
    {
        LittleEndian,
        BigEndian,
    }
}
