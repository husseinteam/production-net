﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DICTROM.Core.Model.DicomMessage.Types;

namespace DICTROM.Core.Model.DicomMessage.Managers
{
    public static class SUid
    {
        #region props
        public static TUID applicationContext = new TUID("1.2.840.10008.3.1.1.1");
        public static TUID transferSyntaxLittleEndian = new TUID("1.2.840.10008.1.2");
        public static TUID verification = new TUID("1.2.840.10008.1.1 ");
        #endregion
    }
}
