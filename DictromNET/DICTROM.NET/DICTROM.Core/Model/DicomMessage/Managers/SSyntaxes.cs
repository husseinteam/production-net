﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DICTROM.Core.Model.DicomMessage.Types;

namespace DICTROM.Core.Model.DicomMessage.Managers
{
    public static class SSyntaxes
    {
        public static class Abstract
        {
            #region props
            //public static TItem verification = new TItem(0x3000, new TUID("1.2.840.10008.1.1"));
            //public static TItem findStudy = new TItem(0x3000, new TUID("1.2.840.10008.5.1.4.1.2.2.1"));
            //public static TItem getStudy = new TItem(0x3000, new TUID("1.2.840.10008.5.1.4.1.2.2.3"));
            //public static TItem moveStudy = new TItem(0x3000, new TUID("1.2.840.10008.5.1.4.1.2.2.2"));
            //public static TItem findPatient = new TItem(0x3000, new TUID("1.2.840.10008.5.1.4.1.2.1.1"));
            //public static TItem getPatient = new TItem(0x3000, new TUID("1.2.840.10008.5.1.4.1.2.1.3"));
            //public static TItem movePatient = new TItem(0x3000, new TUID("1.2.840.10008.5.1.4.1.2.1.2"));
            //public static TItem findModalityWL = new TItem(0x3000, new TUID("1.2.840.10008.5.1.4.31"));
            //public static TItem crImageStorage = new TItem(0x3000, new TUID("1.2.840.10008.5.1.4.1.1.1"));
            //public static TItem ctImageStorage = new TItem(0x3000, new TUID("1.2.840.10008.5.1.4.1.1.2"));
            //public static TItem mrImageStorage = new TItem(0x3000, new TUID("1.2.840.10008.5.1.4.1.1.4"));
            //public static TItem usgImageStorage = new TItem(0x3000, new TUID("1.2.840.10008.5.1.4.1.1.6.1"));
            //public static TItem nmImageStorage = new TItem(0x3000, new TUID("1.2.840.10008.5.1.4.1.1.20"));
            //public static TItem petImageStorage = new TItem(0x3000, new TUID("1.2.840.10008.5.1.4.1.1.128"));
            //public static TItem printBasicFilmSession = new TItem(0x3000, new TUID("1.2.840.10008.5.1.1.1"));
            //public static TItem printBasicFilmBox = new TItem(0x3000, new TUID("1.2.840.10008.5.1.1.2"));
            //public static TItem printBasicGrayscaleImagebox = new TItem(0x3000, new TUID("1.2.840.10008.5.1.1.4"));
            #endregion
        }

        public static class Transfer
        {
            //public static TItem implicitLittleEndian = new TItem(0x4000, new TUID("1.2.840.10008.1.2"));
            //public static TItem explicitLittleEndian = new TItem(0x4000, new TUID("1.2.840.10008.1.2.1"));
            //public static TItem explicitBigEndian = new TItem(0x4000, new TUID("1.2.840.10008.1.2.2"));
            //public static TItem jpeg8LossyCompression = new TItem(0x4000, new TUID("1.2.840.10008.1.2.4.50"));
            //public static TItem jpeg12LossyCompression = new TItem(0x4000, new TUID("1.2.840.10008.1.2.4.51"));
            //public static TItem jpegLosslessCompression = new TItem(0x4000, new TUID("1.2.840.10008.1.2.4.57"));
            //public static TItem jpeglsLosslessCompression = new TItem(0x4000, new TUID("1.2.840.10008.1.2.4.80"));
            //public static TItem jpeglsNearLosslessCompression = new TItem(0x4000, new TUID("1.2.840.10008.1.2.4.81"));
            //public static TItem jpeg2000LosslessCompression = new TItem(0x4000, new TUID("1.2.840.10008.1.2.4.90"));
            //public static TItem jpeg2000LossyCompression = new TItem(0x4000, new TUID("1.2.840.10008.1.2.4.91"));
        }
    }
}
