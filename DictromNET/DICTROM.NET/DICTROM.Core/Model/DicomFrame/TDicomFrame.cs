﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using DICTROM.Core.Assistant;
using DICTROM.Core.Model.DataSet;
using DICTROM.Core.Resource;
using DICTROM.Core.Model.AnalyzeModel;
using DICTROM.Core.Model.DicomFrame;
using DICTROM.Core.Data.AccessLayer;
using System.Runtime.Serialization;

namespace DICTROM.Core.Model.DicomFrame {

    [Serializable()]
    public class TDicomFrame : TDicomDataset {

        #region ctors

        public TDicomFrame(string fullPath)
            : base(fullPath) {
        }

        #endregion

        #region image modification methods
        public void ScaleImage(Size size) {
            //fit fixed size into size
            ScaledSize = SGraphicsAssistant.TranslateDisplay(FixedSize, size);
        }
        public void Invert() {
            ColorsInverted = !ColorsInverted;
        }
        public void Colorize() {
            Colorized = !Colorized;
        }
        public void MakeStrong(Byte gray, Int32 dose) {
            Int32 top = gray + dose;
            while (top < 0)
                top += 256;

            float grayFactor = 256.0f / top;

            //byte R, G, B;

            for (int i = 0; i < _GrayColors.Length; i++) {
                byte p = _GrayColors[i];
                p = (byte)(p * grayFactor);
                _GrayColors[i] = p;
            }
        }
        public void ChangeDose(decimal wCenter = 0, decimal wWidth = 0) {
            ReloadLUT(wCenter, wWidth);
        }
        #endregion

        #region public methods
        public void Display(Graphics dc, ref Point shift) {
            if (!ValidDicomImage)
                return;

            dc.InterpolationMode = InterpolationMode.Bilinear;
            dc.DrawImage(LoadFromDB(dc),
                new Rectangle(shift.X, shift.Y, ScaledSize.Width, ScaledSize.Height),
                0, 0, AnalyzeHeader.Columns, AnalyzeHeader.Rows, GraphicsUnit.Pixel,
                GetImageAttributes());
        }
        public void DrawThumbnail(Graphics dc, int srcX, int srcY, int srcWidth, int srcHeight) {
            if (!ValidDicomImage)
                return;

            dc.DrawImage(LoadFromDB(dc), srcX, srcY, srcWidth, srcHeight);
        }
        public PointF TranslatePoint(ref Point point) {
            return new PointF() {
                X = (float)point.X * (float)FixedSize.Width / (float)ScaledSize.Width,
                Y = (float)point.Y * (float)FixedSize.Height / (float)ScaledSize.Height
            };
        }
        public void AdjustImageAttributes(float brightness = 1.0f, float contrast = 2.0f, float gamma = 1.0f) {
            //double brightness = 1.0f; // no change in brightness
            //double constrast = 2.0f; // twice the contrast
            //double gamma = 1.0f; // no change in gamma

            // create matrix that will brighten and contrast the image
            ImageColorMatrix = new float[][]{
                    new float[] {contrast, 0, 0, 0, 0}, // scale red
                    new float[] {0, contrast, 0, 0, 0}, // scale green
                    new float[] {0, 0, contrast, 0, 0}, // scale blue
                    new float[] {0, 0, 0, 1.0f, 0}, // don't scale alpha
                    new float[] {brightness, brightness, brightness, 0, 1}};

            Gamma = gamma;
        }
        public void PreloadPixelBuffer() {
            if (PixelDataBuffer == null)
                lock (this) {
                    PixelDataBuffer = AnalyzeImage.PixelDataElement.GetAllocation();
                }
        }
        public Image GetSnapshot(Graphics dc, Size size) {
            return LoadFromDB(dc).GetThumbnailImage(size.Width, size.Height, null, IntPtr.Zero);
        }
        #endregion

        #region private methods
        private Bitmap LoadFromDB(Graphics dc) {
            PreloadPixelBuffer();
            Bitmap bmp;
            bmp = LoadBitmapGrayScale(dc, PixelDataBuffer);
            bmp.SetResolution(300f, 300f);
            return bmp;
        }
        private Bitmap LoadBitmapGrayScale(Graphics dc, byte[] pixelData) {
            //locals
            Bitmap bitmap;
            byte[] allocation;
            int numPixels = AnalyzeHeader.Rows * AnalyzeHeader.Columns;

            //BitmapData specs
            bitmap = new Bitmap((Int32)AnalyzeHeader.Columns, (Int32)AnalyzeHeader.Rows, dc);
            allocation = pixelData;

            Rectangle rect = new Rectangle(0, 0, bitmap.Width, bitmap.Height);
            BitmapData bmpData = bitmap.LockBits(rect, ImageLockMode.ReadWrite, bitmap.PixelFormat);
            IntPtr ptr = bmpData.Scan0;

            int iSampler = 0;
            int iAlloc = 0;
            int colorCounter = 0;

            Int32 allocSampleSize = (Int32)(allocation.Length / numPixels);
            Int32 bmdSampleSize = bmpData.Stride / bitmap.Width;

            if (_GrayColors == null)
                _GrayColors = new byte[numPixels];
            else if (_GrayColors.Length != numPixels)
                Array.Resize<byte>(ref _GrayColors, (int)numPixels);

            unsafe
            {
                byte* row = (byte*)ptr;
                while (colorCounter < numPixels) {
                    //pixelsampling:
                    byte[] pixel = new byte[3];
                    pixel[0] = allocation[iAlloc++];
                    if (allocSampleSize > 1)
                        pixel[1] = allocation[iAlloc++];
                    if (allocSampleSize > 2)
                        pixel[2] = allocation[iAlloc++];

                    byte gray = SPixel.Get(pixel, allocSampleSize, AnalyzeHeader.HighBit,
                        AnalyzeHeader.PixelRepresentation == 1, ColumnMax, ColumnMin, ColorsInverted);
                    if (Colorized) {
                        byte r, g, b;
                        SPixel.ToRGB(ref gray, out r, out g, out b);

                        row[iSampler++] = r;
                        if (bmdSampleSize > 1)
                            row[iSampler++] = g;
                        if (bmdSampleSize > 2)
                            row[iSampler++] = b;
                        if (bmdSampleSize > 3)
                            row[iSampler++] = 255;
                    } else {
                        row[iSampler++] = gray;
                        if (bmdSampleSize > 1)
                            row[iSampler++] = gray;
                        if (bmdSampleSize > 2)
                            row[iSampler++] = gray;
                        if (bmdSampleSize > 3)
                            row[iSampler++] = 255;
                    }

                    _GrayColors[colorCounter++] = gray;
                }
            }

            bitmap.UnlockBits(bmpData);
            return bitmap;
        }
        private ImageAttributes GetImageAttributes() {
            ImageAttributes imgAttr = new ImageAttributes();

            if (ImageColorMatrix == null)
                return imgAttr;

            imgAttr.ClearColorMatrix();
            imgAttr.SetColorMatrix(new ColorMatrix(ImageColorMatrix), ColorMatrixFlag.Default, ColorAdjustType.Bitmap);
            imgAttr.SetGamma(Gamma, ColorAdjustType.Bitmap);

            return imgAttr;
        }
        private void ReloadLUT(decimal wCenter = 0, decimal wWidth = 0) {
            if (wCenter == 0 && wWidth == 0) {
                wCenter = AnalyzeHeader.WindowCenter;
                wWidth = AnalyzeHeader.WindowWidth;
            }

            ColumnMax = (double)(wCenter + wWidth / 2);
            ColumnMin = (double)(ColumnMax - (double)wWidth);
        }
        private void CreatePixelsFromJpegData() {
            throw new NotImplementedException();
            //int parsed32 = 0;
            //byte[] pixelBytes = new byte[2];
            //byte r, g, b;

            //if (Voxels == null)
            //    Voxels = new byte[Rows, Columns];

            //int c = 0;

            ////12 bits per pixel to 24 bit (the greatest common factor)
            ////ffff..fff
            //for (int x = 0; x < Voxels.GetLength(0); x++)
            //{
            //    for (int y = 0; y < Voxels.GetLength(1); y++)
            //    {
            //        byte[] pixel = new byte[2];
            //        pixel[0] = ByteAllocation[c++];
            //        pixel[1] = ByteAllocation[c++];

            //        Voxels[x, y] = SPixel.Get(pixel, HighBit, PixelRepresentation == 1, ref VOILUT);
            //    }
            //}

        }
        private TVoxel[,] InitVertexVoxels() {
            if (TransferSyntax != ETransferSyntax.Bitmap) {
                CreatePixelsFromJpegData();
                return null;
            }

            TVoxel[,] voxels = new TVoxel[2, 2];

            decimal[] cosines = AnalyzeHeader.ImageOrientationPatient;

            //pixel spacing
            decimal rowSpacing = AnalyzeHeader.PixelSpacing[1];
            decimal colSpacing = AnalyzeHeader.PixelSpacing[0];

            //cosine products
            decimal[] cps = new decimal[cosines.Length];
            cps[0] = cosines[0] * colSpacing; cps[3] = cosines[3] * rowSpacing;
            cps[1] = cosines[1] * colSpacing; cps[4] = cosines[4] * rowSpacing;
            cps[2] = cosines[2] * colSpacing; cps[5] = cosines[5] * rowSpacing;

            decimal[] imagePosition = AnalyzeHeader.ImagePositionPatient;

            //loop:
            int lastx, lasty;
            lastx = AnalyzeHeader.Columns - 1;
            lasty = AnalyzeHeader.Rows - 1;
            for (int i = 0; i < voxels.GetLength(0); i++)
                for (int j = 0; j < voxels.GetLength(1); j++) {
                    voxels[i, j] = new TVoxel();
                    voxels[i, j].X = (float)(i * lastx * cps[0] + j * lasty * cps[3] + imagePosition[0]);
                    voxels[i, j].Y = (float)(i * lastx * cps[1] + j * lasty * cps[4] + imagePosition[1]);
                    voxels[i, j].Z = (float)(i * lastx * cps[2] + j * lasty * cps[5] + imagePosition[2]);
                }

            return voxels;
        }
        private void AssignInitials() {
            //Formatted Values
            PatientsNameFormat = "Kimlik: " + AnalyzeHeader.PatientsName;
            PatientIDFormat = "ID: " + AnalyzeHeader.PatientID;
            PatientSexFormat = "Cinsiyet: " + AnalyzeHeader.PatientSex;
            InstitutionNameFormat = "Kurum: " + AnalyzeHeader.InstitutionName;
            StudyDateFormat = "Tarih: " + AnalyzeHeader.StudyDate.ToShortDateString();
            StudyTimeFormat = "Saat: " + AnalyzeHeader.StudyTime.ToShortTimeString();
            PatientPositionFormat = "Pozisyon: " + AnalyzeHeader.PatientPosition;
            AccessionNumberFormat = "Erişim(Acc) No: " + AnalyzeHeader.AccessionNumber;
            SliceInformationFormat = String.Format("Sıra(Pos): {0} Kalınlık(ST): {1}", AnalyzeHeader.ImagePosition, AnalyzeHeader.SliceThickness);
            DoseInfoFormat = String.Format("W{0} / C{1}", AnalyzeHeader.WindowWidth, AnalyzeHeader.WindowCenter);

            ScaledSize = new Size((Int32)AnalyzeHeader.Columns, (Int32)AnalyzeHeader.Rows);
            FixedSize = new Size((Int32)AnalyzeHeader.Columns, (Int32)AnalyzeHeader.Rows);

            ColumnMax = (double)(AnalyzeHeader.WindowCenter + AnalyzeHeader.WindowWidth / 2);
            ColumnMin = (double)(ColumnMax - (double)AnalyzeHeader.WindowWidth);
        }
        #endregion

        #region Serializator Members
        protected override void AddData(SerializationInfo info, StreamingContext context) {
            base.AddData(info, context);
        }

        protected TDicomFrame(SerializationInfo info, StreamingContext context)
            : base(info, context) {
        }
        #endregion

        #region properties
        private TVoxel[,] _VertexVoxels;
        public TVoxel[,] VertexVoxels {
            get {
                if (_VertexVoxels == null)
                    _VertexVoxels = InitVertexVoxels();
                return _VertexVoxels;
            }
        }
        public Size ScaledSize;
        public Size FixedSize;
        public bool ValidDicomImage { get { return AnalyzeImage.PixelDataSize > 0; } }

        //AnalyzeBody Part
        public TAnalyzeHeader AnalyzeHeader {
            get {
                if (_AnalyzeHeader == null) {
                    _AnalyzeHeader = new TAnalyzeHeader(this);
                    if (_AnalyzeImage == null)
                        _AnalyzeImage = new TAnalyzeImage(this);
                    AssignInitials();
                }
                return _AnalyzeHeader;
            }
        }
        public TAnalyzeImage AnalyzeImage {
            get {
                if (_AnalyzeImage == null) {
                    _AnalyzeImage = new TAnalyzeImage(this);
                    if (_AnalyzeHeader == null)
                        _AnalyzeHeader = new TAnalyzeHeader(this);
                    AssignInitials();
                }
                return _AnalyzeImage;
            }
        }

        //PatientInfoFormat
        public string PatientsNameFormat;
        public string PatientIDFormat;
        public string PatientSexFormat;
        public string InstitutionNameFormat;
        public string StudyDateFormat;
        public string StudyTimeFormat;
        public string PatientPositionFormat;
        public string AccessionNumberFormat;
        public string SliceInformationFormat;
        public string DoseInfoFormat;
        #endregion

        #region field
        [NonSerialized]
        private byte[] _GrayColors;
        private float[][] ImageColorMatrix;
        private float Gamma;
        [NonSerialized]
        private byte[] PixelDataBuffer;
        private bool ColorsInverted;
        private bool Colorized;
        private double ColumnMin;
        private double ColumnMax;
        private TAnalyzeHeader _AnalyzeHeader;
        private TAnalyzeImage _AnalyzeImage;
        #endregion

    }
}