﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace DICTROM.Core.Model.DicomFrame
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    [Serializable()]
    public struct TVoxel
    {
        #region properties
        public float X;
        public float Y;
        public float Z;
        public byte Gray;
        #endregion
    }
}
