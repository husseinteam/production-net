﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DICTROM.Core.Model.DicomCursor;
using DICTROM.Core.Model.DicomMessage.Types;

namespace DICTROM.Core.Model.Protocols
{
    public interface IAllocatable
    {
        TBinary binary { get; }
        IAllocatable allocate(ref TCursor cursor);
    }
}
