﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DICTROM.Core.Model.DataElement;
using DICTROM.Core.Model.DataSet;
using System.Runtime.Serialization;

namespace DICTROM.Core.Model.AnalyzeModel {
    [Serializable]
    public class TAnalyzeHeader : AAnalyzeBody, ISerializable {

        #region ctors
        public TAnalyzeHeader(TDicomDataset ds)
            : base(ds) {
        }
        #endregion

        #region abstract override
        protected override void AssignProperties() {
            AssignDisplayProperties(Dataset);
            AssignStudyProperties(Dataset);
        }
        #endregion

        #region Assigners
        private void AssignStudyProperties(TDicomDataset ds) {
            Modality = ds.GetElementTranslation<string>(0x00080060);
            StudyDate = ds.GetElementTranslation<DateTime>(0x00080020);
            StudyTime = ds.GetElementTranslation<DateTime>(0x00080030);
            InstitutionName = ds.GetElementTranslation<string>(0x00080080);
            ReferringPhysiciansName = ds.GetElementTranslation<string>(0x00080090);
            StudyDescription = ds.GetElementTranslation<string>(0x00081030);
            PatientsName = ds.GetElementTranslation<string>(0x00100010);
            PatientID = ds.GetElementTranslation<string>(0x00100020);
            PatientSex = ds.GetElementTranslation<string>(0x00100040);
            InstitutionName = ds.GetElementTranslation<string>(0x00080080);
            PatientPosition = ds.GetElementTranslation<string>(0x00185100);
            AccessionNumber = ds.GetElementTranslation<string>(0x00080050);
            PatientsAge = ds.GetElementTranslation<string>(0x00101010);
            PatientsWeight = ds.SolveDecimalProblem(0x00101030);
            PatientsBirthDate = ds.GetElementTranslation<DateTime>(0x00100030);
            ReferringPhysiciansName = ds.GetElementTranslation<string>(0x00080090);
            StudyDescription = ds.GetElementTranslation<string>(0x00081030);
        }
        private void AssignDisplayProperties(TDicomDataset ds) {
            BitsAllocated = ds.GetElementTranslation<int>(0x00280100);
            HighBit = ds.GetElementTranslation<int>(0x00280102);
            Columns = ds.GetElementTranslation<int>(0x00280011);
            Rows = ds.GetElementTranslation<int>(0x00280010);

            WindowCenter = ds.SolveDecimalProblem(0x00281050);
            WindowWidth = ds.SolveDecimalProblem(0x00281051);

            PhotometricInterpretation = ds.GetElementTranslation<string>(0x00280004);
            PixelRepresentation = ds.GetElementTranslation<int>(0x00280103);
            SamplesperPixel = ds.GetElementTranslation<int>(0x00280002);
            PixelSpacing = ds.GetElementTranslation<decimal[]>(0x00280030);
            SliceThickness = ds.SolveDecimalProblem(0x00180050);
            ImagePosition = ds.SolveDecimalProblem(0x00200030);
            SeriesNumber = ds.SolveDecimalProblem(0x00200011);
            ImageOrientation = ds.GetElementTranslation<decimal[]>(0x00200035);
            ImageOrientationPatient = ds.GetElementTranslation<decimal[]>(0x00200037);
            ImagePositionPatient = ds.GetElementTranslation<decimal[]>(0x00200032);

        }
        #endregion

        #region Display Header
        public int BitsAllocated;
        public int HighBit;
        public int Columns;
        public int Rows;
        public decimal WindowCenter;
        public decimal WindowWidth;
        public string PhotometricInterpretation;
        public int PixelRepresentation;
        public int SamplesperPixel;
        public decimal[] PixelSpacing;
        public decimal SliceThickness;
        public decimal ImagePosition;
        public decimal[] ImageOrientationPatient;
        public decimal[] ImageOrientation;
        public decimal[] ImagePositionPatient;
        public decimal SeriesNumber;
        #endregion

        #region Study Header
        public DateTime StudyDate { get; set; }
        public DateTime StudyTime { get; set; }
        public string InstitutionName { get; set; }
        public string ReferringPhysiciansName { get; set; }
        public string StudyDescription { get; set; }
        public string Modality { get; set; }
        public string PatientsName { get; set; }
        public string PatientID { get; set; }
        public string PatientSex { get; set; }
        public string PatientPosition { get; set; }
        public string AccessionNumber { get; set; }
        public string PatientsAge { get; set; }
        public decimal PatientsWeight { get; set; }
        public DateTime PatientsBirthDate { get; set; }
        #endregion

        #region ISerializable Members
        protected TAnalyzeHeader(SerializationInfo info, StreamingContext context)
            : base(info, context) {
            GetStudyData(info);
            GetDisplayData(info);
        }
        private void GetStudyData(SerializationInfo info) {
            StudyDate = (DateTime)info.GetValue("StudyDate", typeof(DateTime));
            StudyTime = (DateTime)info.GetValue("StudyTime", typeof(DateTime));
            InstitutionName = (string)info.GetValue("InstitutionName", typeof(string));
            ReferringPhysiciansName = (string)info.GetValue("ReferringPhysiciansName", typeof(string));
            StudyDescription = (string)info.GetValue("StudyDescription", typeof(string));
            Modality = (string)info.GetValue("Modality", typeof(string));
            PatientsName = (string)info.GetValue("PatientsName", typeof(string));
            PatientID = (string)info.GetValue("PatientID", typeof(string));
            PatientSex = (string)info.GetValue("PatientSex", typeof(string));
            PatientPosition = (string)info.GetValue("PatientPosition", typeof(string));
            AccessionNumber = (string)info.GetValue("AccessionNumber", typeof(string));
            PatientsAge = (string)info.GetValue("PatientsAge", typeof(string));
            PatientsWeight = (decimal)info.GetValue("PatientsWeight", typeof(decimal));
            PatientsBirthDate = (DateTime)info.GetValue("PatientsBirthDate", typeof(DateTime));
        }
        private void GetDisplayData(SerializationInfo info) {
            BitsAllocated = (int)info.GetValue("BitsAllocated", typeof(int));
            HighBit = (int)info.GetValue("HighBit", typeof(int));
            Columns = (int)info.GetValue("Columns", typeof(int));
            Rows = (int)info.GetValue("Rows", typeof(int));
            WindowCenter = (decimal)info.GetValue("WindowCenter", typeof(decimal));
            WindowWidth = (decimal)info.GetValue("WindowWidth", typeof(decimal));
            PhotometricInterpretation = (string)info.GetValue("PhotometricInterpretation", typeof(string));
            PixelRepresentation = (int)info.GetValue("PixelRepresentation", typeof(int));
            SamplesperPixel = (int)info.GetValue("SamplesperPixel", typeof(int));
            PixelSpacing = (decimal[])info.GetValue("PixelSpacing", typeof(decimal[]));
            SliceThickness = (decimal)info.GetValue("SliceThickness", typeof(decimal));
            ImagePosition = (decimal)info.GetValue("ImagePosition", typeof(decimal));
            ImageOrientationPatient = (decimal[])info.GetValue("ImageOrientationPatient", typeof(decimal[]));
            ImageOrientation = (decimal[])info.GetValue("ImageOrientation", typeof(decimal[]));
            ImagePositionPatient = (decimal[])info.GetValue("ImagePositionPatient", typeof(decimal[]));
            SeriesNumber = (decimal)info.GetValue("SeriesNumber", typeof(decimal));
        }

        protected override void AddData(SerializationInfo info, StreamingContext context) {
            AddDisplayData(info);
            AddStudyData(info);
            base.AddData(info, context);
        }
        private static void AddStudyData(SerializationInfo info) {
            info.AddValue("StudyDate", typeof(DateTime));
            info.AddValue("StudyTime", typeof(DateTime));
            info.AddValue("InstitutionName", typeof(string));
            info.AddValue("ReferringPhysiciansName", typeof(string));
            info.AddValue("StudyDescription", typeof(string));
            info.AddValue("Modality", typeof(string));
            info.AddValue("PatientsName", typeof(string));
            info.AddValue("PatientID", typeof(string));
            info.AddValue("PatientSex", typeof(string));
            info.AddValue("PatientPosition", typeof(string));
            info.AddValue("AccessionNumber", typeof(string));
            info.AddValue("PatientsAge", typeof(string));
            info.AddValue("PatientsWeight", typeof(decimal));
            info.AddValue("PatientsBirthDate", typeof(DateTime));
        }
        private static void AddDisplayData(SerializationInfo info) {
            info.AddValue("PixelDataSize", typeof(long));
            info.AddValue("BitsAllocated", typeof(int));
            info.AddValue("HighBit", typeof(int));
            info.AddValue("Columns", typeof(int));
            info.AddValue("Rows", typeof(int));
            info.AddValue("WindowCenter", typeof(decimal));
            info.AddValue("WindowWidth", typeof(decimal));
            info.AddValue("PhotometricInterpretation", typeof(string));
            info.AddValue("PixelRepresentation", typeof(int));
            info.AddValue("SamplesperPixel", typeof(int));
            info.AddValue("PixelSpacing", typeof(decimal));
            info.AddValue("SliceThickness", typeof(decimal));
            info.AddValue("ImagePosition", typeof(decimal));
            info.AddValue("ImageOrientationPatient", typeof(decimal[]));
            info.AddValue("ImageOrientation", typeof(decimal[]));
            info.AddValue("ImagePositionPatient", typeof(decimal[]));
            info.AddValue("SeriesNumber", typeof(decimal));
        }
        public void GetObjectData(SerializationInfo info, StreamingContext context) {
            AddData(info, context);
        }
        #endregion

    }
}
