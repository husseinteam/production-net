﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DICTROM.Core.Model.DataSet;
using System.Runtime.Serialization;
using DICTROM.Core.Model.DataElement;
using DICTROM.Core.Resource;
using System.Threading.Tasks;

namespace DICTROM.Core.Model.AnalyzeModel {
    [Serializable]
    public class TAnalyzeImage : AAnalyzeBody, ISerializable {

        #region ctors
        public TAnalyzeImage(TDicomDataset ds)
            : base(ds) {
        }
        #endregion

        #region override
        protected override void AssignProperties() {

            PixelDataElement = Dataset.GetElement(SConstants.PixelDataTag);

        }
        #endregion

        #region properties
        public TDataElement PixelDataElement;
        public long PixelDataSize { get { return PixelDataElement.DataLength; } }
        #endregion

        #region ISerializable Members
        protected TAnalyzeImage(SerializationInfo info, StreamingContext context)
            : base(info, context) {
        }
        public void GetObjectData(SerializationInfo info, StreamingContext context) {
            AddData(info, context);
        }
        protected override void AddData(SerializationInfo info, StreamingContext context) {
            base.AddData(info, context);
        }
        #endregion

    }
}
