﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DICTROM.Core.Model.DataSet;
using System.Runtime.Serialization;

namespace DICTROM.Core.Model.AnalyzeModel {
    [Serializable()]
    public abstract class AAnalyzeBody {

        #region ctor
        public AAnalyzeBody(TDicomDataset ds) {
            Dataset = ds;
            AssignProperties();
        }
        #endregion

        #region abstract method
        protected abstract void AssignProperties();
        #endregion

        #region ISerializable Members
        protected AAnalyzeBody(SerializationInfo info, StreamingContext context) {
            Dataset = (TDicomDataset)info.GetValue("Dataset", typeof(TDicomDataset));
            AssignProperties();
        }
        protected virtual void AddData(SerializationInfo info, StreamingContext context) {
            info.AddValue("Dataset", Dataset);
        }
        #endregion

        #region fields
        protected TDicomDataset Dataset;
        #endregion

    }
}
