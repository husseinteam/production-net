﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.Threading;
using DICTROM.Core.MPH.EventArgs;

namespace DICTROM.Core.MPH {
    [Serializable()]
    public class TMultipleProgressHost : ISerializable, IDisposable {
        #region ctor
        public TMultipleProgressHost(Int32 loaderCount) {
            InitInstance();

            Loaders = new TBackgroundLoader[loaderCount];
            for (int i = 0; i < LoaderCount; i++) {
                Loaders[i] = new TBackgroundLoader();
                Loaders[i].AddWorkCompleted(new TBackgroundLoader.WorkCompletedDelegate(bl_WorkCompleted));
            }
        }

        public TMultipleProgressHost(IEnumerable<TBackgroundLoader> bgLoaderlist) {
            InitInstance();

            Loaders = bgLoaderlist.ToArray<TBackgroundLoader>();
        }
        #endregion

        #region private methods
        private void InitInstance() {
            CompletedLoaderCount = 0;
            MphTimer = new System.Windows.Forms.Timer();
            MphTimer.Tick += new EventHandler(MphTimer_Tick);
        }

        private void Completor_DoWork(object sender, TDoWorkEventArgs args) {
            MphTimer.Stop();
            OnOverallCompleted(this, args.Argument as TWorkCompletedEventArgs);
        }

        private void bl_WorkCompleted(object sender, TWorkCompletedEventArgs args) {
            lock (this) {
                if (++CompletedLoaderCount == LoaderCount) {
                    if (Completor == null /*&& (sender as TBackgroundLoader).MphChainActivated == false*/) {
                        Completor = new TBackgroundLoader();
                        Completor.AddWork(Completor_DoWork);
                        Completor.SetArgument(args);
                        Completor.InitWork();
                    }
                }
            }
        }
        private void MphTimer_Tick(object sender, System.EventArgs e) {
            TProgressEventArgs args;
            args = new TProgressEventArgs() {
                CompletionCount = CompletedLoaderCount,
                CompletionRatio = (double)CompletedLoaderCount / LoaderCount,
                TimeElapsed = DateTime.Now.Subtract(TimeOfStart)
            };
            OnOverallProgress(this, args);
        }
        private void StartProgressReporting() {
            MphTimer.Start();
            TimeOfStart = DateTime.Now;
        }
        #endregion

        #region public methods
        public TMultipleProgressHost AddOverallCompleted(OverallCompletedDelegate handler) {
            OverallCompletedHandler += handler;
            return this;
        }
        public TMultipleProgressHost AddOverallInit(InitOverallDelegate handler) {
            InitOverallHandler += handler;
            return this;
        }
        public TMultipleProgressHost AddOverallProgress(OverallProgressDelegate handler) {
            OverallProgressHandler += handler;
            return this;
        }
        public void InitAll() {
            OnInitOverall(this, new DoWorkEventArgs(null));
            //Init Progress Reporting
            StartProgressReporting();

            //Start Multiple Init
            for (int i = 0; i < Loaders.Length; i++) {
                Loaders[i].InitWork();
            }
        }
        #endregion

        #region indexer
        public TBackgroundLoader this[int index] {
            get {
                if (index >= Loaders.Length)
                    throw new IndexOutOfRangeException();

                return Loaders[index];
            }
        }
        #endregion

        #region events
        //OverallCompleted
        public delegate void OverallCompletedDelegate(object sender, TWorkCompletedEventArgs args);
        private OverallCompletedDelegate OverallCompletedHandler;
        private void OnOverallCompleted(object sender, TWorkCompletedEventArgs args) {
            if (OverallCompletedHandler != null)
                OverallCompletedHandler(sender, args);
        }

        //OverallProgress
        public delegate void OverallProgressDelegate(object sender, TProgressEventArgs args);
        private OverallProgressDelegate OverallProgressHandler;
        private void OnOverallProgress(object sender, TProgressEventArgs args) {
            if (OverallProgressHandler != null) {
                OverallProgressHandler(sender, args);
            }
        }
        //DoWorkOverAll
        public delegate void InitOverallDelegate(object sender, DoWorkEventArgs args);
        private InitOverallDelegate InitOverallHandler;
        private void OnInitOverall(object sender, DoWorkEventArgs args) {
            if (InitOverallHandler != null)
                InitOverallHandler(sender, args);
        }
        #endregion

        #region properties
        private TBackgroundLoader[] Loaders;
        private TBackgroundLoader Completor;
        private Int32 CompletedLoaderCount;
        private Int32 LoaderCount { get { return Loaders.Length; } }
        private System.Windows.Forms.Timer MphTimer;
        private DateTime TimeOfStart;
        #endregion

        #region ISerializable Members

        public void GetObjectData(SerializationInfo info, StreamingContext context) {
        }

        protected TMultipleProgressHost(SerializationInfo info, StreamingContext context) {
            CompletedLoaderCount = 0;
            Loaders = new TBackgroundLoader[LoaderCount];
        }
        #endregion

        #region IDisposable Members

        public void Dispose() {
            Loaders = null;
        }

        #endregion
    }
}
