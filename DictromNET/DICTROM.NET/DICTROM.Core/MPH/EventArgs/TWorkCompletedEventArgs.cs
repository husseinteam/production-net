﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DICTROM.Core.MPH.EventArgs
{
    public class TWorkCompletedEventArgs : RunWorkerCompletedEventArgs
    {
        #region ctor
        public TWorkCompletedEventArgs(object result, Exception error, bool cancelled)
            : base(result, error, cancelled)
        {
        }
        #endregion

        #region properties
        public TimeSpan CompletionTime { get; set; }
        public Int32 ProgressValue { get; set; }
        public bool DescendantSignals { get; set; }
        #endregion
    }
}
