﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Runtime.Serialization;
using DICTROM.Core.MPH.EventArgs;

namespace DICTROM.Core.MPH
{
    [Serializable()]
    public class TBackgroundLoader : ISerializable
    {
        #region ctors
        public TBackgroundLoader()
        { }
        #endregion

        #region private methods
        private void RunDefault()
        {
            Worker = new BackgroundWorker();
            Worker.DoWork += new DoWorkEventHandler(Worker_DoWork);
            Worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(Worker_RunWorkerCompleted);

            Worker.RunWorkerAsync(Argument);
        }
        private void Worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            TWorkCompletedEventArgs args;
            args = new TWorkCompletedEventArgs(e.Result, e.Error, e.Cancelled);
            args.CompletionTime = DateTime.Now.Subtract(StartTime);

            OnWorkCompleted(sender, args);
            Argument = null;

        }
        private void Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            TDoWorkEventArgs args;
            args = new TDoWorkEventArgs(e.Argument);

            OnDoWork(sender, args);
        }
        #endregion

        #region public methods
        public void InitWork()
        {
            StartTime = DateTime.Now;

            if (MphChainActivated)
                EmbeddedMph.InitAll();
            else
                RunDefault();
        }
        public TBackgroundLoader AddWork(DoWorkDelegate eh)
        {
            if (MphChainActivated)
                throw new Exception("SetMPH executed. No way for AddWork");
            DoWork += eh;

            return this;
        }
        public void AddWorkCompleted(WorkCompletedDelegate eh)
        {
            WorkCompleted += eh;
        }
        public void SetEmbeddedMPH(TMultipleProgressHost mph)
        {
            EmbeddedMph = mph;
            MphChainActivated = true;
            EmbeddedMph.AddOverallCompleted((o, e) => Worker_RunWorkerCompleted(o, e));
        }
        public TBackgroundLoader SetArgument(object arg)
        {
            Argument = arg;
            return this;
        }
        #endregion

        #region events
        public delegate void DoWorkDelegate(object sender, TDoWorkEventArgs args);
        public delegate void WorkCompletedDelegate(object sender, TWorkCompletedEventArgs args);

        private DoWorkDelegate DoWorkHandler;
        private WorkCompletedDelegate WorkCompletedHandler;

        public event DoWorkDelegate DoWork
        {
            add { DoWorkHandler += value; }
            remove { DoWorkHandler -= value; }
        }
        public event WorkCompletedDelegate WorkCompleted
        {
            add { WorkCompletedHandler += value; }
            remove { WorkCompletedHandler -= value; }
        }

        private void OnDoWork(object sender, TDoWorkEventArgs args)
        {
            if (DoWorkHandler != null)
                DoWorkHandler(sender, args);
        }
        private void OnWorkCompleted(object sender, TWorkCompletedEventArgs args)
        {
            if (WorkCompletedHandler != null)
                WorkCompletedHandler(sender, args);
        }
        #endregion

        #region properties
        public bool MphChainActivated;
        #endregion

        #region fields
        private BackgroundWorker Worker;
        private DateTime StartTime;
        private TMultipleProgressHost EmbeddedMph;
        private object Argument;
        #endregion

        #region ISerializable Members

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Worker", Worker);
            info.AddValue("Argument", Argument);
        }

        protected TBackgroundLoader(SerializationInfo info, StreamingContext context)
        {
            Argument = info.GetValue("Argument", typeof(Object));
            Worker = new BackgroundWorker();
            StartTime = DateTime.Now;
        }
        #endregion

    }
}
