﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;

namespace DICTROM.Core.Resource
{
    public class SCustomCursors
    {
        #region static ctor
        static SCustomCursors()
        {
            //Zoom = AdvancedCursors.Create(Path.Combine(coreName, @"Resource\CustomCursors\zoom-in.cur"));
            //BrigthnessContrast = AdvancedCursors.Create(Path.Combine(coreName, @"Resource\CustomCursors\brightness_icon.cur"));
            CursorPath = Path.Combine(Application.StartupPath, "Cursors");
            if (!Directory.Exists(CursorPath))
                Directory.CreateDirectory(CursorPath);
            string curPath = Path.Combine(CursorPath, Guid.NewGuid().ToString("N") + ".cur");

            File.WriteAllBytes(curPath, Properties.Resources.zoom_in_cur);
            Zoom = AdvancedCursors.Create(curPath);

            File.WriteAllBytes(curPath, Properties.Resources.brightness_cur);
            BrigthnessContrast = AdvancedCursors.Create(curPath);

            Directory.Delete(CursorPath, true);
        }
        #endregion

        #region cursors
        public static string CursorPath { get; private set; }
        public static Cursor Zoom { get; private set; }
        public static Cursor BrigthnessContrast { get; private set; }
        #endregion

    }

    public class AdvancedCursors
    {

        [DllImport("User32.dll")]
        private static extern IntPtr LoadCursorFromFile(String str);

        public static Cursor Create(string filename)
        {
            IntPtr hCursor = LoadCursorFromFile(filename);

            if (!IntPtr.Zero.Equals(hCursor))
            {
                return new Cursor(hCursor);
            }
            else
            {
                throw new ApplicationException("Could not create cursor from file " + filename);
            }
        }
    }
}
