﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DICTROM.Core.Model.DataElement;
using System.Windows.Forms;

namespace DICTROM.Core.Resource
{
    public class SConstants
    {
        public static readonly int UnDefinedLength = int.MaxValue;
        public static readonly int AbstractLength = 0;
        public static readonly long ItemTag = 0xFFFEE000;
        public static readonly long ItemDelimitationTag = 0xFFFEE00D;
        public static readonly long SeqDelimitationTag = 0xFFFEE0DD;
        public static readonly byte[] DicomPrefix = new byte[] 
            { 
                (byte)'D',
                (byte)'I',
                (byte)'C',
                (byte)'M',
            };
        public static readonly long PreambleLength = 128;
        public static readonly long DicomOffset = PreambleLength + (int)DicomPrefix.Length;
        public static readonly int PixelDataTag = 0x7FE00010;

        public static readonly Int32 FileTransferChunkSize = 10240;

        public static readonly string DefaultUser = "DefaultUser";
        public static readonly string ServerAlias = "DefaultDicomServer";

        public static readonly int WorklistExtraColCount = 1;

        //public static readonly string DicomFileSearchPattern = @"*.dcm|*.hdr|*.img";
        public static readonly string DicomFileSearchPattern = @"*.dcm";
        public static readonly string ConnectionArgsSerializationPath = 
            Application.StartupPath + "ConnectionArgs.dat";

        public static string EntityConnStrFormat = 
            @"metadata=res://*/Data.Model.DictromDbModel.csdl|res://*/Data.Model.DictromDbModel.ssdl|res://*/Data.Model.DictromDbModel.msl;provider=System.Data.SqlClient;provider connection string=""{0}"";";
    }
}
