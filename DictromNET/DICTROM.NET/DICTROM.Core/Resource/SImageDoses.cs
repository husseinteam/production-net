﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DICTROM.Core.Resource
{
    public static class SImageDoses
    {
        #region property
        public static Dictionary<string, decimal[]> Dict = new Dictionary<string, decimal[]>()
        {
            {"AutoContrast", new decimal[] {0,0}},
            {"BrainBase", new decimal[] {50,130}},
            {"Brain", new decimal[] {50, 80}},
            {"Skull", new decimal[] {1000, 3500}},
            {"Lung", new decimal[] {-400,1400}},
            {"Mediastinum", new decimal[] {50, 300}},
            {"Abdomen", new decimal[] {50,350}},
            {"Bones", new decimal[] {800, 1800}},
        };

        #endregion
    }
}
