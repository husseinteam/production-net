﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DICTROM.Transcription.Attributes;
using DICTROM.Transcription.Constraints;
using DICTROM.DNA.PDU.AAssociate.Items;

namespace DICTROM.DNA.PDU
{
    [GenomDescriptor(typeof(TAssociateRQ))]
    public class TAssociateRQ
    {
        [IODDescriptor(0, typeof(byte), DefaultValue = 0x01)]
        public byte CommandCode { get; set; }
        [IODDescriptor(1, typeof(byte), DefaultValue = 0x00)]
        public byte Reserved1 { get; set; }
        [IODDescriptor(2, typeof(uint))]
        public uint Length { get; set; }
        [IODDescriptor(3, typeof(ushort), DefaultValue = 0x0000)]
        public ushort ProtocolVersion { get; set; }
        [IODDescriptor(4, typeof(ushort), DefaultValue = 0x0000)]
        public ushort Reserved2 { get; set; }
        [StringConstraint(16)]
        [IODDescriptor(5, typeof(string), DefaultValue = 0x0000)]
        public string CalledAET { get; set; }
        [StringConstraint(16)]
        [IODDescriptor(6, typeof(string), DefaultValue = 0x0000)]
        public string CallingAET { get; set; }
        [IODDescriptor(7, typeof(uint), DefaultValue = 0x0000)]
        public uint Reserved3 { get; set; }
        [IODDescriptor(8, typeof(TApplicationContext), DefaultValue = 0x0000)]
        public TApplicationContext ApplicationContext { get; set; }
        [IODDescriptor(9, typeof(TPresentationContextRQ[]), DefaultValue = 0x0000)]
        public TPresentationContextRQ[] PresentationContextItems { get; set; }
        [IODDescriptor(10, typeof(TUserInformationItemRQ))]
        public TUserInformationItemRQ UserInformation { get; set; }
    }
}
