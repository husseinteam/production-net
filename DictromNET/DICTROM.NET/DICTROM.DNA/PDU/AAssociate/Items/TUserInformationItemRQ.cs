﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DICTROM.Transcription.Attributes;
using DICTROM.DNA.PDU.AAssociate.Items.Subitems;

namespace DICTROM.DNA.PDU.AAssociate.Items
{
    [GenomDescriptor(typeof(TUserInformationItemRQ))]
    public class TUserInformationItemRQ
    {
        [IODDescriptor(0, typeof(TMaximumLength))]
        public TMaximumLength MaximumLength { get; set; }
        [IODDescriptor(1, typeof(TImplementationClassUIDRQ))]
        public TImplementationClassUIDRQ ImplementationClassUIDRQ { get; set; }
        [IODDescriptor(2, typeof(TImplementationVersionNameRQ))]
        public TImplementationVersionNameRQ ImplementationVersionNameRQ { get; set; }
        [IODDescriptor(3, typeof(TAsynchronousOperationsWindowRQ))]
        public TAsynchronousOperationsWindowRQ AsynchronousOperationsWindowRQ { get; set; }
        [IODDescriptor(4, typeof(TSCPSCURoleRQ[]))]
        public TSCPSCURoleRQ[] SCPSCURoleRQArr { get; set; }
        [IODDescriptor(5, typeof(TSOPClassExtendedNegotiationRQ))]
        public TSOPClassExtendedNegotiationRQ SOPClassExtendedNegotiationRQ { get; set; }
        [IODDescriptor(6, typeof(TSOPClassCommonExtendedNegotiationRQ))]
        public TSOPClassCommonExtendedNegotiationRQ SOPClassCommonExtendedNegotiationRQ { get; set; }
        [IODDescriptor(7, typeof(TUserIdentityNegotiationRQ))]
        public TUserIdentityNegotiationRQ UserIdentityNegotiationRQ { get; set; }
    }
}
