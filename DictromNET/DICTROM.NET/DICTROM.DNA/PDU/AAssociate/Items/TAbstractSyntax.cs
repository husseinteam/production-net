﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DICTROM.Transcription.Attributes;

namespace DICTROM.DNA.PDU.AAssociate.Items
{
    [GenomDescriptor(typeof(TAbstractSyntax))]
    public class TAbstractSyntax
    {
        [IODDescriptor(0, typeof(byte), DefaultValue=0x30)]
        public byte Code { get; set; }
        [IODDescriptor(1, typeof(byte), DefaultValue = 0x00)]
        public byte Reserved1 { get; set; }
        [IODDescriptor(2, typeof(ushort))]
        public ushort Length { get; set; }
        [IODDescriptor(3, typeof(string))]
        public string Uid { get; set; }
    }
}
