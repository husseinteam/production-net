﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DICTROM.Transcription.Attributes;
using DICTROM.Transcription.Protocols;

namespace DICTROM.DNA.PDU.AAssociate.Items.Subitems
{
    [GenomDescriptor(typeof(TSOPClassExtendedNegotiationRQ))]
    public class TSOPClassExtendedNegotiationRQ : IItem
    {
        [IODDescriptor(0, typeof(byte), DefaultValue = 0x56)]
        public byte Code { get; set; }
        [IODDescriptor(1, typeof(byte), DefaultValue = 0x00)]
        public byte Reserved1 { get; set; }
        [IODDescriptor(2, typeof(ushort))]
        public ushort ItemLength { get; set; }
        [IODDescriptor(3, typeof(ushort))]
        public ushort SOPClassUidLength { get; set; }
        [IODDescriptor(4, typeof(string))]
        public string ServiceClassApplicationInformation { get; set; }
    }
}
