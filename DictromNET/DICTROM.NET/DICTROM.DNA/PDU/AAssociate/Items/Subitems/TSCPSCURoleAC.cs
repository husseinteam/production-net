﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DICTROM.Transcription.Attributes;
using DICTROM.Transcription.Constraints;
using DICTROM.Transcription.Protocols;

namespace DICTROM.DNA.PDU.AAssociate.Items.Subitems
{
    [GenomDescriptor(typeof(TSCPSCURoleAC))]
    public class TSCPSCURoleAC : IItem
    {
        [IODDescriptor(0, typeof(byte), DefaultValue=0x54)]
        public byte Code { get; set; }
        [IODDescriptor(1, typeof(byte), DefaultValue = 0x00)]
        public byte Reserved1 { get; set; }
        [IODDescriptor(2, typeof(ushort))]
        public ushort ItemLength { get; set; }
        [IODDescriptor(3, typeof(ushort))]
        public ushort UidLength { get; set; }
        [IODDescriptor(4, typeof(string))]
        public string SOPClassUid { get; set; }
        [EnumerationConstraint(0, 1)]
        [IODDescriptor(5, typeof(byte))]
        public byte SCURole { get; set; }
        [EnumerationConstraint(0, 1)]
        [IODDescriptor(6, typeof(byte))]
        public byte SCPRole { get; set; }
    }
}
