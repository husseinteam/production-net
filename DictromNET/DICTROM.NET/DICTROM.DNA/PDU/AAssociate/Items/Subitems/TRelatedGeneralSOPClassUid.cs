﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DICTROM.Transcription.Attributes;

namespace DICTROM.DNA.PDU.AAssociate.Items.Subitems
{
    [GenomDescriptor(typeof(TSOPClassCommonExtendedNegotiationRQ))]
    public class TRelatedGeneralSOPClassUid
    {
        [IODDescriptor(0, typeof(byte))]
        public ushort RelatedGeneralSOPClassUidLength { get; set; }
        [IODDescriptor(1, typeof(string))]
        public string RelatedGeneralSOPClassUid { get; set; }
    }
}
