﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DICTROM.Transcription.Attributes;
using DICTROM.Transcription.Protocols;

namespace DICTROM.DNA.PDU.AAssociate.Items.Subitems
{
    [GenomDescriptor(typeof(TSOPClassCommonExtendedNegotiationRQ))]
    public class TSOPClassCommonExtendedNegotiationRQ : IItem
    {
        [IODDescriptor(0, typeof(byte), DefaultValue = 0x57)]
        public byte Code { get; set; }
        [IODDescriptor(1, typeof(byte), DefaultValue = 0x00)]
        public byte SubItemVersion { get; set; }
        [IODDescriptor(2, typeof(ushort))]
        public ushort ItemLength { get; set; }
        [IODDescriptor(3, typeof(ushort))]
        public ushort SOPClassUidLength { get; set; }
        [IODDescriptor(4, typeof(string))]
        public string SOPClassUid { get; set; }
        [IODDescriptor(5, typeof(ushort))]
        public ushort ServiceClassUidLength { get; set; }
        [IODDescriptor(6, typeof(string))]
        public string ServiceClassUid { get; set; }
        [IODDescriptor(7, typeof(ushort))]
        public ushort RelatedGeneralSopClassIdentificationLength { get; set; }
        [IODDescriptor(8, typeof(TRelatedGeneralSOPClassUid[]))]
        public TRelatedGeneralSOPClassUid[] RelatedGeneralSopClassIdentification { get; set; }
    }
}
