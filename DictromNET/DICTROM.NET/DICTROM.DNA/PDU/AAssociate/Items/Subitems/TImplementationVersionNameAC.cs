﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DICTROM.Transcription.Attributes;
using DICTROM.Transcription.Constraints;
using DICTROM.Transcription.Protocols;

namespace DICTROM.DNA.PDU.AAssociate.Items.Subitems
{
    [GenomDescriptor(typeof(TImplementationVersionNameAC))]
    public class TImplementationVersionNameAC : IItem
    {
        [IODDescriptor(0, typeof(byte), DefaultValue = 0x55)]
        public byte Code { get; set; }
        [IODDescriptor(1, typeof(byte), DefaultValue = 0x00)]
        public byte Reserved { get; set; }
        [IODDescriptor(2, typeof(ushort))]
        public ushort Length { get; set; }
        [StringConstraint(16)]
        [IODDescriptor(3, typeof(string))]
        public string ImplementationUID { get; set; }
    }
}
