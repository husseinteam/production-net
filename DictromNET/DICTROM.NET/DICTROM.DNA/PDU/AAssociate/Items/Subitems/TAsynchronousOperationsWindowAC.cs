﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DICTROM.Transcription.Attributes;
using DICTROM.Transcription.Protocols;

namespace DICTROM.DNA.PDU.AAssociate.Items.Subitems
{
    [GenomDescriptor(typeof(TAsynchronousOperationsWindowRC))]
    public class TAsynchronousOperationsWindowRC : IItem
    {
        [IODDescriptor(0, typeof(byte), DefaultValue = 0x53)]
        public byte Code { get; set; }
        [IODDescriptor(1, typeof(byte), DefaultValue = 0x00)]
        public byte Reserved { get; set; }
        [IODDescriptor(2, typeof(ushort), DefaultValue = 0x00000004)]
        public ushort Length { get; set; }
        [IODDescriptor(3, typeof(ushort))]
        public ushort MaximimNumberOperationsInvoked { get; set; }
        [IODDescriptor(4, typeof(ushort))]
        public ushort MaximimNumberOperationsPerformed { get; set; }
    }
}
