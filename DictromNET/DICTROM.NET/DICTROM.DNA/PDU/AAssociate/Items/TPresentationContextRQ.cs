﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DICTROM.Transcription.Attributes;

namespace DICTROM.DNA.PDU.AAssociate.Items
{
    [GenomDescriptor(typeof(TPresentationContextRQ))]
    public class TPresentationContextRQ
    {
        [IODDescriptor(0, typeof(byte), DefaultValue = 0x20)]
        public byte Code { get; set; }
        [IODDescriptor(1, typeof(byte), DefaultValue = 0x00)]
        public byte Reserved1 { get; set; }
        [IODDescriptor(2, typeof(ushort))]
        public ushort Length { get; set; }
        [IODDescriptor(3, typeof(byte))]
        public byte PrCID { get; set; }
        [IODDescriptor(4, typeof(byte), DefaultValue = 0x00)]
        public byte Reserved2 { get; set; }
        [IODDescriptor(5, typeof(byte), DefaultValue = 0x00)]
        public byte Reserved3 { get; set; }
        [IODDescriptor(6, typeof(byte), DefaultValue = 0x00)]
        public byte Reserved4 { get; set; }
        [IODDescriptor(7, typeof(TAbstractSyntax))]
        public TAbstractSyntax AbstractSyntax { get; set; }
        [IODDescriptor(8, typeof(TTransferSyntax))]
        public TTransferSyntax TransferSyntax1 { get; set; }
        [IODDescriptor(9, typeof(TTransferSyntax))]
        public TTransferSyntax TransferSyntax2 { get; set; }
        [IODDescriptor(10, typeof(TTransferSyntax))]
        public TTransferSyntax TransferSyntax3 { get; set; }
    }
}
