﻿using System;
using System.Linq;
using BilBildir.Model.Entities.Enums;
using BilBildir.ViewModel.Extensions;
using BilBildir.Model.Entities.POCO;

using BilBildir.Actors.Core;

using LLF.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BilBildir.Definitions.Static;
using BilBildir.DataSeed.Static;

namespace BilBildir.UnitTests.CoreData {
    [TestClass]
    public class TakeWhereTests {


		[ClassInitialize]
		public static void SetCredentials(TestContext ctx) {

			SCredentialProvider.Current();

		}

        [TestInitialize]
        public void InitEach() {

            SEnterprise.GenerateModels(true);

        }

        [TestMethod]
        public void TakeMessagesOfActorRunsOK() {

            var actorWithMessage = new Actor().SelectMany().First(ac =>
                TakeWhere.TakeMessagesOf(ac.ID).FirstOrDefault() != null);
            actorWithMessage.AssertNotNull();
            actorWithMessage.Author.AssertNotNull();
            actorWithMessage.Author.AssertNotNull();

        }

        [TestMethod]
        public void TakeTheActorRunsOK() {

			var actorID = new Actor().SelectOne().ID;
			var selected = TakeWhere.TakeTheActor(actorID);
            selected.AssertNotNull();
            selected.Author.AssertNotNull();
            selected.Author.Profile.AssertNotNull();
            Assert.IsNotNull(selected.Author.Profile.Identity);

        }

        [TestMethod]
        public void TakeAvaliableBidsRunsOK() {

			var actorID = new Actor().SelectOne().ID;
			var selected = TakeWhere.TakeAvaliableBids();
            Assert.IsNotNull(selected);
            Assert.AreNotEqual(0, selected.Count());
            foreach (dynamic s in selected) {
                Assert.IsNotNull(s.BiddedOn); 
                Assert.IsNotNull(s.LabelClass); 
                Assert.IsNotNull(s.Deadline); 
                Assert.IsNotNull(s.Category); 
            }
            var unique = selected.Unique(d => d.BidID);
            Assert.AreEqual(selected.Count(), unique.Count());
        }

        [TestMethod]
        public void TakeApplicationsRunsOK() {

			var actorID = new Actor().SelectOne().ID;
			var allbids = new Bid().SelectMany();
            var bids = allbids.Take(3);
            Bidder.Applify(ETaskStatus.Reviewed, bids.ElementAt(0).ID, actorID);
            var reviewed =
                TakeWhere.TakeApplications(ETaskStatus.Reviewed, actorID);
            Bidder.Applify(ETaskStatus.Applied, bids.ElementAt(1).ID, actorID);
            var applied =
                TakeWhere.TakeApplications(ETaskStatus.Applied, actorID);
            Bidder.Applify(ETaskStatus.Ignored, bids.ElementAt(2).ID, actorID);
            var ignored =
                TakeWhere.TakeApplications(ETaskStatus.Ignored, actorID);
            var available = TakeWhere.TakeAvaliableBids();

            Assert.AreEqual(available.Count(), available.Unique(av=> av.BidID).Count());

        }

		[TestMethod]
		public void TakeCountRunsOK() {

			var allbids = new Bid().SelectMany();
			var allBidsCount = new Bid().SelectCount<Int32>();
			Assert.AreEqual(allbids.Count(), allBidsCount);

			allbids = new Bid().SelectMany(new { BiddedPrice = 1200M });
			allBidsCount = new Bid().SelectCount<Int32>(new { BiddedPrice = 1200M });
			Assert.AreEqual(allbids.Count(), allBidsCount);

		}

    }
}
