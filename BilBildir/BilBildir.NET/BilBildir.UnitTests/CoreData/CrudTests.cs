﻿using System;
using System.Collections.Generic;
using System.Linq;
using BilBildir.Model.Entities.Enums;
using BilBildir.Model.Entities.POCO;
using LLF.Extensions;
using LLF.Data.Engine;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BilBildir.ViewModel.Extensions;


using LLF.Abstract.Data.Engine;
using BilBildir.Definitions.Static;
using BilBildir.DataSeed.Static;

namespace BilBildir.UnitTests.CoreData {

    [TestClass]
    public class CrudTests {


        [ClassInitialize]
        public static void SetCredentials(TestContext ctx) {

            SCredentialProvider.Current();

        }

        [TestInitialize]
        public void InitEach() {

            SEnterprise.GenerateModels();

        }

        [TestMethod]
        public void QuestionInsertsOK() {

            var q = GetQuestionInstance();
            
            Assert.IsNull(q.Fault);
            Assert.IsNull(q.DeleteSelf(true).Fault);

        }

        [TestMethod]
        public void CollectAnswersRunsOK() {

            var q = GetQuestionInstance();
            var regenerated = new Question().SelectThe(q.ID);
            Assert.IsNull(regenerated.Fault);
            Assert.IsNotNull(q.Script);
            Assert.IsTrue(regenerated.Include<Answer>().Answers.Count() > 0);

            Assert.IsNull(q.DeleteSelf(true).Fault);

        }

        [TestMethod]
        public void UpdateSelfRunsOK() {

            var actor = GetActorInstance();
            actor.Author.Email = "changed@outlook.com";
            actor.UpdateSelf();
            Assert.IsNull(actor.Fault);

            var fromdb = new Actor().SelectThe(actor.ID);
            Assert.IsNotNull(fromdb);
            Assert.AreEqual("changed@outlook.com", fromdb.Author.Email);

            Assert.IsNull(actor.DeleteSelf().Fault);

        }

        [TestMethod]
        public void ActorInsertsProperly() {

            var newactor = new Actor() {
                Rank = new Rank() {
                    MaximumAllowedQuestion = 10,
                    MinimumAllowedQuestion = 3,
                    RankRatio = 0.1f,
                    Reliability = EReliability.Uncertain
                },
                Author = new Author() {
                    Email = "lampiclobe@outlook.com",
                    PasswordHash = "a1q2w3e".ComputeHash(),
                    Profile = new Profile() {
                        Identity = "Özgür Sönmez",
                        AvatarPath = null,
                        SecretQuestion = "",
                        AnswerOfSecretQuestion = ""
                    }
                }
            };
            Assert.IsNull(newactor.InsertSelf().Fault);
            Assert.IsNull(newactor.DeleteSelf().Fault);

        }

        private static Question GetQuestionInstance() {

            var actor = GetActorInstance();
            return new Question() {
                CodeToken = "{0}".GenerateToken(),
                Script = "How much does this demo cost?",
                State = EQuestionState.Draft,
                Topic = new Topic() {
                    Code = "topic1",
                    Description = "Random Topic",
                    ParentTopic = null
                },
                Actor = actor,
                Answers = new Answer[] {
                    new Answer() {
                        CodeToken = "{0}".GenerateToken(),
                        Script = "Answer {0}".GenerateNumber(),
                        State = EAnswerState.Draft,
                        Actor = actor
                    },
                    new Answer() {
                        CodeToken = "{0}".GenerateToken(),
                        Script = "Answer {0}".GenerateNumber(),
                        State = EAnswerState.Candidate,
                        Actor = actor
                    },
                    new Answer() {
                        CodeToken = "{0}".GenerateToken(),
                        Script = "Answer {0}".GenerateNumber(),
                        State = EAnswerState.Seen,
                        Actor = actor
                    },
                }
            }.InsertSelf().UpsertCollections<Answer>();

        }
        private static Actor GetActorInstance() {

            return new Actor() {
                Rank = new Rank() {
                    MaximumAllowedQuestion = 10,
                    MinimumAllowedQuestion = 5,
                    RankRatio = 0.7f,
                    Reliability = EReliability.Trusted
                },
                Author = new Author() {
                    Email = "lampiclook@outbook.com",
                    PasswordHash = "a1q2w3e".ComputeHash(),
                    Profile = new Profile() {
                        SecretQuestion = "How much?",
                        AnswerOfSecretQuestion = "nope",
                        Identity = "Demo Person"
                    }
                }
            }.InsertSelf();

        }


    }
}
