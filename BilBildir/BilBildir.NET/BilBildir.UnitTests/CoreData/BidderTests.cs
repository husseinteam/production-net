﻿using System;
using System.Linq;
using BilBildir.Model.Entities.Enums;
using BilBildir.Model.Entities.POCO;
using BilBildir.Actors.Core;
using LLF.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BilBildir.Definitions.Static;
using BilBildir.DataSeed.Static;

namespace BilBildir.UnitTests.CoreData {
    [TestClass]
    public class BidderTests {

		[ClassInitialize]
		public static void SetCredentials(TestContext ctx) {

			SCredentialProvider.Current();

		}

        [TestInitialize]
        public void InitEach() {

            SEnterprise.GenerateModels(true);

        }

        [TestMethod]
        public void ApplifyRunsOK() {

            var bidApp = new TaskItem().SelectOne();
            Bidder.Applify(ETaskStatus.Applied, bidApp.Bid.ID, bidApp.Applicant.ID);
            var appl = new TaskItem().SelectOne(
                  new { BidID = bidApp.Bid.ID, ApplicantID = bidApp.Applicant.ID });

            var reverse = new TaskItem().SelectThe(appl.ID);
            Assert.IsNotNull(reverse);
            Assert.AreEqual(ETaskStatus.Applied, reverse.Status);

            var appls = TakeWhere.TakeApplications(ETaskStatus.Applied, appl.Applicant.ID);
            Assert.IsTrue(appls.Count() > 0);

            Bidder.Applify(ETaskStatus.Ignored, bidApp.Bid.ID, bidApp.Applicant.ID);
            appl = new TaskItem().SelectOne(
                new { BidID = bidApp.Bid.ID, ApplicantID = bidApp.Applicant.ID });

            appls = TakeWhere.TakeApplications(ETaskStatus.Ignored, appl.Applicant.ID);
            Assert.IsTrue(appls.Count() > 0);


        }
    }
}
