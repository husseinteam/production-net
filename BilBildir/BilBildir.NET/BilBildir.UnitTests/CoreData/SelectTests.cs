﻿using System;
using System.Linq;
using BilBildir.Model.Entities.POCO;
using BilBildir.Actors.Core;
using LLF.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using BilBildir.ViewModel.Extensions;
using LLF.Data.Engine;
using LLF.Abstract.Data.Engine;
using BilBildir.Definitions.Static;
using BilBildir.DataSeed.Static;

namespace BilBildir.UnitTests.CoreData {
    [TestClass]
    public class SelectTests {

		[ClassInitialize]
		public static void SetCredentials(TestContext ctx) {

			SCredentialProvider.Current();

		}

        [TestInitialize]
        public void InitEach() {

            SEnterprise.GenerateModels(true);

        }

        [TestMethod]
        public void SelectOneAuthorRunsOK() {

            var selected = new Author().SelectOne(new { PasswordHash = "a1q2w3e".ComputeHash() });
            selected.AssertNotNull();
            selected.Profile.AssertNotNull();
            selected.Profile.AnswerOfSecretQuestion.AssertNotNull();
            selected.Profile.Identity.AssertNotNull();
            Assert.AreEqual("a1q2w3e".ComputeHash(), selected.PasswordHash);
        }

        [TestMethod]
        public void SelectOneBidRunsOK() {

            var selected = new Bid().SelectOne(new { BiddedPrice = 120M });
            selected.BiddedOn.AssertNotNull();
            selected.Status.AssertNotNull();

        }

        [TestMethod]
        public void CollectRunsOK() {

            var selected = new Question().SelectOne();
            var answers = selected.Collect<Question, Answer>();
            answers.AssertNotNull();
            Assert.IsTrue(answers.Count() > 0);

        }

        [TestMethod]
        public void SelectManyIsUniqueByID() {

            var actorID = 210;
            var bids = new TaskItem().SelectMany(
                new { ApplicantID = actorID }).Select(ba => ba.Bid);

            Assert.AreEqual(bids.Count(), bids.Unique(b => b.ID).Count());

        }

    }
}
