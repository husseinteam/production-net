﻿using System;
using System.Linq;
using BilBildir.Model.Entities.Enums;
using BilBildir.Model.Entities.POCO;
using BilBildir.Actors.Core;
using LLF.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BilBildir.Definitions.Static;
using BilBildir.DataSeed.Static;

namespace BilBildir.UnitTests.CoreData {
    [TestClass]
    public class CatalogTests {


		[ClassInitialize]
		public static void SetCredentials(TestContext ctx) {

			SCredentialProvider.Current();

        }

        [TestInitialize]
        public void InitEach() {

            SEnterprise.GenerateModels(true);

        }

        [TestMethod]
        public void ListAllQuestionsRunsOK() {

            var qsList = Catalog.ListAllQuestions();
            Assert.IsTrue(qsList.Count() > 0);
            qsList.Enumerate(qs => Assert.IsNotNull(qs.Topic));
            qsList.Enumerate(qs => Assert.IsNotNull(qs.Actor));

        }

        [TestMethod]
        public void ListAllTopicsRunsOK() {

            var tpList = Catalog.ListAllTopics();
            Assert.IsTrue(tpList.Count() > 0);


        }

        [TestMethod]
        public void CandidateActionsRunOK() {

            var qsList = Catalog.ListAllQuestions();
            var qs = qsList.First();
            var aws = new Answer() {
                Question = qs,
                Script = "You shoould review the link",
                CodeToken = "{0}".GenerateToken(),
                Actor = Catalog.ListAllActors().First()
            };

            try {
                Catalog.CandidateInstance.Answer(aws, qs);
            } catch (Exception ex) {
                Assert.Fail(ex.Message);
            }

        }

        [TestMethod]
        public void QuestionerInstanceRunsOK() {

            var qs = new Question() {
                Actor = Catalog.ListAllActors().First(),
                Topic = Catalog.ListAllTopics().First(),
                CodeToken = "{0}".GenerateToken(),
                Script = "What is this?",
                State = EQuestionState.Draft
            };
            var tg = Catalog.ListAllTags().First();
            try {
                Catalog.QuestionerInstance.Ask(qs, tg);
            } catch (Exception ex) {
                Assert.Fail(ex.Message);
            }

        }

        [TestMethod]
        public void CatalogOfCategoriesRunsOK() {

            var ctgList = Catalog.ListAllCategories();

            foreach (var ctg in ctgList) {
                Assert.IsNotNull(ctg.Code);
            }

        }

    }
}
