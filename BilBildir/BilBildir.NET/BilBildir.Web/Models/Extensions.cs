﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;
using BilBildir.Model.Entities.POCO;
using BilBildir.Actors.Core;
using BilBildir.Web.Controllers.Base;
using LLF.Extensions;

namespace BilBildir.Web.Models {
    public static class Extensions {

        public static Boolean ActorLoggedIn(this ViewContext self) {

            return (self.Controller as BaseController).SessionFor<Int64>("primarypk") != default(Int64);

        }

        public static String ActorIdentity(this ViewContext self) {

            var id = (self.Controller as BaseController).SessionFor<Int64>("primarypk");
            var auth = TakeWhere.TakeTheActor(id);
            auth.AssertNotNull();
            return auth.Author.Profile.Identity;

        }

        public static Boolean CurrentActionIsBy(this ControllerContext self, params String[] keys) {

            return self.RouteData.Values["action"].ToString().ToLower().In(keys);

        }

		public static MvcHtmlString Script(this HtmlHelper htmlHelper, Int32 order, params Func<object, HelperResult>[] templates) {
			templates.Enumerate((i, template) =>
				htmlHelper.ViewContext.HttpContext.Items["_script_{0}_{1}".PostFormat(order, i)] = template);
			return MvcHtmlString.Empty;
		}

        public static MvcHtmlString Style(this HtmlHelper htmlHelper, params Func<object, HelperResult>[] templates) {
            templates.Enumerate((i, template) =>
                htmlHelper.ViewContext.HttpContext.Items["_style_" + i] = template);
            return MvcHtmlString.Empty;
        }

		public static IHtmlString Virtualize(this HtmlHelper htmlHelper, String url) {

			return new MvcHtmlString(url);

		}

		public static IHtmlString RenderScripts(this HtmlHelper htmlHelper) {
			var sorted = htmlHelper.ViewContext.HttpContext.Items.Keys.OfType<String>()
				.Where(k => k.StartsWith("_script_")).ToList();
			sorted.Sort((prev, next) =>
				Convert.ToInt32(prev.Replace("_script_", "").Split('_')[0]).CompareTo(
					Convert.ToInt32(next.Replace("_script_", "").Split('_')[0])));
			foreach (var key in sorted) {
				var template = htmlHelper.ViewContext.HttpContext.Items[key] as Func<object, HelperResult>;
				if (template != null) {
					htmlHelper.ViewContext.Writer.Write(template(null));
				}
			}
			return MvcHtmlString.Empty;
		}

        public static IHtmlString RenderStyles(this HtmlHelper htmlHelper) {
            var sorted = htmlHelper.ViewContext.HttpContext.Items.Keys.OfType<String>()
                .Where(k => k.StartsWith("_style_")).ToList();
            sorted.Sort((prev, next) =>
                prev.Replace("_style_", "").ToType<Int32>().CompareTo(
                    next.Replace("_style_", "").ToType<Int32>()));
            foreach (var key in sorted) {
                var template = htmlHelper.ViewContext.HttpContext.Items[key] as Func<object, HelperResult>;
                if (template != null) {
                    htmlHelper.ViewContext.Writer.Write(template(null));
                }
            }
            return MvcHtmlString.Empty;
        }

		public static String RouteValueForKey(this UrlHelper self, String key) {

			return (self.RequestContext.RouteData.Values[key] ?? "").ToString();

		}

    }
}