﻿function ShowModal(parsed, callback) {

	if (parsed.IsHtml) {
		$('#div-modal-body').html($(parsed.Html).html());
	} else {
		$('#div-modal-body').html(parsed.Message);
	}
	$('#div-modal-body')
		.removeClass('text-danger').removeClass('text-success')
		.addClass(parsed.Success ? 'text-success' : 'text-danger');
	$('#h-modal-title').html(parsed.ModalTitle);
	$('#h-modal-title')
		.removeClass('text-danger').removeClass('text-success')
		.addClass(parsed.Success ? 'text-success' : 'text-danger');
	$('#modal-container').on('hide.bs.modal', function (e) {
		callback && callback(parsed);
	});
	$('#modal-container').modal({
		show: true
	});
	return false;

}
function succeeded(data) {
	var parsed = JSON.parse(data);
	ShowModal(parsed, function (parsed) {
		if (parsed.Redirect) {
			if (parsed.Redirect != "postback") {
				window.location.pathname = parsed.Redirect;
			} else {
				document.location.href = document.location.href;
			}
		}
	});
}
function succeeds(Message, ModalTitle, Success, Redirect) {
	var parsed = {
		Message: Message,
		ModalTitle: ModalTitle,
		Success: Success,
		Redirect: Redirect
	}
	ShowModal(parsed, function (parsed) {
		if (parsed.Redirect) {
			if (parsed.Redirect != "postback") {
				window.location.pathname = parsed.Redirect;
			} else {
				document.location.href = document.location.href;
			}
		}
	});
}
function onbtnclick(element, confirmText) {
	if (confirmText) {
		if (confirm(confirmText)) {
			$(element).closest('form').submit();
		};
	} else {
		$(element).closest('form').submit();
	}
}