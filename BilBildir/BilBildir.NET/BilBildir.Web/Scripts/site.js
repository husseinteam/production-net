﻿
$(document).ajaxStart(ajaxStart);
$(document).ajaxComplete(ajaxComplete);

function ajaxStart() {

    var screen = $('<div class="__screenlobe"/>').css({
        'opacity': '0.6', 'background-color': 'rgb(242,242,242)', 'z-index': '99999',
        'background-image': 'url("/Images/loading.gif")',
        'background-position': 'center', 'background-repeat': 'no-repeat',
        'margin-top': '0px',
        'position': 'fixed',
        'top': '0px',
        'right': '0px'
    }).width($(document).width()).height($(window).height());
    if ($('.__screenlobe').length > 0) {
        $('.__screenlobe').remove();
    }
    screen.appendTo('body');

}
function ajaxComplete() {

    if ($('.__screenlobe').length > 0) {
        $('.__screenlobe').remove();
    }

}
