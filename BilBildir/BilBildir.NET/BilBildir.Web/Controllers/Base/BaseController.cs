﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using BilBildir.Actors.Core;
using BilBildir.Definitions.Static;
using BilBildir.ViewModel.UI.User;
using LLF.Extensions;
using System.Threading;
using BilBildir.ViewModel.UI.Error;
using BilBildir.Definitions.Locals;

namespace BilBildir.Web.Controllers.Base {
    public class BaseController : Controller {

        internal TItem SessionFor<TItem>(String key) {

            return Session[key].ToType<TItem>();

        }

		#region Protected

		protected void ProcessError(ErrorViewModel vm, Int32 statusCode) {

			Response.StatusCode = statusCode;

			vm.ResultObject.ShowModal = true;
			vm.ResultObject.Success = false;
			vm.ResultObject.ModalTitle = Messages.ModalTitleFail;
			vm.ResultObject.Message = statusCode == 404 ? Messages.ErrorMessage404 : Messages.ErrorMessage500;
			vm.ResultObject.Redirect = Url.Action(SegmentDefaults.Login, "auth");

		}

		protected RedirectResult ToAtrium() {

			return Redirect(Url.Action(SegmentDefaults.Atrium, "landing"));

		}

		protected RedirectResult ToReferrer() {

			return Redirect(Request.UrlReferrer.ToString());

		}

		protected RedirectResult ToLogin() {

			return Redirect(Url.Action(SegmentDefaults.Login, "auth"));

		}

        protected ActionResult GrantAccess<TVM>(TVM vm, Func<Int64, ActionResult> commit)
			where TVM : UserViewModel {

            var authid = SessionFor<Int64>("primarypk");
            if (authid != default(Int64)) {
				HandleUser(vm, authid);
                return commit(authid);
            } else {
                return ToLogin();
            }

        }

		protected Int64 ActorID {
			get {
				return SessionFor<Int64>("primarypk");
			}
		}


        protected void SetSessionFor<TItem>(String key, TItem item) {

            Session[key] = item;

        }

        protected void RemoveSessionFor<TItem>(String key) {

            Session[key] = default(TItem);

        }

        #endregion

		#region Private

		private void HandleUser(UserViewModel vm, long id) {
			vm.AssignedTasks = TakeWhere.TakeAssignedTasksOf(id);
			vm.Notifications = TakeWhere.TakeNotificationsOf(id);
			vm.Messages = TakeWhere.TakeMessagesOf(id);
			vm.ExistingBidCount = TakeWhere.TakeExistingBidCount();
			vm.PublishedBidCount = TakeWhere.TakePublishedBidCount(id);
			vm.ResultObject.PrimaryKey = id;
			vm.ResultObject.ShowModal = Request.IsAjaxRequest();
			vm.ResultObject.Success = !Request.IsAjaxRequest();
		}

		#endregion

	}
}