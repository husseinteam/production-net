﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BilBildir.Web.Controllers.Base;
using BilBildir.Definitions.Static;

namespace BilBildir.Web.Controllers {

	[RoutePrefix("w")]
    public class DefaultController : BaseController {

        [Route("~/")]
        public ActionResult Relay() {

			return ToAtrium();

        }

		[Route(SegmentDefaults.Language + "/{language}", Name = SegmentDefaults.Language)]
		[HttpGet]
		public ActionResult Lang(String language) {

			var cookie = Response.Cookies["culture"];
			if (cookie == null) {
				Response.Cookies.Add(new HttpCookie("culture", language));
			} else {
				Response.Cookies["culture"].Value = language;
			}
			return ToReferrer();

		}

    }
}