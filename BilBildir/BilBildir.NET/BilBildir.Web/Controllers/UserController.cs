﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BilBildir.Model.Entities.Enums;
using BilBildir.Model.Entities.POCO;
using BilBildir.Actors.Core;
using BilBildir.Definitions.Static;
using BilBildir.ViewModel.UI.Common;
using BilBildir.ViewModel.UI.User;
using BilBildir.Model.TransferObjects;
using BilBildir.Web.Controllers.Base;
using LLF.Extensions;
using Newtonsoft.Json;

namespace BilBildir.Web.Controllers {

    [RoutePrefix("u")]
    public class UserController : BaseController {

        [Route("",
            Name = SegmentDefaults.UserIndex)]
        public ActionResult Index(UserViewModel vm) {

            return GrantAccess(vm, id => {

                return View(vm);
            });

        }

        [Route(SegmentDefaults.Bids + "/{command:values(apply|review|ignore)?}/{argument?}",
            Name = SegmentDefaults.Bids)]
        public ActionResult Bids(BidViewModel vm) {

            return GrantAccess(vm, id => {

                if (Request.IsAjaxRequest()) {
                    var command = RouteData.Values["command"].ToType<String>();

                    switch (command) {
                        case "apply":
                            vm.ResultObject = Bidder.Applify(ETaskStatus.Applied, RouteData.Values["argument"].ToType<Int64>(), id);
                            vm.ResultObject.Redirect = Url.Action(SegmentDefaults.MyBids, "user", new { command = String.Empty, argument = String.Empty });
                            break;
                        case "review":
                            vm.ResultObject = Bidder.Applify(ETaskStatus.Reviewed, RouteData.Values["argument"].ToType<Int64>(), id);
                            vm.ResultObject.Redirect = Url.Action(SegmentDefaults.Bids, "user", new { command = String.Empty });
                            break;
                        case "ignore":
                            vm.ResultObject = Bidder.Applify(ETaskStatus.Ignored, RouteData.Values["argument"].ToType<Int64>(), id);
                            vm.ResultObject.Redirect = Url.Action(SegmentDefaults.Bids, "user", new { command = String.Empty });
                            break;
                        default:
                            break;
                    }
                    return new JsonResult() { Data = JsonConvert.SerializeObject(vm.ResultObject) };
                } else {
                    vm.MyBids = TakeWhere.TakeAvaliableBids();
                    vm.MyReviewedBids =
                           TakeWhere.TakeApplications(ETaskStatus.Reviewed, id);
                    vm.MyIgnoredBids =
                            TakeWhere.TakeApplications(ETaskStatus.Ignored, id);

                    return View(vm);
                }
            });

        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        [Route(SegmentDefaults.AddBid, Name = SegmentDefaults.AddBid)]
        [ValidateInput(false)]
        public ActionResult AddBid(TheBidViewModel vm) {

            if (Request.IsAjaxRequest()) {
                return GrantAccess(vm, id => {
                    var result = Bidder.AddBid(vm).ResultObject;
                    if (result.Success) {
                        result.Redirect = Url.Action(SegmentDefaults.MyBids, "user", new { command = String.Empty, argument = String.Empty });
                    }
                    return new JsonResult() { Data = JsonConvert.SerializeObject(result) };
                });
            } else {
                return GrantAccess(vm, id => {
                    vm.CategoryList = TakeWhere.TakeAllCategories();
                    vm.Deadline = DateTime.Now.ToString();
                    return View(vm);
                });
            }

        }


        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        [Route(SegmentDefaults.TheBid + "/{argument}/{argument2?}",
            Name = SegmentDefaults.TheBid)]
        [ValidateInput(false)]
        public ActionResult TheBid(TheBidViewModel vm) {

            if (Request.IsAjaxRequest()) {
                return GrantAccess(vm, id => {

                    var bidID = RouteData.Values["argument"].ToType<Int64>();
                    IResultObject result = Bidder.EditBid(bidID, vm);
                    if (result.Success) {
                        result.Redirect = Url.Action(SegmentDefaults.MyBids, "user", new { command = String.Empty, argument = String.Empty });
                    }
                    return new JsonResult() { Data = JsonConvert.SerializeObject(result) };
                });
            } else {
                return GrantAccess(vm, id => {

                    var bidID = RouteData.Values["argument"].ToType<Int64>();
                    var applicantID = RouteData.Values["argument2"].ToType<Int64>();

                    Bid thebid = null;
                    if (applicantID != default(Int64)) {
                        var theTaskItem = TakeWhere.TakeTheTaskItem(applicantID, bidID);
                        vm.ApplicantIdentity = theTaskItem.Applicant.Author.Profile.Identity;
                        thebid = theTaskItem.Bid;
                    } else {
                        thebid = TakeWhere.TakeTheBid(bidID);
                        vm.BidFormEditMode = true;
                    }

                    vm.Price = thebid.BiddedPrice;
                    vm.Deadline = thebid.Deadline.ToString();
                    vm.CategoryList = TakeWhere.TakeAllCategories();
                    vm.SelectedCategories = TakeWhere.TakeSelectedCategoriesOf(bidID);
                    vm.BidDescription = thebid.BidDescription;

                    return View(vm);
                });
            }

        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        [Route(SegmentDefaults.SetProfile, Name = SegmentDefaults.SetProfile)]
        public ActionResult SetProfile(ProfileFormViewModel vm) {

            if (Request.IsAjaxRequest()) {
                return GrantAccess(vm, id => {
                    var result = Authenticator.SetProfile(vm, id);
                    result.Redirect = Url.Action(SegmentDefaults.UserIndex, "user");
                    return new JsonResult() { Data = JsonConvert.SerializeObject(result) };
                });
            } else {
                return GrantAccess(vm, id => {
                    var actor = TakeWhere.TakeTheActor(id);
                    vm.Email = actor.Author.Email;
                    vm.Identity = actor.Author.Profile.Identity;
                    vm.AvatarPath = actor.Author.Profile.AvatarPath;
                    vm.SecretQuestion = actor.Author.Profile.SecretQuestion;
                    vm.AnswerOfSecretQuestion = actor.Author.Profile.AnswerOfSecretQuestion;
                    vm.ResultObject = SessionFor<IResultObject<String>>("robj") ?? vm.ResultObject;
                    RemoveSessionFor<IResultObject<String>>("robj");
                    return View(vm);
                });
            }

        }

        [ChildActionOnly]
        public PartialViewResult ImageCropper() {

            var vm = new ImageCropperViewModel() {
                Image = SessionFor<String>("avtr").DefaultIfNullOrEmpty("/Images/login.png")
            };
            return PartialView("_ImageCropper", vm);

        }

        [Route(SegmentDefaults.Avatarize, Name = SegmentDefaults.Avatarize)]
        public ActionResult Avatarize(ImageCropperViewModel vm) {

            return GrantAccess(vm, id => {
                IResultObject<String> robj = null;
                var path = String.Empty;
                foreach (string fileName in Request.Files) {
                    var file = Request.Files[fileName];
                    try {
                        if (file != null && file.ContentLength > 0) {
                            path = Server.MapPath("/Images/ClientImages/{0}-{1}{2}"
                                .PostFormat(id, Guid.NewGuid().ToString("N"),
                                        file.FileName.Substring(file.FileName.LastIndexOf("."))));
                            Directory.EnumerateFiles(Server.MapPath("/Images/ClientImages/"),
                                "{0}-*".PostFormat(id)).Enumerate(p => System.IO.File.Delete(p));
                            file.SaveAs(path);
                            break;
                        }
                    } catch {
                        continue;
                    }
                }
                if (!path.IsUnassigned()) {
                    if (System.IO.File.Exists(path)) {
                        robj = Authenticator.SaveAvatar(id, path);
                        if (robj != null) {
                            robj.Redirect = Url.Action(SegmentDefaults.SetProfile, "user");
                        }
                    }
                }
                if (robj == null) {
                    robj = Authenticator.AvatarNotSavedResult();
                    robj.Redirect = Url.Action(SegmentDefaults.SetProfile, "user");
                }

                vm.Image = robj.Result;
                SetSessionFor("avtr", vm.Image);
                SetSessionFor("robj", robj);
                return Redirect(SegmentDefaults.SetProfile);

            });
        }

        [Route(SegmentDefaults.MyBids + "/{command:values(delete|republish|suspend)?}/{argument?}",
            Name = SegmentDefaults.MyBids)]
        public ActionResult MyBids(BidViewModel vm) {

            return GrantAccess(vm, id => {

                if (Request.IsAjaxRequest()) {
                    var command = RouteData.Values["command"].ToType<String>();

                    switch (command) {
                        case "delete":
                            Bidder.Delete(vm, RouteData.Values["argument"].ToType<Int64>());
                            vm.ResultObject.ShowModal = true;
                            break;
                        case "republish":
                            Bidder.Republish(vm, RouteData.Values["argument"].ToType<Int64>());
                            vm.ResultObject.ShowModal = true;
                            break;
                        case "suspend":
                            Bidder.Suspend(vm, RouteData.Values["argument"].ToType<Int64>());
                            vm.ResultObject.ShowModal = true;
                            break;
                        default:
                            break;
                    }
                    vm.ResultObject.Redirect = Url.Action(SegmentDefaults.MyBids, "user", new { command = String.Empty, argument = String.Empty });
                    return new JsonResult() { Data = JsonConvert.SerializeObject(vm.ResultObject) };
                } else {
                    vm.ResultObject.Redirect = Url.Action(SegmentDefaults.MyBids, "user", new { command = String.Empty, argument = String.Empty });
                    vm.MyApplications = TakeWhere.TakeTaskItemsOf(id);
                    vm.MyPublishedBids = TakeWhere.TakePublishedBidsOf(id);

                    return View(vm);
                }

            });

        }

        [Route(SegmentDefaults.MyApplicants + "/{command:values(view|approve)?}/{argument?}/{argument2?}",
            Name = SegmentDefaults.MyApplicants)]
        public ActionResult MyApplicants(MyApplicantsViewModel vm) {

            return GrantAccess(vm, id => {


                var command = RouteData.Values["command"].ToType<String>();
                var bidID = RouteData.Values["argument"].ToType<Int64>();

                switch (command) {
                    case "approve":
                        vm.ResultObject = Bidder.ApproveApplication(RouteData.Values["argument2"].ToType<Int64>(), bidID);
                        vm.ResultObject.ShowModal = true;
                        vm.ResultObject.Redirect = Url.Action(SegmentDefaults.MyBids, "user", new { command = String.Empty, argument = String.Empty });
                        break;
                    case "view":
                        vm.ResultObject.ShowModal = false;
                        break;
                    default:
                        break;
                }

                vm.MyApplicants = TakeWhere.TakeApplicantsOf(bidID);

                return Request.IsAjaxRequest() ?
                    new JsonResult() { Data = JsonConvert.SerializeObject(vm.ResultObject) } as ActionResult : View(vm);

            });

        }

        [Route(SegmentDefaults.MyTasks + "/{argument?}"
            , Name = SegmentDefaults.MyTasks)]
        public ActionResult MyTasks(MyTasksViewModel vm) {

            if (Request.IsAjaxRequest()) {
                return GrantAccess(vm, id => {
                    IResultObject result = null;
                    var taskID = RouteData.Values["argument"].ToType<Int64>();
                    var theTask = TakeWhere.TakeTheTask(taskID);
                    new TheTaskViewModel(theTask).MapTo(ref vm);
                    result = Taskifier.CompleteTask(theTask);
                    result.Redirect = "postback";
                    return new JsonResult() { Data = JsonConvert.SerializeObject(result) };
                });
            } else {
                return GrantAccess(vm, id => {
                    vm.ResultObject.ShowModal = false;
                    return View(vm);
                });
            }
        }

        [Route(SegmentDefaults.TheTask +
            "/{command:values(view|additem|removeitem|update)}/{argument}", Name = SegmentDefaults.TheTask)]
        [ValidateInput(false)]
        public ActionResult TheTask(TheTaskViewModel vm) {

            var command = RouteData.Values["command"].ToType<String>();
            if (Request.IsAjaxRequest()) {
                return GrantAccess(vm, id => {
                    IResultObject result = null;
                    if (command == "additem") {
                        var taskID = RouteData.Values["argument"].ToType<Int64>();
                        var theTask = TakeWhere.TakeTheTask(taskID);
                        new TheTaskViewModel(theTask).MapTo(ref vm);
                        result = Taskifier.AddToDoItem(vm, theTask);
                    } else if (command == "removeitem") {
                        var todoItemID = RouteData.Values["argument"].ToType<Int64>();
                        var todoItem = TakeWhere.TakeTheToDoItem(todoItemID);
                        result = Taskifier.RemoveToDoItem(todoItem);
                        result.ShowModal = false;
                    } else if (command == "update") {
                        var taskID = RouteData.Values["argument"].ToType<Int64>();
                        var theTask = TakeWhere.TakeTheTask(taskID);
                        result = Taskifier.UpdateTask(vm, theTask);
                        if (result.Success) {
                            result.Redirect = Url.Action(SegmentDefaults.MyTasks, "user", new { argument = String.Empty });
                        } else {
                            result.Redirect = "postback";
                        }
                    }

                    return new JsonResult() { Data = JsonConvert.SerializeObject(result) };
                });
            } else {
                return GrantAccess(vm, id => {
                    if (command == "view") {

                        var taskID = RouteData.Values["argument"].ToType<Int64>();
                        var theTask = TakeWhere.TakeTheTask(taskID);
                        if (theTask.Applicant.ID == id) {
                            new TheTaskViewModel(theTask).MapTo(ref vm);
                        } else {
                            throw new ArgumentException();
                        }
                    }
                    vm.ResultObject.ShowModal = false;
                    return View(vm);
                });
            }

        }

    }
}