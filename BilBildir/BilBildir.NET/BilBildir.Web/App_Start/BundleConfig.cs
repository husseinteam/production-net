﻿using System.Threading;
using System.Web;
using System.Web.Optimization;
using LLF.Extensions;

namespace BilBildir.Web {

	public class BundleConfig {
		// For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
		public static void RegisterBundles(BundleCollection bundles) {

			#region Scripts

			bundles.Add(new ScriptBundle("~/bundles/landing").Include(
					"~/Scripts/jquery-{version}.js",
					"~/Scripts/pace.min.js",
					"~/Scripts/bootstrap.min.js",
					"~/Scripts/respond.min.js",
					"~/Scripts/classie.js",
					"~/Scripts/cbpAnimatedHeader.js",
					"~/Scripts/wow.min.js",
					"~/Scripts/inspinia.js",
					"~/Scripts/site.js"
				));

			bundles.Add(new ScriptBundle("~/bundles/layout").Include(
					"~/Scripts/jquery-{version}.js",
					"~/Scripts/bootstrap.min.js",
					"~/Scripts/respond.min.js",
					"~/Scripts/site.js"
				));

			bundles.Add(new ScriptBundle("~/bundles/msajax")
				.Include("~/Scripts/jquery.validate*")
				.Include("~/Scripts/jquery.unobtrusive-ajax.min.js"));

			// Use the development version of Modernizr to develop with and learn from. Then, when you"re
			// ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
			bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
						"~/Scripts/modernizr-*"));

			bundles.Add(new ScriptBundle("~/bundles/custom").Include(
									  "~/Scripts/common/clock.js"
									  ));

			#endregion

			#region Styles

			bundles.Add(new StyleBundle("~/Content/landing").Include(
					 "~/Content/bootstrap.min.css",
					 "~/Content/animate.css",
					 "~/Content/font-awesome.min.css",
					 "~/Content/landing.css",
					 "~/Content/site.css"
					 ));

			bundles.Add(new StyleBundle("~/Content/layout").Include(
						  "~/Content/bootstrap.min.css",
						  "~/Content/font-awesome.min.css",
						  "~/Content/site.css"
					 ));

			bundles.Add(new StyleBundle("~/Content/auth").Include(
						  "~/Content/bootstrap.min.yeti.css",
						  "~/Content/font-awesome.min.css",
						  "~/Content/site.css"
					 ));

			#endregion

		}

		public static void RegisterUserBundles(BundleCollection bundles) {

			bundles.Add(new StyleBundle("~/Content/board").Include(
					"~/Content/dashboard/bootstrap.min.css",
					"~/Content/dashboard/bootstrap-responsive.min.css",
					"~/Content/dashboard/jquery-ui-1.8.21.custom.css",
					"~/Content/dashboard/fullcalendar.css",
					"~/Content/dashboard/chosen.css",
					"~/Content/dashboard/uniform.default.css",
					"~/Content/dashboard/jquery.cleditor.css",
					"~/Content/dashboard/jquery.noty.css",
					"~/Content/dashboard/noty_theme_default.css",
					"~/Content/dashboard/elfinder.min.css",
					"~/Content/dashboard/elfinder.theme.css",
					"~/Content/dashboard/jquery.iphone.toggle.css",
					"~/Content/dashboard/uploadify.css",
					"~/Content/dashboard/jquery.gritter.css",
					"~/Content/dashboard/font-awesome.min.css",
					"~/Content/dashboard/font-awesome-ie7.min.css",
					"~/Content/dashboard/glyphicons.css",
					"~/Content/dashboard/halflings.css",
					"~/Content/dashboard/style-forms.css",
					"~/Content/dashboard/style.css",
					"~/Content/dashboard/style-responsive.css",
					"~/Content/Gridmvc.css",
					"~/Content/site.css"
					));
			bundles.Add(new ScriptBundle("~/bundles/board").Include(
				"~/Scripts/dashboard/jquery-1.9.1.min.js",
				"~/Scripts/dashboard/jquery-migrate-1.2.1.min.js",
				"~/Scripts/dashboard/jquery-ui-1.10.0.custom.min.js",
				"~/Scripts/dashboard/jquery.ui.touch-punch.js",
				//"~/Scripts/dashboard/modernizr.js",
				"~/Scripts/dashboard/bootstrap.min.js",
				"~/Scripts/dashboard/jquery.cookie.js",
				"~/Scripts/dashboard/fullcalendar.min.js",
				"~/Scripts/dashboard/jquery.dataTables.min.js",
				"~/Scripts/dashboard/excanvas.js",
				"~/Scripts/dashboard/jquery.flot.js",
				"~/Scripts/dashboard/jquery.flot.pie.js",
				"~/Scripts/dashboard/jquery.flot.stack.js",
				"~/Scripts/dashboard/jquery.flot.resize.min.js",
				"~/Scripts/dashboard/jquery.chosen.min.js",
				"~/Scripts/dashboard/jquery.uniform.min.js",
				"~/Scripts/dashboard/jquery.cleditor.min.js",
				"~/Scripts/dashboard/jquery.noty.js",
				"~/Scripts/dashboard/jquery.elfinder.min.js",
				"~/Scripts/dashboard/jquery.raty.min.js",
				"~/Scripts/dashboard/jquery.iphone.toggle.js",
				"~/Scripts/dashboard/jquery.uploadify-3.1.min.js",
				"~/Scripts/dashboard/jquery.gritter.min.js",
				"~/Scripts/dashboard/jquery.imagesloaded.js",
				"~/Scripts/dashboard/jquery.masonry.min.js",
				"~/Scripts/dashboard/jquery.knob.modified.js",
				"~/Scripts/dashboard/jquery.sparkline.min.js",
				"~/Scripts/dashboard/counter.js",
				"~/Scripts/dashboard/retina.js",
				"~/Scripts/dashboard/custom.js",
				"~/Scripts/gridmvc.min.js",
				"~/Scripts/site.js"));


		}

	}
}
