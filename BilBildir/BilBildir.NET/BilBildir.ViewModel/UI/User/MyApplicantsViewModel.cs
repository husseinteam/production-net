﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Annotations.ViewModel;
using BilBildir.Model.Entities.POCO;
using BilBildir.Definitions.Locals;
using BilBildir.ViewModel.UI.Base;
using BilBildir.ViewModel.UI.User.RowModels;

namespace BilBildir.ViewModel.UI.User {

    [LocalResource(typeof(Classified))]
    public class MyApplicantsViewModel : BidViewModel {

        #region ToUI

        #endregion

		public IEnumerable<MyApplicantsRowModel> MyApplicants { get; set; }

        public override Type ViewModelType {
            get {
                return typeof(MyApplicantsViewModel);
            }
        }

		public override string ViewTitle {
			get { return ViewTitles.MyApplicants; }
		}

    }

}
