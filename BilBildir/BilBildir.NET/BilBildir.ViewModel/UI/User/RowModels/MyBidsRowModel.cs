﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilBildir.Model.Entities.POCO;
using BilBildir.Definitions.Locals;
using BilBildir.ViewModel.Extensions;

using LLF.Extensions;
using BilBildir.Model.Entities.Enums;

namespace BilBildir.ViewModel.UI.User.RowModels {

	public class MyBidsRowModel : MyTasksRowModel {

		public MyBidsRowModel(TaskItem ba)
		: base(ba) {

			BidID = ba.Bid.ID;
			AppliedOn = ba.PublishedOn;

		}

		#region Properties

		public Int64 BidID { get; set; }
		public DateTime AppliedOn { get; set; }

		#endregion

	}

}
