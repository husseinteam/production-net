﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilBildir.Model.Entities.POCO;
using BilBildir.Definitions.Locals;
using BilBildir.ViewModel.Extensions;

using LLF.Extensions;
using BilBildir.Model.Entities.Enums;

namespace BilBildir.ViewModel.UI.User.RowModels {

    public class MyPublishedBidsRowModel : MyTasksRowModel {

        public MyPublishedBidsRowModel(Bid b)
            : base(b) {

            BidID = b.ID;
            IsPublished = b.Status != EBidStatus.Suspended;
            IsAccepted = b.Status == EBidStatus.Accepted;

        }

        #region Properties

        public Int64 BidID { get; set; }
        public Boolean IsAccepted { get; set; }
        public Boolean IsPublished { get; set; }

        #endregion
    }

}
