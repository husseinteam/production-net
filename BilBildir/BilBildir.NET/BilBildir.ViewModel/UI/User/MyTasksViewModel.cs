﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Annotations.ViewModel;
using BilBildir.ViewModel.Extensions;
using BilBildir.Model.Entities.POCO;
using BilBildir.Definitions.Locals;
using BilBildir.ViewModel.UI.Base;
using LLF.Extensions;

namespace BilBildir.ViewModel.UI.User {

    [LocalResource(typeof(Classified))]
    public class MyTasksViewModel : BidViewModel {

        public MyTasksViewModel(TaskItem at) {

            this.TaskID = at.ID;
			this.Title = at.Title;
			this.TitleSummary = at.Title.Summary(20);
            this.ActorIdentity = at.Applicant.Author.Profile.Identity;
			this.BidderIdentity = at.Applicant.Author.Profile.Identity;
            this.CompletionPercent = at.Percent.Percentise();
            this.TaskCompletionColorClass = at.TaskCompletionColor.GetDescription();
            this.Status = at.Status.GetStatus();
            this.TaskStatusColorClass = at.Status.GetDescription();

        }
        public MyTasksViewModel() {
                
        }

        #region ToUI

        [ToUI]
        public Int64 TaskID { get; set; }
        [ToUI]
		public String Title { get; set; }
		[ToUI]
		public String TitleSummary { get; set; }
        [ToUI]
        public String ActorIdentity { get; set; }
        [ToUI]
        public String BidderIdentity { get; set; }
        [ToUI]
        public Int32 CompletionPercent { get; set; }
        [ToUI]
        public String TaskCompletionColorClass { get; set; }
        [ToUI]
        public String Status { get; set; }
        [ToUI]
        public String TaskStatusColorClass { get; set; }
        
        #endregion

        public override Type ViewModelType {
            get {
                return typeof(MyTasksViewModel);
            }
        }

		public override string ViewTitle {
			get { return ViewTitles.MyTasks; }
		}

    }

}
