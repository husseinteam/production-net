﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Annotations.ViewModel;

using BilBildir.Model.Entities.Enums;
using BilBildir.ViewModel.Extensions;
using BilBildir.Model.Entities.POCO;
using BilBildir.Definitions.Locals;
using BilBildir.ViewModel.UI.Base;
using LLF.Extensions;
using BilBildir.Definitions.Static;
using LLF.Data.Engine;

namespace BilBildir.ViewModel.UI.User {
    [LocalResource(typeof(Classified))]
    public class TheTaskViewModel : MyTasksViewModel {

        public TheTaskViewModel(TaskItem at)
            : base(at) {

            this.ToDoItems = at.Collect<TaskItem, ToDoItem>().Select(td => new ToDoItemViewModel(td)).ToArray();
            var barelist = new List<String>().PassOverEnum<EToDoItemEmergency>(en => en.GetStatus())
                .Select(el => @"""{0}""".PostFormat(el)).Gather(", ");
            this.EmergencyStatusList = "[{0}]".PostFormat(barelist);

        }
        public TheTaskViewModel() {
                
        }

        #region ToUI

		[ToUI(ValidationErrorKey = "ToDoTitleNullError")]
		public String ToDoTitle { get; set; }
		[ToUI(ValidationErrorKey = "EmergencyStatusNullError")]
		public String EmergencyStatus { get; set; }
        
            
        #endregion

		public ICollection<ToDoItemViewModel> ToDoItems { get; set; }
		public String EmergencyStatusList { get; set; }

        public override Type ViewModelType {
            get {
                return typeof(TheTaskViewModel);
            }
        }

		public override string ViewTitle {
			get { return ViewTitles.TheTask; }
		}

    }
}
