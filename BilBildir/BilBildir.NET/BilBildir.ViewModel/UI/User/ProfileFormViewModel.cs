﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Annotations.ViewModel;

using BilBildir.Definitions.Locals;
using BilBildir.ViewModel.UI.Common;
using BilBildir.Definitions.Static;

namespace BilBildir.ViewModel.UI.User {

    [LocalResource(typeof(Classified))]
    public class ProfileFormViewModel : UserViewModel {

        #region ToUI

		[ToUI(ValidationErrorKey = "IdentityNullError")]
		[ApplyRegex("Identity", "IdentityValidationError")]
        public String Identity { get; set; }

        [ToUI]
        public String AvatarPath { get; set; }

		[ToUI(ValidationErrorKey = "SecretQuestionNullError")]
		public String SecretQuestion { get; set; }

		[ToUI(ValidationErrorKey = "AnswerOfSecretQuestionNullError")]
		public String AnswerOfSecretQuestion { get; set; }

        [ToUI]
        [ApplyRegex("Email", "EmailValidationError")]
        public String Email { get; set; }

		[ToUI(ValidationErrorKey = "PasswordNullError")]
        [ApplyRegex("Password", "PasswordValidationError")]
        public String Password { get; set; }

		[ToUI(ValidationErrorKey = "PasswordRepeatNullError")]
		[ApplyRegex("Password", "PasswordValidationError")]
        public String PasswordRepeat { get; set; }

        #endregion

		public override Type ViewModelType {
            get {
                return typeof(ProfileFormViewModel);
            }
        }

		public override string ViewTitle {
			get { return ViewTitles.ProfileChange; }
		}

    }

}
