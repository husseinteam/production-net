﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LLF.Annotations.ViewModel;
using BilBildir.Model.Entities.POCO;
using BilBildir.Definitions.Locals;
using BilBildir.ViewModel.UI.Landing;
using BilBildir.Model.TransferObjects;
using BilBildir.Definitions.Static;

namespace BilBildir.ViewModel.UI.Auth {

    [LocalResource(typeof(Public))]
    public class LoginViewModel : AuthViewModel {

		[ToUI(ValidationErrorKey = "UserNameNullError")]
        [ApplyRegex("Email", "UserNameValidationError")]
        public String UserName { get; set; }
		[ToUI(ValidationErrorKey = "PasswordNullError")]
        [ApplyRegex("Password", "PasswordValidationError")]
        public String Password { get; set; }

        public override Type ViewModelType {
            get {
                return typeof(LoginViewModel);
            }
        }

		public override string ViewTitle {
			get { return ViewTitles.Login; }
		}
    }
}