﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LLF.Annotations.ViewModel;
using BilBildir.Model.Entities.POCO;
using BilBildir.Definitions.Locals;
using BilBildir.ViewModel.UI.Landing;
using BilBildir.Model.TransferObjects;
using BilBildir.Definitions.Static;

namespace BilBildir.ViewModel.UI.Auth {

    [LocalResource(typeof(Public))]
	public class RecoverViewModel : AuthViewModel {

        #region ToUI

		[ToUI(ValidationErrorKey = "UserNameNullError")]
		[ApplyRegex("Email", "EmailValidationError")]
		public String Email { get; set; }

        #endregion

        public override Type ViewModelType {
            get {
                return typeof(RecoverViewModel);
            }
        }

		public override string ViewTitle {
			get { return ViewTitles.Recover; }
		}

	}
}