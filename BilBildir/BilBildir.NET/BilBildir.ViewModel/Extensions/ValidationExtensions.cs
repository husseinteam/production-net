﻿using System;
using System.Linq;
using LLF.Extensions;
using BilBildir.ViewModel.UI.Auth;
using BilBildir.Definitions.Locals;
using BilBildir.ViewModel.UI.User;
using BilBildir.Model.Entities.Enums;
using System.Threading;
using System.Globalization;
using LLF.ViewModel.Core.Extensions;
using BilBildir.ViewModel.API.Claims;
using LLF.Abstract.Data.Engine;

namespace BilBildir.ViewModel.Extensions {
    public static class ValidationExtensions {

        #region ViewModel Extensions

        public static IValidationResponse Validate(this LoginViewModel vmodel) {

            return vmodel.CommonValidator<LoginViewModel>(null, false);

        }
        public static IValidationResponse Validate(this TheBidViewModel vmodel) {

            return vmodel.CommonValidator<TheBidViewModel>((resp, vm) => {
                DateTime date = DateTime.MinValue;
                if (!DateTime.TryParse(vmodel.Deadline, Thread.CurrentThread.CurrentCulture, DateTimeStyles.None, out date)) {
                    resp.AppendMessage("{0}", Messages.DeadlineFormatError);
                    resp.IsValid = false;
                } else if (date < DateTime.Now) {
                    resp.AppendMessage("{0}", Messages.DeadlineBeforeNowError);
                    resp.IsValid = false;
                }
                if (vmodel.SelectedCategories == null || vmodel.SelectedCategories.Count() == 0) {
                    resp.AppendMessage("{0}", Messages.SelectedCategoriesEmptyError);
                    resp.IsValid = false;
                }
            });

        }
        public static IValidationResponse Validate(this BidViewModel vmodel) {

            return vmodel.CommonValidator<BidViewModel>();

        }
        public static IValidationResponse Validate(this ProfileFormViewModel vmodel) {

            return vmodel.CommonValidator<ProfileFormViewModel>((resp, vm) => {
                if (!vm.Password.Equals(vm.PasswordRepeat)) {
                    resp.AppendMessage("{0}", Messages.PasswordsDoNotMatchError);
                    resp.IsValid = false;
                }
            });

        }
        public static IValidationResponse Validate(this SignupViewModel vmodel) {

            return vmodel.CommonValidator<SignupViewModel>((resp, vm) => {
                if (vm.Password != vm.PasswordRepeat) {
                    resp.AppendMessage("{0}", Messages.PasswordsDoNotMatchError);
                    resp.IsValid = false;
                }
            });

        }
        public static IValidationResponse Validate(this TheTaskViewModel vmodel) {

            return vmodel.CommonValidator<TheTaskViewModel>((resp, vm) => {
                if (!vm.EmergencyStatus.EnumValueIn<EToDoItemEmergency>(en => en.GetStatus())) {
                    resp.AppendMessage("{0}", Messages.ToDoEmergencyStatusFormatError);
                    resp.IsValid = false;
                }
            });

        }
        public static IValidationResponse Validate(this RecoverViewModel vmodel) {

            return vmodel.CommonValidator<RecoverViewModel>();

        }
        public static IValidationResponse Validate(this ResetPasswordViewModel vmodel) {

            return vmodel.CommonValidator<ResetPasswordViewModel>((resp, vm) => {
                if (!vm.Password.Equals(vm.PasswordRepeat)) {
                    resp.AppendMessage("{0}", Messages.PasswordsDoNotMatchError);
                    resp.IsValid = false;
                }
            });

        }
        public static IValidationResponse Validate(this AuthenticationViewModel vmodel) {

            return vmodel.CommonValidator<AuthenticationViewModel>();

        }

        #endregion

    }
}
