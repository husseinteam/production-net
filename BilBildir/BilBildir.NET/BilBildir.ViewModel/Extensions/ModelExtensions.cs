﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Extensions;
using BilBildir.ViewModel.UI.Auth;
using LLF.Annotations.ViewModel;
using System.Text.RegularExpressions;
using BilBildir.ViewModel.UI.User;
using BilBildir.Model.Entities.Enums;
using BilBildir.ViewModel.Extensions;
using System.Threading;
using System.Globalization;
using LLF.ViewModel.Core.Extensions;
using BilBildir.Definitions.Static;
using BilBildir.Model.Entities.POCO;
using LLF.Data.Engine;

namespace BilBildir.ViewModel.Extensions {

	public static class ModelExtensions {

		public static String GetTextGlobalized(this Manifest manifest) {

			var statement = manifest.Collect<Manifest, Statement>().SingleOrDefault(st => st.Language.LanguageCode == Thread.CurrentThread.CurrentCulture.Name.Substring(0, 2));
			if (statement != null) {
				return statement.Text;
			} else {
				return null;
			}

		}

	}
}
