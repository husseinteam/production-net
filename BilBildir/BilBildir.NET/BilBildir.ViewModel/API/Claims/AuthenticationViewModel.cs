﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilBildir.Model.Entities.POCO;
using BilBildir.ViewModel.API.Base;
using LLF.Annotations.ViewModel;

namespace BilBildir.ViewModel.API.Claims {
	public class AuthenticationViewModel : BaseAPIViewModel {

		public AuthenticationViewModel(Actor a)
			: this() {
			UserName = a.Author.Email;
			Password = "a1q2w3e";
		}

		public AuthenticationViewModel() {

		}

		[ToUI(ValidationErrorKey = "UserNameNull")]
		public String UserName { get; set; }
		[ToUI(ValidationErrorKey = "PasswordNull")]
		public String Password { get; set; }

	}
}
