﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilBildir.Model.Entities.POCO;
using BilBildir.DataSeed.Catalog;
using LLF.Extensions;
using LLF.Data.Containers;
using LLF.Abstract.Data.Engine;

namespace BilBildir.DataSeed.Static {

	public static class SEnterprise {

		public static Exception GenerateModels(Boolean seed = false) {

			var expt = DependencyContainer.Take<IDatabaseEngine>()
			   .EngineFor<Comment>()
			   .EngineFor<Carousel>()
			   .EngineFor<Profile>()
			   .EngineFor<Author>()
			   .EngineFor<Rank>()
			   .EngineFor<Actor>()
			   .EngineFor<Topic>()
			   .EngineFor<Question>()
			   .EngineFor<Answer>()
			   .EngineFor<Tag>()
			   .EngineFor<Category>()
			   .EngineFor<Bid>()
			   .EngineFor<MessageItem>()
			   .EngineFor<Notification>()
			   .EngineFor<TaskItem>()
			   .EngineFor<AppConfig>()
			   .EngineFor<BidCategory>()
			   .EngineFor<ToDoItem>()
			   .EngineFor<Language>()
			   .EngineFor<Statement>()
			   .EngineFor<Manifest>()
			   .GenerateModels();
            if (seed) {
                SeedAll();
            }

			return expt;

		}

		public static IList<Exception> SeedAll() {

			IList<Exception> exptList = new List<Exception>();

			AppConfigSeed.GetResponse().DestroySeed().PersistSeed(ref exptList);
			LanguageSeed.GetResponse().DestroySeed().PersistSeed(ref exptList);

			CommentSeed.GetResponse().DestroySeed().PersistSeed(ref exptList);
			CarouselSeed.GetResponse().DestroySeed().PersistSeed(ref exptList);

			var actors = ActorSeed.GetResponse().DestroySeed().PersistSeed(ref exptList).SeedList;
			var topics = TopicSeed.GetResponse().DestroySeed().PersistSeed(ref exptList).SeedList;

			var questions = QuestionSeed.GetResponse(actors, topics).DestroySeed().PersistSeed(ref exptList).SeedList;
			AnswerSeed.GetResponse(actors, questions).DestroySeed().PersistSeed(ref exptList);
			TagSeed.GetResponse(questions).DestroySeed().PersistSeed(ref exptList);

			NotificationSeed.GetResponse(actors).DestroySeed().PersistSeed(ref exptList);
			MessageSeed.GetResponse(actors).DestroySeed().PersistSeed(ref exptList);

			var bids = BidSeed.GetResponse().DestroySeed().PersistSeed(ref exptList).SeedList;
			var categories = CategorySeed.GetResponse().DestroySeed().PersistSeed(ref exptList).SeedList;
			BidCategorySeed.GetResponse(bids, categories).DestroySeed().PersistSeed(ref exptList);

			var tasks = TaskItemSeed.GetResponse(bids, actors.First()).DestroySeed().PersistSeed(ref exptList).SeedList;
			ToDoItemSeed.GetResponse(tasks).DestroySeed().PersistSeed(ref exptList);

			var message = new StringBuilder();
			exptList.Where(e => e != null).Enumerate(ex => message.Append(ex.Message));
			return exptList;

		}

	}
}
