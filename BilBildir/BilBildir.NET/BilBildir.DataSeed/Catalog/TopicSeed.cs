﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilBildir.Model.Entities.Enums;
using BilBildir.Model.Entities.POCO;


using LLF.Abstract.Data.Engine;
using LLF.Extensions;
using LLF.Data.Tools;

namespace BilBildir.DataSeed.Catalog {
	public static class TopicSeed {

		public static ISeedResponse<Topic> GetResponse() {


			var codes = new String[] { "science", "maths", "geometry", "geography", "biology" };
			var descs = new String[] {
                "Science",
                "Mathematics",
                "Geometrics",
                "Geographical Sciences",
                "Biological Sciences" };

			var collegeTopic = new Topic() {
				Code = "college",
				Description = "College Lessons Topics",
				ParentTopic = null,
			};

			ISeedResponse<Topic> response = SeedProvider.SeedInstanceOf<Topic>(descs.Count(),
				(i) => new Topic() {
					Code = codes.ElementAt(i),
					Description = descs.ElementAt(i),
					ParentTopic = collegeTopic
				});
			return response;

		}

	}
}
