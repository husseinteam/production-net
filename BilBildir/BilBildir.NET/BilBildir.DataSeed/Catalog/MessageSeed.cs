﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilBildir.Model.Entities.Enums;
using BilBildir.Model.Entities.POCO;


using LLF.Abstract.Data.Engine;
using LLF.Extensions;
using LLF.Data.Tools;

namespace BilBildir.DataSeed.Catalog {
	public static class MessageSeed {

		public static ISeedResponse<MessageItem> GetResponse(IEnumerable<Actor> actors) {

			var messageTimes = new DateTime[] { 10.DaysAgo(), 54.DaysAgo(), 3.MinutesAgo(), 4.MonthsAgo() };
			var messageTexts = new String[] { 
				"Lorem amet elit, et al commore", 
				"Lorem amet consectetur adipiscing elit, et al commore", 
				"Lorem ipsum dolor sit elit, et al commore", 
				"Lorem ipsum dolor sit amet consectetur, et al commore"
			};

			ISeedResponse<MessageItem> response = null;

			foreach (var at in actors) {
				var inorder = SeedProvider.SeedInstanceOf<MessageItem>(messageTimes.Count(),
						(i) => new MessageItem() {
							Actor = at,
							MessagedOn = messageTimes.ElementAt(i),
							MessageText = messageTexts.ElementAt(i)
						});
				response = response == null ? inorder : response.Extend(inorder);
			}
			return response;

		}

	}
}
