﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilBildir.Model.Entities.Enums;
using BilBildir.Model.Entities.POCO;

using LLF.Abstract.Data.Engine;
using LLF.Extensions;
using LLF.Data.Tools;

namespace BilBildir.DataSeed.Catalog {
	public static class BidCategorySeed {

		public static ISeedResponse<BidCategory> GetResponse(IEnumerable<Bid> bids, IEnumerable<Category> categories) {

			var response = SeedProvider.SeedInstanceOf<BidCategory>(5,
						(i) => new BidCategory() {
							Bid = bids.Cycle(i),
							Category = categories.Cycle(i)
						});
			return response;

		}

	}
}
