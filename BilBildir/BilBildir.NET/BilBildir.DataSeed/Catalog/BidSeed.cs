﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilBildir.Model.Entities.Enums;
using BilBildir.Model.Entities.POCO;
using BilBildir.Definitions.Resources;

using LLF.Abstract.Data.Engine;
using LLF.Extensions;
using LLF.Data.Tools;

namespace BilBildir.DataSeed.Catalog {
    public static class BidSeed {

        public static ISeedResponse<Bid> GetResponse() {

            var deadlines = new DateTime[] { 10.DaysLater(), 54.DaysLater(), 3.WeeksLater(), 4.MonthsLater(), 2.HoursLater() };
            var prices = new Decimal[] { 1200M, 130M, 120M, 4500M, 1000M };
            var statuses = new EBidStatus[] { EBidStatus.Issued, EBidStatus.Pending, EBidStatus.Pending, EBidStatus.Pending, EBidStatus.Accepted };


            return SeedProvider.SeedInstanceOf<Bid>(deadlines.Count(),
                        (i) => new Bid() {
                            Deadline = deadlines.ElementAt(i),
                            BiddedOn = DateTime.Now,
                            BiddedPrice = prices.ElementAt(i),
                            BidDescription = Demonstration.lipsum,
                            Status = statuses.ElementAt(i)
                        });
        }

    }
}
