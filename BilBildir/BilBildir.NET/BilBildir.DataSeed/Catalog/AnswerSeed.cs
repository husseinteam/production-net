﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilBildir.Model.Entities.Enums;
using BilBildir.Model.Entities.POCO;

using LLF.Abstract.Data.Engine;
using LLF.Extensions;
using LLF.Data.Tools;

namespace BilBildir.DataSeed.Catalog {
	public static class AnswerSeed {

		public static ISeedResponse<Answer> GetResponse(IEnumerable<Actor> actors, IEnumerable<Question> questions) {

			var states = new EAnswerState[] { EAnswerState.Candidate, EAnswerState.Draft, EAnswerState.Draft, EAnswerState.Approved };
			var scripts = new String[] { 
				"Lorem amet elit, et al commore", 
				"Lorem amet consectetur adipiscing elit, et al commore", 
				"Lorem ipsum dolor sit elit, et al commore", 
				"Lorem ipsum dolor sit amet consectetur, et al commore"
			};

			var response = SeedProvider.SeedInstanceOf<Answer>(states.Count(),
						(i) => new Answer() {
							CodeToken = "{0}".GenerateToken(),
							Script = scripts.ElementAt(i),
							State = states.ElementAt(i),
							Actor = actors.Cycle(i),
							Question = questions.Cycle(i)
						});
			return response;

		}

	}
}
