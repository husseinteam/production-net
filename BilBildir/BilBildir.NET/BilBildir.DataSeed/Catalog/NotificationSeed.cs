﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilBildir.Model.Entities.Enums;
using BilBildir.Model.Entities.POCO;


using LLF.Abstract.Data.Engine;
using LLF.Extensions;
using LLF.Data.Tools;

namespace BilBildir.DataSeed.Catalog {
	public static class NotificationSeed {

		public static ISeedResponse<Notification> GetResponse(IEnumerable<Actor> actors) {

			var notificationTimes = new DateTime[] { 3.DaysAgo(), 2.DaysAgo(), 7.MinutesAgo(), 1.MonthsAgo() };
			var messageTexts = new String[] { 
				"Lorem amet elit, et al commore", 
				"Lorem amet consectetur adipiscing elit, et al commore", 
				"Lorem ipsum dolor sit elit, et al commore", 
				"Lorem ipsum dolor sit amet consectetur, et al commore"
			};
			var colors = new String[] { "green", "red", "yellow", "blue", "blue" };
			var iconClasses = new String[] { "icon-user", "icon-comment-alt", "icon-shopping-cart", "icon-user" };

			ISeedResponse<Notification> response = null;

			foreach (var at in actors) {
				var inorder = SeedProvider.SeedInstanceOf<Notification>(notificationTimes.Count(),
						(i) => new Notification() {
							Actor = at,
							IconClass = iconClasses.ElementAt(i),
							ColorClass = colors.ElementAt(i),
							Message = messageTexts.ElementAt(i),
							Time = notificationTimes.ElementAt(i)
						});
				response = response == null ? inorder : response.Extend(inorder);
			}
			return response;

		}

	}
}
