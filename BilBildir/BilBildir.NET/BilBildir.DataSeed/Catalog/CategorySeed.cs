﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilBildir.Model.Entities.Enums;
using BilBildir.Model.Entities.POCO;
using LLF.Abstract.Data.Engine;
using LLF.Extensions;
using LLF.Data.Tools;

namespace BilBildir.DataSeed.Catalog {
	public static class CategorySeed {

		public static ISeedResponse<Category> GetResponse() {

			var codes = new String[] { "biology", "chemistry", "history", "maths", "translation", "geography" };
			var descsTR = new String[] {
                "Biyoloji Konu Başlığı",
                "Kimya Konu Başlığı",
                "Tarih Konu Başlığı",
                "Matematik Konu Başlığı",
                "Çeviri Konu Başlığı",
                "Coğrafya Konu Başlığı" };
			var descsEN = new String[] {
                "Biology",
                "Chemistry",
                "History",
                "Mathematics",
                "Translation",
                "Geography" };

			var langTR = new Language().SelectOne(new { LanguageCode = "tr" } );
            var langEN = new Language().SelectOne(new { LanguageCode = "en", RegionCode = "US" });
			ISeedResponse<Category> response = SeedProvider.SeedInstanceOf<Category>(codes.Count(),
				(i) => new Category() {
					Code = codes[i.Modulus(codes.Length)],
					DescriptionManifest = new Manifest() {
						Code = codes.ElementAt(i)
					}.Assign(mf => mf.Statements
						.Extend(new Statement() { Language = langTR, Text = descsTR.ElementAt(i), Manifest = mf })
						.Extend(new Statement() { Language = langEN, Text = descsEN.ElementAt(i), Manifest = mf })),
				}, ctg => ctg.DescriptionManifest.UpsertCollections<Statement>());
			return response;

		}

	}
}
