﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilBildir.Model.Entities.Enums;
using BilBildir.Model.Entities.POCO;
using LLF.Abstract.Data.Engine;
using LLF.Extensions;
using LLF.Data.Tools;

namespace BilBildir.DataSeed.Catalog {
	public static class CommentSeed {

		public static ISeedResponse<Comment> GetResponse() {

			var codes = new String[] { "andys", "amelias", "johnn", "amelian" };
			var commentTimes = new DateTime[] { 2.DaysAgo(), 3.DaysAgo(), 10.MinutesAgo(), 5.MonthsAgo() };

			var langTR = new Language().SelectOne(new { LanguageCode = "tr" });
            var langEN = new Language().SelectOne(new { LanguageCode = "en", RegionCode = "US" });
			var identities = new String[] {
                "Andy Smith",
                "Amelia Smith",
                "John Novak",
                "Amelia Nut"
			};
			var commentsTR = new String[] { 
				"Bu siteyi daha önce neden göremedim?", 
				"Harika Bir Portal!", 
				"Beğendim, Tasiye Ederim", 
				"Muhteşem!"
			};
			var commentsEN = new String[] { 
				"Why didn't I get to this portal before??", 
				"This portal is Fascinating!", 
				"I liked it, adviced.", 
				"Great!"
			};

			ISeedResponse<Comment> response = SeedProvider.SeedInstanceOf<Comment>(codes.Count(),
				(i) => new Comment() {
                    Active = i == 0,
					CommenterIdentity = identities.ElementAt(i),
					CommentedOn = commentTimes.ElementAt(i),
					DescriptionManifest = new Manifest() {
						Code = codes.ElementAt(i)
					}.Assign(mf => mf.Statements
						.Extend(new Statement() { Language = langTR, Text = commentsTR.ElementAt(i), Manifest = mf })
						.Extend(new Statement() { Language = langEN, Text = commentsEN.ElementAt(i), Manifest = mf }))
				}, comment => comment.DescriptionManifest.UpsertCollections<Statement>());
			return response;

		}

	}
}
