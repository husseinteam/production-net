﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilBildir.Model.Entities.Enums;
using BilBildir.Model.Entities.POCO;

using LLF.Abstract.Data.Engine;
using LLF.Extensions;
using LLF.Data.Tools;

namespace BilBildir.DataSeed.Catalog {
	public static class ToDoItemSeed {

		public static ISeedResponse<ToDoItem> GetResponse(IEnumerable<TaskItem> tasks) {

			var titles = new String[] { "Windows Phone 8 App", "New frontend layout", "Hire developers" };
			var emergencies = new EToDoItemEmergency[] { EToDoItemEmergency.Today, EToDoItemEmergency.Tomorrow, EToDoItemEmergency.ThisWeek };

			ISeedResponse<ToDoItem> response = null;

			foreach (var task in tasks) {
				var inorder = SeedProvider.SeedInstanceOf<ToDoItem>(titles.Count(),
						(i) => new ToDoItem() {
							Title = titles.ElementAt(i),
							ToDoEmergency = emergencies.ElementAt(i),
							TaskItem = task
						});
				response = response == null ? inorder : response.Extend(inorder);
			}
			return response;

		}

	}
}
