﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using LLF.Abstract.Data.Engine;
using BilBildir.Model.Entities.Enums;
using BilBildir.ViewModel.Extensions;
using BilBildir.Model.Entities.POCO;
using BilBildir.Definitions.Locals;
using BilBildir.ViewModel.UI.User;
using LLF.Extensions;
using BilBildir.ViewModel.UI.User.RowModels;

namespace BilBildir.Actors.Core {

	
	public static class TakeWhere {

		
		public static IEnumerable<MyBidsRowModel> TakeAvaliableBids() {

            var all = new TaskItem().SelectMany(null,
                new { Status = EBidStatus.Suspended });
            return all.Select(ta => new MyBidsRowModel(ta));

        }

		
		public static IEnumerable<MyBidsRowModel> TakeApplications(ETaskStatus status,
            Int64 applicantID) {

            var all = new TaskItem().SelectMany(
                new { Status = status, ApplicantID = applicantID });
            if (all != null) {
                return all.Select(bapp => new MyBidsRowModel(bapp));
            } else {
				return new MyBidsRowModel[0];
			}

        }

		
		public static Actor TakeTheActor(Int64 actorID) {

            return new Actor().SelectThe(actorID);

        }

		
		public static Actor TakeActorByUserName(String userName) {

			var author = new Author().SelectOne(new { Email = userName });
			if (author != null) {
				return new Actor().SelectOne(new { AuthorID = author.ID });
			} else {
				return null;
			}

		}

		
		public static Actor TakeActorByPasswordResetToken(Guid token) {

			var author = new Author().SelectOne(new { PasswordResetToken = token });
			if (author != null) {
				return new Actor().SelectOne(new { AuthorID = author.ID });
			} else {
				return null;
			}

		}

		
		public static IEnumerable<MessageItem> TakeMessagesOf(Int64 actorID) {

            return new MessageItem().SelectMany(new { ActorID = actorID });

        }

		
		public static IEnumerable<Notification> TakeNotificationsOf(Int64 actorID) {

            return new Notification().SelectMany(new { ActorID = actorID });

        }

		
		public static IEnumerable<MyTasksRowModel> TakeTaskItemsOf(Int64 actorID) {

            return new TaskItem().SelectMany(
                new { ApplicantID = actorID })
				.Select(ta => new MyTasksRowModel(ta));

        }

		
		public static Bid TakeTheBid(long bidID) {

            return new Bid().SelectThe(bidID);

        }


		public static TaskItem TakeTheTaskItem(Int64 actorID, Int64 bidID) {

            return new TaskItem().SelectOne(new { ApplicantID = actorID, BidID = bidID });

        }

		
		public static IEnumerable<Int64> TakeSelectedCategoriesOf(Int64 bidID) {

            return new BidCategory().SelectMany(new { BidID = bidID }).Select(bc => bc.Category).Select(ctg => ctg.ID);

        }

		
		public static IEnumerable<MyPublishedBidsRowModel> TakePublishedBidsOf(Int64 actorID) {

            return new Bid().SelectMany()
                .Select(b => new MyPublishedBidsRowModel(b));

        }

		
		public static IEnumerable<MyTasksViewModel> TakeAssignedTasksOf(Int64 actorID) {

            return new TaskItem().SelectMany()
                .Where(at => at.Applicant.ID == actorID)
                .Select(at => new MyTasksViewModel(at));

        }
		
		public static IEnumerable<dynamic> TakeAllCategories() {

			return new Category().SelectMany().Select(ctg => new { ID = ctg.ID, Code = ctg.Code, Text = ctg.DescriptionManifest.GetTextGlobalized() });

        }

		
		public static IEnumerable<MyApplicantsRowModel> TakeApplicantsOf(Int64 bidID) {

            return new TaskItem().SelectMany(new { BidID = bidID })
				.Select(ba => new MyApplicantsRowModel(ba));
        }

		
		public static TaskItem TakeTheTask(Int64 taskID) {

            return new TaskItem().SelectThe(taskID);

        }

		
		public static ToDoItem TakeTheToDoItem(Int64 todoItemID) {

            return new ToDoItem().SelectThe(todoItemID);

        }

		
		public static Int32 TakeExistingBidCount() {

			return TakeAvaliableBids().Count();

		}

		
		public static Int32 TakePublishedBidCount(Int64 actorID) {

			var allBidsOfActorCount = new TaskItem().SelectCount<Int32>(
				new { ApplicantID = actorID });
			return allBidsOfActorCount;

		}
	}

}
