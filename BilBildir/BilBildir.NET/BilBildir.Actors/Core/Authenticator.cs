﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Abstract.Data.Engine;
using BilBildir.Model.Entities.Enums;
using BilBildir.Model.Entities.POCO;
using BilBildir.Definitions.Locals;
using BilBildir.Definitions.Static;
using BilBildir.ViewModel.UI.Auth;
using BilBildir.ViewModel.UI.User;
using BilBildir.ViewModel.Extensions;
using BilBildir.Model.TransferObjects;
using LLF.Extensions;
using System.Net.Mail;
using System.Net;
using BilBildir.ViewModel.API.Claims;


namespace BilBildir.Actors.Core {
	
	public static class Authenticator {

		#region Private

		private static void ResponseFailed(IResultObject robj, string message) {
			robj.Message = message;
			robj.Success = false;
			robj.ModalTitle = Messages.ModalTitleValidationFailed;
		}

		#endregion

        #region Web MVC

		public static IResultObject Login(LoginViewModel vm) {

			IResultObject robj = new ResultObject();
			var validated = vm.Validate();
            if (validated.IsValid) {
				var fromdb = new Author().SelectOne(new {
					Email = vm.UserName,
					PasswordHash = vm.Password.ComputeHash()
				});
				robj.Success = fromdb != null;
				if (robj.Success) {
					fromdb.PasswordResetRequested = false;
					fromdb.PasswordResetToken = null;
					robj.Success = fromdb.UpdateSelf().Fault.IsUnassigned();
					if (robj.Success) {
						robj.Message = Messages.WelcomeHomeUser
							.PostFormat(fromdb.Profile.Identity);
						robj.PrimaryKey = new Actor()
							.SelectOne(new { AuthorID = fromdb.ID }).ID;
					} else {
						robj.Message = Messages.SevereDbUpdateError;
					}
				} else {
					robj.Message = Messages.UserCredentialsNotExist;
				}
				robj.ModalTitle = robj.Success ?
					Messages.ModalTitleSuccess : Messages.ModalTitleFail;
			} else {
                ResponseFailed(robj, validated.FinalMessage);
			}
			return robj;

		}

		public static IResultObject Signup(SignupViewModel vm) {

			IResultObject robj = new ResultObject();
            var validated = vm.Validate();
            if (validated.IsValid) {
                if (new Author().SelectCount<Int32>(new { Email = vm.UserName }) == 0) {
                    var min = new AppConfig().SelectOne(
                                new { Attribute = "MinimumBidCountToApply" })
                                .Value.ToType<Int32>();
                    var max = new AppConfig().SelectOne(
                        new { Attribute = "MaximumBidCountToCloseApplications" })
                        .Value.ToType<Int32>();
                    var newactor = new Actor() {
                        Rank = new Rank() {
                            MaximumAllowedQuestion = max,
                            MinimumAllowedQuestion = min,
                            RankRatio = 0.1f,
                            Reliability = EReliability.Uncertain
                        },
                        Author = new Author() {
                            Email = vm.UserName,
                            PasswordHash = vm.Password.ComputeHash(),
                            Profile = new Profile() {
                                Identity = vm.Identity,
                                AvatarPath = vm.AvatarPath,
                                SecretQuestion = "",
                                AnswerOfSecretQuestion = ""
                            }
                        }
                    };
                    robj.Success = newactor.InsertSelf().Fault.IsUnassigned();
                    if (robj.Success) {
                        robj.Message = Messages.WelcomeHomeUser.PostFormat(vm.Identity);
                        robj.PrimaryKey = newactor.ID;
                    } else {
                        robj.Message = Messages.SignupUnsuccessful;
                    }
                } else {
                    robj.Success = false;
                    robj.Message = Messages.UserExists.PostFormat(vm.UserName);
                }
				robj.ModalTitle = robj.Success ?
					Messages.ModalTitleSuccess : Messages.ModalTitleFail;
			} else {
                ResponseFailed(robj, validated.FinalMessage);
			}
			return robj;

		}


		public static async Task<IResultObject> Recover(RecoverViewModel vm, NetworkCredential credentials, MailAddress from) {

			IResultObject robj = new ResultObject();
            var validated = vm.Validate();
            if (validated.IsValid) {
                var actor = TakeWhere.TakeActorByUserName(vm.Email);
				if (actor != null) {
					var to = new MailAddressCollection().Assign(coll => coll.Add(vm.Email));
					var subject = MailSender.ComposeMailSubjectForRecovery(actor, vm.SiteName);
					var body = MailSender.ComposeMailBodyForRecovery(actor, vm);
					robj = await MailSender.SendAsync(credentials, from, to, subject, body);
				} else {
					robj.ShowModal = false;
					robj.Success = false;
					robj.Message = Messages.RecoveryFailedWithEmail.PostFormat(vm.Email);
				}
			} else {
                ResponseFailed(robj, validated.FinalMessage);
			}
			return robj;

		}


		public static IResultObject ValidatePasswordResetToken(ResetPasswordViewModel vm, Object token) {

			IResultObject robj = new ResultObject();
			if (token != null) {
				Guid guid = Guid.Empty;
				if (Guid.TryParse(token.ToString(), out guid)) {
					var actor = TakeWhere.TakeActorByPasswordResetToken(guid);
					if (actor != null && actor.Author.PasswordResetRequested) {
						robj.Message = Messages.PasswordResetTokenIsValid;
						robj.Success = true;
					} else {
						robj.Message = Messages.PasswordResetTokenExpired;
						robj.Success = false;
					}
				} else {
					robj.Message = Messages.PasswordResetTokenIsNotLegal;
					robj.Success = false;
				}
			} else {
				robj.Message = Messages.PasswordResetTokenNullError;
				robj.Success = false;
			}
			robj.ModalTitle = robj.Success ?
					Messages.ModalTitleSuccess : Messages.ModalTitleFail;
			return robj;

		}


		public static IResultObject ResetUserPassword(ResetPasswordViewModel vm, Object token) {

			IResultObject robj = new ResultObject();
			if (token != null) {
				Guid guid = Guid.Empty;
				if (Guid.TryParse(token.ToString(), out guid)) {
                    var validated = vm.Validate();
                    if (validated.IsValid) {
                        var actor = TakeWhere.TakeActorByPasswordResetToken(guid);
						if (actor != null && actor.Author.PasswordResetRequested) {
							actor.Author.PasswordHash = vm.Password.ComputeHash();
							actor.Author.PasswordResetToken = null;
							actor.Author.PasswordResetRequested = false;
							if (actor.UpdateSelf().Fault.IsUnassigned()) {
								robj.Message = Messages.PasswordResetSucceeded;
								robj.Success = true;
							} else {
								robj.Message = Messages.PasswordResetFailed;
								robj.Success = false;
							}
						} else {
							robj.Message = Messages.PasswordResetTokenExpired;
							robj.Success = false;
						}
					} else {
                        robj.Message = validated.FinalMessage;
						robj.Success = false;
					}
				} else {
					robj.Message = Messages.PasswordResetTokenIsNotLegal;
					robj.Success = false;
				}
			} else {
				robj.Message = Messages.PasswordResetTokenNullError;
				robj.Success = false;
			}
			robj.ModalTitle = robj.Success ?
					Messages.ModalTitleSuccess : Messages.ModalTitleFail;
			return robj;

		}


		public static IResultObject<String> AvatarNotSavedResult() {

			return new GenericResultObject<String>() {
				Message = Messages.AvatarNotSaved,
				Success = false,
				ShowModal = true,
				ModalTitle = Messages.ModalTitleFail,
				Result = "/Images/login.png"
			};

		}


		public static IResultObject<String> SaveAvatar(Int64 actorID, String avatarPath) {

			avatarPath = avatarPath.Substring(avatarPath.IndexOf(@"\Images\ClientImages\")).Replace("\\", "/");

			IResultObject<String> robj = new GenericResultObject<String>();
			var actor = new Actor().SelectThe(actorID);
			actor.Author.Profile.AvatarPath = avatarPath;
			robj.Success = actor.UpdateSelf().Fault.IsUnassigned();

			if (robj.Success) {
				robj.Message = Messages.AvatarSavedSuccessfully;
				robj.ShowModal = true;
			} else {
				robj.Message = Messages.AvatarSaveFailed;
			}
			robj.ModalTitle = robj.Success ?
				Messages.ModalTitleSuccess : Messages.ModalTitleFail;
			return robj.Assign(r => r.Result = avatarPath);

		}


		public static IResultObject SetProfile(ProfileFormViewModel vm, Int64 id) {

			IResultObject robj = new ResultObject();
            var validated = vm.Validate();
            if (validated.IsValid) {
                var fromdb = new Actor().SelectThe(id);
				robj.Success = fromdb != null;

				fromdb.Author.Profile.Identity = vm.Identity;
				fromdb.Author.Profile.AvatarPath = vm.AvatarPath;
				fromdb.Author.Profile.SecretQuestion = vm.SecretQuestion;
				fromdb.Author.Profile.AnswerOfSecretQuestion = vm.AnswerOfSecretQuestion;
				fromdb.Author.PasswordHash = vm.Password.ComputeHash();

				robj.Success = robj.Success && fromdb.UpdateSelf().Fault.IsUnassigned();

				if (robj.Success) {
					robj.Message = Messages.ProfileSuccessfullySaved;
				} else {
					robj.Message = Messages.ProfileCoudntBeSaved;
				}
				robj.ModalTitle = robj.Success ?
					Messages.ModalTitleSuccess : Messages.ModalTitleFail;
			} else {
                ResponseFailed(robj, validated.FinalMessage);
			}
			return robj;

		}


		public static String GenerateResetQueryStringFor(Actor actor, String resetPasswordUrl) {

			actor.Author.PasswordResetRequested = true;
			actor.Author.PasswordResetToken = Guid.NewGuid();
			if (actor.UpdateSelf().Fault.IsUnassigned()) {
				return "{0}/{1}".PostFormat(resetPasswordUrl, actor.Author.PasswordResetToken);
			} else {
				return null;
			}
		}

		#endregion

	}
}
