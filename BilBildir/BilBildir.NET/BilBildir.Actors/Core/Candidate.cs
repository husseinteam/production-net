﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilBildir.Model.Entities.Enums;
using BilBildir.Model.Entities.POCO;
using LLF.Extensions;



namespace BilBildir.Actors.Core {
	
	public class Candidate {

		
		public void Answer(Answer aws, Question qs) {

            aws.State = EAnswerState.Draft;

            aws.Question = qs;
            aws.UpdateSelf();

            qs.State = EQuestionState.Issued;
            qs.UpdateSelf();

        }

    }
}
