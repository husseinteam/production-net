﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using BilBildir.Model.TransferObjects;
using SendGrid;
using LLF.Extensions;
using System.Threading.Tasks;
using BilBildir.Definitions.Locals;
using BilBildir.Model.Entities.POCO;
using BilBildir.Definitions.Resources;
using System.Text;
using BilBildir.ViewModel.UI.Auth;
using System.Net.Mime;



namespace BilBildir.Actors.Core {
	
	public static class MailSender {

        public static async Task<dynamic> SendAsync(NetworkCredential credentials, MailAddress from, MailAddressCollection to, String subject, String bodyHtml) {

            ISendGrid myMessage = new SendGridMessage();
            myMessage.From = from;

            // Add multiple addresses to the To field.
            var recipients = (from t in to.GetEnumerator().MakeQuery() select t.Address).ToList();
            myMessage.AddTo(recipients);

            myMessage.Subject = subject;

            //Add the HTML and Text bodies
            myMessage.Html = bodyHtml;
            //myMessage.Text = "Hello World plain text!";

            var transportWeb = new Web(credentials);

            IResultObject robj = null;
            try {
                // Send the email.
                // You can also use the **DeliverAsync** method, which returns an awaitable task.
                await transportWeb.DeliverAsync(myMessage);
                robj = new ResultObject() {
                    Message = Messages.MailSendSuccess,
                    Success = true,
                    ModalTitle = Messages.ModalTitleSuccess,
                };

            } catch {
                robj = new ResultObject() {
                    Message = Messages.MailSendFail,
                    Success = false,
                    ShowModal = true,
                    ModalTitle = Messages.ModalTitleFail,
                };

            }
            return robj;

        }

		public static IResultObject Send(NetworkCredential credentials, MailAddress from, MailAddressCollection to, String subject, String bodyHtml) {

			var message = new MailMessage();
			message.From = from;

			// Add multiple addresses to the To field.
			var recipients = (from t in to.GetEnumerator().MakeQuery() select t.Address).ToList();
			recipients.ForEach(recp => message.To.Add(recp));

			message.Subject = subject;

			//Add the HTML and Text bodies
			message.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(bodyHtml, null, MediaTypeNames.Text.Html));
			//myMessage.Text = "Hello World plain text!";

			var smtpClient = new SmtpClient("smtp.sendgrid.net", Convert.ToInt32(587));
			smtpClient.Credentials = credentials;



            IResultObject robj = null;
			try {
				// Send the email.
				// You can also use the **DeliverAsync** method, which returns an awaitable task.
				smtpClient.Send(message);
				robj = new ResultObject() {
					Message = Messages.MailSendSuccess,
					Success = true,
					ModalTitle = Messages.ModalTitleSuccess,
				};

			} catch {
				robj = new ResultObject() {
					Message = Messages.MailSendFail,
					Success = false,
					ShowModal = true,
					ModalTitle = Messages.ModalTitleFail,
				};

			}
			return robj;

		}

		
		public static String ComposeMailBodyForRecovery(Actor actor, RecoverViewModel vm) {
			
			var body = MailTemplates.TemplateText
				.Replace("##identity##", actor.Author.Profile.Identity)
				.Replace("##sitelogo##", vm.SiteLogoLink)
				.Replace("##sitename##", vm.SiteName)
				.Replace("##reset_link##", Authenticator.GenerateResetQueryStringFor(actor, vm.ResetPasswordLink))
				.Replace("##siteslogan##", vm.SiteSlogan)
				.Replace("##recovery_link##", vm.RecoveryLink)
				.Replace("##login_link##", vm.LoginLink)
				.Replace("##signup_link##", vm.SignupLink)
				.Replace("##terms_link##", vm.TermsLink)
				.Replace("##privacy_link##", vm.PrivacyLink)
				.Replace("##unsubscribe_link##", vm.UnsubscribeLink)
				.Replace("##company_phone##", vm.CompanyContactPhone)
				.Replace("##contact_email##", vm.CompanyContactEmail);
			return body;

		}

		
		public static String ComposeMailSubjectForRecovery(Actor actor, String sitename) {

			return MailTemplates.MailSubjectForRecovery.Replace("##identity##", actor.Author.Profile.Identity).Replace("##sitename##", sitename);

		}

	}
}
