﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilBildir.Model.Entities.Enums;
using BilBildir.ViewModel.Extensions;
using BilBildir.Model.Entities.POCO;
using BilBildir.Definitions.Locals;
using BilBildir.Definitions.Static;
using BilBildir.ViewModel.UI.User;
using BilBildir.Model.TransferObjects;
using LLF.Extensions;

namespace BilBildir.Actors.Core {
    public static class Taskifier {
        public static IResultObject AddToDoItem(TheTaskViewModel vm, TaskItem at) {

            IResultObject robj = new ResultObject();
            var validated = vm.Validate();
            if (validated.IsValid) {
                var todoItem = new ToDoItem() {
                    ToDoEmergency = vm.EmergencyStatus.ToEnum<EToDoItemEmergency>(k => k.GetStatus()),
                    Title = vm.ToDoTitle,
                    TaskItem = at
                }.InsertSelf();
                robj.Success = todoItem.Fault.IsUnassigned();
                if (robj.Success) {
                    robj.Message = Messages.ToDoItemAddedSuccessfully;
					robj.Redirect = "postback";
                    robj.PrimaryKey = todoItem.ID;
                } else {
                    robj.Message = Messages.ToDoItemAddFailed;
                }
                robj.ModalTitle = robj.Success ?
                    Messages.ModalTitleSuccess : Messages.ModalTitleFail;
            } else {
                robj.Message = validated.FinalMessage;
                robj.Success = false;
                robj.ModalTitle = Messages.ModalTitleValidationFailed;
            }
            return robj;

        }

        public static IResultObject RemoveToDoItem(ToDoItem todoItem) {

            IResultObject robj = new ResultObject();
            robj.Success = todoItem.DeleteSelf().Fault.IsUnassigned();
            if (!robj.Success) {
                robj.Redirect = "postback";
                robj.Message = Messages.ToDoItemRemoveFailed;
                robj.ModalTitle = Messages.ModalTitleFail;
            }
            return robj;

        }

        public static IResultObject UpdateTask(TheTaskViewModel vm, TaskItem at) {

            IResultObject robj = new ResultObject();
            at.Title = vm.Title;
            at.Percent = vm.CompletionPercent / 100f;
            at.Status = at.Percent == 1.0f ? ETaskStatus.Completed :
                at.Status == ETaskStatus.Completed ? ETaskStatus.Applied : at.Status;
            robj.Success = at.UpdateSelf().Fault.IsUnassigned();
            if (robj.Success) {
                robj.Message = Messages.UpdateTaskDone;
                robj.ModalTitle = Messages.ModalTitleSuccess;
            } else {
                robj.Message = Messages.UpdateTaskFailed;
                robj.ModalTitle = Messages.ModalTitleFail;
            }
            return robj;

        }

        public static IResultObject CompleteTask(TaskItem at) {

            IResultObject robj = new ResultObject();
            at.Percent = 1.0f;
            at.Status = ETaskStatus.Completed;
            robj.Success = at.UpdateSelf().Fault.IsUnassigned();
            if (robj.Success) {
                
                robj.Message = Messages.CompleteTaskDone;
                robj.ModalTitle = Messages.ModalTitleSuccess;
            } else {
                robj.Message = Messages.CompleteTaskFailed;
                robj.ModalTitle = Messages.ModalTitleFail;
            }
            return robj;

        }
    }
}
