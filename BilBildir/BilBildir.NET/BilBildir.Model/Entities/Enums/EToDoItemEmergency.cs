﻿using System.ComponentModel;

namespace BilBildir.Model.Entities.Enums {

    public enum EToDoItemEmergency {

        [Description("important")]
        Today = 1,
        [Description("warning")]
        Tomorrow,
        [Description("success")]
        ThisWeek,
        [Description("info")]
        ThisMonth

    }

}