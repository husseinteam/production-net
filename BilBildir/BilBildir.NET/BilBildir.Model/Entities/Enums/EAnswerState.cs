﻿namespace BilBildir.Model.Entities.Enums {

    public enum EAnswerState {

        Draft = 1,
        Published,
        Seen,
        Candidate,
        Approved

    }

}