﻿using System.ComponentModel;

namespace BilBildir.Model.Entities.Enums {

    public enum EBidStatus {


        [Description("warning")]
        Pending = 1,
        [Description("")]
        Suspended,
        [Description("important")]
        Issued,
        [Description("success")]
        Accepted

    }
}