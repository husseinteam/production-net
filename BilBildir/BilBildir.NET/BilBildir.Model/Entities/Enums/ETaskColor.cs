﻿using System.ComponentModel;

namespace BilBildir.Model.Entities.Enums {

    public enum ETaskColor {

        [Description("green")]
        Green = 1,
        [Description("red")]
        Red,
        [Description("yellow")]
        Yellow,
        [Description("greenLight")]
        GreenLight

    }
}