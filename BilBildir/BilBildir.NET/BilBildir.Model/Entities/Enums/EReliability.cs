﻿using System.ComponentModel;

namespace BilBildir.Model.Entities.Enums {
    public enum EReliability {

        [Description("success")]
        Trusted = 1,
        [Description("warning")]
        Uncertain,
        [Description("important")]
        Unstable,
        [Description("warning")]
        Distrusted

    }
}