﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Annotations.Data;
using LLF.Data.Engine;
using LLF.Extensions.Data.Enums;

namespace BilBildir.Model.Entities.POCO {

    [EntityTitle("Notifications", Schema = "Monitoring")]
    public class Notification : BaseEntity<Notification> {

        [DataColumn(EDbType.NVarChar, MaxLength = 32)]
        public String ColorClass { get; set; }
        [DataColumn(EDbType.NVarChar, MaxLength = 32)]
        public String IconClass { get; set; }
        [DataColumn(EDbType.DateTime)]
        public DateTime Time { get; set; }
        [DataColumn(EDbType.NVarChar, MaxLength = 512)]
        public String Message { get; set; }

        [ReferenceKey("ActorID")]
        public Actor Actor { get; set; }

    }
}
