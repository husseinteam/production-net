﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Annotations.Data;
using LLF.Data.Engine;
using LLF.Extensions.Data.Enums;

namespace BilBildir.Model.Entities.POCO {

	[EntityTitle("Languages", Schema = "Translation")]
	public class Language : BaseEntity<Language> {

        [DataColumn(EDbType.Char, MaxLength = 2)]
		public String LanguageCode { get; set; }

        [DataColumn(EDbType.Char, MaxLength = 2)]
		public String RegionCode { get; set; }

    }
}
