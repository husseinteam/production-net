﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Annotations.Data;
using LLF.Data.Engine;
using BilBildir.Model.Entities.Enums;
using LLF.Extensions.Data.Enums;

namespace BilBildir.Model.Entities.POCO {

    [EntityTitle("Bids", Schema = "Auction")]
    public class Bid : BaseEntity<Bid> {

        #region Properties

        [DataColumn(EDbType.DateTime)]
        public DateTime Deadline { get; set; }

        [DataColumn(EDbType.DateTime)]
        public DateTime BiddedOn { get; set; }

        [DataColumn(EDbType.Decimal, MaxLength=15)]
        public Decimal BiddedPrice { get; set; }

        [DataColumn(EDbType.Text)]
        public String BidDescription { get; set; }

        [Enumeration]
        public EBidStatus Status { get; set; }

        #endregion

    }
}
