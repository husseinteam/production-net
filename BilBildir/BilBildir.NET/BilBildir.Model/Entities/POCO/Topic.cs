﻿using System.Collections.Generic;
using LLF.Annotations.Data;
using LLF.Data.Engine;
using LLF.Extensions.Data.Enums;

namespace BilBildir.Model.Entities.POCO {

    [EntityTitle("Topics", Schema = "Mainframe")]
    public class Topic : BaseEntity<Topic> {

        #region Ctor

        public Topic() {

            this.Questions = new List<Question>();

        }

        #endregion

        [DataColumn(EDbType.NVarChar, MaxLength = 64), Unique]
        public string Code { get; set; }

        [DataColumn(EDbType.NVarChar, MaxLength = 64)]
        public string Description { get; set; }

        [ReferenceKey("ParentTopicID"), Nullable]
        public Topic ParentTopic { get; set; }

        [ReferenceCollection]
        public ICollection<Question> Questions { get; set; }

    }
}