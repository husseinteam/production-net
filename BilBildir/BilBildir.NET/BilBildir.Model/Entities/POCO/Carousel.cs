﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Annotations.Data;
using LLF.Data.Engine;
using LLF.Extensions.Data.Enums;

namespace BilBildir.Model.Entities.POCO {

    [EntityTitle("Carousels", Schema = "Mainframe")]
    public class Carousel : BaseEntity<Carousel> {

        [DataColumn(EDbType.Boolean)]
        public Boolean Blank { get; set; }
        [DataColumn(EDbType.NVarChar, MaxLength = 128)]
        public String IntroductionLine1 { get; set; }
        [DataColumn(EDbType.NVarChar, MaxLength = 128)]
        public String IntroductionLine2 { get; set; }
        [DataColumn(EDbType.NVarChar, MaxLength = 128)]
        public String IntroductionLine3 { get; set; }
        [DataColumn(EDbType.NVarChar, MaxLength = 128)]
        public String IntroductionLine4 { get; set; }
        [DataColumn(EDbType.NVarChar, MaxLength = 128)]
        public String IntroductionSubText { get; set; }
        [DataColumn(EDbType.NVarChar, MaxLength = 255), Nullable]
        public String ImageLink { get; set; }


    }
}
