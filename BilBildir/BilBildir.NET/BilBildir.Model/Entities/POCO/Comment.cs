﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Annotations.Data;
using LLF.Data.Engine;
using LLF.Extensions.Data.Enums;

namespace BilBildir.Model.Entities.POCO {

	[EntityTitle("Comments", Schema = "Mainframe")]
	public class Comment : BaseEntity<Comment> {

		[ReferenceKey("ManifestID", IsUniqueIndex = true)]
		public Manifest DescriptionManifest { get; set; }

		[DataColumn(EDbType.DateTime)]
		public DateTime CommentedOn { get; set; }

		[DataColumn(EDbType.NVarChar, MaxLength = 255)]
		public String CommenterIdentity { get; set; }

		[DataColumn(EDbType.Boolean)]
		public Boolean Active { get; set; }
	}
}
