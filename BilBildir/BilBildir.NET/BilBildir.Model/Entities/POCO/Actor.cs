﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Data.Engine;
using LLF.Annotations.Data;

namespace BilBildir.Model.Entities.POCO {

    [EntityTitle("Actors", Schema = "Identity")]
    public class Actor : BaseEntity<Actor> {

        [ReferenceKey("RankID", IsUniqueIndex = true)]
        public Rank Rank { get; set; }

        [ReferenceKey("AuthorID", IsUniqueIndex = true)]
		public Author Author { get; set; }

    }
}
