﻿using System.Collections.Generic;
using LLF.Annotations.Data;
using LLF.Data.Engine;
using LLF.Extensions.Data.Enums;

namespace BilBildir.Model.Entities.POCO {

    [EntityTitle("Categories", Schema = "Auction")]
    public class Category : BaseEntity<Category>{

        [DataColumn(EDbType.NVarChar, MaxLength = 64), Unique]
        public string Code { get; set; }

		[ReferenceKey("DescriptionManifestID")]
		public Manifest DescriptionManifest { get; set; }


    }
}