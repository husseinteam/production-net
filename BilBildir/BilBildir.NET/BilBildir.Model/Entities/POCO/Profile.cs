﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Annotations.Data;
using LLF.Data.Engine;
using LLF.Extensions.Data.Enums;

namespace BilBildir.Model.Entities.POCO {

    [EntityTitle("Profiles", Schema = "Identity")]
    public class Profile : BaseEntity<Profile>{

        [DataColumn(EDbType.NVarChar, MaxLength = 255)]
        public String Identity { get; set; }


        [DataColumn(EDbType.NVarChar, MaxLength = 512), Nullable]
        public String AvatarPath { get; set; }


        [DataColumn(EDbType.NVarChar, MaxLength = 512), Nullable]
        public String SecretQuestion { get; set; }


        [DataColumn(EDbType.NVarChar, MaxLength = 255), Nullable]
        public String AnswerOfSecretQuestion { get; set; }



    }
}
