﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Annotations.Data;
using LLF.Data.Engine;
using BilBildir.Model.Entities.Enums;
using LLF.Extensions.Data.Enums;

namespace BilBildir.Model.Entities.POCO {

    [EntityTitle("Questions", Schema = "Mainframe")]
    public class Question : BaseEntity<Question> {

        #region Ctor

        public Question() {

            this.Answers = new List<Answer>();
            this.Tags = new List<Tag>();

        }

        #endregion

        [ReferenceKey("TopicID")]
        public Topic Topic { get; set; }

        [ReferenceKey("ActorID")]
        public Actor Actor { get; set; }

        [DataColumn(EDbType.NVarChar, MaxLength = 4000)]
        public String Script { get; set; }

        [DataColumn(EDbType.NVarChar, MaxLength = 32), Nullable]
        public String CodeToken { get; set; }

        [Enumeration]
        public EQuestionState State { get; set; }

        [ReferenceCollection]
        public ICollection<Answer> Answers { get; set; }

        [ReferenceCollection]
        public ICollection<Tag> Tags { get; set; }

    }
}