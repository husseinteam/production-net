﻿using System;
using LLF.Annotations.Data;
using LLF.Data.Engine;
using BilBildir.Model.Entities.Enums;
using LLF.Extensions.Data.Enums;

namespace BilBildir.Model.Entities.POCO {

    [EntityTitle("Ranks", Schema = "Identity")]
    public class Rank : BaseEntity<Rank>{

        [Enumeration]
        public EReliability Reliability { get; set; }

        [DataColumn(EDbType.Float)]
        public Single RankRatio { get; set; }

        [DataColumn(EDbType.Int32)]
        public Int32 MinimumAllowedQuestion { get; set; }

        [DataColumn(EDbType.Int32)]
        public Int32 MaximumAllowedQuestion { get; set; }

    }

}