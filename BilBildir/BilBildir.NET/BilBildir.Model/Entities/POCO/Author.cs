﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Annotations.Data;
using LLF.Data.Engine;
using LLF.Extensions.Data.Enums;

namespace BilBildir.Model.Entities.POCO {

    [EntityTitle("Authors", Schema = "Identity")]
    public class Author : BaseEntity<Author> {

        [DataColumn(EDbType.NVarChar, MaxLength = 255), Unique]
        public String Email { get; set; }

        [DataColumn(EDbType.NVarChar, MaxLength = 128)]
        public String PasswordHash { get; set; }

        [ReferenceKey("ProfileID", IsUniqueIndex = true)]
        public Profile Profile { get; set; }

        [DataColumn(EDbType.UniqueIdentifier), Nullable]
		public Guid? PasswordResetToken { get; set; }

		[DataColumn(EDbType.Boolean), Nullable, DefaultValue(false)]
		public Boolean PasswordResetRequested { get; set; }

		[DataColumn(EDbType.NVarChar, MaxLength = 64), Nullable]
		public String AuthenticateToken { get; set; }

		[DataColumn(EDbType.DateTime), Nullable]
		public DateTime TokenExpiresAt { get; set; }


    }
}
