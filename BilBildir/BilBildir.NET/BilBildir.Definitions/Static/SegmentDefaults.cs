﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilBildir.Definitions.Static {
    public static class SegmentDefaults {
        
        public const String Atrium = "atrium";
        public const String Bids = "bids"; 
        public const String EditBid = "editbid";
        public const String MyApplicants = "myapplicants";
        public const String MyTasks = "mytasks";
        public const String MyBids = "mybids";
        public const String SetProfile = "setprofile";
        public const String TheTask = "thetask";
        public const String TheBid = "thebid";
        public const String AddBid = "addbid";
        public const String UserIndex = "index";
        public const String Logout = "logout";
        public const String Login = "login";
        public const String Recover = "recover";
		public const String Terms = "terms";
		public const String Privacy = "privacy";
		public const String Unsubscribe = "unsubscribe";
        public const String SignUp = "signup";
		public const String Repass = "repass";
		public const String Avatarize = "avatarize";
		public const String LocalizedDate = "localizeddate";
		public const String Language = "lang";
		public const String ErrorIndex = "error";
		public const String ErrorNotFound = "notfound";
		public const String InternalServerError = "internal";
        
    }
}
