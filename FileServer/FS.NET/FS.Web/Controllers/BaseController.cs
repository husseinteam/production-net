﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FS.Repository.Data;
using FS.ViewModel;

namespace FS.Web.Controllers {
    public class BaseController : Controller {

        protected UserInfoViewModel _userInfo;

        public BaseController() {
            _userInfo = new UserInfoViewModel();
        }


        [ChildActionOnly]
        public PartialViewResult UserInfo() {

            var _userInfo = new UserInfoViewModel();
            if (Session["msg"] != null) {
                _userInfo.Message = Session["msg"].ToString();
                _userInfo.ContextualClass = (EContextualClass)Convert.ChangeType(
                    Session["ctx"], typeof(EContextualClass));
            }
            Session["msg"] = null;
            Session["ctx"] = null;
            return PartialView("_UserInfo", _userInfo);

        }

        #region Protected

        protected Boolean SignedIn() {

            return Session["uid"] != null;

        }

        protected RedirectResult ToLogin() {

            return Redirect("~/m/login");

        }

        protected RedirectResult ToHome() {

            return Redirect("~/h/index");

        }

        protected Int32 UserID() {

            if (SignedIn()) {
                return Convert.ToInt32(Session["uid"]);
            } else {
                return 0;
            }

        }

        protected void Responsify<TModel>(IActionResponse<TModel> resp) {
            Session["msg"] = resp.Message;
            Session["ctx"] = resp.HasFault ?
                EContextualClass.Danger : EContextualClass.Success;
        }

        #endregion

    }
}