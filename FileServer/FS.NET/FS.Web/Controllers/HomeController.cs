﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FS.Model.Objects;
using FS.Repository.Data;
using FS.ViewModel;

namespace FS.Web.Controllers {
    [RoutePrefix("h")]
    public class HomeController : BaseController {

        private FileRepository _fileManager;
        private UserRepository _userManager;

        public HomeController() {
            _fileManager = new FileRepository();
            _userManager = new UserRepository();
        }

        [Route("index")]
        public ActionResult Index(FileGridViewModel vm) {
            if (!SignedIn()) {
                return ToLogin();
            } else {
                vm = new FileGridViewModel(UserID());
                if (vm.FileItemResponse.HasFault) {
                    Responsify(vm.FileItemResponse);
                    vm.FileItems = new List<FileItem>();
                }
                return View(vm);
            }
        }

        [Route("upload")]
        public ActionResult Upload(FileUploadViewModel vm) {
            if (!SignedIn()) {
                return ToLogin();
            } else {
                if (Request.HttpMethod == "POST") {
                    string deposit = Server.MapPath(@"~/Content/deposit");

                    if (vm.File != null && vm.File.ContentLength > 0) {
                        var resp = _fileManager.AddFileItem(vm.File.ContentLength,
                             vm.File.ContentType, deposit, vm.File.FileName, UserID());
                        if (!resp.HasFault) {
                            vm.File.SaveAs(resp.SingleResponse.ServerPath);
                        }
                        Responsify(resp);
                    } else {
                        Session["msg"] = "Bir Dosya Seçmediniz";
                        Session["ctx"] = EContextualClass.Danger;
                    }
                    return ToHome();
                } else {
                    return ToHome();
                }
            }
        }

        [Route("downloadfile/{argument}")]
        public ActionResult DownloadFile(FileItem fi) {

            if (!SignedIn()) {
                return ToLogin();
            } else {
                if (Request.HttpMethod == "POST") {
                    var fid = Convert.ToInt32(RouteData.Values["argument"]);
                    var resp = _fileManager.GetFileByID(fid);
                    Responsify(resp);
                    if (!resp.HasFault) {
                        return new FilePathResult(resp.SingleResponse.ServerPath, resp.SingleResponse.ContentType) {
                            FileDownloadName = resp.SingleResponse.FileName
                        };
                    } else {
                        return ToHome();
                    }
                } else {
                    return ToHome();
                }
            }

        }

        [Route("displayfile/{argument}")]
        public ActionResult DisplayFile(FileItem fi) {

            if (!SignedIn()) {
                return ToLogin();
            } else {
                if (Request.HttpMethod == "POST") {
                    var fid = Convert.ToInt32(RouteData.Values["argument"]);
                    String contentType;
                    var resp = _fileManager.ViewFileItem(fid, out contentType);
                    Responsify(resp);
                    if (!resp.HasFault) {
                        return new FileStreamResult(resp.SingleResponse, contentType);
                    } else {
                        return ToHome();
                    }
                } else {
                    return ToHome();
                }
            }

        }

        [Route("deletefile/{argument}")]
        public ActionResult DeleteFile(FileItem fi) {

            if (!SignedIn()) {
                return ToLogin();
            } else {
                if (Request.HttpMethod == "POST") {
                    var fid = Convert.ToInt32(RouteData.Values["argument"]);
                    var resp = _fileManager.RemoveFileItem(fid);
                    if (!resp.HasFault) {
                        System.IO.File.Delete(resp.SingleResponse.ServerPath);
                    }
                    Responsify(resp);
                    return ToHome();
                } else {
                    return ToHome();
                }
            }

        }

        [ChildActionOnly]
        public PartialViewResult LeftMenu() {

            var vm = new LeftMenuViewModel();
            return PartialView("_LeftMenu", vm);

        }

        [ChildActionOnly]
        public PartialViewResult LoginInfo() {

            var vm = new LoginInfoViewModel(_userManager, Session["uid"]);
            return PartialView("_LoginInfo", vm);

        }

    }
}