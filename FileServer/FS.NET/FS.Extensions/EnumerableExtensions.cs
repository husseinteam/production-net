﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FS.Extensions {
    public static class EnumerableExtensions {

        public static TItem Cycle<TItem>(this IEnumerable<TItem> self, Int32 k) {

            var i = k % self.Count();
            return self.ElementAt(i);

        }

    }
}
