﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FS.Extensions {
    public static class AssignmentExtensions {

        public static TItem That<TItem>(this TItem self, Action<TItem> act) {

            act(self);
            return self;

        }

        public static Boolean IsUnassigned(this Object item) {
            DateTime reponse1;
            Guid response2;

            var itemNull = item == null;
            if (itemNull) {
                return true;
            } else {
                var itemIsNumber = item.IsNumeric();
                var itemIsDate = DateTime.TryParse(item.ToString(), out reponse1);
                var itemIsGuid = Guid.TryParse(item.ToString(), out response2);
                var itemIsEnum = item.GetType().IsEnum;
                var itemIsString = item.GetType().Equals(typeof(String));

                return (itemIsNumber && (item.Equals(0) || item.Equals(0.0)
                        || item.Equals(0M) || item.Equals(0L)))
                    || (itemIsDate && item.Equals(default(DateTime)))
                    || (itemIsGuid && item.Equals(default(Guid)))
                    || (itemIsEnum && item.Equals(0)
                    || (itemIsString && item.Equals(""))
                    );
            }
        }

        public static Boolean IsNumeric(this Object item) {

            var repr = item.ToString();
            Int64 rLong;
            Decimal rDecimal;
            if (Int64.TryParse(repr, out rLong)) {
                return true;
            } else if (Decimal.TryParse(repr, out rDecimal)) {
                return true;
            } else {
                return false;
            }

        }


    }
}
