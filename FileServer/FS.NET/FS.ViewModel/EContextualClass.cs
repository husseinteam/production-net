﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace FS.ViewModel {

    public enum EContextualClass {

        [Description("success")]
        Success = 1,
        [Description("info")]
        Info,
        [Description("warning")]
        Warning,
        [Description("danger")]
        Danger

    }

}
