﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FS.Model.Objects;
using FS.Repository.Data;

namespace FS.ViewModel.Membership {

    public class SetRolesViewModel {

        private RoleAuthorityRepository _RoleAuthorityRepository;

        public SetRolesViewModel() {

            this._RoleAuthorityRepository = new RoleAuthorityRepository();
            this.AllRoles = _RoleAuthorityRepository.GetAllRoles().MultipleResponse;
            this.AllAuthorities = _RoleAuthorityRepository.GetAllAuthorities().MultipleResponse;

            SetAuthoritiesOfRole();

        }

        public void SetAuthoritiesOfRole() {
            if (this.SelectedRoleID == 0) {
                var first = this.AllRoles.FirstOrDefault();
                if (first != null) {
                    this.SelectedRoleID = first.ID;
                } else {
                    this.SelectedRoleID = 0;
                }
            }
            this.AuthoritiesOfRole = _RoleAuthorityRepository.GetAuthoritiesOfRole(this.SelectedRoleID).MultipleResponse;
        }

        public IEnumerable<Role> AllRoles { get; set; }
        public IEnumerable<Authority> AllAuthorities { get; set; }

        public IEnumerable<Authority> AuthoritiesOfRole { get; set; }
        public IEnumerable<Authority> SelectedAuthoritiesOfRole { get; set; }

        public Int32 SelectedRoleID { get; set; }
        public Int32 SelectedAuthorityID { get; set; }

    }

}
