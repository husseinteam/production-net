﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FS.ViewModel.Membership {
    public class LoginViewModel {

        public String UserName { get; set; }
        public String Password { get; set; }

    }
}
