﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using FS.DataContext.ConnectionProvider;
using FS.DataContext.Mappings;
using FS.Model.Objects;

namespace FS.DataContext.Context {
    public class FSAppContext : DbContext, IDisposable {

        public FSAppContext()
            : base(SConnectionProvider.CurrentConnection, true){

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder) {

            base.OnModelCreating(modelBuilder);
            modelBuilder.Configurations.Add(new AuthorityMapping());
            modelBuilder.Configurations.Add(new RoleMapping());
            modelBuilder.Configurations.Add(new UserMapping());
            modelBuilder.Configurations.Add(new FileItemMapping());

        }

        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Authority> Authorities { get; set; }
        public DbSet<FileItem> FileItems { get; set; }
        public DbSet<RoleAuthority> RoleAuthorities { get; set; }
        
    }
}
