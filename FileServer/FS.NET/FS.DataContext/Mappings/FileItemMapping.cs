﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Linq;
using System.Text;
using FS.Model.Objects;

namespace FS.DataContext.Mappings {
    internal class FileItemMapping : GenericMapping<FileItem> {

        public FileItemMapping() {

            #region Properties

            this.Property(t => t.ServerPath)
                .HasColumnName("ServerPath")
                .HasMaxLength(512)
                .HasColumnType("varchar")
                .IsRequired().HasColumnAnnotation(
                    IndexAnnotation.AnnotationName,
                    new IndexAnnotation(
                        new IndexAttribute("IX_ServerPath", 1) { IsUnique = true }));

            this.Property(t => t.FileName)
                .HasColumnName("FileName")
                .HasMaxLength(128)
                .IsRequired();

            this.Property(t => t.Extension)
                .HasColumnName("Extension")
                .HasMaxLength(32)
                .IsRequired();

            this.Property(t => t.ContentType)
                .HasColumnName("ContentType")
                .HasMaxLength(64)
                .IsRequired();

            this.Property(t => t.ContentLength)
                .HasColumnName("ContentLength")
                .IsRequired();

            #endregion

            this.ToTable("FileItems");

        }

    }
}
