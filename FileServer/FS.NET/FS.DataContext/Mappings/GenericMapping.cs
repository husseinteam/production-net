﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FS.Model.Objects;

namespace FS.DataContext.Mappings {
    internal class GenericMapping<TEntity> : EntityTypeConfiguration<TEntity>
            where TEntity : BaseModel {

        internal GenericMapping() {

            #region Properties

            this.HasKey(t => t.ID).Property(t => t.ID).HasColumnName("ID")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity).IsRequired();

            #endregion

        }

    }
}
