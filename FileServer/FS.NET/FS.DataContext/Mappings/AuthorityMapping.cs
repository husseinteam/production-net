﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Linq;
using System.Text;
using FS.Model.Objects;

namespace FS.DataContext.Mappings {
    internal class AuthorityMapping : GenericMapping<Authority> {

        public AuthorityMapping() {

            #region Properties

            this.Property(t => t.Code)
                .HasColumnName("Code")
                .HasMaxLength(128)
                .IsRequired().HasColumnAnnotation(
                    IndexAnnotation.AnnotationName,
                    new IndexAnnotation(
                        new IndexAttribute("IX_Code", 1) { IsUnique = true }));

            this.Property(t => t.Description)
                .HasColumnName("Description")
                .HasMaxLength(128)
                .IsRequired();

            #endregion

            #region Navigation Properties

            this.HasMany(t => t.RoleAuthorities).WithRequired(t => t.Authority)
                .Map(x => x.MapKey("AuthorityID"));

            #endregion


            this.ToTable("Authorities");

        }

    }
}
