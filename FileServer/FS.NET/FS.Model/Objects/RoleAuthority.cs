﻿
using System;
using System.Collections.Generic;

namespace FS.Model.Objects {
    public class RoleAuthority : BaseModel {

        public virtual Authority Authority { get; set; }
        public virtual Role Role { get; set; }

    }

}