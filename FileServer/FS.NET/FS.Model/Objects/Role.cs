﻿
using System;
using System.Collections.Generic;

namespace FS.Model.Objects {
    public class Role : BaseModel {

        public String Code { get; set; }
        public String Description { get; set; }
        public String AllowedExtensionsList { get; set; }
        public Int32 AllowedMaximumFileLength { get; set; }

        public virtual ICollection<RoleAuthority> RoleAuthorities{ get; set; }
        public ICollection<User> Users { get; set; }
    }

}