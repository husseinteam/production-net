﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FS.Model.Objects {
    public class FileItem : BaseModel {

        public Int64 ContentLength { get; set; }
        public String ContentType { get; set; }
        public String ServerPath { get; set; }
        public String FileName { get; set; }
        public String Extension { get; set; }

        public virtual User Owner { get; set; }

    }
}
