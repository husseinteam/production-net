﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FS.Repository.Variables {
    internal static class SDefaults {

        internal const Int32 AllowedMaximumFileLength = 1024 * 1024 * 8;
        internal const String AllowedExtensionsList = "doc,docx,png,jpg,jpeg";

    }
}
