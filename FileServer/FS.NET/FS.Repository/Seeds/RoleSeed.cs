﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FS.Extensions;
using FS.Model.Objects;
using FS.Repository.Variables;

namespace FS.Repository.Seeds {
    public static class RoleSeed {

        public static ISeedResponse<Role> GetResponse() {

            var codes = new String[] { "admin", "viewer", "uploader", "tester", "superuser" };
            var descriptions = new String[] { "Yönetici", "Görüntüleyici", "Yükleyici", "Test Operatörü", "Üst Yönetici" };
            var seedResponse = new SeedResponse<Role>(codes.Count()) {
                SingleSeed = (k) => new Role() {
                    AllowedExtensionsList = SDefaults.AllowedExtensionsList,
                    AllowedMaximumFileLength = SDefaults.AllowedMaximumFileLength,
                    Code = codes.ElementAt(k - 1),
                    Description = descriptions.ElementAt(k - 1)
                }
            };
            return seedResponse;
        }

    }
}
