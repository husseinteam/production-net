﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FS.Extensions;
using FS.Model.Objects;
using FS.Repository.Variables;

namespace FS.Repository.Seeds {
    public static class AuthoritySeed {

        public static ISeedResponse<Authority> GetResponse() {

            var codes = new String[] { "add", "delete", "list", "view", "displaynedit", "download" };
            var descriptions = new String[] { "Dosya Ekleme", "Dosya Silme" , "Dosya Listeleme", "Dosya Görüntüleme", "Çevrimiçi Dosya Düzenleme", "Dosya İndirme" };
            var seedResponse = new SeedResponse<Authority>(codes.Count()) {
                SingleSeed = (k) => new Authority() {
                    Code = codes.ElementAt(k - 1),
                    Description = descriptions.ElementAt(k - 1)
                }
            };
            return seedResponse;
        }

    }
}
