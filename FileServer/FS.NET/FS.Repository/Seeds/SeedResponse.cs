﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using FS.DataContext.Context;
using FS.Model.Objects;
using FS.Repository.Variables;

namespace FS.Repository.Seeds {
    internal class SeedResponse<TEntity> : ISeedResponse<TEntity>
        where TEntity : BaseModel {

        public SeedResponse(Int32 magnitude) {
            _Magnitude = magnitude;
        }
        private Int32 _Magnitude;

        public Func<Int32, TEntity> SingleSeed { get; internal set; }
        public IEnumerable<TEntity> CollectSeed() {

            var list = new List<TEntity>();
            for (int k = 1; k <= _Magnitude; k++) {
                list.Add(SingleSeed(k));
            }
            return list.ToArray();

        }

        public IEnumerable<TEntity> PersistSeed(DbContext db) {

            var seed = CollectSeed();
            foreach (var sd in seed) {
                db.Set<TEntity>().Add(sd);
            }
            try {
                db.SaveChanges();
                return db.Set<TEntity>().ToList();
            } catch (Exception) {
                return null;
            }

        }

        public Boolean DestroySeed(DbContext db) {


            foreach (var element in db.Set<TEntity>()) {
                db.Set<TEntity>().Remove(element);
            }
            try {
                db.SaveChanges();
                return true;
            } catch (Exception) {
                return false;
            }

        }

    }

    public interface ISeedResponse<TEntity>
        where TEntity : BaseModel {

        Func<Int32, TEntity> SingleSeed { get; }
        IEnumerable<TEntity> CollectSeed();

        IEnumerable<TEntity> PersistSeed(DbContext db);
        Boolean DestroySeed(DbContext db);

    }

}
