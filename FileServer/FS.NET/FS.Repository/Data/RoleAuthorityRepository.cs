﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FS.DataContext.Context;
using FS.Extensions;
using FS.Model.Objects;
using FS.Repository.Variables;

namespace FS.Repository.Data {

    public class RoleAuthorityRepository {

        public IActionResponse<Role> AddRole(String roleDescription) {

            var resp = new ActionResponse<Role>();
            using (var context = new FSAppContext()) {
                var role = new Role() {
                    Description = roleDescription,
                    AllowedMaximumFileLength = SDefaults.AllowedMaximumFileLength,
                    AllowedExtensionsList = SDefaults.AllowedExtensionsList
                };
                context.Roles.Add(role);
                try {
                    context.SaveChanges();
                    resp.SingleResponse = role;
                    resp.Message = "Rol <{0}> Eklendi!".Formatise(role.Description);
                } catch {
                    resp.Message = "<{0}> Rolü Eklenemedi!".Formatise(role.Description);
                    resp.HasFault = true;
                }
            }
            return resp;

        }

        public IActionResponse<Authority> GetAllAuthorities() {

            var resp = new ActionResponse<Authority>();
            using (var context = new FSAppContext()) {
                try {
                    resp.MultipleResponse = context.Authorities.ToArray();
                } catch {
                    resp.HasFault = true;
                }
            }
            return resp;

        }

        public IActionResponse<Role> GetAllRoles() {

            var resp = new ActionResponse<Role>();
            using (var context = new FSAppContext()) {
                try {
                    resp.MultipleResponse = context.Roles.ToArray();
                } catch {
                    resp.HasFault = true;
                }
            }
            return resp;

        }

        public IActionResponse<Authority> GetAuthoritiesOfRole(Int32 roleID) {

            var resp = new ActionResponse<Authority>();
            using (var context = new FSAppContext()) {
                try {
                    resp.MultipleResponse = context.RoleAuthorities.Where(ra => ra.Role.ID == roleID).Select(ra => ra.Authority).ToList();
                } catch {
                    resp.HasFault = true;
                }
            }
            return resp;

        }


        public IActionResponse<Boolean> HasTheAuthority(Int32 roleID, String authCode) {

            var resp = new ActionResponse<Boolean>();
            using (var context = new FSAppContext()) {
                try {
                    resp.SingleResponse = context.RoleAuthorities.SingleOrDefault(ra => ra.Role.ID == roleID && ra.Authority.Code == authCode) != null;
                } catch {
                    resp.HasFault = true;
                }
            }
            return resp;

        }

        public IActionResponse<RoleAuthority> AddRoleAuthority(Int32 userID, Int32 selectedRoleID, Int32 selectedAuthorityID) {

            var resp = new ActionResponse<RoleAuthority>();
            using (var context = new FSAppContext()) {
                try {
                    var existing = context.RoleAuthorities.SingleOrDefault(ra => ra.Role.ID == selectedRoleID && ra.Authority.ID == selectedAuthorityID);
                    if (existing == null) {
                        var role = context.Roles.Find(selectedRoleID);
                        var auth = context.Authorities.Find(selectedAuthorityID);
                        context.Roles.Attach(role);
                        context.Authorities.Attach(auth);
                        var rauth = new RoleAuthority() { Authority = auth, Role = role };
                        role.RoleAuthorities.Add(rauth);
                        context.SaveChanges();

                        resp.Message = "{0} Yetkisi, {1} Rolüne Eklendi!"
                            .Formatise(auth.Description, role.Description);
                        resp.SingleResponse = rauth;
                    } else {
                        resp.HasFault = true;
                        resp.Message = "Böyle Bir Yetkilendirme Zaten Mevcut!";
                    }
                } catch {
                    resp.HasFault = true;
                    resp.Message = "Role Yetki Ekleme Sırasında Hata Oluştu!";
                }
            }
            return resp;

        }
        public IActionResponse<RoleAuthority> DeleteRoleAuthority(Int32 userID, Int32 selectedRoleID, Int32 selectedAuthorityID) {

            var resp = new ActionResponse<RoleAuthority>();
            using (var context = new FSAppContext()) {
                try {
                    var existing = context.RoleAuthorities.SingleOrDefault(ra => ra.Role.ID == selectedRoleID && ra.Authority.ID == selectedAuthorityID);
                    if (existing != null) {
                        context.RoleAuthorities.Remove(existing);
                        context.SaveChanges();
                        var role = context.Roles.Find(selectedRoleID);
                        var auth = context.Authorities.Find(selectedAuthorityID);
                        resp.Message = "{0} Yetkisi, {1} Rolünden Çıkarıldı!"
                           .Formatise(auth.Description, role.Description);
                        resp.SingleResponse = existing;
                    } else {
                        resp.HasFault = true;
                        resp.Message = "Böyle Bir Yetkilendirme Yok!";
                    }
                } catch {
                    resp.HasFault = true;
                    resp.Message = "Rolden Yetki Çıkarma Sırasında Hata Oluştu!";
                }
            }
            return resp;

        }

    }

}
