﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FS.Model.Objects;

namespace FS.Repository.Data {
    internal class ActionResponse<TItem> : IActionResponse<TItem> {

        public Boolean HasFault { get; internal set; }
        public ICollection<TItem> MultipleResponse { get; internal set; }
        public TItem SingleResponse { get; internal set; }
        public String Message { get; internal set; }

    }

    public interface IActionResponse<TItem> {

        TItem SingleResponse { get; }
        ICollection<TItem> MultipleResponse { get; }
        Boolean HasFault { get; }
        String Message { get; }

    }

}
