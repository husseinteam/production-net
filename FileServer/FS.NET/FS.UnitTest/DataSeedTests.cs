﻿using System;
using System.Linq;
using FS.DataContext.Context;
using FS.Repository.Seeds;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FS.UnitTest {
    [TestClass]
    public class DataSeedTests {
        [TestMethod]
        public void UserInRoleSeed() {

            using (var db = new FSAppContext()) {

                var roleResp = RoleSeed.GetResponse();
                Assert.IsTrue(roleResp.DestroySeed(db));
                var roles = roleResp.PersistSeed(db);
                Assert.IsNotNull(roles);

                var authResp = AuthoritySeed.GetResponse();
                Assert.IsTrue(authResp.DestroySeed(db));
                var auths = authResp.PersistSeed(db);
                Assert.IsNotNull(auths);

                roles.ToList().ForEach(r => db.Roles.Attach(r));
                auths.ToList().ForEach(a => db.Authorities.Attach(a));

                var roleAuthResp = RoleAuthoritySeed.GetResponse(roles, auths);
                Assert.IsTrue(roleAuthResp.DestroySeed(db));
                var roleAuths = roleAuthResp.PersistSeed(db);
                Assert.IsNotNull(roleAuths);

                var userResp = UserSeed.GetResponse(roles);
                Assert.IsTrue(userResp.DestroySeed(db));
                Assert.IsNotNull(userResp.PersistSeed(db));

            }

        }
    }
}
