﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinery.Extensions.Core {
	public static class DateTimeExtensions {

		public static String GetDay(this DayOfWeek dow) {
			var repr = String.Empty;
			switch (dow) {
				case DayOfWeek.Sunday:
					repr = "Sunday";
					break;
				case DayOfWeek.Monday:
					repr = "Monday";
					break;
				case DayOfWeek.Tuesday:
					repr = "Tuesday";
					break;
				case DayOfWeek.Wednesday:
					repr = "Wednesday";
					break;
				case DayOfWeek.Thursday:
					repr = "Thursday";
					break;
				case DayOfWeek.Friday:
					repr = "Friday";
					break;
				case DayOfWeek.Saturday:
					repr = "Saturday";
					break;
				default:
					break;
			}
			return repr;
		}

		public static String SimplifyDaysPassed(this DateTime date) {

			return DateTime.Now.Subtract(date).Days > 1 ? date.DayOfWeek.GetDay() : (DateTime.Now.Subtract(date).Days == 0 ? "Today" : "Yesterday");

		}

		public static String ActualAgo(this DateTime date) {

			var ts = DateTime.Now.Subtract(date);
			var time = new StringBuilder();

			if (DateTime.Now.Year - date.Year > 0) {
				time.AppendFormat("{0} years ", DateTime.Now.Year - date.Year);
			}
			if (ts.Days > 0) {
				time.AppendFormat("{0} days ", ts.Days);
			}
			if (ts.Hours > 0) {
				time.AppendFormat("{0} hours ", ts.Hours);
			}
			if (ts.Minutes > 0) {
				time.AppendFormat("{0} minutes ", ts.Minutes);
			}
			if (ts.Seconds > 0) {
				time.AppendFormat("{0} seconds ", ts.Seconds);
			}
			if (time.ToString().IsUnassigned()) {
				return "just now";
			}
			time.Append("ago");
			return time.ToString();

		}

		public static DateTime DaysAgo(this Int32 duration) {

			return DateTime.Now.Subtract(TimeSpan.FromDays(duration));

		}

		public static DateTime MonthsAgo(this Int32 duration) {

			return DateTime.Now.Subtract(TimeSpan.FromDays(duration * 30));

		}

		public static DateTime HoursAgo(this Int32 duration) {

			return DateTime.Now.Subtract(TimeSpan.FromHours(duration));

		}

		public static DateTime MinutesAgo(this Int32 duration) {

			return DateTime.Now.Subtract(TimeSpan.FromMinutes(duration));

		}

		public static DateTime SecondsAgo(this Int32 duration) {

			return DateTime.Now.Subtract(TimeSpan.FromSeconds(duration));

		}

		public static DateTime DaysLater(this Int32 duration) {

			return DateTime.Now.Add(TimeSpan.FromDays(duration));

		}
		public static DateTime MonthsLater(this Int32 duration) {

			return DateTime.Now.Add(TimeSpan.FromDays(duration * 30));

		}
		public static DateTime HoursLater(this Int32 duration) {

			return DateTime.Now.Add(TimeSpan.FromHours(duration));

		}
		public static DateTime WeeksLater(this Int32 duration) {

			return DateTime.Now.Add(TimeSpan.FromDays(duration / 7));

		}
		public static DateTime MinutesLater(this Int32 duration) {

			return DateTime.Now.Add(TimeSpan.FromMinutes(duration));

		}

	}
}
