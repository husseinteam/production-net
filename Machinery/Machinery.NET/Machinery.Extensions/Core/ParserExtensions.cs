﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Machinery.Extensions.Core {
	public static class ParserExtensions {

		public static Int32 ToInt32(this Object instance) {

			try {
				return (Int32)Convert.ToUInt32(instance);
			} catch {
				return (Int32)Convert.ToUInt32(instance.ToString());
			}
		}

		public static Int32 ToInt32(this Int32 numberLong) {
			return numberLong < Int32.MaxValue ? (Int32)numberLong : -1;
		}

		public static Int64 ToInt64(this Int64 numberLong) {
			return numberLong < Int64.MaxValue ? (Int64)numberLong : -1;
		}

		public static TItem ToType<TItem>(this Object instance) {
			if (instance == null) {
				return default(TItem);
			} else if (instance.IsUnassigned()) {
				return default(TItem);
			} else {
				if (instance.GetType().GetInterface("IConvertible") == null || typeof(TItem).IsAssignableFrom(typeof(ValueType))) {
					return instance != null ? (TItem)instance : default(TItem);
				} else {
					if (instance.GetType().IsEnum) {
						return (TItem)Enum.Parse(instance.GetType(), instance.ToString());
					} else {
						var conv = TypeDescriptor.GetConverter(typeof(TItem));
						if (conv.CanConvertFrom(instance.GetType())) {
							return (TItem)conv.ConvertFrom(instance);
						} else {
							return instance.ToString().ToType<TItem>();
						}
					}
				}
			}
		}

		public static Object ToType(this Object instance, Type convertedType) {
			if (instance == null) {
				return null;
			} else if (instance.IsUnassigned()) {
				return null;
			} else {
				if (instance.GetType().GetInterface("IConvertible") == null || convertedType.IsAssignableFrom(typeof(ValueType))) {
					return instance != null ? instance : null;
				} else {
					if (instance.GetType().IsEnum) {
						return Enum.Parse(instance.GetType(), instance.ToString());
					} else {
						var conv = TypeDescriptor.GetConverter(convertedType);
						if (conv.CanConvertFrom(instance.GetType())) {
							return conv.ConvertFrom(instance);
						} else {
							return instance.ToString().ToType(convertedType);
						}
					}
				}
			}
		}

		public static void MapTo<TSelf, TOther>(this TSelf self, ref TOther other)
			where TSelf : class
			where TOther : class {

			if (self != null) {
				var propsSelf = typeof(TSelf).GetProperties(BindingFlags.Instance | BindingFlags.Public)
					   .Where(pi => pi.CanRead && pi.CanWrite).ToList();

				var propsOther = typeof(TOther).GetProperties(BindingFlags.Instance | BindingFlags.Public)
					.Where(pi => pi.CanRead && pi.CanWrite).ToList();

				foreach (var poth in propsOther) {
					var selfProp = propsSelf.SingleOrDefault(pslf => pslf.Name == poth.Name
						&& pslf.PropertyType.FullName == poth.PropertyType.FullName);
					if (selfProp != null) {
						var val = selfProp.GetValue(self, null);
						if (!val.IsUnassigned()) {
							poth.SetValue(other, val);
						}
					}
				}
			}

		}
		public static TOther MapToDirect<TSelf, TOther>(this TSelf self, Func<TOther> constructor = null)
			where TSelf : class
			where TOther : class, new() {

			constructor = constructor ?? new Func<TOther>(() => new TOther());

			var other = constructor();
			var flags = BindingFlags.Instance | BindingFlags.FlattenHierarchy | BindingFlags.Public | BindingFlags.NonPublic;

			var propsSelf = typeof(TSelf).GetProperties(flags)
				.Where(pi => pi.CanRead && pi.CanWrite).ToList();

			var propsOther = typeof(TOther).GetProperties(flags)
				.Where(pi => pi.CanRead && pi.CanWrite).ToList();

			foreach (var poth in propsOther) {
				var selfProp = propsSelf.SingleOrDefault(pslf => pslf.Name == poth.Name
					&& pslf.PropertyType.FullName == poth.PropertyType.FullName);
				if (selfProp != null) {
					var val = selfProp.GetValue(self, null);
					if (!val.IsUnassigned()) {
						poth.SetValue(other, val);
					}
				}
			}

			return other;

		}

		public static Boolean IsNumeric(this Object item) {

			var repr = item.ToString();
			Int64 rLong;
			Decimal rDecimal;
			if (Int64.TryParse(repr, out rLong)) {
				return true;
			} else if (Decimal.TryParse(repr, out rDecimal)) {
				return true;
			} else {
				return false;
			}

		}
		public static TItem ShallowCopy<TItem>(this TItem item) {

			if (Object.ReferenceEquals(item, null)) {
				return default(TItem);
			}

			return JsonConvert.DeserializeObject<TItem>(JsonConvert.SerializeObject(item));

		}

		public static Boolean IsUnassigned(this Object item) {
			DateTime reponse1;
			Guid response2;

			var itemNull = item == null;
			if (itemNull) {
				return true;
			} else {
				var itemIsNumber = item.IsNumeric();
				var itemIsDate = DateTime.TryParse(item.ToString(), out reponse1);
				var itemIsGuid = Guid.TryParse(item.ToString(), out response2);
				var itemIsEnum = item.GetType().IsEnum;
				var itemIsString = item.GetType().Equals(typeof(String));

				return (itemIsNumber && (item.Equals(0) || item.Equals(0.0)
						|| item.Equals(0M) || item.Equals(0L)))
					|| (itemIsDate && item.Equals(default(DateTime)))
					|| (itemIsGuid && item.Equals(default(Guid)))
					|| (itemIsEnum && item.Equals(0)
					|| (itemIsString && item.Equals(""))
					);
			}
		}

		public static TDerived As<TDerived>(this Object item)
				where TDerived : class {

			return item as TDerived;

		}

		public static TProperty DefaultIfNull<TItem, TProperty>(this TItem self, Func<TItem, TProperty> selector, TProperty prop) {

			if (Object.ReferenceEquals(self, null)) {
				return prop;
			} else {
				return selector(self);
			}

		}

	}
}
