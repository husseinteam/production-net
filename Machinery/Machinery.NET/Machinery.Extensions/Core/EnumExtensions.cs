﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Machinery.Extensions.Core {
    public static class EnumExtensions {

        public static String GetDescription<TEnum>(this TEnum value) {
            var q = from TEnum en in Enum.GetValues(typeof(TEnum)).AsQueryable()
                    where en.ToCode() == value.ToCode()
                    select typeof(TEnum).GetFields().Where<FieldInfo>((fi) =>
                        fi.GetCustomAttributes(true).Where(
                    (ca) => (ca is DescriptionAttribute)
                    && fi.GetValue(en).ToType<Int32>() == value.ToType<Int32>())
                    .FirstOrDefault() != null)
                        .Select<FieldInfo, String>((fi) =>
                            fi.GetCustomAttributes(true).OfType<DescriptionAttribute>().First().Description).FirstOrDefault();

            var r = q.FirstOrDefault();
            if (r == null) {
                q = from TEnum en in Enum.GetValues(typeof(TEnum)).AsQueryable()
                    where en.ToCode() == value.ToCode()
                    select typeof(TEnum).GetFields().Where<FieldInfo>((fi) =>
                        fi.GetCustomAttributes(true).Where(
                    (ca) => (ca is DisplayAttribute)
                    && fi.GetValue(en).ToType<Int32>() == value.ToType<Int32>())
                    .FirstOrDefault() != null)
                        .Select<FieldInfo, String>((fi) =>
                            fi.GetCustomAttributes(true).OfType<DisplayAttribute>().First().Name).FirstOrDefault();
            }
            return q.FirstOrDefault();
        }
        public static Int32 ToCode(this Object instance) {

            if (instance.GetType().IsEnum) {
                return Convert.ToInt32(instance);
            } else {
                throw new ArgumentException("Enum Required");
            }

        }

        public static TEnum ToEnum<TEnum>(this String key, Func<TEnum, String> selector = null) {

            if (selector == null) {
                selector = k => k.GetDescription();
            }
            if (typeof(TEnum).IsEnum) {
                return Enum.GetValues(typeof(TEnum)).OfType<TEnum>()
                    .SingleOrDefault(k => selector(k) == key);
            } else {
                throw new ArgumentException("Not Enum");
            }

        }

        public static List<String> EnlistEnumDescriptions<TEnum>(this List<String> list) {

            if (typeof(TEnum).IsEnum) {
                Enum.GetValues(typeof(TEnum)).OfType<TEnum>()
                    .Select(k => k.GetDescription()).Enumerate(desc => list.Add(desc));
                return list;
            } else {
                throw new ArgumentException("Not Enum");
            }

        }

        public static List<String> PassOverEnum<TEnum>(this List<String> list, Func<TEnum, String> by) {

            if (typeof(TEnum).IsEnum) {
                Enum.GetValues(typeof(TEnum)).OfType<TEnum>()
                    .Select(k => by(k)).Enumerate(desc => list.Add(desc));
                return list;
            } else {
                throw new ArgumentException("Not Enum");
            }

        }

        public static Boolean EnumDescriptionIn<TEnum>(this String keydesc) {

            if (typeof(TEnum).IsEnum) {
                return Enum.GetValues(typeof(TEnum)).OfType<TEnum>()
                    .Where(k => k.GetDescription() == keydesc).Count() != 0;
            } else {
                throw new ArgumentException("Not Enum");
            }
        }


        public static Boolean EnumValueIn<TEnum>(this String keydesc, Func<TEnum, String> selector) {

            if (typeof(TEnum).IsEnum) {
                return Enum.GetValues(typeof(TEnum)).OfType<TEnum>()
                    .Where(k => selector(k) == keydesc).Count() != 0;
            } else {
                throw new ArgumentException("Not Enum");
            }
        }

    }
}
