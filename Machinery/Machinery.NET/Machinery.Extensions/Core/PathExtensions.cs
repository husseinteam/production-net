﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;

namespace Machinery.Extensions.Core {
    public static class PathExtensions {

        public async static Task<IEnumerable<TResult>> PickBranches<TResult>(this String directory, Func<String, TResult> selector = null)
                 where TResult : class {

            selector = selector ?? new Func<String, TResult>((str) => str as TResult);
            return await Task.Run<IEnumerable<TResult>>(()
                => {
                    var permission = new FileIOPermission(
                        FileIOPermissionAccess.AllAccess, directory);
                    directory.AssertIfDirectory();
                    try {
                        permission.Assert();
                        return Directory.EnumerateDirectories(directory)
                            .Select<String, TResult>(selector);
                    } catch (Exception ex) {
                        return new String[] { ex.Message }.Select<String, TResult>(selector);
                    }
                });

        }

        public async static Task<IEnumerable<TResult>> PickLeaves<TResult>(this String directory, Func<String, TResult> selector = null)
                where TResult : class {

            selector = selector ?? new Func<String, TResult>((str) => str as TResult);
            return await Task.Run<IEnumerable<TResult>>(()
                => {
                    var permission = new FileIOPermission(
                        FileIOPermissionAccess.AllAccess, directory);
                    directory.AssertIfDirectory();
                    try {
                        permission.Assert();
                        return Directory.EnumerateFiles(directory)
                            .Select<String, TResult>(selector);
                    } catch (Exception ex) {
                        return new String[] { ex.Message }.Select<String, TResult>(selector);
                    }
                });

        }
        public static Boolean CheckIfDirectory(this String item) {
            return Directory.Exists(item);
        }

        public static Boolean CheckIfFile(this String item) {
            return File.Exists(item);
        }

        public static void AssertIfDirectory(this String item) {
            item.CheckIfDirectory().Assert<NotSupportedException>(
                "Should be a Directory: <{0}>".PostFormat(item));
        }

        public static void AssertIfFile(this String item) {
            item.CheckIfFile().Assert<NotSupportedException>(
                "Should be a File: <{0}>".PostFormat(item));
        }

        public static String ToExtension(this String item) {

            return item.Substring(item.LastIndexOf('.'), item.Length);

        }

        public static String ToSimpleName(this String item) {

            return item.Substring(item.LastIndexOf('\\') + 1, item.Length);

        }


    }
}
