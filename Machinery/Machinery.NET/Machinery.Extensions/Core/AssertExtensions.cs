﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinery.Extensions.Core {
    public static class AssertExtensions {

        public static void Commit(this Boolean trueOrFalse, Action<Boolean> commit) {

            commit(trueOrFalse);

        }

        public static void Assert<TException>(this Boolean isTrue, String message)
            where TException : Exception, new() {

            if (!isTrue) {
                var ex = Activator.CreateInstance(typeof(TException), message)
                    as TException;
                throw ex;
            }

        }

        public static TItem AssertNotNull<TItem>(this TItem instance) {

            var assertion = false;
            if (typeof(TItem).IsValueType) {
                assertion = !instance.Equals(0);
            } else {
                assertion = !instance.Equals(default(TItem));
            }
            assertion.Assert<NullReferenceException>("{0} type is null or default".PostFormat(typeof(TItem).Name));
            return instance;
        }

        public static TItem Should<TItem, TException>(this TItem item, Func<TItem, Boolean> check, String message)
            where TException : Exception, new() {

            if (!check(item)) {
                var ex = Activator.CreateInstance(typeof(TException), message)
                    as TException;
                throw ex;
            }
            return item;
        }

        public static void ShouldBeAssigned<TItem>(this TItem self) {

            if (self.IsUnassigned()) {
                throw new ArgumentException("Should Be Assigned:{0}:{1}"
                    .PostFormat(self.GetType().Name, self));
            }

        }
        public static void ShouldBeUnAssigned<TItem>(this TItem self) {

            if (!self.IsUnassigned()) {
                throw new ArgumentException("Should Be UnAssigned:{0}:{1}"
                    .PostFormat(self.GetType().Name, self));
            }

        }

    }
}
