﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Machinery.Extensions.Core {
    public static class AssignmentExtensions {

        public static TItem Assign<TItem>(this TItem item, Action<TItem> assigner)
                where TItem : class {

            assigner(item);
            return item;

        }

        public static NameValueCollection Assign<TValue>(this NameValueCollection collection, String key, TValue value) {

            collection[key] = value.ToString();
            return collection;

        }

        public static TItem IfExists<TItem>(this TItem self, Func<TItem, TItem> commit) {

            return self != null ? commit(self) : default(TItem);

        }

        public static TProperty IfPropertyExists<TItem, TProperty>(this TItem self, Func<TItem, TProperty> selector) {

            return self != null ? selector(self) : default(TProperty);

        }

        public static IEnumerable<Object> HasLengthOf(this IEnumerable<Object> array, Int32 length) {

            var isTrue = array.Count() == length;
            isTrue.Assert<IndexOutOfRangeException>("Should Have Length of<{0}> but having: <{1}>"
                .PostFormat(length, array.Count()));
            return array;

        }
        public static IEnumerable<Object> HasntLengthOf(this IEnumerable<Object> array, Int32 length) {

            var isTrue = array.Count() != length;
            isTrue.Assert<IndexOutOfRangeException>("Should Have The Same Length of<{0}> but having: <{1}>"
                .PostFormat(length, array.Count()));
            return array;

        }

        public static void Digest<TItem>(this TItem self, TItem other) {

            var props = self.GetType()
               .GetProperties(BindingFlags.Instance | BindingFlags.Public)
               .Where(pi => pi.CanRead && pi.CanWrite).ToList();

            foreach (var prop in props) {
                try {
                    var v = prop.GetValue(other, null);
                    if (!v.IsUnassigned()) {
                        prop.SetValue(self, v);
                    }
                } catch {

                }
            }

        }

    }

}
