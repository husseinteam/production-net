﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Machinery.Extensions.Core.Dictionary;

namespace Machinery.Extensions.Core {
    public static class LexicalExtensions {

        #region Public Static Methods

        public static String Pluralise(this String name) {
            return name.EndsWith("y") ? name.Substring(0, name.Length - 1) + "ies" : name.EndsWith("s") || name.EndsWith("z") ? name + "es" : name + "s";
        }

        public static String CapitalLetterToLower(this String name) {
            return name[0].ToString().ToLowerInvariant() + name.Substring(1);
        }

        public static String GenerateIdentifier(this String suffix, Int32 count = 4) {

            SetFaust();

            var identifier = count.Times(() => Faust.SelectRandom()).Gather();
            return suffix.PostFormat(identifier.ToString());

        }

        public static String GenerateNumber(this String format) {

            return format.PostFormat(_Random.Next(1024 * 1024).ToString());

        }
        public static String GenerateToken(this String format, Int32 length = 7) {

            var alpha = (90 - 64).Times(k => (char)(k + 64))
                .Extend((122 - 96).Times(k => (char)(k + 96)).ToArray())
                .Extend((57 - 47).Times(k => (char)(k + 47)).ToArray())
                .ToArray();

            var token = length.Times(k => alpha.SelectRandom()).Gather();
            return format.PostFormat(token);

        }

        #endregion

        #region Fields

        private static void SetFaust() {
            if (Faust == null) {
                var alpha = (90 - 64).Times(k => (char)(k + 64)).Extend((122 - 96).Times(k => (char)(k + 96)).ToArray()).ToArray();
                var list = new List<String>();
                var text = Lexer.ResourceManager.GetObject("faust", Thread.CurrentThread.CurrentCulture).ToString();
                var words = text.Split(' ');
                foreach (var word in words) {
                    StringBuilder stripped = new StringBuilder("");
                    word.Enumerate(ch => {
                        if (ch.In(alpha)) {
                            stripped.Append(ch);
                        }
                    });
                    var selected = stripped.ToString();
                    if (!String.IsNullOrWhiteSpace(selected)) {
                        list.Add(selected.ToUpperInvariant()[0] + selected.ToLowerInvariant().Substring(1)); 
                    }
                }
                Faust = list.ToArray();
            }
        }
        private static Random _Random = new Random();
        private static IEnumerable<String> Faust { get; set; }

        #endregion

    }

}
