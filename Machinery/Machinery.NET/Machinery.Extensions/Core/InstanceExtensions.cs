﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Machinery.Extensions.Core {
    public static class InstanceExtensions {

        public static Boolean InstanceEqual<TItem>(this TItem item, TItem to) {

            var props = typeof(TItem).GetProperties(BindingFlags.Instance
                | BindingFlags.Public)
                .Where(pi => pi.CanRead && pi.CanWrite).ToArray();

            foreach (var prop in props) {
                try {
                    if (!prop.GetValue(item).Equals(prop.GetValue(to))) {
                        return false;
                    }
                } catch {
                    continue;
                }
            }
            return true;

        }

        public static Boolean TypesEqual(this Type selfType, Type otherType) {

            return selfType.Equals(otherType);

        }

    }
}
