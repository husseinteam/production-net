﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Machinery.Extensions.Core {
	public static class EnumerableExtensions {

		private static Random r = new Random();

		public static void Times(this Int32 number, Action<Int32> commit) {

			for (int i = 0; i < number; i++) {
				commit(i);
			}

		}

		public static TItem Cycle<TItem>(this IEnumerable<TItem> self, Int32 i) {

			return self.ElementAt(i % self.Count());

		}

		public static ICollection<TItem> Times<TItem>(this Int32 number, Func<TItem> commit) {

			var list = new List<TItem>();
			for (int i = 0; i < number; i++) {
				list.Add(commit());
			}
			return list;
		}

		public static ICollection<TItem> Times<TItem>(this Int32 number, Func<Int32, TItem> commit) {

			var list = new List<TItem>();
			for (int i = 1; i <= number; i++) {
				list.Add(commit(i));
			}
			return list;
		}


		public static IList<TItem> Extend<TItem>(this ICollection<TItem> list, params TItem[] items) {

			items.Enumerate(it => (list as List<TItem>).Add(it));
			return list as IList<TItem>;

		}

		public static IEnumerable<TItem> Unique<TItem, TProperty>(this IEnumerable<TItem> self, Func<TItem, TProperty> selector) {

			var props = from s in self
						orderby selector(s) ascending
						group s by selector(s) into Group
						select Group.First();

			return props.ToList();

		}

		public static IEnumerable<TItem> Unique<TItem>(this IEnumerable<TItem> self) {

			var gr = from s in self
					 orderby s ascending
					 group s by s into Group
					 select Group;
			return gr.Select(g => g.Key).ToArray();
		}

		public static Int32 Magnitude<TItem>(this IEnumerable<TItem> array) {

			var c = 0;
			foreach (var item in array) {
				c++;
			}
			return c;
		}

		public static Boolean In<TItem>(this TItem item, params TItem[] array) {

			return array.Contains(item);

		}

		public static IEnumerable<TItem> Enumerate<TItem>(this IEnumerable<TItem> array
			, Action<TItem> commit) {

			if (array != null) {
				foreach (var item in array) {
					commit(item);
				}
			}
			return array;

		}
		public static IEnumerable<TItem> Enumerate<TItem>(this IEnumerable<TItem> array
			, Action<Int32, TItem> commit) {

			if (array != null) {
				for (int i = 0; i < array.Count(); i++) {
					commit(i, array.ElementAt(i));
				}
			}
			return array;

		}

		public static IEnumerable<TItem> Translate<TItem>(this IEnumerable<TItem> array
			, Func<Int32, TItem, TItem> commit) {

			var li = new List<TItem>();
			if (array != null) {
				for (int i = 0; i < array.Count(); i++) {
					li.Add(commit(i, array.ElementAt(i)));
				}
			}
			return li.ToArray();

		}

		public static Boolean IsEnumerable(this Type type) {
			return type.GetInterfaces()
				  .Any(inf => inf.Name.StartsWith("ICollection")
				  || inf.Name.StartsWith("IList")
				  || inf.Name.StartsWith("IEnumerable")) ||
				  (type.Name.StartsWith("ICollection")
				  || type.Name.StartsWith("IList")
				  || type.Name.StartsWith("IEnumerable"));
		}

		public static Boolean IsCollection(this Type type) {
			return type.GetInterfaces()
				  .Any(inf => inf.Name.StartsWith("ICollection")
				  || inf.Name.StartsWith("IList")) ||
				  (type.Name.StartsWith("ICollection")
				  || type.Name.StartsWith("IList"));
		}

		public static IQueryable<TItem> MakeQuery<TItem>(this IEnumerator<TItem> self) {

			var l = new List<TItem>();
			while (self.MoveNext()) {
				l.Add(self.Current);
			}
			return l.AsQueryable();

		}

		public static IEnumerable<TItem> SelectBy<TItem>(this IEnumerable<TItem> array
			, Func<TItem, Boolean> check) {

			var list = new List<TItem>();
			foreach (var item in array) {
				if (check(item)) {
					list.Add(item);
				}
			}
			return array = list.ToArray();

		}

		public static IEnumerable<TItem> Invert<TItem>(this IEnumerable<TItem> array) {

			var list = new List<TItem>(array);
			for (int start = 0, end = list.Count - 1; start < list.Count / 2; start++, end--) {
				var temp = list[start];
				list[start] = list[end];
				list[end] = temp;
			}
			return list.ToArray();

		}

		public static void ThrowOut<TItem>(this ICollection<TItem> self, TItem item, Func<TItem,
			object> selector) {

			var increment = 0;
			for (int i = 0; i < self.Count; i++) {
				var it = self.ElementAt(i - increment);
				if (selector(item).Equals(selector(it))) {
					self.Remove(it);
					increment++;
				}
			}
		}

	}

}
