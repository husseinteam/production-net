﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinery.CoreData.DbEngine;
using Machinery.CoreData.EntityEngine.Base;
using Machinery.CoreData.SeedEngine;
using Machinery.CoreData.ViewModels;
using Machinery.Extensions.Core;

namespace Machinery.CoreData {
	public static class SInstanceProvider {

		#region Instances

		private static IDbGenerator _DbGeneratorInstance;

		#endregion

		public static IDbGenerator DbGeneratorInstance {
			get {
				return _DbGeneratorInstance = _DbGeneratorInstance ?? new DbGenerator();
			}
		}

		public static ISeedResponse<TEntity> SeedInstance<TEntity>(Int32 magnitude, Func<Int32, TEntity> singleSeed, Action<TEntity> extraOperations = null)
			where TEntity : BaseEntity<TEntity> {

			extraOperations = extraOperations ?? new Action<TEntity>(e => { });
			return new SeedResponse<TEntity>(magnitude, extraOperations) {
				SingleSeed = singleSeed
			}.CollectSeed();

		}

		public static IPartialModel Partial(Boolean isPlainLink = false) {

			return new PartialModel(isPlainLink);

		}

	}

}

