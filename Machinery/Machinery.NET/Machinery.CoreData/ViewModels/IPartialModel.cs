﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinery.CoreData.ViewModels {
	public interface IPartialModel {

		IPartialModel Attribute(String key);
		IPartialModel Has(Object value);

		ICollection<Dictionary<String, Object>> Enumerate();
		Object this[String key, Int32 index] { get; }
		Boolean IsPlainLink { get; }
	}
}
