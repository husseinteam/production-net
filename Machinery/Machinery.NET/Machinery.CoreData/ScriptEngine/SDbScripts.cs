﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinery.Instruments.Constructs.Credentials;
using Machinery.Instruments.Protocols;
using Machinery.Extensions.Core;

namespace Machinery.CoreData.EntityEngine.ScriptEngine {
    public static class SDbScripts {

		#region Commented
		//		internal static IEnumerable<String> RegenerateDatabaseScript(IConnectionCredential credentials) {

		//			var sb = new StringBuilder();
		//			sb.AppendFormat(@"
		//				USE [master]
		//				IF  EXISTS (SELECT * FROM sys.databases WHERE name = N'{0}')
		//					DROP DATABASE [{0}]
		//				GO
		//            ", credentials.Database);
		//			sb.AppendFormat(@"
		//					CREATE DATABASE [{0}]
		//				GO
		//					ALTER DATABASE [{0}] SET COMPATIBILITY_LEVEL = {2}
		//				GO
		//{1}
		//            ", credentials.Database, credentials.ServiceType == EServiceType.AzureSql ? "" : @"
		//				IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
		//                BEGIN
		//					EXEC [{0}].[dbo].[sp_fulltext_database] @action = 'enable'
		//                END
		//				GO
		//				".PostFormat(credentials.Database), credentials.ServerCompatibilityLevel);
		//			return SplitByGo(sb);

		//		}

		//		public static String RegenerateUserScript(IConnectionCredential credentials) {

		//			var sb = new StringBuilder();
		//			if (credentials.CredentialType == ECredentialType.USERAUTH) {
		//				sb.AppendFormat(@"
		//                    USE [{2}]
		//                    IF EXISTS (SELECT * FROM sys.database_principals WHERE name = N'{0}')
		//                    DROP USER {0}
		//                    IF EXISTS (SELECT * FROM sys.server_principals WHERE name = N'{0}')
		//                    DROP LOGIN {0}
		//                    CREATE LOGIN [{0}] WITH PASSWORD=N'{1}', DEFAULT_DATABASE=[master], DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF
		//                    ALTER LOGIN [{0}] ENABLE
		//                    CREATE USER [{0}] FOR LOGIN [{0}] WITH DEFAULT_SCHEMA=[dbo]
		//                    ALTER ROLE [db_owner] ADD MEMBER [{0}]
		//                ", credentials.UserID, credentials.Password, credentials.Database);
		//			}
		//			return sb.ToString();

		//		}

		//		public static IEnumerable<String> RegenerateAzureUserScript(IConnectionCredential credentials) {

		//			var sb = new StringBuilder();
		//			if (credentials.CredentialType == ECredentialType.USERAUTH) {
		//				sb.AppendFormat(@"
		//                    IF EXISTS (SELECT name FROM sys.database_principals WHERE name=N'{0}')
		//						EXEC sys.sp_executesql N'DROP USER [{0}]'
		//					GO
		//					CREATE LOGIN [{1}] WITH password='{2}'
		//					GO
		//					CREATE USER [{0}] FROM LOGIN [{1}]
		//					GO
		//                ", credentials.UserID, credentials.UserID, credentials.Password);
		//			}
		//			return SplitByGo(sb);

		//		} 
		//private static IEnumerable<String> SplitByGo(StringBuilder sql) {

		//	var strl = sql.ToString().Split(new String[] { "GO" }, StringSplitOptions.RemoveEmptyEntries);
		//	Func<String, String> transform = (str) => str.Replace("\r\n", " ").Replace("\t", "");
		//	return strl.Translate((i, str) => {
		//		if (!String.IsNullOrWhiteSpace(str)) {
		//			return transform(str);
		//		} else {
		//			return String.Empty;
		//		}
		//	}).Where(str => !String.IsNullOrWhiteSpace(str));

		//}

		#endregion

		public static String ClearAllSchemasSproc() {

			return @"while(exists(select 1 from INFORMATION_SCHEMA.TABLE_CONSTRAINTS where CONSTRAINT_TYPE=''FOREIGN KEY''))
begin
 declare @sql nvarchar(2000)
 SELECT TOP 1 @sql=(''ALTER TABLE ['' + TABLE_SCHEMA + ''].['' + TABLE_NAME
 + ''] DROP CONSTRAINT ['' + CONSTRAINT_NAME + '']'')
 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS
 WHERE CONSTRAINT_TYPE = ''FOREIGN KEY''
 exec (@sql)
 PRINT @sql
end        
            while(exists(select 1 from INFORMATION_SCHEMA.TABLES where TABLE_NAME != ''__MigrationHistory''))
begin
 declare @sql2 nvarchar(2000)
 SELECT TOP 1 @sql2=(''DROP TABLE ['' + TABLE_SCHEMA + ''].['' + TABLE_NAME
 + '']'')
 FROM INFORMATION_SCHEMA.TABLES
 WHERE TABLE_NAME != ''__MigrationHistory''
exec (@sql2)
 PRINT @sql2
end
--http://www.sqlservercentral.com/blogs/sqlservertips/2011/10/11/remove-all-foreign-keys/";

		}


    }
}
