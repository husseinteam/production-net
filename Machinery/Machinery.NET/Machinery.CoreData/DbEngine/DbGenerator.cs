﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinery.CoreData.EntityEngine.Annotations;
using Machinery.CoreData.EntityEngine.Base;
using Machinery.CoreData.DbEngine;
using Machinery.CoreData.EntityEngine.ScriptEngine;
using Machinery.Extensions.Core;
using Machinery.Instruments.Constructs.Credentials;
using Machinery.Instruments.Protocols;

namespace Machinery.CoreData.DbEngine {
	internal class DbGenerator : IDbGenerator {

		//public IDbGenerator RegenerateDatabase(out Exception expt) {

		//	var cred = SCredentialProvider.ProvideCurrent();
		//	expt = ExecuteByGo(cred.ProvideMasterConnection(), SDbScripts.RegenerateDatabaseScript(cred));
		//	if (expt != null) {
		//		return this;
		//	}
		//	if (cred.ServiceType == EServiceType.AzureSql) {
		//		expt = ExecuteByGo(cred.ProvideMasterConnection(), SDbScripts.RegenerateAzureUserScript(cred));
		//	} else {
		//		using (IDbConnection conn = cred.ProvideMasterConnection()) {
		//			conn.Open();
		//			try {
		//				conn.RunSqlCommand(SDbScripts.RegenerateUserScript(cred));
		//				expt = null;
		//			} catch (Exception ex) {
		//				expt = ex;
		//			} finally {
		//				conn.Close();
		//			}
		//		}
		//	}
		//	return this;

		//}

		public IDbGenerator EngineFor<TEntity>()
			where TEntity : BaseEntity<TEntity>, new() {

			SScriptProvider.AggregatorInstance.GenerateModelFor<TEntity>();
			return this;

		}

		public Exception GenerateModels() {

			var cred = SCredentialProvider.ProvideCurrent();
			Exception expt = null;
			using (IDbConnection conn = cred.ProvideSuperAdminConnection()) {
				try {
					var aggr = SScriptProvider.AggregatorInstance
						.GenerateSproc("spClearAllTables", SDbScripts.ClearAllSchemasSproc());
					conn.Open();
					conn.RunSqlCommand(aggr.Sprocs);
					conn.RunSqlCommand(aggr.Finalize());
					return null;
				} catch (Exception ex) {
					expt = ex;
				} finally {
					conn.Close();
				}
			}
			return expt;

		}

		private static Exception ExecuteByGo(IDbConnection conn, IEnumerable<String> scripts) {
			Exception expt = null;
			foreach (var sql in scripts) {
				try {
					conn.Open();
					conn.RunSqlCommand(sql);
				} catch (Exception ex) {
					expt = ex;
					break;
				} finally {
					conn.Close();
				}
			}
			return expt;
		}
	}
}
