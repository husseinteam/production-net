﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinery.CoreData.EntityEngine.Base;
using Machinery.Instruments.Protocols;

namespace Machinery.CoreData.DbEngine {
    public interface IDbGenerator {

		//IDbGenerator RegenerateDatabase(out Exception expt);
        IDbGenerator EngineFor<TEntity>()
            where TEntity : BaseEntity<TEntity>, new();
        Exception GenerateModels();

    }
}
