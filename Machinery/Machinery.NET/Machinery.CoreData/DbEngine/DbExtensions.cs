﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Machinery.CoreData.EntityEngine.Annotations;
using Machinery.CoreData.EntityEngine.Base;
using Machinery.CoreData.EntityEngine.Tools;
using Machinery.Extensions.Core;

namespace Machinery.CoreData.DbEngine {
    public static class DbExtensions {

        public static TEntity Locate<TEntity>(this IDbCommand command, TEntity entity)
            where TEntity : BaseEntity<TEntity> {

            var reader = command.ExecuteReader(CommandBehavior.CloseConnection);
            var read = entity.ReadSelf(reader);
            reader.Close();
            return read;

        }

        public static IEnumerable<TEntity> Populate<TEntity>(this IDbCommand command, TEntity entity)
            where TEntity : BaseEntity<TEntity> {

            var reader = command.ExecuteReader(CommandBehavior.CloseConnection);
            var entArr = new List<TEntity>();
            while (reader.Read()) {
                var e = entity.ReadSelf(reader, EReadSelfOptions.DoNotAdvanceReader)
                    .ShallowCopy();
                entArr.Add(e);
            }
            reader.Close();
            return entArr.ToArray();

        }

		public static TScalar Scalar<TScalar>(this IDbCommand command) {

			return command.ExecuteScalar().ToType<TScalar>();

		}

		public static Int64 Identity(this IDbConnection conn) {

			var command = new SqlCommand("SELECT @@IDENTITY ID", conn as SqlConnection);
			return command.ExecuteScalar().ToType<Int64>();

		}

		public static IEnumerable<TEnumerable> Collect<TEntity, TEnumerable>(this TEntity self)
        where TEntity : BaseEntity<TEntity>
        where TEnumerable : BaseEntity<TEnumerable> {

            var cols = self.GetColumns();
            var pkcols = self.GetPrimaryKeys();
            var colpi = cols.Where(pi => pi.PropertyType
                        .GetGenericArguments().FirstOrDefault() != null)
                        .Single(pi =>
                            pi.PropertyType.GetGenericArguments().First().Equals(typeof(TEnumerable)));
            var collkey = colpi.GetCustomAttribute<ReferenceCollectionAttribute>();

            if (collkey != null) {
                var elementType = typeof(TEnumerable);
                var selfTargetingPi = elementType.GetProperties()
                    .Where(pi => pi.GetCustomAttribute<ReferenceKeyAttribute>() != null
                        && pi.PropertyType.Equals(typeof(TEntity))).First();
                var selfTargetKey = selfTargetingPi
                    .GetCustomAttribute<ReferenceKeyAttribute>().ReferenceKey;
                //selfTargetKey = SFormatter.MakeColumnAlias(
                //    SFormatter.MakeTableAlias(elementType), selfTargetKey);

                IDictionary<String, object> selector = new { }.ToExpando();
                selector[selfTargetKey] = pkcols.First().GetValue(self, null);

                var many = SEntity<TEnumerable>.Instance.SelectMany(selector);
                colpi.SetValue(self, many);
                return many;
            }
            return null;

        }

		public static Int64 RunSqlCommand(this IDbConnection conn, String sql,
			Dictionary<String, Object> parameters = null) {

			var command = new SqlCommand(sql, conn as SqlConnection);
			if (parameters != null) {
				parameters.Enumerate(kv => command.Parameters.AddWithValue(kv.Key, kv.Value));
			}
			return command.ExecuteNonQuery();

		}

		public static DbDataReader RunStoredProcedure(this IDbConnection conn, String name,
			params DbParameter[] parameters) {

			var command = new SqlCommand(name, conn as SqlConnection);
			command.CommandType = CommandType.StoredProcedure;
			if (parameters.Count() > 0) {
				command.Parameters.AddRange(parameters);
			}

			return command.ExecuteReader();

		}

    }
}
