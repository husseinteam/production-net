﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Machinery.Extensions.Core;
using Machinery.CoreData.EntityEngine.Annotations;
using Machinery.CoreData.DbEngine;
using Machinery.CoreData.EntityEngine.Tools;

namespace Machinery.CoreData.EntityEngine.Base {
    public class BaseEntity<TEntity> : CrudEngine<TEntity>
        where TEntity : BaseEntity<TEntity> {

        internal TEntity ReadSelf(IDataReader reader,
            EReadSelfOptions options = EReadSelfOptions.Default) {

            var advance = false;
            if (options == EReadSelfOptions.DoNotAdvanceReader) {
                advance = true;
            } else {
                advance = reader.Read();
                if (advance == false) {
                    return default(TEntity);
                }
            }
            if (advance) {

                var pkcols = GetPrimaryKeys();
                foreach (var pkpi in pkcols) {
                    var key = SFormatter.MakeColumnAlias(Alias, pkpi.Name);
                    var value = reader[key];
                    pkpi.SetValue(this, value);
                }

                var cols = GetColumns();
                foreach (var colpi in cols) {
                    if (colpi.GetCustomAttribute<DataColumnAttribute>() != null
                        || colpi.GetCustomAttribute<EnumerationAttribute>() != null) {
                        var key = SFormatter.MakeColumnAlias(Alias, colpi.Name);
						if (reader[key] != DBNull.Value) {
							Object value;
							if (!colpi.PropertyType.IsEnum && 
								typeof(IConvertible).IsAssignableFrom(colpi.PropertyType)) {
								value = Convert.ChangeType(reader[key], colpi.PropertyType);
							} else {
								value = reader[key];
							}
							colpi.SetValue(this, value); 
						}
                    }
                }
                foreach (var colpi in cols) {
                    var rk = colpi.GetCustomAttributes(typeof(ReferenceKeyAttribute), true)
                        .FirstOrDefault() as ReferenceKeyAttribute;
                    if (rk != null) {
                        if (!colpi.PropertyType.Equals(typeof(TEntity))) {
                            var reference = colpi.GetValue(this, null);
                            reference.RunMethod("ReadSelf", new object[] { reader,
                                EReadSelfOptions.DoNotAdvanceReader });
                            colpi.SetValue(this, reference);
                        }
                    }
                }

            }
            return this as TEntity;

        }

        #region Inherited

        [PrimaryKey, Identity, DataColumn("bigint")]
        public Int64 ID { get; set; }

        #endregion

    }
}
