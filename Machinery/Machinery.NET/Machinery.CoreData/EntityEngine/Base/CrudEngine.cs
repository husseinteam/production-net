﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Machinery.CoreData.EntityEngine.Annotations;
using Machinery.CoreData.DbEngine;
using Machinery.CoreData.EntityEngine.Tools;
using Machinery.Extensions.Core;

namespace Machinery.CoreData.EntityEngine.Base {
	public class CrudEngine<TEntity>
		where TEntity : BaseEntity<TEntity> {

		#region ctor
		public CrudEngine() {
			Alias = SFormatter.MakeTableAlias(typeof(TEntity));
		}

		#endregion

		#region Select

		public TEntity SelectThe(Int64 id) {

			var cred = SCredentialProvider.ProvideCurrent();
			using (IDbConnection conn = cred.ProvideSuperAdminConnection()) {
				var pk = GetPrimaryKeys().First();
				var selector = new { }.ToExpando() as IDictionary<String, Object>;
				selector.Add(pk.Name, id);
				var cmd = GenerateSqlCommand(conn, selector);
				cmd.Connection.Open();
				try {
					return cmd.Locate<TEntity>(this as TEntity);
				} catch {
					return default(TEntity);
				} finally {
					cmd.Connection.Close();
				}
			}

		}

		public TScalar SelectCount<TScalar>(object selector = null, object selectornot = null) {

			var cred = SCredentialProvider.ProvideCurrent();
			using (IDbConnection conn = cred.ProvideSuperAdminConnection()) {
				var cmd = GenerateSqlCommand(conn, selector, selectornot, ESelectClauseType.Count);
				cmd.Connection.Open();
				try {
					return cmd.Scalar<TScalar>();
				} catch {
					return default(TScalar);
				} finally {
					cmd.Connection.Close();
				}
			}

		}

		public TEntity SelectOne(object selector = null) {

			var cred = SCredentialProvider.ProvideCurrent();
			using (IDbConnection conn = cred.ProvideSuperAdminConnection()) {
				var cmd = GenerateSqlCommand(conn, selector);
				cmd.Connection.Open();
				try {
					return cmd.Locate<TEntity>(this as TEntity);
				} catch {
					return default(TEntity);
				} finally {
					cmd.Connection.Close();
				}
			}

		}

		public IEnumerable<TEntity> SelectMany(object selector = null, object selectornot = null) {

			var cred = SCredentialProvider.ProvideCurrent();
			using (IDbConnection conn = cred.ProvideSuperAdminConnection()) {
				var cmd = GenerateSqlCommand(conn, selector, selectornot);
				cmd.Connection.Open();
				try {
					return cmd.Populate<TEntity>(this as TEntity);
				} catch {
					return new TEntity[0];
				} finally {
					cmd.Connection.Close();
				}
			}

		}

		internal IDbCommand GenerateSqlCommand(IDbConnection conn, object selector = null, object selectornot = null, ESelectClauseType genType = ESelectClauseType.AllColumns) {

			var et = GetEntityTitle();
			var sqlb = new StringBuilder(
				"SELECT #select# FROM {0} {1} #join# ".PostFormat(et, Alias));
			GenerateCommandText(sqlb, genType);
			var qtext = sqlb.Replace("#join#", "").Replace(", #select#", "").ToString();

			var where = new StringBuilder();
			SqlCommand command = new SqlCommand();
			if (selector != null) {
				var dict = selector as IDictionary<String, Object>;
				if (dict != null) {
					foreach (var kv in dict) {
						command.Parameters.AddWithValue(kv.Key, kv.Value);
						where.AppendFormat("[{2}].[{0}]=@{1} AND ", kv.Key, kv.Key, Alias);
					}
				} else {
					foreach (var spi in selector.GetAnonymousProperties()) {
						command.Parameters.AddWithValue(spi.Name, spi.GetValue(selector, null));
						where.AppendFormat("[{2}].[{0}]=@{1} AND ", spi.Name, spi.Name, Alias);
					}
				}
			}
			if (selectornot != null) {
				var dict = selectornot as IDictionary<String, Object>;
				if (dict != null) {
					foreach (var kv in dict) {
						if (!command.Parameters.Contains(kv.Key)) {
							command.Parameters.AddWithValue(kv.Key, kv.Value);
						}
						where.AppendFormat("[{2}].[{0}]=@{1} AND ", kv.Key, kv.Key, Alias);
					}
				} else {
					foreach (var spi in selectornot.GetAnonymousProperties()) {
						if (!command.Parameters.Contains(spi.Name)) {
							command.Parameters.AddWithValue(spi.Name, spi.GetValue(selectornot, null));
						}
						where.AppendFormat("[{2}].[{0}]!=@{1} AND ", spi.Name, spi.Name, Alias);
					}
				}
			}
			var wh = where.ToString().TrimLast(" AND ");
			command.CommandText = "{0} {1}".PostFormat(qtext,
						wh.IsUnassigned() ? "" : "WHERE {0}".PostFormat(wh));
			command.Connection = conn as SqlConnection;
			return command;

		}

		internal void GenerateCommandText(StringBuilder sqlb, ESelectClauseType genType = ESelectClauseType.AllColumns) {

			if (genType == ESelectClauseType.AllColumns) {
				var pkcols = GetPrimaryKeys();
				foreach (var pkpi in pkcols) {
					sqlb.Replace("#select#", @"{0}.[{1}] AS {2}, #select#".PostFormat(
						Alias, pkpi.Name, SFormatter.MakeColumnAlias(Alias, pkpi.Name)));
				}

				var cols = GetColumns();

				foreach (var colpi in cols) {
					var dt = colpi.GetCustomAttributes(typeof(DataColumnAttribute), true)
					   .FirstOrDefault() as DataColumnAttribute;
					var en = colpi.GetCustomAttributes(typeof(EnumerationAttribute), true)
					   .FirstOrDefault() as EnumerationAttribute;
					if (dt != null || en != null) {
						sqlb.Replace("#select#", @"{0}.[{1}] AS {2}, #select#".PostFormat(
							Alias, colpi.Name, SFormatter.MakeColumnAlias(Alias, colpi.Name)));
					}
				}

				foreach (var colpi in cols) {
					var refkey = colpi.GetCustomAttribute<ReferenceKeyAttribute>();
					var nl = colpi.GetCustomAttribute<NullableAttribute>();
					if (refkey != null && nl == null) {
						var refColInstance = Activator.CreateInstance(colpi.PropertyType, true);
						var refTableAlias = refColInstance.PropertyFor<String>("Alias");
						var selfET = GetEntityTitle();
						var referenceET = colpi.PropertyType.GetCustomAttribute<EntityTitleAttribute>();
						var pkattr = GetPrimaryKeys().First();
						var referencedPKName = colpi.PropertyType.GetProperties().Single(pp => pp.GetCustomAttribute<PrimaryKeyAttribute>() != null).Name;
						sqlb.Replace("#join#", @"
                        left join {0} {1} on {1}.{2} = {3}.{4}
                        #join#".PostFormat(referenceET, refTableAlias,
								pkattr.Name, Alias, refkey.ReferenceKey));
						colpi.SetValue(this, refColInstance);
						if (!colpi.PropertyType.Equals(typeof(TEntity))) {
							refColInstance.RunMethod("GenerateCommandText", new Object[] { sqlb, genType });
						}
					}
				}

			} else if (genType == ESelectClauseType.Count) {
				sqlb.Replace("#select#", "COUNT(*) #select#");
			}

		}

		public EntityTitleAttribute GetEntityTitle() {

			return typeof(TEntity).GetClassDecoration<EntityTitleAttribute>();

		}

		public IEnumerable<PropertyInfo> GetColumns() {

			return typeof(TEntity).GetPublicProperties().Where(pi =>
				pi.GetCustomAttribute<PrimaryKeyAttribute>() == null);

		}

		#endregion

		#region CRUD

		public TEntity InsertSelf() {

			StringBuilder paramList = new StringBuilder(), columnList = new StringBuilder();

			var p = GenerateParameters((colpi, dp, rowattr) => {
				var colvalue = colpi.GetValue(this, null);
				var dt = colpi.GetCustomAttribute<DefaultValueAttribute>();
				paramList.AppendFormat("@{0}, ", colpi.Name);
				columnList.AppendFormat("[{0}], ", colpi.Name);
				if (dt != null) {
					dp.Add(colpi.Name, colvalue ??
						Convert.ChangeType(dt.Value, colpi.PropertyType));
				} else {
					dp.Add(colpi.Name, colvalue);
				}
			}, (colpi, dp, rkattr) => {
				var colvalue = colpi.GetValue(this, null);
				var rkeyAttribute = colpi.GetCustomAttribute<ReferenceKeyAttribute>();
				paramList.AppendFormat("@{0}, ", rkeyAttribute.ReferenceKey);
				columnList.AppendFormat("[{0}], ", rkeyAttribute.ReferenceKey);
				var referencedPKName = colpi.PropertyType.GetProperties().Single(pp => pp.GetCustomAttribute<PrimaryKeyAttribute>() != null).Name;
				var referencedPKValue = colpi.PropertyType.PropertyFor(referencedPKName)
					.GetValue(colvalue, null);
				if (referencedPKValue.IsUnassigned()) {
					colvalue.RunMethod("InsertSelf", null);
					referencedPKValue = colpi.PropertyType.PropertyFor(referencedPKName)
						.GetValue(colvalue, null);
				}
				dp.Add(rkeyAttribute.ReferenceKey, referencedPKValue);
			}, (colpi, dp, enumattr) => {
				paramList.AppendFormat("@{0}, ", colpi.Name);
				columnList.AppendFormat("[{0}], ", colpi.Name);
				dp.Add(colpi.Name, colpi.GetValue(this, null).ToCode());
			});

			var values = "VALUES({0})".PostFormat(paramList.ToString().Trim().TrimEnd(','));
			var inserts = "({0})".PostFormat(columnList.ToString().Trim().TrimEnd(','));

			var sql = "INSERT INTO {0} {1} {2}".PostFormat(GetEntityTitle(),
				inserts != "()" ? inserts : "", values != "VALUES()" ? values : "DEFAULT VALUES");

			IdentityInsert(sql, p);
			return this as TEntity;
		}

		public TEntity InsertCollections<TEnumerable>()
			where TEnumerable : BaseEntity<TEnumerable> {

			var cols = GetColumns();
			foreach (var colpi in cols) {
				var colvalue = colpi.GetValue(this, null);
				if (colpi.GetCustomAttribute<ReferenceCollectionAttribute>() != null) {
					var elementType = colpi.PropertyType.GetGenericArguments().First();
					var refPk = elementType.GetPublicProperties().Single(
						pi => pi.GetCustomAttribute<ReferenceKeyAttribute>() != null &&
						pi.PropertyType.Equals(typeof(TEntity)));
					var pkid = GetPrimaryKeys().First().GetValue(this, null);
					if (!pkid.IsUnassigned()) {
						var refkey = refPk.GetCustomAttribute<ReferenceKeyAttribute>().ReferenceKey;
						var collection = typeof(TEntity)
							.PropertyFor(colpi.Name).GetValue(this, null) as IEnumerable<TEnumerable>;
						collection.Enumerate(ent => ent.Assign(e =>
							elementType.PropertyFor(refPk.Name).SetValue(e, this))
							.InsertSelf());
					}
				}
			}
			return this as TEntity;

		}

		public TEntity DeleteSelf() {

			var where = new StringBuilder();
			GetPrimaryKeys().Enumerate(pkpi => where.AppendFormat("[{0}]={1} AND ",
				pkpi.Name, pkpi.GetValue(this, null)));
			var sql = "DELETE FROM {0} WHERE {1}".PostFormat(GetEntityTitle()
				, where.ToString().TrimLast(" AND "));
			ExecuteSql(sql);
			return this as TEntity;

		}

		public TEntity UpdateSelf() {

			var where = new StringBuilder();
			GetPrimaryKeys().Enumerate(pkpi => where.AppendFormat("[{0}]={1} AND ",
				pkpi.Name, pkpi.GetValue(this, null)));

			var setList = new StringBuilder();
			var p = GenerateParameters((colpi, dp, rowattr) => {
				var colvalue = colpi.GetValue(this, null);
				setList.AppendFormat("[{0}]=@{0}, ", colpi.Name);
				dp.Add(colpi.Name, colvalue);
			}, (colpi, dp, rkattr) => {
				var colvalue = colpi.GetValue(this, null);
				var rkeyAttribute = colpi.GetCustomAttribute<ReferenceKeyAttribute>();
				var referencedPKName = colpi.PropertyType.GetProperties().Single(pp => pp.GetCustomAttribute<PrimaryKeyAttribute>() != null).Name;
				var referencedPKValue = colpi.PropertyType.PropertyFor(referencedPKName)
					.GetValue(colvalue, null);
				if (!referencedPKValue.IsUnassigned()) {
					colvalue.RunMethod("UpdateSelf", null);
					referencedPKValue = colpi.PropertyType.PropertyFor(referencedPKName)
						.GetValue(colvalue, null);
				}
				setList.AppendFormat("[{0}]=@{0}, ", rkeyAttribute.ReferenceKey);
				dp.Add(rkeyAttribute.ReferenceKey, referencedPKValue);
			}, (colpi, dp, enumattr) => {
				setList.AppendFormat("[{0}]=@{0}, ", colpi.Name);
				dp.Add(colpi.Name, colpi.GetValue(this, null).ToCode());
			});

			var sql = "UPDATE {0} SET {1} WHERE {2}".PostFormat(GetEntityTitle()
				, setList.ToString().Trim().TrimEnd(','), where.ToString().TrimLast(" AND "));
			ExecuteSql(sql, p);
			return this as TEntity;

		}

		#endregion

		#region Sql Execution

		private void IdentityInsert(String sql, Dictionary<String, Object> p = null) {

			var pkpi = GetPrimaryKeys().First();
			var cred = SCredentialProvider.ProvideCurrent();
			using (IDbConnection conn = cred.ProvideSuperAdminConnection()) {
				conn.Open();
				try {
					conn.RunSqlCommand(sql, p);
					pkpi.SetValue(this, conn.Identity());
					Fault = Fault ?? null;
				} catch (Exception ex) {
					Fault = ex;
				} finally {
					conn.Close();
				}
			}

		}

		private void ExecuteSql(String sql, Dictionary<String, Object> p = null) {

			var cred = SCredentialProvider.ProvideCurrent();
			using (IDbConnection conn = cred.ProvideSuperAdminConnection()) {
				conn.Open();
				try {
					conn.RunSqlCommand(sql, p);
					Fault = Fault ?? null;
				} catch (Exception ex) {
					Fault = ex;
				} finally {
					conn.Close();
				}
			}

		}

		#endregion

		#region Properties

		internal String Alias { get; set; }
		public Exception Fault { get; set; }

		#endregion

		#region  Private
		private Dictionary<String, Object> GenerateParameters(Action<PropertyInfo, Dictionary<String, Object>, DataColumnAttribute> rowActor,
			Action<PropertyInfo, Dictionary<String, Object>, ReferenceKeyAttribute> rkActor,
			Action<PropertyInfo, Dictionary<String, Object>, EnumerationAttribute> enumActor) {

			var p = new Dictionary<String, Object>();
			var cols = GetColumns();

			foreach (var colpi in cols) {
				var colvalue = colpi.GetValue(this, null);
				if (!colvalue.IsUnassigned()) {
					if (colpi.GetCustomAttribute<DataColumnAttribute>() != null) {
						rowActor(colpi, p, colpi.GetCustomAttribute<DataColumnAttribute>());
					}
					if (colpi.GetCustomAttribute<EnumerationAttribute>() != null) {
						enumActor(colpi, p, colpi.GetCustomAttribute<EnumerationAttribute>());
					}
				}
			}

			foreach (var colpi in cols) {
				var colvalue = colpi.GetValue(this, null);
				if (!colvalue.IsUnassigned()) {
					if (colpi.GetCustomAttribute<ReferenceKeyAttribute>() != null) {
						rkActor(colpi, p, colpi.GetCustomAttribute<ReferenceKeyAttribute>());
					}
				}
			}

			return p;

		}

		internal IEnumerable<PropertyInfo> GetPrimaryKeys() {

			var cols = typeof(TEntity).GetPublicProperties();
			foreach (var colpi in cols) {
				if (colpi.GetCustomAttribute<PrimaryKeyAttribute>() != null) {
					yield return colpi;
				}
			}
		}

		#endregion

	}
}
