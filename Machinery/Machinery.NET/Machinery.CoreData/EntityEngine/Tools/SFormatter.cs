﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Machinery.Extensions.Core;

namespace Machinery.CoreData.EntityEngine.Tools {
    internal static class SFormatter {

        internal static String MakeTableAlias(Type t) {

            return "{0}_{1}".PostFormat(t.Name.ToLowerInvariant(), Guid.NewGuid().ToString("N"));

        }

        internal static String MakeColumnAlias(String tableAlias, String columnName) {

            return "{0}_{1}".PostFormat(tableAlias, columnName);

        }
    }
}
