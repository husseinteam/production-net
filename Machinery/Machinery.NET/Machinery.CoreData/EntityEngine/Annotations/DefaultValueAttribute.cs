﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinery.CoreData.EntityEngine.Annotations {
    [System.AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public sealed class DefaultValueAttribute : Attribute {

        readonly Object value;

        public DefaultValueAttribute(Object value) {
            this.value = value;

        }

        public Object Value {
            get { return value; }
        }

    }
}
