﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinery.Instruments.Constructs.Credentials;
using Machinery.Instruments.Constructs.Manifests.Manifest;
using Machinery.Instruments.Protocols;

namespace Machinery.Instruments.InstanceProvider {
    public static class SManifester {

        #region Providers

        public static IManifest<String> ToMessage() {

            return _Message.Value;

        }

        public static IConnectionCredential ToConnectionCredential(EServiceType serviceType) {

            return new ConnectionCredential(serviceType);

        }

        #endregion

        #region Fields

        private static Lazy<IManifest<String>> _Message = new Lazy<IManifest<String>>(() => new Message());

        #endregion

    }
}
