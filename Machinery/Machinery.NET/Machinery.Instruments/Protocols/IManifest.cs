﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machinery.Instruments.Protocols {
    public interface IManifest<TItem> {

        TItem Announce(String key);

    }
}
