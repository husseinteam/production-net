﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinery.Instruments.Constructs.Manifests.Base;

namespace Machinery.Instruments.Constructs.Manifests.Manifest {
    internal class Message : BaseManifest<String> {

        protected override String Category {
            get {
                return "Message";
            }
        }

        protected override Dictionary<String, String> Book {
            get {
                return new Dictionary<String, String>()
                {
                    { "InvalidServiceType", "ServiceType: <{0}> is invalid, no such implementation" },
                    { "PostgreSqlConnectionManagementNotImplemented", "PostgreSql connection generation failed no such implementation" },
                    { "ConnectionToServerFailed", "Connection to <{0}> string failed" },
                };
            }
        }
    }
}
