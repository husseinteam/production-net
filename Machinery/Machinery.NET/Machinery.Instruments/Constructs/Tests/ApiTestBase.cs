﻿using Machinery.Extensions.Core;
using System;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web.Http;

namespace Machinery.Instruments.Constructs.Tests {

    public abstract class ApiTestBase {

        #region Inherited

        protected async Task<TResponse> ClientCallGetAsync<TResponse>(String path)
            where TResponse : class {

            using (var client = new HttpClient()) {
                var uri = "http://localhost:{0}/{1}/{2}/{3}".PostFormat(
                   Port, ApiRoute, ControllerRoutePrefix, path);
                var responseMessage = await client.GetAsync(uri, HttpCompletionOption.ResponseContentRead);
                responseMessage.EnsureSuccessStatusCode();
                if (responseMessage.IsSuccessStatusCode) {
                    var response =
                        await responseMessage.Content.ReadAsAsync<TResponse>();
                    return response;
                } else {
                    return default(TResponse);
                }
            }

        }

        protected async Task<TEntity> ClientCallPostAsync<TEntity>(String path
            , TEntity entity)
            where TEntity : class {

            using (var client = new HttpClient()) {

				var uri = "http://localhost:{0}/{1}/{2}/{3}".PostFormat(
				   Port, ApiRoute, ControllerRoutePrefix, path);
                var response = await client.PostAsync<TEntity>(uri, entity, JsonFormatter);
                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode) {
                    return await response.Content.ReadAsAsync<TEntity>(new MediaTypeFormatter[] { JsonFormatter });
                } else {
                    return default(TEntity);
                }
            }

        }

        #endregion

        #region Abstract

        protected abstract String ControllerRoutePrefix { get; }
        protected abstract String ApiRoute { get; }
        protected abstract JsonMediaTypeFormatter JsonFormatter { get; }
		protected abstract Int32 Port { get; }

        #endregion


	}

}
