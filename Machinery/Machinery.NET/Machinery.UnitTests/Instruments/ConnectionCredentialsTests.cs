﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Machinery.Instruments.InstanceProvider;
using Machinery.Instruments.Protocols;
using Machinery.Extensions.Core;
using System.Data.SqlClient;
using Machinery.Instruments.Constructs.Credentials;
using System.Data;

namespace Machinery.UnitTests.Instruments {
	[TestClass]
	public class ConnectionCredentialTests {

		[TestMethod]
		public void CredentialTestMSSQLConnectionString() {

			var cred = SManifester.ToConnectionCredential(EServiceType.MSSQL);
			var str = cred.Assign(cr => cr.Server = ".")
				.Assign(cr => cr.Database = "ondb")
				.Assign(cr => cr.UserID = "onadmin")
				.Assign(cr => cr.Password = "1q2w3e4r5t").ToString();
			Assert.AreEqual("Server=.;Initial Catalog=ondb;User Id=onadmin;Password=1q2w3e4r5t;MultipleActiveResultSets=true;", str);

		}

		[TestMethod]
		public void ProvideConnectionRunsOK() {

			var cred = SManifester.ToConnectionCredential(EServiceType.MSSQL);
			cred.Assign(cr => cr.Server = ".")
				.Assign(cr => cr.Database = "bbdb")
				.Assign(cr => cr.UserID = "bbadmin")
				.Assign(cr => cr.Password = "1q2w3e4r5t");
			Assert.AreEqual(ConnectionState.Open, cred.ProvideConnection<SqlConnection>().Assign(conn => conn.Open()).State);

		}

		[TestMethod]
		public void CredentialTestAzureSqlConnectionString() {
			var cred = SManifester.ToConnectionCredential(EServiceType.AzureSql);
			cred.Assign(cr => cr.Server = "tcp:g6xqbb8gqs.database.windows.net")
				.Assign(cr => cr.Database = "bbdb")
				.Assign(cr => cr.SuperAdminUserID = "bbsa@g6xqbb8gqs")
				.Assign(cr => cr.SuperAdminPassword = "1q2w3e4r5t")
				.Assign(cr => cr.UserID = "bbadmin")
				.Assign(cr => cr.Password = "1q2w3e4r5t")
				.Assign(cr => cr.Port = 1433);
			Assert.AreEqual("Server=tcp:g6xqbb8gqs.database.windows.net,1433;Database=bbdb;User Id=bbsa@g6xqbb8gqs;Password=1q2w3e4r5t;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;", cred.ProvideSuperAdminConnection().ConnectionString);
			Assert.AreEqual("Server=tcp:g6xqbb8gqs.database.windows.net,1433;Database=bbdb;User Id=bbadmin;Password=1q2w3e4r5t;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;", cred.ProvideConnection().ConnectionString);

		}

		[TestMethod]
		public void CredentialNamedPipeTestsConnectionString() {

			var cred = SManifester.ToConnectionCredential(EServiceType.MSSQLNamedPipe);
			var str = cred.Assign(cr => cr.Server = "code-db.mssql.somee.com")
				.Assign(cr => cr.Database = "code-db")
				.Assign(cr => cr.UserID = "lampiclobe_SQLLogin_1")
				.Assign(cr => cr.Password = "2bwe1v99iv").ToString();
			Assert.AreEqual(
				"workstation id=code-db.mssql.somee.com;packet size=4096;user id=lampiclobe_SQLLogin_1;pwd=2bwe1v99iv;data source=code-db.mssql.somee.com;persist security info=False;initial catalog=code-db", str);

		}

		[TestMethod]
		public void CredentialContainerTest() {

			var container = new CredentialContainer();
			container["named"] = SManifester.ToConnectionCredential(EServiceType.MSSQLNamedPipe);
			container["instance"] = SManifester.ToConnectionCredential(EServiceType.MSSQL);
			Assert.IsNotNull(container["named"].ServiceType);
			Assert.AreEqual(EServiceType.MSSQLNamedPipe, container["named"].ServiceType);

			Assert.IsNotNull(container["instance"].ServiceType);
			Assert.AreEqual(EServiceType.MSSQL, container["instance"].ServiceType);

		}

	}
}
