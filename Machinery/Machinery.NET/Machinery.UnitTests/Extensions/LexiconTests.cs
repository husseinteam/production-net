﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using Machinery.Extensions.Core;
using System.Diagnostics;

namespace Machinery.UnitTests.Extensions {

    [TestClass]
    public class LexiconTests {

        [TestMethod]
        public void GenerateIdentifierWorksProperly() {

            var identifier = "{0}-{{0}}".GenerateNumber().GenerateIdentifier();
            Assert.IsTrue(identifier.Length > 0, identifier);
            Assert.AreNotEqual(Enumerable.Count(identifier, (ch) => Char.IsUpper(ch)), 0);
            Debug.Write(identifier);
            for (int i = 0; i < 10000; i++) {
                identifier = "{0}-{{0}}".GenerateNumber().GenerateIdentifier();
                Assert.IsFalse(identifier.Contains("'"));
                Assert.IsFalse(identifier.Contains("."));
            }
        }

        [TestMethod]
        public void GenerateTokenRunsOK() {

            var tokenized = "{0}".GenerateToken();
            Assert.AreNotEqual(0, tokenized.Length);
            Debug.Write(tokenized);

        }

    }
}
