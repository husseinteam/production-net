﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Machinery.Extensions.Core;

namespace Machinery.UnitTests.Extensions {
    [TestClass]
    public class AssignmentTests {

        [TestMethod]
        public void IfExistsRunsOK() {

            String nullString = null;
            nullString.IfExists(str => str = "123");

            Assert.AreEqual(null, nullString);
            nullString = "123";
            var deferred = nullString.IfExists(str => str + "456");
            Assert.AreEqual("123456", deferred);
            Assert.AreEqual("123", nullString);

        }

        [TestMethod]
        public void IfPropertyExistsRunsOK() {

            var dc = new DummyClass() {
                Prop1 = 12,
                Prop2 = "12"
            };
            DummyClass nulldc = new DummyClass();
            Assert.AreEqual(12, dc.IfPropertyExists(d => d.Prop1));
            Assert.AreEqual(0, nulldc.IfPropertyExists(d => d.Prop1));
            Assert.AreNotEqual("12", nulldc.IfPropertyExists(d => d.Prop2));

        }

        [TestMethod]
        public void DigestRunsOK() {

            var t1 = new DummyClass() {
                Prop1 = 12,
                Prop2 = "12"
            };

            var t2 = new DummyClass();

            t2.Digest(t1);
            Assert.AreEqual(12, t2.Prop1);
            Assert.AreEqual("12", t2.Prop2);

        }

        internal class DummyClass {

            public Int32 Prop1 { get; set; }
            public String Prop2 { get; set; }

        }

    }
}
