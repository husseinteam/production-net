﻿using System;
using System.Dynamic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Machinery.Extensions.Core;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Machinery.UnitTests.Extensions {

    [TestClass]
    public class ParserTests {

        [TestMethod]
        public void TransferToWorksProperly() {

            MyClass1 mc1 = new MyClass1("myField1") {
                MyProperty = 1
            };
            dynamic expando = new ExpandoObject();
            expando.MyProperty = mc1.MyProperty;
            expando.MyField = mc1.MyField;

            mc1.TransferTo(expando as Object);
            Assert.AreEqual(mc1.MyProperty, expando.MyProperty);
            Assert.AreEqual(mc1.MyField, expando.MyField);

        }

        [TestMethod]
        public void MapToWorksProperly() {

            MyClass1 mc1 = new MyClass1("myField1") {
                MyProperty = 1
            };
            var mc2 = new MyClass2("myField1");

            mc1.MapTo(ref mc2);
            Assert.AreEqual(mc1.MyProperty, mc2.MyProperty);
            Assert.AreEqual(mc1.MyField, mc2.MyField);

        }

        [TestMethod]
        public void MapToDirectWorksProperly() {

            MyClass1 mc1 = new MyClass1("myField1") {
                MyProperty = 1
            };
            var mc2 = new MyClass2("myField1");

            mc2 = mc1.MapToDirect(() => new MyClass2("myField1"));
            Assert.AreEqual(mc1.MyProperty, mc2.MyProperty);
            Assert.AreEqual(mc1.MyField, mc2.MyField);

        }

        [TestMethod]
        public void ImportWorksProperly() {

            MyClass1 mc1 = new MyClass1("myField1");

            dynamic expando = new ExpandoObject();
            expando.MyProperty = 1;
            expando.MyField = "myField11";

            mc1.Import<MyClass1>(expando as Object);
            Assert.AreEqual(mc1.MyProperty, expando.MyProperty);
            Assert.AreEqual("myField11", expando.MyField);

        }

        [TestMethod]
        public void GetDecoratorsProperly() {

            var mc1 = new MyClass1("myField1");
            var c = typeof(MyClass1).GetAllPropertyDecorations<RequiredAttribute>().Count();
            Assert.AreEqual(2, c);
        }

        [TestMethod]
        public void IsUnassignedRunsOK() {

            Assert.IsTrue("".IsUnassigned());

        }

        [TestMethod]
        public void ShallowCopyRunsOK() {

            var m1 = new MyClass1() {
                MyField = "fe",
                MyProperty = 13,
                MyPropertyRequired = 14,
                ClassObject2 = new MyClass2() {
                    MyField = "15",
                    MyProperty = 18
                }
            };

            var m2 = m1.ShallowCopy();
            Assert.AreEqual(m2.MyPropertyRequired, m1.MyPropertyRequired);
            Assert.AreEqual(m2.MyProperty, m1.MyProperty);
            Assert.AreEqual(m2.ClassObject2.MyProperty, m1.ClassObject2.MyProperty);

        }

        [TestMethod]
        public void DefaultIfNullRunsOk() {

            var mc = new MyClass1() {
                MyPropertyRequired = 12
            };
            Assert.AreEqual(12, mc.DefaultIfNull(m => m.MyPropertyRequired, 13));

        }

    }

    class MyClass1 {

        [Required]
        public int MyProperty { get; set; }
        [Required]
        public int MyPropertyRequired { get; set; }
        [Display]
        internal String MyField;
        public MyClass2 ClassObject2 { get; set; }

        public MyClass1(String myField) {
            MyField = myField;
        }

        public MyClass1() {

        }

    }

    class MyClass2 {

        public int MyProperty { get; set; }
        internal String MyField;

        public MyClass2(String myField) {
            MyField = myField;
        }

        public MyClass2() {

        }

    }
}
