﻿using System;
using System.ComponentModel.DataAnnotations;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Machinery.Extensions.Core;
using System.Linq;
using System.Collections.Generic;

namespace Machinery.UnitTests.Extensions {
    [TestClass]
    public class EnumExtensionsTests {

        [TestMethod]
        public void GetDescriptionRunsOK() {

            var na = EProperty.NotApplicable;
            Assert.AreEqual("NotApplicable", na.GetDescription());

        }

        [TestMethod]
        public void ToEnumRunsOK() {

            var no = "No";
            var en = no.ToEnum<EProperty>(e => e.GetDescription());
            Assert.AreEqual(EProperty.No, en);

        }

        [TestMethod]
        public void EnumDescriptionInRunsOK() {

            Assert.IsTrue("No".EnumDescriptionIn<EProperty>());
            Assert.IsFalse("Noo".EnumDescriptionIn<EProperty>());
            Assert.IsTrue("Yes".EnumDescriptionIn<EProperty>());
            Assert.IsTrue("NotApplicable".EnumDescriptionIn<EProperty>());

        }


        [TestMethod]
        public void EnumValueInRunsOK() {

            Assert.IsTrue("No".EnumValueIn<EProperty>(en => en.GetDescription()));
            Assert.IsFalse("Noo".EnumValueIn<EProperty>(en => en.GetDescription()));

        }
        [TestMethod]
        public void EnlistEnumDescriptionsRunsOK() {

            var list = new List<String>().EnlistEnumDescriptions<EProperty>();
            Assert.AreEqual(3, list.Count);

        }

        [TestMethod]
        public void PassOverEnumRunsOK() {

            var list = new List<String>().PassOverEnum<EProperty>(en => en.GetDescription());
            Assert.AreEqual(3, list.Count);

        }

        public enum EProperty {

            [Display(Name = "Yes")]
            Yes,
            [Display(Name ="No")]
            No,
            [Display(Name = "NotApplicable")]
            NotApplicable

        }

        public enum EState {
            [System.ComponentModel.Description("Published")]
            Published = 1,

            [System.ComponentModel.Description("Accepted")]
            Accepted

        }


    }
}
