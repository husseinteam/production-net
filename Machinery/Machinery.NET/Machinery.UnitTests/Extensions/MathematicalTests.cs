﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinery.Extensions.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Machinery.UnitTests.Extensions {

    [TestClass]
    public class MathematicalTests {

        [TestMethod]
        public void PercentiseRunsOK() {

            var s = 0.4f;
            Assert.AreEqual(40, s.Percentise());

        }

    }

}
