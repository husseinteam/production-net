﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Machinery.Extensions.Core;
using System.Collections.Generic;
using System.Threading;

namespace Machinery.UnitTests.Extensions {
    [TestClass]
    public class EnumerableTests {
        [TestMethod]
        public void SelectByWorksProperly() {

            var array = new Int32[] { 1, 2, 4 };
            var filtered = array.SelectBy(n => n % 2 == 1);
            Assert.AreEqual(1, filtered.Count());
            Assert.AreEqual(filtered.ElementAt(0), 1);

        }

        [TestMethod]
        public void ExtendRunsProperly() {

            var list = new List<Int32>() { 1, 2, 4 };
            list.Extend(5, 6, 7);
            Assert.AreEqual(6, list.Count);

			list.Extend(12).Extend(144);
			Assert.AreEqual(8, list.Count);

        }


        [TestMethod]
        public void InRunsOK() {

            var list = new List<Int32>() { 1, 2, 4 };
            var r = list.Shuffle();
            Assert.IsTrue(1.In(list.ToArray()));
            Assert.IsFalse(5.In(list.ToArray()));

        }
        [TestMethod]
        public void UniqueRunsOK() {

            var list = new List<Int32>() { 1, 2, 4, 4, 4, 6, 6, 7, 7, 9, 9, 9, 9 };
            var r = list.Unique();
            Assert.AreEqual(6, r.Count());

            var tagList = new List<TagItem>();
            10.Times(i => tagList.Add(new TagItem() { Description = i.ToString() }));
            var filtered = tagList.Unique(t => t.Description);
            Assert.AreEqual(10, filtered.Count());

            10.Times(idx => tagList.AddRange(10.Times(i => new TagItem() { Description = i.ToString() })));
            filtered = tagList.Unique(t => t.Description);
            Assert.AreEqual(11, filtered.Count());

        }

        [TestMethod]
        public void ReverseRunsOK() {

            var array = new Int32[] { 1, 2, 3, 4, 5, 6, 7 };
            var reversed = array.Invert();
            Assert.AreEqual(7, reversed.ElementAt(0));
        }

        [TestMethod]
        public void EnumerateRunsOK() {

            var arr = new Int32[] { 0, 1, 2, 3, 4, 5, 6 };
            arr.Enumerate((i, item) => Assert.AreEqual(i, item));

        }

        [TestMethod]
        public void TimesRunsOK() {

            var hundreds = 100.Times(k => new { Value = k, Text = k.ToString() });

            Assert.AreEqual(hundreds.ElementAt(2).Value, 3);


        }

    }

    public class TagItem   {

        public String Description { get; set; }
    }

}
