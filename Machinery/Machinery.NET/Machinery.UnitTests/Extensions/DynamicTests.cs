﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Machinery.Extensions.Core;
using System.Collections.Generic;
using System.Linq;

namespace Machinery.UnitTests.Extensions {
    [TestClass]
    public class DynamicTests {

        [TestMethod]
        public void ToExpandoRunsOK() {

            var self = new {
                ID = 12,
                Prop2 = "12"
            };

            var to = self.ToExpando(m => m.IDD = 2);
            Assert.AreEqual(12, to.ID);
            Assert.AreEqual(2, to.IDD);

        }

        [TestMethod]
        public void TransforToGenericRunsOK() {

            var td1 = new TransferDemoClass() {
                Number = 13,
                Text = "mekke",
                Texts = new String[] { "Selamün", "aleyküm" }
            };
            var td2 = new TransferDemoClass() {
                Number = 10,
                Text = "medine"
            };
            td1.TransferTo(td2, (pi) => pi.Name == "Texts", (pi) => td1.Texts.Select(_ =>_.ToUpperInvariant()).ToArray());
            Assert.IsTrue(td2.Texts.All(t => t.All(c => Char.IsUpper(c))));

        }

        [TestMethod]
        public void TransferToObjectRunsOK() {

            var td1 = new TransferDemoClass() {
                Number = 13,
                Text = "mekke",
                Texts = new String[] { "Selamün", "aleyküm" },
                Inner = new Inner() {
                    Inner1 = 14
                }
            };
            var td2 = new TransferDemoClass() {
                Number = 10,
                Text = "medine"
            };
            td1.TransferTo(td2);
            Assert.IsNotNull(td2.Inner);

        }

    }

    class TransferDemoClass {

        public Int32 Number { get; set; }

        public String Text { get; set; }

        public Inner Inner { get; set; }

        public ICollection<String> Texts{ get; set; }

    }

    class Inner {
        public int Inner1 { get; set; }
    }
}
