﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Machinery.Extensions.Core;
using System.Globalization;

namespace Machinery.UnitTests.Extensions {

    [TestClass]
	public class CultureTests {

		[TestMethod]
		public void GlobalizeRunsOK() {

			var cultureName = "tr-TR";
			var culture = new CultureInfo("en-US");
			culture = cultureName.Globalize();
			Assert.AreEqual(cultureName, culture.Name);

		}

    }
}
