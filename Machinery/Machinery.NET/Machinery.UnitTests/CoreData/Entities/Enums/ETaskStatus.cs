﻿using System.ComponentModel;

namespace Machinery.UnitTests.CoreData.Entities.Enums {

    public enum ETaskStatus {

		[Description("info")]
        Applied = 1,
        [Description("important")]
        Reviewed,
		[Description("warning")]
        Ignored,
        [Description("info")]
        Approved,
		[Description("success")]
        Completed

    }
}