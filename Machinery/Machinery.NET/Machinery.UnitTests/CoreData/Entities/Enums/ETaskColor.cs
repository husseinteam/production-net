﻿using System.ComponentModel;

namespace Machinery.UnitTests.CoreData.Entities.Enums {

    public enum ETaskColor {

        [Description("green")]
        Green = 1,
        [Description("red")]
        Red,
        [Description("yellow")]
        Yellow,
        [Description("greenLight")]
        GreenLight

    }
}