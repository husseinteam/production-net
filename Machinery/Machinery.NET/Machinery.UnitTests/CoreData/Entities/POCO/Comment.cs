﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinery.CoreData.EntityEngine.Annotations;
using Machinery.CoreData.EntityEngine.Base;

namespace Machinery.UnitTests.CoreData.Entities.POCO {

    [EntityTitle("Comments", Schema = "Mainframe")]
    public class Comment : BaseEntity<Comment> {

        [DataColumn("nvarchar", MaxLength = 128)]
        public String Text { get; set; }

        [DataColumn("datetime")]
        public DateTime CommentedOn { get; set; }

        [DataColumn("nvarchar", MaxLength = 255)]
        public String CommenterIdentity { get; set; }

        [DataColumn("bit")]
        public Boolean Active { get; set; }
    }
}
