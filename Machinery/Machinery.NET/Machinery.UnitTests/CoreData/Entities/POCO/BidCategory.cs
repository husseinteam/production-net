﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinery.CoreData.EntityEngine.Annotations;
using Machinery.CoreData.EntityEngine.Base;

namespace Machinery.UnitTests.CoreData.Entities.POCO {
    [EntityTitle("BidCategories", Schema = "Auction")]
    public class BidCategory : BaseEntity<BidCategory>{


        [ReferenceKey("BidID"), Unique]
        public Bid Bid { get; set; }


        [ReferenceKey("CategoryID"), Unique]
        public Category Category { get; set; }


    }
}
