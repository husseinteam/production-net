﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinery.CoreData.EntityEngine.Annotations;
using Machinery.CoreData.EntityEngine.Base;

namespace Machinery.UnitTests.CoreData.Entities.POCO {

	[EntityTitle("Manifests", Schema = "Translation")]
	public class Manifest : BaseEntity<Manifest> {

		public Manifest() {
			Statements = new List<Statement>();
		}

		[DataColumn("varchar", MaxLength = 128)]
		public String Code { get; set; }

		[ReferenceCollection]
		public ICollection<Statement> Statements { get; set; }

	}
}
