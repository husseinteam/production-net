﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinery.CoreData.EntityEngine.Annotations;
using Machinery.CoreData.EntityEngine.Base;

namespace Machinery.UnitTests.CoreData.Entities.POCO {

    [EntityTitle("Tag", Schema = "Mainframe")]
    public class Tag : BaseEntity<Tag> {

        [DataColumn("nvarchar", MaxLength = 64)]
        public String Title { get; set; }

        [ReferenceKey("RelatedQuestionID")]
        public Question RelatedQuestion { get; set; }

    }


}
