﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinery.CoreData.EntityEngine.Annotations;
using Machinery.CoreData.EntityEngine.Base;

namespace Machinery.UnitTests.CoreData.Entities.POCO {

    [EntityTitle("Carousels", Schema = "Mainframe")]
    public class Carousel : BaseEntity<Carousel> {

        [DataColumn("bit")]
        public Boolean Blank { get; set; }
        [DataColumn("nvarchar", MaxLength = 128)]
        public String IntroductionLine1 { get; set; }
        [DataColumn("nvarchar", MaxLength = 128)]
        public String IntroductionLine2 { get; set; }
        [DataColumn("nvarchar", MaxLength = 128)]
        public String IntroductionLine3 { get; set; }
        [DataColumn("nvarchar", MaxLength = 128)]
        public String IntroductionLine4 { get; set; }
        [DataColumn("nvarchar", MaxLength = 128)]
        public String IntroductionSubText { get; set; }
        [DataColumn("nvarchar", MaxLength = 255), Nullable]
        public String ImageLink { get; set; }


    }
}
