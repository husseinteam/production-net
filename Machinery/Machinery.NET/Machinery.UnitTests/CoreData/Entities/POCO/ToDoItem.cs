﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinery.CoreData.EntityEngine.Annotations;
using Machinery.CoreData.EntityEngine.Base;
using Machinery.UnitTests.CoreData.Entities.Enums;

namespace Machinery.UnitTests.CoreData.Entities.POCO {

    [EntityTitle("ToDoItems", Schema = "Auction")]
    public class ToDoItem : BaseEntity<ToDoItem> {

        [DataColumn("nvarchar", MaxLength = 128)]
        public String Title { get; set; }
        [Enumeration]
        public EToDoItemEmergency ToDoEmergency { get; set; }

        [ReferenceKey("AssignedTaskID")]
        public TaskItem TaskItem { get; set; }


    }

}
