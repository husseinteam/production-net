﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinery.CoreData.EntityEngine.Annotations;
using Machinery.CoreData.EntityEngine.Base;
using Machinery.UnitTests.CoreData.Entities.Enums;

namespace Machinery.UnitTests.CoreData.Entities.POCO {

	[EntityTitle("Tasks", Schema = "Auction")]
	public class TaskItem : BaseEntity<TaskItem> {

		public TaskItem() {
			ToDoItems = new List<ToDoItem>();
		}

		[DataColumn("nvarchar", MaxLength = 128)]
		public String Title { get; set; }

		[DataColumn("smallmoney")]
		public Single Percent { get; set; }

		[Enumeration]
		public ETaskColor TaskColor { get; set; }

		[DataColumn("datetime")]
		public DateTime PublishedOn { get; set; }

		[Enumeration]
		public ETaskStatus Status { get; set; }

		[ReferenceKey("ApplicantID"), Unique]
		public Actor Applicant { get; set; }

		[ReferenceKey("BidID"), Unique]
		public Bid Bid { get; set; }

		[ReferenceCollection]
		public ICollection<ToDoItem> ToDoItems { get; set; }

	}
}
