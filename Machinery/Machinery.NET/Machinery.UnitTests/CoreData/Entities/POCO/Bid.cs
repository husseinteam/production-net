﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinery.CoreData.EntityEngine.Annotations;
using Machinery.CoreData.EntityEngine.Base;
using Machinery.UnitTests.CoreData.Entities.Enums;

namespace Machinery.UnitTests.CoreData.Entities.POCO {

    [EntityTitle("Bids", Schema = "Auction")]
    public class Bid : BaseEntity<Bid> {

        #region Properties

        [DataColumn("datetime")]
        public DateTime Deadline { get; set; }

        [DataColumn("datetime")]
        public DateTime BiddedOn { get; set; }

        [DataColumn("money")]
        public Decimal BiddedPrice { get; set; }

        [DataColumn("nvarchar", MaxLength = -1)]
        public String BidDescription { get; set; }

        [Enumeration]
        public EBidStatus Status { get; set; }

        #endregion

        [ReferenceKey("ActorID")]
        public Actor Actor { get; set; }

    }
}
