﻿using System.Collections.Generic;
using Machinery.CoreData.EntityEngine.Annotations;
using Machinery.CoreData.EntityEngine.Base;

namespace Machinery.UnitTests.CoreData.Entities.POCO {

    [EntityTitle("Categories", Schema = "Auction")]
    public class Category : BaseEntity<Category>{

        [DataColumn("nvarchar", MaxLength = 64), Unique]
        public string Code { get; set; }

		[ReferenceKey("DescriptionManifestID")]
		public Manifest DescriptionManifest { get; set; }


    }
}