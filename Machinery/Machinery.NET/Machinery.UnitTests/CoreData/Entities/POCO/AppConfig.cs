﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinery.CoreData.EntityEngine.Annotations;
using Machinery.CoreData.EntityEngine.Base;

namespace Machinery.UnitTests.CoreData.Entities.POCO {

    [EntityTitle("AppConfig", Schema = "Configuration")]
    public class AppConfig : BaseEntity<AppConfig>{

        [DataColumn("nvarchar", MaxLength = 128)]
        public String Attribute { get; set; }
        [DataColumn("nvarchar", MaxLength = 256)]
        public String Value { get; set; }

    }
}
