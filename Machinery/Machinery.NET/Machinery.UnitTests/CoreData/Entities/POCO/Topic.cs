﻿using System.Collections.Generic;
using Machinery.CoreData.EntityEngine.Annotations;
using Machinery.CoreData.EntityEngine.Base;

namespace Machinery.UnitTests.CoreData.Entities.POCO {

    [EntityTitle("Topics", Schema = "Mainframe")]
    public class Topic : BaseEntity<Topic> {

        #region Ctor

        public Topic() {

            this.Questions = new List<Question>();

        }

        #endregion

        [DataColumn("nvarchar", MaxLength = 64), Unique]
        public string Code { get; set; }

        [DataColumn("nvarchar", MaxLength = 64)]
        public string Description { get; set; }

        [ReferenceKey("ParentTopicID"), Nullable]
        public Topic ParentTopic { get; set; }

        [ReferenceCollection]
        public ICollection<Question> Questions { get; set; }

    }
}