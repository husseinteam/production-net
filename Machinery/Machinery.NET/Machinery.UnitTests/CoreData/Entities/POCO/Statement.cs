﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinery.CoreData.EntityEngine.Annotations;
using Machinery.CoreData.EntityEngine.Base;

namespace Machinery.UnitTests.CoreData.Entities.POCO {

	[EntityTitle("Statements", Schema = "Translation")]
	public class Statement : BaseEntity<Statement> {

		[DataColumn("nvarchar", MaxLength = 4000)]
		public String Text { get; set; }

		[ReferenceKey("LanguageID")]
		public Language Language { get; set; }

		[ReferenceKey("ManifestID")]
		public Manifest Manifest { get; set; }

    }
}
