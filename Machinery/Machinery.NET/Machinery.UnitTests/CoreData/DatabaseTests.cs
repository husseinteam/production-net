﻿using System;
using Machinery.CoreData;
using Machinery.CoreData.DbEngine;
using Machinery.Instruments.Constructs.Credentials;
using Machinery.Instruments.InstanceProvider;
using Machinery.UnitTests.CoreData.Entities.POCO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Machinery.Extensions.Core;

namespace Machinery.UnitTests.CoreData {
	[TestClass]
	public class DatabaseTests {

		public static Exception RegenerateDatabase(Boolean regenerateDb = true) {

			var expt = SInstanceProvider.DbGeneratorInstance
			   .EngineFor<Comment>()
			   .EngineFor<Carousel>()
			   .EngineFor<Profile>()
			   .EngineFor<Author>()
			   .EngineFor<Rank>()
			   .EngineFor<Actor>()
			   .EngineFor<Topic>()
			   .EngineFor<Question>()
			   .EngineFor<Answer>()
			   .EngineFor<Tag>()
			   .EngineFor<Category>()
			   .EngineFor<Bid>()
			   .EngineFor<MessageItem>()
			   .EngineFor<Notification>()
			   .EngineFor<TaskItem>()
			   .EngineFor<AppConfig>()
			   .EngineFor<BidCategory>()
			   .EngineFor<ToDoItem>()
			   .EngineFor<Language>()
			   .EngineFor<Statement>()
			   .EngineFor<Manifest>()
			   .GenerateModels();

			return expt;

		}

		[ClassInitialize]
		public static void ClassInit(TestContext ctx) {

			SCredentialProvider.CurrentContainer["local"] =
				SManifester.ToConnectionCredential(EServiceType.MSSQL);
			SCredentialProvider.CurrentContainer["local"]
				.Assign(cr => cr.Server = ".")
				.Assign(cr => cr.Database = "bbdb")
				.Assign(cr => cr.SuperAdminUserID = "bbadmin")
				.Assign(cr => cr.SuperAdminPassword = "1q2w3e4r5t")
				.Assign(cr => cr.UserID = "bbadmin")
				.Assign(cr => cr.Password = "1q2w3e4r5t");
			SCredentialProvider.CurrentContainer["azure"] = SManifester.ToConnectionCredential(EServiceType.AzureSql);
			SCredentialProvider.CurrentContainer["azure"]
				.Assign(cr => cr.Server = "tcp:qse2ln2a5i.database.windows.net")
				.Assign(cr => cr.Port = 1433)
				.Assign(cr => cr.Database = "bbdb")
				.Assign(cr => cr.SuperAdminUserID = "lampiclobe@qse2ln2a5i")
				.Assign(cr => cr.SuperAdminPassword = "Lmpclb!293117")
				.Assign(cr => cr.UserID = "bbadmin")
				.Assign(cr => cr.Password = "1q2w3e4r5t");
			SCredentialProvider.CurrentContainerKey = "azure";

		}

		[TestMethod]
		public void RegenerateDatabaseRunsOK() {

			var expt = RegenerateDatabase();
			var msg = expt != null ? expt.Message : "process succeeded";
			Assert.IsNull(expt, msg);

		}
	}
}
