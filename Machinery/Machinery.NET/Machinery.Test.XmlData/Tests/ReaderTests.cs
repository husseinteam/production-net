﻿using System;
using System.Linq;
using System.IO;
using Machinery.Test.XmlData.Code;
using Machinery.XmlData.Tools;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Machinery.UnitTests.XmlData.Tests {

	[TestClass]
	public class ReaderTests {

		[TestMethod]
		public void ExtractXmlRunsOK() {

			var xmlPath = Path.Combine(Environment.CurrentDirectory, @"Resources\demo.xml");
			var products = SXmlReader.ExtractXml<Product>(xmlPath);

			Assert.AreNotEqual(0, products.Count);
			Assert.AreEqual(3, products.Count);
			Assert.AreEqual("Generic Mouse", products.First().ProductName);

		}

	}

}
