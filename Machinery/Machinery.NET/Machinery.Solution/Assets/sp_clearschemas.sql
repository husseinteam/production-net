
DECLARE @proc nvarchar(MAX)
DECLARE @altcre nvarchar(12)

IF EXISTS (select * 
	from bbazureANVsvDWBs.information_schema.routines 
	where routine_type = 'PROCEDURE' AND SPECIFIC_NAME='sp_clearschemas')
	SET @altcre='ALTER'
ELSE
	SET @altcre='CREATE'
	
SET @proc=@altcre + N' USE [bbdb]
GO

/****** Object:  StoredProcedure [dbo].[spClearAllSchemas]    Script Date: 2.6.2015 23:34:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[spClearAllSchemas] 
				AS
				BEGIN
					DECLARE @schema nvarchar(256)
					DECLARE @sql nvarchar(256)
					DECLARE @Exceptions table(exceptsql nvarchar(256))

					DECLARE schemas CURSOR
						FOR SELECT name FROM sys.schemas
							WHERE principal_id=(SELECT schema_id from sys.schemas WHERE name='dbo') AND name!='dbo'
					OPEN schemas
					FETCH NEXT FROM schemas 
					INTO @schema

					WHILE @@FETCH_STATUS = 0
					BEGIN
						SET @sql=''DROP SCHEMA ['' + @schema + '']''
						BEGIN TRY 
							EXEC sys.sp_executesql @sql
						END TRY 
						BEGIN CATCH 
							INSERT INTO @Exceptions (exceptsql) values (@sql)
						END CATCH 
						FETCH NEXT FROM schemas 
						INTO @schema
					END

					CLOSE schemas;
					DEALLOCATE schemas;

					WHILE EXISTS (SELECT COUNT(*) FROM @Exceptions)
					BEGIN
						DECLARE excptschemas CURSOR
							FOR SELECT exceptsql FROM @Exceptions
						OPEN excptschemas
						FETCH NEXT FROM excptschemas 
							INTO @schema

						WHILE @@FETCH_STATUS = 0
						BEGIN
							SET @sql=''DROP SCHEMA ['' + @schema + '']''
							BEGIN TRY 
								EXEC sys.sp_executesql @sql
								DELETE FROM @Exceptions WHERE exceptsql=@sql
							END TRY 
							BEGIN CATCH 
								INSERT INTO @Exceptions (exceptsql) values (@sql)
							END CATCH 
							FETCH NEXT FROM excptschemas 
								INTO @schema
						END
					END
					CLOSE excptschemas;
					DEALLOCATE excptschemas;
				END
'
EXEC sys.sp_executesql @proc