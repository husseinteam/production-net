﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TT.Data.Seed.Catalog;

namespace TT.Data.Seed.Tools {

    public static class SSeedTools {

        public static IEnumerable<Exception> SeedDatabase() {

            IList<Exception> expList = new List<Exception>();

            var langTR = LanguageSeed.GetResponse().DestroySeed().PersistSeed(ref expList).SeedList.SingleOrDefault(ln => ln.LanguageCode == "tr");
            var greetingManifests = ManifestSeed.GetGreetingResponse(langTR).DestroySeed().PersistSeed(ref expList).SeedList;
            var complimentManifests = ManifestSeed.GetComplimentResponse(langTR).PersistSeed(ref expList).SeedList;
            var proposalManifests = ManifestSeed.GetProposalResponse(langTR).PersistSeed(ref expList).SeedList;

            var milestones = MilestoneSeed.GetResponse().DestroySeed().PersistSeed(ref expList).SeedList;
            var events = EventSeed.GetResponse(milestones).DestroySeed().PersistSeed(ref expList).SeedList;

            var actors = ActorSeed.GetResponse().DestroySeed().PersistSeed(ref expList).SeedList;
            var organizations = OrganizationSeed.GetResponse(actors, events).DestroySeed().PersistSeed(ref expList).SeedList;

            GreetingSeed.GetResponse(actors, events, organizations, greetingManifests).DestroySeed().PersistSeed(ref expList);
            ComplimentSeed.GetResponse(actors, events, organizations, complimentManifests).DestroySeed().PersistSeed(ref expList);
            ProposalSeed.GetResponse(actors, events, organizations, proposalManifests).DestroySeed().PersistSeed(ref expList);

            return expList.ToArray();

        }

    }


}
