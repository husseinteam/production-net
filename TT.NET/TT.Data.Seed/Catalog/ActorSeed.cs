﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Abstract.Data.Engine;
using LLF.Extensions;
using LLF.Data.Tools;
using TT.Data.Entities;
using TT.Data.Enums;

namespace TT.Data.Seed.Catalog {
	public static class ActorSeed {

		public static ISeedResponse<Actor> GetResponse() {

			var emails = new String[] { 
				"lampiclobe@outlook.com", 
				"ondermetu@gmail.com", 
				"dilek1982@outlook.com",
				"section@gmail.com",
				"hithere@outlook.com" 
			};
			var identities = new String[] {
                "Özgür Eser Sönmez",
                "Önder Heper",
                "Dilek Neidek",
                "Naber Babalık",
				"Özgür Dönmez Sönmez"
			};

			ISeedResponse<Actor> response = SeedProvider.SeedInstanceOf<Actor>(emails.Count(),
				(i) => new Actor() {
                    Email = emails.ElementAt(i),
                    PasswordHash = "a1q2w3e".ComputeHash(),
                    Identity = identities.ElementAt(i),
                    IsAdministrator = i == 0,
                    MembershipStatus = EMembershipStatus.Active
				});
			return response;

		}

	}
}
