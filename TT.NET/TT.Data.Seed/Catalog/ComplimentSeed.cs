﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Abstract.Data.Engine;
using LLF.Extensions;
using LLF.Data.Tools;
using TT.Data.Entities;
using TT.Data.Enums;

namespace TT.Data.Seed.Catalog {

	public static class ComplimentSeed {

        public static ISeedResponse<Compliment> GetResponse(IList<Actor> actors, IList<Event> events, IList<Organization> organizations, IList<Manifest> manifests) {

            var types = new ERelationType[] {
                ERelationType.Family,
                ERelationType.Inlove
            };
            ISeedResponse<Compliment> response =
                SeedProvider.SeedInstanceOf<Compliment>(types.Count(),
                        (i) => new Compliment() {
                            Actor = actors.ElementAt(i),
                            Partner = actors.Reverse().ElementAt(i),
                            RelationType = types.ElementAt(i),
                            Manifest = manifests.ElementAt(i)
                        });

            return response;

        }

    }
}
