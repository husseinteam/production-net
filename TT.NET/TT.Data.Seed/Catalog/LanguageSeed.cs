﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Abstract.Data.Engine;
using LLF.Extensions;
using LLF.Data.Tools;
using TT.Data.Entities;

namespace TT.Data.Seed.Catalog {
	public static class LanguageSeed {

		public static ISeedResponse<Language> GetResponse() {

			var langCodes = new String[] { 
				"tr", 
				"en",
				"en"
			};
			var regionCodes = new String[] {
                "TR",
                "US",
                "GB"
			};

			var response = SeedProvider.SeedInstanceOf<Language>(langCodes.Count(),
				(i) => new Language() {
					LanguageCode = langCodes.ElementAt(i),
					RegionCode = regionCodes.ElementAt(i)
				});
			return response;

		}

	}
}
