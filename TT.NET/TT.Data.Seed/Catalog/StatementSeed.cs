﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Abstract.Data.Engine;
using LLF.Extensions;
using LLF.Data.Tools;
using TT.Data.Entities;
using TT.Data.Enums;

namespace TT.Data.Seed.Catalog {

	public static class StatementSeed {

		public static ISeedResponse<Statement> GetResponse(Manifest manifest, IList<Language> languages) {

			ISeedResponse<Statement> response = 
                SeedProvider.SeedInstanceOf<Statement>(languages.Count(),
                        (i) => new Statement() {
                            Manifest = manifest,
                            Language = languages.ElementAt(i)
                        });

            return response;

		}

	}
}
