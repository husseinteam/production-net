﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Abstract.Data.Engine;
using LLF.Extensions;
using LLF.Data.Tools;
using TT.Data.Entities;
using TT.Data.Enums;

namespace TT.Data.Seed.Catalog {

	public static class OrganizationSeed {

		public static ISeedResponse<Organization> GetResponse(IList<Actor> actors, IList<Event> events) {

            var titles = new String[] {
               "Organization 1",
               "Organization 2",
               "Organization 3",
               "Organization 4",
               "Organization 5"
            };

            ISeedResponse<Organization> response = 
                SeedProvider.SeedInstanceOf<Organization>(events.Count(),
                        (i) => new Organization() {
                            Event = events.ElementAt(i),
                            Participant = actors.ElementAt(i),
                            Title = titles.Cycle(i)
                        });

            return response;

		}

	}
}
