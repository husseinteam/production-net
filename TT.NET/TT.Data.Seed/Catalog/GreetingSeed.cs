﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Abstract.Data.Engine;
using LLF.Extensions;
using LLF.Data.Tools;
using TT.Data.Entities;
using TT.Data.Enums;

namespace TT.Data.Seed.Catalog {

	public static class GreetingSeed {

        public static ISeedResponse<Greeting> GetResponse(IList<Actor> actors, IList<Event> events, IList<Organization> organizations, IList<Manifest> manifests) {

            ISeedResponse<Greeting> response =
                SeedProvider.SeedInstanceOf<Greeting>(actors.Count(),
                        (i) => new Greeting() {
                            Greeter = actors.ElementAt(i),
                            Organization = organizations.ElementAt(i),
                            Manifest = manifests.ElementAt(i)
                        });

            return response;

        }

    }
}
