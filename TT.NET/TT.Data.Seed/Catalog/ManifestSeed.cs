﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Abstract.Data.Engine;
using LLF.Extensions;
using LLF.Data.Tools;
using TT.Data.Entities;

namespace TT.Data.Seed.Catalog {
	public static class ManifestSeed {

        public static ISeedResponse<Manifest> GetProposalResponse(Language trLang) {


            var statementsTRMarriage = new String[] {
                "Hayat arkadaşım, can yoldaşım olur musun",
                "Bir ömrü benimle paylaşır mısın",
                "Benimle evlenir misin"
            };

            var statementsTRPartnership = new String[] {
                "Arkadaşım olur musun",
                "Benimle dostluğa ne dersin",
                "Adamım dostum olur musun"
            };

            var statementsTRBusiness = new String[] {
                "Ekibimize katılır mısın",
                "Bizimle bir iş görüşmesi yapar mısın",
                "Bizimle çalışmak ister misin"
            };


            var statementsTR = new String[][] {
                statementsTRMarriage,
                statementsTRPartnership,
                statementsTRBusiness
            };

            var statementCodesTR = new String[] {
                "statementsTRMarriage",
                "statementsTRPartnership",
                "statementsTRBusiness"
            };

            var response = SeedProvider.SeedInstanceOf<Manifest>(statementsTR.Count(),
                (i) => new Manifest() {
                    Code = statementCodesTR.ElementAt(i)
                }.Assign(mf => mf.Statements.Extend(statementsTR.ElementAt(i).Select(st => new Statement() { Content = st, Language = trLang }).ToArray())));
            return response;

        }

        public static ISeedResponse<Manifest> GetComplimentResponse(Language trLang) {


            var statementsTRFamilyRelation = new String[] {
                "Hayatımın anlamı seni çok seviyorum",
                "Biriciğim iyi ki varsın",
                "Hayat arkadaşım, can yoldaşım seni çok seviyorum"
            };

            var statementsTRFamilyInlove = new String[] {
                "Canım sevgilim, seni çok seviyorum",
                "Allahıma şükürler olsun ki seni benimle karşılaştırdı",
                "Sana bir şey diyeceğim, daima seni seveceğim"
            };


            var statementsTR = new String[][] {
                statementsTRFamilyRelation,
                statementsTRFamilyInlove
            };

            var statementCodesTR = new String[] {
                "statementsTRFamilyRelation",
                "statementsTRFamilyInlove"
            };

            var response = SeedProvider.SeedInstanceOf<Manifest>(statementsTR.Count(),
				(i) => new Manifest() {
                    Code = statementCodesTR.ElementAt(i)
                }.Assign(mf => mf.Statements.Extend(statementsTR.ElementAt(i).Select(st => new Statement() { Content = st, Language = trLang } ).ToArray())));
			return response;

		}

        public static ISeedResponse<Manifest> GetGreetingResponse(Language trLang) {

            var statementsTRMarriage = new String[] {
                "Allah bir yastıkta kocatsın",
                "Ömür boyu mutluluklar",
                "Bir ömür beraber yaşlanasınız"
            };

            var statementsTRGiveBirth = new String[] {
                "Allah analı babalı büyütsün",
                "Ömrü bahtı güzel olsun",
                ""
            };

            var statementsTRPromotion = new String[] {
                "Hayırlı olsun",
                "Daha nice başarılara",
                "Başarılarınız devamlı olsun"
            };

            var statementsTRNewRelation = new String[] {
                "Allah tamamına erdirsin",
                "Allah ayırmasın",
                ""
            };

            var statementsTRMothersDay = new String[] {
                "Canım anneciğim iyi ki varsın",
                "Allah yokluğunuzu göstermesin",
                "Canım anneciğim seni çok seviyorum"
            };

            var statementsTRFathersDay = new String[] {
                "Canım babacığım Allah sizi başımızdan eksik etmesin",
                "Allah yokluğunuzu göstermesin",
                "Canım babacığım seni çok seviyorum"
            };

            var statementsTRRamadhan = new String[] {
                "Mübarek Ramazan Bayramınızı kutlar, hayırlara vesile olmasını Cenab-ı Allah'dan niyaz ederim.",
                "Hayırlı Bayramlar",
                "Sevdiklerinizle birlikte nice mutlu ce huzurlu bayramlar geçirmenizi temenni ederim"
            };

            var statementsTREid = new String[] {
                "Mübarek Kurban Bayramınızı kutlar, hayırlara vesile olmasını Cenab-ı Allah'dan niyaz ederim.",
                "Kurban Bayramınız Mübarek olsun",
                "Sevdiklerinizle birlikte nice mutlu ce huzurlu bayramlar geçirmenizi temenni ederim"
            };

            var statementsTRHicjrNewYear = new String[] {
                "Hicri Yeni Yılınız mübarek olsun",
                "Hicri Yeni Yılınızı sevdiklerinizle birlikte huzurlu ve mutlu geçirmenizi temenni ederim",
                "İslam aleminin hicri yeni yılının hayırlara vesile olmasını Cenab-ı Allah'dan niyaz ederim"
            };

            var statementsTRTeachersDay = new String[] {
                "Canım öğretmenim seni çok seviyorum",
                "Canım hocam öğretmenler gününüz kutlu olsun",
                "Canım hocam ellerinizden öper Öğretmenler gününüzü tebrik ederim"
            };

            var statementsTR = new String[][] {
                statementsTREid,
                statementsTRFathersDay,
                statementsTRGiveBirth,
                statementsTRHicjrNewYear,
                statementsTRMarriage,
                statementsTRMothersDay,
                statementsTRNewRelation,
                statementsTRPromotion,
                statementsTRRamadhan,
                statementsTRTeachersDay
            };

            var statementCodesTR = new String[] {
                "statementsTREid",
                "statementsTRFathersDay",
                "statementsTRGiveBirth",
                "statementsTRHicjrNewYear",
                "statementsTRMarriage",
                "statementsTRMothersDay",
                "statementsTRNewRelation",
                "statementsTRPromotion",
                "statementsTRRamadhan",
                "statementsTRTeachersDay"
            };

            var response = SeedProvider.SeedInstanceOf<Manifest>(statementsTR.Count(),
                (i) => new Manifest() {
                    Code = statementCodesTR.ElementAt(i)
                }.Assign(mf => mf.Statements.Extend(statementsTR.ElementAt(i).Select(st => new Statement() { Content = st, Language = trLang }).ToArray())));
            return response;

        }

    }
}
