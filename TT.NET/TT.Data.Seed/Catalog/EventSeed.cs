﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Abstract.Data.Engine;
using LLF.Extensions;
using LLF.Data.Tools;
using TT.Data.Entities;
using TT.Data.Enums;

namespace TT.Data.Seed.Catalog {

	public static class EventSeed {

		public static ISeedResponse<Event> GetResponse(IList<Milestone> milestones) {

            var happened = new DateTime[] {
               2.DaysAgo(),
               3.HoursAgo(),
               12.HoursAgo(),
               4.MonthsAgo(),
               1.MonthsAgo()
            };

            ISeedResponse<Event> response = 
                SeedProvider.SeedInstanceOf<Event>(happened.Count(),
                        (i) => new Event() {
                            Milestone = milestones.ElementAt(i),
                            HappenedOn = happened.Cycle(i)
                        });

            return response;

		}

	}
}
