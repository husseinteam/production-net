﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Abstract.Data.Engine;
using LLF.Extensions;
using LLF.Data.Tools;
using TT.Data.Entities;
using TT.Data.Enums;

namespace TT.Data.Seed.Catalog {
    public static class MilestoneSeed {

        public static ISeedResponse<Milestone> GetResponse() {

            var milestoneTypes = new EMilestoneType[] {
                EMilestoneType.Marriage,
                EMilestoneType.GiveBirth,
                EMilestoneType.Promotion,
                EMilestoneType.NewRelation,
                EMilestoneType.MothersDay,
                EMilestoneType.FathersDay,
                EMilestoneType.Ramadhan,
                EMilestoneType.Eid,
                EMilestoneType.HicjrNewYear,
                EMilestoneType.TeachersDay,
                EMilestoneType.MarriageAnniversary
            };

            ISeedResponse<Milestone> response =
                SeedProvider.SeedInstanceOf<Milestone>(milestoneTypes.Count(),
                        (i) => new Milestone() {
                            MilestoneType = milestoneTypes.ElementAt(i)
                        });

            return response;

        }

    }
}
