﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Abstract.Data.Engine;
using LLF.Extensions;
using LLF.Data.Tools;
using TT.Data.Entities;
using TT.Data.Enums;

namespace TT.Data.Seed.Catalog {

	public static class ProposalSeed {

        public static ISeedResponse<Proposal> GetResponse(IList<Actor> actors, IList<Event> events, IList<Organization> organizations, IList<Manifest> manifests) {

            var types = new EProposalType[] {
                EProposalType.MarriageProposal,
                EProposalType.PartnershipProposal,
                EProposalType.BusinessProposal
            };

            ISeedResponse<Proposal> response =
                SeedProvider.SeedInstanceOf<Proposal>(types.Count(),
                        (i) => new Proposal() {
                            Actor = actors.ElementAt(i),
                            Counterpart = actors.Reverse().ElementAt(i),
                            ProposalType = types.ElementAt(i),
                            Manifest = manifests.ElementAt(i)
                        });

            return response;

        }

    }
}
