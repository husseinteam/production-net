﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TT.Data.Enums {

    public enum EMilestoneType {

        Marriage = 1,
        GiveBirth,
        Promotion,
        NewRelation,
        MothersDay,
        FathersDay,
        Ramadhan,
        Eid,
        HicjrNewYear,
        TeachersDay,
        MarriageAnniversary

    }

}
