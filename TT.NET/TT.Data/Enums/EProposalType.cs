﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TT.Data.Enums {

    public enum EProposalType {

        MarriageProposal,
        PartnershipProposal,
        BusinessProposal

    }

}
