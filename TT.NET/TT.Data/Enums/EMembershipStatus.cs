﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TT.Data.Enums {

    public enum EMembershipStatus {

        [Description("success")]
        Active = 1,
        [Description("info")]
        Frozen,
        [Description("danger")]
        Deleted,
        [Description("warning")]
        PendingActivation,
        [Description("info")]
        Any

    }

}
