﻿using LLF.Data.Engine;
using LLF.Annotations.Data;
using TT.Data.Enums;

namespace TT.Data.Entities {

    [EntityTitle("Compliments", Schema = "TT")]
    public class Compliment : BaseEntity<Compliment> {

        #region Ctor
        public Compliment() {

        }
        #endregion

        #region Properties

        [Enumeration]
        public ERelationType RelationType { get; set; }

        #endregion

        #region Navigation Properties

        [ReferenceKey("ActorID"), Unique]
        public Actor Actor { get; set; }

        [ReferenceKey("PartnerID"), Unique]
        public Actor Partner { get; set; }

        [ReferenceKey("ManifestID")]
        public Manifest Manifest { get; set; }

        #endregion

    }
}
