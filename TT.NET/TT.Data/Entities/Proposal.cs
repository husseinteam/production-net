﻿using LLF.Data.Engine;
using LLF.Annotations.Data;
using TT.Data.Enums;

namespace TT.Data.Entities {

    [EntityTitle("Proposals", Schema = "TT")]
    public class Proposal : BaseEntity<Proposal> {

        #region Ctor
        public Proposal() {

        }
        #endregion

        #region Properties

        public EProposalType ProposalType { get; set; }

        #endregion

        #region Navigation Properties

        [ReferenceKey("ActorID"), Unique]
        public Actor Actor { get; set; }

        [ReferenceKey("CounterpartID"), Unique]
        public Actor Counterpart { get; set; }

        [ReferenceKey("ManifestID")]
        public Manifest Manifest { get; set; }

        #endregion

    }
}
