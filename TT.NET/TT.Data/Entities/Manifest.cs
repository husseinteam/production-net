﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Annotations.Data;
using LLF.Data.Engine;
using LLF.Extensions.Data.Enums;

namespace TT.Data.Entities {

	[EntityTitle("Manifests", Schema = "TT")]
	public class Manifest : BaseEntity<Manifest> {

		public Manifest() {
			Statements = new List<Statement>();
		}

		[DataColumn(EDbType.NVarChar, MaxLength = 128)]
		public String Code { get; set; }

		[ReferenceCollection]
		public ICollection<Statement> Statements { get; set; }

	}
}
