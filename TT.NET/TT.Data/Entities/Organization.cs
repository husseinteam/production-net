﻿using LLF.Data.Engine;
using LLF.Annotations.Data;
using System.Collections.Generic;
using System;
using LLF.Extensions.Data.Enums;

namespace TT.Data.Entities {

    [EntityTitle("Organizations", Schema = "TT")]
    public class Organization : BaseEntity<Organization> {

        #region Ctor
        public Organization() {

        }
        #endregion

        #region Properties

        [DataColumn(EDbType.NVarChar, MaxLength=128)]
        public String Title { get; set; }

        #endregion

        #region Navigation Properties

        [ReferenceKey("ParticipantID"), Unique]
        public Actor Participant { get; set; }

        [ReferenceKey("EventID"), Unique]
        public Event Event { get; set; }

        #endregion

    }
}
