﻿using LLF.Data.Engine;
using LLF.Annotations.Data;
using TT.Data.Enums;

namespace TT.Data.Entities {

    [EntityTitle("Greetings", Schema = "TT")]
    public class Greeting : BaseEntity<Greeting> {

        #region Ctor
        public Greeting() {

        }
        #endregion

        #region Properties

        #endregion

        #region Navigation Properties

        [ReferenceKey("GreeterID"), Unique]
        public Actor Greeter { get; set; }

        [ReferenceKey("OrganizationID"), Unique]
        public Organization Organization { get; set; }

        [ReferenceKey("ManifestID")]
        public Manifest Manifest { get; set; }

        #endregion

    }
}
