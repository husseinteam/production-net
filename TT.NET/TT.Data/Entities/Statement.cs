﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLF.Annotations.Data;
using LLF.Data.Engine;
using LLF.Extensions.Data.Enums;

namespace TT.Data.Entities {

    [EntityTitle("Statements", Schema = "TT")]
    public class Statement : BaseEntity<Statement> {

        #region Properties

        [DataColumn(EDbType.NVarChar, MaxLength = 4000)]
        public String Content { get; set; }

        [ReferenceKey("LanguageID"), Unique]
        public Language Language { get; set; }

        [ReferenceKey("ManifestID"), Unique]
        public Manifest Manifest { get; set; }

        #endregion

        #region Navigation Properties

        #endregion

    }
}
