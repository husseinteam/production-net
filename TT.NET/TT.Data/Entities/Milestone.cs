﻿using LLF.Data.Engine;
using LLF.Annotations.Data;
using TT.Data.Enums;

namespace TT.Data.Entities {

    [EntityTitle("Milestones", Schema = "TT")]
    public class Milestone : BaseEntity<Milestone> {

        #region Ctor
        public Milestone() {

        }
        #endregion

        #region Properties

        [Enumeration]
        public EMilestoneType MilestoneType { get; set; }

        #endregion

        #region Navigation Properties

        #endregion

    }
}
