﻿using System;
using LLF.Data.Engine;
using LLF.Annotations.Data;
using LLF.Extensions.Data.Enums;
using TT.Data.Enums;
using System.Collections.Generic;

namespace TT.Data.Entities {

    [EntityTitle("Actors", Schema = "TT")]
    public class Actor : BaseEntity<Actor> {

        #region Ctor
        public Actor() {

        }
        #endregion

        #region Properties

        [DataColumn(EDbType.NVarChar, MaxLength = 128)]
        public String Identity { get; set; }

        [DataColumn(EDbType.NVarChar, MaxLength = 128), Unique]
        public String Email { get; set; }

        [DataColumn(EDbType.NVarChar, MaxLength = 512)]
        public String PasswordHash { get; set; }

        [DataColumn(EDbType.Boolean)]
        public Boolean IsAdministrator { get; set; }

        [DataColumn(EDbType.NVarChar, MaxLength = 128), Nullable]
        public String PasswordRecoveryToken { get; set; }

        [Enumeration]
        public EMembershipStatus MembershipStatus { get; set; }

        #endregion

        #region Navigation Properties

        #endregion

    }
}
