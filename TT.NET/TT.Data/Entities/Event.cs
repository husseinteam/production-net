﻿using LLF.Data.Engine;
using LLF.Annotations.Data;
using System.Collections.Generic;
using System;
using LLF.Extensions.Data.Enums;

namespace TT.Data.Entities {

    [EntityTitle("Events", Schema = "TT")]
    public class Event : BaseEntity<Event> {

        #region Ctor
        public Event() {
        }
        #endregion

        #region Properties

        [DataColumn(EDbType.DateTime)]
        public DateTime HappenedOn { get; set; }

        #endregion

        #region Navigation Properties

        [ReferenceKey("MilestoneID")]
        public Milestone Milestone { get; set; }

        #endregion

    }
}
