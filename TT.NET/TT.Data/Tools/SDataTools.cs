﻿using LLF.Abstract.Data.Engine;
using LLF.Data.Containers;
using LLF.Data.Tools;
using LLF.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TT.Data.Entities;

namespace TT.Data.Tools {
    public static class SDataTools {

        public static Exception ReinitializeModels(Boolean wipe = true) {

            if (wipe) {
                DbProvider.WipeDatabase(); 
            }

            var expt = DependencyContainer.Take<IDatabaseEngine>()
                   .EngineFor<Milestone>()
                   .EngineFor<Language>()
                   .EngineFor<Manifest>()
                   .EngineFor<Statement>()
                   .EngineFor<Actor>()
                   .EngineFor<Event>()
                   .EngineFor<Organization>()
                   .EngineFor<Greeting>()
                   .EngineFor<Compliment>()
                   .EngineFor<Proposal>()
                   .GenerateModels();
            return expt;

        }

    }
}
