﻿using LLF.Abstract.Data.Engine;
using LLF.Data.Containers;
using LLF.Data.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TT.Resources.Globals {

    public static class Bootstrapper {

        public static void RegisterDependencies() {

            if (!_Booted) {

                SCredentialProvider.Current();
                _Booted = true;
            }
        }

        private static Boolean _Booted = false;

    }
}