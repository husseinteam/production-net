﻿using LLF.Abstract.Data.Engine;
using LLF.Data.Containers;
using LLF.Data.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TT.Resources.Locals;

namespace TT.Resources.Globals {

    public static class SCredentialProvider {

        public static IConnectionCredential Current() {

            if (CredentialProvider.CurrentCredential == null) {
                ResourceContainer.Register<IConnectionCredential>(XmlResources.ConnectionCredentials);
                CredentialProvider.CurrentCredential = ResourceContainer.ReadXmlFor<IConnectionCredential>(SDefinitions.CurrentCredentialKey);
            }
            return CredentialProvider.CurrentCredential;

        }

    }

}
