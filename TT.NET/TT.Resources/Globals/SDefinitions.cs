﻿using LLF.Abstract.Xml;
using LLF.Data.Containers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TT.Resources.Globals {
    public static class SDefinitions {

        public static class Regex {

            public static readonly String Email = ResourceContainer.ReadXmlFor<IDefinitionItem>("Email").Value;
            public static readonly String Identity = ResourceContainer.ReadXmlFor<IDefinitionItem>("Identity").Value;

        }

#if DEBUG
        public static readonly String CurrentCredentialKey = "local-mssql";
#else
        public static readonly String CurrentCredentialKey = "turhost-mysql";
#endif

    }
}
