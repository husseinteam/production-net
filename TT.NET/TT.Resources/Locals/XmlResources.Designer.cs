﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TT.Resources.Locals {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class XmlResources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal XmlResources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("TT.Resources.Locals.XmlResources", typeof(XmlResources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;?xml version=&quot;1.0&quot; encoding=&quot;utf-8&quot; ?&gt;
        ///&lt;ConnectionCredentials&gt;
        ///  &lt;ConnectionCredential&gt;
        ///    &lt;Key&gt;local-mysql&lt;/Key&gt;
        ///    &lt;Server&gt;localhost&lt;/Server&gt;
        ///    &lt;Database&gt;ggdb&lt;/Database&gt;
        ///    &lt;SuperAdminUserID&gt;ggadmin&lt;/SuperAdminUserID&gt;
        ///    &lt;SuperAdminPassword&gt;1q2w3e4r5t&lt;/SuperAdminPassword&gt;
        ///    &lt;UserID&gt;ggadmin&lt;/UserID&gt;
        ///    &lt;Password&gt;1q2w3e4r5t&lt;/Password&gt;
        ///    &lt;Port&gt;3306&lt;/Port&gt;
        ///    &lt;AttachDbFilename&gt;&lt;/AttachDbFilename&gt;
        ///    &lt;ServerCompatibilityLevel&gt;&lt;/ServerCompatibilityLevel&gt;
        ///    &lt;ServiceType&gt;MySQL&lt;/Servic [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string ConnectionCredentials {
            get {
                return ResourceManager.GetString("ConnectionCredentials", resourceCulture);
            }
        }
    }
}
