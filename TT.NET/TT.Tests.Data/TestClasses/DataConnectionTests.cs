﻿using System;
using TT.Resources.Globals;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LLF.Extensions;
using FluentAssertions;

namespace TT.Tests.Data.TestClasses {

    [TestClass]
    public class DataConnectionTests {

        [TestMethod]
        public void CurrentConnectionIsSuccessfull() {

            Bootstrapper.RegisterDependencies();
            var cred = SCredentialProvider.Current();
            var c = cred.ProvideSuperAdminConnection();
            cred.Should().NotBeNull("Because bootstrapper should have initialized it");
            c.Should().NotBeNull("because sql connection is intended");
            Action connectifier = () => c.Assign(conn => conn.Open()).Assign(conn => conn.Close());
            connectifier.ShouldNotThrow<Exception>("Because Connection To Sql Server Should Succeed");

        }

    }

}
