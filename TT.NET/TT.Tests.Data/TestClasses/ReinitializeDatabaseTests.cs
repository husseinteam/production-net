﻿using System;
using TT.Resources.Globals;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TT.Data.Tools;
using FluentAssertions;
using System.Linq;
using System.Collections.Generic;
using TT.Data.Seed.Tools;
using System.Diagnostics;

namespace TT.Tests.Data.TestClasses {

    [TestClass]
    public class ReinitializeDatabaseTests {

        [ClassInitialize]
        public static void TestInit(TestContext ctx) {

            Bootstrapper.RegisterDependencies();

        }

        [TestMethod]
        public void GenerateModelTest() {

            var exp = SDataTools.ReinitializeModels();
            exp.Should().BeNull((exp ?? new Exception("success")).Message);

        }

        [TestMethod]
        public void SeedDatabaseTest() {

            var expLi = new List<Exception>();
            expLi.Add(SDataTools.ReinitializeModels());
            expLi.AddRange(SSeedTools.SeedDatabase());
            expLi.Where(e => e != null).ToList().ForEach(e => Debug.Write(e.Message + "\r\n"));
            Assert.AreEqual(0, expLi.Where(e => e != null).Count());

        }

    }

}
